package br.com.customsaduana.ibc.structured.library;

/*
 * @SNX_GENERATE [TRUE]
 * @SNX_SCOPE [LIBRA/RIODEJANEIRO]
 * @SNX_TYPE [GENERAL_NOTICES_CODE_EXTENSION]
 * @SNX_VERSION [3.94.0.613]
 */

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import com.navis.argo.ContextHelper;
import com.navis.framework.business.Roastery;
import com.navis.inventory.business.units.Unit;
import com.navis.services.business.event.GroovyEvent;

import br.com.customaduana.fmwportn4.generalnotice.GenericGeneralNotice;
import br.com.customaduana.fmwportn4.log.LoggerN4;
import br.com.customaduana.fmwportn4.mail.MailHelper;
import br.com.customaduana.fmwportn4.scope.Scope;

public class EnvioSiscomexGeneralNoticesLibrary extends GenericGeneralNotice {
	
	public void execute(GroovyEvent inEvent) {
		sbLog = new LoggerN4(true, "ERROR");
		scope = new Scope(sbLog);
		getDependencies(sbLog, scope);
		
		sbLog.appendInfo("### EnvioSiscomexGeneralNoticesLibrary - EXECUTE");
		sbLog.appendInfo("### EVENTO TypeId -> %s ", inEvent.getEvent().getEventTypeId());
		sbLog.appendInfo("### EVENTO EntityName -> %s ", inEvent.getEvent().getEntityName());
		sbLog.appendInfo("### EVENTO AppliedToClass -> %s ", inEvent.getEvent().getEventAppliedToClass());
		sbLog.appendInfo("### EVENTO AppliedToGkey -> %s ", inEvent.getEvent().getEventAppliedToGkey());
		sbLog.appendInfo("### EVENTO AppliedToNaturalKey -> %s ", inEvent.getEvent().getEventAppliedToNaturalKey());
		
		try {
			action(inEvent);
		} catch (Exception e) {
			MailHelper mail = new MailHelper(sbLog);
			
			String emailUsr = scope.getGeneralReference("DUE", "EMAIL_RESPONSAVEL").getRefValue1();
			String emailSys = scope.getGeneralReference("DUE", "EMAIL_SYSTEM").getRefValue1();
			
			if (e.getMessage() != null) {
				mail.sendMail(emailUsr, emailSys,
						"Erro no EnvioSiscomexGeneralNoticesLibrary", e.getMessage() + sbLog.toString(), true,
						ContextHelper.getThreadUserContext());
			} else {
				mail.sendMail(emailUsr, emailSys,
						"Erro no EnvioSiscomexGeneralNoticesLibrary", sbLog.toString(), true,
						ContextHelper.getThreadUserContext());
			}
		}
		return;
	}

	public void action(GroovyEvent inEvent) {
		Unit unit = (Unit) inEvent.getEntity();
		sendToQueue(unit, inEvent);
	}
	
	private void sendToQueue(Unit unit, GroovyEvent inEvent) throws MalformedURLException, IOException, Exception {
		sbLog.setLevel(scope.getGeneralReference("BOLETIM", "CONFIGS").getRefValue1());
		sbLog.appendInfo("EnvioSiscomexGeneralNoticesLibrary sendToQueue - Inicio do processamento do Evento");
		sbLog.appendDebug("EnvioSiscomexGeneralNoticesLibrary - UNIT : %s ", unit);
		
		String queue = "";
		
		if("UNIT_RECEIVE".equals(inEvent.getEvent().getEventTypeId()) || "UNIT_RECEIVE_TESTE_SISCOMEX".equals(inEvent.getEvent().getEventTypeId()) ) {
			queue = "due-ltr-service.recepcao-nfe.queue";
		}
		
		if("UNIT_IN_VESSEL".equals(inEvent.getEvent().getEventTypeId()) || "UNIT_IN_VESSEL_TESTE_SISCOMEX".equals(inEvent.getEvent().getEventTypeId())) {
			queue = "due-ltr-service.recepcao-transbordomaritimo.queue";
		}
		
		if("".equals(queue)) {
			throw new Exception("Erro EnvioSiscomexGeneralNoticesLibrary - Evento " + inEvent.getEvent().getEventTypeId() + " não permitido.");
		}
				
		sbLog.appendInfo("### EnvioSiscomexGeneralNoticesLibrary - FILA : %s ", queue);
		
		String gkey = unit.getUnitGkey();
		HttpURLConnection connection = (HttpURLConnection) new URL(scope.getGeneralReference("MQ_CONNECTION", "MQ_CONNECTION_URL").getRefValue1() + "/message?queue=" + queue + "&message=" + gkey + "").openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();
		Scanner s = new Scanner(connection.getInputStream());
		Object object = s.hasNext() ? s.next() : "";
		sbLog.appendInfo("EnvioSiscomexGeneralNoticesLibrary - Fim do processamento do Evento - sendToQueue" + object);
	}
}
