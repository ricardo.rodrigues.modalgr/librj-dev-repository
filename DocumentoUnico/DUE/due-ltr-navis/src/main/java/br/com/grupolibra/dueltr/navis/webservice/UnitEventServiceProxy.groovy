package br.com.grupolibra.dueltr.navis.webservice;

/*
 * @SNX_GENERATE [TRUE]
 * @SNX_SCOPE [LIBRA/RIODEJANEIRO]
 * @SNX_TYPE [GROOVY_WS_CODE_EXTENSION]
 * @SNX_VERSION [3.71.0]
 */

import java.util.Map;

import br.com.customaduana.fmwportn4.webservice.GenericJsonWS;

public class UnitEventServiceProxy extends GenericJsonWS {

	public Object unitEventService;

	public String createUnitEvent(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String event    = params.get("event");
			String notes    = params.get("notes");
			return unitEventService.createUnitEvent(unitGkey, event, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	public String createBlEvent(Map<String, String> params) {
		try {
			String blGkey = params.get("blGkey");
			String event    = params.get("event");
			String notes    = params.get("notes");
			return unitEventService.createBlEvent(blGkey, event, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	public String createUnitDUEEvents(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String notes    = params.get("notes");
			return unitEventService.createUnitDUEEvents(unitGkey, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	public String applyHoldUnit(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String hold     = params.get("hold");
			String notes    = params.get("notes");
			return unitEventService.applyHoldUnit(unitGkey, hold, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	public String createDesbloqueioEvents(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String notes    = params.get("notes");
			String eventsToCreate = params.get("events_to_create");
			return unitEventService.createDesbloqueioEvents(unitGkey, notes, eventsToCreate);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
}
