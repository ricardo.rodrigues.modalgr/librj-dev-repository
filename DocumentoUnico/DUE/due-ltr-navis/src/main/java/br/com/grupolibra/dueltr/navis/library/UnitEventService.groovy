package br.com.grupolibra.dueltr.navis.library;

import java.util.Arrays;
import java.util.List;

/*
 * @SNX_GENERATE [TRUE]
 * @SNX_SCOPE [LIBRA/RIODEJANEIRO]
 * @SNX_TYPE [LIBRARY]
 * @SNX_VERSION [3.94.0.613]
 */

import com.navis.argo.business.api.ServicesManager;
import com.navis.cargo.business.model.BillOfLading;
import com.navis.framework.business.Roastery;
import com.navis.framework.metafields.MetafieldIdFactory;
import com.navis.framework.portal.QueryUtils;
import com.navis.framework.portal.query.DomainQuery;
import com.navis.framework.portal.query.PredicateFactory;
import com.navis.inventory.business.units.Unit;
import com.navis.services.business.rules.EventType;

import br.com.customaduana.fmwportn4.log.LoggerN4;

public class UnitEventService {

	private LoggerN4 sbLog;

	private static ServicesManager serviceManager = (ServicesManager) Roastery.getBean(ServicesManager.BEAN_ID);

	public String createUnitEvent(String unitGkey, String event, String notes) {
		sbLog.appendInfo("createUnitEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null) {
				EventType eventType = EventType.findEventType(event);
				if (eventType == null) {
					return "Evento " + event + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String createBlEvent(String blGkey, String event, String notes) {
		sbLog.appendInfo("createBlEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			BillOfLading bl = findBlByGkey(Long.valueOf(blGkey));
			if (bl != null && bl.getBlGkey() != null) {
				EventType eventType = EventType.findEventType(event);
				if (eventType == null) {
					return "Evento " + event + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, bl, null);
				return "OK";
			} else {
				return "BL not found : " + blGkey;
			}
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String createUnitDUEEvents(String unitGkey, String notes) {
		sbLog.appendInfo("createUnitDUEEvents");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null) {
				EventType eventType = EventType.findEventType("UNIT_VINC_DUE");
				if (eventType == null) {
					return "Evento " + eventType + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
				eventType = EventType.findEventType("LIBERACAO_PARA_EMBARQUE");
				if (eventType == null) {
					return "Evento " + eventType + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String applyHoldUnit(String unitGkey, String hold, String notes) {
		sbLog.appendInfo("applyHoldUnit");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			serviceManager.applyHold(hold, unit, null, null, notes);
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String createDesbloqueioEvents(String unitGkey, String notes, String eventsToCreate) {
		sbLog.appendError("############ createDesbloqueioEvents");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null && eventsToCreate != null && !"".equals(eventsToCreate)) {
				List<String> listaEventos = Arrays.asList(eventsToCreate.split(","));
				for (String evento : listaEventos) {
					EventType eventType = EventType.findEventType(evento.trim());
					if (eventType == null) {
						return "EVENTO " + evento + " NAO CADASTRADO";
					} 
					
					if ("LIBERACAO_DE_EXPORTACAO".equals(evento)) {
						serviceManager.recordEvent(eventType, "Liberado para Despacho", null, null, null, unit, null);
					} 
					
					if ("INCLUSAO_DOC_DESPACHO".equals(evento)) {
						serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
					} 
				}
			} 
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public BillOfLading findBlByGkey(Long gkey) {
		sbLog.appendInfo("findBlByGkey");
		Roastery.getHibernateApi().createSessionIfNecessary();
		DomainQuery dqBL = QueryUtils.createDomainQuery("BillOfLading");
		dqBL.addDqPredicate(PredicateFactory.eq(MetafieldIdFactory.valueOf("blGkey"), gkey));
		return (BillOfLading) Roastery.getHibernateApi().getUniqueEntityByDomainQuery(dqBL);
	}
}
