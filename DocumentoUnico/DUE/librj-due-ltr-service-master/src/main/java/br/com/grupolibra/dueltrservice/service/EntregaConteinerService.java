package br.com.grupolibra.dueltrservice.service;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregasConteineres;

public interface EntregaConteinerService {
	
	public EntregasConteineres getXMLEntregaConteinerNFE(Cct cct);
	
	public EntregasConteineres getXMLEntregaConteinerTransbordoMaritimo(Cct cct);
	
	EntregasConteineres getXMLEntregaConteinerDUE(Cct cct);
	
}
