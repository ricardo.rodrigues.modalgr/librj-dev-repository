package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRecebedor is a Querydsl query type for Recebedor
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRecebedor extends EntityPathBase<Recebedor> {

    private static final long serialVersionUID = -554992125L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRecebedor recebedor = new QRecebedor("recebedor");

    public final QCct cct;

    public final StringPath cnpj = createString("cnpj");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nome = createString("nome");

    public QRecebedor(String variable) {
        this(Recebedor.class, forVariable(variable), INITS);
    }

    public QRecebedor(Path<? extends Recebedor> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRecebedor(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRecebedor(PathMetadata metadata, PathInits inits) {
        this(Recebedor.class, metadata, inits);
    }

    public QRecebedor(Class<? extends Recebedor> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cct = inits.isInitialized("cct") ? new QCct(forProperty("cct"), inits.get("cct")) : null;
    }

}

