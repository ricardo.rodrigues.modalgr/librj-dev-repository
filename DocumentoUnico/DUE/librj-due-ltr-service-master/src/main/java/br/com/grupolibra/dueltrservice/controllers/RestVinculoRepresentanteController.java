package br.com.grupolibra.dueltrservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueltrservice.dto.VinculoRepresentanteDTO;
import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;
import br.com.grupolibra.dueltrservice.service.VinculoRepresentanteService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestVinculoRepresentanteController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private VinculoRepresentanteService vinculoRepresentanteService;

	@CrossOrigin
	@RequestMapping(value = "api/vinculo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO findAllVinculo(@RequestParam("searchtext") String searchText, @RequestParam("pageindex") String pageIndex, @RequestParam("pagesize") String pageSize) {
		logger.info("findAllVinculo -> searchText: {} - pageIndex: {} - pageSize: {} ", searchText, pageIndex, pageSize);
		return vinculoRepresentanteService.findAllVinculo(searchText, pageIndex, pageSize);
	}

	@CrossOrigin
	@RequestMapping(value = "api/representante", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO findRepresentante(@RequestParam("cnpj") String cnpj, @RequestParam("nome") String nome) {
		logger.info("findAllVinculo -> cnpj: {} - nome: {} ", cnpj, nome);
		return vinculoRepresentanteService.findRepresentante(cnpj, nome);
	}

	@CrossOrigin
	@RequestMapping(value = "api/vinculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO vincular(@RequestBody(required = true) VinculoRepresentante vinculo) {
		logger.info("salvaListaCFP -> vinculo: {} ", vinculo.toString());
		return vinculoRepresentanteService.vincular(vinculo);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/desvinculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO desvincular(@RequestBody(required = true) VinculoRepresentante vinculo) {
		logger.info("salvaListaCFP -> vinculo: {} ", vinculo.toString());
		return vinculoRepresentanteService.desvincular(vinculo);
	}

}