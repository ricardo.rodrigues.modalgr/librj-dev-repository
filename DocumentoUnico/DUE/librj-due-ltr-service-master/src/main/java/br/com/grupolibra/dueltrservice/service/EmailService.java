package br.com.grupolibra.dueltrservice.service;

public interface EmailService {
	
	public String sendSimpleMessage(String to, String subject, String mensagem);
	
}
