package br.com.grupolibra.dueltrservice.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.Monitoramento;

@Repository
public interface MonitoramentoRepository extends CrudRepository<Monitoramento, Long>, QuerydslPredicateExecutor<Monitoramento> {
	
}