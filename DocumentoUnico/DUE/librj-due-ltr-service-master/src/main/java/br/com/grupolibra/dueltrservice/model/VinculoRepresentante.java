package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "VINCULO_REPRESENTANTE")
@NamedQuery(name = "VinculoRepresentante.findAll", query = "SELECT vr FROM VinculoRepresentante vr")
public class VinculoRepresentante implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vinculo_representante_seq")
	@SequenceGenerator(name = "vinculo_representante_seq", sequenceName = "vinculo_representante_seq", allocationSize = 1)
	private Long id;

	@Column(name = "ID_EXTERNO")
	private String idExterno;

	@Column(name = "CODIGO_LINE_OPERATOR")
	private String codigo;

	@Column(name = "NOME_LINE_OPERATOR")
	private String nomeLineOp;

	@Column(name = "CNPJ_REPRESENTANTE")
	private String cnpj;

	@Column(name = "NOME_REPRESENTANTE")
	private String nomeRepresentante;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNomeLineOp() {
		return nomeLineOp;
	}

	public void setNomeLineOp(String nomeLineOp) {
		this.nomeLineOp = nomeLineOp;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeRepresentante() {
		return nomeRepresentante;
	}

	public void setNomeRepresentante(String nomeRepresentante) {
		this.nomeRepresentante = nomeRepresentante;
	}

	@Override
	public String toString() {
		return "VinculoRepresentante [id=" + id + ", idExterno=" + idExterno + ", codigo=" + codigo + ", nomeLineOp=" + nomeLineOp + ", cnpj=" + cnpj + ", nomeRepresentante=" + nomeRepresentante + "]";
	}

}
