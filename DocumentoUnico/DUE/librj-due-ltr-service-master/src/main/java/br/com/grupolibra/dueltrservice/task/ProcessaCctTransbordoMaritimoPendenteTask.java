package br.com.grupolibra.dueltrservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class ProcessaCctTransbordoMaritimoPendenteTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;
	
	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void processaCctTransbordoMaritimoPendente() {
		logger.info("###### INICIO TASK PROCESSA CCT TRANSBORDO MARITIMO PENDENTE");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
//		logger.info("processaCctTransbordoMaritimoPendente");
		List<Cct> cctListTransbordoMaritimoPendente = cctService.getCctTransbordoMaritimoPendente();
		if (cctListTransbordoMaritimoPendente != null && !cctListTransbordoMaritimoPendente.isEmpty()) {
			
//			logger.info("processaCctTransbordoMaritimoPendente - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListTransbordoMaritimoPendenteAux.size());
//			List<Cct> cctListTransbordoMaritimoPendente = n4Repository.validaUnitFacilityVisitStateActive(cctListTransbordoMaritimoPendenteAux);
			
			logger.info("processaCctTransbordoMaritimoPendente - qtd registros transbordo maritimo pendente : {} ", cctListTransbordoMaritimoPendente.size());
			n4Repository.validaCorrecaoPendenciasCctTrasbordoMaritimo(cctListTransbordoMaritimoPendente);
			if (!cctListTransbordoMaritimoPendente.isEmpty()) {
				logger.info("processaCctTransbordoMaritimoPendente - qtd registros transbordo maritimo pendente corrigidos : {} ", cctListTransbordoMaritimoPendente.size());
				for (Cct cct : cctListTransbordoMaritimoPendente) {
					cct.setStatusProc(Cct.STATUS_AGUARDANDO_DUE_TRANSBORDO_MARITIMO);
					cct.setStatus(Cct.STATUS_OK);
					cct.setErro(null);
					cct.setMensagem(null);
//					cctRepository.save(cct);
					cctService.save(cct);
				}
			}
		} else {
			logger.info("processaCctTransbordoMaritimoPendente - sem registro transbordo maritimo pendente");
		}
	}
}
