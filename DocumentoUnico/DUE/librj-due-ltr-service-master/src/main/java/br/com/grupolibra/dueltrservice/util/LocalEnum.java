package br.com.grupolibra.dueltrservice.util;

public enum LocalEnum {

	LIBRA_TERMINAL_RJ("7921302"), PORTO_DE_RJ("0717600"), LATITUDE("-22.873438"), LONGITUDE("-43.205887");

	private String value;

	LocalEnum(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
