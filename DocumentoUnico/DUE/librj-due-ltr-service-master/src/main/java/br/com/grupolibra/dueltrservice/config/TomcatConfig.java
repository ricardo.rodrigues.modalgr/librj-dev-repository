package br.com.grupolibra.dueltrservice.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Configuration
public class TomcatConfig {

	@Bean
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
		tomcat.setPort(Integer.parseInt(PropertiesUtil.getProp().getProperty("server.port.http")));
		tomcat.addAdditionalTomcatConnectors(createSslConnector());
		return tomcat;
	}

	private Connector createSslConnector() {
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();

		connector.setPort(Integer.parseInt(PropertiesUtil.getProp().getProperty("server.port.https")));
		connector.setScheme("https");
		connector.setSecure(true);
		connector.setURIEncoding("UTF-8");

		protocol.setKeyAlias(PropertiesUtil.getSSLProp().getProperty("server.ssl.keyAlias"));
		protocol.setKeystorePass(PropertiesUtil.getSSLProp().getProperty("server.ssl.key-store-password"));
		protocol.setKeystoreFile(PropertiesUtil.getProp().getProperty("ssl.path.properties") + PropertiesUtil.getSSLProp().getProperty("server.ssl.key-store"));
		protocol.setKeystoreType(PropertiesUtil.getSSLProp().getProperty("server.ssl.keyStoreType"));
		protocol.setSSLEnabled(true);

		protocol.setSSLCACertificatePath(PropertiesUtil.getProp().getProperty("ssl.path.properties"));
		protocol.setSSLCACertificateFile(PropertiesUtil.getSSLProp().getProperty("SSLCACertificateFile"));

		return connector;
	}

}
