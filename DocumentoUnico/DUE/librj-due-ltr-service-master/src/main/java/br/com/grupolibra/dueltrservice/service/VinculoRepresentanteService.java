package br.com.grupolibra.dueltrservice.service;

import br.com.grupolibra.dueltrservice.dto.VinculoRepresentanteDTO;
import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;

public interface VinculoRepresentanteService {
	
	VinculoRepresentanteDTO findAllVinculo(String searchText, String pageIndex, String pageSize);

	VinculoRepresentanteDTO findRepresentante(String cnpj, String nome);

	VinculoRepresentanteDTO vincular(VinculoRepresentante vinculo);

	VinculoRepresentanteDTO desvincular(VinculoRepresentante vinculo);
	
}
