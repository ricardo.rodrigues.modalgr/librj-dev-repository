package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDanfe is a Querydsl query type for Danfe
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDanfe extends EntityPathBase<Danfe> {

    private static final long serialVersionUID = 85517988L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDanfe danfe = new QDanfe("danfe");

    public final QCct cct;

    public final StringPath chave = createString("chave");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QDanfe(String variable) {
        this(Danfe.class, forVariable(variable), INITS);
    }

    public QDanfe(Path<? extends Danfe> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDanfe(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDanfe(PathMetadata metadata, PathInits inits) {
        this(Danfe.class, metadata, inits);
    }

    public QDanfe(Class<? extends Danfe> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cct = inits.isInitialized("cct") ? new QCct(forProperty("cct"), inits.get("cct")) : null;
    }

}

