package br.com.grupolibra.dueltrservice.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.grupolibra.dueltrservice.model.Armador;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;
import br.com.grupolibra.dueltrservice.model.siscomex.Conteiner;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregaConteiner;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.Local;
import br.com.grupolibra.dueltrservice.model.siscomex.Recebedor;
import br.com.grupolibra.dueltrservice.repository.VinculoRepresentanteRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.EntregaConteinerService;
import br.com.grupolibra.dueltrservice.util.LocalEnum;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Service
public class EntregaConteinerServiceImpl implements EntregaConteinerService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private VinculoRepresentanteRepository vinculoRepresentanteRepository; 
	
	@Autowired
	private N4Repository n4Repository;

	@Override
	public EntregasConteineres getXMLEntregaConteinerNFE(Cct cct) {
//		logger.info("getXMLEntregaConteinerNFE");
		EntregasConteineres entregasConteineres = new EntregasConteineres();
		EntregaConteiner entregaConteiner = new EntregaConteiner();
		List<EntregaConteiner> listEntregaConteiner = new ArrayList<>();
		entregaConteiner.setIdentificacaoEntrega("ENTCNT|" + cct.getNumeroCont() + new Date().getTime());
		entregaConteiner.setIdentificacaoPessoaJuridica(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		entregaConteiner.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		setLocal(entregaConteiner);
		setRecebedorNFE(cct, entregaConteiner);
		List<Conteiner> conteineres = getListConteineres(cct, entregaConteiner);
		entregaConteiner.setConteineres(conteineres);
		listEntregaConteiner.add(entregaConteiner);
		entregasConteineres.setEntregaConteiner(listEntregaConteiner);
		return entregasConteineres;
	}

	private void setRecebedorNFE(Cct cct, EntregaConteiner entregaConteiner) {
//		logger.info("setRecebedorNFE");
		Recebedor recebedor = new Recebedor();
		Armador armador = cct.getArmador();
		if(armador.getEstrangeiro() != null && armador.getEstrangeiro() == 1) {
			String cnpj = getCNPJRepresentanteByArmadorId(armador.getNome());
			if(cnpj != null) {
				recebedor.setCnpj(cnpj);
				recebedor.setViaTransporte(Recebedor.MARITIMA);
			} else {
				recebedor.setNomeEstrangeiro(armador.getNome().substring(0, 59));
			}
		} else {
			recebedor.setCnpj(armador.getCnpj());
			recebedor.setViaTransporte(Recebedor.MARITIMA);
		}
		entregaConteiner.setRecebedor(recebedor);	
	}
	
	private void setRecebedorTransbordoMaritimo(Cct cct, EntregaConteiner entregaConteiner) {
//		logger.info("setRecebedorTransbordoMaritimo");
		Recebedor recebedor = new Recebedor();
		if(cct.getRecebedor() != null) {
			if(cct.getRecebedor().getNome() != null) {
				String cnpj = getCNPJRepresentanteByArmadorId(cct.getRecebedor().getNome());
				if(cnpj != null) {
					recebedor.setCnpj(cnpj);
					recebedor.setBaldeacaoOuTransbordo("S");
				} else {
					String armador = n4Repository.getRecebedorIdByConteinerGkey(cct.getIdExterno().toString());
					if(armador != null) {
						recebedor.setNomeEstrangeiro(armador.substring(0, 59));
					} else {
						recebedor.setNomeEstrangeiro(cct.getRecebedor().getNome().substring(0, 59));
					} 
				}
			} else {
				recebedor.setCnpj(cct.getRecebedor().getCnpj());
				recebedor.setBaldeacaoOuTransbordo("S");
			}
		}
		entregaConteiner.setRecebedor(recebedor);	
	}

	private void setLocal(EntregaConteiner entregaConteiner) {
//		logger.info("setLocal");
		Local local = new Local();
		local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
		entregaConteiner.setLocal(local);
	}
	
	private List<Conteiner> getListConteineres(Cct cct, EntregaConteiner entregaConteiner) {
//		logger.info("getListConteineres");
		List<Conteiner> conteineres = new ArrayList<>();
		Conteiner conteiner = new Conteiner();
		conteiner.setTara(cct.getTara().setScale(3));
		conteiner.setNumeroConteiner(cct.getNumeroCont());
		
		if(cct.getPesoBruto() != null) {
			conteiner.setPesoAferido(cct.getPesoBruto().setScale(3));
		} else if(cct.getMotivoNaoPesagem() != null && !cct.getMotivoNaoPesagem().isEmpty()) {
			conteiner.setMotivoNaoPesagem(cct.getMotivoNaoPesagem());
		}
		
		List<String> lacres = Arrays.asList(cct.getLacres().split(","));
		conteiner.setLacres(lacres);
		conteineres.add(conteiner);
		if (!StringUtils.isEmpty(cct.getAvaria())) {
			entregaConteiner.setAvariasIdentificadas(cct.getAvaria());
		} 
		return conteineres;
	}

	@Override
	public EntregasConteineres getXMLEntregaConteinerTransbordoMaritimo(Cct cct) {
//		logger.info("getXMLEntregaConteinerTransbordoMaritimo");
		EntregasConteineres entregasConteineres = new EntregasConteineres();
		EntregaConteiner entregaConteiner = new EntregaConteiner();
		List<EntregaConteiner> listEntregaConteiner = new ArrayList<>();
		entregaConteiner.setIdentificacaoEntrega("ENTCNT|" + cct.getNumeroCont() + new Date().getTime());
		entregaConteiner.setIdentificacaoPessoaJuridica(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		entregaConteiner.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		setLocal(entregaConteiner);
		setRecebedorTransbordoMaritimo(cct, entregaConteiner);
		List<Conteiner> conteineres = getListConteineres(cct, entregaConteiner);
		entregaConteiner.setConteineres(conteineres);
		listEntregaConteiner.add(entregaConteiner);
		entregasConteineres.setEntregaConteiner(listEntregaConteiner);
		return entregasConteineres;
	}
	
	@Override
	public EntregasConteineres getXMLEntregaConteinerDUE(Cct cct) {
//		logger.info("getXMLEntregaConteinerNFE");
		EntregasConteineres entregasConteineres = new EntregasConteineres();
		EntregaConteiner entregaConteiner = new EntregaConteiner();
		List<EntregaConteiner> listEntregaConteiner = new ArrayList<>();
		entregaConteiner.setIdentificacaoEntrega("ENTCNT|" + cct.getNumeroCont() + new Date().getTime());
		entregaConteiner.setIdentificacaoPessoaJuridica(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		entregaConteiner.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		setLocal(entregaConteiner);
		setRecebedorDUE(cct, entregaConteiner);
		List<Conteiner> conteineres = getListConteineres(cct, entregaConteiner);
		entregaConteiner.setConteineres(conteineres);
		listEntregaConteiner.add(entregaConteiner);
		entregasConteineres.setEntregaConteiner(listEntregaConteiner);
		return entregasConteineres;
	}

	private void setRecebedorDUE(Cct cct, EntregaConteiner entregaConteiner) {
//		logger.info("setRecebedorDUE");
		Recebedor recebedor = new Recebedor();
		if(cct.getRecebedor() != null) {
			if(cct.getRecebedor().getNome() != null) {
				String cnpj = getCNPJRepresentanteByArmadorId(cct.getRecebedor().getNome());
				if(cnpj != null) {
					recebedor.setCnpj(cnpj);
					recebedor.setBaldeacaoOuTransbordo("S");
				} else {
					String armador = n4Repository.getRecebedorIdByConteinerGkey(cct.getIdExterno().toString());
					if(armador != null) {
						recebedor.setNomeEstrangeiro(armador.substring(0, 59));
					} else {
						recebedor.setNomeEstrangeiro(cct.getRecebedor().getNome().substring(0, 59));
					}
				}
			} else {
				recebedor.setCnpj(cct.getRecebedor().getCnpj());
				recebedor.setBaldeacaoOuTransbordo("S");
			}
			
		}
		entregaConteiner.setRecebedor(recebedor);	
	}
	
	public String getCNPJRepresentanteByArmadorId(String armadorId) {
		List<VinculoRepresentante> lstAux = vinculoRepresentanteRepository.findByCodigo(armadorId);
		if(lstAux != null && !lstAux.isEmpty()) {
			return lstAux.get(0).getCnpj();
		} else {
			return null;
		}
	}
}
