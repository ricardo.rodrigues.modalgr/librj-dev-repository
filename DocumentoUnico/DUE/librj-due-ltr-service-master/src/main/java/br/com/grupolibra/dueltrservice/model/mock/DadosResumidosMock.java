package br.com.grupolibra.dueltrservice.model.mock;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;

@Entity
@Table(name = "DADOS_RESUMIDOS_MOCK")
@NamedQuery(name = "DadosResumidosMock.findAll", query = "SELECT d FROM DadosResumidosMock d")
public class DadosResumidosMock {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DADOS_RESUMIDOS_MOCK_SEQ")
	@SequenceGenerator(name = "DADOS_RESUMIDOS_MOCK_SEQ", sequenceName = "DADOS_RESUMIDOS_MOCK_SEQ", allocationSize = 1)
	private int id;
	
	private String numeroDUE;
	
	private String situacaoDUE;
	
	private String indicadorBloqueio;
	
	@OneToMany(mappedBy = "dadosResumidosMock", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = ExportadorMock.class)
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	private List<ExportadorMock> exportadores;

	public int getId() {
		return id;
	}

	public String getNumeroDUE() {
		return numeroDUE;
	}

	public String getSituacaoDUE() {
		return situacaoDUE;
	}

	public String getIndicadorBloqueio() {
		return indicadorBloqueio;
	}

	public List<ExportadorMock> getExportadores() {
		return exportadores;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNumeroDUE(String numeroDUE) {
		this.numeroDUE = numeroDUE;
	}

	public void setSituacaoDUE(String situacaoDUE) {
		this.situacaoDUE = situacaoDUE;
	}

	public void setIndicadorBloqueio(String indicadorBloqueio) {
		this.indicadorBloqueio = indicadorBloqueio;
	}

	public void setExportadores(List<ExportadorMock> exportadores) {
		this.exportadores = exportadores;
	}
	
}
