package br.com.grupolibra.dueltrservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DueServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DueServiceApplication.class, args);
	}
}
