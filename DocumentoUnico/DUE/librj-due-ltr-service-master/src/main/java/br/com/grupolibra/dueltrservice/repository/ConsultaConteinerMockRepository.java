package br.com.grupolibra.dueltrservice.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.mock.ConsultaConteinerMock;

@Repository
public interface ConsultaConteinerMockRepository extends CrudRepository<ConsultaConteinerMock, Long>, QuerydslPredicateExecutor<ConsultaConteinerMock> {
	
	public ConsultaConteinerMock findByNrConteiner(String nrConteiner);
	
}