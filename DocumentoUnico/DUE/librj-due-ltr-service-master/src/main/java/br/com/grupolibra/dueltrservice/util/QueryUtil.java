package br.com.grupolibra.dueltrservice.util;

public class QueryUtil {
	
	private static final String USERDATABASE = PropertiesUtil.getProp().getProperty("n4.datasource.schema");

	private QueryUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static final String SELECT_DADOS_TRANSPORTADORA_CONDUTOR = new StringBuilder()
			.append(" SELECT DRIVER.FLEX_STRING01 CPFCONDUTOR							\r\n" +
					"      , TRUCKCO.WEBSITE_URL CNPJTRANSPORTADOR						\r\n" +
					"      , DRIVER2ST.CUSTOMD2STRA_CUSTOMTRALER1 PLACA_VEICULO			\r\n" +
					"      , DRIVER2ST.CUSTOMD2STRA_TRUCK PLACA_REBOQUE 				\r\n" +
					"      , TRUCKCO.SMS_NUMBER TIPO 									\r\n" +
					"      , TRUCKCO.NAME NOME_TRANSPORTADORA							\r\n" +		
					"      , DRIVER.NAME NOME_MOTORISTA									\r\n" +
					"   FROM "+USERDATABASE+".CUSTOM_SCHEDULETRANSIT TRANSIT			\r\n" +
					"      , "+USERDATABASE+".ROAD_TRUCKING_COMPANIES TRUCK				\r\n" +
					"      , "+USERDATABASE+".REF_BIZUNIT_SCOPED TRUCKCO				\r\n" +
					"      , "+USERDATABASE+".CUSTOM_DRIVER2SCHEDULETRANSIT DRIVER2ST	\r\n" +
					"      , "+USERDATABASE+".ROAD_TRUCK_DRIVERS DRIVER					\r\n" +
					"  WHERE TRANSIT.CUSTOMSCTRAN_CUSTOMTRUCKCO = TRUCK.TRKC_ID			\r\n" +
					"    AND TRUCK.TRKC_ID = TRUCKCO.GKEY								\r\n" +
					"    AND TRANSIT.GKEY = DRIVER2ST.CUSTOMD2STRA_CUSTOMSCHEDULETRA	\r\n" +
					"    AND DRIVER2ST.CUSTOMD2STRA_CUSTOMDRIVER = DRIVER.GKEY			\r\n" +
					"    AND TRANSIT.CUSTOMSCTRAN_UNITGKEY = :UNITGKEY ")
			.toString();

	public static final String SELECT_CHAVE_DANFE = new StringBuilder()
			.append(" SELECT DOC.CUSTOMD_DANFE														\r\n" +
					"   FROM "+USERDATABASE+".CUSTOM_SCHEDULETRANSIT TRANSIT						\r\n" +
					"      , "+USERDATABASE+".CUSTOM_DOCUMENT2SCHEDULETRANSI TRANSITDOCUMENT		\r\n" +
					"      , "+USERDATABASE+".CUSTOM_DOCUMENT DOC									\r\n" +
					"      , "+USERDATABASE+".CUSTOM_TYPEDOCUMENT TIPO								\r\n" +
					"  WHERE TRANSIT.GKEY = TRANSITDOCUMENT.CUSTOMDO2STR_CUSTOMSCHEDULETRA			\r\n" +
					"	 AND TRANSITDOCUMENT.CUSTOMDO2STR_CUSTOMDOCUMENT = DOC.GKEY					\r\n" +
					"	 AND DOC.CUSTOMD_TYPEDOC = TIPO.GKEY										\r\n" +
					"	 AND TIPO.CUSTOMTPDOC_SHORTNAME = 'NF'										\r\n" +
					"    AND DOC.CUSTOMD_DANFE IS NOT NULL 											\r\n" +			
					"	 AND TRANSIT.CUSTOMSCTRAN_UNITGKEY = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_CHAVE_DUE = new StringBuilder()
			.append(" SELECT DOC.CUSTOMD_NBR_S														\r\n" +
					"   FROM "+USERDATABASE+".CUSTOM_SCHEDULETRANSIT TRANSIT						\r\n" +
					"      , "+USERDATABASE+".CUSTOM_DOCUMENT2SCHEDULETRANSI TRANSITDOCUMENT		\r\n" +
					"      , "+USERDATABASE+".CUSTOM_DOCUMENT DOC									\r\n" +
					"      , "+USERDATABASE+".CUSTOM_TYPEDOCUMENT TIPO								\r\n" +
					"  WHERE TRANSIT.GKEY = TRANSITDOCUMENT.CUSTOMDO2STR_CUSTOMSCHEDULETRA			\r\n" +
					"	 AND TRANSITDOCUMENT.CUSTOMDO2STR_CUSTOMDOCUMENT = DOC.GKEY					\r\n" +
					"	 AND DOC.CUSTOMD_TYPEDOC = TIPO.GKEY										\r\n" +
					"	 AND TIPO.CUSTOMTPDOC_SHORTNAME = 'DUE'										\r\n" +
					"	 AND TRANSIT.CUSTOMSCTRAN_UNITGKEY = :UNITGKEY ")
			.toString();

	public static final String SELECT_DADOS_CONTEINER = new StringBuilder()
			.append(" SELECT UNIT.ID NUMERO_CONTEINER				\r\n" +
					"      , E.TARE_KG TARA							\r\n" +
					"      , UNIT.GOODS_AND_CTR_WT_KG PESO_AFERIDO	\r\n" +
					"      , UNIT.SEAL_NBR1 LACRE1					\r\n" +
					"      , UNIT.SEAL_NBR2 LACRE2					\r\n" +
					"      , UNIT.SEAL_NBR3 LACRE3					\r\n" +
					"      , UNIT.SEAL_NBR4 LACRE4					\r\n" +
					"      , UNIT.PRIMARY_UE AVARIA_FK				\r\n" +
					"	   , CASE WHEN UNIT.GOODS_AND_CTR_WT_KG = UNIT.GOODS_CTR_WT_KG_ADVISED THEN ''    \r\n" +                                                                         
					"             WHEN ((UNIT.GOODS_AND_CTR_WT_KG - UNIT.GOODS_CTR_WT_KG_ADVISED) > 0 AND (UNIT.GOODS_AND_CTR_WT_KG - UNIT.GOODS_CTR_WT_KG_ADVISED) > (UNIT.GOODS_AND_CTR_WT_KG * :DIVERGENCIA_PESO_ACIMA)) THEN 'PESO DIVERGENTE'   \r\n" +
					"             WHEN ((UNIT.GOODS_AND_CTR_WT_KG - UNIT.GOODS_CTR_WT_KG_ADVISED) < 0 AND ((UNIT.GOODS_AND_CTR_WT_KG - UNIT.GOODS_CTR_WT_KG_ADVISED)* (-1)) > (UNIT.GOODS_AND_CTR_WT_KG * :DIVERGENCIA_PESO_ABAIXO)) THEN 'PESO DIVERGENTE' \r\n" + 
					"        ELSE ''   								\r\n" + 						                                                                                                               
					"        END 									\r\n" +
					"   FROM "+USERDATABASE+".INV_UNIT UNIT 				\r\n" +
					"      , "+USERDATABASE+".INV_UNIT_EQUIP UE 			\r\n" +
					"      , "+USERDATABASE+".REF_EQUIPMENT E				\r\n" +
					"  WHERE  UNIT.GKEY = UE.UNIT_GKEY				\r\n" +
					"    AND UE.EQ_GKEY = E.GKEY 					\r\n" +
					"    AND UNIT.GKEY = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_DADOS_LACRES = new StringBuilder()
			.append(" SELECT UNIT.SEAL_NBR1 LACRE1	\r\n" +
					"      , UNIT.SEAL_NBR2 LACRE2	\r\n" +
					"      , UNIT.SEAL_NBR3 LACRE3	\r\n" +
					"      , UNIT.SEAL_NBR4 LACRE4	\r\n" +
					"   FROM "+USERDATABASE+".INV_UNIT UNIT \r\n" +
					"  WHERE UNIT.GKEY = :UNITGKEY ")
			.toString();

	public static final String SELECT_AVARIAS = new StringBuilder()
			.append(" SELECT REFEQDAMAGE.DESCRIPTION                      					\r\n" +
					"   FROM "+USERDATABASE+".INV_UNIT_EQUIP UNITEQUIP                            	\r\n" +
					"   LEFT OUTER JOIN "+USERDATABASE+".INV_UNIT_EQUIP_DAMAGES EQDAMAGES         	\r\n" +
					"	  ON (EQDAMAGES.GKEY = UNITEQUIP.DMGS_GKEY)          				\r\n" +
					"	LEFT OUTER JOIN "+USERDATABASE+".INV_UNIT_EQUIP_DAMAGE_ITEM EQDAMAGESITEM 	\r\n" +
					"	  ON (EQDAMAGESITEM.DMGS_GKEY = EQDAMAGES.GKEY)      				\r\n" +
					"	LEFT OUTER JOIN "+USERDATABASE+".REF_EQUIP_DAMAGE_TYPES REFEQDAMAGE       	\r\n" +
					"	  ON (REFEQDAMAGE.GKEY = EQDAMAGESITEM.TYPE_GKEY)    				\r\n" +
					"  WHERE EQDAMAGESITEM.SEVERITY = 'MAJOR'								\r\n" +
					"    AND UNITEQUIP.GKEY = :AVARIA_FK ")
			.toString();

	public static final String SELECT_ARMADOR_POR_NUMERO_CONTEINER = new StringBuilder()
			.append(" SELECT LINEOP.*                                  	\r\n" +
					"   FROM "+USERDATABASE+".INV_EQ_BASE_ORDER BKG             \r\n" +
					"      , "+USERDATABASE+".INV_EQ_BASE_ORDER_ITEM BKG_ITEM   \r\n" +
					"      , "+USERDATABASE+".INV_UNIT_EQUIP  UE                \r\n" +
					"      , "+USERDATABASE+".REF_BIZUNIT_SCOPED LINEOP         \r\n" +
					"  WHERE BKG.SUB_TYPE = 'BOOK'					  	\r\n" +
					"    AND LINEOP.GKEY = BKG.LINE_GKEY 				\r\n" +
					"    AND BKG_ITEM.EQO_GKEY = BKG.GKEY               \r\n" +
					"    AND UE.DEPART_ORDER_ITEM_GKEY = BKG_ITEM.GKEY  \r\n" +
					"    AND UE.UNIT_GKEY = :UNITGKEY")
			.toString();
	
	public static final String SELECT_ARMADOR_GKEY_POR_NUMERO_CONTEINER = new StringBuilder()
			.append(" SELECT LINEOP.GKEY                                  		\r\n" +
					"   FROM "+USERDATABASE+".INV_EQ_BASE_ORDER BKG             \r\n" +
					"      , "+USERDATABASE+".INV_EQ_BASE_ORDER_ITEM BKG_ITEM   \r\n" +
					"      , "+USERDATABASE+".INV_UNIT_EQUIP  UE                \r\n" +
					"      , "+USERDATABASE+".REF_BIZUNIT_SCOPED LINEOP         \r\n" +
					"  WHERE BKG.SUB_TYPE = 'BOOK'					  			\r\n" +
					"    AND LINEOP.GKEY = BKG.LINE_GKEY 						\r\n" +
					"    AND BKG_ITEM.EQO_GKEY = BKG.GKEY               		\r\n" +
					"    AND UE.DEPART_ORDER_ITEM_GKEY = BKG_ITEM.GKEY  		\r\n" +
					"    AND UE.UNIT_GKEY = :UNITGKEY")
			.toString();

	public static final String SELECT_BOOKING_NUMBER_POR_NUMERO_CONTEINER = new StringBuilder()
			.append(" SELECT BKG.NBR                 				 	\r\n" +
					"   FROM "+USERDATABASE+".INV_EQ_BASE_ORDER BKG             \r\n" +
					"      , "+USERDATABASE+".INV_EQ_BASE_ORDER_ITEM BKG_ITEM   \r\n" +
					"      , "+USERDATABASE+".INV_UNIT_EQUIP  UE                \r\n" +
					"  WHERE BKG.SUB_TYPE = 'BOOK'  					\r\n" +
					"    AND BKG_ITEM.EQO_GKEY = BKG.GKEY              	\r\n" +
					"    AND UE.DEPART_ORDER_ITEM_GKEY = BKG_ITEM.GKEY  \r\n" +
					"    AND UE.UNIT_GKEY = :UNITGKEY")
			.toString();
	
	public static final String SELECT_BOOKING_GKEY_BY_BOOKING_NBR = new StringBuilder()
			.append(" SELECT GKEY FROM "+USERDATABASE+".INV_EQ_BASE_ORDER BKG WHERE NBR = :NBR ORDER BY CHANGED DESC, CREATED DESC ").toString();
			
	public static final String SELECT_EVENT_PLACED_TIME = new StringBuilder()
			.append(" SELECT TO_CHAR(E.PLACED_TIME, 'dd/MM/yyyy HH24:mi:ss')  AS DATA_ENTRADA	\r\n" + 
					"   FROM "+USERDATABASE+".SRV_EVENT E 												\r\n" +
					"	   , "+USERDATABASE+".SRV_EVENT_TYPES ET 										\r\n" +
					"	   , "+USERDATABASE+".INV_UNIT U  												\r\n" +
					"  WHERE E.APPLIED_TO_GKEY = U.GKEY 										\r\n" +
					"	 AND ET.GKEY = E.EVENT_TYPE_GKEY 										\r\n" +
					"	 AND E.APPLIED_TO_CLASS = 'UNIT'										\r\n" +
					"	 AND (ET.ID = :EVENT OR ET.ID = :EVENTTEST) 	\r\n" +
					"	 AND U.GKEY = :UNITGKEY 												\r\n" +
					"    ORDER BY 1 DESC ")
			.toString();

	public static final String SELECT_UNIT_SEAL_EVENT = new StringBuilder()
			.append(" SELECT E.PLACED_TIME					\r\n" + 
					"   FROM "+USERDATABASE+".SRV_EVENT E 			\r\n" +
					"	   , "+USERDATABASE+".SRV_EVENT_TYPES ET 	\r\n" +
					"	   , "+USERDATABASE+".INV_UNIT U  			\r\n" +
					"  WHERE E.APPLIED_TO_GKEY = U.GKEY 	\r\n" +
					"	 AND ET.GKEY = E.EVENT_TYPE_GKEY 	\r\n" +
					"	 AND E.APPLIED_TO_CLASS = 'UNIT'	\r\n" + 
					"	 AND ET.ID = 'UNIT_SEAL'			\r\n" +
					"	 AND U.GKEY = :UNITGKEY")
			.toString();
	
	public static final String SELECT_REGRA_PESAGEM_RECEPCAO = new StringBuilder()
			.append(" SELECT UNIT.IS_OOG 								\r\n" + 
					"      , UFV.FLEX_STRING06 FIRST_WEIGHING			\r\n" + 
					"   FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV	\r\n" + 
					"      , "+USERDATABASE+".INV_UNIT UNIT				\r\n" + 
					"  WHERE UFV.UNIT_GKEY = UNIT.GKEY					\r\n" +
					"    AND UFV.VISIT_STATE = '1ACTIVE' 				\r\n" + 
					"    AND UNIT.GKEY = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_VESSEL_VISIT_ACTUAL_TIME_ARRIVAL_BY_SCHEDULE_TRANSIT = new StringBuilder()
			.append(" SELECT CARRIER.ATA											\r\n" + 
					"	FROM "+USERDATABASE+".CUSTOM_SCHEDULETRANSIT TRANSIT		\r\n" + 
					"	   , "+USERDATABASE+".VSL_VESSEL_VISIT_DETAILS VVD			\r\n" + 
					"	   , "+USERDATABASE+".ARGO_CARRIER_VISIT CARRIER			\r\n" + 
					"  WHERE TRANSIT.CUSTOMSCTRAN_CUSTOMVESSELVISIT = VVD.VVD_GKEY	\r\n" + 
					"	 AND VVD.VVD_GKEY = CARRIER.CVCVD_GKEY						\r\n" + 
					"	 AND TRANSIT.CUSTOMSCTRAN_UNITGKEY = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_VESSEL_VISIT_ACTUAL_TIME_ARRIVAL_BY_OUTBOUND_CARRIER = new StringBuilder()
			.append(" SELECT S.ATA FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S WHERE GKEY IN 			\r\n" + 
					" (SELECT ACTUAL_OB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV WHERE GKEY = 	\r\n" +  
					" (SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY)) ").toString();
	
	public static final String SELECT_DADOS_ARMADOR_BY_AGENDAMENTO = new StringBuilder()
			.append(" SELECT ARMADOR.GKEY ID_EXTERNO								\r\n" + 
					"      , ARMADOR.ID NOME										\r\n" +	
					"	   , ARMADOR.SMS_NUMBER TIPO								\r\n" + 
					"      , ARMADOR.WEBSITE_URL DOCUMENTO							\r\n" + 
					"      , VESSEL.NAME NOME_NAVIO									\r\n" +
					"  FROM "+USERDATABASE+".CUSTOM_SCHEDULETRANSIT TRANSIT					\r\n" + 
					"     , "+USERDATABASE+".VSL_VESSEL_VISIT_DETAILS VVD					\r\n" + 
					"     , "+USERDATABASE+".REF_BIZUNIT_SCOPED ARMADOR						\r\n" + 
					"     , "+USERDATABASE+".VSL_VESSELS VESSEL		                        \r\n" +
					" WHERE TRANSIT.CUSTOMSCTRAN_CUSTOMVESSELVISIT = VVD.VVD_GKEY	\r\n" + 
					"   AND VVD.BIZU_GKEY = ARMADOR.GKEY							\r\n" +
					"   AND VVD.VESSEL_GKEY = VESSEL.GKEY (+) 						\r\n" +
					"   AND TRANSIT.CUSTOMSCTRAN_UNITGKEY = :UNITGKEY ")
			.toString();
		
	public static final String SELECT_EVENTOSLIBERACAO_BY_GKEY = new StringBuilder()
			.append(" SELECT id \r\n" +
					"   FROM "+USERDATABASE+".srv_event_types et	\r\n" +
					"  WHERE (et.id = \'LIBERACAO_DE_EXPORTACAO\' OR et.id = \'INCLUSAO_DOC_DESPACHO\') \r\n" +
					"    AND gkey != :GKEY")
			.toString();
	
	public static final String SELECT_EVENTOSLIBERACAO_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT distinct(et.gkey) 							\r\n" +
					"   FROM "+USERDATABASE+".srv_event e, 				\r\n" +
					"      	 "+USERDATABASE+".srv_event_types et 		\r\n" +
					"  WHERE et.gkey = e.event_type_gkey 				\r\n" +
					"    AND E.applied_to_class = \'UNIT\' 				\r\n" +
					"    AND (et.id = \'LIBERACAO_DE_EXPORTACAO\' OR et.id = \'INCLUSAO_DOC_DESPACHO\') 	\r\n" +
					"    AND applied_to_gkey = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT distinct(et.gkey) 							\r\n" +
					"   FROM "+USERDATABASE+".srv_event e, 				\r\n" +
					"      	 "+USERDATABASE+".srv_event_types et 		\r\n" +
					"  WHERE et.gkey = e.event_type_gkey 				\r\n" +
					"    AND E.applied_to_class = \'UNIT\' 				\r\n" +
					"    AND et.id = \'LIBERACAO_DE_EXPORTACAO\' 		\r\n" +
					"    AND applied_to_gkey = :UNITGKEY ")
			.toString();
	
	public static final String SELECT_NOTES_EVENTO_INCLUSAO_DOC_DESPACHO_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT E.NOTE FROM "+USERDATABASE+".SRV_EVENT E, "+USERDATABASE+".SRV_EVENT_TYPES ET 	\r\n" +
					"  WHERE ET.GKEY = E.EVENT_TYPE_GKEY AND E.APPLIED_TO_CLASS = \'UNIT\' 	\r\n" + 	
					"  AND ET.ID = \'INCLUSAO_DOC_DESPACHO\' AND APPLIED_TO_GKEY = :UNITGKEY ").toString();
	
	public static final String SELECT_UNIT_CATEGORY_BY_GKEY = new StringBuilder()
			.append(" SELECT CATEGORY FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY ").toString();
	
	public static final String SELECT_UNIT_POL_OPL_GKEY = new StringBuilder()
			.append(" SELECT OPL_GKEY, POL_GKEY FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY ").toString();
	
	public static final String SELECT_COUNTRY_BY_ROUTING_POINT_GKEY = new StringBuilder()
			.append(" SELECT CNTRY_CODE FROM "+USERDATABASE+".REF_UNLOC_CODE WHERE GKEY = \r\n" +
		            " (SELECT UNLOC_GKEY FROM "+USERDATABASE+".REF_ROUTING_POINT WHERE GKEY = :ROUTING_POINT_GKEY) ").toString();
	
	public static final String SELECT_UNIT_POD_GKEY = new StringBuilder()
			.append(" SELECT POD1_GKEY, POD2_GKEY FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY ").toString();
	
	public static final String SELECT_INBOUND_CARRIER_MODE_BY_UNIT_GKEY = new StringBuilder()
			.append(" select CARRIER_MODE FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S where GKEY in 		\r\n" +
					" (SELECT ACTUAL_IB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT ufv WHERE GKEY = 	\r\n" + 
					" (SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY)) ").toString();
	
	public static final String SELECT_OUTBOUND_CARRIER_MODE_BY_UNIT_GKEY = new StringBuilder()
			.append(" select CARRIER_MODE FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S where GKEY in 		\r\n" +
					" (SELECT ACTUAL_OB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT ufv WHERE GKEY = 	\r\n" + 
					" (SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY)) ").toString();
	
	public static final String SELECT_DADOS_INBOUND_LINE_OPERATOR_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT S.SMS_NUMBER AS TIPO, S.WEBSITE_URL AS CNPJ, S.NAME AS NOME 		\r\n" +
					" FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED S WHERE GKEY IN 							\r\n" + 
					" (SELECT OPERATOR_GKEY FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S WHERE GKEY IN 	\r\n" + 
					" (SELECT ACTUAL_IB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV WHERE GKEY =	\r\n" + 
					" (SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY))) ").toString();

	public static final String SELECT_DADOS_OUTBOUND_LINE_OPERATOR_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT S.SMS_NUMBER AS TIPO, S.WEBSITE_URL AS CNPJ, S.ID AS NOME, S.GKEY AS ID_EXTERNO 	\r\n" +
					" FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED S WHERE GKEY IN 									\r\n" + 
					" (SELECT OPERATOR_GKEY FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S WHERE GKEY IN 			\r\n" + 
					" (SELECT ACTUAL_OB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV WHERE GKEY =			\r\n" + 
					" (SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY))) ").toString();
	
	public static final String SELECT_NOME_NAVIO_OUTBOUND_CARRIER_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT V.NAME, C.ID																			\r\n" + 
					"	FROM "+USERDATABASE+".VSL_VESSEL_VISIT_DETAILS D											\r\n" + 
					"	   , "+USERDATABASE+".VSL_VESSELS V															\r\n" + 
					"	   , "+USERDATABASE+".ARGO_VISIT_DETAILS A													\r\n" + 
					"	   , "+USERDATABASE+".ARGO_CARRIER_VISIT C													\r\n" + 
					"	WHERE D.VESSEL_GKEY = V.GKEY (+)															\r\n" + 
					"	AND D.VVD_GKEY = A.GKEY																		\r\n" + 
					"	AND A.GKEY = C.CVCVD_GKEY																	\r\n" + 
					"	AND D.VVD_GKEY = (SELECT GKEY FROM "+USERDATABASE+".ARGO_VISIT_DETAILS WHERE GKEY =  (		\r\n" + 		
					"	SELECT CVCVD_GKEY FROM "+USERDATABASE+".ARGO_CARRIER_VISIT S WHERE GKEY IN (				\r\n" + 		
					"	SELECT ACTUAL_OB_CV FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV WHERE GKEY =				\r\n" + 		
					"	(SELECT ACTIVE_UFV FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY))))").toString();
	
	public static final String SELECT_CUSTOM_RECINTO_BY_CNPJ_TRANSPORTADORA = new StringBuilder()
			.append(" SELECT * FROM "+USERDATABASE+".CUSTOM_RECINTO WHERE CUSTOMRECINT_ISNTE = 1 AND CUSTOMRECINT_CNPJ = :CNPJ ").toString();

	public static final String SELECT_BOOKING_BY_NBR = new StringBuilder()
			.append(" SELECT * FROM "+USERDATABASE+".INV_EQ_BASE_ORDER WHERE NBR = :NBR ").toString();
	
	public static final String SELECT_BOOKING_BY_NBR_LINE_OPERATOR = new StringBuilder()
			.append(" SELECT * FROM "+USERDATABASE+".INV_EQ_BASE_ORDER WHERE NBR = :NBR AND LINE_GKEY = :LINEGKEY").toString();
	
	public static final String SELECT_LINE_OPERATOR_BY_GKEY = new StringBuilder()
			.append(" SELECT * FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE GKEY = :GKEY ").toString();
	
	public static final String SELECT_DEADLINE_BY_BOOKING_NBR = new StringBuilder()
			.append(" SELECT CARGO_CUTOFF FROM "+USERDATABASE+".VSL_VESSEL_VISIT_DETAILS WHERE VVD_GKEY = (		\r\n" +
					" SELECT CVCVD_GKEY FROM "+USERDATABASE+".ARGO_CARRIER_VISIT WHERE GKEY = (					\r\n" +
					" SELECT VESSEL_VISIT_GKEY FROM "+USERDATABASE+".INV_EQ_BASE_ORDER WHERE NBR = :NBR AND VESSEL_VISIT_GKEY IS NOT NULL)) ").toString();
	
	public static final String SELECT_DEADLINE_BY_BOOKING_NBR_AND_LINE_OPERATOR = new StringBuilder()
			.append(" SELECT CARGO_CUTOFF FROM "+USERDATABASE+".VSL_VESSEL_VISIT_DETAILS WHERE VVD_GKEY = (		\r\n" +
					" SELECT CVCVD_GKEY FROM "+USERDATABASE+".ARGO_CARRIER_VISIT WHERE GKEY = (					\r\n" +
					" SELECT VESSEL_VISIT_GKEY FROM "+USERDATABASE+".INV_EQ_BASE_ORDER WHERE NBR = :NBR AND LINE_GKEY = :LINEGKEY)) ").toString();
	
	public static final String SELECT_AGENT_GKEY_BY_AGENT_DOC = new StringBuilder()
			.append(" SELECT GKEY FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE ROLE = 'AGENT' AND ( WEBSITE_URL = :DESPACHANTE OR ID = :DESPACHANTE ) ").toString();
	
	public static final String SELECT_AGENT_DOC_AND_NAME_BY_AGENT_ID = new StringBuilder()
			.append(" SELECT WEBSITE_URL, NAME FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE ROLE = 'AGENT' AND ( WEBSITE_URL = :DESPACHANTE OR ID = :DESPACHANTE ) ").toString();
	
	public static final String SELECT_AGENT_REPRESENTATIONS_BY_AGENT_GKEY = new StringBuilder()
			.append(" SELECT SBU.NAME, SBU.ID, SBU.WEBSITE_URL, RAR.START_DATE, RAR.END_DATE		\r\n" +
					" FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED SBU, "+USERDATABASE+".REF_AGENT_REPRESENTATION RAR 	\r\n" +
					" WHERE SBU.GKEY = RAR.BZU_GKEY 												\r\n" +
					" AND RAR.AGENT_GKEY IN (:AGENT_GKEYS) ").toString(); 
	
	public static final String SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID = new StringBuilder()
			.append(" SELECT GKEY FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE ROLE = 'SHIPPER' AND ID = :ID ").toString();
	
	public static final String SELECT_SCOPED_BIZ_UNIT_NAME_AND_DOC_BY_SHIPPER_ID = new StringBuilder()
			.append(" SELECT WEBSITE_URL, NAME FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE ROLE = 'SHIPPER' AND ID = :ID ").toString();
	
	public static final String SELECT_CUSTOM_RECINTO_NTE_BY_CNPJ_EXPORTADOR = new StringBuilder()
			.append(" SELECT * FROM "+USERDATABASE+".CUSTOM_RECINTO WHERE CUSTOMRECINT_ISNTE IS NOT NULL AND CUSTOMRECINT_CNPJ = :CNPJ ").toString();
	
	public static final String SELECT_UNIT_FACILITY_VISIT_STATE_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT Max(UFV.GKEY), UFV.VISIT_STATE 			\r\n" +	
					"   FROM "+USERDATABASE+".INV_UNIT_FCY_VISIT UFV	\r\n" +
					"      , "+USERDATABASE+".INV_UNIT UNIT		 		\r\n" +			
					"  WHERE UFV.UNIT_GKEY = UNIT.GKEY					\r\n"  +
					"    AND UNIT.GKEY = :GKEY							\r\n" +   
					"    GROUP BY UFV.VISIT_STATE ").toString();
	
	public static final String SELECT_REPRESENTANTE = new StringBuilder()
			.append(" SELECT ID, NAME FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED WHERE LIFE_CYCLE_STATE = 'ACT' AND ROLE = 'AGENT' AND SMS_NUMBER = 'CNPJ' AND NAME IS NOT NULL AND ID IS NOT NULL  ").toString();

	public static final String SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT S.SMS_NUMBER AS TIPO, S.WEBSITE_URL AS CNPJ, S.ID AS NOME, S.GKEY AS ID_EXTERNO FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED S WHERE S.GKEY = ( \r\n" +	
					" SELECT LINE_OP FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY)  ").toString();
	
	public static final String SELECT_ACTUAL_ENTREGADOR_ARMADOR_BY_UNIT_GKEY = new StringBuilder()
			.append(" SELECT S.SMS_NUMBER AS TIPO, S.WEBSITE_URL AS CNPJ, S.NAME AS NOME  FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED S WHERE S.GKEY = ( \r\n" +	
					" SELECT LINE_OP FROM "+USERDATABASE+".INV_UNIT WHERE GKEY = :UNITGKEY)  ").toString();
	
}