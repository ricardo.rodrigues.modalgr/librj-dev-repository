package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QVinculoRepresentante is a Querydsl query type for VinculoRepresentante
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVinculoRepresentante extends EntityPathBase<VinculoRepresentante> {

    private static final long serialVersionUID = -1840063678L;

    public static final QVinculoRepresentante vinculoRepresentante = new QVinculoRepresentante("vinculoRepresentante");

    public final StringPath cnpj = createString("cnpj");

    public final StringPath codigo = createString("codigo");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nomeLineOp = createString("nomeLineOp");

    public final StringPath nomeRepresentante = createString("nomeRepresentante");

    public QVinculoRepresentante(String variable) {
        super(VinculoRepresentante.class, forVariable(variable));
    }

    public QVinculoRepresentante(Path<? extends VinculoRepresentante> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVinculoRepresentante(PathMetadata metadata) {
        super(VinculoRepresentante.class, metadata);
    }

}

