package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQuery(name = "Entregador.findAll", query = "SELECT e FROM Entregador e")
public class Entregador implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENTREGADOR_SEQ")
	@SequenceGenerator(name = "ENTREGADOR_SEQ", sequenceName = "ENTREGADOR_SEQ", allocationSize = 1)
	private long id;

	private String cnpj;
	
	private String nome;
	
	private String cpf;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "CCT_ID")
	private Cct cct;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cct getCct() {
		return cct;
	}

	public void setCct(Cct cct) {
		this.cct = cct;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}