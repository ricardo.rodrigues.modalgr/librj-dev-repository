package br.com.grupolibra.dueltrservice.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.RecepcaoService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;

@Component
public class FilaTransbordoMaritimoListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String FILA_RECEPCAO_TRANSBORDO = "due-ltr-service.recepcao-transbordomaritimo.queue";

	@Autowired
	private CctService cctService;

	/**
	 * Listener da fila de Transbosdo Maritimo Ao receber um evento UNIT_IN_VESSEL será postado na fila a gkey da unit para envio de recepção para siscomex
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_RECEPCAO_TRANSBORDO)
	public void processaTransbordoMaritimo(final String unitGkey) {
		logger.info("###### INICIO PROCESSO POR TRANSBORDO MARITIMO");
		logger.info("processaTransbordoMaritimo - gKey UNIT_RECEIVE : {} ", unitGkey);
		try {
			cctService.createCctTransbordoMaritimo(unitGkey);
		} catch (Exception e) {
			logger.info("ERRO : {}", e.getMessage());
		}
	}
}
