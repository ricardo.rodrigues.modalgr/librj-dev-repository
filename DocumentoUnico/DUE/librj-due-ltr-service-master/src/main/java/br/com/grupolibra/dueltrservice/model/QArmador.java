package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QArmador is a Querydsl query type for Armador
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QArmador extends EntityPathBase<Armador> {

    private static final long serialVersionUID = -1598477514L;

    public static final QArmador armador = new QArmador("armador");

    public final StringPath cnpj = createString("cnpj");

    public final StringPath cpf = createString("cpf");

    public final NumberPath<Integer> estrangeiro = createNumber("estrangeiro", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> idExterno = createNumber("idExterno", java.math.BigDecimal.class);

    public final StringPath nome = createString("nome");

    public final StringPath nomeEstrangeiro = createString("nomeEstrangeiro");

    public QArmador(String variable) {
        super(Armador.class, forVariable(variable));
    }

    public QArmador(Path<? extends Armador> path) {
        super(path.getType(), path.getMetadata());
    }

    public QArmador(PathMetadata metadata) {
        super(Armador.class, metadata);
    }

}

