package br.com.grupolibra.dueltrservice.messages;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;

@Component
public class FilaUnitRollListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String FILA_UNIT_ROLL = "due-ltr-service.unit-roll.queue";

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private CctService cctService;

	/**
	 * Listener da fila de Danfe Ao receber um evento UNIT_RECEIVE será postado na fila a gkey da unit para envio de recepção para siscomex
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_UNIT_ROLL)
	public void processaValidacaoUnitRoll(final String unitGkey) {
		logger.info("###### INICIO VALIDACAO UNIT_ROLL");
		logger.info("processaValidacaoUnitRoll - gKey UNIT_ROLL : {} ", unitGkey);
		try {
			
			List<Cct> cctAux = cctRepository.findByIdExterno(new BigDecimal(unitGkey));
			if(cctAux != null && !cctAux.isEmpty()) {
				Cct cct = cctAux.get(0);
				n4Repository.setDadosViagem(unitGkey, cct);
				n4Repository.setDadosRecebedorByOutBoundCarrier(unitGkey, cct);
				n4Repository.setBooking(unitGkey, cct);
				
				logger.info("processaValidacaoUnitRoll - SALVANDO UNIT_ROLL ");
				
//				cctRepository.save(cct);
				cctService.save(cct);
			}
			
		} catch (Exception e) {
			logger.info("ERRO : {}", e.getMessage());
		}
	}

	
}
