package br.com.grupolibra.dueltrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaConteineresResponseDTO {

	private String nrConteiner;

	private String nrDUE;

	private String nrRUC;

	private String mensagem;

}
