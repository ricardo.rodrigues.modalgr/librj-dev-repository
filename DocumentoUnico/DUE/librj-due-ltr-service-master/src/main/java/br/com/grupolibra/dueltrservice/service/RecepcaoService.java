package br.com.grupolibra.dueltrservice.service;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;

public interface RecepcaoService {
	
	public RecepcoesNFE getXMLRecepcaoNFE(Cct cct);
	
	public RecepcoesConteineres getXMLRecepcaoConteinerTransbordoMaritimo(Cct cct);
	
	public RecepcoesConteineres getXMLRecepcaoDUE(Cct cct);
	
}
