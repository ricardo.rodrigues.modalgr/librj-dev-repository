package br.com.grupolibra.dueltrservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.EntregaConteinerService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class EnviaEntregaConteinerTransbordoMaritimoTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private EntregaConteinerService entregaConteinerService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void enviaEntregaConteinerTrasbordoMaritimo() {
		logger.info("###### INICIO TASK ENVIO ENTREGA POR TRANSBORDO MARITIMO");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
		logger.info("enviaEntregaConteinerTrasbordoMaritimo");
		List<Cct> cctListEntregaConteinerAux = cctService.getCctsEnviaEntregaConteiner(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString());
		if (cctListEntregaConteinerAux != null && !cctListEntregaConteinerAux.isEmpty()) {
			
			logger.info("enviaEntregaConteiner - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListEntregaConteinerAux.size());
			List<Cct> cctListEntregaConteiner = n4Repository.validaUnitFacilityVisitStateActive(cctListEntregaConteinerAux, "ENTREGA CONTEINER TRANSBORDO MARITIMO");	
			
			logger.info("enviaEntregaConteiner - qtd registros aguardando entrega conteiner : {} ", cctListEntregaConteiner.size());
			n4Repository.validaVesselATAInserido(cctListEntregaConteiner);
			if (!cctListEntregaConteiner.isEmpty()) {
				logger.info("enviaEntregaConteiner - qtd registro com ATA inserido {} ", cctListEntregaConteiner.size());
				for (Cct cct : cctListEntregaConteiner) {
					EntregasConteineres xml = entregaConteinerService.getXMLEntregaConteinerTransbordoMaritimo(cct);
					siscomexService.sendEntregaConteiner(xml, cct);
				}
			} else {
				logger.info("enviaEntregaConteinerTrasbordoMaritimo - sem registro ATA");
			}
		} else {
			logger.info("enviaEntregaConteinerTrasbordoMaritimo - sem registro para entrega conteiner");
		}
	}

}
