package br.com.grupolibra.dueltrservice.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueltrservice.dto.ListaMonitoramentoDTO;
import br.com.grupolibra.dueltrservice.dto.MonitoramentoDTO;
import br.com.grupolibra.dueltrservice.dto.MonitoramentoRequestDTO;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.Danfe;
import br.com.grupolibra.dueltrservice.model.Exportador;
import br.com.grupolibra.dueltrservice.model.Monitoramento;
import br.com.grupolibra.dueltrservice.model.QCct;
import br.com.grupolibra.dueltrservice.model.QMonitoramento;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.MonitoramentoRepository;
import br.com.grupolibra.dueltrservice.service.CctService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestMonitoramentoDueController {

	
	private static final String BD_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private MonitoramentoRepository monitoramentoRepository;
	
	@Autowired
	private CctService cctService;
		
		
	/**
	 * @param tipo
	 * 1 - ATIVOS
     * 2 - ERRORS
     * 3 - ENTREGUES / DEVOLVIDOS
	 * @return
	 * @throws ParseException
	 */	
	@CrossOrigin
	@RequestMapping(value = "api/monitoramento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE) 
	@ResponseBody
	public MonitoramentoDTO findMonitoramento(@RequestBody(required = true) MonitoramentoRequestDTO dto) throws ParseException {

		logger.info("findAllMonitoramento");				
		
		MonitoramentoDTO retorno = new MonitoramentoDTO();
		retorno.setCurrentPage(Integer.parseInt(dto.getPageIndex())); 		
		BooleanBuilder builder = new BooleanBuilder();
		long numRegistros = 0L;
		
		if("3".equals(dto.getTipo())) { //TODO GET
			List<ListaMonitoramentoDTO> listaFinalizado = new ArrayList<>();
			QCct qcct = QCct.cct;
			builder.and(qcct.finalizado.eq(1));
			logger.info("findMonitoramento - QUERY FINALIZADOS : {} ", builder.toString());

			getBuilderFilterText(builder, qcct, dto);
			
			numRegistros = cctRepository.count(builder.getValue());
			Page<Cct> cctList = cctRepository.findAll(builder.getValue(), PageRequest.of(Integer.parseInt(dto.getPageIndex()) -1, Integer.parseInt(dto.getPageSize()))); 
			
			for (Cct cct : cctList) {
				listaFinalizado.add(fillDTOMonitoramento(cct));
			}
			retorno.setListaFinalizado(listaFinalizado);
			
		} else {
			QMonitoramento qMonitoramento = QMonitoramento.monitoramento;
			builder.and(qMonitoramento.ok.eq("1".equals(dto.getTipo()))); 

			getBuilderFilterText(builder, qMonitoramento, dto); 
			
			numRegistros = monitoramentoRepository.count(builder.getValue());
			Page<Monitoramento> listaMonitoramento = monitoramentoRepository.findAll(builder.getValue(), PageRequest.of(Integer.parseInt(dto.getPageIndex()) -1, Integer.parseInt(dto.getPageSize()), Sort.Direction.DESC, "dataProcessamento") ); //TODO POST
			
//			for (Monitoramento monitoramento : listaMonitoramento.getContent()) {
//				logger.info(" DATA PROCESSAMENTO -> " + monitoramento.getDataProcessamento());
//			}
			
			retorno.setListaMonitoramento(listaMonitoramento.getContent());
			
		}
		
		retorno.setQtdRegistros(numRegistros);
		
		return retorno;		
	}
	
	private void getBuilderFilterText(BooleanBuilder builder, QCct qcct, MonitoramentoRequestDTO dto) {
		if(dto.getNavio() != null && !"".equals(dto.getNavio())) {
			builder.and(qcct.navio.containsIgnoreCase(dto.getNavio()));
		}
		if(dto.getVisit() != null && !"".equals(dto.getVisit())) {
			builder.and(qcct.viagem.containsIgnoreCase(dto.getVisit()));
		}
		if(dto.getLineOperator() != null && !"".equals(dto.getLineOperator())) {
			builder.and(qcct.armador.nome.containsIgnoreCase(dto.getLineOperator()));
		}
		if(dto.getBooking() != null && !"".equals(dto.getBooking())) {
			builder.and(qcct.booking.containsIgnoreCase(dto.getBooking()));
		}
		if(dto.getConteiner() != null && !"".equals(dto.getConteiner())) {
			builder.and(qcct.numeroCont.containsIgnoreCase(dto.getConteiner()));
		}
		if(dto.getDanfe() != null && !"".equals(dto.getDanfe())) {
			builder.and(qcct.danfes.any().chave.containsIgnoreCase(dto.getDanfe()));
		}
		if(dto.getDue() != null && !"".equals(dto.getDue())) {
			builder.and(qcct.nrodue.containsIgnoreCase(dto.getDue()));
		}
		
		if(dto.getDataEntradaDe() != null && !"".equals(dto.getDataEntradaDe()) && dto.getDataEntradaAte() != null && !"".equals(dto.getDataEntradaAte()) ) {
			
			try {
				logger.info("DATA ENTRADA DE : {}", dto.getDataEntradaDe());
				logger.info("DATA ENTRADA ATE : {}", dto.getDataEntradaAte());
				
				logger.info("DATA ENTRADA DE PARSED : {}", new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaDe()));
				logger.info("DATA ENTRADA ATE PARSED : {}", new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaAte()));
			
				builder.and(qcct.dtEntr.between(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaDe()), new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaAte())));
			} catch(Exception e) {
				logger.info("getBuilderFilterText - DATA ENTRADA PARSE ERROR");
			}
		}
		
		if(dto.getDataProcDe() != null && !"".equals(dto.getDataProcDe()) && dto.getDataProcAte() != null && !"".equals(dto.getDataProcAte()) ) {
			try {
				builder.and(qcct.dtProc.between(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataProcDe()), new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataProcAte())));
			} catch(Exception e) {
				logger.info("getBuilderFilterText - DATA PROCESSAMENTO PARSE ERROR");
			}	
		}
		
		logger.info("getBuilderFilterText - QUERY FINALIZADOS : {} ", builder.toString());
	}
	

	private void getBuilderFilterText(BooleanBuilder builder, QMonitoramento qMonitoramento, MonitoramentoRequestDTO dto) {
		if(dto.getNavio() != null && !"".equals(dto.getNavio())) {
			builder.and(qMonitoramento.navio.containsIgnoreCase(dto.getNavio()));
		}
		if(dto.getVisit() != null && !"".equals(dto.getVisit())) {
			builder.and(qMonitoramento.visit.containsIgnoreCase(dto.getVisit()));
		}
		if(dto.getLineOperator() != null && !"".equals(dto.getLineOperator())) {
			builder.and(qMonitoramento.lineOp.containsIgnoreCase(dto.getLineOperator()));
		}
		if(dto.getBooking() != null && !"".equals(dto.getBooking())) {
			builder.and(qMonitoramento.booking.containsIgnoreCase(dto.getBooking()));
		}
		if(dto.getConteiner() != null && !"".equals(dto.getConteiner())) {
			builder.and(qMonitoramento.conteiner.containsIgnoreCase(dto.getConteiner()));
		}
		if(dto.getDanfe() != null && !"".equals(dto.getDanfe())) {
			builder.and(qMonitoramento.danfesString.containsIgnoreCase(dto.getDanfe()));
		}
		if(dto.getDue() != null && !"".equals(dto.getDue())) {
			builder.and(qMonitoramento.duesString.containsIgnoreCase(dto.getDue()));
		}

		if(dto.getDataEntradaDe() != null && !"".equals(dto.getDataEntradaDe()) && dto.getDataEntradaAte() != null && !"".equals(dto.getDataEntradaAte()) ) {
			
			try {
				logger.info("DATA ENTRADA DE : {}", dto.getDataEntradaDe());
				logger.info("DATA ENTRADA ATE : {}", dto.getDataEntradaAte());
				
				logger.info("DATA ENTRADA DE PARSED : {}", new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaDe()));
				logger.info("DATA ENTRADA ATE PARSED : {}", new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaAte()));
			
				builder.and(qMonitoramento.dataEntrada.between(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaDe()), new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataEntradaAte())));
			} catch(Exception e) {
				logger.info("getBuilderFilterText - DATA ENTRADA PARSE ERROR");
			}
		}
		
		if(dto.getDataProcDe() != null && !"".equals(dto.getDataProcDe()) && dto.getDataProcAte() != null && !"".equals(dto.getDataProcAte()) ) {
			try {
				builder.and(qMonitoramento.dataProcessamento.between(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataProcDe()), new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataProcAte())));
			} catch(Exception e) {
				logger.info("getBuilderFilterText - DATA PROCESSAMENTO PARSE ERROR");
			}	
		}
		
		logger.info("getBuilderFilterText - QUERY ATIVOS/ERRO : {} ", builder.toString());
	}
	
	private void getQtdPaginas(String pageSize, MonitoramentoDTO retorno, long numRegistros) {
		if(numRegistros > 0) {
			float resultFloat = (float)numRegistros / Integer.parseInt(pageSize);
			int result = 0;
			if(resultFloat > (int) resultFloat) {
				result = (int) resultFloat + 1;  
			} else {
				result = (int) resultFloat;
			}
			retorno.setQtdPaginas(result);
		} else {
			retorno.setQtdPaginas(0L);
		}
	}
	
	private ListaMonitoramentoDTO fillDTOMonitoramento(Cct cct) throws ParseException {
		ListaMonitoramentoDTO result = new ListaMonitoramentoDTO();
		result.setId(cct.getId());
		result.setNavio(cct.getNavio());
		result.setVisit(cct.getViagem());
		result.setLineOp((cct.getArmador() != null) ?  (cct.getArmador().getNomeEstrangeiro() != null ? cct.getArmador().getNomeEstrangeiro() : cct.getArmador().getNome()) : "");
		result.setBooking(cct.getBooking());
		result.setConteiner(cct.getNumeroCont());
		List<String> danfes = new ArrayList<>();
		for(Danfe danfe : cct.getDanfes()) {
			danfes.add(danfe.getChave());
		}
		result.setDanfes(danfes);
		result.setDues(cct.getNrodue() != null ? Arrays.asList(cct.getNrodue().split(",")) : new ArrayList<>());
		result.setStatus(cct.getStatusProc());
		
		if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
			String situacao = "";
			for (Exportador exportador : cct.getExportadores()) {
				if(exportador.getSituacaoDUE() != null && exportador.getSituacaoDUE() > 0) {
					situacao += exportador.getSituacaoDUE() + "-";
				}
			}
			result.setSituacao(!"".equals(situacao) ? situacao.substring(0, situacao.length()-1) : "Não processado");
		} else {
			result.setSituacao("Não processado");
		}
		
		
		result.setBloqueado(cct.getBloqueadoSiscomex() != null && cct.getBloqueadoSiscomex() == 1 ? true : false);
		result.setMensagem(cct.getMensagem());
		result.setErro(cct.getErro());
		
		result.setDataEntrada(cct.getDtentr() != null ? cct.getDtentr() : null);
		result.setDataProcessamento(cct.getDtproc() != null ? cct.getDtproc() : null);
		
		return result;
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/monitoramento/finalizar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String finalizar(@RequestParam("cctId") long cctId) throws ParseException {
		logger.info("finalizar");
		Optional<Cct> cctOpt = cctRepository.findById(cctId);
		if(cctOpt.isPresent()) {
			Cct cct = cctOpt.get();
			cct.setFinalizado(1);
			//cctRepository.save(cct);
			cctService.save(cct);
			return "OK";
		} else {
			return "ERRO";
		}
	}
	
}