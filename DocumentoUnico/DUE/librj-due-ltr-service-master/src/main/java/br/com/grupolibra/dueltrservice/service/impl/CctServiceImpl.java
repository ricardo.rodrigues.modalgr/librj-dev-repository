package br.com.grupolibra.dueltrservice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.Danfe;
import br.com.grupolibra.dueltrservice.model.Exportador;
import br.com.grupolibra.dueltrservice.model.Monitoramento;
import br.com.grupolibra.dueltrservice.model.QCct;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.DanfeRepository;
import br.com.grupolibra.dueltrservice.repository.MonitoramentoRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Service
public class CctServiceImpl implements CctService {

	private static final String BD_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	private static final String VALIDA_ENVIO_RECEPCAO_NFE = "validaEnvioRecepcao - {} ";
	
	private static final String VALIDA_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO = "validaEnvioRecepcaoTransbordoMaritimo - {} ";
	
	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private DanfeRepository danfeRepository;

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private MonitoramentoRepository monitoramentoRepository;

	@Override
	public Cct saveCctNFE(Cct cct) {
//		logger.info("saveCct");
//		cct = cctRepository.save(cct);
		cct = this.save(cct);
		for (Danfe danfe : cct.getDanfes()) {
			danfe.setCct(cct);
			danfeRepository.save(danfe);
		}
		return cct;
	}

	@Override
	public Cct createCctRecepcaoNFE(String unitGkey) {
		logger.info("CctServiceImpl - createCctRecepcaoNFE");
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoNFEByUnitGkey(unitGkey);
		} catch (ParseException e) {
			logger.error("CctServiceImpl - createCctRecepcaoNFE " + e.getMessage());
		}
		return validaEnvioRecepcaoNFE(cct);
	}

	private Cct validaEnvioRecepcaoNFE(Cct cct) {
//		logger.info("validaEnvioRecepcaoNFE");
		if (cct.getDanfes().isEmpty()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
			return null;
		} else if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			return null;
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			return null;
		} else if(cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_VALIDACAO_REGRA_PESO);
			return null;
		} else {
			logger.info("validaEnvioRecepcao - OK");
			return this.saveCctNFE(cct);
		}
	}

	@Override
	public void updateNumeroDUE(List<Cct> resultList) {
//		logger.info("updateNumeroDUE");
		logger.info("updateNumeroDUE - Qtd registros : {} ", resultList.size());
		for (Cct cct : resultList) {
//			logger.info("updateNumeroDUE - Criando Eventos N4");
			logger.info("updateNumeroDUE - CONTEINER : {} | DUE : {} ", cct.getNumeroCont(), cct.getNrodue()); 
			if(cct.getNrodue() != null && !cct.getNrodue().isEmpty() && !"".equals(cct.getNrodue())) {
				
				cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
				cct.setDtproc(new Date(System.currentTimeMillis()));
				
				String notes = "DUE: " + cct.getNrodue();
				boolean isNFE = CategoriaProcessoEnum.NFE.toString().equals(cct.getCategoria());			
				String retorno = n4Repository.createDueEvents(cct.getIdExterno(), notes, isNFE);
//				logger.info("updateNumeroDUE - RETORNO N4 : {} ", retorno);
				if(!"OK".equals(retorno)) {
					cct.setStatus(Cct.STATUS_ERRO);
					cct.setMensagem(Cct.STATUS_ERRO_EVENTO_DUE);
				} else if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())) {
					cct.setStatus(Cct.STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO);
					cct.setStatusProc(Cct.STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO);
				}
				
				if(isNFE) {
					n4Repository.processaExportador(cct);
				}
				
				this.save(cct);
			}
		}
//		logger.info("updateNumeroDUE - Atualizando DUE CCT");
		
//		cctRepository.saveAll(resultList);
	}

	@Override
	public void createCctTransbordoMaritimo(String unitGkey) {
		logger.info("createCctTransbordoMaritimo");
		if(!n4Repository.isConteinerTransbordo(unitGkey)) {
			logger.info("createCctRecepcaoTransbordo - Nao e conteiner transbordo");
			return;
		}
		if(!n4Repository.isVesselInBoundCarrier(unitGkey)) {
			logger.info("createCctRecepcaoTransbordo - Nao e origem Vessel");
			return;
		}
		if(!n4Repository.isVesselOutBoundCarrier(unitGkey)) {
			logger.info("createCctRecepcaoTransbordo - Nao e destino Vessel");
			return;
		}
		if(!n4Repository.isPortoOrigemNacional(unitGkey)) {
			logger.info("createCctRecepcaoTransbordo - Nao e porto de origem nacional");
			return;
		}
		if(!n4Repository.isPortoDestinoInternacional(unitGkey)) {
			logger.info("createCctRecepcaoTransbordo - Nao e porto de destino internacional");
			return;
		}
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoTransbordoMaritimoByUnitGkey(unitGkey);
		} catch (ParseException e) {
			logger.error("CctServiceImpl - createCctTransbordoMaritimo " + e.getMessage());
		}
		validaCctTransbordoMaritimo(cct);
	}

	private void validaCctTransbordoMaritimo(Cct cct) {
//		logger.info("validaCctTransbordoMaritimo");
		if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
//			cctRepository.save(cct);
			this.save(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO, Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
//			cctRepository.save(cct);
			this.save(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO, Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
		} else if(cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO);
//			cctRepository.save(cct);
			this.save(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO, Cct.FALHA_VALIDACAO_REGRA_PESO);
		} else {
//			logger.info("validaEnvioRecepcaoTransbordoMaritimo - OK");
			cct.setStatusProc(Cct.STATUS_AGUARDANDO_DUE_TRANSBORDO_MARITIMO);
			cct.setStatus(Cct.STATUS_OK);
//			cctRepository.save(cct);
			this.save(cct);
		}
	}
	
	@Override
	public Cct createCctRecepcaoDUE(String unitGkey) {
		logger.info("CctServiceImpl - createCctRecepcaoDUE");
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoDUEByUnitGkey(unitGkey);
		} catch (ParseException e) {
			logger.error("CctServiceImpl - createCctRecepcaoDUE " + e.getMessage());
		}
		return validaEnvioRecepcaoDUE(cct);
	}

	private Cct validaEnvioRecepcaoDUE(Cct cct) {
//		logger.info("validaEnvioRecepcaoDUE");
		if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			return null;
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			return null;
		} else if(cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO);
			this.saveCctNFE(cct);
//			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_VALIDACAO_REGRA_PESO);
			return null;
		} else {
//			logger.info("validaEnvioRecepcaoDUE - OK");
//			return cctRepository.save(cct);
			return this.save(cct);
		}
	}
	
	@Override
	public List<Cct> getEnvioRecepcaoNFEPendente() {
//		logger.info("getEnvioRecepcaoNFEPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_SEM_LACRE));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_SEM_UNIT_SEAL));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_REGRA_PESO));
//		builder.or(cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()));
//		builder.or(cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()));
//		builder.or(cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()));
//		builder.or(cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()));
//		builder.or(cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()));
//		builder.or(cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.danfes.isNotEmpty());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getEnvioRecepcaoPendente - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getEnvioRecepcaoDUEPendente() {
//		logger.info("getEnvioRecepcaoDUEPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));

		builder.and(cct.categoria.eq(CategoriaProcessoEnum.DUE.toString()));
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getEnvioRecepcaoPendente - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getEnvioRecepcaoTransbordoMaritimo() {
//		logger.info("getEnvioRecepcaoTransbordoMaritimo");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.statusProc.eq(Cct.STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO), cct.statusProc.eq(Cct.STATUS_ERRO_RECEPCAO));
		builder.andAnyOf(cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()), cct.mensagem.isNull() );
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString()));
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getEnvioRecepcaoTransbordoMaritimo - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctTransbordoMaritimoPendente() {
//		logger.info("getCctTransbordoMaritimoPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_SEM_LACRE));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_SEM_UNIT_SEAL));
//		builder.or(cct.erro.eq(Cct.STATUS_ERRO_REGRA_PESO));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString()));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctTransbordoMaritimoPendente - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctsEnviaEntregaConteiner(String categoria) {
//		logger.info("getCctsEnviaEntregaConteiner - CATEGORIA: {} ", categoria);
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		
		builder.andAnyOf(cct.statusProc.eq(Cct.STATUS_RECEPCIONADO_OK), cct.statusProc.eq(Cct.STATUS_ERRO_ENTREGA));
		builder.andAnyOf(cct.mensagem.eq(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO), cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		
		builder.and(cct.categoria.eq(categoria));
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctsEnviaEntregaConteiner QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
		
	@Override
	public List<Cct> getCctsConsultaDadosResumidos() {
//		logger.info("getCctsConsultaDadosResumidos");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.nrodue.isNotNull());
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctsConsultaDadosResumidos - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctsTransbordoMaritimoConsultaCadastroDUE() {
//		logger.info("getCctsTransbordoMaritimoConsultaCadastroDUE");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.statusProc.eq(Cct.STATUS_AGUARDANDO_DUE_TRANSBORDO_MARITIMO));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString()));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctsTransbordoMaritimoConsultaCadastroDUE - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctsNFEConsultaCadastroDUE() {
//		logger.info("getCctsNFEConsultaCadastroDUE");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.statusProc.eq(Cct.STATUS_RECEPCIONADO_OK));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctsNFEConsultaCadastroDUE - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getAllCCT() {
//		logger.info("getCctsNFEConsultaCadastroDUE");
		return (List<Cct>) cctRepository.findAll();
	}
	
	@Override
	public List<Cct> getCctsExportadorPendente() {
//		logger.info("getCctsExportadorPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.exportadores.isEmpty());
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));
		logger.info("getCctsExportadorPendente - QUERY : {} ", builder.toString());
		return cctRepository.findAll(builder.getValue());
	}
	
	public Cct save(Cct cct) {
		cct = cctRepository.save(cct);
		
		if(cct.getFinalizado() != null && cct.getFinalizado() == 1) {
			logger.info("DELETE MONITORAMENTO");
			monitoramentoRepository.deleteById(cct.getId());
		} else {
			saveMonitoramento(cct);
		}
		
		return cct;
	}
	
	public void saveMonitoramento(Cct cct) {
		logger.info("SAVE MONITORAMENTO");
		Monitoramento monitoramento = new Monitoramento();
		monitoramento.setId(cct.getId());
		monitoramento.setNavio(cct.getNavio());
		monitoramento.setVisit(cct.getViagem());
		monitoramento.setLineOp((cct.getArmador() != null) ?  (cct.getArmador().getNomeEstrangeiro() != null ? cct.getArmador().getNomeEstrangeiro() : cct.getArmador().getNome()) : "");
		monitoramento.setBooking(cct.getBooking());
		monitoramento.setConteiner(cct.getNumeroCont());
		StringBuilder danfes = new StringBuilder();
		for(Danfe danfe : cct.getDanfes()) {
			danfes.append(danfe.getChave() + " - ");
		}
		monitoramento.setDanfesString((danfes != null && danfes.toString() != null && !danfes.toString().isEmpty()) ? danfes.toString().substring(0, danfes.toString().length() - 3) : null);
		monitoramento.setDuesString(cct.getNrodue());
		monitoramento.setStatus(cct.getStatusProc());
		
		if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
			String situacao = "";
			for (Exportador exportador : cct.getExportadores()) {
				if(exportador.getSituacaoDUE() != null && exportador.getSituacaoDUE() > 0) {
					situacao += exportador.getSituacaoDUE() + "-";
				}
			}
			monitoramento.setSituacao(!"".equals(situacao) ? situacao.substring(0, situacao.length()-1) : "Não processado");
		} else {
			monitoramento.setSituacao("Não processado");
		}
		
		monitoramento.setBloqueado(cct.getBloqueadoSiscomex() != null && cct.getBloqueadoSiscomex() == 1 ? true : false);
		monitoramento.setMensagem(cct.getMensagem());
		monitoramento.setErro(cct.getErro());
		try {
			monitoramento.setDataEntrada(cct.getDtentr() != null ? cct.getDtentr() : null);
		} catch(Exception e) {
			monitoramento.setDataEntrada(null);
		}
		try {	
			monitoramento.setDataProcessamento(cct.getDtproc() != null ? cct.getDtproc() : null);
		} catch(Exception e) {
			monitoramento.setDataProcessamento(null);
		}
		
		if(Cct.STATUS_ERRO.equals(cct.getStatus())) {
			monitoramento.setOk(false);
		} else {
			monitoramento.setOk(true);
		}
		
		monitoramentoRepository.save(monitoramento);
	}
}
