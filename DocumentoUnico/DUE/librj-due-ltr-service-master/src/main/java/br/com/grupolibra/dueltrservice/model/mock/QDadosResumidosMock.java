package br.com.grupolibra.dueltrservice.model.mock;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDadosResumidosMock is a Querydsl query type for DadosResumidosMock
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDadosResumidosMock extends EntityPathBase<DadosResumidosMock> {

    private static final long serialVersionUID = 1071133806L;

    public static final QDadosResumidosMock dadosResumidosMock = new QDadosResumidosMock("dadosResumidosMock");

    public final ListPath<ExportadorMock, QExportadorMock> exportadores = this.<ExportadorMock, QExportadorMock>createList("exportadores", ExportadorMock.class, QExportadorMock.class, PathInits.DIRECT2);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath indicadorBloqueio = createString("indicadorBloqueio");

    public final StringPath numeroDUE = createString("numeroDUE");

    public final StringPath situacaoDUE = createString("situacaoDUE");

    public QDadosResumidosMock(String variable) {
        super(DadosResumidosMock.class, forVariable(variable));
    }

    public QDadosResumidosMock(Path<? extends DadosResumidosMock> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDadosResumidosMock(PathMetadata metadata) {
        super(DadosResumidosMock.class, metadata);
    }

}

