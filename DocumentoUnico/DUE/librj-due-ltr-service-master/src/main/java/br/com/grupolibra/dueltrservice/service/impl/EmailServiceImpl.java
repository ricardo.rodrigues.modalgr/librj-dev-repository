package br.com.grupolibra.dueltrservice.service.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.grupolibra.dueltrservice.config.EmailConfig;
import br.com.grupolibra.dueltrservice.service.EmailService;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Service
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	public EmailConfig emailConfig;

	public String sendSimpleMessage(String to, String subject, String mensagem) {
		try {
			if (!to.isEmpty()) {
				Message message = new MimeMessage(emailConfig.getJavaMailSession());
				message.setHeader("Content-Type", "text/html; charset=UTF-8");
				message.setFrom(new InternetAddress(PropertiesUtil.getProp().getProperty("mail.emailfrom"))); //ArquivoConfiguracao.getMailSenderMailFrom()));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
				message.setSubject(subject);
				message.setContent(mensagem, "text/html; charset=UTF-8");
	
				Transport.send(message);
			}
		} catch (MessagingException e) {
			return "ERRO";		
		}
		return "OK";
	}
}
