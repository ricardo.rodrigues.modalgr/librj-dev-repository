package br.com.grupolibra.dueltrservice.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class ConsultaCadastroDUETask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private SiscomexService siscomexService;
	
	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void consultaCadastroDUEPorNumeroConteiner() {
		logger.info("###### INICIO TASK CONSULTA CADASTRO DUE");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
//		logger.info("consultaCadastroDUEPorNumeroConteiner");
		List<Cct> cctListConsultaCadastroDUE = cctService.getCctsNFEConsultaCadastroDUE();
		logger.info("consultaCadastroDUEPorNumeroConteiner - NFE SIZE {} ", cctListConsultaCadastroDUE.size());
		cctListConsultaCadastroDUE.addAll(cctService.getCctsTransbordoMaritimoConsultaCadastroDUE());
		if (cctListConsultaCadastroDUE != null && !cctListConsultaCadastroDUE.isEmpty()) {
			
//			logger.info("consultaCadastroDUEPorNumeroConteiner - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListConsultaCadastroDUEAux.size());
//			List<Cct> cctListConsultaCadastroDUE = n4Repository.validaUnitFacilityVisitStateActive(cctListConsultaCadastroDUEAux);
			
			logger.info("consultaCadastroDUEPorNumeroConteiner - qtd registros para consulta : {} ", cctListConsultaCadastroDUE.size());
			List<Cct> resultList = null;
			if (cctListConsultaCadastroDUE.size() > 50) {
				resultList = consultaCadastroDUEPorLote(cctListConsultaCadastroDUE);
			} else {
				// TODO Retirar MOCK
//				resultList = siscomexService.consultaDUEPorConteinerMOCK(cctListConsultaCadastroDUE);
				resultList = siscomexService.consultaDUEPorConteiner(cctListConsultaCadastroDUE);
			}
			if (resultList != null && !resultList.isEmpty()) {
				cctService.updateNumeroDUE(resultList);
			} else {
				logger.info("consultaCadastroDUEPorNumeroConteiner - Sem registros com cadastro de DUE");
			}
		} else {
			logger.info("consultaCadastroDUEPorNumeroConteiner - sem registros para consulta");
		}
	}

	private List<Cct> consultaCadastroDUEPorLote(List<Cct> cctListConsultaCadastroDUE) {
//		logger.info("consultaCadastroDUEPorLote");
		List<Cct> resultList = new ArrayList<>();
		List<List<Cct>> lotes = Lists.partition(cctListConsultaCadastroDUE, 50);
		logger.info("consultaCadastroDUEPorLote - QTD LISTAS PARTICIONADAS -> {} ", lotes.size());
//		int count = 1;
		Iterator<List<Cct>> loteIterator = lotes.iterator();
		while(loteIterator.hasNext()) {
//		for (List<Cct> lote : lotes) {
			List<Cct> lote = loteIterator.next();
			List<Cct> loteAux = new ArrayList<>();
//			logger.info("consultaCadastroDUEPorLote - DENTRO DO FOR - LACO {} ", count);
//			logger.info("consultaCadastroDUEPorLote - QTD DESTE LOTE - {} ", lote.size());
//			logger.info("consultaCadastroDUEPorLote - REGISTROS DESTE LACO");
			for (Cct cct : lote) {
				loteAux.add(cct);
			}
//			resultList.addAll(siscomexService.consultaDUEPorConteinerMOCK(loteAux));
			resultList.addAll(siscomexService.consultaDUEPorConteiner(loteAux));
//			logger.info("consultaCadastroDUEPorLote - FIM DO LACO {} ", count++);
		}
		
		return resultList;
	}
}
