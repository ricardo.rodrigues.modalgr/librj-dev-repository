package br.com.grupolibra.dueltrservice.service;

import java.util.List;

import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;

public interface SiscomexService {
	
	public void sendRecepcaoNFE(RecepcoesNFE recepcoesNFE, Cct cct);
	
	public void sendRecepcaoConteiner(RecepcoesConteineres recepcoesConteineres, Cct cct);

	public List<Cct> consultaDUEPorConteiner(List<Cct> cctListConsultaCadastroDUE);
	
	public List<Cct> consultaDUEPorConteinerMOCK(List<Cct> cctListConsultaCadastroDUE);
	
	public void sendEntregaConteiner(EntregasConteineres entregasConteineres, Cct cct);

	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos);
		
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidosMOCK(List<Cct> cctListConsultaDadosResumidos);
	
}
