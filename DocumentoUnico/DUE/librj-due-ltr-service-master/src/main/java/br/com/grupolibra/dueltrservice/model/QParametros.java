package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QParametros is a Querydsl query type for Parametros
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QParametros extends EntityPathBase<Parametros> {

    private static final long serialVersionUID = 440727622L;

    public static final QParametros parametros = new QParametros("parametros");

    public final StringPath chave = createString("chave");

    public final StringPath data = createString("data");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath valor = createString("valor");

    public QParametros(String variable) {
        super(Parametros.class, forVariable(variable));
    }

    public QParametros(Path<? extends Parametros> path) {
        super(path.getType(), path.getMetadata());
    }

    public QParametros(PathMetadata metadata) {
        super(Parametros.class, metadata);
    }

}

