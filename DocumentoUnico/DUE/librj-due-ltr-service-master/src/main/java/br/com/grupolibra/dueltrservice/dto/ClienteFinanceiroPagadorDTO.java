package br.com.grupolibra.dueltrservice.dto;

import java.util.List;

public class ClienteFinanceiroPagadorDTO {

	private String booking;

	private String despachante;

	private List<ClienteFinanceiroPagadorDTO.Tabela> listaTabela;

	private ClienteFinanceiroPagadorDTO.LineOperator lineOperator;

	private List<ClienteFinanceiroPagadorDTO.LineOperator> lineOperatorList;

	private String mensagem;

	private boolean erro;

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getDespachante() {
		return despachante;
	}

	public void setDespachante(String despachante) {
		this.despachante = despachante;
	}

	public ClienteFinanceiroPagadorDTO.LineOperator getLineOperator() {
		return lineOperator;
	}

	public void setLineOperator(ClienteFinanceiroPagadorDTO.LineOperator lineOperator) {
		this.lineOperator = lineOperator;
	}

	public List<ClienteFinanceiroPagadorDTO.LineOperator> getLineOperatorList() {
		return lineOperatorList;
	}

	public void setLineOperatorList(List<ClienteFinanceiroPagadorDTO.LineOperator> lineOperatorList) {
		this.lineOperatorList = lineOperatorList;
	}

	public List<ClienteFinanceiroPagadorDTO.Tabela> getListaTabela() {
		return listaTabela;
	}

	public void setListaTabela(List<ClienteFinanceiroPagadorDTO.Tabela> listaTabela) {
		this.listaTabela = listaTabela;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public static class Tabela {

		private long idExportadorCct;

		private String mensagem;

		private String conteiner;

		private String idExterno;

		private String due;

		private List<ClienteFinanceiroPagadorDTO.Exportador> exportadores;

		private ClienteFinanceiroPagadorDTO.Exportador exportadorSel;
		
		private String idExportadorSel;

		private String status;

		private int confirmado;

		private boolean alterado;

		public long getIdExportadorCct() {
			return idExportadorCct;
		}

		public void setIdExportadorCct(long idExportadorCct) {
			this.idExportadorCct = idExportadorCct;
		}

		public String getMensagem() {
			return mensagem;
		}

		public String getConteiner() {
			return conteiner;
		}

		public String getDue() {
			return due;
		}

		public List<ClienteFinanceiroPagadorDTO.Exportador> getExportadores() {
			return exportadores;
		}

		public String getStatus() {
			return status;
		}

		public int getConfirmado() {
			return confirmado;
		}

		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}

		public void setConteiner(String conteiner) {
			this.conteiner = conteiner;
		}

		public void setDue(String due) {
			this.due = due;
		}

		public void setExportadores(List<ClienteFinanceiroPagadorDTO.Exportador> exportadores) {
			this.exportadores = exportadores;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public void setConfirmado(int confirmado) {
			this.confirmado = confirmado;
		}

		public boolean isAlterado() {
			return alterado;
		}

		public void setAlterado(boolean alterado) {
			this.alterado = alterado;
		}

		public String getIdExterno() {
			return idExterno;
		}

		public void setIdExterno(String idExterno) {
			this.idExterno = idExterno;
		}

		public ClienteFinanceiroPagadorDTO.Exportador getExportadorSel() {
			return exportadorSel;
		}

		public void setExportadorSel(ClienteFinanceiroPagadorDTO.Exportador exportadorSel) {
			this.exportadorSel = exportadorSel;
		}

		public String getIdExportadorSel() {
			return idExportadorSel;
		}

		public void setIdExportadorSel(String idExportadorSel) {
			this.idExportadorSel = idExportadorSel;
		}
	}

	public static class LineOperator {

		protected String label;

		protected String gkey;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getGkey() {
			return gkey;
		}

		public void setGkey(String gkey) {
			this.gkey = gkey;
		}

	}

	public static class Exportador {

		protected String id;

		protected String label;

		protected String doc;

		protected boolean siscomex;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getLabel() {
			return label;
		}

		public String getDoc() {
			return doc;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public void setDoc(String doc) {
			this.doc = doc;
		}

		public boolean isSiscomex() {
			return siscomex;
		}

		public void setSiscomex(boolean siscomex) {
			this.siscomex = siscomex;
		}

	}

}
