package br.com.grupolibra.dueltrservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueltrservice.dto.ClienteFinanceiroPagadorDTO;
import br.com.grupolibra.dueltrservice.service.ClienteFinanceiroPagadorService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestClienteFInanceiroPagadorController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ClienteFinanceiroPagadorService clienteFinanceiroPagadorService;

	@CrossOrigin
	@RequestMapping(value = "/api/savecfp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO salvaListaCFP(@RequestBody(required = true) ClienteFinanceiroPagadorDTO dto) {
		logger.info("salvaListaCFP");
		return clienteFinanceiroPagadorService.salvaListaCFP(dto);
	}

	@CrossOrigin
	@RequestMapping(value = "/api/validaparam", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public int validaCheckObrigatorio() {
		logger.info("validaCheckObrigatorio");
		return clienteFinanceiroPagadorService.validaCheckObrigatorio();
	}

	@CrossOrigin
	@RequestMapping(value = "api/cfp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(@RequestParam("bookingNbr") String bookingNbr, @RequestParam("lineOperatorGkey") String lineOperatorGkey, @RequestParam("despachante") String despachante) {
		logger.info("getDadosClienteFinanceiroPagador");
		return clienteFinanceiroPagadorService.getDadosClienteFinanceiroPagador(bookingNbr, lineOperatorGkey, despachante);
	}

}