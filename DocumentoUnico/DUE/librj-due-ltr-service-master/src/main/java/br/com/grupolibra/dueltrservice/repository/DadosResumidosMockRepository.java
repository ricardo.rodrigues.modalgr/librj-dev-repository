package br.com.grupolibra.dueltrservice.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.mock.DadosResumidosMock;

@Repository
public interface DadosResumidosMockRepository extends CrudRepository<DadosResumidosMock, Integer>, QuerydslPredicateExecutor<DadosResumidosMock> {
	
	public DadosResumidosMock findByNumeroDUE(String numeroDUE);
	
}