package br.com.grupolibra.dueltrservice.task;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class ConsultaDadosResumidosTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private SiscomexService siscomexService;
	
	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void consultaDadosResumidos() {
		logger.info("###### INICIO TASK CONSULTA DADOS RESUMIDOS");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
//		logger.info("consultaDadosResumidos");
		List<Cct> cctListConsultaDadosResumidosAux = cctService.getCctsConsultaDadosResumidos();
		if (cctListConsultaDadosResumidosAux != null && !cctListConsultaDadosResumidosAux.isEmpty()) {
			
			logger.info("consultaDadosResumidos - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListConsultaDadosResumidosAux.size());
			List<Cct> cctListConsultaDadosResumidos = n4Repository.validaUnitFacilityVisitStateActive(cctListConsultaDadosResumidosAux, "CONSULTA DADOS RESUMIDOS");
			
			logger.info("consultaDadosResumidos - qtd registros para consulta : {} ", cctListConsultaDadosResumidos.size());
			ConsultaDadosResumidosResponseListDTO resultList = null;
			if (cctListConsultaDadosResumidos.size() > 50) {
				resultList = consultaDadosResumidosPorLote(cctListConsultaDadosResumidos);
			} else {
				// TODO Retirar MOCK
//				resultList = siscomexService.consultaDadosResumidosMOCK(cctListConsultaDadosResumidos);
				resultList = siscomexService.consultaDadosResumidos(cctListConsultaDadosResumidos);
			}
			if (resultList != null && resultList.getListaRetorno() != null && !resultList.getListaRetorno().isEmpty()) {
				logger.info("consultaDadosResumidos - Retorno SISCOMEX : resultList.size() {} ", resultList.getListaRetorno().size());
				n4Repository.processaRetornoConsultaDadosResumidos(cctListConsultaDadosResumidos, resultList.getListaRetorno());
			} else {
				logger.info("consultaDadosResumidos - SEM Retorno SISCOMEX");
			}
		} else {
			logger.info("consultaDadosResumidos - sem registros para consulta");
		}
	}

	private ConsultaDadosResumidosResponseListDTO consultaDadosResumidosPorLote(List<Cct> cctListConsultaDadosResumidos) {
		ConsultaDadosResumidosResponseListDTO resultList = new ConsultaDadosResumidosResponseListDTO();
		resultList.setListaRetorno(new ArrayList<>());
		List<List<Cct>> lotes = Lists.partition(cctListConsultaDadosResumidos, 50);
		logger.info("consultaDadosResumidosPorLote - QTD LISTAS PARTICIONADAS -> {} ", lotes.size());
		for (List<Cct> lote : lotes) {
			// TODO Retirar MOCK
//			ConsultaDadosResumidosResponseListDTO resultListAux =  siscomexService.consultaDadosResumidosMOCK(lote);
			ConsultaDadosResumidosResponseListDTO resultListAux =  siscomexService.consultaDadosResumidos(lote);
			if (resultListAux.getListaRetorno() != null && !resultListAux.getListaRetorno().isEmpty()) {
				resultList.getListaRetorno().addAll(resultListAux.getListaRetorno());
			}
		}
		return resultList;
	}
}
