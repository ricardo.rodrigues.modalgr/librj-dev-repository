package br.com.grupolibra.dueltrservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestAPIController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private CctService cctService;

	@CrossOrigin
	@RequestMapping(value = "api/teste", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String teste() {
		logger.info("teste");
		return "ALIVE";
	}
	
	@CrossOrigin
	@RequestMapping(value = "robot/getconteiner", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getConteiner(@RequestParam("conteiner") String conteiner) {
		logger.info("getConteiner");
		return n4Repository.getStatusConteiner(conteiner);
	}
	
//	@CrossOrigin
//	@RequestMapping(value = "api/cargamonitoramento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String cargaMonitoramento() {
//		logger.info("cargaMonitoramento");
//		try {
//			QCct qcct = QCct.cct;
//			BooleanBuilder builder = new BooleanBuilder();
//			builder.or(qcct.finalizado.isNull());
//			builder.or(qcct.finalizado.eq(0));
//			
//			logger.info("QUERY -> " + builder.toString());
//			
//			Iterable<Cct> cctList = cctRepository.findAll(builder.getValue());
//			
//			cctList.forEach(cct -> cctService.saveMonitoramento(cct));
//		} catch(Exception e) {
//			return "ERRO AO EFETUAR CARGA - " + e.getMessage();
//		}
//		
//		return "CARGA CONCLUIDA COM SUCESSO";
//	}	
}