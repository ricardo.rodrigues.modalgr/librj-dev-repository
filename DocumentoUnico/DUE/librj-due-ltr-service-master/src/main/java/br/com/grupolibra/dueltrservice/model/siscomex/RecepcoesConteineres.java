package br.com.grupolibra.dueltrservice.model.siscomex;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("recepcoesConteineres")
public class RecepcoesConteineres {

	@XStreamAsAttribute
	final String xmlns = "http://www.pucomex.serpro.gov.br/cct";

	protected RecepcaoConteiner recepcaoConteiner;

	public RecepcaoConteiner getRecepcaoConteiner() {
		return recepcaoConteiner;
	}

	public void setRecepcaoConteiner(RecepcaoConteiner recepcaoConteiner) {
		this.recepcaoConteiner = recepcaoConteiner;
	}

}
