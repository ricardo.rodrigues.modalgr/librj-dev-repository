package br.com.grupolibra.dueltrservice.dto;

import java.util.List;

import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VinculoRepresentanteDTO {
	
	private long qtdPaginas;
	
	private long qtdRegistros;
	
	private int currentPage;
	
	private List<VinculoRepresentante> listaVinculo;
	
	private boolean erro;
	
	private String mensagem;
	
}
