package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQuery(name = "Armador.findAll", query = "SELECT c FROM Armador c")
public class Armador implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final Integer BRASILEIRO = 0;
	public static final Integer ESTRANGEIRO = 1;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "armador_seq")
	@SequenceGenerator(name = "armador_seq", sequenceName = "armador_seq", allocationSize = 1)
	private long id;

	private String nome;

	@Column(name = "NOME_ESTRANGEIRO")
	private String nomeEstrangeiro;

	private Integer estrangeiro;

	@Column(name = "ID_EXTERNO")
	private BigDecimal idExterno;

	private String cpf;

	private String cnpj;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeEstrangeiro() {
		return nomeEstrangeiro;
	}

	public void setNomeEstrangeiro(String nomeEstrangeiro) {
		this.nomeEstrangeiro = nomeEstrangeiro;
	}

	public Integer getEstrangeiro() {
		return estrangeiro;
	}

	public void setEstrangeiro(Integer estrangeiro) {
		this.estrangeiro = estrangeiro;
	}

	public BigDecimal getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(BigDecimal idExterno) {
		this.idExterno = idExterno;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
