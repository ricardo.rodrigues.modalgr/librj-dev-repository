package br.com.grupolibra.dueltrservice.model.mock;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QExportadorMock is a Querydsl query type for ExportadorMock
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QExportadorMock extends EntityPathBase<ExportadorMock> {

    private static final long serialVersionUID = -1666410404L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QExportadorMock exportadorMock = new QExportadorMock("exportadorMock");

    public final QDadosResumidosMock dadosResumidosMock;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath numero = createString("numero");

    public final StringPath tipo = createString("tipo");

    public QExportadorMock(String variable) {
        this(ExportadorMock.class, forVariable(variable), INITS);
    }

    public QExportadorMock(Path<? extends ExportadorMock> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QExportadorMock(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QExportadorMock(PathMetadata metadata, PathInits inits) {
        this(ExportadorMock.class, metadata, inits);
    }

    public QExportadorMock(Class<? extends ExportadorMock> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.dadosResumidosMock = inits.isInitialized("dadosResumidosMock") ? new QDadosResumidosMock(forProperty("dadosResumidosMock")) : null;
    }

}

