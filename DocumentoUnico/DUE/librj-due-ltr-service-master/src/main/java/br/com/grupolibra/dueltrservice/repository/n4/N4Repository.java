package br.com.grupolibra.dueltrservice.repository.n4;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.text.MaskFormatter;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.common.base.CharMatcher;
import com.google.common.collect.Lists;

import br.com.grupolibra.dueltrservice.dto.ClienteFinanceiroPagadorDTO.Tabela;
import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueltrservice.dto.VinculoRepresentanteDTO;
import br.com.grupolibra.dueltrservice.model.Armador;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.Danfe;
import br.com.grupolibra.dueltrservice.model.Entregador;
import br.com.grupolibra.dueltrservice.model.Exportador;
import br.com.grupolibra.dueltrservice.model.Parametros;
import br.com.grupolibra.dueltrservice.model.Recebedor;
import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;
import br.com.grupolibra.dueltrservice.repository.ArmadorRepository;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.ExportadorRepository;
import br.com.grupolibra.dueltrservice.repository.ParametrosRepository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;
import br.com.grupolibra.dueltrservice.util.QueryUtil;
import br.com.grupolibra.dueltrservice.util.SituacaoDUEEnum;
import br.com.grupolibra.fmwPortService.exception.N4ServiceInvokeException;
import br.com.grupolibra.fmwPortService.genericInvoke.N4ServiceInvoke;
import br.com.grupolibra.fmwPortService.genericInvoke.ServiceParam;
import br.com.grupolibra.fmwPortService.helper.logger.LoggerN4;

@Component
public class N4Repository {

	private static final String EXPRT = "EXPRT";
	
	private static final String TRSHP = "TRSHP";
	
	private static final String VESSEL = "VESSEL";

	private static final String ERRO = "ERRO : ";

	private static final String NOTES = "notes";

	private static final String EVENT = "event";
	
	private static final String HOLD = "hold";
	
	private static final String PERMISSION = "permission";

	private static final String UNIT_GKEY = "unitGkey";
	
	private static final String BR = "BR";

	private static final String ACTION = "action";

	private static final String DUE_GROOVY_PROXY_NAME = "DueServiceProxy";

	private static final String CREATE_UNIT_EVENT_SERVICE = "createUnitEvent";
	
	private static final String APPLY_HOLD_SERVICE = "applyHoldUnit";
	
	private static final String APPLY_PERMISSION_SERVICE = "applyPermissionUnit";

	private static final String CREATE_UNIT_DUE_EVENTS_SERVICE = "createUnitDUEEvents";
	
	private static final String CREATE_DESBLOQUEIO_EVENTS_SERVICE = "createDesbloqueioEvents";
	
	private static final String CREATE_CFP_EVENT_SERVICE = "createCFPEvent";
	
	private static final String CREATE_CFP_LIBERACAO_DE_EXPORTACAO_EVENT_SERVICE = "createCFPLiberacaoExportacaoEvent";

	private static final String RECEPCAO_NFE_UNIT_EVENT = "UNIT_RECEP_DANFE";
	
	private static final String RECEPCAO_CONTEINER_TRANSBORDO_MARITIMO_UNIT_EVENT = "UNIT_RECEP_TRANSBORDO";
	
	private static final String RECEPCAO_CONTEINER_DUE_UNIT_EVENT = "UNIT_RECEP_DUE";

	private static final String ENTREGA_UNIT_EVENT = "UNIT_ENTREGA";
	
	private static final String ENTREGA_TRANSBORDO_MARITIMO_UNIT_EVENT = "UNIT_ENTREGA_DUE";
	
	private static final String HOLD_PERMISSION_EVENT = "PRT_REQUIRES_EXP_CUSTOMS_CLEARANCE";

	private static final String UNITGKEY = "UNITGKEY";
	
	private static final String ROUTING_POINT_GKEY = "ROUTING_POINT_GKEY";

	private static final String DIVERGENCIA_PESO_ACIMA = "DIVERGENCIA_PESO_ACIMA";
	
	private static final String VALIDACAO_LIBERACAO = "VALIDACAO_LIBERACAO";

	private String divergenciaPesoAcima;

	private static final String DIVERGENCIA_PESO_ABAIXO = "DIVERGENCIA_PESO_ABAIXO";

	private String divergenciaPesoAbaixo;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String LOG_N4 = LoggerN4.ERROR;
	
	private static final String USERDATABASE = PropertiesUtil.getProp().getProperty("n4.datasource.schema");

	private static final String CNPJ = "CNPJ";
	
	private static final String UNIT_RECEIVE = "UNIT_RECEIVE";
	
	private static final String UNIT_IN_VESSEL = "UNIT_IN_VESSEL";

	private static final String RESPONSIBLE_PARTY = "responsiblePartyGkey";
	
	private static final String CNPJ_PATTERN = "##.###.###/####-##";

	private static final String NBR = "NBR";

	private static final String ATUALIZA_BOOKING_SHIPPER = "atualizaBookingShipper";
	
	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";

	@Autowired
	@Qualifier("n4EntityManagerFactory")
	private EntityManager entityManager;

	@Autowired
	@Qualifier("n4ServiceInvoke")
	private N4ServiceInvoke n4ServiceInvoke;

	@Autowired
	private CCTRepository cctRepository;

	@Autowired
	private ArmadorRepository armadorRepository;
	
	@Autowired
	private ExportadorRepository exportadorRepository;

	@Autowired
	private ParametrosRepository parametrosRepository;
	
	@Autowired
	private SiscomexService siscomexService;
	
	@Autowired
	private CctService cctService;

	public Cct getCctRecepcaoNFEByUnitGkey(String unitGkey) throws ParseException {
//		logger.info("getCctRecepcaoNFEByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.NFE.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);
		
		//testaQuery(unitGkey);
		setDadosTransporte(unitGkey, cct);
		setDanfes(unitGkey, cct);
		setDadosConteiner(unitGkey, cct);
		setBooking(unitGkey, cct);
		setDataEntrada(unitGkey, cct, UNIT_RECEIVE);
		setUnitSeal(unitGkey, cct);
		setRegraPesagemRecepcao(unitGkey, cct);
		setDadosArmador(unitGkey, cct);
		
		setDadosViagem(unitGkey, cct);
		
		
		return cct;
	}
		
	public Cct getCctRecepcaoTransbordoMaritimoByUnitGkey(String unitGkey) throws ParseException {
//		logger.info("getCctRecepcaoTransbordoMaritimoByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);

		setDataEntrada(unitGkey, cct, UNIT_IN_VESSEL);
		setDadosConteiner(unitGkey, cct);
		setRegraPesagemRecepcao(unitGkey, cct);
		setUnitSeal(unitGkey, cct);
		setDadosEntregadorTransbordoMaritimo(unitGkey, cct);
		setDadosRecebedorByOutBoundCarrier(unitGkey, cct);
		
		setBooking(unitGkey, cct);
		setDadosViagem(unitGkey, cct);
		
		return cct;
	}
	
	public Cct getCctRecepcaoDUEByUnitGkey(String unitGkey) throws ParseException {
//		logger.info("getCctRecepcaoDUEByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.DUE.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);
		
		setDataEntrada(unitGkey, cct, UNIT_RECEIVE);
		setDadosConteiner(unitGkey, cct);
		setRegraPesagemRecepcao(unitGkey, cct);
		setUnitSeal(unitGkey, cct);
		setDues(unitGkey, cct);
		setBooking(unitGkey, cct);
		
		setDadosTransporte(unitGkey, cct);
		setDadosEntregadorDUE(unitGkey, cct);
		setDadosRecebedorByOutBoundCarrier(unitGkey, cct);
		
		setDadosViagem(unitGkey, cct);
		
		return cct;
	}

	public void setDadosViagem(String unitGkey, Cct cct) {
		logger.info("setDadosViagem");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOME_NAVIO_OUTBOUND_CARRIER_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] obj = null;
		try {
			obj = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		if(obj != null) {
			cct.setNavio(obj[0].toString());
			cct.setViagem(obj[1].toString());
		}
	}

	@SuppressWarnings("unchecked")
	private void setDadosEntregadorDUE(String unitGkey, Cct cct) {
//		logger.info("setDadosEntregadorDUE");
		Entregador entregador = null;
		if(cct.getEntregador() != null) {
			entregador = cct.getEntregador();
		} else {
			entregador = new Entregador();
		}
		if(cct.getTipoTransportador() == 1) {
			entregador.setNome(cct.getNomeTransportador().substring(0, 59)); 
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CUSTOM_RECINTO_BY_CNPJ_TRANSPORTADORA);
			q.setParameter(CNPJ, formataCNPJ(cct.getCnpjTransportador()));
			List<Object[]> rows = q.getResultList();
			if (rows != null && !rows.isEmpty()) {
				entregador.setCpf(cct.getCpfCondutor());
			} else {
				entregador.setCnpj(cct.getCnpjTransportador());
			}
		}
		entregador.setCct(cct);
		cct.setEntregador(entregador);
	}

	@SuppressWarnings("unchecked")
	public void setDadosRecebedorByOutBoundCarrier(String unitGkey, Cct cct) {
		logger.info("setDadosRecebedorByOutBoundCarrier");
//		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_OUTBOUND_LINE_OPERATOR_BY_UNIT_GKEY);
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Recebedor recebedor = null;
			if(cct.getRecebedor() != null) {
				recebedor = cct.getRecebedor();
			} else {
				recebedor = new Recebedor();
			}
			String tipo = "" + rows.get(0)[0];
			if ("ESTRANGEIRO".equals(tipo)) {
				recebedor.setNome("" + rows.get(0)[2].toString().substring(0, 59));
			} else if ("CNPJ".equals(tipo)) {
				recebedor.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
			recebedor.setCct(cct);
			cct.setRecebedor(recebedor);
			
			Optional<Armador> armador = armadorRepository.findByIdExterno(new BigDecimal(rows.get(0)[3].toString()));
			if (armador.isPresent()) {
				cct.setArmador(armador.get());
			} else {
				Armador novoArmador = new Armador();
				novoArmador.setIdExterno(new BigDecimal(rows.get(0)[3].toString()));
				novoArmador.setNome("" + rows.get(0)[2].toString().substring(0, 59));
				if ("ESTRANGEIRO".equals(tipo))
					novoArmador.setEstrangeiro(1);
				novoArmador.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
				armadorRepository.save(novoArmador);
				cct.setArmador(novoArmador);
			}
		} else {
			logger.info("setDadosRecebedorByOutBoundCarrier - ERRO SEM DADOS SELECT_ACTUAL_ARMADOR_BY_UNIT_GKEY ");
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getRecebedorIdByConteinerGkey(String unitGkey) {
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Object[] o = rows.get(0);
			if(o != null) {
				return (o[2] != null ? o[2].toString() : null);
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private void setDadosEntregadorTransbordoMaritimo(String unitGkey, Cct cct) {
		logger.info("setDadosEntregadorTransbordoMaritimo");
//		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_INBOUND_LINE_OPERATOR_BY_UNIT_GKEY);
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_ENTREGADOR_ARMADOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Entregador entregador = new Entregador();
			String tipo = "" + rows.get(0)[0];
			if ("ESTRANGEIRO".equals(tipo)) {
				entregador.setNome("" + rows.get(0)[2].toString().substring(0, 59));
			} else if ("CNPJ".equals(tipo)) {
				entregador.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
			entregador.setCct(cct);
			cct.setEntregador(entregador);
		}
	}

	private Cct validaConteinerExistenteBaseCCT(String unitGkey) {
//		logger.info("validaConteinerExistenteBaseCCT");
		Cct cct = null;
		List<Cct> cctlist = cctRepository.findByIdExterno(new BigDecimal(unitGkey));
		if (cctlist.isEmpty()) {
//			logger.info("findByIdExterno - EMPTY");
			cct = new Cct();
		} else {
//			logger.info("findByIdExterno - FINDED");
			cct = cctlist.get(0);
		}
		return cct;
	}

	@SuppressWarnings("unused")
	private void testaQuery(String gkey) {
		logger.info("testaQuery");
		logger.info("TESTE 1");
		String sql = " SELECT x.* FROM "+USERDATABASE+".INV_UNIT x where x.GKEY = " + gkey;
		logger.info("QUERY {}", sql);
		logger.info("TESTE 2");
		Query q = entityManager.createNativeQuery(sql);
		logger.info("TESTE 3");
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		if(o != null) {
			logger.info("TESTE 4");
			logger.info("RETORNO {} ", o.toString());
		}
	}

	@SuppressWarnings("unchecked")
	private void setRegraPesagemRecepcao(String unitGkey, Cct cct) {
		logger.info("setRegraPesagemRecepcao");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_REGRA_PESAGEM_RECEPCAO);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			cct.setOog((rows.get(0)[0] != null && "1".equals(rows.get(0)[0].toString())) ? true : false);
			cct.setFirstWeighing(("YES".equals(rows.get(0)[1])) ? true : false);
			if (cct.isFirstWeighing()) {
				cct.setPendenciaPeso(false);
			} else if (cct.isOog()) {
				cct.setPendenciaPeso(false);
				cct.setMotivoNaoPesagem(Cct.MOTIVO_NAO_PESAGEM);
				cct.setPesoBruto(null);
			} else {
				cct.setPendenciaPeso(true);
			}
		} else {
			cct.setPendenciaPeso(true);
		}
	}

	@SuppressWarnings("unchecked")
	private void setUnitSeal(String unitGkey, Cct cct) {
		logger.info("setUnitSeal");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_SEAL_EVENT);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		cct.setUnitSeal(!rows.isEmpty() && rows.get(0) != null ? true : false);
	}

	@SuppressWarnings("unchecked")
	private void setDataEntrada(String unitGkey, Cct cct, String event) throws ParseException {
		logger.info("setDataEntrada");	
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENT_PLACED_TIME);
		q.setParameter(UNITGKEY, unitGkey);
		q.setParameter("EVENT", event);
		q.setParameter("EVENTTEST", event+"_TESTE_SISCOMEX");
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty() && rows.get(0) != null) {
			try {
				Object dtTmp=rows.get(0);
				cct.setDtentr(new SimpleDateFormat(FORMATO_DATA).parse((String) dtTmp));
			} catch(Exception e) {
				cct.setDtentr(null);
				logger.error("SEM DATA DE ENTRADA");
			}
		} else {
			logger.error("SEM DATA DE ENTRADA");
		}
	}

	@SuppressWarnings("unchecked")
	public void setBooking(String unitGkey, Cct cct) {
		logger.info("setBooking");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_NUMBER_POR_NUMERO_CONTEINER);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		cct.setBooking(rows != null && !rows.isEmpty() ? "" + rows.get(0) : "");
	}

	@SuppressWarnings("unchecked")
	private void setDadosTransporte(String unitGkey, Cct cct) {
//		logger.info("setDadosTransporte");
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_TRANSPORTADORA_CONDUTOR);
		q.setParameter(UNITGKEY, unitGkey);
		
		List<Object[]> rows = q.getResultList();
		
		if (rows != null && !rows.isEmpty()) {
			String tipo = "" + rows.get(0)[4];
			if ("ESTRANGEIRO".equals(tipo)) {
				cct.setTipoTransportador(1);
				cct.setNomeTransportador("" + rows.get(0)[5].toString().substring(0, 59));
				cct.setNomeCondutor("" + rows.get(0)[6]);
				cct.setCpfTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			} else if ("CPF".equals("" + rows.get(0)[4])) {
				cct.setTipoTransportador(0);
				cct.setCpfTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			} else if ("CNPJ".equals("" + rows.get(0)[4])) {
				cct.setTipoTransportador(0);
				cct.setCnpjTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
			cct.setCpfCondutor(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[0]));
			cct.setPlacaveiculo("" + rows.get(0)[2]);
			cct.setPlacareboque("" + rows.get(0)[3]);
		}
	}

	@SuppressWarnings("unchecked")
	private void setDadosArmador(String unitGkey, Cct cct) {
		logger.info("setDadosArmador");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Optional<Armador> armador = armadorRepository.findByIdExterno(new BigDecimal(rows.get(0)[3].toString()));
			if (armador.isPresent()) {
				cct.setArmador(armador.get());
			} else {
				Armador novoArmador = new Armador();
				novoArmador.setIdExterno(new BigDecimal(rows.get(0)[3].toString()));
				novoArmador.setNome("" + rows.get(0)[2].toString().substring(0, 59));
				if ("ESTRANGEIRO".equals("" + rows.get(0)[0]))
					novoArmador.setEstrangeiro(1);
				novoArmador.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
				armadorRepository.save(novoArmador);
				cct.setArmador(novoArmador);
			}
			//cct.setNavio("" + rows.get(0)[4]);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setDanfes(String unitGkey, Cct cct) {
//		logger.info("setDanfes");
		List<Danfe> danfeList = new ArrayList<>();
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DANFE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		if (cct.getDanfes() != null && !cct.getDanfes().isEmpty()) {
			Set<String> cctDanfes = new HashSet<>();
			cct.getDanfes().forEach(danfe -> cctDanfes.add(danfe.getChave()));
			Set<String> n4Danfes = new HashSet<>();
			rows.forEach(chave -> n4Danfes.add(chave));
			n4Danfes.removeAll(cctDanfes);
			if (n4Danfes.isEmpty()) {
				return;
			} else {
				for (String chaveDanfe : n4Danfes) {
					Danfe danfe = new Danfe();
					danfe.setChave(chaveDanfe);
					if(danfe.getChave() != null && !"".equals(danfe.getChave()) && !danfe.getChave().isEmpty()) {
						danfeList.add(danfe);
					}
				}
			}
		} else {
			for (String chaveDanfe : rows) {
				Danfe danfe = new Danfe();
				danfe.setChave(chaveDanfe);
				if(danfe.getChave() != null && !"".equals(danfe.getChave()) && !danfe.getChave().isEmpty()) {
					danfeList.add(danfe);
				}
			}
		}
		cct.setDanfes(danfeList);
	}
	
	@SuppressWarnings("unchecked")
	private void setDues(String unitGkey, Cct cct) {
//		logger.info("setDues");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DUE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		String dues = "";
		for (String due : rows) {
			dues = dues + due + ",";
		}
		cct.setNrodue(dues.substring(0, dues.length()-1));
	}
	
	@SuppressWarnings("unchecked")
	public boolean hasDanfe(String unitGkey) {
//		logger.info("hasDanfe");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DANFE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		return rows != null && !rows.isEmpty();
	}
	
	@SuppressWarnings("unchecked")
	public boolean hasDUE(String unitGkey) {
//		logger.info("hasDUE");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DUE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		return rows != null && !rows.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void setDadosConteiner(String unitGkey, Cct cct) {
		logger.info("setDadosConteiner");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_CONTEINER);
		q.setParameter(DIVERGENCIA_PESO_ACIMA, Double.parseDouble(this.getDivergenciaPesoAcima()));
//		logger.info(" Param DIVERGENCIA_PESO_ACIMA : {} ", this.getDivergenciaPesoAcima());
		q.setParameter(DIVERGENCIA_PESO_ABAIXO, Double.parseDouble(this.getDivergenciaPesoAbaixo()));
//		logger.info("Param DIVERGENCIA_PESO_ABAIXO : {} ", this.getDivergenciaPesoAbaixo());
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			cct.setNumeroCont("" + rows.get(0)[0]);
			cct.setTara(new BigDecimal(rows.get(0)[1] != null ? rows.get(0)[1].toString() : "0").setScale(3));
			cct.setPesoBruto(new BigDecimal(rows.get(0)[2] != null ? rows.get(0)[2].toString() : "0").setScale(3));

			if (rows.get(0)[7] != null) {
				q = entityManager.createNativeQuery(QueryUtil.SELECT_AVARIAS);
				q.setParameter("AVARIA_FK", rows.get(0)[7].toString());
				List<String> results = q.getResultList();
				cct.setAvaria(String.join("\n", results));
			} else {
				cct.setAvaria("");
			}
			preencheLacres(cct, rows);
			cct.setDivergenciasIdentificadas(rows.get(0)[8] != null ? rows.get(0)[8].toString() : "");
		}
	}

	private void preencheLacres(Cct cct, List<Object[]> rows) {
//		logger.info("preencheLacres");
		List<String> lacres = new ArrayList<>();
		if (rows.get(0)[3] != null)
			lacres.add(rows.get(0)[3].toString());
		if (rows.get(0)[4] != null)
			lacres.add(rows.get(0)[4].toString());
		if (rows.get(0)[5] != null)
			lacres.add(rows.get(0)[5].toString());
		if (rows.get(0)[6] != null)
			lacres.add(rows.get(0)[6].toString());

		cct.setLacres(String.join(",", lacres));
	}

	@SuppressWarnings("unchecked")
	public void setDadosLacres(String unitGkey, Cct cct) {
//		logger.info("setDadosLacres");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_LACRES);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			List<String> lacres = new ArrayList<>();
			if (rows.get(0)[0] != null)
				lacres.add(rows.get(0)[0].toString());
			if (rows.get(0)[1] != null)
				lacres.add(rows.get(0)[1].toString());
			if (rows.get(0)[2] != null)
				lacres.add(rows.get(0)[2].toString());
			if (rows.get(0)[3] != null)
				lacres.add(rows.get(0)[3].toString());

			cct.setLacres(String.join(",", lacres));
		}
	}

	public String createRecepcaoNFEEvent(BigDecimal unitGkey, String notes) {
//		logger.info("createRecepcaoNFEEvent");
		logger.info("### createRecepcaoNFEEvent - CRIACAO DE EVENTO : UNIT_RECEP_DANFE"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(EVENT, RECEPCAO_NFE_UNIT_EVENT);
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}
	
	public String createRecepcaoConteinerEvent(Cct cct, String notes) {
//		logger.info("createRecepcaoConteinerEvent");
		logger.info("### createRecepcaoConteinerEvent - CRIACAO DE EVENTO : UNIT_RECEP_TRANSBORDO, UNIT_RECEP_DUE"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
			if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())) {
				param.addParameter(EVENT, RECEPCAO_CONTEINER_TRANSBORDO_MARITIMO_UNIT_EVENT);
			} else if(CategoriaProcessoEnum.DUE.toString().equals(cct.getCategoria())) {
				param.addParameter(EVENT, RECEPCAO_CONTEINER_DUE_UNIT_EVENT);
			}
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}

	public String createDueEvents(BigDecimal unitGkey, String notes, boolean geraEventoLiberacaoEmbarque) {
//		logger.info("createDueEvents");
//		logger.info("createDueEvents - unitGkey : {} | notes : {} ", unitGkey, notes);
		logger.info("### createDueEvents - unitGkey : {} | notes : {} - CRIACAO DE EVENTO : UNIT_VINC_DUE, LIBERACAO_PARA_EMBARQUE", unitGkey, notes); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_DUE_EVENTS_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(NOTES, notes);
			param.addParameter("geraLiberacaoEmbarque", geraEventoLiberacaoEmbarque ? "1" : "0");
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.info("createDueEvent - ERROR : {} ", e.getMessage());
			return ERRO + e.getMessage();
		}
	}

	public List<Cct> validaCorrecaoPendenciasRecepcaoNFE(List<Cct> cctListRecepcaoPendente) {
//		logger.info("validaCorrecaoPendencias");
		List<Cct> cctListRecepcaoPendenteAux = new ArrayList<>();
		ListIterator<Cct> cctIt = cctListRecepcaoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (!existePendenciaRecepcaoNFE(cct)) {
				cctListRecepcaoPendenteAux.add(cct);
			}
		}
		return cctListRecepcaoPendenteAux;
	}

	private boolean existePendenciaRecepcaoNFE(Cct cct) {
//		logger.info("existePendenciaRecepcaoNFE");
		switch (cct.getErro()) {
		case Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG:
			setDanfes(cct.getIdExterno().toString(), cct);
			if (cct.getDanfes() == null || cct.getDanfes().isEmpty()) {
//				logger.info("existePendencia - TRUE");
				return true;
			}
			break;
		case Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
//				logger.info("existePendencia - TRUE");
				return true;
			}
			break;
		case Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
//				logger.info("existePendencia - TRUE");
				return true;
			}
			break;
		case Cct.FALHA_VALIDACAO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
//				logger.info("existePendencia - TRUE");
				return true;
			}
			break;
		default:
//			logger.info("existePendencia - FALSE");
			return false;
		}
//		logger.info("existePendencia - FALSE");
		return false;
	}
	
	public List<Cct> validaCorrecaoPendenciasRecepcaoDUE(List<Cct> cctListRecepcaoPendente) {
//		logger.info("validaCorrecaoPendenciasRecepcaoDUE");
		List<Cct> cctListRecepcaoPendenteAux = new ArrayList<>();
		ListIterator<Cct> cctIt = cctListRecepcaoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (existePendenciaRecepcaoDUE(cct)) {
				cctListRecepcaoPendenteAux.add(cct);
			}
		}
		return cctListRecepcaoPendenteAux;
	}

	private boolean existePendenciaRecepcaoDUE(Cct cct) {
//		logger.info("existePendenciaRecepcaoDUE");
		switch (cct.getErro()) {
		case Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				return true;
			}
			break;
		case Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				return true;
			}
			break;
		case Cct.FALHA_VALIDACAO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				return true;
			}
			break;
		default:
			return false;
		}
		return false;
	}
	
	public void validaCorrecaoPendenciasCctTrasbordoMaritimo(List<Cct> cctListTransbordoMaritimoPendente) {
//		logger.info("validaCorrecaoPendenciasCctTrasbordoMaritimo");
		ListIterator<Cct> cctIt = cctListTransbordoMaritimoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (existePendenciaCctTransbordoMaritimo(cct))
				cctIt.remove();
		}
	}

	private boolean existePendenciaCctTransbordoMaritimo(Cct cct) {
//		logger.info("existePendenciaCctTransbordoMaritimo");
		switch (cct.getErro()) {
		case Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				return true;
			}
			break;
		case Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				return true;
			}
			break;
		case Cct.FALHA_VALIDACAO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				return true;
			}
			break;
		default:
			return false;
		}
		return false;
	}

	public void validaVesselATAInserido(List<Cct> cctListEntregaConteiner) {
//		logger.info("validaVesselATAInserido");
		ListIterator<Cct> cctIt = cctListEntregaConteiner.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			validaMudancaNavioEntrega(cct);
			if (!isVesselATAInserido(cct)) {
				cctIt.remove();
			}
		}
	}

	private void validaMudancaNavioEntrega(Cct cct) {
//		logger.info("validaMudancaNavioEntrega");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOME_NAVIO_OUTBOUND_CARRIER_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		Object[] obj = null;
		try {
			obj = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		if(obj != null) {
			
			if(cct.getNavio() == null || cct.getViagem() == null) {
				cct.setNavio(obj[0].toString());
				cct.setViagem(obj[1].toString());
//				cctRepository.save(cct);
				cctService.save(cct);
			} else if(!cct.getNavio().equals(obj[0].toString()) || !cct.getViagem().equals(obj[1].toString())) {
				cct.setNavio(obj[0].toString());
				cct.setViagem(obj[1].toString());
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean isVesselATAInserido(Cct cct) {
//		logger.info("isVesselATAInserido");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_VESSEL_VISIT_ACTUAL_TIME_ARRIVAL_BY_OUTBOUND_CARRIER);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		List<Object[]> rows = q.getResultList();
		return (rows != null && !rows.isEmpty() && rows.get(0) != null);
	}

	public String createEntregaEvent(Cct cct, String notes) {
//		logger.info("createEntregaEvent");
		logger.info("### createEntregaEvent - CRIACAO DE EVENTO : UNIT_ENTREGA, UNIT_ENTREGA_DUE");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
			if(CategoriaProcessoEnum.NFE.toString().equals(cct.getCategoria())){
				param.addParameter(EVENT, ENTREGA_UNIT_EVENT);
			} else if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())){
				param.addParameter(EVENT, ENTREGA_TRANSBORDO_MARITIMO_UNIT_EVENT);
			}
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}

	public String getDivergenciaPesoAcima() {
//		logger.info("getDivergenciaPesoAcima");
		if (divergenciaPesoAcima == null) {
			Parametros param = getParameter(DIVERGENCIA_PESO_ACIMA);
			if (param != null && param.getValor() != null && !param.getValor().isEmpty()) {
				divergenciaPesoAcima = param.getValor();
			} else {
				divergenciaPesoAcima = "0.05";
			}
		}
		return divergenciaPesoAcima;
	}

	public String getDivergenciaPesoAbaixo() {
//		logger.info("getDivergenciaPesoAbaixo");
		if (divergenciaPesoAbaixo == null) {
			Parametros param = getParameter(DIVERGENCIA_PESO_ABAIXO);
			if (param != null && param.getValor() != null && !param.getValor().isEmpty()) {
				divergenciaPesoAbaixo = param.getValor();
			} else {
				divergenciaPesoAbaixo = "0.05";
			}
		}
		return divergenciaPesoAbaixo;
	}

	private Parametros getParameter(String chave) {
//		logger.info("getParameter");
		return parametrosRepository.findByChave(chave);
	}

	public void processaRetornoConsultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos, List<ConsultaDadosResumidosResponseDTO> listaRetorno) {
//		logger.info("processaRetornoConsultaDadosResumidos");
		boolean isValidacaoLiberacao = isValidacaoLiberacao();
		
		ListIterator<Cct> cctIt = cctListConsultaDadosResumidos.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			for (ConsultaDadosResumidosResponseDTO response : listaRetorno) {
				if (response != null && response.getNumeroDUE() != null && cct.getNrodue().contains(response.getNumeroDUE())) {
					
					boolean isAlterado = processaEventosConsultaDadosResumidos(cct, response, isValidacaoLiberacao);
										
					//cct.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
					
					if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
						for (Exportador exportador : cct.getExportadores()) {
							if(exportador.getNrodue() != null && response.getNumeroDUE().equals(exportador.getNrodue())) {
								if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio()) && exportador.getBloqueadoSiscomex() == 0) {
									exportador.setBloqueadoSiscomex(1);
									isAlterado = true;
								} else if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio()) && exportador.getBloqueadoSiscomex() == 1) {
									exportador.setBloqueadoSiscomex(0);
									isAlterado = true;
								}
								if(exportador.getSituacaoDUE() != Integer.parseInt(response.getSituacaoDUE())) {
									exportador.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
									isAlterado = true;
								}
								if(isAlterado) {
									logger.info("##### ATUALIZANDO STATUS DUE TABELA EXPORTADOR");
									exportadorRepository.save(exportador);
								}
							}
						}
					}
//					cctRepository.save(cct);
					if(isAlterado) {
						if(Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio()) && isLiberadoDUE(response.getSituacaoDUE())) {
							aplicaEventoLiberacaoDeExportacaoCasoNaoExista(cct);
						}
						cctService.save(cct);
					} 
//					else {
//						logger.info("processaRetornoConsultaDadosResumidos - SEM ALTERACAO - NAO SALVAR");
//					}
				}
			}
		}
	}

	private void aplicaEventoLiberacaoDeExportacaoCasoNaoExista(Cct cct) {
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		Object event = null;
		try {
			event = q.getSingleResult();
		} catch(Exception e) {
			event = null;
		}
		if(event == null) { 
			List<String> idsUnitEventToCreate = new ArrayList<>();
			idsUnitEventToCreate.add("LIBERACAO_DE_EXPORTACAO");
			try {
				createDadosResumidosEvents(cct, idsUnitEventToCreate);
			} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
				logger.error("aplicaEventoLiberacaoDeExportacaoCasoNaoExista - ERRO {} ", e.getMessage());
			}
		}
	}

	private boolean processaEventosConsultaDadosResumidos(Cct cct, ConsultaDadosResumidosResponseDTO response, boolean isValidacaoLiberacao) {
//		logger.info("processaEventosConsultaDadosResumidos");
		boolean isAlterado = true;
		try {
			if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
				if(cct.getBloqueadoSiscomex() == null || cct.getBloqueadoSiscomex() == 0) {
					
					String unitGkey = cct.getIdExterno().toString();
					
					logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO SISCOMEX : {} ", cct.getNumeroCont());
					cct.setBloqueadoSiscomex(1);
					ServiceParam param = new ServiceParam();
					param.addParameter(ACTION, APPLY_HOLD_SERVICE);
					param.addParameter(UNIT_GKEY, unitGkey);
					param.addParameter(HOLD, HOLD_PERMISSION_EVENT);
					param.addParameter(NOTES, "BLOQUEIO CONSULTA DADOS RESUMIDOS SISCOMEX");
					String retornoApplyHold = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
					logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO APPLY HOLD : {} ", retornoApplyHold);
					
					Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY);
					q.setParameter(UNITGKEY, unitGkey);
					Object event = null;
					try {
						event = q.getSingleResult();
					} catch(Exception e) {
						event = null;
					}
					if(event != null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(EVENT, "LIBERACAO_DE_EXPORTACAO_CANCELAR");
						param.addParameter(NOTES, "Liberado para Despacho");
						String retornoUnitEvent = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO EVENT LIBERACAO_DE_EXPORTACAO_CANCELAR : {} ", retornoUnitEvent);
					}
					
				} else {
//					logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - SEM ALTERACAO");
					isAlterado = false;
				}
			}
			if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
				if(cct.getBloqueadoSiscomex() == null || cct.getBloqueadoSiscomex() == 1) {
					logger.info("### processaConsultaDadosResumidos - CONTAINER DESBLOQUEADO SISCOMEX : {} - CRIACAO DE EVENTO : LIBERACAO_DE_EXPORTACAO, INCLUSAO_DOC_DESPACHO", cct.getNumeroCont());
					
					cct.setBloqueadoSiscomex(0);
					
					List<String> idsUnitEventToCreate = new ArrayList<>();
					if(naoExisteEventoInclusaoDocDespachoDestaDUE(cct)) {
						idsUnitEventToCreate.add("INCLUSAO_DOC_DESPACHO");
					}
					
					if(!isValidacaoLiberacao) {
						
						if(isTodasDUEsDoConteinerDesembaracadas(cct, response)) {
							
							idsUnitEventToCreate.add("LIBERACAO_DE_EXPORTACAO");
							
	//						logger.info("processaConsultaDadosResumidos - LIBERANDO HOLD"); 
							ServiceParam param = new ServiceParam();
							param.addParameter(ACTION, APPLY_PERMISSION_SERVICE);
							param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
							param.addParameter(PERMISSION, HOLD_PERMISSION_EVENT);
							param.addParameter(NOTES, "DESBLOQUEIO CONSULTA DADOS RESUMIDOS SISCOMEX");
							String retornoApplyPermission = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
							logger.info("processaConsultaDadosResumidos - CONTAINER DESBLOQUEADO - RETORNO APPLY PERMISSION : {} ", retornoApplyPermission);
						}
					}
					if (!idsUnitEventToCreate.isEmpty()) {
						logger.info("processaConsultaDadosResumidos - CRIAR Eventos : {} ", idsUnitEventToCreate.toString()); 
						String retorno = createDadosResumidosEvents(cct, idsUnitEventToCreate);
						logger.info("processaConsultaDadosResumidos - CREATE_DESBLOQUEIO_EVENTS = {} - RETORNO N4 {} ", idsUnitEventToCreate.toString(), retorno);
					} else {
						logger.info("processaConsultaDadosResumidos - SEM Eventos a serem criados"); 
					}
				} else {
//					logger.info("processaConsultaDadosResumidos - CONTAINER DESBLOQUEADO - SEM ALTERACAO");
					isAlterado = false;
				}
			}
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("processaConsultaDadosResumidos - ERRO {} ", e.getMessage());
		}
		return isAlterado;
	}

	private String createDadosResumidosEvents(Cct cct, List<String> idsUnitEventToCreate) 
			throws N4ServiceInvokeException, MalformedURLException, RemoteException, ServiceException {
		ServiceParam param = new ServiceParam();
		param.addParameter(ACTION, CREATE_DESBLOQUEIO_EVENTS_SERVICE);
		param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
		param.addParameter(NOTES, "Inclusão documento DUE número " + cct.getNrodue());
		
		String eventos = idsUnitEventToCreate.toString().replaceAll(" ", "");
		eventos = eventos.replaceAll("[\\[\\]]", "");
		eventos = eventos.trim();
		
//					logger.info("### LISTA DE EVENTOS -> {} ", idsUnitEventToCreate.toString());
//					logger.info("### PARAM EENTOS -> {} ", eventos);
		
		param.addParameter("events_to_create", eventos);
		String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		return retorno;
	}
	
	@SuppressWarnings({ "unchecked" })
	private boolean naoExisteEventoInclusaoDocDespachoDestaDUE(Cct cct) {
//		logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOTES_EVENTO_INCLUSAO_DOC_DESPACHO_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		List<Object> lista = q.getResultList();
		for (Object note : lista) {
			if(note != null && !"".equals(note.toString()) && note.toString().contains(cct.getNrodue())) {
				logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE - ACHOU EVENTO DA DUE | NOTE : {} ", note);
				return false;
			}
		}
		logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE - NAO ACHOU EVENTO DA DUE");
		return true;
	}

	private boolean isTodasDUEsDoConteinerDesembaracadas(Cct cct, ConsultaDadosResumidosResponseDTO response) {
		logger.info("isTodasDUEsDoConteinerDesembaracadas - SISCOMEX - IndicadorBloqueio: {} - SituacaoDUE: {} ", response.getIndicadorBloqueio(), response.getSituacaoDUE());
		if(cct.getNrodue().contains(",")) {
//			logger.info("isTodasDUEsDoConteinerDesembaracadas - MAIS DE UMA DUE NO CONTEINER");
			List<Cct> cctListConsultaDadosResumidos = new ArrayList<>();
			cctListConsultaDadosResumidos.add(cct);
			ConsultaDadosResumidosResponseListDTO result = null;
//			result = siscomexService.consultaDadosResumidosMOCK(cctListConsultaDadosResumidos);
			result = siscomexService.consultaDadosResumidos(cctListConsultaDadosResumidos);
			for (ConsultaDadosResumidosResponseDTO dto : result.getListaRetorno()) {
				if (Cct.CONTAINER_BLOQUEADO.toString().equals(dto.getIndicadorBloqueio()) || !isLiberadoDUE(dto.getSituacaoDUE())) {
					logger.info("isTodasDUEsDoConteinerDesembaracadas - CONTEINER TEM UMA DUE BLOQUEADA");
//					logger.info("isTodasDUEsDoConteinerDesembaracadas - FALSE");
					return false;
				}
			}
			
		} else {
			if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio()) || !isLiberadoDUE(response.getSituacaoDUE())) {
				logger.info("isTodasDUEsDoConteinerDesembaracadas - CONTEINER TEM UMA DUE BLOQUEADA");
//				logger.info("isTodasDUEsDoConteinerDesembaracadas - FALSE");
				return false;
			}
		}
		logger.info("isTodasDUEsDoConteinerDesembaracadas - TRUE");
		return true;
	}

//	public String createDesbloqueioDUEEvents(Cct cct) {
//		logger.info("createDesbloqueioDUEEvents");
//		try {
//			List<String> idsUnitEventToCreate = validaEventosLiberacao(cct.getIdExterno().toString());
//			if (!idsUnitEventToCreate.isEmpty()) {
//				logger.info("processaConsultaDadosResumidos - CRIAR Eventos : {} ", idsUnitEventToCreate.toString());
//				ServiceParam param = new ServiceParam();
//				param.addParameter(ACTION, CREATE_DESBLOQUEIO_EVENTS_SERVICE);
//				param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
//				param.addParameter(NOTES, "DUE: " + cct.getNrodue());
//				String ids = idsUnitEventToCreate.toString();
//				ids = ids.replaceAll("[\\[\\]]", "");
//				ids = ids.replaceAll(" ", "");
//				param.addParameter("events_to_create", ids);
//				return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
//			} else {
//				logger.info("processaConsultaDadosResumidos - SEM Eventos a serem criados");
//				return "OK";
//			}
//		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
//			logger.error(ERRO + e.getMessage());
//			return "ERRO";
//		}
//	}

	private boolean isLiberadoDUE(String situacaoDUE) {
//		logger.info("isLiberadoDUE");
		return ( String.valueOf(SituacaoDUEEnum.LIBERADA_SEM_CONFERENCIA_ADUANEIRA_CANAL_VERDE.value()).equals(situacaoDUE) ||
				 String.valueOf(SituacaoDUEEnum.EMBARQUE_ANTECIPADO_AUTORIZADO.value()).equals(situacaoDUE) ||
				 String.valueOf(SituacaoDUEEnum.DESEMBARACADA.value()).equals(situacaoDUE) ); 
	}
	
	@SuppressWarnings("unchecked")
	public List<String> validaEventosLiberacao(String unitGkey) {
//		logger.info("validaEventosLiberacao");
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTOSLIBERACAO_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object> lista = q.getResultList();
		
		if (lista != null && !lista.isEmpty()) {
			if(lista.size() == 2) {
				return Collections.emptyList();
			}
			if(lista.size() == 1) {
				q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTOSLIBERACAO_BY_GKEY);
				q.setParameter("GKEY", lista.get(0));
				return q.getResultList();
			}
			return Arrays.asList("LIBERACAO_DE_EXPORTACAO", "INCLUSAO_DOC_DESPACHO");
		} else {
			return Arrays.asList("LIBERACAO_DE_EXPORTACAO", "INCLUSAO_DOC_DESPACHO");
		}
	}
	
	public boolean isConteinerExportacao(String unitGkey) {
//		logger.info("isConteinerExportacao");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_CATEGORY_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && EXPRT.equals(o.toString()));
	}
	
	public boolean isConteinerTransbordo(String unitGkey) {
//		logger.info("isConteinerTransbordo");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_CATEGORY_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && TRSHP.equals(o.toString()));
	}
	
	public boolean isPortoOrigemNacional(String unitGkey) {
//		logger.info("isPortoOrigemNacional");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_POL_OPL_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] o = null;
		try {
			o = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return false;
		}
		String routingPointGkey = "";
		for (Object object : o) {
			if(object != null) {
				routingPointGkey = object.toString();
			}
		}
		q = entityManager.createNativeQuery(QueryUtil.SELECT_COUNTRY_BY_ROUTING_POINT_GKEY);
		q.setParameter(ROUTING_POINT_GKEY, routingPointGkey);
		Object obj = null;
		try {
			obj = q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		return (obj != null && BR.equals(obj.toString()));		
	}
	
	public boolean isPortoDestinoInternacional(String unitGkey) {
//		logger.info("isPortoDestinoInternacional");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_POD_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] o = null;
		try {
			o = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return false;
		}
		String routingPointGkey = "";
		for (Object object : o) {
			if(object != null) {
				routingPointGkey = object.toString();
			}
		}
		q = entityManager.createNativeQuery(QueryUtil.SELECT_COUNTRY_BY_ROUTING_POINT_GKEY);
		q.setParameter(ROUTING_POINT_GKEY, routingPointGkey);
		Object obj = null;
		try {
			obj = q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		return (obj != null && !BR.equals(obj.toString()));
	}

	public boolean isVesselInBoundCarrier(String unitGkey) {
//		logger.info("isVesselInBoundCarrier");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_INBOUND_CARRIER_MODE_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && VESSEL.equals(o.toString()));
	}
	
	public boolean isVesselOutBoundCarrier(String unitGkey) {
//		logger.info("isVesselOutBoundCarrier");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_OUTBOUND_CARRIER_MODE_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && VESSEL.equals(o.toString()));
	}
	
	private String formataCNPJ(String cnpj) {
//		logger.info("formataCNPJ");
		cnpj = CharMatcher.inRange('0', '9').retainFrom(cnpj);
		try {
			MaskFormatter mask = null;
			mask = new MaskFormatter(CNPJ_PATTERN);
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(cnpj);
		} catch (ParseException e) {
			return null;
		}
	}
	
	
	/** CLIENTE FINANCEIRO PAGADOR */
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getBooking(String nbr, String lineOperatorGkey) {
//		logger.info("getBooking");
		if(lineOperatorGkey == null) {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_BY_NBR);
			q.setParameter("NBR", nbr);
			List<Object[]> rows = q.getResultList();
			return rows;
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_BY_NBR_LINE_OPERATOR);
			q.setParameter("NBR", nbr);
			q.setParameter("LINEGKEY", lineOperatorGkey);
			List<Object[]> rows = q.getResultList();
			return rows;
		}
	}
	
	public Object[] getLineOperatorByGkey(Long gkey) {
//		logger.info("getLineOperatorByGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_LINE_OPERATOR_BY_GKEY);
		q.setParameter("GKEY", gkey);
		Object[] row = null;
		try {
			row = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return row;
	}
	
	public Object getLineOpGkeyByUnitGkey(String unitGkey) {
//		logger.info("getLineOpGkeyByUnitGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ARMADOR_GKEY_POR_NUMERO_CONTEINER);
		q.setParameter("UNITGKEY", unitGkey);
		Object row = null;
		try {
			row = (Object) q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return row;
	}
	
	public String getDeadLineByBookingNbr(String bookingNbr, String lineOperatorGkey) {
//		logger.info("getDeadLineByBookingNbr");
		if(lineOperatorGkey == null) {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DEADLINE_BY_BOOKING_NBR);
			q.setParameter("NBR", bookingNbr);
			Object deadLine = null;
			try {
				deadLine = q.getSingleResult();
			} catch(Exception e) {
				return null;
			}
			return deadLine.toString();
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DEADLINE_BY_BOOKING_NBR_AND_LINE_OPERATOR);
			q.setParameter("NBR", bookingNbr);
			q.setParameter("LINEGKEY", lineOperatorGkey);
			Object deadLine = null;
			try {
				deadLine = q.getSingleResult();
			} catch(Exception e) {
				return null;
			}
			return deadLine.toString();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAgentRepresentations(String despachante) {
//		logger.info("getAgentRepresentations");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_GKEY_BY_AGENT_DOC);
		q.setParameter("DESPACHANTE", despachante);
		List<Object> agentGkeys = q.getResultList();
		if(agentGkeys == null || agentGkeys.isEmpty() || agentGkeys.size() == 0) {
			return null;
		} else {
			q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_REPRESENTATIONS_BY_AGENT_GKEY);
			q.setParameter("AGENT_GKEYS", agentGkeys);
			List<Object[]> rows = q.getResultList();
			return rows;
		}
	}

	public String createClienteFinanceiroPagadorEvents(Cct cct, Tabela cfp) {
//		logger.info("createClienteFinanceiroPagadorEvents");
		try {
			ServiceParam param = null;
			String unitGkey = cct.getIdExterno().toString();
			for (Exportador exportador : cct.getExportadores()) {
				param = new ServiceParam();
				param.addParameter(ACTION, CREATE_CFP_EVENT_SERVICE);
				param.addParameter(UNIT_GKEY, unitGkey);
				param.addParameter(NOTES, "DUE/" + exportador.getNrodue());
				param.addParameter(RESPONSIBLE_PARTY, getGkeyScopedBizUnitByShipperId(String.valueOf(exportador.getNumero())));
				String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
				logger.info("createClienteFinanceiroPagadorEvents - RETORNO EVENTO CLIENTE_FINANCEIRO_PAGADOR : {} ", retorno);
			}
			
			if(isValidacaoLiberacao()) {
				boolean geraEventoLiberacaoExportacao = true;
				logger.info("#### VALIDANDO CONTEINER LIBERADO E DESBLOQUEADO");
				for (Exportador exportador : cct.getExportadores()) {
					if(exportador.getBloqueadoSiscomex() == 1 || !isLiberadoDUE(exportador.getSituacaoDUE().toString())) {
						logger.info("#### VALIDANDO CONTEINER NÃO LIBERADO OU BLOQUEADO - NAO SERA GERADO EVENTO LIBERACAO_DE_EXPORTACAO");
						geraEventoLiberacaoExportacao = false;
						break;
					}
				}
				
				Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY);
				q.setParameter(UNITGKEY, unitGkey);
				Object event = null;
				try {
					event = q.getSingleResult();
				} catch(Exception e) {
					event = null;
				}
				
				if(geraEventoLiberacaoExportacao) {
					if(event == null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_CFP_LIBERACAO_DE_EXPORTACAO_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(NOTES, "Liberado para Despacho");
						String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("createClienteFinanceiroPagadorEvents - RETORNO EVENTO LIBERACAO_DE_EXPORTACAO : {} ", retorno);
					}
				} else {
					if(event != null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(EVENT, "LIBERACAO_DE_EXPORTACAO_CANCELAR");
						param.addParameter(NOTES, "Liberado para Despacho");
						String retornoUnitEvent = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO EVENT LIBERACAO_DE_EXPORTACAO_CANCELAR : {} ", retornoUnitEvent);
					}
				}
			}
						
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error(ERRO + e.getMessage());
			return Cct.STATUS_ERRO;
		}
		return Cct.STATUS_OK;
	}

	private boolean isValidacaoLiberacao() {
//		logger.info("isValidacaoLiberacao");
		Parametros parametroCFP = parametrosRepository.findByChave(VALIDACAO_LIBERACAO);
//		logger.info("isValidacaoLiberacao - PARAM VALIDACAO_LIBERACAO = {} ", parametroCFP.getValor());
		boolean isValidacaoLiberacao = parametroCFP != null && parametroCFP.getValor() != null && parametroCFP.getValor().equals("1");
//		logger.info("isValidacaoLiberacao - RETORNO parametroCFP.getValor().equals(\"1\")  ->  {} ", isValidacaoLiberacao);
		return isValidacaoLiberacao;
	}

	private String getGkeyScopedBizUnitByShipperId(String shipperId) {
//		logger.info("getGkeyScopedBizUnitByShipperId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", shipperId);
		Object gkey = null;
		try {
			gkey = q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return gkey.toString();
	}

	@SuppressWarnings("unchecked")
	public boolean hasCustomRecintoNTE(Cct cct) {
//		logger.info("hasCustomRecintoNTE");
		String cnpj = cct.getCnpjTransportador();
		if(cnpj != null || !"".equals(cnpj)) {
			cnpj = formataCNPJ(cnpj);
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CUSTOM_RECINTO_NTE_BY_CNPJ_EXPORTADOR);
			q.setParameter("CNPJ", cnpj);
			List<Object[]> result = null;
			result = q.getResultList();
			return result != null;
		}
		return false;
	}
	
	public boolean processaExportador(Cct cct) {
//		logger.info("processaExportador");
		logger.info("#### PRIMEIRO CADASTRO EXPORTADORES - DADOS RESUMIDOS");
		boolean isAlterado = true;
		List<Cct> cctList = new ArrayList<>();
		cctList.add(cct);
		ConsultaDadosResumidosResponseListDTO result = null;
		//TODO retirar MOCK
//		result = siscomexService.consultaDadosResumidosMOCK(cctList);
		result = siscomexService.consultaDadosResumidos(cctList);
		
		if(result != null && result.getListaRetorno() != null && !result.getListaRetorno().isEmpty()) {
			
			cct.setExportadores(processaExportadoresRetornoSiscomex(result.getListaRetorno(), cct));
			
			if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
				if(!isValidacaoLiberacao()) {
					logger.info("#### VERIFICANDO ATUALIZACAO SHIPPER NO BOOKING");
					atualizaBookingShipper(cct, null);
				} else {
					logger.info("#### PARAM CFP LIGADA - NAO ATUALIZAR SHIPPER NO BOOKING");
				}
			} else {
				logger.info("#### NENHUM EXPORTADOR VINCULADO NO CCT");
				isAlterado = false;
			}
		}
		return isAlterado;
	}

	private List<br.com.grupolibra.dueltrservice.model.Exportador> processaExportadoresRetornoSiscomex(List<ConsultaDadosResumidosResponseDTO> listaRetorno,Cct cct) {
//		logger.info("processaExportadoresRetornoSiscomex");
		List<br.com.grupolibra.dueltrservice.model.Exportador> exportadorList = new ArrayList<>();
		for (ConsultaDadosResumidosResponseDTO response : listaRetorno) {
			logger.info("processaExportadoresRetornoSiscomex - QTD EXPORTADORES RETORNADOS SISCOMEX : {} ", ( response != null && response.getExportadores() != null ? response.getExportadores().size() : "0" ));
			if(response != null && response.getExportadores() != null) {
				for (br.com.grupolibra.dueltrservice.dto.Exportador exportadorResp : response.getExportadores()) {
					
					boolean hasExportador = false;
					if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
						for (Exportador exportadorAux : cct.getExportadores()) {
							if(exportadorResp.getNumero().equals(exportadorAux.getNumero()) ) {
								hasExportador = true;
							}
						}
					}
					
					if(!hasExportador) {
						br.com.grupolibra.dueltrservice.model.Exportador exportador = new br.com.grupolibra.dueltrservice.model.Exportador();
						exportador.setNumero(exportadorResp.getNumero());
						exportador.setTipo(exportadorResp.getTipo());
						exportador.setCct(cct);
						exportador.setNrodue(response.getNumeroDUE());
						exportador.setValidado(0);
						if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
							exportador.setBloqueadoSiscomex(1);
						} else if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
							exportador.setBloqueadoSiscomex(0);
						}
						exportador.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
						exportadorList.add(exportador);
					}
				}
			}
		}
		return exportadorList;
	}

	public void atualizaBookingShipper(Cct cct, String exportadorId) {
//		logger.info("atualizaBookingShipper");

		if(cct.getExportadores() != null && !cct.getExportadores().isEmpty() && cct.getBooking() != null) {
			
			String shipperGkeyAtualizarBooking = "";
			
			if(exportadorId == null) {
				logger.info("atualizaBookingShipper - ATUALIZANDO PELO DADOS RESUMIDOS");
				for (br.com.grupolibra.dueltrservice.model.Exportador exportador : cct.getExportadores()) {
					
					String shipperGkey = getShipperGkeyByExportador(exportador);
		    		if(shipperGkey == null) {
		    			cct.setMensagem(Cct.EXPORTADOR_NAO_CADASTRADO + " - CNPJ: " + exportador.getNumero());
		    			cct.setStatus(Cct.STATUS_ERRO);
		    			cct.setStatusProc(Cct.STATUS_ERRO_EXPORTADOR);
		    			return;
		    		} else {
		    			shipperGkeyAtualizarBooking = shipperGkey;
		    		}
				}
			} else {
				logger.info("atualizaBookingShipper - ATUALIZANDO PELO CFP");
				String shipperGkey = getShipperGkeyByExportadorId(exportadorId);
				shipperGkeyAtualizarBooking = shipperGkey;
			}
			
        	Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_GKEY_BY_BOOKING_NBR);
    		q.setParameter(NBR, cct.getBooking());
    		String bookingGkey = q.getResultList().get(0).toString();
    		
	    	try {	
	    		ServiceParam param = new ServiceParam();
				param.addParameter(ACTION, ATUALIZA_BOOKING_SHIPPER);
				param.addParameter("bookingGkey", bookingGkey);
				param.addParameter("shipperGkey", shipperGkeyAtualizarBooking);
				String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
				logger.info("atualizaBookingShipper - RETORNO N4 : {} ", retorno);
			} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
				logger.error(ERRO + e.getMessage());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public String getShipperGkeyByExportador(br.com.grupolibra.dueltrservice.model.Exportador exportador) {
//		logger.info("getShipperGkeyByExportadorId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", exportador.getNumero());
		List<Object> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0).toString();
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getShipperGkeyByExportadorId(String exportadoId) {
//		logger.info("getShipperGkeyByExportadorId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", exportadoId);
		List<Object> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0).toString();
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Object[] getDespachanteDocAndNameById(String despachante) {
//		logger.info("getDespachanteDocAndNameById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_DOC_AND_NAME_BY_AGENT_ID);
		q.setParameter("DESPACHANTE", despachante);
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getShipperDocAndNameById(String shipper) {
//		logger.info("getShipperDocAndNameById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_NAME_AND_DOC_BY_SHIPPER_ID);
		q.setParameter("ID", shipper);
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0);
		} else {
			return null;
		}
	}
	
	public List<Cct> validaUnitFacilityVisitStateActive(List<Cct> cctList, String tipo) {
		//logger.info("validaUnitFacilityVisitStateActive");
		List<Cct> listaRetorno = new ArrayList<>();
		for (Cct cct : cctList) {
			try {
				Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_FACILITY_VISIT_STATE_BY_UNIT_GKEY);
				q.setParameter("GKEY", cct.getIdExterno());
				List<Object[]> resultList = q.getResultList();
				if(resultList != null && !resultList.isEmpty()) {
					Object[] o = resultList.get(0);
					if(o != null && o[1] != null) {
						if("1ACTIVE".equals(o[1].toString())) {
							//logger.info("validaUnitFacilityVisitStateActive - ACTIVE");
							listaRetorno.add(cct);
						} else {
							logger.info(" ------------------------------------------------------------------------------------- ");
							logger.info(" ------------------------------------------------------------------------------------- ");
							logger.info("validaUnitFacilityVisitStateActive - FINALIZANDO UNIT NOT ACTIVE");
							logger.info("validaUnitFacilityVisitStateActive - TIPO DA CHAMADA : {} ", tipo);
							logger.info("validaUnitFacilityVisitStateActive - UFV STATE : {} ", o[1].toString());
							logger.info("validaUnitFacilityVisitStateActive - CONTEINER : {} {} ", cct.getIdExterno(), cct.getNumeroCont());
							logger.info(" ------------------------------------------------------------------------------------- ");
							logger.info(" ------------------------------------------------------------------------------------- ");
							cct.setFinalizado(1);
							cct.setStatusProc(Cct.STATUS_DEVOLVIDO);
							cct.setErro("UNIT FACILITY VISIT STATE " + o[1]);
	//						cctRepository.save(cct);
							cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
							cct.setDtproc(new Date(System.currentTimeMillis()));
							cctService.save(cct);
						}
					}
				} else {
					logger.info(" ------------------------------------------------------------------------------------- ");
					logger.info(" ------------------------------------------------------------------------------------- ");
					logger.info("validaUnitFacilityVisitStateActive - UNIT SEM UFV");
					logger.info("validaUnitFacilityVisitStateActive - TIPO DA CHAMADA : {} ", tipo);
					logger.info("validaUnitFacilityVisitStateActive - CONTEINER : {} {} ", cct.getIdExterno(), cct.getNumeroCont());
					logger.info(" ------------------------------------------------------------------------------------- ");
					logger.info(" ------------------------------------------------------------------------------------- ");
//					cct.setFinalizado(1);
//					cct.setStatusProc(Cct.STATUS_DEVOLVIDO);
//					cct.setErro("SEM UNIT FACILITY VISIT NO N4");
////					cctRepository.save(cct);
//					cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
//					cctService.save(cct);
				}
			} catch(Exception e) {
				logger.info("validaUnitFacilityVisitStateActive ERRO : {}", e.getMessage());
			}
		}
		return listaRetorno;
	}

	@SuppressWarnings("unchecked")
	public VinculoRepresentanteDTO findRepresentante(String cnpj, String nome) {
		logger.info("findRepresentante - N4Repository");
		
		VinculoRepresentanteDTO retorno = new VinculoRepresentanteDTO();
		StringBuilder sql = new StringBuilder(QueryUtil.SELECT_REPRESENTANTE);
		if(cnpj != null && !"".equals(cnpj) && !"null".equals(cnpj) && !"undefined".equals(cnpj)) {
			sql.append(" AND ID = '" + cnpj + "' "); 
		}
		if(nome != null && !"".equals(nome) && !"null".equals(nome) && !"undefined".equals(nome)) {
			sql.append(" AND UPPER(NAME) LIKE UPPER('%" + nome + "%') ");
		}
		
		logger.info("findRepresentante - QUERY: {} ", sql.toString() );
		
		Query q = entityManager.createNativeQuery(sql.toString());
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			
			logger.info("findRepresentante - Result size: {}" , resultList.size()); 
			
			List<VinculoRepresentante> lst = new ArrayList<>();
			for (Object[] object : resultList) {
				VinculoRepresentante vr = new VinculoRepresentante();
				vr.setCnpj(object[0].toString());
				vr.setNomeRepresentante(object[1].toString());
				lst.add(vr);
			}
			retorno.setListaVinculo(lst);
			return retorno;
		} else {
			logger.info("findRepresentante - sem representante ");
			retorno.setErro(true);
			retorno.setMensagem("Nenhum representante encontrado.");
			return retorno;
		}
	}

	public VinculoRepresentanteDTO findAllVinculo(String searchText, String pageIndex, String pageSize) {
		
		logger.info("findAllVinculo - N4Repository ");
		
		VinculoRepresentanteDTO retorno = new VinculoRepresentanteDTO();
		retorno.setCurrentPage(Integer.parseInt(pageIndex));
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT X.GKEY, X.ID, X.NAME, V.ID AS ID_TABELA_EXTERNA, V.CNPJ_REPRESENTANTE, V.NOME_REPRESENTANTE " +
				"	FROM "+USERDATABASE+".REF_BIZUNIT_SCOPED X " +
				"	   , VINCULO_REPRESENTANTE V " +
				"	WHERE V.ID_EXTERNO (+) = X.GKEY  " +
				"	AND X.LIFE_CYCLE_STATE = 'ACT' " +
				"	AND X.ROLE = 'LINEOP' " +
				"	AND X.SMS_NUMBER = 'ESTRANGEIRO' " +
				"	AND X.ID <> 'GENERIC'  ");
		
		if(searchText != null && !"".equals(searchText)) {
			queryString.append(" AND ( V.CNPJ_REPRESENTANTE like '%" + searchText + "%' " +
							" OR V.NOME_REPRESENTANTE like '%" + searchText + "%' " +
							" OR X.ID like '%" + searchText + "%' " +
							" OR X.NAME like '%" + searchText + "%' ) ");
		}
		
		queryString.append("	ORDER BY X.NAME ");
		
		logger.info("findAllVinculo - QUERY: {} ", queryString.toString());
		
		Query q = entityManager.createNativeQuery(queryString.toString());
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			
			logger.info("findAllVinculo - QTD REGISTROS QUERY: {} ", resultList.size());
			
			List<VinculoRepresentante> lst = new ArrayList<>();
			
			for (Object[] o : resultList) {
				VinculoRepresentante vr = new VinculoRepresentante();
				vr.setIdExterno(o[0].toString());
				vr.setCodigo(o[1].toString());
				vr.setNomeLineOp(o[2].toString());
				
				vr.setId(o[3] != null ? Long.valueOf(o[3].toString()) : null);
				vr.setCnpj(o[4] != null ? o[4].toString() : null);
				vr.setNomeRepresentante(o[5] != null ? o[5].toString() : null);
				
				lst.add(vr);
			}
			retorno.setQtdRegistros(lst.size());			
			List<List<VinculoRepresentante>> paginatedList = Lists.partition(lst, Integer.valueOf(pageSize));
			
			logger.info("findAllVinculo - QTD PAGINAS: {} ", paginatedList.size());
			logger.info("findAllVinculo - QTD DA PAGINA : {} ", paginatedList.get(Integer.valueOf(pageIndex)-1).size());
			
			retorno.setListaVinculo(paginatedList.get(Integer.valueOf(pageIndex)-1));
			return retorno;
		} else {
			logger.info("findAllVinculo - sem registros ");
			return null;
		}
		
	}

	public String getStatusConteiner(String conteiner) {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select VISIT_STATE from SPARCSN4.INV_UNIT where ID like '%" + conteiner + "%' ");
		Query q = entityManager.createNativeQuery(queryString.toString());
		List<Object> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			String result = resultList.get(0) != null ? resultList.get(0).toString() : "não encontrado"; 
			return result;
		}
		return "não encontrado";
	}
	
}
