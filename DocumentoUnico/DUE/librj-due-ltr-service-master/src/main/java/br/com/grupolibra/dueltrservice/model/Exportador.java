package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQuery(name = "Exportador.findAll", query = "SELECT e FROM Exportador e")
public class Exportador implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXPORTADOR_SEQ")
	@SequenceGenerator(name = "EXPORTADOR_SEQ", sequenceName = "EXPORTADOR_SEQ", allocationSize = 1)
	private long id;

	private String numero;
	
	private String tipo;
	
	@Column(name = "NRODUE")
	private String nrodue;
	
	private int validado;
	
	/**
	 * 0 - DESBLOQUEADO
	 * 1 - BLOQUEADO
	 * */
	@Column(name = "BLOQUEADO_SISCOMEX")
	private Integer bloqueadoSiscomex;
	
	@Column(name = "SITUACAO_DUE")
	private Integer situacaoDUE;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "CCT_ID")
	private Cct cct;

	public long getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public String getTipo() {
		return tipo;
	}

	public Cct getCct() {
		return cct;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setCct(Cct cct) {
		this.cct = cct;
	}

	public String getNrodue() {
		return nrodue;
	}

	public void setNrodue(String nrodue) {
		this.nrodue = nrodue;
	}

	public int getValidado() {
		return validado;
	}

	public void setValidado(int validado) {
		this.validado = validado;
	}

	public Integer getBloqueadoSiscomex() {
		return bloqueadoSiscomex;
	}

	public Integer getSituacaoDUE() {
		return situacaoDUE;
	}

	public void setBloqueadoSiscomex(Integer bloqueadoSiscomex) {
		this.bloqueadoSiscomex = bloqueadoSiscomex;
	}

	public void setSituacaoDUE(Integer situacaoDUE) {
		this.situacaoDUE = situacaoDUE;
	}

	
}