package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQuery(name = "Danfe.findAll", query = "SELECT d FROM Danfe d")
public class Danfe implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DANFE_SEQ")
	@SequenceGenerator(name = "DANFE_SEQ", sequenceName = "DANFE_SEQ", allocationSize = 1)
	private long id;

	private String chave;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "CCT_ID")
	private Cct cct;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public Cct getCct() {
		return cct;
	}

	public void setCct(Cct cct) {
		this.cct = cct;
	}

}