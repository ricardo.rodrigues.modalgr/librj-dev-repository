package br.com.grupolibra.dueltrservice.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Monitoramento.findAll", query = "SELECT m FROM Monitoramento m")
public class Monitoramento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	private String navio;

	private String visit;

	private String lineOp;

	private String booking;

	private String conteiner;

	@Column(name="DANFES")
	private String danfesString;

	@Column(name="DUES")
	private String duesString;

	private String status;

	private String situacao;

	private boolean bloqueado;

	private String mensagem;

	private Date dataEntrada;

	private Date dataProcessamento;

	private String erro;
	
	private boolean ok;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNavio() {
		return navio;
	}

	public void setNavio(String navio) {
		this.navio = navio;
	}

	public String getVisit() {
		return visit;
	}

	public void setVisit(String visit) {
		this.visit = visit;
	}

	public String getLineOp() {
		return lineOp;
	}

	public void setLineOp(String lineOp) {
		this.lineOp = lineOp;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getConteiner() {
		return conteiner;
	}

	public void setConteiner(String conteiner) {
		this.conteiner = conteiner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Date getDataEntrada() {		
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getDanfesString() {
		return danfesString;
	}

	public void setDanfesString(String danfesString) {
		this.danfesString = danfesString;
	}

	public String getDuesString() {
		return duesString;
	}

	public void setDuesString(String duesString) {
		this.duesString = duesString;
	}

}
