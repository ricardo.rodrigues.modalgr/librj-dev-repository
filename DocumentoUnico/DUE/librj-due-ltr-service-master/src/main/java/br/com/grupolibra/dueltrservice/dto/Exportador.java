package br.com.grupolibra.dueltrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Exportador {

	private String numero;
	
	private String tipo;
	
}
