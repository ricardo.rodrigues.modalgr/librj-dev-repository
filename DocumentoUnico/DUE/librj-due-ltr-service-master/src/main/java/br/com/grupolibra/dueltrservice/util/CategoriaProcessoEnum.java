package br.com.grupolibra.dueltrservice.util;

/**
 * 
 * @author dev
 *
 * Categoria de processamento Recepcao/Entrega
 *
 */
public enum CategoriaProcessoEnum {

	NFE, TRANSBORDO_MARITIMO, DUE;

}
