package br.com.grupolibra.dueltrservice.dto;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListaMonitoramentoDTO {
	
	private long id;

	private String navio;
	
	private String visit;
	
	private String lineOp;
	
	private String booking;
	
	private String conteiner;
	
	private List<String> danfes;
	
	private List<String> dues;
	
	private String status;
	
	private String situacao;
	
	private boolean bloqueado;
	
	private String mensagem;
	
	private Date dataEntrada;
	
	private Date dataProcessamento;
	
	private String erro;
	
}
