package br.com.grupolibra.dueltrservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class ProcessaCctExportadorPendenteTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private N4Repository n4Repository;
	
	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void processaCctExportadorPendente() {
		logger.info("###### INICIO TASK PROCESSA CCT EXPORTADOR PENDENTE");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
//		logger.info("processaCctExportadorPendente");
		List<Cct> cctListExportadorPendente = cctService.getCctsExportadorPendente();
		if (cctListExportadorPendente != null && !cctListExportadorPendente.isEmpty()) {
			
//			logger.info("processaCctExportadorPendente - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListExportadorPendenteAux.size());
//			List<Cct> cctListExportadorPendente = n4Repository.validaUnitFacilityVisitStateActive(cctListExportadorPendenteAux);
			
			logger.info("processaCctExportadorPendente - qtd registros exportador pendente : {} ", cctListExportadorPendente.size());
			for (Cct cct : cctListExportadorPendente) {
				boolean isAlterado = n4Repository.processaExportador(cct);
				if(isAlterado) {
	//				cctRepository.save(cct);
					cctService.save(cct);
				}
			}
		} else {
			logger.info("processaCctExportadorPendente - sem registro exportador pendente");
		}
	}
}
