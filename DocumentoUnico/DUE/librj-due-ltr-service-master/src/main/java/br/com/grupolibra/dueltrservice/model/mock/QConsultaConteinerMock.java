package br.com.grupolibra.dueltrservice.model.mock;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QConsultaConteinerMock is a Querydsl query type for ConsultaConteinerMock
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QConsultaConteinerMock extends EntityPathBase<ConsultaConteinerMock> {

    private static final long serialVersionUID = -581185022L;

    public static final QConsultaConteinerMock consultaConteinerMock = new QConsultaConteinerMock("consultaConteinerMock");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nrConteiner = createString("nrConteiner");

    public final StringPath nrDUE = createString("nrDUE");

    public QConsultaConteinerMock(String variable) {
        super(ConsultaConteinerMock.class, forVariable(variable));
    }

    public QConsultaConteinerMock(Path<? extends ConsultaConteinerMock> path) {
        super(path.getType(), path.getMetadata());
    }

    public QConsultaConteinerMock(PathMetadata metadata) {
        super(ConsultaConteinerMock.class, metadata);
    }

}

