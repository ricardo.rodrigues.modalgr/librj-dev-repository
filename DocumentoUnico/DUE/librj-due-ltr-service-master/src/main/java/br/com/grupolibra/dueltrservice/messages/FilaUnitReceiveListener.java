package br.com.grupolibra.dueltrservice.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.RecepcaoService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;

@Component
public class FilaUnitReceiveListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

//	private static final String FILA_RECEPCAO_NFE = "due-ltr-service.recepcao-nfe-due.queue";
	private static final String FILA_RECEPCAO_NFE = "due-ltr-service.recepcao-nfe.queue";

	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private CctService cctService;
	
	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private CCTRepository cctRepository;

	/**
	 * Listener da fila de Danfe Ao receber um evento UNIT_RECEIVE será postado na fila a gkey da unit para envio de recepção para siscomex
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_RECEPCAO_NFE)
	public void processaRecepcaoNFEDUE(final String unitGkey) {
		logger.info("###### INICIO RECEPCAO POR NFE/DUE");
		logger.info("processaRecepcaoNFEDUE - gKey UNIT_RECEIVE : {} ", unitGkey);
		
		try {
			if(!n4Repository.isConteinerExportacao(unitGkey)) {
				logger.info("processaRecepcaoNFEDUE - Nao e conteiner exportacao");
				return;
			}
			if(n4Repository.hasDanfe(unitGkey)) {
				processaRecepcaoNFE(unitGkey);
			} else if(n4Repository.hasDUE(unitGkey)) {
				processaRecepcaoDUE(unitGkey);
			}
		} catch (Exception e) {
			logger.info("ERRO : {}", e.getMessage());
		}
	}

	private void processaRecepcaoNFE(final String unitGkey) {
		logger.info("processaRecepcaoNFE");
		Cct cct = cctService.createCctRecepcaoNFE(unitGkey);
		if (cct != null) {
			RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFE(cct);
			siscomexService.sendRecepcaoNFE(xml, cct);
		} else {
			logger.info("processaRecepcaoNFE - UNIT com pendencia para envio de recepcao");
		}
	}
	
	private void processaRecepcaoDUE(final String unitGkey) {
		logger.info("processaRecepcaoDUE");
		Cct cct = cctService.createCctRecepcaoDUE(unitGkey);
		if (cct != null) {
			RecepcoesConteineres xml = recepcaoService.getXMLRecepcaoDUE(cct);
			siscomexService.sendRecepcaoConteiner(xml, cct);
			
			boolean isAlterado = n4Repository.processaExportador(cct);
			if(isAlterado) {
	//			cctRepository.save(cct);
				cctService.save(cct);
			}
			
		} else {
			logger.info("processaRecepcaoNFE - UNIT com pendencia para envio de recepcao");
		}
	}
}
