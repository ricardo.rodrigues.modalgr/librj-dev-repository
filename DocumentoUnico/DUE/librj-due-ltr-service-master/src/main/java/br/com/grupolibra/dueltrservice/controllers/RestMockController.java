package br.com.grupolibra.dueltrservice.controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueltrservice.model.mock.ConsultaConteinerMock;
import br.com.grupolibra.dueltrservice.model.mock.DadosResumidosMock;
import br.com.grupolibra.dueltrservice.model.mock.ExportadorMock;
import br.com.grupolibra.dueltrservice.repository.ConsultaConteinerMockRepository;
import br.com.grupolibra.dueltrservice.repository.DadosResumidosMockRepository;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestMockController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String NR_RUC = "nrRUC";

	private static final String NR_DUE = "nrDUE";

	private static final String NR_CONTEINER = "nrConteiner";
	
	private static final String LISTA_RETORNO = "listaRetorno";

	@Autowired
	private ConsultaConteinerMockRepository consultaConteinerMockRepository;
	
	@Autowired
	private DadosResumidosMockRepository dadosResumidosMockRepository;
	
	@CrossOrigin
	@RequestMapping(value = "/consultaconteiner", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HashMap<String, Object> consultaConteinerMock(@RequestParam(value = "nrConteiner", required = true) List<String> conteineres) {
		logger.info("consultaConteinerMock");
		HashMap<String, Object> retorno = new HashMap<>();
		List<HashMap<String, Object>> resultList = new ArrayList<>();
		
		for (String conteiner : conteineres) {
			HashMap<String, Object> result = new HashMap<>();
			result.put(NR_CONTEINER, conteiner);
			ConsultaConteinerMock consultaConteinerMock = consultaConteinerMockRepository.findByNrConteiner(conteiner);
			if(consultaConteinerMock != null && consultaConteinerMock.getNrDUE() != null) {
				result.put(NR_DUE, consultaConteinerMock.getNrDUE());
//				result.put(NR_RUC, nrRuc);
				resultList.add(result);
			}
		}
		retorno.put(LISTA_RETORNO, resultList);
		return retorno;
	}
	
//	@CrossOrigin
//	@RequestMapping(value = "/cadastrardue", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String cadastrardue(@RequestBody(required = true) ConsultaConteinerMock consultaConteinerMock) {
//		logger.info("cadastrardue");
//		consultaConteinerMockRepository.save(consultaConteinerMock);
//		return "OK";
//	}
//	
//	@CrossOrigin
//	@RequestMapping(value = "/deletardue", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public String deletardue(@RequestBody(required = true) ConsultaConteinerMock consultaConteinerMock) {
//		logger.info("deletardue");
//		ConsultaConteinerMock mock = consultaConteinerMockRepository.findByNrConteiner(consultaConteinerMock.getNrConteiner());
//		consultaConteinerMockRepository.delete(mock);
//		return "OK";
//	}
	

	/*@CrossOrigin
	@RequestMapping(value = "/consultadadosresumidos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidosMock(@RequestParam(value = "numeroDUE", required = true) List<String> dues) {
		logger.info("consultaConteinerMock");
		ConsultaDadosResumidosResponseListDTO retorno = new ConsultaDadosResumidosResponseListDTO();
		List<ConsultaDadosResumidosResponseDTO> retornoList = new ArrayList<>();
		
		for (String due : dues) {
			DadosResumidosMock mock = dadosResumidosMockRepository.findByNumeroDUE(due);
			if(mock != null) { 
				ConsultaDadosResumidosResponseDTO dto = preencheRetornoMock(mock);
				retornoList.add(dto);
			}
		}
		retorno.setListaRetorno(retornoList);
		return retorno;
	}*/
	
	@Autowired
	private SiscomexService SiscomexService;
	
	@CrossOrigin
	@RequestMapping(value = "/consultadadosresumidos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(
			@RequestParam(value = "numeroDUE", required = true) List<String> dues) {
		//return SiscomexService.consultaDadosResumidosLst(dues);
		
		//TODO PRD
		ConsultaDadosResumidosResponseListDTO retorno = new ConsultaDadosResumidosResponseListDTO();
		List<ConsultaDadosResumidosResponseDTO> resultList = new ArrayList<>();
		
		for (String due : dues) {
			consultaDadosResumidosPorDUE(resultList, due);
		}
		retorno.setListaRetorno(resultList);
		return retorno;		
	}
	

	@Autowired
	private RestTemplate rest;
	
	private void consultaDadosResumidosPorDUE(List<ConsultaDadosResumidosResponseDTO> resultList, String due) {
		
		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultadadosresumidos");
		
		builder.queryParam("numeroDUE", due);
		
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ConsultaDadosResumidosResponseListDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity,
				ConsultaDadosResumidosResponseListDTO.class);
		ConsultaDadosResumidosResponseListDTO response = responseEntity.getBody();
					
		//resultList.add(response);
	}
	
	private HttpHeaders getHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	private ConsultaDadosResumidosResponseDTO preencheRetornoMock(DadosResumidosMock mock) {
		logger.info("preencheRetornoMock");
		logger.info("preencheRetornoMock - MOCK - DUE : {} ", mock.getNumeroDUE() );
		ConsultaDadosResumidosResponseDTO dto = new ConsultaDadosResumidosResponseDTO();
		dto.setNumeroDUE(mock.getNumeroDUE());
		dto.setIndicadorBloqueio(mock.getIndicadorBloqueio());
		dto.setSituacaoDUE(mock.getSituacaoDUE());
		List<br.com.grupolibra.dueltrservice.dto.Exportador> expList = new ArrayList<>();
		for(ExportadorMock e : mock.getExportadores()) {
			logger.info("preencheRetornoMock - MOCK - EXPORTADOR : {} ", e.getNumero() );
			
			br.com.grupolibra.dueltrservice.dto.Exportador x = new br.com.grupolibra.dueltrservice.dto.Exportador();
			x.setNumero(e.getNumero());
			x.setTipo(e.getTipo());
			expList.add(x);
		}
		dto.setExportadores(expList);
		return dto;
	}
	
	
}