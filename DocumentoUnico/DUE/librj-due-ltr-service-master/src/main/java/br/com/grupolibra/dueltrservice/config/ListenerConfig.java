package br.com.grupolibra.dueltrservice.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

import br.com.grupolibra.dueltrservice.messages.FilaUnitReceiveListener;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@EnableJms
@Configuration
public class ListenerConfig {

	@Bean
	public ActiveMQConnectionFactory activeMQConnectionFactory() {
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
		activeMQConnectionFactory.setBrokerURL(PropertiesUtil.getProp().getProperty("activemq.broker-url"));
		return activeMQConnectionFactory;
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(activeMQConnectionFactory());
		factory.setConcurrency("3-10");
		return factory;
	}

	@Bean
	public FilaUnitReceiveListener receiver() {
		return new FilaUnitReceiveListener();
	}
}
