package br.com.grupolibra.dueltrservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;

@Repository
public interface VinculoRepresentanteRepository extends CrudRepository<VinculoRepresentante, Long>, QuerydslPredicateExecutor<VinculoRepresentante> {
	
	public List<VinculoRepresentante> findByCodigo(String codigo);
	
}