package br.com.grupolibra.dueltrservice.service;

import br.com.grupolibra.dueltrservice.dto.ClienteFinanceiroPagadorDTO;

public interface ClienteFinanceiroPagadorService {

	public ClienteFinanceiroPagadorDTO salvaListaCFP(ClienteFinanceiroPagadorDTO dto);

	public int validaCheckObrigatorio();

	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(String bookingNbr, String lineOperatorGkey, String despachante);

}
