package br.com.grupolibra.dueltrservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMonitoramento is a Querydsl query type for Monitoramento
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMonitoramento extends EntityPathBase<Monitoramento> {

    private static final long serialVersionUID = -2068209794L;

    public static final QMonitoramento monitoramento = new QMonitoramento("monitoramento");

    public final BooleanPath bloqueado = createBoolean("bloqueado");

    public final StringPath booking = createString("booking");

    public final StringPath conteiner = createString("conteiner");

    public final StringPath danfesString = createString("danfesString");

    public final DateTimePath<java.util.Date> dataEntrada = createDateTime("dataEntrada", java.util.Date.class);

    public final DateTimePath<java.util.Date> dataProcessamento = createDateTime("dataProcessamento", java.util.Date.class);

    public final StringPath duesString = createString("duesString");

    public final StringPath erro = createString("erro");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lineOp = createString("lineOp");

    public final StringPath mensagem = createString("mensagem");

    public final StringPath navio = createString("navio");

    public final BooleanPath ok = createBoolean("ok");

    public final StringPath situacao = createString("situacao");

    public final StringPath status = createString("status");

    public final StringPath visit = createString("visit");

    public QMonitoramento(String variable) {
        super(Monitoramento.class, forVariable(variable));
    }

    public QMonitoramento(Path<? extends Monitoramento> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMonitoramento(PathMetadata metadata) {
        super(Monitoramento.class, metadata);
    }

}

