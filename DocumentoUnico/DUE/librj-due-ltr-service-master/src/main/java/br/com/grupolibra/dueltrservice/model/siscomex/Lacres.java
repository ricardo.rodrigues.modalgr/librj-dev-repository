package br.com.grupolibra.dueltrservice.model.siscomex;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("lacres")
public class Lacres {

	@XStreamImplicit(itemFieldName = "lacre")
	List<String> lacres;

	public Lacres() {
		this.lacres = new ArrayList<>();
	}

	public List<String> getLacres() {
		return lacres;
	}

	public void setLacres(List<String> lacres) {

		this.lacres = lacres;
	}

}
