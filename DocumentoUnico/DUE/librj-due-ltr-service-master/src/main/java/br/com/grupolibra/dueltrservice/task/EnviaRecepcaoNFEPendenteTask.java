package br.com.grupolibra.dueltrservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.RecepcaoService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class EnviaRecepcaoNFEPendenteTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void enviaRecepcaoNFEDUEPendente() {
		logger.info("###### INICIO TASK ENVIO RECEPCAO POR NFE / DUE PENDENTE");
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}

		List<Cct> cctListRecepcaoPendente = cctService.getEnvioRecepcaoNFEPendente();
		if (cctListRecepcaoPendente != null && !cctListRecepcaoPendente.isEmpty()) {

			logger.info("enviaRecepcaoPendente NFE - qtd registros recepcao pendente : {} ", cctListRecepcaoPendente.size());
			List<Cct> cctListRecepcaoPendenteCorrigidos = n4Repository.validaCorrecaoPendenciasRecepcaoNFE(cctListRecepcaoPendente);
			
			if (!cctListRecepcaoPendenteCorrigidos.isEmpty()) {
				logger.info("enviaRecepcaoPendente NFE - qtd registros recepcao pendente corrigidos : {} ", cctListRecepcaoPendenteCorrigidos.size());
				for (Cct cct : cctListRecepcaoPendenteCorrigidos) {
					try {
						RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFE(cct);
						siscomexService.sendRecepcaoNFE(xml, cct);
					} catch (Exception e) {
						logger.info("enviaRecepcaoPendente NFE - ERRO: " + e.toString());
						logger.info("enviaRecepcaoPendente NFE - ERRO - cct: " + cct == null ? "null" : cct.toString());
						e.printStackTrace();
					}
				}
			} else {
				logger.info("enviaRecepcaoPendente NFE - sem correcao recepcao pendente");
			}
		} else {
			logger.info("enviaRecepcaoPendente NFE - sem registro recepcao pendente");
		}

		cctListRecepcaoPendente = cctService.getEnvioRecepcaoDUEPendente();
		if (cctListRecepcaoPendente != null && !cctListRecepcaoPendente.isEmpty()) {

			logger.info("enviaRecepcaoPendente DUE - qtd registros recepcao pendente : {} ", cctListRecepcaoPendente.size());
			List<Cct> cctListRecepcaoPendenteCorrigidos = n4Repository.validaCorrecaoPendenciasRecepcaoDUE(cctListRecepcaoPendente);
	
			if (!cctListRecepcaoPendenteCorrigidos.isEmpty()) {
	
				logger.info("enviaRecepcaoPendente DUE - qtd registros recepcao pendente corrigidos : {} ", cctListRecepcaoPendenteCorrigidos.size());
	
				for (Cct cct : cctListRecepcaoPendenteCorrigidos) {
					RecepcoesConteineres xml = recepcaoService.getXMLRecepcaoDUE(cct);
					siscomexService.sendRecepcaoConteiner(xml, cct);
				}
			} else {
				logger.info("enviaRecepcaoPendente DUE - sem correcao recepcao pendente");
			}
		} else {
			logger.info("enviaRecepcaoPendente DUE - sem registro recepcao pendente");
		}
	}
}
