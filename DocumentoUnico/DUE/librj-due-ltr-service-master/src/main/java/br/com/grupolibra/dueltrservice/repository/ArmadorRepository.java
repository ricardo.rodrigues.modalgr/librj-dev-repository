package br.com.grupolibra.dueltrservice.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.Armador;

@Repository
public interface ArmadorRepository extends CrudRepository<Armador, Long>, QuerydslPredicateExecutor<Armador> {
	
	public Optional<Armador> findByIdExterno(BigDecimal idExterno);
	
}