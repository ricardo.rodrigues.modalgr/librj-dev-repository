package br.com.grupolibra.dueltrservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.RecepcaoService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.ParadaProgramadaUtil;

@Component
public class EnviaRecepcaoTransbordoMaritimoTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;
	
	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void enviaRecepcaoTransbordoMaritimo() {
		logger.info("###### INICIO TASK ENVIO RECEPCAO POR TRANSBORDO MARITIMO");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
//		logger.info("enviaRecepcaoTransbordoMaritimo");
		List<Cct> cctListRecepcaoTransbordoMaritimo = cctService.getEnvioRecepcaoTransbordoMaritimo();
		if (cctListRecepcaoTransbordoMaritimo != null && !cctListRecepcaoTransbordoMaritimo.isEmpty()) {
			
//			logger.info("enviaRecepcaoTransbordoMaritimo - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd : {} ", cctListRecepcaoTransbordoMaritimoAux.size());
//			List<Cct> cctListRecepcaoTransbordoMaritimo = n4Repository.validaUnitFacilityVisitStateActive(cctListRecepcaoTransbordoMaritimoAux);	
			
			logger.info("enviaRecepcaoTransbordoMaritimo - qtd registros recepcao transbordo maritimo : {} ", cctListRecepcaoTransbordoMaritimo.size());
			for (Cct cct : cctListRecepcaoTransbordoMaritimo) {
				RecepcoesConteineres xml = recepcaoService.getXMLRecepcaoConteinerTransbordoMaritimo(cct);
				siscomexService.sendRecepcaoConteiner(xml, cct);
			}

		} else {
			logger.info("enviaRecepcaoTransbordoMaritimo - sem registro recepcao transbordo maitimo");
		}
	}
}
