package br.com.grupolibra.dueltrservice.service.impl;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.thoughtworks.xstream.XStream;

import br.com.grupolibra.dueltrservice.dto.ConsultaConteineresResponseDTO;
import br.com.grupolibra.dueltrservice.dto.ConsultaConteineresResponseListDTO;
import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.dueltrservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueltrservice.dto.EntregaConteinerResponseDTO;
import br.com.grupolibra.dueltrservice.dto.RecepcaoResponseDTO;
import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.mock.ConsultaConteinerMock;
import br.com.grupolibra.dueltrservice.model.mock.DadosResumidosMock;
import br.com.grupolibra.dueltrservice.model.mock.ExportadorMock;
import br.com.grupolibra.dueltrservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueltrservice.repository.ConsultaConteinerMockRepository;
import br.com.grupolibra.dueltrservice.repository.DadosResumidosMockRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.CctService;
import br.com.grupolibra.dueltrservice.service.SiscomexService;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Service
public class SiscomexServiceImpl implements SiscomexService {

	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestTemplate rest;

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private DadosResumidosMockRepository dadosResumidosMockRepository;
	
	@Autowired
	private ConsultaConteinerMockRepository consultaConteinerMockRepository;
	
	@Autowired
	private CctService cctService;

	@Override
	public void sendRecepcaoNFE(RecepcoesNFE recepcoesNFE, Cct cct) {
		logger.info("sendRecepcaoNFE - Enviando recepcão NFE ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(recepcoesNFE);

//		logger.info("Body: {} ", body);
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("RECEPCAO NFE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);
		try {
			String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");
//			logger.info("URL: {} ", url);
			ResponseEntity<RecepcaoResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, RecepcaoResponseDTO.class);
			processResponseRecepcaoNFE(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
			logger.error("Fim da recepcão ao siscomex. HttpStatusCode Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. ResourceAccess Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. Generic Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		}
	}
	
	private void processResponseRecepcaoNFE(Cct cct, ResponseEntity<RecepcaoResponseDTO> responseEntity) {
		logger.info("processResponseRecepcaoNFE - CONTEINER: {} - {} ", cct.getNumeroCont(), cct.getIdExterno());
		RecepcaoResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ", response.getErro());
			
			if(response.getErro() != null && !response.getErro().isEmpty() && response.getErro().contains("parada programada")) {
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setErro(Cct.STATUS_ERRO_PARADA_PROGRAMADA);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			} else {
				if (response.getMensagem().isEmpty()) {
					cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
					countEnvio(cct);
				} else {
					processResponseErrorSiscomexMS(cct, response.getMensagem());
				}
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_RECEPCIONADO_OK);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				List<String> danfeList = new ArrayList<>();
				cct.getDanfes().forEach(danfe -> danfeList.add(danfe.getChave()));
				String notes = "DANFE : " + String.join(", DANFE : ", danfeList);
				String retorno = n4Repository.createRecepcaoNFEEvent(cct.getIdExterno(), notes);
				logger.info("Retorno createRecepcaoEvent : {} ", retorno);
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO RECEPCAO: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro("ERRO CRIANDO EVENTO RECEPCAO - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}

	private String limitaQuantidadeCaracteresBD(String msg) {
		if(msg != null) {
			if(msg.length() >= 4000) {
				return msg.substring(0,  3000) + " ....";
			} else {
				return msg;
			}
		} else {
			return null;
		}
	}

	@Override
	public List<Cct> consultaDUEPorConteiner(List<Cct> cctListConsultaCadastroDUE) {
//		logger.info("consultaDUEPorConteiner");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultaconteiner");
		for (Cct cct : cctListConsultaCadastroDUE) {
			builder.queryParam("nrConteiner", cct.getNumeroCont());
		}
		URI url = builder.build().encode().toUri();
		try {
			List<Cct> listaRetorno = new ArrayList<>();
			ResponseEntity<ConsultaConteineresResponseListDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaConteineresResponseListDTO.class);
			ConsultaConteineresResponseListDTO responseDTO = responseEntity.getBody();
			if (!Boolean.valueOf(responseDTO.isFlag()) && responseDTO.getListaRetorno() != null && !responseDTO.getListaRetorno().isEmpty()) {
				listaRetorno = processResponseConsultaDUE(cctListConsultaCadastroDUE, responseDTO);
			}
			logger.info("consultaDUEPorConteiner - qtd registro com cadastro de DUE : {} ", listaRetorno.size());
			return listaRetorno;
		} catch(Exception e) {
			logger.error("SISCOMEX ERROR - consultaDUEPorConteiner : {} ", e.getMessage());
			return Collections.emptyList();
		}
	}
	
	public List<Cct> consultaDUEPorConteinerMOCK(List<Cct> cctListConsultaCadastroDUE) {
		ConsultaConteineresResponseListDTO dto = new ConsultaConteineresResponseListDTO();
		List<ConsultaConteineresResponseDTO> retornoList = new ArrayList<>();
		for (Cct cct : cctListConsultaCadastroDUE) {
			ConsultaConteinerMock mock = consultaConteinerMockRepository.findByNrConteiner(cct.getNumeroCont());
			if(mock != null) {
				ConsultaConteineresResponseDTO retorno = new ConsultaConteineresResponseDTO();
				retorno.setNrConteiner(mock.getNrConteiner());
				retorno.setNrDUE(mock.getNrDUE());
				retornoList.add(retorno);
			}
		}
		dto.setListaRetorno(retornoList);
		
		List<Cct> listaRetorno = new ArrayList<>();
		listaRetorno = processResponseConsultaDUE(cctListConsultaCadastroDUE, dto);
		return listaRetorno;
	}

	private List<Cct> processResponseConsultaDUE(List<Cct> cctListConsultaCadastroDUE, ConsultaConteineresResponseListDTO responseDTO) {
//		logger.info("processResponseConsultaDUE");
		for (Cct cct : cctListConsultaCadastroDUE) {			
			for (ConsultaConteineresResponseDTO response : responseDTO.getListaRetorno()) {
				if (response.getNrConteiner().equals(cct.getNumeroCont())) {
					if (response.getNrDUE() != null && !"".equals(response.getNrDUE())) {
//						logger.info("processResponseConsultaDUE - RESPONSE DUE {} ", response.getNrDUE());
						if(cct.getNrodue() != null && !"".equals(cct.getNrodue())) {
							cct.setNrodue(cct.getNrodue() + ", " + response.getNrDUE());
						} else {
							cct.setNrodue(response.getNrDUE());
						}
					}
				}
			}
		}
		List<Cct> listaRetorno = new ArrayList<>();
		for (Cct cct : cctListConsultaCadastroDUE) {
			if(cct.getNrodue() != null && !"".equals(cct.getNrodue())) {
				listaRetorno.add(cct);
			}
		}
		logger.info("processResponseConsultaDUE - QTD Atualizar DUE : {} ", listaRetorno.size());
		return listaRetorno;
	}

	@Override
	public void sendEntregaConteiner(EntregasConteineres entregasConteineres, Cct cct) {
		logger.info("sendEntregaConteiner - Enviando entrega conteiner ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(entregasConteineres);

//		logger.info("Body: {} ", body);
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("ENTREGA NFE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);

		String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		try {
			ResponseEntity<EntregaConteinerResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, EntregaConteinerResponseDTO.class);
			processResponseEntregaConteier(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. ResourceAccess Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. Generic Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		}
	}

	private void processResponseEntregaConteier(Cct cct, ResponseEntity<EntregaConteinerResponseDTO> responseEntity) {
		logger.info("processResponseEntregaConteier - CONTEINER: {} - {} ", cct.getNumeroCont(), cct.getIdExterno());
		EntregaConteinerResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ",response.getErro());

			if (response.getMensagem().isEmpty()) {
				cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
				countEnvio(cct);
			} else {
				processResponseErrorSiscomexMS(cct, response.getMensagem());
			}
			cct.setStatus(Cct.STATUS_ERRO);
			cct.setStatusProc(Cct.STATUS_ERRO_ENTREGA);
			cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//			cctRepository.save(cct);
			cctService.save(cct);
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_ENTREGA_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_ENTREGUE);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				String notes = "DUE : " + cct.getNrodue();
				String retorno = n4Repository.createEntregaEvent(cct, notes);
				logger.info("Retorno createEntregaEvent : {} ", retorno);				
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO ENTREGA: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_ENTREGA);
				cct.setErro("ERRO CRIANDO EVENTO ENTREGA - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}

	private void processResponseErrorSiscomexMS(Cct cct, String responseMsg) {
		if(Cct.ERRO_INTERNO_SERVIDOR.equals(responseMsg) || Cct.ERRO_TIMEOUT.equals(responseMsg) || 
				Cct.SERVICO_INDISPONIVEL.equals(responseMsg) || Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.equals(responseMsg) || 
				Cct.ERRO_ENVIO_RECEITA.equals(responseMsg)) {
			cct.setMensagem(responseMsg);
		} else {
			cct.setMensagem(responseMsg);
			countEnvio(cct);
		} 
	}

	private void processHttpStatusCodeException(Cct cct, HttpStatusCodeException e) {
		logger.info("processHttpStatusCodeException - STATUS CODE : {} ", e.getStatusCode());
		HttpStatus statusCode = e.getStatusCode();
		if (statusCode == HttpStatus.BAD_REQUEST) {
			cct.setMensagem(Cct.REQUISICAO_MAL_FORMATADA.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
			countEnvio(cct);
		} else if (statusCode == HttpStatus.UNAUTHORIZED) {
			cct.setMensagem(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			countEnvio(cct);
		} else if (statusCode == HttpStatus.FORBIDDEN) {
			cct.setMensagem(Cct.REQUISICAO_NAO_AUTORIZADA.toString());
			countEnvio(cct);
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
		} else if (statusCode == HttpStatus.NOT_FOUND) {
			cct.setMensagem(Cct.REGISTRO_NAO_ENCONTRADO.toString());
			countEnvio(cct);
		} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
			cct.setMensagem(Cct.ERRO_NEGOCIO.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
			countEnvio(cct);
		} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
		} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
			cct.setMensagem(Cct.SERVICO_INDISPONIVEL.toString());
		}
		cct.setStatus("Erro");
	}		
	
	private HttpHeaders getHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	@Override
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos) {
		logger.info("consultaDadosResumidos");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultadadosresumidos");
		for (Cct cct : cctListConsultaDadosResumidos) {
			if(cct.getNrodue().contains(",")) {
				String[] dues = cct.getNrodue().split(",");
				for (String due : dues) {
					due = due.replaceAll(" ", "");
					due = due.trim();
					builder.queryParam("numeroDUE", due);
				}
			} else {
				builder.queryParam("numeroDUE", cct.getNrodue());
			}
		}
		URI url = builder.build().encode().toUri();
		logger.info("consultaDadosResumidos - URL: {} ", url.toString());
		try {
			ResponseEntity<ConsultaDadosResumidosResponseListDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaDadosResumidosResponseListDTO.class);
			ConsultaDadosResumidosResponseListDTO responseDTO = responseEntity.getBody();
			if (responseDTO.getListaRetorno() != null && !responseDTO.getListaRetorno().isEmpty()) {
				return responseDTO;
			}
		} catch(Exception e) {
			logger.error("SISCOMEX ERROR - consultaDadosResumidos : {} ", e.getMessage());
		}
		return null;
	}
	
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidosMOCK(List<Cct> cctListConsultaDadosResumidos) {
//		logger.info("consultaDadosResumidosMOCK");		
		
		ConsultaDadosResumidosResponseListDTO retorno = new ConsultaDadosResumidosResponseListDTO();
		List<ConsultaDadosResumidosResponseDTO> retornoList = new ArrayList<>();
		
		for (Cct cct : cctListConsultaDadosResumidos) {
			logger.info("consultaDadosResumidosMOCK - CONTEINER {} - DUE {} ", cct.getNumeroCont(), cct.getNrodue());		
			
			if(cct.getNrodue().contains(",")) {
				String[] dues = cct.getNrodue().split(",");
				for (String due : dues) {
					due = due.replaceAll(" ", "");
					due = due.trim();
					DadosResumidosMock mock = dadosResumidosMockRepository.findByNumeroDUE(due);
					if(mock != null) { 
						ConsultaDadosResumidosResponseDTO dto = preencheRetornoMock(mock);
						retornoList.add(dto);
					} else {
						logger.info("consultaDadosResumidosMOCK - MOCK NULL");
					}
				}
			} else {
				DadosResumidosMock mock = dadosResumidosMockRepository.findByNumeroDUE(cct.getNrodue());
				if(mock != null) { 
					ConsultaDadosResumidosResponseDTO dto = preencheRetornoMock(mock);
					retornoList.add(dto);
				} else {
					logger.info("consultaDadosResumidosMOCK - MOCK NULL");
				}
			}
		}
		retorno.setListaRetorno(retornoList);
		return retorno;
	}

	private ConsultaDadosResumidosResponseDTO preencheRetornoMock(DadosResumidosMock mock) {
//		logger.info("preencheRetornoMock");
//		logger.info("preencheRetornoMock - MOCK - DUE : {} ", mock.getNumeroDUE() );
		ConsultaDadosResumidosResponseDTO dto = new ConsultaDadosResumidosResponseDTO();
		dto.setNumeroDUE(mock.getNumeroDUE());
		dto.setIndicadorBloqueio(mock.getIndicadorBloqueio());
		dto.setSituacaoDUE(mock.getSituacaoDUE());
		List<br.com.grupolibra.dueltrservice.dto.Exportador> expList = new ArrayList<>();
		for(ExportadorMock e : mock.getExportadores()) {
//			logger.info("preencheRetornoMock - MOCK - EXPORTADOR : {} ", e.getNumero() );
			
			br.com.grupolibra.dueltrservice.dto.Exportador x = new br.com.grupolibra.dueltrservice.dto.Exportador();
			x.setNumero(e.getNumero());
			x.setTipo(e.getTipo());
			expList.add(x);
		}
		dto.setExportadores(expList);
		return dto;
	}
	
	private void countEnvio(Cct cct) {
//		logger.info("countEnvio");
		if(cct.getContadorEnvio() == null) {
			cct.setContadorEnvio(1L);
			return;
		} else {
			cct.setContadorEnvio(cct.getContadorEnvio() + 1L);
		}
	}

	@Override
	public void sendRecepcaoConteiner(RecepcoesConteineres recepcoesConteineres, Cct cct) {
		logger.info("sendRecepcaoConteiner - Enviando recepcão por Conteiner ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(recepcoesConteineres);

//		logger.info("Body: {} ", body);
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("RECEPCAO CONTEINER " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);
		try {
			String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");
//			logger.info("URL: {} ", url);
			ResponseEntity<RecepcaoResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, RecepcaoResponseDTO.class);
			processResponseRecepcaoConteiner(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. ResourceAccess Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. Generic Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		}		
	}
	
	private void processResponseRecepcaoConteiner(Cct cct, ResponseEntity<RecepcaoResponseDTO> responseEntity) {
		logger.info("processResponseRecepcaoConteiner - CONTEINER: {} - {} ", cct.getNumeroCont(), cct.getIdExterno());
		RecepcaoResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ", response.getErro());
			
			if(response.getErro() != null && !response.getErro().isEmpty() && response.getErro().contains("parada programada")) {
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setErro(Cct.STATUS_ERRO_PARADA_PROGRAMADA);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			} else {
			
				if (response.getMensagem().isEmpty()) {
					cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
					countEnvio(cct);
				} else {
					processResponseErrorSiscomexMS(cct, response.getMensagem());
				}
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_RECEPCIONADO_OK);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				String notes = "DUE : " + cct.getNrodue();
				String retorno = n4Repository.createRecepcaoConteinerEvent(cct, notes);
				logger.info("Retorno createRecepcaoEvent : {} ", retorno);
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO RECEPCAO: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro("ERRO CRIANDO EVENTO RECEPCAO - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}
}
