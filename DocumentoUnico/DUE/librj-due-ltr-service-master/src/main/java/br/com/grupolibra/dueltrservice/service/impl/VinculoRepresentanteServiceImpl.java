package br.com.grupolibra.dueltrservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueltrservice.dto.VinculoRepresentanteDTO;
import br.com.grupolibra.dueltrservice.model.QVinculoRepresentante;
import br.com.grupolibra.dueltrservice.model.VinculoRepresentante;
import br.com.grupolibra.dueltrservice.repository.VinculoRepresentanteRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.VinculoRepresentanteService;

@Service
public class VinculoRepresentanteServiceImpl implements VinculoRepresentanteService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private VinculoRepresentanteRepository vinculoRepresentanteRepository;

	@Autowired
	private N4Repository n4Repository;

	@Override
	public VinculoRepresentanteDTO findAllVinculo(String searchText, String pageIndex, String pageSize) {
		logger.info("findAllVinculo SERVICE");
		
//		VinculoRepresentanteDTO retorno = new VinculoRepresentanteDTO();
//		retorno.setCurrentPage(Integer.parseInt(pageIndex));
//		Page<VinculoRepresentante> listaVinculo = null;
//		
//		if(searchText != null && !"".equals(searchText)) {
//			
//			logger.info("findAllVinculo - has searchText: {} ", searchText);
//			
//			QVinculoRepresentante qVinculo = QVinculoRepresentante.vinculoRepresentante;
//			BooleanBuilder builder = new BooleanBuilder();
//			
//			builder.or(qVinculo.codigo.likeIgnoreCase(searchText));
//			builder.or(qVinculo.nomeLineOp.likeIgnoreCase(searchText));
//			builder.or(qVinculo.cnpj.likeIgnoreCase(searchText));
//			builder.or(qVinculo.nomeRepresentante.likeIgnoreCase(searchText));
//			logger.info("findAllVinculo - QUERY : {} ", builder.toString());
//			
//			long numRegistros = vinculoRepresentanteRepository.count(builder.getValue());
//			//getQtdPaginas(pageSize, retorno, numRegistros);
//			retorno.setQtdRegistros(numRegistros);
//			
//			listaVinculo = vinculoRepresentanteRepository.findAll(builder.getValue(), PageRequest.of(Integer.parseInt(pageIndex)-1, Integer.parseInt(pageSize)));
//		} else {
//			
//			logger.info("findAllVinculo - no searchText");
//			
//			long numRegistros = vinculoRepresentanteRepository.count();
//			//getQtdPaginas(pageSize, retorno, numRegistros);
//			retorno.setQtdRegistros(numRegistros);
//			
//			listaVinculo = vinculoRepresentanteRepository.findAll(null, PageRequest.of(Integer.parseInt(pageIndex)-1, Integer.parseInt(pageSize), Sort.by(Order.asc("nomeLineOp"))));
//		}
//		
//		retorno.setListaVinculo(listaVinculo.getContent());
//		return retorno;
		
		return n4Repository.findAllVinculo(searchText, pageIndex, pageSize);
	}
	
	private void getQtdPaginas(String pageSize, VinculoRepresentanteDTO retorno, long numRegistros) {
		if(numRegistros > 0) {
			float resultFloat = (float)numRegistros / Integer.parseInt(pageSize);
			int result = 0;
			if(resultFloat > (int) resultFloat) {
				result = (int) resultFloat + 1;  
			} else {
				result = (int) resultFloat;
			}
			retorno.setQtdPaginas(result);
		} else {
			retorno.setQtdPaginas(0L);
		}
	}

	@Override
	public VinculoRepresentanteDTO findRepresentante(String cnpj, String nome) {
		logger.info("findRepresentante SERVICE");
		return n4Repository.findRepresentante(cnpj, nome);
	}

	@Override
	public VinculoRepresentanteDTO vincular(VinculoRepresentante vinculo) {
		logger.info("vincular SERVICE");
		VinculoRepresentanteDTO retorno = new VinculoRepresentanteDTO();
		try {
			vinculoRepresentanteRepository.save(vinculo);
			
			logger.info("vincular - SAVE OK");
			
			retorno.setErro(false);
			retorno.setMensagem("Representante nacional vinculado com sucesso.");
		} catch(Exception e) {
			logger.info("vincular - ERRO: {} ", e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem("Ocorreu um erro ao vincular o Representante nacional.");
		}
		return retorno;
	}
	
	@Override
	public VinculoRepresentanteDTO desvincular(VinculoRepresentante vinculo) {
		logger.info("desvincular SERVICE");
		VinculoRepresentanteDTO retorno = new VinculoRepresentanteDTO();
		try {
			
			vinculo.setCnpj(null);
			vinculo.setNomeRepresentante(null);
			
			vinculoRepresentanteRepository.save(vinculo);
			
			logger.info("vincular - SAVE OK");
			
			retorno.setErro(false);
			retorno.setMensagem("Representande nacional desvinculado com sucesso.");
		} catch(Exception e) {
			logger.info("vincular - ERRO: {} ", e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem("Ocorreu um erro ao desvincular o Representante nacional.");
		}
		return retorno;
	}

	
}
