package br.com.grupolibra.dueltrservice.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.model.Danfe;
import br.com.grupolibra.dueltrservice.model.siscomex.Conteiner;
import br.com.grupolibra.dueltrservice.model.siscomex.Entregador;
import br.com.grupolibra.dueltrservice.model.siscomex.Local;
import br.com.grupolibra.dueltrservice.model.siscomex.NotaFiscalEletronica;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcaoConteiner;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcaoNFE;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueltrservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueltrservice.model.siscomex.TVeiculoTransitoSimplificado;
import br.com.grupolibra.dueltrservice.model.siscomex.TransitoSimplificadoRecepcao;
import br.com.grupolibra.dueltrservice.model.siscomex.TransitoSimplificadoRecepcao.VeiculoRodoviario;
import br.com.grupolibra.dueltrservice.model.siscomex.TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos;
import br.com.grupolibra.dueltrservice.model.siscomex.Transportador;
import br.com.grupolibra.dueltrservice.repository.DanfeRepository;
import br.com.grupolibra.dueltrservice.repository.n4.N4Repository;
import br.com.grupolibra.dueltrservice.service.RecepcaoService;
import br.com.grupolibra.dueltrservice.util.LocalEnum;
import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Service
public class RecepcaoServiceImpl implements RecepcaoService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// @Value("${siscomex.cnpj-cert}")
	// private String cnpjCert;

	@Autowired
	private DanfeRepository danfeRepository;
	
	@Autowired
	private N4Repository n4Repository;

	@Override
	public RecepcoesNFE getXMLRecepcaoNFE(Cct cct) {
//		logger.info("getXMLRecepcaoNFE");
		cct.setDanfes(danfeRepository.findByCctId(cct.getId()));
		RecepcoesNFE recepcoesNFE = new RecepcoesNFE();
		RecepcaoNFE recepcaoNFE = new RecepcaoNFE();
		recepcaoNFE.setIdentificacaoRecepcao("RECNFE|" + cct.getNumeroCont() + new Date().getTime());
		recepcaoNFE.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		if (cct.getDivergenciasIdentificadas() != null && !cct.getDivergenciasIdentificadas().isEmpty()) {
			recepcaoNFE.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		}
		recepcaoNFE.setLocal(getLocal());
		List<Conteiner> conteineres = getListConteineres(cct, recepcaoNFE);
		setNFes(cct, conteineres, recepcaoNFE);
		setTransportador(cct, recepcaoNFE);
		recepcoesNFE.setRecepcaoNFE(recepcaoNFE);
		return recepcoesNFE;
	}

	private void setTransportador(Cct cct, RecepcaoNFE recepcaoNFE) {
//		logger.info("setTransportador");
		Transportador transportador = new Transportador();
		if (cct.getTipoTransportador() == 1) {
			transportador.setNomeEstrangeiro(cct.getNomeTransportador().substring(0, 59));
			transportador.setNomeCondutorEstrangeiro(cct.getNomeCondutor());
		} else {
			if (cct.getCnpjTransportador() != null && !cct.getCnpjTransportador().isEmpty()) {
				transportador.setCnpj(cct.getCnpjTransportador());
			}
			if (cct.getCpfTransportador() != null && !cct.getCpfTransportador().isEmpty()) {
				transportador.setCpf(cct.getCpfTransportador());
			}
			if (cct.getCpfCondutor() != null && !cct.getCpfCondutor().isEmpty()) {
				transportador.setCpfCondutor(cct.getCpfCondutor());
			}
		}
		recepcaoNFE.setTransportador(transportador);
	}

	private void setNFes(Cct cct, List<Conteiner> conteineres, RecepcaoNFE recepcaoNFE) {
//		logger.info("setNFes");
		List<NotaFiscalEletronica> notasFiscais = new ArrayList<>();
		for (Danfe danfe : cct.getDanfes()) {
			NotaFiscalEletronica nfe = new NotaFiscalEletronica();
			nfe.setChaveAcesso(danfe.getChave());
			nfe.setConteineres(conteineres);
			notasFiscais.add(nfe);
		}
		recepcaoNFE.setNotasFiscais(notasFiscais);
	}

	private Local getLocal() {
//		logger.info("setLocal");
		Local local = new Local();
		local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
		local.setCodigoURF(LocalEnum.PORTO_DE_RJ.value());
		return local;
	}

	private List<Conteiner> getListConteineres(Cct cct, RecepcaoNFE recepcaoNFE) {
//		logger.info("getListConteineres");
		List<Conteiner> conteineres = new ArrayList<>();
		Conteiner conteiner = new Conteiner();
		if (cct.getPesoBruto() == null && cct.getMotivoNaoPesagem() != null && !cct.getMotivoNaoPesagem().isEmpty()) {
			recepcaoNFE.setMotivoNaoPesagem(cct.getMotivoNaoPesagem());
		} else {
			if(cct.getPesoBruto() != null && cct.getPesoBruto().longValue() > 0L ) {
				recepcaoNFE.setPesoAferido(new BigDecimal(cct.getPesoBruto().toString()).setScale(3));
			} else {
				recepcaoNFE.setPesoAferido(new BigDecimal("0.001"));
			}
		}
		conteiner.setTara(new BigDecimal(cct.getTara() != null ? cct.getTara().toString() : "0").setScale(3));
		conteiner.setNumeroConteiner(cct.getNumeroCont());
		List<String> lacres = Arrays.asList(cct.getLacres().split(","));
		conteiner.setLacres(lacres);
		conteineres.add(conteiner);
		if (!StringUtils.isEmpty(cct.getAvaria())) {
			recepcaoNFE.setAvariasIdentificadas(cct.getAvaria());
		}
		return conteineres;
	}
	
	@Override
	public RecepcoesConteineres getXMLRecepcaoConteinerTransbordoMaritimo(Cct cct) {
		RecepcoesConteineres recepcoesConteineres = new RecepcoesConteineres();
		RecepcaoConteiner recepcaoConteiner = new RecepcaoConteiner();
		recepcaoConteiner.setIdentificacaoRecepcao("RECTRSHP|" + cct.getNumeroCont() + new Date().getTime());
		recepcaoConteiner.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		//recepcaoConteiner.setLocal(getLocal());
		
		Local local = new Local();
		local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
		recepcaoConteiner.setLocal(local);
		
		setEntregador(cct, recepcaoConteiner);
		setListConteineres(cct, recepcaoConteiner);
		if (cct.getDivergenciasIdentificadas() != null && !cct.getDivergenciasIdentificadas().isEmpty()) {
			recepcaoConteiner.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		}
		recepcoesConteineres.setRecepcaoConteiner(recepcaoConteiner);
		return recepcoesConteineres;
	}

	private void setEntregador(Cct cct, RecepcaoConteiner recepcaoConteiner) {
		Entregador entregador = new Entregador();
		if (cct.getEntregador() != null) {
			if (cct.getEntregador().getNome() != null) {
				entregador.setNomeEstrangeiro(cct.getEntregador().getNome().substring(0, 59));
			} else {
				
				if(!n4Repository.hasCustomRecintoNTE(cct)) {
					entregador.setCnpj(cct.getEntregador().getCnpj());
				} else {
					entregador.setCpf(cct.getCpfTransportador() != null && !"".equals(cct.getCpfTransportador()) ? cct.getCpfTransportador() : cct.getCpfCondutor() != null && !"".equals(cct.getCpfCondutor()) ? cct.getCpfCondutor() : "");
				}
			}
		}
		recepcaoConteiner.setEntregador(entregador);
	}
	
	private void setListConteineres(Cct cct, RecepcaoConteiner recepcaoConteiner) {
//		logger.info("getListConteineres");
		List<Conteiner> conteineres = new ArrayList<>();
		Conteiner conteiner = new Conteiner();
		if (cct.getPesoBruto() == null && cct.getMotivoNaoPesagem() != null && !cct.getMotivoNaoPesagem().isEmpty()) {
			conteiner.setMotivoNaoPesagem(cct.getMotivoNaoPesagem());
		} else {
			conteiner.setPesoAferido(new BigDecimal(cct.getPesoBruto() != null ? cct.getPesoBruto().toString() : "0").setScale(3));
		}
		conteiner.setTara(new BigDecimal(cct.getTara() != null ? cct.getTara().toString() : "0").setScale(3));
		conteiner.setNumeroConteiner(cct.getNumeroCont());
		List<String> lacres = Arrays.asList(cct.getLacres().split(","));
		conteiner.setLacres(lacres);
		conteineres.add(conteiner);
		if (!StringUtils.isEmpty(cct.getAvaria())) {
			recepcaoConteiner.setAvariasIdentificadas(cct.getAvaria());
		}
		recepcaoConteiner.setConteineres(conteineres);
	}
	
	@Override
	public RecepcoesConteineres getXMLRecepcaoDUE(Cct cct) {
		RecepcoesConteineres recepcoesConteineres = new RecepcoesConteineres();
		RecepcaoConteiner recepcaoConteiner = new RecepcaoConteiner();
		recepcaoConteiner.setIdentificacaoRecepcao("RECDUE|" + cct.getNumeroCont() + new Date().getTime());
		recepcaoConteiner.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		
//		recepcaoConteiner.setLocal(getLocal());
		Local local = new Local();
		local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
		recepcaoConteiner.setLocal(local);
		
		
		setEntregador(cct, recepcaoConteiner);
		
		setListConteineres(cct, recepcaoConteiner);
		if (cct.getDivergenciasIdentificadas() != null && !cct.getDivergenciasIdentificadas().isEmpty()) {
			recepcaoConteiner.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		}
		setVeiculoRodoviario(cct, recepcaoConteiner);
		recepcoesConteineres.setRecepcaoConteiner(recepcaoConteiner);
		return recepcoesConteineres;
	}

	private void setVeiculoRodoviario(Cct cct, RecepcaoConteiner recepcaoConteiner) {
		TransitoSimplificadoRecepcao transitoSimplificado = new TransitoSimplificadoRecepcao();
		VeiculoRodoviario veiculoRodoviario = new VeiculoRodoviario();
		if(cct.getTipoTransportador() == 1) {
			veiculoRodoviario.setNomeCondutorEstrangeiro(cct.getNomeCondutor());
			veiculoRodoviario.setDocumentoCondutorEstrangeiro(cct.getCpfCondutor());
		} else {
			veiculoRodoviario.setCpfCondutor(cct.getCpfCondutor());
		}
		Veiculos veiculos = new Veiculos();
		List<TVeiculoTransitoSimplificado> veiculosList = new ArrayList<>();
		TVeiculoTransitoSimplificado veiculo = new TVeiculoTransitoSimplificado();
		veiculo.setPlaca(cct.getPlacaveiculo());		
		
		veiculosList.add(veiculo);
		veiculos.getVeiculo().addAll(veiculosList);
		veiculoRodoviario.setVeiculos(veiculos);
		transitoSimplificado.setVeiculoRodoviario(veiculoRodoviario);
		recepcaoConteiner.setTransitoSimplificado(transitoSimplificado);
	}
	
}
