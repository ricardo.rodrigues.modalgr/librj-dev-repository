package br.com.grupolibra.dueltrservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueltrservice.model.Danfe;

@Repository
public interface DanfeRepository extends CrudRepository<Danfe, Long>, QuerydslPredicateExecutor<Danfe> {
	
	public List<Danfe> findByCctId(long id);
}