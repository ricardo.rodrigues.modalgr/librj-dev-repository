package br.com.grupolibra.dueltrservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaConteineresResponseListDTO {

	private List<ConsultaConteineresResponseDTO> listaRetorno;

	private boolean flag;

	private String erro;

	private String mensagem;
}
