package br.com.grupolibra.dueltrservice.model.siscomex;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transportador", propOrder = {
    "cnpj",
    "cpf",
    "nomeEstrangeiro",
    "cpfCondutor",
    "nomeCondutorEstrangeiro"
})
public class Transportador {

    protected String cnpj;
    protected String cpf;
    protected String nomeEstrangeiro;
    protected String cpfCondutor;
    protected String nomeCondutorEstrangeiro;

    /**
     * Obt�m o valor da propriedade cnpj.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnpj(String value) {
        this.cnpj = value;
    }

    /**
     * Obt�m o valor da propriedade cpf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Define o valor da propriedade cpf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpf(String value) {
        this.cpf = value;
    }

    /**
     * Obt�m o valor da propriedade nomeEstrangeiro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeEstrangeiro() {
        return nomeEstrangeiro;
    }

    /**
     * Define o valor da propriedade nomeEstrangeiro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeEstrangeiro(String value) {
        this.nomeEstrangeiro = value;
    }

    /**
     * Obt�m o valor da propriedade cpfCondutor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCondutor() {
        return cpfCondutor;
    }

    /**
     * Define o valor da propriedade cpfCondutor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCondutor(String value) {
        this.cpfCondutor = value;
    }

    /**
     * Obt�m o valor da propriedade nomeCondutorEstrangeiro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCondutorEstrangeiro() {
        return nomeCondutorEstrangeiro;
    }

    /**
     * Define o valor da propriedade nomeCondutorEstrangeiro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCondutorEstrangeiro(String value) {
        this.nomeCondutorEstrangeiro = value;
    }

}
