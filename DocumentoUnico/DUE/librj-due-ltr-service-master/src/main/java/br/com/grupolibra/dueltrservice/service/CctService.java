package br.com.grupolibra.dueltrservice.service;

import java.util.List;

import br.com.grupolibra.dueltrservice.model.Cct;

public interface CctService {
	
	Cct saveCctNFE(Cct cct);

	Cct createCctRecepcaoNFE(String unitGkey);
	
	List<Cct> getCctsNFEConsultaCadastroDUE();

	void updateNumeroDUE(List<Cct> resultList);
	
	List<Cct> getEnvioRecepcaoNFEPendente();
	
	List<Cct> getEnvioRecepcaoDUEPendente();
	
	List<Cct> getCctsEnviaEntregaConteiner(String categoria);
	
	List<Cct> getCctsConsultaDadosResumidos();
	
	void createCctTransbordoMaritimo(String unitGkey);
	
	List<Cct> getCctTransbordoMaritimoPendente();
	
	List<Cct> getCctsTransbordoMaritimoConsultaCadastroDUE();
	
	List<Cct> getEnvioRecepcaoTransbordoMaritimo();
	
	Cct createCctRecepcaoDUE(String unitGkey);
	
	List<Cct> getCctsExportadorPendente();

	Cct save(Cct cct);
	
	void saveMonitoramento(Cct cct);
	
	List<Cct> getAllCCT();
}
