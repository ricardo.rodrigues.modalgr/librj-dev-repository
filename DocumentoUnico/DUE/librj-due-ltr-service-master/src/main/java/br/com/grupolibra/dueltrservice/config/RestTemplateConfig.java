package br.com.grupolibra.dueltrservice.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import br.com.grupolibra.dueltrservice.util.PropertiesUtil;

@Configuration
public class RestTemplateConfig {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		int timeout = Integer.parseInt(PropertiesUtil.getProp().getProperty("siscomex.timeout"));
		return restTemplateBuilder.setConnectTimeout(timeout).setReadTimeout(timeout).build();
	}
}
