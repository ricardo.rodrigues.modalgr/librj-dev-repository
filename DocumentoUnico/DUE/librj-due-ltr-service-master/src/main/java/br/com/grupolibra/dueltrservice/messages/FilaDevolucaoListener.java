package br.com.grupolibra.dueltrservice.messages;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueltrservice.model.Cct;
import br.com.grupolibra.dueltrservice.repository.CCTRepository;
import br.com.grupolibra.dueltrservice.service.CctService;


@Component
public class FilaDevolucaoListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String FILA_DEVOLUCAO = "due-ltr-service.devolucao-conteiner.queue";

	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private CctService cctService;

	/**
	 * Listener da fila de TDevolução Ao receber um evento UNIT_OUT_GATE será postado na fila a gkey da unit para atualizacao base CCT
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_DEVOLUCAO)
	public void processaDevolucao(final String unitGkey) {
		logger.info("###### INICIO PROCESSO DEVOLUCAO");
		logger.info("processaDevolucao - gKey UNIT_OUT_GATE : {} ", unitGkey);
		try {
			List<Cct> cctList = cctRepository.findByIdExterno(new BigDecimal(unitGkey));
			if(cctList != null && !cctList.isEmpty())  {
				Cct cct = cctList.get(0);
				cct.setFinalizado(1);
				cct.setStatusProc(Cct.STATUS_DEVOLVIDO);
				cct.setErro("EVENT UNIT_OUT_GATE applied");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} catch (Exception e) {
			logger.info("ERRO : {}", e.getMessage());
		}
	}
}
