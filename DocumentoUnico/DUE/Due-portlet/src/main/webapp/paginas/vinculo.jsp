<div ng-controller="vinculoController">

    
    <div class="row-fluid">

        <div class="span3" style="margin-left:30px; float: left;">
            <input style="border-radius: 12px;" type="text" class="input-block-level" ng-model="searchText">
        </div>
        <div class="span1" style="margin-left:25px; float: left;">
	        <button ng-click="findAllVinculo()"
					style="height: 30px; margin-right: 10%; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 90px;"
					class="btn btn-primary">Pesquisar</button>
		</div>
		<div id="infoUpload" style="float: left; margin-right: 5px; margin-left: 25px;">
			<i class="fa fa-question-circle" title="Use a caixa de texto para filtrar qualquer dado da tabela abaixo."></i>
		</div>
		
		<div class="span4" style="float: right; margin-top: 15px;">
                <select class="span2" style="float: right;" ng-options="item as item.qtd for item in qtdporpaginalist track by item.id" ng-model="qtdporpagina" ng-change="changeqtdporpagina()"></select>
                <label style="float: right;">Itens por pagina</label>
        </div>

        <div class="span12">

            <table class="table table-bordered" style="overflow:scroll;">
	
				<thead>
					<tr>
						<th colspan="2"> ARMADOR INTERNACIONAL </th>
						<th colspan="2"> REPRESENTANTE NACIONAL </th>
						<th colspan="2"></th> 
					</tr>
				</thead>			
                <thead>
                    <tr>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'codigo'; sortReverse = !sortReverse">
							Codigo 
							<span ng-show="sortType == 'codigo' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'codigo' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'nomeLineOp'; sortReverse = !sortReverse">
							Nome 
							<span ng-show="sortType == 'nomeLineOp' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'nomeLineOp' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'cnpj'; sortReverse = !sortReverse">
							CNPJ 
							<span ng-show="sortType == 'cnpj' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'cnpj' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'nomeRepresentante'; sortReverse = !sortReverse">
							Nome 
							<span ng-show="sortType == 'nomeRepresentante' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'nomeRepresentante' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        
                        <th> EDITAR </th>
                        <th> EXCLUIR </th>
                    </tr>
                </thead>

                <tbody>
                    <tr ng-repeat="vinculo in listaVinculo | orderBy:sortType:sortReverse " >
                        <td style="text-align:center;font-size: 11px;">{{ vinculo.codigo }}</td>
                        <td style="text-align:center;font-size: 11px;">{{ vinculo.nomeLineOp }}</td>
                        <td style="text-align:center;font-size: 11px;">{{ vinculo.cnpj }}</td>
                        <td style="text-align:center;font-size: 11px;">{{ vinculo.nomeRepresentante }}</td>
                        
                        <td>
                            <a href="#" title="Editar" data-ng-click="editar(vinculo)"><i class="icon-edit icon-white" style="color:#013B53 !important;"></i></a>
                        </td>
                        
                        <td>
                            <a href="#" title="Excluir" data-ng-click="excluir(vinculo)"><i class="icon-remove icon-white" style="color:#013B53 !important;"></i></a>
                        </td>

                    </tr>

                </tbody>

                <tfoot>
                    <td colspan="14">
                        <!-- <pagination total-items="bigTotalItems" ng-model="currentPage" max-size="maxPage" class="pagination-sm" boundary-links="true" rotate="false" num-pages="qtdPaginas"></pagination> -->
                        <div class="pagination pull-left">
                        	
                        	<paging
							  page="currentPage" 
							  page-size="itemsPerPage" 
							  total="qtdRegistros"
							  show-prev-next="true"
							  show-first-last="true"
							  paging-action="findAllVinculo(page)">
							</paging>
							                          
                        </div>
                    </td>
                </tfoot>

            </table>

        </div>

    </div>               

</div>
