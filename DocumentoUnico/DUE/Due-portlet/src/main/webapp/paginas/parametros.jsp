<div ng-controller=parametrosController style="margin: 0px;">
    <div class=page-header style="margin-left: 3%;">


        <div class="panel panel-default span12">
            <div class="panel-heading">
                <h5>Diverg�ncia de Peso
                    <span title="Deveria ser informado o percentual que ser� aplicado para a informa��o de diverg�ncia de peso na DU-E, 
					para cima e/ou para baixo.">(?)</span>
                </h5>
            </div>

            <div class="panel-body">
                <form name="divergenciaPesoForm" novalidate>

                    <div class="row-fluid">
                        <div class="span4" style="width: 280px;">
                            <label>Indice (%) de Diverg�ncia de Peso ACIMA: </label>
                        </div>
                        <div class="span1">
                            <input type="text" id="divergenciaPesoAcima" style="text-align: center;" ng-model="divergenciaPesoAcima.valor" value="{{divergenciaPesoAcima.valor}}" ng-disabled="true" />
                        </div>
                        <div class="span2" style="margin-left: 1%;">
                            <button type="button" class="btn btn-primary" aria-label="Left Align" id="btnSubtractAcima" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-right: 7%;">
								<i class="fa fa-minus" aria-hidden="true" id="subtractAcima"></i>
							</button>
                            <button type="button" class="btn btn-primary" aria-label="Left Align" id="btnAddAcima" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-right: 7%;">
								<i class="fa fa-plus" aria-hidden="true" id="addAcima"></i>
							</button>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4" style="width: 280px;">
                            <label>Indice (%) de Diverg�ncia de Peso ABAIXO: </label>
                        </div>
                        <div class="span1">
                            <input type="text" id="divergenciaPesoAbaixo" style="text-align: center;" ng-model="divergenciaPesoAbaixo.valor" value="{{divergenciaPesoAbaixo.valor}}" ng-disabled="true" />
                        </div>
                        <div class="span2" style="margin-left: 1%;">
                            <button type="button" class="btn btn-primary" aria-label="Left Align" id="btnSubtractAbaixo" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-right: 7%;">
<!-- 								ng-click="decreaseDivPesoAbaixo()" title="+"> -->
								<i class="fa fa-minus" aria-hidden="true" id="subtractAbaixo"></i>
							</button>
                            <button type="button" class="btn btn-primary" aria-label="Left Align" id="btnAddAbaixo" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-right: 7%;">
								<i class="fa fa-plus" aria-hidden="true" id="addAbaixo"></i>
							</button>
                        </div>
                    </div>

                    <div class="row-fluid" style="margin-top: 1%;">
                        <div class="span8">
                            <label class="marginRight control-label">Data da �ltima Atualiza��o 
								<span title="Data atualizada automaticamente a cada altera��o."
								style="font-size: x-small;">(?)</span> :</label>
                            <input type="text" id="dataAtualizacao" class="span2" ng-model="dataUltimaAtualizacao" value="{{dataUltimaAtualizacao}}" ng-disabled="true" />
                        </div>
                    </div>

                    <div class="row-fluid" style="margin-top: 1%;">
                        <button ng-click="salvarParametrosDivergenciaPeso()" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px;" title="Salvar" class="btn-primary">
							<i class="fa fa-check" aria-hidden="true"></i>
						</button>
                    </div>

                </form>
            </div>

        </div>

    </div>

    <br><br>
    
    

    <!--div panel Cliente Financeiro / Pagador-->
    <div class="page-header" style="margin-left: 3%;">
        <div class="panel panel-default span12">
            <!-- div panel-heading -->
            <div class="panel-heading">
                <h5>Cliente Financeiro / Pagador</h5>
            </div>

            <!-- div panel body -->
            <div class="panel-body">
                <!-- início form -->
                <form name="#">
                    <div class="row-fluid">
                        <div class="span1">
                            <label class="checkbox inline">
								<input type="checkbox" id="validacaoLiberacao" style="margin-left: -10px"> 
							</label>
                        </div>

                        <div class="span3">
                            <label style="margin-left: -70px; margin-top: 7px">Valida��o obrigat�ria / Libera��o de Evento</label>
                        </div>
                    </div>

					<div class="row-fluid" style="margin-top: 1%;">
                        <div class="span8">
                            <label class="marginRight control-label">Data da �ltima Atualiza��o 
								<span title="Data atualizada automaticamente a cada altera��o."
								style="font-size: x-small;">(?)</span> :</label>
                            <input type="text" id="dataAtualizacaoClienteFP" class="span2" ng-model="dataUltimaAtualizacaoClienteFP" value="{{dataUltimaAtualizacaoClienteFP}}" ng-disabled="true" />
                        </div>
                    </div>

                    <div class="row-fluid">
                        <button ng-click="salvarParametrosClienteFinanceiroPagador()" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-top: 5px" title="Salvar" class="btn-primary">
							<i class="fa fa-check" aria-hidden="true"></i>
						</button>
                    </div>
                </form>
                <!-- termino form-->
            </div>
        </div>
    </div>    
</div>