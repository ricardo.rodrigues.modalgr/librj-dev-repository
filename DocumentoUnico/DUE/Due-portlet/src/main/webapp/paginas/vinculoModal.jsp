<div tabindex="-1" role="dialog" class="modal fade ng-isolate-scope in" ng-class="{in: animate}" modal-window="" index="0" animate="animate" style="z-index: 1050;display: none">
    <div class="modal-dialog" ng-class="{'modal-sm': size == 'sm', 'modal-lg': size == 'lg'}">
        <div class="modal-content">
            <div class="modal-header">
		  		<button type="button" ng-click="closeDetailVinculo()" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h3 style="text-align:center;">	
                	<div class="row-fluid" style="margin-bottom: 20px;">
						<h5>Sele��o de Representante nacional para o Armador Internacional : <p>{{vinc.nomeLineOp}}</p> </h5> 
					</div>
				</h3>
	
            </div>

            <div class="modal-body">
            
				<div class="row-fluid">
					<label class="span1">CNPJ:</label> 
					<input type="text" style="border-radius: 12px;margin-left: 30px;" class="input-block-level span6" ng-model="representante.cnpj" id="representanteCnpj" somentenumeros>
				</div>
				<div class="row-fluid">
					<label class="span1">NOME:</label> 
					<input type="text" style="border-radius: 12px;margin-left: 30px;" class="input-block-level span6" ng-model="representante.nome" id="representanteNome">
				</div>

				<div class="row-fluid" style="margin-top: 10px;">
					<button ng-click="findRepresentante()"
						style="height: 30px; margin-right: 10%; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 90px; margin-bottom: 10px;"
						class="btn btn-primary">Pesquisar</button>
				</div>

				<div class="row-fluid" style="margin-top: 20px;" ng-show="showListaRepresentante">
			          <p>Selecione o Representante nacional na lista abaixo para efetuar o vinculo com o Armador internacional.</p>                        
			    </div>

				<div class="row-fluid" ng-show="showListaRepresentante">
				
					 <table class="table table-bordered" style="overflow:scroll;">
				
							<thead>
								<tr>
									<th> CNPJ </th>
									<th> NOME </th>
								</tr>
							</thead>
							
							<tbody>
			                    <tr ng-repeat="rep in listaRepresentante" style="cursor: pointer;" ng-click="selRepresentante(rep)">
			                        <td style="text-align:center;font-size: 11px;">{{ rep.cnpj }}</td>
			                        <td style="text-align:center;font-size: 11px;">{{ rep.nomeRepresentante }}</td>
			                    </tr>
			                </tbody>
							
						</table>
				
				</div>
			</div>
		</div>
	</div>	
</div>