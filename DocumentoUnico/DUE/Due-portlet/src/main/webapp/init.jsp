<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.model.Group"%>
<%@ page import="com.liferay.portal.model.UserGroup"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.io.Serializable"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Map.Entry"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<!-- SCRIPTS -->
<script src="/DuePortlet/lib/js/main.min.js?v=0.0.7"></script>
<!-- Custom Scripts -->   
<script type="text/javascript" src="/DuePortlet/lib/ui-bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>
<!-- MOMENT -->
<script type="text/javascript" src="/DuePortlet/lib/js/moment.min.js"></script>
<script type="text/javascript" src="/DuePortlet/lib/js/moment-with-locales.min.js"></script>

<!-- Plugins -->
<link rel="stylesheet" type="text/css" media="screen" href="/DuePortlet/js/datePicker2.3.2/css/bootstrap-datepicker.min.css" />
<script src="/DuePortlet/js/datePicker2.3.2/js/bootstrap-datepicker.min.js"></script>
<script src="/DuePortlet/js/datePicker2.3.2/locales/bootstrap-datepicker.pt-BR.js"></script>

<script type="text/javascript" src="/DuePortlet/angularjs/module/app.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/factory/monitoramentoService.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/factory/parametrosService.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/factory/cfpService.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/factory/vinculoService.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/controller/masterCtrl.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/controller/monitoramentoController.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/controller/parametrosController.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/controller/cfpController.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/controller/vinculoController.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/factory/modalService.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/directives/capitalizeDirective.js"></script>
<script type="text/javascript" src="/DuePortlet/angularjs/directives/pagingDirective.js"></script>


<%
List<UserGroup> groups = user.getUserGroups();
Boolean hasPermissionDUE = false;
Boolean hasPermissionDespachante = false;
Boolean hasPermissionSupervisor = false;
for (int x=0;x<groups.size();x++){
	if (groups.get(x).getName().equals("DUE")) { 
		hasPermissionDUE=true;
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Despachante")) {
		hasPermissionDespachante=true;		
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Supervisor DUE")) {
		hasPermissionSupervisor=true;		
		break;
	}
}

Map<String, Serializable> expandoBridgeAttributes = user.getExpandoBridge().getAttributes();
String cpf = (String) expandoBridgeAttributes.get("NumeroDocumentoUsuario");
String validado = (String) expandoBridgeAttributes.get("Validado");
session.setAttribute("cpf", cpf);
session.setAttribute("validado", validado);
session.setAttribute("nmUserLogado", user.getFullName());
%>
