app.factory('vinculoService', ['$http', '$q',
    function($http, $q) {

        var _findAllVinculo = function(apiUrl, searchText, pageindex, pagesize) {
        	
//        	return  $http.get(apiUrl + "/api/vinculo?searchtext=" + searchText + "&pageindex=" + pageindex + "&pagesize=" + pagesize);
        	return  $http.get("/DuePortlet/due/vinculo?searchtext=" + searchText + "&pageindex=" + pageindex + "&pagesize=" + pagesize);
           
        }
                
        var _vincular = function(apiUrl, dto) {
        	
        	var config = {
        		headers : {
        			"Content-Type": "application/json;charset=UTF-8"
        		}
        	}
        	
//            return $http.post( apiUrl + '/api/vinculo', dto, config );
        	return $http.post("/DuePortlet/due/vinculo", dto, config );
        }
        
        var _desvincular = function(apiUrl, dto) {
        	
        	var config = {
        		headers : {
        			"Content-Type": "application/json;charset=UTF-8"
        		}
        	}
        	
//            return $http.post( apiUrl + '/api/desvinculo', dto, config );
        	return $http.post("/DuePortlet/due/desvinculo", dto, config );
        }
        
        var _findRepresentante = function(apiUrl, cnpj, nome) {
        	
//        	return  $http.get(apiUrl + "/api/representante?cnpj=" + cnpj + "&nome=" + nome);
        	return  $http.get("/DuePortlet/due/representante?cnpj=" + cnpj + "&nome=" + nome);
           
        }

        return {
        	findAllVinculo: _findAllVinculo,
        	vincular : _vincular,
        	desvincular : _desvincular,
        	findRepresentante : _findRepresentante
        }
    }
]);