app.factory('monitoramentoService', ['$http', '$q',
    function($http, $q) {
	
		function AllowAccesServiceAPI() {
	        $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
	        // Send this header only in post requests. Specifies you are sending a JSON object
	        $http.defaults.headers.post['dataType'] = 'json';
	        $http.defaults.headers.post['Content-Type'] = 'application/json';
	    }
	        
        var _findMonitoramento = function(dto) {
        	
        	var config = {
        		headers : {
        			"Content-Type": "application/json;charset=UTF-8"
        		}
        	}
        	return $http.post("/DuePortlet/due/monitoramento", dto, config );
        }
                
        var _finalizar = function(apiUrl, cctId) {
            
        	return $http.get("/DuePortlet/due/finalizar?cctId=" + cctId);
        }

        return {
        	findMonitoramento: _findMonitoramento,
        	finalizar : _finalizar
        }
    }
]);