app.factory('parametrosService', ['$http', '$q',
    function($http, $q) {

        var _findAllParametros = function(apiUrl) {
            var def = $q.defer();
//            $http.get(apiUrl + "/api/parametros").then(
            $http.get("/DuePortlet/due/parametros").then(
                function(response) {
                    console.log(response);
                    def.resolve(response);
                },
                function(error) {
                    console.log('error', error);
                });
            return def.promise;
        }

        var _saveParametrosDivergenciaPeso = function(apiUrl, parametros) {
            var def = $q.defer();
            $http({
//                url: apiUrl + '/api/parametros/divergenciapeso',
            	url: "/DuePortlet/due/parametros/divergenciapeso",
                method: "POST",
                data: parametros,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then(function(response) {
                console.log(response);
                def.resolve(response);
            }, function(error) {
                console.log('error', error);
                def.rejected(error);
            });
            return def.promise;
        }
        
        var _salvarParametrosClienteFinanceiroPagador = function(apiUrl, obj) {
            var def = $q.defer();
            $http({
//                url: apiUrl + '/api/parametros/clientefinanceiropagador',
            	url: "/DuePortlet/due/parametros/clientefinanceiropagador",
                method: "POST",
                data: obj,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then(function(response) {
                console.log(response);
                def.resolve(response);
            }, function(error) {
                console.log('error', error);
                def.rejected(error);
            });
            return def.promise;
        }

        return {
            findAllParametros: _findAllParametros,
            saveParametrosDivergenciaPeso: _saveParametrosDivergenciaPeso,
            salvarParametrosClienteFinanceiroPagador : _salvarParametrosClienteFinanceiroPagador
        }
    }
]);