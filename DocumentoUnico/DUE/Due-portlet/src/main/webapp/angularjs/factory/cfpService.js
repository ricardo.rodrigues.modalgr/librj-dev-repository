app.factory('cfpService', ['$http', '$q',
	function($http, $q) {

	
    var _pesquisaBooking = function(apiUrl, booking, lineOperatorGkey, despachante) {
    	
//        return $http.get(apiUrl + "/api/cfp?bookingNbr=" + booking + "&lineOperatorGkey=" + lineOperatorGkey + "&despachante=" + despachante);
    	return $http.get("/DuePortlet/due/cfp?bookingNbr=" + booking + "&lineOperatorGkey=" + lineOperatorGkey + "&despachante=" + despachante);
    }
    
    var _validaCheckObrigatorio = function(apiUrl) {
    	
//        return $http.get(apiUrl + "/api/validaparam");
    	return $http.get("/DuePortlet/due/validaparam");
    }
    
    var _salvaListaCFP = function(apiUrl, dto) {
    	
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
//        return $http.post( apiUrl + '/api/savecfp', dto, config );
    	return $http.post("/DuePortlet/due/savecfp", dto, config );
    }

    return {
    	pesquisaBooking: _pesquisaBooking,
    	validaCheckObrigatorio : _validaCheckObrigatorio,
    	salvaListaCFP : _salvaListaCFP
    }

}]);