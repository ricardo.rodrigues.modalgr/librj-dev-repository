var app = angular.module('App', ['ui.bootstrap', 'ui.router']);

'use strict';

angular.module('App').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/monitoramento');
}]).run(['$rootScope', '$state', '$location',
    function($rootScope, $state, $location) {
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                $rootScope.toState = toState;
                $rootScope.fromState = fromState;
                console.log(event);
                console.log('fromState>>', fromState, ' ', 'toState>>', toState);
                $rootScope.isLogin = (toState.name == 'index');
            });
    }
]);