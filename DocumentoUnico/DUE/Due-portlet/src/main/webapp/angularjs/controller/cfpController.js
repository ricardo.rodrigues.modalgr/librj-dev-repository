app.controller('cfpController',['$scope','$modal','cfpService','modalService',
	function($scope, $modal, cfpService, modalService) {

		// VARIABLES
		$scope.Modal = "";
		$scope.Mensagem = "";
		$scope.Title = "";
		$scope.ModalGif;
		$scope.showDialogError = false;
		$scope.showDialogSuccess = false;
		$scope.showDialogSalvar = false;

		$scope.apiURL = $(document.getElementById("divUrlApi").childNodes[1]).find('span').html();
		$scope.usuario = $('#usuario').val();
		$scope.cpfDespachanteLogado = $('#cpf').val();		

		$scope.booking = "";
		$scope.lineOpList = [];
		$scope.lineOpSel = {};
		$scope.lineOpSel.label = "";
		$scope.lineOpSel.gkey = null;

		$scope.listaTabela = [];
		$scope.checkTodos = false;
		
		$scope.dto = {};
		
		$scope.exportadorselmudatodos = {};

		$scope.pesquisaBooking = function() {
			
			if ($scope.booking == "") {
				$scope.Mensagem = "Booking não encontrado!";
				modalService.closeModalErrors($scope.Mensagem);
			} else {
				$scope.openGifModal();
				
				cfpService.pesquisaBooking($scope.apiURL,$scope.booking,$scope.lineOpSel.gkey,$scope.cpfDespachanteLogado).then(function(response) {
					// SUCCESS
					// 100,101,200,201
					console.log(response);

					$scope.dto = response.data;

					if ($scope.dto.mensagem != null) {
						$scope.closeGifModal();
						$scope.Mensagem = $scope.dto.mensagem;
						modalService.closeModalErrors($scope.Mensagem);
						return;
					} else if ($scope.dto.lineOperatorList != null && $scope.dto.lineOperatorList.length > 1) {
						$scope.lineOpList = $scope.dto.lineOperatorList;
						$scope.closeGifModal();
						$scope.Mensagem = "O Booking possui mais de um armador. Selecione o amador e clique em pesquisar novamente.";
						modalService.closeModalErrors($scope.Mensagem);
						return;
					} else {
						if($scope.dto.listaTabela == undefined || $scope.dto.listaTabela == null || $scope.dto.listaTabela.length == 0) {
							$scope.closeGifModal();
							$scope.Mensagem = "Não foram encontrados registros para o Booking / Armador informados.";
							modalService.closeModalErrors($scope.Mensagem);
							return;
						}
						var lineOp = {};
						lineOp.label = $scope.dto.lineOperator.label;
						lineOp.gkey = $scope.dto.lineOperator.gkey;
						$scope.lineOpList = $scope.dto.lineOperatorList;
						$scope.lineOpSel = lineOp;

						$scope.listaTabela = $scope.dto.listaTabela;
						
						for(let i = 0; i < $scope.listaTabela.length; i++){
							$scope.listaTabela[i].exportadorSel = {};
							$scope.listaTabela[i].checado = $scope.listaTabela[i].confirmado == 1;
						
							for(let j = 0; j < $scope.listaTabela[i].exportadores.length; j++){
								if($scope.listaTabela[i].exportadores[j].siscomex) {
									$scope.listaTabela[i].exportadorSel = $scope.listaTabela[i].exportadores[j];
									$scope.listaTabela[i].idExportadorSel = $scope.listaTabela[i].exportadores[j].id;
								}
							}	
						}
						$scope.closeGifModal();
						return;
					}
				},
				function errorCallBack(response) {
					// ERROR 400,401,500
					console.log(response);
					$scope.closeGifModal();
					$scope.Mensagem = response.data;
					modalService.closeModalErrors($scope.Mensagem);
				})
			}
		}
		
		$scope.salvar = function() {
			
			
//			var existeAlterado = false;
//			for(let i = 0; i < $scope.listaTabela.length; i++){
//				if(($scope.listaTabela[i].status != 'VENCIDO' && $scope.listaTabela[i].status != 'INEXISTENTE' && $scope.listaTabela[i].status != 'NAO_REPRESENTA') && ($scope.listaTabela[i].alterado)) {
//					existeAlterado = true;
//				}
//			}
//			if(existeAlterado) {
				var todosChecados = true;
				for(let i = 0; i < $scope.listaTabela.length; i++){
					if( ($scope.listaTabela[i].status != 'VENCIDO' && $scope.listaTabela[i].status != 'INEXISTENTE' && $scope.listaTabela[i].status != 'NAO_REPRESENTA') && (!$scope.listaTabela[i].checado) ) {
						todosChecados = false;
					}
				}
				if(!todosChecados) {
					$scope.Mensagem = "Para salvar, será necessário confirmar todos os registros. Utilizar a opção selecionar todos, caso não exista mais alterações.";
					modalService.closeModalErrors($scope.Mensagem);
					return;
				} else {
					
					$scope.openGifModal();
					
					cfpService.salvaListaCFP($scope.apiURL, $scope.dto)
					.success(function(response) {
						// SUCCESS
						// 100,101,200,201
						if (response == ""){
							$scope.closeGifModal();
							$scope.Mensagem = "Funcionalidade não está funcionando."
							modalService.closeModalErrors($scope.Mensagem);
							return;							
						}
												
						console.log(response);
						
						$scope.checkTodos = false;
						
						$scope.dto = response;
						
						if($scope.dto.mensagem != null) {
							if ($scope.dto.erro) {
								$scope.closeGifModal();
								$scope.Mensagem = $scope.dto.mensagem;
								modalService.closeModalErrors($scope.Mensagem);
								return;
							} else {
								
								var lineOp = {};
								lineOp.label = $scope.dto.lineOperator.label;
								lineOp.gkey = $scope.dto.lineOperator.gkey;
								$scope.lineOpList = $scope.dto.lineOperatorList;
								$scope.lineOpSel = lineOp;
								
								$scope.listaTabela = $scope.dto.listaTabela;
								
								for(let i = 0; i < $scope.listaTabela.length; i++){
									$scope.listaTabela[i].exportadorSel = {};
									$scope.listaTabela[i].checado = $scope.listaTabela[i].confirmado == 1;
								
									for(let j = 0; j < $scope.listaTabela[i].exportadores.length; j++){
										if($scope.listaTabela[i].exportadores[j].siscomex) {
											$scope.listaTabela[i].exportadorSel = $scope.listaTabela[i].exportadores[j]; 
											$scope.listaTabela[i].idExportadorSel = $scope.listaTabela[i].exportadores[j].id;
										}
									}	
								}
								$scope.closeGifModal();
								$scope.Mensagem = $scope.dto.mensagem;
								modalService.closeModalErrors($scope.Mensagem);
								return;
							}
						}
					})
					.error(function(response) {
						// ERROR 400,401,500
						console.log(response);
						$scope.closeGifModal();
						$scope.Mensagem = response.data;
						modalService.closeModalErrors($scope.Mensagem);					
					});
					
				}
//			} else {
//				$scope.Mensagem = "Nenhum registro foi alterado.";
//				$scope.showDialogError = true;
//			}
		}
		
		$scope.changeExportador = function(row) {
			row.alterado = true;
			$scope.exportadorselmudatodos = row.exportadorSel;
			
			for(let i = 0; i < $scope.listaTabela.length; i++){
				if($scope.listaTabela[i].due == row.due ) {
					$scope.listaTabela[i].exportadorSel = row.exportadorSel;
					$scope.listaTabela[i].alterado = true;
				}
			}
			
			$scope.Mensagem = "Deseja efetuar alteração em todos os Exportadores ?";
			modalService.dialogModal($scope.Mensagem, $scope.alterarTodos);
		}
		
		//TODO
		$scope.alterarTodos = function() {
			for(let i = 0; i < $scope.listaTabela.length; i++){
				if( ($scope.listaTabela[i].status != 'VENCIDO' && $scope.listaTabela[i].status != 'INEXISTENTE' && $scope.listaTabela[i].status != 'NAO_REPRESENTA') ) {
					$scope.listaTabela[i].exportadorSel = $scope.exportadorselmudatodos;
					$scope.listaTabela[i].alterado = true;
				}
			}
		}
		
		$scope.checarTodos = function(){
			for(let i = 0; i < $scope.listaTabela.length; i++){
				if( ($scope.listaTabela[i].status != 'VENCIDO' && $scope.listaTabela[i].status != 'INEXISTENTE' && $scope.listaTabela[i].status != 'NAO_REPRESENTA') ) {
					$scope.listaTabela[i].checado = $scope.checkTodos;
				}
			}
		};

		$scope.changeBooking = function() {
			$scope.lineOpList = [];
			$scope.lineOpSel = {};
			$scope.lineOpSel.label = "";
			$scope.lineOpSel.gkey = null;
			$scope.listaTabela = [];
		}
		
		$scope.$on('3', function(event, args) {
			$scope.booking = "";
			$scope.lineOpList = [];
			$scope.lineOpSel = {};
			$scope.lineOpSel.label = "";
			$scope.lineOpSel.gkey = null;
			$scope.listaTabela = [];
			$scope.checkTodos = false;
			$scope.dto = {};
        });								
	} ]);