app.controller('vinculoController', ['$scope', '$modal', '$location', '$rootScope', 'vinculoService', '$filter', 'modalService',
    function($scope, $modal, $location, $rootScope, vinculoService, $filter, modalService) {

        $scope.Mensagem = "";

        $scope.sortType = 'nomeLineOp';
        $scope.sortReverse = true;
        $scope.searchText = '';
        $scope.listaVinculo = [];
        $scope.vinculo = {};
        
        $scope.representante = {};
        $scope.representanteSel = {};
        $scope.listaRepresentante = [];        
        $scope.showModalVinculo = false;
        $scope.showListaRepresentante = false; 
        
        $scope.itemsPerPage = 10;        
        $scope.qtdRegistros = 0;        
        $scope.currentPage = 1;
        
        $scope.qtdporpaginalist = [{id : 0, qtd: 10}, {id : 1, qtd: 25}, {id : 2, qtd: 50}];
        $scope.qtdporpagina = {id : 0, qtd: 10};
        $scope.changeqtdporpagina = function() {
        	$scope.currentPage = 1;
        	$scope.itemsPerPage = $scope.qtdporpagina.qtd;
        	$scope.findAllVinculo();
        }
        
        $scope.showDetailVinculo = function(vinculo) {
        	this.Modal = $modal.open({
        		templateUrl: '/DuePortlet/lib/ui-bootstrap/modal-vinculo.html',
        		controller: function($modalInstance, $scope){
        		    $scope.vinculo = vinculo;
                    
                    $scope.closeDetailCCT = function() {
                        $modalInstance.close('cancel');
                    };
                    
        		}
        	});
        
        };        
        
        $scope.findAllVinculo = function(page) {
        	openGifModal();
        	if(page != undefined) {
     	       $scope.currentPage = page;
        	} else {
         		$scope.currentPage = 1;
         	}
        	vinculoService.findAllVinculo($scope.apiURL, $scope.searchText, $scope.currentPage, $scope.itemsPerPage).then(
                function(response) {
                    console.log(response.data);
                    $scope.listaVinculo = response.data.listaVinculo;
                    $scope.qtdRegistros = response.data.qtdRegistros;
                    closeGifModal();
                },
                function(error) {
                    console.log(error);
                    closeGifModal();
                })
        };
        
        $scope.editar = function(vinculo) {
        	$scope.representanteSel = {};
        	$scope.representante = {};
        	$scope.listaRepresentante = [];
        	$scope.showListaRepresentante = false;
        	
        	$scope.showModalVinculo = true;
        	
        	$scope.setActive(5, vinculo);
        };        
        
        $scope.closeDialogQuestion = function() {
        	$scope.representanteSel = {};
        	$scope.Mensagem = "";
        	$scope.showDialogQuestion = false;
        };
        
        $scope.excluir = function(vinculo) {
        	$scope.vinculo = vinculo;
        	if(!$scope.vinculo.cnpj) {
        		$scope.Mensagem = "Somente registros com representantes nacionais vinculados podem ser excluidos.";
        		modalService.closeModalErrors($scope.Mensagem);
                return;
        	}
        	
        	$scope.Mensagem = "Deseja desvincular o Representante nacional " + $scope.vinculo.nomeRepresentante + " do Armador internacional " + $scope.vinculo.nomeLineOp + " ?";
        	modalService.dialogModal($scope.Mensagem, $scope.excluirConfirm);
        	
        };
        
        $scope.excluirConfirm = function() {
        	openGifModal();
        	vinculoService.desvincular($scope.apiURL, $scope.vinculo).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if(dto.erro) {
                	closeGifModal();
                	$scope.Mensagem = dto.mensagem;
                    $scope.showDialogError = true;
                } else {
                	var dto = response.data;
                	$scope.vinculo = {};
                	$scope.showDialogDelete = false;
                	closeGifModal();
                    $scope.Mensagem = "Representante nacional desvinculado com sucesso.";
                    modalService.closeModalErrors($scope.Mensagem);
                    $scope.findAllVinculo();
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                var dto = response.data;
                $scope.Mensagem = dto.mensagem;
                modalService.closeModalErrors($scope.Mensagem);
            })
        };
        
        $scope.closeDialogDelete = function() {
        	$scope.vinculo = {};
        	$scope.showDialogDelete = false;
        };
        
        $scope.closeModalVinculo = function() {
        	$scope.vinculoModal.active = false;
        	$scope.vinculo.active = true;
        };
        
        $scope.closeDialogErrorModal = function() {
        	$scope.showDialogErrorModal = false;
        };
        
        $scope.closeDialogSuccessModal = function() {
        	$scope.showDialogSuccessModal = false;
        };
        
        $scope.closeDialogError = function() {
        	$scope.showDialogError = false;
        };
        
        $scope.closeDialogSuccess = function() {
        	$scope.findAllVinculo($scope.currentPage);
        	$scope.showDialogSuccess = false;
        };
        
        $scope.resetPage = function() {
            $scope.sortType = 'codigo';
            $scope.sortReverse = true;
            $scope.searchText = '';
            $scope.listaVinculo = [];
        };

        $scope.$on('4', function(event, args) {
            $scope.resetPage();
            $scope.findAllVinculo();
        });

        $scope.showDetailVinculo = function(vinculo) {
        	
        	$scope.representanteSel = {};
        	$scope.representante = {};
        	$scope.listaRepresentante = [];
        	$scope.showListaRepresentante = false;
        	$scope.showModalVinculo = true;
        	$scope.vinculo = vinculo;
        	
        	this.Modal = $modal.open({
        		templateUrl: '/DuePortlet/lib/ui-bootstrap/modal-dados-vinculo.html',
        		controller: function($modalInstance, $scope){
        		    $scope.vinculoDetail = vinculo;
                    
                    $scope.closeModalVinculo = function() {
                        $modalInstance.close('cancel');
                    };
                    
        		}
        	});
        
        };        
        
        
        $scope.findAllVinculo();
        
        function openGifModal() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DuePortlet/lib/ui-bootstrap/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }

        function closeGifModal() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }
    }
]);