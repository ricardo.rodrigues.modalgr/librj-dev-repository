app.controller('MasterCtrl', ['$scope', '$location', '$rootScope', '$modal', 'vinculoService', 'modalService',
    function($scope, $location, $rootScope, $modal, vinculoService, modalService) {
	
		$scope.hasPermissionDUE = $('#hasPermissionDUE').val();
		$scope.hasPermissionDespachante = $('#hasPermissionDespachante').val();
		$scope.hasPermissionSupervisor = $('#hasPermissionSupervisor').val();
		
		console.log("hasPermissionDUE -> " + hasPermissionDUE);
		console.log("hasPermissionDespachante -> " + hasPermissionDespachante);
		console.log("hasPermissionSupervisor -> " + hasPermissionSupervisor);
	
		$scope.monitoramento = {
				active: true
		};
		$scope.parametros = {
				active: false
		};
		$scope.clienteFinanceiroPagador = {
				active: false
		};
		$scope.vinculo = {
				active: false
		};
		$scope.vinculoModal = {
				active: false
		};		
		
		$scope.vinc = {};
		
		
        $scope.setActive = function(idPagina, vinculo) {
        	$scope.monitoramento.active = false;
        	$scope.parametros.active = false;
        	$scope.clienteFinanceiroPagador.active = false;
        	$scope.vinculo.active = false;
        	$scope.vinculoModal.active = false;
        	if(idPagina == 1) {
        		$scope.monitoramento.active = true;
        	}
        	if(idPagina == 2) {
        		$scope.parametros.active = true;
        	}
        	if(idPagina == 3) {
        		$scope.clienteFinanceiroPagador.active = true;
        	}
        	if(idPagina == 4) {
        		$scope.vinculo.active = true;
        	}
        	if(idPagina == 5) {
        		$scope.vinculo.active = true;
        		$scope.vinculoModal.active = true;  
        		$scope.vinc = vinculo;
        	}        	
        	$rootScope.$broadcast(idPagina);
            
        };
        
        $scope.findRepresentante = function() {
        	$scope.openGifModal();
        	if($scope.representante.cnpj == undefined) {
        		$scope.representante.cnpj = null;
        	}
        	if($scope.representante.nome == undefined) {
        		$scope.representante.nome = null;
        	}
        	vinculoService.findRepresentante($scope.apiURL, $scope.representante.cnpj, $scope.representante.nome).then(
                function(response) {
                    console.log(response.data);
                    var dto = response.data;
                    if(dto.erro) {
                    	$scope.closeGifModal();
                    	$scope.showListaRepresentante = false;
                    	$scope.Mensagem = dto.mensagem;
                        $scope.showDialogErrorModal = true;
                    } else {
                    	var dto = response.data;
                    	$scope.listaRepresentante = dto.listaVinculo;
                    	$scope.showListaRepresentante = true;
                    	$scope.closeGifModal();
                        $scope.Mensagem = "Clique na linha do Representante nacional na tabela abaixo para efetuar o vinculo.";
                        modalService.closeModalErrors($scope.Mensagem);
                    }
                },
                function(error) {
                    console.log(error);
                    $scope.closeGifModal();
                    var dto = error.data;
                    $scope.Mensagem = dto.mensagem;
                    modalService.closeModalErrors($scope.Mensagem);
                })
        };
        
        $scope.selRepresentante = function(rep) {
        	$scope.representanteSel = rep;

        	var msg = "Deseja vincular o Representante nacional " + $scope.representanteSel.nomeRepresentante + " ao Armador internacional " + $scope.vinculo.nomeLineOp + " ?";
        	modalService.dialogModal(msg, $scope.vincular);        	                
        };
        
        $scope.vincular = function() {
        	if(Object.keys($scope.representanteSel).length == 0) {
        		$scope.Mensagem = "Selecione um representante na lista para salvar.";
        		modalService.closeModalErrors($scope.Mensagem);
                return;
        	}
        	$scope.openGifModal();
        	$scope.showDialogQuestion = false;
        	$scope.vinculo = $scope.vinc;
        	$scope.vinculo.cnpj = $scope.representanteSel.cnpj;
        	$scope.vinculo.nomeRepresentante = $scope.representanteSel.nomeRepresentante;
        	
        	vinculoService.vincular($scope.apiURL, $scope.vinculo).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if(dto.erro) {
                	$scope.closeGifModal();
                	$scope.Mensagem = dto.mensagem;
                	modalService.closeModalErrors($scope.Mensagem);
                } else {
                	var dto = response.data;
    				$scope.vinculo = {};
    				$scope.showModalVinculo = false;
    				$scope.closeGifModal();
                    $scope.Mensagem = "Representante nacional vinculado com sucesso.";
                    modalService.closeModalErrors($scope.Mensagem);
                    $scope.setActive(4, null);
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                $scope.closeGifModal();
                var dto = response.data;
                $scope.Mensagem = dto.mensagem;
                modalService.closeModalErrors($scope.Mensagem);
            })
        };        
        
        $scope.closeDetailVinculo = function() {
        	$scope.setActive(4, null);
        }          
        
        $scope.initPaginas = function() {
        	
        	if($scope.hasPermissionSupervisor == "true") {
        		$scope.monitoramento.active = true;
        		$scope.parametros.active = false;
        		$scope.clienteFinanceiroPagador.active = false;
        		$scope.vinculo.active = false;
        		$scope.vinculoModal.active = false;
        	} else if($scope.hasPermissionSupervisor == "false") {
        		$scope.monitoramento.active = false;
        		$scope.parametros.active = false;
        		$scope.clienteFinanceiroPagador.active = true;
        		$scope.vinculo.active = false;
        		$scope.vinculoModal.active = false;
        	}
        	
        };
        
        $scope.initPaginas();

//        function initPaginas() {
//            $scope.paginas = [
//                { arquivo: "monitoramento.html", show: true, active: "active", name: "Monitoramento", id: "1" },
//                { arquivo: "parametros.html", show: false, active: "", name: "Parametrização", id: "2" },
//                { arquivo: "clienteFinanceiroPagador.html", show: false, active: "", name: "Cliente Financeiro Pagador", id: "3" }
//            ];
//        };
//
//        initPaginas();
//
//        $scope.setActive = function(pagina) {
//            //if (!pagina.show) {
//                var length = $scope.paginas.length;
//                for (var i = 0; i < length; i++) {
//                    $scope.paginas[i].active = "";
//                    $scope.paginas[i].show = false;
//                };
//                pagina.active = "active";
//                pagina.show = true;
//                $rootScope.$broadcast(pagina.id);
//            //}
//        };
        
        
        
        var mobileView = 992;
        $scope.getWidth = function() {
            return window.innerWidth;
        };
        $scope.$watch($scope.getWidth, function(newValue, oldValue) {
            if (newValue >= mobileView) {

            } else {
                $scope.toggle = false;
            }

        });
        $scope.toggleSidebar = function() {
            $scope.toggle = !$scope.toggle;
        };
        window.onresize = function() {
            $scope.$apply();
        };
        
        $scope.openGifModal = function() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DuePortlet/lib/ui-bootstrap/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }

        $scope.closeGifModal = function() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }        
        
    }
]);