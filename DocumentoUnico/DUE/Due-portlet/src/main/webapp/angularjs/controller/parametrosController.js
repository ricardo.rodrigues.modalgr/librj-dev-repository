app.controller('parametrosController', ['$scope', '$location', '$rootScope', 'parametrosService', '$modal', 'modalService',
    function($scope, $location, $rootScope, parametrosService, $modal, modalService) {

        $scope.ModalGif;
        $scope.Mensagem = "";
        $scope.Title = "Aviso";
        $scope.Modal;

        $scope.apiURL = $(document.getElementById("divUrlApi").childNodes[1]).find('span').html();
        console.log("API URL -> " + $scope.apiURL);

        $scope.divergenciaPesoAcima = {};
        $scope.divergenciaPesoAbaixo = {};
        $scope.dataUltimaAtualizacao = '';
        
        $scope.validacaoLiberacao = {};
        $scope.dataUltimaAtualizacaoClienteFP = '';

        var init = function() {
        	
        	$scope.openGifModal();
        	
            parametrosService.findAllParametros($scope.apiURL).then(
                function(response) {
                    console.log(response.data);
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].chave == 'DIVERGENCIA_PESO_ACIMA') {
                            $scope.divergenciaPesoAcima.id = response.data[i].id;
                            $scope.divergenciaPesoAcima.chave = response.data[i].chave;
                            $scope.divergenciaPesoAcima.valor = response.data[i].valor;
                            $scope.divergenciaPesoAcima.data = response.data[i].data;
                            $scope.dataUltimaAtualizacao = $scope.divergenciaPesoAcima.data;
                        }
                        if (response.data[i].chave == 'DIVERGENCIA_PESO_ABAIXO') {
                            $scope.divergenciaPesoAbaixo.id = response.data[i].id;
                            $scope.divergenciaPesoAbaixo.chave = response.data[i].chave;
                            $scope.divergenciaPesoAbaixo.valor = response.data[i].valor;
                            $scope.divergenciaPesoAbaixo.data = response.data[i].data;
                            $scope.dataUltimaAtualizacao = $scope.divergenciaPesoAbaixo.data;
                        }
                        if (response.data[i].chave == 'VALIDACAO_LIBERACAO') {
                        	$scope.validacaoLiberacao.id = response.data[i].id;
                        	$scope.validacaoLiberacao.chave = response.data[i].chave;
                        	$scope.validacaoLiberacao.valor = response.data[i].valor;
                        	$scope.validacaoLiberacao.data = response.data[i].data;
                        	$scope.dataUltimaAtualizacaoClienteFP = $scope.validacaoLiberacao.data;
                        	$('#validacaoLiberacao')["0"].checked = ($scope.validacaoLiberacao.valor == 1);
                        }
                    }
                    $scope.closeGifModal();
                });
        };


        var isDown = 0;
        var target = '';
        var delay = 150;
        var nextTime = 0;

        requestAnimationFrame(watcher);

        $("button").mousedown(function(e) {
            handleMouseDown(e);
        });
        $("button").mouseup(function(e) {
            handleMouseUp(e);
        });
        $("button").mouseout(function(e) {
            handleMouseUp(e);
        });

        function handleMouseDown(e) {
            e.preventDefault();
            e.stopPropagation();
            if (e.target.id == 'addAcima' || e.target.id == 'btnAddAcima') {
                isDown = 1;
                target = 'acima'
            }
            if (e.target.id == 'addAbaixo' || e.target.id == 'btnAddAbaixo') {
                isDown = 1;
                target = 'abaixo'
            }
            if (e.target.id == 'subtractAcima' || e.target.id == 'btnSubtractAcima') {
                isDown = -1;
                target = 'acima'
            }
            if (e.target.id == 'subtractAbaixo' || e.target.id == 'btnSubtractAbaixo') {
                isDown = -1;
                target = 'abaixo'
            }

        }

        function handleMouseUp(e) {
            e.preventDefault();
            e.stopPropagation();
            isDown = 0;
        }

        function watcher(time) {
            requestAnimationFrame(watcher);
            if (time < nextTime) {
                return;
            }
            nextTime = time + delay;
            if (isDown !== 0) {
                if (target == 'acima') {
                    var numero = Number($scope.divergenciaPesoAcima.valor);
                    numero += isDown;
                    if (numero < 0 || numero > 100) {
                        return;
                    }
                    $scope.divergenciaPesoAcima.valor = numero;
                    $('#divergenciaPesoAcima').val($scope.divergenciaPesoAcima.valor);
                }
                if (target == 'abaixo') {
                    var numero = Number($scope.divergenciaPesoAbaixo.valor);
                    numero += isDown;
                    if (numero < 0 || numero > 100) {
                        return;
                    }
                    $scope.divergenciaPesoAbaixo.valor = numero;
                    $('#divergenciaPesoAbaixo').val($scope.divergenciaPesoAbaixo.valor);
                }
            }
        }

        $scope.$on('2', function(event, args) {
            init();
        });

        $scope.salvarParametrosDivergenciaPeso = function() {

        	$scope.openGifModal();

            var parametros = [$scope.divergenciaPesoAcima, $scope.divergenciaPesoAbaixo];

            parametrosService.saveParametrosDivergenciaPeso($scope.apiURL, parametros).then(
                function(response) {
                    console.log(response.data);
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].chave == 'DIVERGENCIA_PESO_ACIMA') {
                            $scope.divergenciaPesoAcima.id = response.data[i].id;
                            $scope.divergenciaPesoAcima.chave = response.data[i].chave;
                            $scope.divergenciaPesoAcima.valor = response.data[i].valor;
                            $scope.divergenciaPesoAcima.data = response.data[i].data;
                            $scope.dataUltimaAtualizacao = $scope.divergenciaPesoAcima.data;
                        }
                        if (response.data[i].chave == 'DIVERGENCIA_PESO_ABAIXO') {
                            $scope.divergenciaPesoAbaixo.id = response.data[i].id;
                            $scope.divergenciaPesoAbaixo.chave = response.data[i].chave;
                            $scope.divergenciaPesoAbaixo.valor = response.data[i].valor;
                            $scope.divergenciaPesoAbaixo.data = response.data[i].data;
                            $scope.dataUltimaAtualizacao = $scope.divergenciaPesoAbaixo.data;
                        }
                    }

                    $scope.closeGifModal();
                    var msg = "Indices de parametrização (Divergência de Peso) atualizados com sucesso.";
                    var title = "Aviso";
                    modalService.closeModalErrors(msg);
                },
                function(error) {
                    console.log('error', error);
                    $scope.closeGifModal();
                    var msg = "Ocorreu um erro ao atualizar os parametros, tente novamente ou entre em contato com a administração do sistema.";
                    var title = "ERRO";
                    modalService.closeModalErrors(msg);
                })
        };
        
        
        $scope.salvarParametrosClienteFinanceiroPagador = function() {
        	
        	$scope.openGifModal();
        	
        	if($scope.validacaoLiberacao.chave == '' || $scope.validacaoLiberacao.chave == null || $scope.validacaoLiberacao.chave == undefined) {
     	       $scope.validacaoLiberacao.chave = 'VALIDACAO_LIBERACAO';
	     	}
		     	
	     	$scope.validacaoLiberacao.valor = ($('#validacaoLiberacao')["0"].checked ? 1 : 0);

            parametrosService.salvarParametrosClienteFinanceiroPagador($scope.apiURL,  $scope.validacaoLiberacao).then(
                function(response) {
                    console.log(response.data);
                	$scope.validacaoLiberacao.id = response.data.id;
                	$scope.validacaoLiberacao.chave = response.data.chave;
                	$scope.validacaoLiberacao.valor = response.data.valor;
                	$scope.validacaoLiberacao.data = response.data.data;
                	$scope.dataUltimaAtualizacaoClienteFP = $scope.validacaoLiberacao.data;
                	$scope.isValidacao = ($scope.validacaoLiberacao.valor == 1);

                	$scope.closeGifModal();
                    var msg = "Indices de parametrização (Cliente Financeiro Pagador) atualizados com sucesso.";
                    var title = "Aviso";
                    modalService.closeModalErrors(msg);

                },
                function(error) {
                    console.log('error', error);
                    $scope.closeGifModal();
                    var msg = "Ocorreu um erro ao atualizar os parametros, tente novamente ou entre em contato com a administração do sistema.";
                    var title = "ERRO";
                    modalService.closeModalErrors(msg);
                })
        };        
    }
]);