package br.com.customsaduana.due.service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.customsaduana.due.dto.ClienteFinanceiroPagadorDTO;
import br.com.customsaduana.due.dto.MonitoramentoDTO;
import br.com.customsaduana.due.dto.MonitoramentoRequestDTO;
import br.com.customsaduana.due.dto.Parametros;
import br.com.customsaduana.due.dto.VinculoRepresentante;
import br.com.customsaduana.due.dto.VinculoRepresentanteDTO;
import br.com.customsaduana.due.util.PropertiesUtil;

@Service
public class DueService {

	@Autowired
	private RestTemplate rest;	
	
	private HttpHeaders getHttpHeaders() {
						
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}	
	
	public MonitoramentoDTO findMonitoramento(MonitoramentoRequestDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/monitoramento");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<MonitoramentoRequestDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<MonitoramentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, MonitoramentoDTO.class);
		return responseEntity.getBody();		
	}
	
	public String finalizar(long cctId) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/monitoramento/finalizar");
		builder.queryParam("cctId", cctId);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, String.class);
		return responseEntity.getBody();
	}
	
	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(String bookingNbr, String lineOperatorGkey, String despachante) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/cfp");
		builder.queryParam("bookingNbr", bookingNbr);
		builder.queryParam("lineOperatorGkey", lineOperatorGkey);
		builder.queryParam("despachante", despachante);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ClienteFinanceiroPagadorDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, ClienteFinanceiroPagadorDTO.class);
		return responseEntity.getBody();
	}

	public int validaCheckObrigatorio() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validaparam");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Integer> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, Integer.class);
		return responseEntity.getBody();
	}

	public ClienteFinanceiroPagadorDTO salvaListaCFP(ClienteFinanceiroPagadorDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/savecfp");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<ClienteFinanceiroPagadorDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<ClienteFinanceiroPagadorDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, ClienteFinanceiroPagadorDTO.class);
		return responseEntity.getBody();
	}

	public List<Parametros> getParametrosDivergenciaPeso() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/parametros");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<List> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, List.class);
		return responseEntity.getBody();
	}

	public List<Parametros> salvarParametrosDivergenciaPeso(List<Parametros> parametros) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/parametros/divergenciapeso");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<List<Parametros>> entity = new HttpEntity<>(parametros, headers);
		ResponseEntity<List> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, List.class);
		return responseEntity.getBody();
	}

	public Parametros salvarParametrosClienteFinanceiroPagador(Parametros parametro) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/parametros/clientefinanceiropagador");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Parametros> entity = new HttpEntity<>(parametro, headers);
		ResponseEntity<Parametros> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, Parametros.class);
		return responseEntity.getBody();
	}

	public VinculoRepresentanteDTO findAllVinculo(String searchText, String pageIndex, String pageSize) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/vinculo");
		builder.queryParam("searchtext", searchText);
		builder.queryParam("pageindex", pageIndex);
		builder.queryParam("pagesize", pageSize);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<VinculoRepresentanteDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, VinculoRepresentanteDTO.class);
		return responseEntity.getBody();
	}

	public VinculoRepresentanteDTO findRepresentante(String cnpj, String nome) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/representante");
		builder.queryParam("cnpj", cnpj);
		builder.queryParam("nome", nome);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<VinculoRepresentanteDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, VinculoRepresentanteDTO.class);
		return responseEntity.getBody();
	}

	public VinculoRepresentanteDTO vincular(VinculoRepresentante vinculo) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/vinculo");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<VinculoRepresentante> entity = new HttpEntity<>(vinculo, headers);
		ResponseEntity<VinculoRepresentanteDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, VinculoRepresentanteDTO.class);
		return responseEntity.getBody();
	}

	public VinculoRepresentanteDTO desvincular(VinculoRepresentante vinculo) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/desvinculo");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<VinculoRepresentante> entity = new HttpEntity<>(vinculo, headers);
		ResponseEntity<VinculoRepresentanteDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, VinculoRepresentanteDTO.class);
		return responseEntity.getBody();
	}
	
}
