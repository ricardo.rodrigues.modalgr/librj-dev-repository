package br.com.customsaduana.due.dto;

import java.io.Serializable;
import java.util.List;

public class VinculoRepresentanteDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private long qtdPaginas;
	
	private long qtdRegistros;
	
	private int currentPage;
	
	private List<VinculoRepresentante> listaVinculo;
	
	private boolean erro;
	
	private String mensagem;

	public long getQtdPaginas() {
		return qtdPaginas;
	}

	public void setQtdPaginas(long qtdPaginas) {
		this.qtdPaginas = qtdPaginas;
	}

	public long getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(long qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<VinculoRepresentante> getListaVinculo() {
		return listaVinculo;
	}

	public void setListaVinculo(List<VinculoRepresentante> listaVinculo) {
		this.listaVinculo = listaVinculo;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
