package br.com.customsaduana.due.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.customsaduana.due.dto.ClienteFinanceiroPagadorDTO;
import br.com.customsaduana.due.dto.MonitoramentoDTO;
import br.com.customsaduana.due.dto.MonitoramentoRequestDTO;
import br.com.customsaduana.due.dto.Parametros;
import br.com.customsaduana.due.dto.VinculoRepresentante;
import br.com.customsaduana.due.dto.VinculoRepresentanteDTO;
import br.com.customsaduana.due.service.DueService;

@RestController
@RequestMapping("/due")
public class DueController {
	
	@Autowired
	private DueService dueService;
	
	@RequestMapping(value = "/monitoramento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MonitoramentoDTO findMonitoramento(@RequestBody(required = true) MonitoramentoRequestDTO dto) {
		return dueService.findMonitoramento(dto);
	}
	
	@RequestMapping(value = "/finalizar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String finalizar(@RequestParam("cctId") long cctId) {
		return dueService.finalizar(cctId);
	}
	
	@RequestMapping(value = "/cfp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(@RequestParam("bookingNbr") String bookingNbr, @RequestParam("lineOperatorGkey") String lineOperatorGkey, @RequestParam("despachante") String despachante) {
		return dueService.getDadosClienteFinanceiroPagador(bookingNbr, lineOperatorGkey, despachante);
	}
	
	@RequestMapping(value = "/validaparam", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public int validaCheckObrigatorio() {
		return dueService.validaCheckObrigatorio();
	}
	
	@RequestMapping(value = "/savecfp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO salvaListaCFP(@RequestBody(required = true) ClienteFinanceiroPagadorDTO dto) {
		return dueService.salvaListaCFP(dto);
	}
	
	@RequestMapping(value = "/parametros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> getParametrosDivergenciaPeso() {
		return dueService.getParametrosDivergenciaPeso();
	}
	
	@RequestMapping(value = "/parametros/divergenciapeso", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> salvarParametrosDivergenciaPeso(@RequestBody(required = true) List<Parametros> parametros) {
		return dueService.salvarParametrosDivergenciaPeso(parametros);
	}
	
	@RequestMapping(value = "/parametros/clientefinanceiropagador", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Parametros salvarParametrosClienteFinanceiroPagador(@RequestBody(required = true) Parametros parametro) {
		return dueService.salvarParametrosClienteFinanceiroPagador(parametro);
	}
	
	@RequestMapping(value = "/vinculo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO findAllVinculo(@RequestParam("searchtext") String searchText, @RequestParam("pageindex") String pageIndex, @RequestParam("pagesize") String pageSize) {
		return dueService.findAllVinculo(searchText, pageIndex, pageSize);
	}
	
	@RequestMapping(value = "/representante", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO findRepresentante(@RequestParam("cnpj") String cnpj, @RequestParam("nome") String nome) {
		return dueService.findRepresentante(cnpj, nome);
	}
	
	@RequestMapping(value = "/vinculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO vincular(@RequestBody(required = true) VinculoRepresentante vinculo) {
		return dueService.vincular(vinculo);
	}
	
	@RequestMapping(value = "/desvinculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VinculoRepresentanteDTO desvincular(@RequestBody(required = true) VinculoRepresentante vinculo) {
		return dueService.desvincular(vinculo);
	}
	
}
