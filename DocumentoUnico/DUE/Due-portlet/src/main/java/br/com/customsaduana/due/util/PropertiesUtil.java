package br.com.customsaduana.due.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class PropertiesUtil {


	private static Properties prop;
	
	public static String getProp() {
		if (PropertiesUtil.prop == null || PropertiesUtil.prop.isEmpty()) {
			PropertiesUtil.prop = new Properties();
			try {
				PropertiesUtil.prop.load(new FileInputStream(getBarra() + "data" + getBarra() + "config" +  getBarra() + "due-portlet.properties"));
			} catch (IOException e) {
			}
		}
		return PropertiesUtil.prop.getProperty("url");
	}

	private static String getBarra() {
		if ("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
			return "\\";
		}
		return "//";
	}

}
