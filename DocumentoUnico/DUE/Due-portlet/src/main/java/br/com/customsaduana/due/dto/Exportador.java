package br.com.customsaduana.due.dto;

public class Exportador {

	protected String id;

	protected String label;

	protected String doc;

	protected boolean siscomex;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public String getDoc() {
		return doc;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}
	
	public boolean getSiscomex() {
		return siscomex;
	}

	public boolean isSiscomex() {
		return siscomex;
	}

	public void setSiscomex(boolean siscomex) {
		this.siscomex = siscomex;
	}

}