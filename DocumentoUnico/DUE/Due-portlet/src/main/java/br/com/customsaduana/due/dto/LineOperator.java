package br.com.customsaduana.due.dto;

public class LineOperator {

	protected String label;

	protected String gkey;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGkey() {
		return gkey;
	}

	public void setGkey(String gkey) {
		this.gkey = gkey;
	}

}