package br.com.customsaduana.due.dto;

import java.io.Serializable;

public class VinculoRepresentante implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String idExterno;

	private String codigo;

	private String nomeLineOp;

	private String cnpj;

	private String nomeRepresentante;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNomeLineOp() {
		return nomeLineOp;
	}

	public void setNomeLineOp(String nomeLineOp) {
		this.nomeLineOp = nomeLineOp;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeRepresentante() {
		return nomeRepresentante;
	}

	public void setNomeRepresentante(String nomeRepresentante) {
		this.nomeRepresentante = nomeRepresentante;
	}

	@Override
	public String toString() {
		return "VinculoRepresentante [id=" + id + ", idExterno=" + idExterno + ", codigo=" + codigo + ", nomeLineOp=" + nomeLineOp + ", cnpj=" + cnpj + ", nomeRepresentante=" + nomeRepresentante + "]";
	}

}
