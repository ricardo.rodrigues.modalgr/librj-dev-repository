package br.com.customsaduana.due.dto;

import java.io.Serializable;

public class Parametros implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private String chave;

	private String valor;
	
	private String data;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
