package br.com.customsaduana.due.dto;

import java.io.Serializable;
import java.util.List;


public class MonitoramentoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long qtdPaginas;
	
	private long qtdRegistros;
	
	private int currentPage;
	
	private List<ListaMonitoramentoDTO> listaFinalizado;
	
	private List<Monitoramento> listaMonitoramento;

	public long getQtdPaginas() {
		return qtdPaginas;
	}

	public void setQtdPaginas(long qtdPaginas) {
		this.qtdPaginas = qtdPaginas;
	}

	public long getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(long qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<ListaMonitoramentoDTO> getListaFinalizado() {
		return listaFinalizado;
	}

	public void setListaFinalizado(List<ListaMonitoramentoDTO> listaFinalizado) {
		this.listaFinalizado = listaFinalizado;
	}

	public List<Monitoramento> getListaMonitoramento() {
		return listaMonitoramento;
	}

	public void setListaMonitoramento(List<Monitoramento> listaMonitoramento) {
		this.listaMonitoramento = listaMonitoramento;
	}
	
}
