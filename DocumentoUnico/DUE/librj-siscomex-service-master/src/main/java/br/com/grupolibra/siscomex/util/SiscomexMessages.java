package br.com.grupolibra.siscomex.util;

public class SiscomexMessages {
	
	public static final String STATUS_RECEPCIONADO_OK = "Recepcionado Ok";
	public static final String STATUS_ERRO = "Erro";
	public static final String CONTAINER_INVALIDO_MSG = "Contêiner inválido (ISO 6346)";
	public static final String CONTAINER_INVALIDO_MSG_NOTIFICACAO = "Notificação:\nContêiner inválido (ISO 6346). O envio será realizado sem o dígito verificador do contêiner.\n\n";
	public static final String STATUS_AGUARDANDO_RECEPCAO = "Aguardando Recepção";
	public static final String FALHA_AO_LEVANTAR_PESO_TARA_MSG = "Falha ao levantar peso ou tara.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_DANFE_MSG = "Falha ao levantar dados da DANFE.\n";
	public static final String FALHA_DANFES_DUPLICADAS = "DANFEs duplicadas.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_DADOS_TRANSPORTADORA_CONDUTOR_MSG = "Falha ao levantar dados da transportadora/condutor.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_NUMERO_CONTAINER_MSG = "Falha ao levantar dados do número do conteiner.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_LACRE_CONTAINER_MSG = "Falha ao levantar dados dos lacres do conteiner.\n";
	public static final String STATUS_OK = "Ok";
	public static final String STATUS_RECEPCAO_EFETUADA_SUCESSO = "Recepção efetuada com sucesso";
	public static final String ERRO_ENVIO_RECEITA = "Erro no envio para a receita.";
	public static final String CONTAINER_RECEBIDO_AGUARDANDO_ENVIO_RFB = "Contêiner recebido. Aguardando envio para RFB.";
	public static final Object CERTIFICADO_NAO_AUTORIZADO_OPERACAO = "Certificado não autorizado para esta operação.";
	public static final Object REQUISICAO_NAO_AUTORIZADA = "Requisição não autorizada.";
	public static final Object REGISTRO_NAO_ENCONTRADO = "Registro não encontrado.";
	public static final Object ERRO_NEGOCIO = "Erro de negócio.";
	public static final Object ERRO_INTERNO_SERVIDO = "Erro interno do servidor.";
	public static final Object ERRO_TIMEOUT = "Timeout, falha ao se conectar ao siscomex.";
	public static final Object SERVICO_INDISPONIVEL = "Serviço SISCOMEX indisponível";
	public static final Object REQUISICAO_MAL_FORMATADA = "Requisição mal formatada.";

	public static final String STATUS_INDICADOR_BLOQUEIO_BLOQUEADO_MSG = "Bloqueado";
	public static final String STATUS_INDICADOR_BLOQUEIO_LIBERADO_MSG = "Liberado";

}
