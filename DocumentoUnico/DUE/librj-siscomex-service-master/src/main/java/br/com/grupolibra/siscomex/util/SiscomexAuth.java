package br.com.grupolibra.siscomex.util;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Scope(value = "singleton")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class SiscomexAuth {

	private String authorization;

	private String xCSRFToken;

	private boolean isSiscomexOnline;
}
