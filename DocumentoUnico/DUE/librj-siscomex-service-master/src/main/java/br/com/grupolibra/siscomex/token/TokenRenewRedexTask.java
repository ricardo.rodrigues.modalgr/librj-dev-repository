package br.com.grupolibra.siscomex.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TokenRenewRedexTask {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	private static final long POLLING_RATE_MS = 1000 * 60 * 30;

	@Autowired
	AcquireTokenRedexService acquireTokenRedexService;

	@Scheduled(fixedRate = POLLING_RATE_MS, initialDelay=300000)
	public void tokenRenew() {
		LOGGER.info("Renovando token REDEX...");
		acquireTokenRedexService.acquireToken();
		LOGGER.info("Fim da renovação do token REDEX");
	}

}
