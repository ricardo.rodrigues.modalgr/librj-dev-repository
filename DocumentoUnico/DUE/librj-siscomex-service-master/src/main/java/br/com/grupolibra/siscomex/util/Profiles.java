package br.com.grupolibra.siscomex.util;

public final class Profiles {
	public final static String IMPORTADOR_EXPORTADOR = "IMPEXP";
	public final static String DEPOSITARIO = "DEPOSIT";
	public final static String OPERADOR_PORTUARIO = "OPERPORT";
	public final static String TRANSPORTADOR = "TRANSPORT";
	public final static String AGENTE_REMESSA = "AGEREMESS";
	public final static String AJUDANTE_DESPACHANTE = "AJUDESPAC";
	public final static String HABILITADOR = "HABILITAD";
	public final static String PUBLICO = "PUBLICO";
}
