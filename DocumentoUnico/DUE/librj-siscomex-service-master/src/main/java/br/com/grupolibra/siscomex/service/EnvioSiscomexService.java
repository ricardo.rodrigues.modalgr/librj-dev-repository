package br.com.grupolibra.siscomex.service;

import java.util.List;
import java.util.Map;

public interface EnvioSiscomexService {
	
	Map<String,Object> enviaXMLSiscomex(String xml, boolean redex);

	Map<String,Object> consultaConteiner(List<String> conteineres, boolean redex);

	Map<String, Object> consultaDadosResumidos(List<String> dues, boolean redex);
	
	Map<String, Object> consultarDUEporNF(String nf, boolean redex);

	Map<String, Object> consultaDue(String due, boolean redex);

}
