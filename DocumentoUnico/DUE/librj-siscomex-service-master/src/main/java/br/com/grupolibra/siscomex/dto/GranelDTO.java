package br.com.grupolibra.siscomex.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GranelDTO {
	
	private Integer tipoGranel;
	
	private String sgUnidadeMedida;
	
	private Float quantidade;	
}
