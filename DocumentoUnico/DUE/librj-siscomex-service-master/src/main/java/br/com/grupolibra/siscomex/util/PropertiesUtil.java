package br.com.grupolibra.siscomex.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesUtil {

	private static final Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

	private static Properties prop;

	private static Properties sslProp;

	public static final String ARQUIVO_SSL_PROPERTIES = "ssl.properties";

	public static Properties getSSLProp() {
		String pathProperties = PropertiesUtil.getLocalProp().getProperty("ssl.path.properties");
		String pathArquivo = pathProperties + ARQUIVO_SSL_PROPERTIES;
		InputStream input = null;

		if (PropertiesUtil.sslProp == null || PropertiesUtil.sslProp.isEmpty()) {
			PropertiesUtil.sslProp = new Properties();
			try {
				input = new FileInputStream(pathArquivo);
				PropertiesUtil.sslProp.load(input);
			} catch (IOException e) {
				logger.error("Erro ao abrir arquivo de configuração --> {} ", e);
			}
		}
		return PropertiesUtil.sslProp;
	}

	public static Properties getLocalProp() {
		if (PropertiesUtil.prop == null || PropertiesUtil.prop.isEmpty()) {
			PropertiesUtil.prop = new Properties();
			try {
				PropertiesUtil.prop.load(new FileInputStream(
						System.getProperty("user.dir") + getBarra() + "siscomex-service.properties"));
			} catch (IOException e) {
				logger.error("Erro ao abrir arquivo de configuração --> {} ", e);
				e.printStackTrace();
			}
		}
		return PropertiesUtil.prop;
	}

	private static String getBarra() {
		if ("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
			return "\\";
		}
		return "//";
	}

}
