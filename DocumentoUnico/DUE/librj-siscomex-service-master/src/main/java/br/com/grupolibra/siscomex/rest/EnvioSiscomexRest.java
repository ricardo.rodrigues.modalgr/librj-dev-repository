package br.com.grupolibra.siscomex.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.siscomex.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.siscomex.service.EnvioSiscomexService;

@RestController
@RequestMapping("enviosiscomex")
public class EnvioSiscomexRest {

	@Autowired
	private EnvioSiscomexService envioSiscomexService;

	@CrossOrigin
	@PostMapping
	public Map<String, Object> enviaXMLSiscomex(@RequestBody String xml) {
		return envioSiscomexService.enviaXMLSiscomex(xml, false);
	}

	@CrossOrigin
	@GetMapping(path = "/consultaconteiner", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> consultaConteiner(
			@RequestParam(value = "nrConteiner", required = true) List<String> conteineres) {
		return envioSiscomexService.consultaConteiner(conteineres, false);
	}
	
	@CrossOrigin
	@GetMapping(path = "/consultadadosresumidos")
	public Map<String, Object> consultaDadosResumidos(
			@RequestParam(value = "numeroDUE", required = true) List<String> dues) {
		return envioSiscomexService.consultaDadosResumidos(dues, false);
	}
}
