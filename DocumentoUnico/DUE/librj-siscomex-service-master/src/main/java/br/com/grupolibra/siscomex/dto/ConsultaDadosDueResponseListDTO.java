package br.com.grupolibra.siscomex.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaDadosDueResponseListDTO {

	private List<ConsultaDadosDueResponseDTO> listaRetorno;

	private boolean flag;

	private String erro;

	private String mensagem;
}
