package br.com.grupolibra.siscomex.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaDadosDueResponseDTO {
	
	private String numeroDUE;
	
	private String numeroRUC;
	
	private boolean ocorreuDesembaracoOuAutorizacaoEmbarqueAntecipado;
	
	private boolean existeImpedimentoEmbarque;
	
	private boolean indicadorSeCargaRUCMaster;
	
	private String numeroRUCMasterDaCarga;
	
	private List<String> conteineres;
	
	private List<DocumentoTransporteDTO> documentosDeTransporte;
	
	private List<CargaSoltaVeiculoDTO> listaCargasSoltasVeiculos;
	
	private List<GranelDTO> listaGraneis;

}
