package br.com.grupolibra.siscomex;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.com.grupolibra.siscomex.token.AcquireTokenRedexService;
import br.com.grupolibra.siscomex.token.AcquireTokenService;

@SpringBootApplication
@EnableScheduling
public class SiscomexApplication {
	
	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
	}

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SiscomexApplication.class, args);
		AcquireTokenService acquireTokenService = (AcquireTokenService) ctx.getBean("acquireTokenService");
		acquireTokenService.acquireToken();
		
		AcquireTokenRedexService acquireTokenRedexService = (AcquireTokenRedexService) ctx.getBean("acquireTokenRedexService");
		acquireTokenRedexService.acquireToken();
	}
}
