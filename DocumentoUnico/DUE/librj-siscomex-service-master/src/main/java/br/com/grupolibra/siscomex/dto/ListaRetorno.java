
package br.com.grupolibra.siscomex.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "numeroConteiner",
    "pesoBruto",
    "tara",
    "lacres",
    "documentosCarga",
    "permiteMovimentacao"
})
public class ListaRetorno {

    @JsonProperty("numeroConteiner")
    private String numeroConteiner;
    @JsonProperty("pesoBruto")
    private Double pesoBruto;
    @JsonProperty("tara")
    private Double tara;
    @JsonProperty("lacres")
    @JsonDeserialize(as = java.util.List.class)
    private List<String> lacres = null;
    @JsonProperty("documentosCarga")
    @JsonDeserialize(as = java.util.List.class)
    private List<DocumentosCarga> documentosCarga = null;
    @JsonProperty("permiteMovimentacao")
    private Boolean permiteMovimentacao;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("numeroConteiner")
    public String getNumeroConteiner() {
        return numeroConteiner;
    }

    @JsonProperty("numeroConteiner")
    public void setNumeroConteiner(String numeroConteiner) {
        this.numeroConteiner = numeroConteiner;
    }

    @JsonProperty("pesoBruto")
    public Double getPesoBruto() {
        return pesoBruto;
    }

    @JsonProperty("pesoBruto")
    public void setPesoBruto(Double pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    @JsonProperty("tara")
    public Double getTara() {
        return tara;
    }

    @JsonProperty("tara")
    public void setTara(Double tara) {
        this.tara = tara;
    }

    @JsonProperty("lacres")
    public List<String> getLacres() {
        return lacres;
    }

    @JsonProperty("lacres")
    public void setLacres(List<String> lacres) {
        this.lacres = lacres;
    }

    @JsonProperty("documentosCarga")
    public List<DocumentosCarga> getDocumentosCarga() {
        return documentosCarga;
    }

    @JsonProperty("documentosCarga")
    public void setDocumentosCarga(List<DocumentosCarga> documentosCarga) {
        this.documentosCarga = documentosCarga;
    }

    @JsonProperty("permiteMovimentacao")
    public Boolean getPermiteMovimentacao() {
        return permiteMovimentacao;
    }

    @JsonProperty("permiteMovimentacao")
    public void setPermiteMovimentacao(Boolean permiteMovimentacao) {
        this.permiteMovimentacao = permiteMovimentacao;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
