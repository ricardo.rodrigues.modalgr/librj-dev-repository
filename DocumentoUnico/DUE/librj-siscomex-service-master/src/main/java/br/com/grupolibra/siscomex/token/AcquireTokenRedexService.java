package br.com.grupolibra.siscomex.token;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.grupolibra.siscomex.util.Profiles;
import br.com.grupolibra.siscomex.util.PropertiesUtil;
import br.com.grupolibra.siscomex.util.SiscomexRedexAuth;

@Service
public class AcquireTokenRedexService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private final SiscomexRedexAuth siscomexRedexAuth;

	public AcquireTokenRedexService(SiscomexRedexAuth siscomexRedexAuth) {
		this.siscomexRedexAuth = siscomexRedexAuth;
	}

//	@Value("${siscomex.key-store-local}")
////	@Value("${siscomex.key-store}")
//	private String keystore;

//	@Value("${siscomex.key-store-type}")
//	private String keystoreType;

//	@Value("${siscomex.key-store-password}")
//	private String keystorePassword;

//	@Value("${siscomex.trust-store-local}")
////	@Value("${siscomex.trust-store}")
//	private String truststore;

//	@Value("${siscomex.trust-store-type}")
//	private String truststoreType;

//	@Value("${siscomex.trust-store-password}")
//	private String truststorePassword;

//	@Value("${siscomex.url}")
//	private String siscomexUrl;
	
//	@Value("${siscomex.token-uri}")
//	private String tokenUri;
	
//	@Value("${siscomex.profile}")
//	private String profile;

	public void acquireToken() {
		LOGGER.info("INIT token acquisition method");
		try {
			String keystore = getProp("siscomex.redex.key-store");
			String keystoreType = getProp("siscomex.redex.key-store-type");
			String keystorePassword = getProp("siscomex.redex.key-store-password");
			String truststore = getProp("siscomex.redex.trust-store");
			String truststoreType = getProp("siscomex.redex.trust-store-type");
			String truststorePassword = getProp("siscomex.redex.trust-store-password");
			String siscomexUrl = getProp("siscomex.url");
			String tokenUri = getProp("siscomex.token-uri");
			String profile = getProp("siscomex.redex.profile");
			
			KeyStore clientStore = KeyStore.getInstance(keystoreType);
			clientStore.load(new FileInputStream(keystore), keystorePassword.toCharArray());

			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(clientStore, keystorePassword.toCharArray());
			KeyManager[] kms = kmf.getKeyManagers();

			KeyStore trustStore = KeyStore.getInstance(truststoreType);
			trustStore.load(new FileInputStream(truststore), truststorePassword.toCharArray());

			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
			TrustManager[] tms = tmf.getTrustManagers();

			SSLContext sslContext = null;
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(kms, tms, new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			//HttpsURLConnection.setDefaultHostnameVerifier(new NoopHostnameVerifier());

			//System.out.println("Getting token...");
			URL url = new URL(siscomexUrl + tokenUri);
			HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();

			urlConn.setRequestMethod("POST");
			urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			if (profile != null) {
				urlConn.setRequestProperty("Role-Type", profile);
			} else {
				urlConn.setRequestProperty("Role-Type", Profiles.OPERADOR_PORTUARIO);
			}

			urlConn.setUseCaches(false);
			
			urlConn.setDoInput(true);
			urlConn.setDoOutput(true);
			if (urlConn.getResponseCode() == 200 || urlConn.getResponseCode() == 201) {
				siscomexRedexAuth.setAuthorization(urlConn.getHeaderField("Set-Token"));
				siscomexRedexAuth.setXCSRFToken(urlConn.getHeaderField("X-CSRF-Token"));
				siscomexRedexAuth.setSiscomexOnline(true);
				
				LOGGER.info("Authorization : {}", siscomexRedexAuth.getAuthorization());
				LOGGER.info("X-CSRF-Token : {}", siscomexRedexAuth.getXCSRFToken());
				
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((urlConn.getErrorStream())));
				StringBuilder sb = new StringBuilder();
				String output;
				while ((output = br.readLine()) != null) 
					sb.append(output);

				//System.out.println(urlConn.getResponseMessage() + ": " + sb.toString()); 
						
			}
		} catch (KeyStoreException e) {
			LOGGER.error("KeyStore ERROR {} ", e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("KeyStore Algorithm ERROR {} ", e.getMessage());
		} catch (CertificateException e) {
			LOGGER.error("KeyStore Certificate ERROR {} ", e.getMessage());
		} catch (FileNotFoundException e) {
			LOGGER.error("KeyStore FileNotFound ERROR {} ", e.getMessage());
		}catch(ConnectException e) {
			LOGGER.error("Connect ERROR {} ", e.getMessage());
		} catch (IOException e) {
			LOGGER.error("KeyStore IO ERROR {} ", e.getMessage());
		} catch (UnrecoverableKeyException e) {
			LOGGER.error("KeyManager ERROR {} ", e.getMessage());
		} catch (KeyManagementException e) {
			LOGGER.error("KeyManager init ERROR {} ", e.getMessage());
		}
	}

	private String getProp(String propertie) {
		return PropertiesUtil.getLocalProp().getProperty(propertie);
	}
}
