package br.com.grupolibra.siscomex.util;

public enum SiscomexRestUrlEnum {
	
	ENTREGAR_CONTEINER("/cct/api/ext/carga/entrega-conteiner", "entregasConteineres"),
	ENTREGAR_DUE("/cct/api/ext/carga/entrega-due-ruc", "entregasDocumentoCarga"),
	RECEPCIONAR_NFF("/cct/api/ext/carga/recepcao-nff", "recepcoesNFF"),
	RECEPCIONAR_NFE("/cct/api/ext/carga/recepcao-nfe", "recepcoesNFE"),
	RECEPCIONAR_CONTEINER("/cct/api/ext/carga/recepcao-conteiner", "recepcoesConteineres"),
	RECEPCIONAR_DUE("/cct/api/ext/carga/recepcao-due-ruc", "recepcoesDocumentoCarga"),
	MANIFESTAR("/cct/api/ext/carga/manifestacao-dados-embarque", "ManifestacoesExportacao"),
	UNITIZAR_CARGA("/cct/api/ext/unitizacao/unitizar-carga", "operacaoUnitizacao"),
	DESUNITIZAR_CARGA("/cct/api/ext/unitizacao/desunitizar-carga", "operacaoDesunitizacao"),
	CONSOLIDAR_CARGA("/cct/api/ext/carga/consolidar-carga", "operacoesConsolidacao"),
	CONSULTA_CONTEINERES("/cct/api/ext/carga/conteiner", "Consulta De Conteineres"),
	CONSULTA_DADOS_RESUMIDOS("/due/api/ext/due/consultarDadosResumidosDUE", "Consulta Dados Resumidos"),
	CONSULTA_DUE_POR_NF("/due/api/ext/due", "Consulta Due por NF"),
	CONSULTA_DUE("/cct/api/ext/carga/due-ruc", "Consulta Due"),
	CONSULTA_DAT("/cct/api/ext/documento-transporte/dat", "Consulta Dat");
	
	public String uri;
	
	public String masterTag;
	
	SiscomexRestUrlEnum(String uri, String masterTag) {
		this.uri = uri;
		this.masterTag = masterTag;
	}

}
