package br.com.grupolibra.siscomex.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import br.com.grupolibra.siscomex.util.PropertiesUtil;

@Configuration
public class RestTemplateConfig {
	
//	@Value("${siscomex.timeout}")
//	private int TIMEOUT;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		int timeout = Integer.parseInt(PropertiesUtil.getLocalProp().getProperty("siscomex.timeout"));
		return restTemplateBuilder.setConnectTimeout(timeout).setReadTimeout(timeout).build();
	}
	
}
