package br.com.grupolibra.siscomex.util;

public final class SwaggerStrings {

	private SwaggerStrings() {
	}

	public static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";

	public final class ApiOperationValue {

		private ApiOperationValue() {}

		public static final String ENVIO_SISCOMEX_VALUE = "Envio de XML para Siscomex";
		public static final String ENVIO_SISCOMEX_NOTES = "Serviço de integração com Portal Único de Comércio Exterior";

	}

	public final static class ApiParamValue {

		private ApiParamValue() {}

		public static final String OBJ_ENVIO_SISCOMEX = "XML para envio ao Portal Único de Comércio Exterior";
		public static final String OBJ_ENVIO_SISCOMEX_EXAMPLE = "<recepcoesNFE xsi:schemaLocation='http://www.pucomex.serpro.gov.br/cct RecepcaoNFE.xsd'\r\n" + 
				"                xmlns='http://www.pucomex.serpro.gov.br/cct'\r\n" + 
				"                xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\r\n" + 
				"    <recepcaoNFE>\r\n" + 
				"        <identificacaoRecepcao> ....";
		
	}
	
	public final static class ApiResponseValue {

		private ApiResponseValue() {}
		
		public static final String RETORNO_200 = "Envio efetuado com sucesso!";

		public static final String ERRO_400 = "Ocorreu um erro na requisição, verifique os dados enviados e refaça a operação.";
		
		public static final String ERRO_500 = "Ocorreu uma falha no servidor, por gentileza, tente mais tarde ou contacte o administrador do sistema.";

	}
	
}