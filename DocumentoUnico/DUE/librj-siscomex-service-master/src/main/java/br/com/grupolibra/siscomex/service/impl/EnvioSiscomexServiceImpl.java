package br.com.grupolibra.siscomex.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.grupolibra.siscomex.dto.ConsultaConteineresResponseDTO;
import br.com.grupolibra.siscomex.dto.ConsultaDadosDueResponseDTO;
import br.com.grupolibra.siscomex.dto.ConsultaDadosDueResponseListDTO;
import br.com.grupolibra.siscomex.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.siscomex.dto.Documento;
import br.com.grupolibra.siscomex.dto.DocumentosCarga;
import br.com.grupolibra.siscomex.dto.ListaMensagem;
import br.com.grupolibra.siscomex.dto.ListaRetorno;
import br.com.grupolibra.siscomex.service.EnvioSiscomexService;
import br.com.grupolibra.siscomex.token.AcquireTokenRedexService;
import br.com.grupolibra.siscomex.token.AcquireTokenService;
import br.com.grupolibra.siscomex.util.PropertiesUtil;
import br.com.grupolibra.siscomex.util.SiscomexAuth;
import br.com.grupolibra.siscomex.util.SiscomexMessages;
import br.com.grupolibra.siscomex.util.SiscomexRedexAuth;
import br.com.grupolibra.siscomex.util.SiscomexRestUrlEnum;

@Service
public class EnvioSiscomexServiceImpl implements EnvioSiscomexService {

	private static final String SISCOMEX_URL = "siscomex.url";

	private static final String LOG_HEADERS = "HEADERS : {} ";

	private static final String LOG_URL = "URL : {} ";

	private static final String GENERIC_EXCEPTION = "Fim da recepcão ao siscomex. Generic Exception";

	private static final String RESOURCE_ACCESS_EXCEPTION = "Fim da recepcão ao siscomex. ResourceAccess Exception";

	private static final String HTTP_STATUS_EXCEPTION = "Fim da recepcão ao siscomex. HttpStatus Exception";

	private static final String NR_RUC = "nrRUC";

	private static final String NR_DUE = "nrDUE";

	private static final String NR_CONTEINER = "nrConteiner";

	private static final String MENSAGEM = "mensagem";

	private static final String ERRO = "erro";

	private static final String FLAG = "flag";

	private static final String LISTA_RETORNO = "listaRetorno";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiscomexAuth siscomexAuth;
	
	@Autowired
	private SiscomexRedexAuth siscomexRedexAuth;

	@Autowired
	private AcquireTokenService acquireTokenService;
	
	@Autowired
	private AcquireTokenRedexService acquireTokenRedexService;

	@Autowired
	private RestTemplate rest;

	@Override
	public Map<String, Object> enviaXMLSiscomex(String xml, boolean redex) {
		logger.info("enviaXMLSiscomex");
		logXml(xml);
		HashMap<String, Object> result = new HashMap<>();
		result.put(FLAG, null);
		result.put(ERRO, null);
		try {
			String siscomexUrl = PropertiesUtil.getLocalProp().getProperty(SISCOMEX_URL);
			String url = siscomexUrl + this.getUri(xml);
			HttpHeaders headers = this.getHttpHeaders(redex);
			logger.info(LOG_URL, url);
			logger.info(LOG_HEADERS, headers);
			HttpEntity<String> entity = new HttpEntity<>(xml, headers);
			rest.exchange(url, HttpMethod.POST, entity, String.class);
			result.put(FLAG, false);
			return result;
		} catch (HttpStatusCodeException e) {
			logger.error(e.getResponseBodyAsString());
			HttpStatus statusCode = e.getStatusCode();
			logError(e, statusCode);
			if (statusCode == HttpStatus.BAD_REQUEST) {
				result = setMapErrorResult(e.getResponseBodyAsString(),
						SiscomexMessages.REQUISICAO_MAL_FORMATADA.toString());
			} else if (statusCode == HttpStatus.UNAUTHORIZED) {
				result = setMapErrorResult(e.getMessage(),
						SiscomexMessages.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			} else if (statusCode == HttpStatus.FORBIDDEN) {
				result = setMapErrorResult(e.getResponseBodyAsString(),
						SiscomexMessages.REQUISICAO_NAO_AUTORIZADA.toString());
			} else if (statusCode == HttpStatus.NOT_FOUND) {
				result = setMapErrorResult(e.getMessage(), SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
			} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
				result = setMapErrorResult(e.getResponseBodyAsString(), SiscomexMessages.ERRO_NEGOCIO.toString());
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
				result = setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
				result = setMapErrorResult(e.getMessage(), SiscomexMessages.SERVICO_INDISPONIVEL.toString());
			}
			logger.error(HTTP_STATUS_EXCEPTION);
			return result;
		} catch (ResourceAccessException e) {
			result = setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_TIMEOUT.toString());
			logger.error(RESOURCE_ACCESS_EXCEPTION);
			return result;
		} catch (Exception e) {
			result = setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			logger.error(GENERIC_EXCEPTION);
			return result;
		}
	}

	@Override
	public Map<String, Object> consultaConteiner(List<String> conteineres, boolean redex) {
		logger.info("consultaConteiner");
		logConteineresParam(conteineres);
		HashMap<String, Object> retorno = new HashMap<>();
		try {
			String siscomexUrl = PropertiesUtil.getLocalProp().getProperty(SISCOMEX_URL);
			String url = siscomexUrl + SiscomexRestUrlEnum.CONSULTA_CONTEINERES.uri;
			UriComponentsBuilder builder = null;
			ConsultaConteineresResponseDTO response = null;
			List<ConsultaConteineresResponseDTO> responseLst = new ArrayList<>();
			
			for (String nrConteiner : conteineres) {
				builder = UriComponentsBuilder.fromHttpUrl(url);
				builder.queryParam(NR_CONTEINER, nrConteiner);
			
				URI urlFormatterd = builder.build().encode().toUri();
				HttpHeaders headers = this.getHttpHeaders(redex);
				logger.info(LOG_URL, url);
				logger.info(LOG_HEADERS, headers);
	
				HttpEntity<String> entity = new HttpEntity<>(headers);
				
				logger.info("Url Formated : {} ", urlFormatterd);
				ResponseEntity<ConsultaConteineresResponseDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity,
						ConsultaConteineresResponseDTO.class);
				
				logger.info("Response StatusCode : {} ", responseEntity.getStatusCode().value());
				
				response = responseEntity.getBody();	
				responseLst.add(response);
			}			
			
			List<HashMap<String, Object>> resultList = new ArrayList<>();
			for (ConsultaConteineresResponseDTO listDto :  responseLst) {
				for (ListaRetorno listaRetorno : listDto.getListaRetorno()) {
					HashMap<String, Object> result = new HashMap<>();
					result.put(NR_CONTEINER, listaRetorno.getNumeroConteiner());
					getNumeroDUE(listaRetorno.getDocumentosCarga(), result, listDto.getListaMensagem());
					resultList.add(result);
				}
			}
			retorno.put(LISTA_RETORNO, resultList);
			retorno.put(FLAG, false);
			retorno.put(ERRO, null);
			retorno.put(MENSAGEM, null);
			return retorno;
		} catch (HttpStatusCodeException e) {
			logger.error(HTTP_STATUS_EXCEPTION);
			logger.error(e.getResponseBodyAsString());
			HttpStatus statusCode = e.getStatusCode();
			logError(e, statusCode);
			if (statusCode == HttpStatus.BAD_REQUEST) {
				return setMapErrorResult(e.getResponseBodyAsString(),
						SiscomexMessages.REQUISICAO_MAL_FORMATADA.toString());
			} else if (statusCode == HttpStatus.UNAUTHORIZED) {
				return setMapErrorResult(e.getMessage(),
						SiscomexMessages.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			} else if (statusCode == HttpStatus.FORBIDDEN) {
				return setMapErrorResult(e.getResponseBodyAsString(),
						SiscomexMessages.REQUISICAO_NAO_AUTORIZADA.toString());
			} else if (statusCode == HttpStatus.NOT_FOUND) {
				return setMapErrorResult(e.getMessage(), SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
			} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
				return setMapErrorResult(e.getResponseBodyAsString(), SiscomexMessages.ERRO_NEGOCIO.toString());
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
				return setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
				return setMapErrorResult(e.getMessage(), SiscomexMessages.SERVICO_INDISPONIVEL.toString());
			} else {
				retorno = new HashMap<>();
				retorno.put(FLAG, true);
				retorno.put(ERRO, e.getMessage());
				retorno.put(MENSAGEM, "ERRO");
				retorno.put(LISTA_RETORNO, null);
				return retorno;
			}
		} catch (ResourceAccessException e) {
			logger.error(RESOURCE_ACCESS_EXCEPTION);
			return setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_TIMEOUT.toString());
		} catch (Exception e) {
			logger.error(GENERIC_EXCEPTION);
			return setMapErrorResult(e.getMessage(), SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
		}
	}

	private HttpHeaders getHttpHeaders(boolean redex) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_XML);
		
		if(redex) {
			if (siscomexRedexAuth == null || siscomexRedexAuth.getAuthorization() == null) {
				acquireTokenRedexService.acquireToken();
			} else {
				headers.set("Authorization", siscomexRedexAuth.getAuthorization());
				headers.set("X-CSRF-Token", siscomexRedexAuth.getXCSRFToken());
			}
		} else {
			if (siscomexAuth == null || siscomexAuth.getAuthorization() == null) {
				acquireTokenService.acquireToken();
			} else {
				headers.set("Authorization", siscomexAuth.getAuthorization());
				headers.set("X-CSRF-Token", siscomexAuth.getXCSRFToken());
			}
		}
		return headers;
	}

	private String getUri(String xml) {
		for (SiscomexRestUrlEnum type : SiscomexRestUrlEnum.values()) {
			if (xml.contains(type.masterTag)) {
				return type.uri;
			}
		}
		return null;
	}

	private void getNumeroDUE(List<DocumentosCarga> documentosCarga, HashMap<String, Object> result,
			List<ListaMensagem> listaMensagem) {
		if (documentosCarga == null || documentosCarga.isEmpty()) {
			getMensagem(result, listaMensagem);
			return;
		}
		for (DocumentosCarga documentoCarga : documentosCarga) {
			if (documentoCarga.getDocumentos() == null || documentoCarga.getDocumentos().isEmpty()) {
				getMensagem(result, listaMensagem);
				return;
			}
			ListIterator<Documento> documentoIt =  documentoCarga.getDocumentos().listIterator();
			StringBuilder nrDUE = new StringBuilder();
			StringBuilder nrRuc = new StringBuilder();
			while(documentoIt.hasNext()) {
				Documento documento = documentoIt.next();
				nrDUE.append(documento.getNumeroDUE() + (documentoIt.hasNext() ? ", " : ""));
				nrRuc.append(documento.getNumeroRUC() + (documentoIt.hasNext() ? ", " : ""));
				
			}
			result.put(NR_DUE, nrDUE);
			result.put(NR_RUC, nrRuc);
		}
	}

	private void getMensagem(HashMap<String, Object> result, List<ListaMensagem> listaMensagem) {
		for (ListaMensagem mensagemObj : listaMensagem) {
			if (mensagemObj.getMensagem().contains(result.get(NR_CONTEINER).toString())) {
				result.put(MENSAGEM, mensagemObj.getMensagem());
				result.put(NR_DUE, "");
				result.put(NR_RUC, "");
				return;
			}
		}
	}

	private HashMap<String, Object> setMapErrorResult(String erro, String mensagem) {
		HashMap<String, Object> result = new HashMap<>();
		result.put(FLAG, true);
		result.put(ERRO, erro);
		result.put(MENSAGEM, mensagem);
		result.put(LISTA_RETORNO, null);
		return result;
	}

	/**
	 * 200 Operação realizada com sucesso 400 Requisição mal formatada 401
	 * Requisição requer autenticação 403 Requisição não autorizada 404 Registro não
	 * encontrado 422 Erro de negócio 500 Erro interno do servidor 503 Serviço
	 * indisponível
	 */
	private void logError(HttpStatusCodeException e, HttpStatus statusCode) {
		logger.error("######################## ERROR ########################");
		logger.error("Retorno Siscomex: {} ", statusCode.value());
		logger.error("getMessage: {} ", e.getMessage());
		logger.error("getResponseBodyAsString: {} ", e.getResponseBodyAsString());
		logger.error("getStatusText: {} ", e.getStatusText());
		logger.error("getLocalizedMessage: {} ", e.getLocalizedMessage());
		logger.error("######################## ERROR ########################");
	}
	
	private void logXml(String xml) {
		logger.info("######## enviaXMLSiscomex ########");
		logger.info("############# XML ################");
		logger.info(xml);
		logger.info("############# XML ################");
	}
	
	private void logConteineresParam(List<String> conteineres) {
		logger.info("######## consultaConteiner ##################");
		logger.info("################ Conteineres ################");
		conteineres.forEach(cont -> logger.info(cont));
		logger.info("################ Conteineres ################");
	}
	
	@Override
	public Map<String, Object> consultaDadosResumidos(List<String> dues, boolean redex) {
		logger.info("consultaDadosResumidos");
		HashMap<String, Object> retorno = new HashMap<>();
		List<ConsultaDadosResumidosResponseDTO> resultList = new ArrayList<>();
		for(String due : dues) {
			consultaDadosResumidosPorDUE(resultList, due, redex);
		}
		retorno.put(LISTA_RETORNO, resultList);
		return retorno;
	}

	private void consultaDadosResumidosPorDUE(List<ConsultaDadosResumidosResponseDTO> resultList, String due, boolean redex) {
		try {
			String siscomexUrl = PropertiesUtil.getLocalProp().getProperty(SISCOMEX_URL);
			String url = siscomexUrl + SiscomexRestUrlEnum.CONSULTA_DADOS_RESUMIDOS.uri;
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
			builder.queryParam("numero", due);
			URI urlFormatterd = builder.build().encode().toUri();
			HttpHeaders headers = this.getHttpHeaders(redex);
			logger.info(LOG_URL, url);
			logger.info(LOG_HEADERS, headers);

			HttpEntity<String> entity = new HttpEntity<>(headers);
			ResponseEntity<ConsultaDadosResumidosResponseDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity,
					ConsultaDadosResumidosResponseDTO.class);
			
			logger.info("Response StatusCode : {} ", responseEntity.getStatusCode().value());
			
			ConsultaDadosResumidosResponseDTO response = responseEntity.getBody();
			resultList.add(response);
		} catch (HttpStatusCodeException e) {
			logger.error(HTTP_STATUS_EXCEPTION);
			logger.error(e.getResponseBodyAsString());
			HttpStatus statusCode = e.getStatusCode();
			logError(e, statusCode);
			if (statusCode == HttpStatus.BAD_REQUEST) {
				logger.error(SiscomexMessages.REQUISICAO_MAL_FORMATADA.toString());
			} else if (statusCode == HttpStatus.UNAUTHORIZED) {
				logger.error(SiscomexMessages.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			} else if (statusCode == HttpStatus.FORBIDDEN) {
				logger.error(SiscomexMessages.REQUISICAO_NAO_AUTORIZADA.toString());
			} else if (statusCode == HttpStatus.NOT_FOUND) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
			} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
				logger.error(SiscomexMessages.ERRO_NEGOCIO.toString());
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
				logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
				logger.error(SiscomexMessages.SERVICO_INDISPONIVEL.toString());
			} 
		} catch (ResourceAccessException e) {
			logger.error(RESOURCE_ACCESS_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_TIMEOUT.toString());
		} catch (Exception e) {
			logger.error(GENERIC_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
		}
	}
	
	
	@Override
	public Map<String, Object> consultarDUEporNF(String nf, boolean redex) {
		ResponseEntity<String> Due = null;
		JsonNode dueRetorno = null;
		JsonNode tree = null;
		try {
			String siscomexUrl = PropertiesUtil.getLocalProp().getProperty(SISCOMEX_URL);
			String url = siscomexUrl + SiscomexRestUrlEnum.CONSULTA_DUE_POR_NF.uri;
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
			builder.queryParam("nota-fiscal", nf);
			URI urlFormatterd = builder.build().encode().toUri();
			HttpHeaders headers = this.getHttpHeaders(redex);
			logger.info(LOG_URL, url);
			logger.info(LOG_HEADERS, headers);

			HttpEntity<String> entity = new HttpEntity<>(headers);
			Due = rest.exchange(urlFormatterd, HttpMethod.GET, entity, String.class);			
			
			if (Due.getBody()==null) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
				return null;
			}				
			
			final ObjectMapper mapper = new ObjectMapper();							
			tree = mapper.readTree(Due.getBody().toString());				
			dueRetorno = tree.findValue("rel");
			
			if (dueRetorno==null) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
				return null;				
			}
			
		} catch (HttpStatusCodeException e) {
			logger.error(HTTP_STATUS_EXCEPTION);
			logger.error(e.getResponseBodyAsString());
			HttpStatus statusCode = e.getStatusCode();
			logError(e, statusCode);
			if (statusCode == HttpStatus.BAD_REQUEST) {
				logger.error(SiscomexMessages.REQUISICAO_MAL_FORMATADA.toString());
			} else if (statusCode == HttpStatus.UNAUTHORIZED) {
				logger.error(SiscomexMessages.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			} else if (statusCode == HttpStatus.FORBIDDEN) {
				logger.error(SiscomexMessages.REQUISICAO_NAO_AUTORIZADA.toString());
			} else if (statusCode == HttpStatus.NOT_FOUND) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
			} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
				logger.error(SiscomexMessages.ERRO_NEGOCIO.toString());
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
				logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
				logger.error(SiscomexMessages.SERVICO_INDISPONIVEL.toString());
			} 
		} catch (ResourceAccessException e) {
			logger.error(RESOURCE_ACCESS_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_TIMEOUT.toString());
		} catch (Exception e) {
			logger.error(GENERIC_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
		}		
					
		logger.info(NR_DUE + ": {}", dueRetorno.toString());					
		HashMap<String, Object> retorno = new HashMap<>();			
		retorno.put("numeroDUE", dueRetorno.toString());
		return retorno;			
				
	}
	
	@Override
	public Map<String, Object> consultaDue(String due, boolean redex) {
		HashMap<String, Object> retorno = new HashMap<>();
		List<ConsultaDadosDueResponseDTO> resultList = new ArrayList<>();
		ResponseEntity<ConsultaDadosDueResponseListDTO> responseEntity=null;
		ConsultaDadosDueResponseListDTO response = new ConsultaDadosDueResponseListDTO();
		String mensagemRetorno = "";
		try {
			String siscomexUrl = PropertiesUtil.getLocalProp().getProperty(SISCOMEX_URL);
			String url = siscomexUrl + SiscomexRestUrlEnum.CONSULTA_DUE.uri;
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
			
			if(due.contains(",")) {
				String[] dues = due.split(",");
				for (String due_ : dues) {
					due_ = due_.replaceAll(" ", "");
					due_ = due_.trim();
					builder.queryParam("nrDocumento", due_);
				}
			} else {
				builder.queryParam("nrDocumento", due);
			}			
			
			URI urlFormatterd = builder.build().encode().toUri();
			HttpHeaders headers = this.getHttpHeaders(redex);
			logger.info(LOG_URL, url);
			logger.info(LOG_HEADERS, headers);

			HttpEntity<String> entity = new HttpEntity<>(headers);
			responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, ConsultaDadosDueResponseListDTO.class);			
			
			if (responseEntity.getBody()==null) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
				return null;
			}		
			
			response = responseEntity.getBody();
			logger.info("consultaDue - response : {}", response.toString());			
			retorno.put(LISTA_RETORNO, response.getListaRetorno());
			
		} catch (HttpStatusCodeException e) {
			logger.error(HTTP_STATUS_EXCEPTION);
			logger.error(e.getResponseBodyAsString());
			HttpStatus statusCode = e.getStatusCode();
			logError(e, statusCode);
			mensagemRetorno="consultaDue :  " + e.getMessage();
			
			if (statusCode == HttpStatus.BAD_REQUEST) {
				logger.error(SiscomexMessages.REQUISICAO_MAL_FORMATADA.toString());
			} else if (statusCode == HttpStatus.UNAUTHORIZED) {
				logger.error(SiscomexMessages.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			} else if (statusCode == HttpStatus.FORBIDDEN) {
				logger.error(SiscomexMessages.REQUISICAO_NAO_AUTORIZADA.toString());
			} else if (statusCode == HttpStatus.NOT_FOUND) {
				logger.error(SiscomexMessages.REGISTRO_NAO_ENCONTRADO.toString());
			} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
				logger.error(SiscomexMessages.ERRO_NEGOCIO.toString());
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
				logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
			} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
				logger.error(SiscomexMessages.SERVICO_INDISPONIVEL.toString());
			} 
		} catch (ResourceAccessException e) {
			logger.error(RESOURCE_ACCESS_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_TIMEOUT.toString());
		} catch (Exception e) {
			logger.error(GENERIC_EXCEPTION);
			logger.error(SiscomexMessages.ERRO_INTERNO_SERVIDO.toString());
		}															

		retorno.put(FLAG, false);
		retorno.put(ERRO, mensagemRetorno);
		retorno.put(MENSAGEM, null);
		return retorno;		
				
	}	

}
