
package br.com.grupolibra.siscomex.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "listaRetorno",
    "listaMensagem"
})
public class ConsultaConteineresResponseDTO {

    @JsonProperty("listaRetorno")
    @JsonDeserialize(as = java.util.List.class)
    private List<ListaRetorno> listaRetorno = null;
    @JsonProperty("listaMensagem")
    private List<ListaMensagem> listaMensagem = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("listaRetorno")
    public List<ListaRetorno> getListaRetorno() {
        return listaRetorno;
    }

    @JsonProperty("listaRetorno")
    public void setListaRetorno(List<ListaRetorno> listaRetorno) {
        this.listaRetorno = listaRetorno;
    }

    @JsonProperty("listaMensagem")
    public List<ListaMensagem> getListaMensagem() {
        return listaMensagem;
    }

    @JsonProperty("listaMensagem")
    public void setListaMensagem(List<ListaMensagem> listaMensagem) {
        this.listaMensagem = listaMensagem;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
