
package br.com.grupolibra.siscomex.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "numeroDUE",
    "numeroRUC"
})
public class Documento {

    @JsonProperty("numeroDUE")
    private String numeroDUE;
    @JsonProperty("numeroRUC")
    private String numeroRUC;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("numeroDUE")
    public String getNumeroDUE() {
        return numeroDUE;
    }

    @JsonProperty("numeroDUE")
    public void setNumeroDUE(String numeroDUE) {
        this.numeroDUE = numeroDUE;
    }

    @JsonProperty("numeroRUC")
    public String getNumeroRUC() {
        return numeroRUC;
    }

    @JsonProperty("numeroRUC")
    public void setNumeroRUC(String numeroRUC) {
        this.numeroRUC = numeroRUC;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
