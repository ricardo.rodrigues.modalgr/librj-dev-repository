package br.com.grupolibra.dueredex.navis.webservice;

import java.util.Map;

import com.navis.cargo.business.model.BillOfLading;

/*
 * @SNX_GENERATE [TRUE]
 * @SNX_SCOPE [LIBRA/RIODEJANEIRO]
 * @SNX_TYPE [GROOVY_WS_CODE_EXTENSION]
 * @SNX_VERSION [3.89.1.87]
 */

import br.com.customaduana.fmwportn4.webservice.GenericJsonWS;

public class DueRedexServiceProxy extends GenericJsonWS {
	
	public Object dueRedexService;
	
	public Object[] validaUnitId(Map<String, String> params) {
		try {
			String numeroConteiner = params.get("numeroConteiner");
			
			return dueRedexService.validaUnitId(numeroConteiner);
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	public Object[] generateUnit(Map<String, String> params) {
		try {
			String bookingGkey = params.get("bookingGkey");
			String equipmentOrderItemGkey = params.get("equipmentOrderItemGkey");
			
			String numeroConteiner = params.get("numeroConteiner");
			String pesoBruto = params.get("pesoBruto");
			String lacre1 = params.get("lacre1");
			String lacre2 = params.get("lacre2");
			String lacre3 = params.get("lacre3");
			String lacre4 = params.get("lacre4");
			String excessoFrontal = params.get("excessoFrontal");
			String excessoTraseiro = params.get("excessoTraseiro");
			String excessoAltura = params.get("excessoAltura");
			String excessoDireita = params.get("excessoDireita");
			String excessoEsquerda = params.get("excessoEsquerda");
			
			
			return dueRedexService.generateNewUnit(bookingGkey, equipmentOrderItemGkey, numeroConteiner, pesoBruto, lacre1, lacre2, lacre3, lacre4, excessoFrontal, excessoTraseiro, 
				excessoAltura, excessoDireita, excessoEsquerda);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public Object[] updateUnit(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String pesoBruto = params.get("pesoBruto");
			String lacre1 = params.get("lacre1");
			String lacre2 = params.get("lacre2");
			String lacre3 = params.get("lacre3");
			String lacre4 = params.get("lacre4");
			String excessoFrontal = params.get("excessoFrontal");
			String excessoTraseiro = params.get("excessoTraseiro");
			String excessoAltura = params.get("excessoAltura");
			String excessoDireita = params.get("excessoDireita");
			String excessoEsquerda = params.get("excessoEsquerda");
//			String equipmentOrderItemGkey = params.get("equipmentOrderItemGkey");
			
			return dueRedexService.updateUnit(unitGkey, pesoBruto, lacre1, lacre2, lacre3, lacre4, excessoFrontal, excessoTraseiro,
				excessoAltura, excessoDireita, excessoEsquerda);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String createUnitEvent(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String event    = params.get("event");
			String notes    = params.get("notes");
			return dueRedexService.createUnitEvent(unitGkey, event, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String deleteUnit(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			return dueRedexService.deleteUnit(unitGkey);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public Object[] geraBillOfLadingDoDocumento(Map<String, String> params) {
		try {
			Map<String, String> blMap = toBLMap(params);			
			return dueRedexService.geraBillOfLadingDoDocumento(blMap);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public Map<String, String> toBLMap(Map<String, String> params) {
		Map<String, String> blMap = new HashMap<>();
		blMap.put("blNbr", params.get("blNbr"));
		blMap.put("blNotes", params.get("blNotes"));
		blMap.put("blShipper", params.get("blShipper"));
		blMap.put("blConsignee", params.get("blConsignee"));
		blMap.put("blCustomDFFBkNF", params.get("blCustomDFFBkNF"));
		blMap.put("blitemNbr", params.get("blitemNbr"));
		blMap.put("blitemCommodity", params.get("blitemCommodity"));
		blMap.put("blitemQuantity", params.get("blitemQuantity"));
		blMap.put("blitemPackageType", params.get("blitemPackageType"));
		blMap.put("blitemWeightTotalKg", params.get("blitemWeightTotalKg"));
		blMap.put("blitemMarksAndNumbers", params.get("blitemMarksAndNumbers"));
		blMap.put("blitemNotes", params.get("blitemNotes"));
		blMap.put("novoEmitente", params.get("novoEmitente"));
		blMap.put("emitente.razaosocial", params.get("emitente.razaosocial"));
		blMap.put("emitente.nomecontato", params.get("emitente.nomecontato"));
		blMap.put("emitente.telefone", params.get("emitente.telefone"));
		blMap.put("emitente.email", params.get("emitente.email"));
		blMap.put("novoConsolidador", params.get("novoConsolidador"));
		blMap.put("consolidador.razaosocial", params.get("consolidador.razaosocial"));
		blMap.put("consolidador.nomecontato", params.get("consolidador.nomecontato"));
		blMap.put("consolidador.telefone", params.get("consolidador.telefone"));
		blMap.put("consolidador.email", params.get("consolidador.email"));
		return blMap;
	}
	
	public String createBLEvent(Map<String, String> params) {
		try {
			String blGkey = params.get("blGkey");
			String event    = params.get("event");
			String notes    = params.get("notes");
			return dueRedexService.createBLEvent(blGkey, event, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String createNewDriver(Map<String, String> params) {
		try {
			String nome			= params.get("nome");
			String cpf    		= params.get("cpf");
			String cnh    		= params.get("cnh");
			String passaporte   = params.get("passaporte");
			return dueRedexService.createNewDriver(nome, cpf, cnh, passaporte);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String deleteBL(Map<String, String> params) {
		try {
			String blGkey = params.get("blGkey");
			return dueRedexService.deleteBL(blGkey);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String createUnitDUEEvents(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String notes    = params.get("notes");
			String geraLiberacaoEmbarque = params.get("geraLiberacaoEmbarque");
			return dueRedexService.createUnitDUEEvents(unitGkey, notes, geraLiberacaoEmbarque);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	
	public String applyHoldUnit(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String hold     = params.get("hold");
			String notes    = params.get("notes");
			return dueRedexService.applyHoldUnit(unitGkey, hold, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	
	public String applyPermissionUnit(Map<String, String> params) {
		try {
			String unitGkey		= params.get("unitGkey");
			String permission	= params.get("permission");
			String notes    	= params.get("notes");
			return dueRedexService.applyPermissionUnit(unitGkey, permission, notes);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String createDesbloqueioEvents(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String notes    = params.get("notes");
			String eventsToCreate = params.get("events_to_create");
			return dueRedexService.createDesbloqueioEvents(unitGkey, notes, eventsToCreate);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	public String atualizaBookingShipper(Map<String, String> params) {
		try {
			String bookingGkey = params.get("bookingGkey");
			String shipperGkey = params.get("shipperGkey");
			return dueRedexService.atualizaBookingShipper(bookingGkey, shipperGkey);
		} catch(Exception e) {
			return e.getMessage();
		}
	}	

	public String createCFPEvent(Map<String, String> params) {
		try {
			String unitGkey = params.get("unitGkey");
			String notes    = params.get("notes");
			String responsiblePartyGkey = params.get("responsiblePartyGkey")
			return dueRedexService.createCFPEvent(unitGkey, notes, responsiblePartyGkey);
		} catch(Exception e) {
			return e.getMessage();
		}
	}
	
	


}
