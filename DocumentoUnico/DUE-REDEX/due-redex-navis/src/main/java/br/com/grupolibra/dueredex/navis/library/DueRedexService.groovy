package br.com.grupolibra.dueredex.navis.library;

/*
 * @SNX_GENERATE [TRUE]
 * @SNX_SCOPE [LIBRA/RIODEJANEIRO]
 * @SNX_TYPE [LIBRARY]
 * @SNX_VERSION [3.89.1.87]
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.navis.argo.ContextHelper;
import com.navis.argo.business.api.ServicesManager;
import com.navis.argo.business.atoms.BizRoleEnum;
import com.navis.argo.business.atoms.DataSourceEnum;
import com.navis.argo.business.atoms.LocTypeEnum;
import com.navis.argo.business.atoms.FreightKindEnum;
import com.navis.argo.business.atoms.UnitCategoryEnum;
import com.navis.argo.business.model.CarrierVisit;
import com.navis.argo.business.model.Complex;
import com.navis.argo.business.model.Facility;
import com.navis.argo.business.model.Operator;
import com.navis.argo.business.reference.LineOperator;
import com.navis.argo.business.reference.Commodity;
import com.navis.argo.business.reference.EquipType;
import com.navis.argo.business.reference.Equipment;
import com.navis.argo.business.reference.ScopedBizUnit;
import com.navis.argo.business.reference.Shipper;
import com.navis.cargo.business.model.BillOfLading;
import com.navis.cargo.business.model.BlItem;
import com.navis.cargo.business.model.PackageType;
import com.navis.framework.business.Roastery;
import com.navis.framework.metafields.MetafieldIdFactory;
import com.navis.framework.portal.QueryUtils;
import com.navis.framework.portal.query.DomainQuery;
import com.navis.framework.portal.query.PredicateFactory;
import com.navis.framework.util.BizViolation;
import com.navis.inventory.business.api.UnitManager;
import com.navis.inventory.business.atoms.UfvTransitStateEnum;
import com.navis.inventory.business.imdg.HazardItem;
import com.navis.inventory.business.imdg.Hazards;
import com.navis.inventory.business.imdg.ImdgClass;
import com.navis.inventory.business.units.Unit;
import com.navis.inventory.business.units.UnitFacilityVisit;
import com.navis.orders.business.eqorders.Booking;
import com.navis.orders.business.eqorders.EquipmentOrderItem;
import com.navis.services.business.rules.EventType;
import com.navis.road.business.model.TruckDriver;
import com.navis.road.business.atoms.DriverStatusEnum;
import com.navis.inventory.business.atoms.UnitVisitStateEnum;
import com.navis.argo.business.model.LocPosition;
import com.navis.cargo.business.model.BillOfLadingManagerPea;

import br.com.customaduana.fmwportn4.log.LoggerN4;

public class DueRedexService {

	private LoggerN4 sbLog;

	private static ServicesManager serviceManager = (ServicesManager) Roastery.getBean(ServicesManager.BEAN_ID);
	
	public Object[] validaUnitId(String numeroConteiner) {
		Object[] retorno = new Object[3];
		List<Unit> unitList = getListUnitById(numeroConteiner);
		
		sbLog.appendError("########## generateUnit unitList.size() - " + unitList.size());
		
		if(unitList == null || unitList.isEmpty()) {
			retorno[0] = false;
			retorno[1] = "OK";
			retorno[2] = "";
			return retorno;
		}

		//Percorre as units para verificar se ja existe uma unit ativa com essa numeracao
		for (Unit u : unitList) {
			if (u.isActive()) {
				
				sbLog.appendError("########## generateUnit - ERRO - HAS UNIT ACTIVE");
				
				retorno[0] = true;
				retorno[1] = "NUMERACAO";
				return retorno;
			}
			
			boolean isFreightKindBBK = u.getUnitFreightKind().equals(FreightKindEnum.BBK);			
			if (isFreightKindBBK) {
				retorno[0] = true;
				retorno[1] = "CARGA SOLTA";
				return retorno;
			}			
		}
		
		for (Unit u : unitList) {
			
			String unitISOEquipType = u.getFieldValue(MetafieldIdFactory.valueOf("unitPrimaryUe.ueEquipment.eqEquipType.eqtypId"));
			String unitArchISOEquipType = u.getFieldValue(MetafieldIdFactory.valueOf("unitPrimaryUe.ueEquipment.eqEquipType.eqtypArchetype"));
			sbLog.appendError("########## generateUnit - UNIT  EQIPTYPE " + unitISOEquipType);

			retorno[0] = false;
			retorno[1] = unitArchISOEquipType;
			retorno[2] = unitISOEquipType;
			return retorno;
			
		}
	}	
	
	private Object[] generateNewUnit(String bookingGkey, String equipmentOrderItemGkey, String numeroConteiner, String pesoBruto, String lacre1, String lacre2, String lacre3, String lacre4, 
		String excessoFrontal, String excessoTraseiro, String excessoAltura, String excessoDireita, String excessoEsquerda) {
		
		Object[] retorno = new Object[3];
		
		sbLog.appendError("########## generateNewUnit INIT PARAMS - bookingGkey=" + bookingGkey + " - equipmentOrderItemGkey=" + equipmentOrderItemGkey + " - numeroConteiner=" + numeroConteiner +
			" - pesoBruto=" + pesoBruto + " - lacre1=" + lacre1 + " - lacre2=" + lacre2 + " - lacre3=" + lacre3 + " - lacre4=" + lacre4 + " - excessoFrontal=" + excessoFrontal +
			" - excessoTraseiro=" + excessoTraseiro + " - excessoAltura=" + excessoAltura + " - excessoDireita=" + excessoDireita + " - excessoEsquerda=" + excessoEsquerda);
		
		Long id = null;
		Unit unit = null;
		Booking booking = Booking.hydrate(Long.valueOf(bookingGkey));
		
		sbLog.appendError("########## generateNewUnit - booking - " + booking);
		
		EquipmentOrderItem bookingItem = EquipmentOrderItem.hydrate(Long.valueOf(equipmentOrderItemGkey));
		
		sbLog.appendError("########## generateNewUnit - bookingItem - " + bookingItem);

		if (booking != null) {
			CarrierVisit cv = booking.getEqoVesselVisit();
			sbLog.appendError("########## generateNewUnit - facility1 " + cv.getCvFacility());
			Facility facility = cv.getCvFacility();
			sbLog.appendError("########## generateNewUnit - complex " + booking.getEqoComplex());
			Complex complex = booking.getEqoComplex();
			Equipment equipment = loadEquipment(bookingItem.getEqoiSampleEquipType());
			sbLog.appendError("########## generateNewUnit - equipment " + equipment);
			try {
				UnitManager unitPea = (UnitManager)Roastery.getBean("unitManager");
				UnitFacilityVisit ufv = unitPea.findOrCreatePreadvisedUnit(facility, numeroConteiner.trim(), bookingItem.getEqoiSampleEquipType(), booking.getEqoCategory(), booking.getEqoEqStatus(),
						booking.getEqoLine(), CarrierVisit.getGenericCarrierVisitForMode(complex, LocTypeEnum.TRUCK),  cv, DataSourceEnum.USER_WEB, "DUE-REDEX");

				sbLog.appendError("########## generateNewUnit - ufv " + ufv);
				unit = ufv.getUfvUnit();
				sbLog.appendError("########## generateNewUnit - unit " + unit);
				sbLog.appendError("########## generateNewUnit - unit Equipment " + unit.getUnitPrimaryUe());
				equipment = unit.getPrimaryEq();
				equipment.setEquipmentIdFields(numeroConteiner);

				ufv.setFieldValue("ufvVisibleInSparcs",true);
                ufv.setFieldValue("ufvIsDirectIbToObMove", bookingItem.getFieldValue("eqoiCustomFlexFields.eqoiCustomDFFIsDirectIbToObMove"));

				sbLog.appendError("########## generateNewUnit - unitId = " + numeroConteiner);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitId"),numeroConteiner);
				
				sbLog.appendError("########## generateNewUnit - unitPrimaryUe.ueDepartureOrderItem = " + bookingItem.getEqboiGkey());
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitPrimaryUe.ueDepartureOrderItem"), bookingItem.getEqboiGkey());
				
				sbLog.appendError("########## generateNewUnit - customFlexFields.unitCustomDFFProcessCreator = " + "DUE-REDEX");
				unit.setFieldValue(MetafieldIdFactory.valueOf("customFlexFields.unitCustomDFFProcessCreator"), "DUE-REDEX");
				
				sbLog.appendError("########## generateNewUnit - customFlexFields.unitCustomDFFisPreDispatch = false ");
				unit.setFieldValue(MetafieldIdFactory.valueOf("customFlexFields.unitCustomDFFisPreDispatch"), new Boolean(false));
				
				sbLog.appendError("########## generateNewUnit - updateCategory = " + booking.getEqoCategory());
				unit.updateCategory(booking.getEqoCategory());
				
				sbLog.appendError("########## generateNewUnit - updateRequiresPower = " + (bookingItem.getEqoiTempRequired() != null ? " != null " : " null "));
				unit.updateRequiresPower(bookingItem.getEqoiTempRequired() != null);
				
				sbLog.appendError("########## generateNewUnit - assignToOrder = " + bookingItem + " - " + equipment);
				unit.assignToOrder(bookingItem, equipment);
				
				sbLog.appendError("########## generateNewUnit - getUnitRouting().setRtgPOD1 = " + booking.getEqoPod1());
				unit.getUnitRouting().setRtgPOD1(booking.getEqoPod1());
				
				sbLog.appendError("########## generateNewUnit - getUnitRouting().setRtgPOD2 = " + booking.getEqoPod2());
				unit.getUnitRouting().setRtgPOD2(booking.getEqoPod2());
				
				sbLog.appendError("########## generateNewUnit - getUnitRouting().setRtgPOL = " + booking.getEqoPol());
				unit.getUnitRouting().setRtgPOL(booking.getEqoPol());
				
				sbLog.appendError("########## generateNewUnit - getUnitGoods().setGdsReeferRqmnts = " + bookingItem.getReeferRqmnts());
				unit.getUnitGoods().setGdsReeferRqmnts(bookingItem.getReeferRqmnts());
				
				sbLog.appendError("########## generateNewUnit - unitGoodsAndCtrWtKgAdvised = " + "null");
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitGoodsAndCtrWtKgAdvised"), null);
				
				sbLog.appendError("########## generateNewUnit - unitGoodsAndCtrWtKg = " + Double.valueOf(pesoBruto));
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitGoodsAndCtrWtKg"), Double.valueOf(pesoBruto));
				
				sbLog.appendError("########## generateNewUnit - unitSealNbr1 = " + lacre1);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr1"), lacre1);
				
				sbLog.appendError("########## generateNewUnit - unitSealNbr2 = " + lacre2);
				if(lacre2 != null) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr2"), lacre2);
				}
				
				sbLog.appendError("########## generateNewUnit - unitSealNbr3 = " + lacre3);
				if(lacre3 != null) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr3"), lacre3);
				}
				
				sbLog.appendError("########## generateNewUnit - unitSealNbr4 = " + lacre4);
				if(lacre4 != null) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr4"), lacre4);
				}
				
				sbLog.appendError("########## generateNewUnit - unitOogFrontCm = " + excessoFrontal);
				if(excessoFrontal != null && !"null".equals(excessoFrontal) && !"".equals(excessoFrontal)) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogFrontCm"), Long.valueOf(excessoFrontal));
				}
				
				sbLog.appendError("########## generateNewUnit - unitOogBackCm = " + excessoTraseiro);
				if(excessoTraseiro != null && !"null".equals(excessoTraseiro) && !"".equals(excessoTraseiro)) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogBackCm"), Long.valueOf(excessoTraseiro));
				}
				
				sbLog.appendError("########## generateNewUnit - unitOogTopCm = " + excessoAltura);
				if(excessoAltura != null && !"null".equals(excessoAltura) && !"".equals(excessoAltura)) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogTopCm"), Long.valueOf(excessoAltura));
				}
				
				sbLog.appendError("########## generateNewUnit - unitOogRightCm = " + excessoDireita);
				if(excessoDireita != null && !"null".equals(excessoDireita) && !"".equals(excessoDireita)) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogRightCm"), Long.valueOf(excessoDireita));
				}
				
				sbLog.appendError("########## generateNewUnit - unitOogLeftCm = " + excessoEsquerda);
				if(excessoEsquerda != null && !"null".equals(excessoEsquerda) && !"".equals(excessoEsquerda)) {
					unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogLeftCm"), Long.valueOf(excessoEsquerda));
				}

				sbLog.appendError("########## generateNewUnit - update UNIT");
				com.navis.framework.business.Roastery.getHibernateApi().update(unit);
				com.navis.framework.business.Roastery.getHibernateApi().flush();
				
				sbLog.appendError("########## generateNewUnit - updateUnitWithHazartItem");
				updateUnitWithHazardItem(bookingItem, unit);

				sbLog.appendError("########## generateNewUnit - UNIT GKEY = " + unit.getUnitGkey());
				id = unit.getUnitGkey();

			} catch (BizViolation e) {
				sbLog.appendError("########## generateNewUnit - ERRO BizViolation - Unit cannot be created");
				retorno[0] = true;
				retorno[1] = "Unit cannot be created";
				return null;
			} catch (Exception e) {
				sbLog.appendError("########## generateNewUnit - ERRO Exception - Unit cannot be created");
				sbLog.appendError("########## generateNewUnit - ERRO Exception - " + e.getMessage());
				retorno[0] = true;
				retorno[1] = "Unit cannot be created";
				return null;
			}
		}
		sbLog.appendError("########## generateNewUnit - OK");
		retorno[0] = false;
		retorno[1] = "OK";
		retorno[2] = unit.getUnitGkey().toString();
		
		sbLog.appendError("########## generateUnit - retorno - OK");
		return retorno;
	}
	
	private Unit generateNewUnitBl(String numeroConteiner) {
		
		Unit unit = null;
		
		Operator op = Operator.findOperator("LIBRA");
		sbLog.appendError("########## generateNewUnit - findOperator - OK ");
		
		Complex complex = Complex.findComplex("RIODEJANEIRO", op);
		sbLog.appendError("########## generateNewUnit - findComplex - OK ");

		Facility facility = Facility.findFacility("LLRR", complex);				// cv.getCvFacility();
		sbLog.appendError("########## generateNewUnit - getCvFacility - OK ");	

		//CarrierVisit cv = CarrierVisit.findVesselVisit(facility, "REDEX");
		CarrierVisit cv = CarrierVisit.getGenericVesselVisit(complex);	
		sbLog.appendError("########## generateNewUnit - getGenericVesselVisit - OK ");		

		LineOperator lo = LineOperator.unknownLineOperator;
		
		UnitManager unitPea = (UnitManager)Roastery.getBean("unitManager");
		sbLog.appendError("########## generateNewUnit - UnitManager - OK ");			
				
		EquipType eqt = EquipType.getUnknownEquipType(); 
		sbLog.appendError("########## generateNewUnit - findEquipType - OK eqt : " + eqt);
		
		UnitCategoryEnum uCategory = UnitCategoryEnum.EXPORT;
		
		UnitFacilityVisit ufv = unitPea.findOrCreatePreadvisedUnit(facility, numeroConteiner.trim(), eqt, uCategory, FreightKindEnum.BBK,
						lo, cv, cv, DataSourceEnum.USER_WEB, "DUE-REDEX");
		sbLog.appendError("########## generateNewUnit - findOrCreatePreadvisedUnit - OK ");
		//unit.setUnitActiveUfv(ufv);	
		unit = ufv.getUfvUnit();
		sbLog.appendError("########## generateNewUnit - setUnitActiveUfv - OK ");

		
		//unit.updateUnitVisitState(UnitVisitStateEnum.ACTIVE);		
		//sbLog.appendError("########## generateNewUnit - updateUnitVisitState - OK ");	

		/*UnitCategoryEnum uCategory = UnitCategoryEnum.EXPORT;
		sbLog.appendError("########## generateNewUnit - updateCategory = " + uCategory);
		unit.updateCategory(uCategory);*/
		
		//unit.updateFreightKind(FreightKindEnum.BBK);
		
		//unit.updateLineOperator(lo);
		
		com.navis.framework.business.Roastery.getHibernateApi().update(unit);
		com.navis.framework.business.Roastery.getHibernateApi().flush();
		sbLog.appendError("########## generateNewUnit - flush 1 ");	
		
		/*Unit unitPA = Unit.hydrate(unit.getUnitGkey());		
		Unit unitContaneirezed = unitPea.createBreakbulkUnitInContainer(facility, unitPA, numeroConteiner + "-2");
		sbLog.appendError("########## generateNewUnit - createBreakbulkUnitInContainer OK ");	
		com.navis.framework.business.Roastery.getHibernateApi().save(unitContaneirezed);
		com.navis.framework.business.Roastery.getHibernateApi().flush();
		sbLog.appendError("########## generateNewUnit - flush 2 ");	*/
		
		//UnitFacilityVisit ufv = unitPea.findOrCreatePreadvisedUnit(facility, numeroConteiner.trim(), bookingItem.getEqoiSampleEquipType(), booking.getEqoCategory(), booking.getEqoEqStatus(),
		//booking.getEqoLine(), CarrierVisit.getGenericCarrierVisitForMode(complex, LocTypeEnum.TRUCK),  cv, DataSourceEnum.USER_WEB, "DUE-REDEX");
		//sbLog.appendError("########## generateNewUnit - ufv " + ufv);		
			

		
		//sbLog.appendError("########## generateNewUnit - unitId = " + numeroConteiner);
		//unit.setFieldValue(MetafieldIdFactory.valueOf("unitId"),numeroConteiner);
		
		//sbLog.appendError("########## generateNewUnit - customFlexFields.unitCustomDFFProcessCreator = " + "DUE-REDEX");
		//unit.setFieldValue(MetafieldIdFactory.valueOf("customFlexFields.unitCustomDFFProcessCreator"), "DUE-REDEX");	

		//sbLog.appendError("########## generateNewUnit - customFlexFields.unitCustomDFFisPreDispatch = false ");
		//unit.setFieldValue(MetafieldIdFactory.valueOf("customFlexFields.unitCustomDFFisPreDispatch"), new Boolean(false));

		
		/*Unit unitUp = Unit.hydrate(unit.getUnitGkey());
		sbLog.appendError("########## generateNewUnit -  Unit.hydrate - OK ");
		
		unitUp.updateUnitVisitState(UnitVisitStateEnum.DEPARTED);		
		sbLog.appendError("########## generateNewUnit - updateUnitVisitState - OK ");	

		UnitCategoryEnum uCategory = UnitCategoryEnum.EXPORT;
		sbLog.appendError("########## generateNewUnit - updateCategory = " + uCategory);
		unitUp.updateCategory(uCategory);
		
		unitUp.updateFreightKind(FreightKindEnum.BBK);
		
		unitUp.updateLineOperator(lo);

		com.navis.framework.business.Roastery.getHibernateApi().update(unitUp);
		com.navis.framework.business.Roastery.getHibernateApi().flush();	*/
		
		//unitPea.createBreakbulkUnitInContainer(facility, unit, numeroConteiner);
 		//sbLog.appendError("########## generateNewUnit - createBreakbulkUnitInContainer - OK unit:" + unit);
		
		/*unit = unitPea.findOrCreateIncomingBreakbulkUnit(complex, numeroConteiner, cv);
		sbLog.appendError("########## generateNewUnit - findOrCreateIncomingBreakbulkUnit - OK ");	
		
		unit = unitPea.createBreakbulkUnitInContainer(facility, unit, numeroConteiner);
		sbLog.appendError("########## generateNewUnit - createBreakbulkUnitInContainer - OK ");						
						
		com.navis.framework.business.Roastery.getHibernateApi().save(unit);
		com.navis.framework.business.Roastery.getHibernateApi().flush();	*/
		
		//sbLog.appendError("########## generateNewUnit - save UNIT - gkey :" + unitUp.getUnitGkey());
				
		return unit;
				
	}	
	
	public Object[] updateUnit(String unitGkey, String pesoBruto, String lacre1, String lacre2, String lacre3, String lacre4,
		String excessoFrontal, String excessoTraseiro, String excessoAltura, String excessoDireita, String excessoEsquerda) {
		Object[] retorno = new Object[2];
		try {
			sbLog.appendError("########## updateUnit INIT PARAMS - unitGkey=" + unitGkey + " - pesoBruto=" + pesoBruto + " - lacre1=" + lacre1 + " - lacre2=" + lacre2 + " - lacre3=" + lacre3 + " - lacre4=" + lacre4 + " - excessoFrontal=" + excessoFrontal +
				" - excessoTraseiro=" + excessoTraseiro + " - excessoAltura=" + excessoAltura + " - excessoDireita=" + excessoDireita + " - excessoEsquerda=" + excessoEsquerda);
			
			Unit unit = Unit.hydrate(Long.valueOf(unitGkey));
			
//			EquipmentOrderItem bookingItem = EquipmentOrderItem.hydrate(Long.valueOf(equipmentOrderItemGkey));
//			unit.setFieldValue(MetafieldIdFactory.valueOf("unitPrimaryUe.ueDepartureOrderItem"), bookingItem.getEqboiGkey());
			
			sbLog.appendError("########## generateNewUnit - unitGoodsAndCtrWtKg = " + Double.valueOf(pesoBruto));
			unit.setFieldValue(MetafieldIdFactory.valueOf("unitGoodsAndCtrWtKg"), Double.valueOf(pesoBruto));
			
			sbLog.appendError("########## generateNewUnit - unitSealNbr1 = " + lacre1);
			unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr1"), lacre1);
			
			sbLog.appendError("########## generateNewUnit - unitSealNbr2 = " + lacre2);
			if(lacre2 != null) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr2"), lacre2);
			}
			
			sbLog.appendError("########## generateNewUnit - unitSealNbr3 = " + lacre3);
			if(lacre3 != null) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr3"), lacre3);
			}
			
			sbLog.appendError("########## generateNewUnit - unitSealNbr4 = " + lacre4);
			if(lacre4 != null) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitSealNbr4"), lacre4);
			}
			
			sbLog.appendError("########## generateNewUnit - unitOogFrontCm = " + excessoFrontal);
			if(excessoFrontal != null && !"null".equals(excessoFrontal) && !"".equals(excessoFrontal)) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogFrontCm"), Long.valueOf(excessoFrontal));
			}else {
			sbLog.appendError("########## generateNewUnit - unitOogFrontCm = NULL " + excessoFrontal);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogFrontCm"), Long.valueOf("0"));
			}
			
			sbLog.appendError("########## generateNewUnit - unitOogBackCm = " + excessoTraseiro);
			if(excessoTraseiro != null && !"null".equals(excessoTraseiro) && !"".equals(excessoTraseiro)) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogBackCm"), Long.valueOf(excessoTraseiro));
			}else {
			sbLog.appendError("########## generateNewUnit - unitOogBackCm = NULL " + excessoTraseiro);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogBackCm"), Long.valueOf("0"));
			}
			
			sbLog.appendError("########## generateNewUnit - unitOogTopCm = " + excessoAltura);
			if(excessoAltura != null && !"null".equals(excessoAltura) && !"".equals(excessoAltura)) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogTopCm"), Long.valueOf(excessoAltura));
			}else {
			sbLog.appendError("########## generateNewUnit - unitOogTopCm NULL = " + excessoAltura);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogTopCm"), Long.valueOf("0"));
			}
			
			sbLog.appendError("########## generateNewUnit - unitOogRightCm = " + excessoDireita);
			if(excessoDireita != null && !"null".equals(excessoDireita) && !"".equals(excessoDireita)) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogRightCm"), Long.valueOf(excessoDireita));
			}else {
			sbLog.appendError("########## generateNewUnit - unitOogRightCm NULL = " + excessoDireita);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogRightCm"), Long.valueOf("0"));
			}
			
			sbLog.appendError("########## generateNewUnit - unitOogLeftCm = " + excessoEsquerda);
			if(excessoEsquerda != null && !"null".equals(excessoEsquerda) && !"".equals(excessoEsquerda)) {
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogLeftCm"), Long.valueOf(excessoEsquerda));
			}else {
			sbLog.appendError("########## generateNewUnit - unitOogLeftCm NULL = " + excessoEsquerda);
				unit.setFieldValue(MetafieldIdFactory.valueOf("unitOogLeftCm"), Long.valueOf("0"));
			}
			
			com.navis.framework.business.Roastery.getHibernateApi().update(unit);
			
			sbLog.appendError("########## updateUnit - retorno - OK");
			retorno[0] = false;
			retorno[1] = "OK";
			return retorno;
		} catch (BizViolation e) {
			sbLog.appendError("########## updateUnit - ERRO BizViolation - Unit cannot be created");
			retorno[0] = true;
			retorno[1] = "Unit cannot be created";
			return null;
		} catch (Exception e) {
			sbLog.appendError("########## updateUnit - ERRO Exception - Unit cannot be created");
			sbLog.appendError("########## updateUnit - ERRO Exception - " + e.getMessage());
			retorno[0] = true;
			retorno[1] = "Unit cannot be created";
			return null;
		}
	}

	private List<Unit> getListUnitById(String unitId) {
		
		sbLog.appendError("########## getListUnitById - INIT PARAM unitId - " + unitId);
		
		//Verifica se o nome da unit esta no padrao para ter o ultimo digito o digito verificador
		boolean hasDigito = Equipment.isBicContainerId(unitId);
		DomainQuery dq = QueryUtils.createDomainQuery("Unit");
		//se tiver o digito verificador faz um like com todas as units que tem o numero igual desprezando o digito verificador
		if (hasDigito) {
			dq.addDqPredicate(PredicateFactory.like(MetafieldIdFactory.valueOf("unitId"), unitId.substring(0, unitId.length() -1) + "%"));
		} else {
			dq.addDqPredicate(PredicateFactory.eq(MetafieldIdFactory.valueOf("unitId"), unitId));
		}
		List<Unit> units = (List<Unit>) Roastery.getHibernateApi().findEntitiesByDomainQuery(dq);
		
		sbLog.appendError("########## getListUnitById - RETURN UNITS units.size() - " + units.size());
		return units;
	}

	private boolean validaFacilityUnit(Unit unit, Object[] retorno) {
		
		sbLog.appendError("########## validaFacilityUnit");
		
		Facility facility = ContextHelper.getThreadFacility();

		UnitFacilityVisit unitFacilityAtiva = unit.getUnitActiveUfvNowActive();
		if (facility.getFcyGkey().equals(unitFacilityAtiva.getUfvFacility().getFcyGkey())) {
			UfvTransitStateEnum tState = unitFacilityAtiva.getUfvTransitState();
			if (tState.equals(UfvTransitStateEnum.S40_YARD) || tState.equals(UfvTransitStateEnum.S30_ECIN) || tState.equals(UfvTransitStateEnum.S50_ECOUT)){
				sbLog.appendError("########## validaFacilityUnit - ERRO - Nao e possivel utilizar este numero de conteiner 1");
				retorno[0] = true;
				retorno[1] = "Nao e possivel utilizar este numero de conteiner.";
				return false;
			}
		} else {
			boolean inFacility = false;
			Set set = unit.getUnitUfvSet();
			for (Object obj: set) {
				if (obj instanceof UnitFacilityVisit) {
					UnitFacilityVisit unitFacility = (UnitFacilityVisit) obj;
					if (facility.getFcyGkey().equals(unitFacility.getUfvFacility().getFcyGkey())) {
						inFacility = true;
						UfvTransitStateEnum tState = unitFacility.getUfvTransitState();
						if (!(tState.equals(UfvTransitStateEnum.S10_ADVISED) || tState.equals(UfvTransitStateEnum.S20_INBOUND))) {
							sbLog.appendError("########## validaFacilityUnit - ERRO - Nao e possivel utilizar este numero de conteiner 2");
							retorno[0] = true;
							retorno[1] = "Nao e possivel utilizar este numero de conteiner.";
							return false;
						}
					}
				}
			}
			if (!inFacility) {
				unit = null;
			}
		}
		sbLog.appendError("########## validaFacilityUnit - RETURN TRUE");
		return true;
	}

	private Equipment loadEquipment(EquipType equipType) {
		
		sbLog.appendError("########## loadEquipment");
		
		if (equipType != null) {
			DomainQuery dqEquipment = QueryUtils.createDomainQuery("Equipment");
			String eqtypeGkey = String.valueOf(equipType.getEqtypGkey());
			dqEquipment.addDqPredicate(PredicateFactory.eq(MetafieldIdFactory.valueOf("eqEquipType"), eqtypeGkey));
			List<Equipment> equipments = (List<Equipment>) Roastery.getHibernateApi().findEntitiesByDomainQuery(dqEquipment);
			Equipment equipment = null;
			if (equipments != null && equipments.size() > 0) {
				equipment = equipments.get(0);
			}
			if (equipment != null) {
				sbLog.appendError("########## loadEquipment - RETURN EQUIPAMENT");
				return equipment;
			}
		}
		sbLog.appendError("########## loadEquipment - RETURN NULL");
		return null;
	}

	private void updateUnitWithHazardItem(EquipmentOrderItem equipment, Unit unit) {
		sbLog.appendError("########## updateUnitWithHazardItem");
		if (equipment.getEqoiHazards() != null && equipment.getEqoiHazards().getHazardItemsIterator() != null) {
			Iterator<HazardItem> it = equipment.getEqoiHazards().getHazardItemsIterator();
			Hazards hazard = Hazards.createHazardsEntity();
			while (it.hasNext()) {
				HazardItem item = it.next();
				ImdgClass imdgClass = item.getHzrdiImdgClass();
				HazardItem hi = HazardItem.createHazardItemEntity(hazard, imdgClass, item.getHzrdiUNnum());
			}
			sbLog.appendError("########## updateUnitWithHazardItem - attachHazards");
			unit.attachHazards(hazard);
		} else {
			sbLog.appendError("########## updateUnitWithHazardItem - SEM HAZARDS");
		}
	}
	
	public String createUnitEvent(String unitGkey, String event, String notes) {
		sbLog.appendInfo("createUnitEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null) {
				EventType eventType = EventType.findEventType(event);
				if (eventType == null) {
					return "Evento " + event + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	public String deleteUnit(String unitGkey) {
		try {
			Unit unit = Unit.hydrate(unitGkey);
			UnitManager unitPea = (UnitManager)Roastery.getBean("unitManager");
			unitPea.purgeUnit(unit);
			return "OK";
		} catch(Exception e) {
			return "Erro - " + e.getMessage();
		}
	}
	
	public Object[] geraBillOfLadingDoDocumento(Map<String, String> blMap) {
		sbLog.appendError("########## geraBillOfLadingDoDocumento");
		
		Object[] retorno = new Object[2];
		
		logBlMap(blMap);
		
		/*BL*/
		BillOfLading bl = new BillOfLading();
		
		sbLog.appendError("########## geraBillOfLadingDoDocumento - PREENCHENDO BL ");
		try {
			if(blMap.get("novoEmitente") != null && !blMap.get("novoEmitente").isEmpty() ) {
				sbLog.appendError("########## geraBillOfLadingDoDocumento - NOVO EMITENTE ");
				bl.setBlShipper(createNewShipper(blMap, "EMITENTE"));
			} else {
				sbLog.appendError("########## geraBillOfLadingDoDocumento - EMITENTE EXISTENTE - " + blMap.get("blShipper"));
				bl.setBlShipper(getShipperById(blMap.get("blShipper")));
			}
		} catch(Exception e) {
			sbLog.appendError("########## geraBillOfLadingDoDocumento - ERRO EMITENTE");
			
			retorno[0] = "";
			retorno[1] = "ERRO ao vincular Emitente";
			return;
		}
			
		if(blMap.get("blConsignee") != null && !blMap.get("blConsignee").isEmpty() ) {
			
			try {
				if(blMap.get("novoConsolidador") != null && !blMap.get("novoConsolidador").isEmpty() ) {
					
					if(blMap.get("blConsignee").equals(blMap.get("blShipper"))) {
						sbLog.appendError("########## geraBillOfLadingDoDocumento - EMITENTE CONSOLIDADOR - " + blMap.get("blConsignee"));
						bl.setBlConsignee(getShipperById(blMap.get("blConsignee")));
					} else {
						sbLog.appendError("########## geraBillOfLadingDoDocumento - NOVO CONSOLIDADOR ");
						bl.setBlConsignee(createNewShipper(blMap, "CONSOLIDADOR"));
					}
				} else {
					bl.setBlConsignee(getShipperById(blMap.get("blConsignee")));
				}
				
				bl.setFieldValue(MetafieldIdFactory.valueOf("customFlexFields.blCustomDFFBkNF"), blMap.get("blCustomDFFBkNF"));
			} catch(Exception e) {
				sbLog.appendError("########## geraBillOfLadingDoDocumento - ERRO CONSOLIDADOR");
				
				retorno[0] = "";
				retorno[1] = "ERRO ao vincular Consolidador";
				return;
			}
		}
		bl.setBlNbr(blMap.get("blNbr"));
		bl.setBlNotes(blMap.get("blNotes"));
		
		UnitCategoryEnum blCategory = UnitCategoryEnum.EXPORT;
		sbLog.appendError("########## geraBillOfLadingDoDocumento - BL CATEGORY - " + blCategory);
		bl.setBlCategory(blCategory);
		
		Operator op = Operator.findOperator("LIBRA");
		Complex c = Complex.findComplex("RIODEJANEIRO", op);
		CarrierVisit cv = CarrierVisit.getGenericVesselVisit(c);
		bl.setBlCarrierVisit(cv);
		bl.setBlComplex(c);
		
		LineOperator lo = LineOperator.unknownLineOperator;
		bl.setBlLineOperator(lo);
		/*BL*/
		
		Long blGkey = com.navis.framework.business.Roastery.getHibernateApi().save(bl);
		com.navis.framework.business.Roastery.getHibernateApi().flush();
		BillOfLading.generateBLGid(bl);
		Long blId=bl.getBlGid();
		
		/*BLITEM*/
		BlItem blItem = new BlItem();
		
		blItem = blItem.createBlItem(bl, getCommodityById(blMap.get("blitemCommodity")), getPackageTypeById(blMap.get("blitemPackageType")), false)
		
		blItem.setBlitemNbr(blMap.get("blitemNbr"));
		
		sbLog.appendError("########## geraBillOfLadingDoDocumento - ITEM COMMODITY - " + blMap.get("blitemCommodity"));
//		try {
//			blItem.setBlitemCommodity(getCommodityById(blMap.get("blitemCommodity")));
//		} catch(Exception e) {
//			return "ERRO ao buscar NCM";
//		}
		sbLog.appendError("########## geraBillOfLadingDoDocumento - ITEM PACKAGE - " + blMap.get("blitemPackageType"));
//		try {
//			blItem.setBlitemPackageType(getPackageTypeById(blMap.get("blitemPackageType")));
//		} catch(Exception e) {
//			return "ERRO ao buscar Embalagem";
//		}
		
		blItem.setBlitemQuantity(new Double(blMap.get("blitemQuantity")));
		blItem.setBlitemWeightTotalKg(new Double(blMap.get("blitemWeightTotalKg")));
		blItem.setBlitemMarksAndNumbers(blMap.get("blitemMarksAndNumbers"));
		blItem.setBlitemNotes(blMap.get("blitemNotes"));
		
		blItem.createDefaultLot();
		
		/*BLITEM*/
		
		sbLog.appendError("########## geraBillOfLadingDoDocumento - SETANDO ITEM NO BL ");
		blItem.setBlitemBl(bl);
		Set<BlItem> blItemSet = new HashSet<>();
		blItemSet.add(blItem);
		bl.setBlItems(blItemSet);
		
		sbLog.appendError("########## geraBillOfLadingDoDocumento - SALVANDO BL ");
				
		blGkey = com.navis.framework.business.Roastery.getHibernateApi().save(bl);
		com.navis.framework.business.Roastery.getHibernateApi().flush();
		
		sbLog.appendError("########## geraBillOfLadingDoDocumento - BL OK - GKEY: " + blGkey);		
		
		Unit unitRetorno = generateNewUnitBl(blId.toString() + "-0-1");				
		sbLog.appendError("########## generateNewUnit - Retorno - OK ");					
		
		Unit unitAssocia = Unit.hydrate(unitRetorno.getUnitGkey());
		BillOfLading blAssocia = BillOfLading.hydrate(Long.valueOf(blGkey));
		BillOfLadingManagerPea billManager = new BillOfLadingManagerPea();
		billManager.assignUnitBillOfLading(unitAssocia, blAssocia);			
		com.navis.framework.business.Roastery.getHibernateApi().update(blAssocia);
		com.navis.framework.business.Roastery.getHibernateApi().flush();	
		sbLog.appendError("########## geraBillOfLadingDoDocumento - assignUnitBillOfLading - OK ");							
		
		retorno[0] = unitRetorno.getUnitId();
		retorno[1] = "OK";
		return retorno; 
		
	}
	
	public PackageType getPackageTypeById(String packageTypeId) {
		StringBuilder hql = new StringBuilder(" from PackageType where pkgtypeGkey = " + packageTypeId);
		sbLog.appendError("#### HQL : " + hql.toString());
		List<PackageType> packageType = com.navis.framework.business.Roastery.getHibernateApi().find(hql.toString());
		sbLog.appendError("########## ACHOU PackageType -> " + packageType.get(0).getPkgtypeId());
		return packageType.get(0);
	}
	
	public Commodity getCommodityById(String idCommodity) {
		sbLog.appendError("########## getCommodityById - ID = " + idCommodity);
		Commodity commodity = Commodity.findCommodity(idCommodity);
		//sbLog.appendError("########## ACHOU Commodity -> " + commodity.getCmdyShortName());
		return commodity;
	}
	
	public ScopedBizUnit getShipperById(String shipperId) {
		sbLog.appendError("########## getShipperById");
		StringBuilder hql = new StringBuilder(" from ScopedBizUnit where bzuId = '" + shipperId + "'");
		sbLog.appendError("#### HQL : " + hql.toString());
		List<ScopedBizUnit> shipper = com.navis.framework.business.Roastery.getHibernateApi().find(hql.toString());
		sbLog.appendError("########## ACHOU SHIPPER -> " + shipper.get(0).bzuId);
		return shipper.get(0);
	}
	
	public ScopedBizUnit createNewShipper(Map<String, String> blMap, String tipo) {
		sbLog.appendError("########## createNewShipper");

		String t = tipo.toLowerCase();
		
		sbLog.appendError("########## createNewShipper - t = " + t);
		
		String entidade = "";
		if ("EMITENTE".equals(tipo)) {
			entidade = "blShipper";
		} else if ("CONSOLIDADOR".equals(tipo)) {
			entidade = "blConsignee";
		}
		
		sbLog.appendError("########## createNewShipper - entidade = " + entidade);
		
		ScopedBizUnit shipper = new Shipper();
		BizRoleEnum role = BizRoleEnum.SHIPPER;
		
		sbLog.appendError("########## createNewShipper - role = " + role);
		
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuId"), blMap.get(entidade));
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuCtct.ctctWebSiteURL"), formataCNPJ(blMap.get(entidade)));
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuName"), blMap.get(t+".razaosocial"));
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuCtct.ctctName"), blMap.get(t+".nomecontato"));
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuCtct.ctctTel"), blMap.get(t+".telefone"));
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuCtct.ctctEmailAddress"), blMap.get(t+".email"));
		
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuRole"), role);
		shipper.setFieldValue(MetafieldIdFactory.valueOf("bzuCtct.ctctSms"), "CNPJ");
		
		Long shipperGkey = com.navis.framework.business.Roastery.getHibernateApi().save(shipper);
		com.navis.framework.business.Roastery.getHibernateApi().flush();
		
		sbLog.appendError("########## createNewShipper - shipperGkey = " + shipperGkey);
		
		return ScopedBizUnit.hydrate(shipperGkey);
	}
	
	public String createBLEvent(String blGkey, String event, String notes) {
		sbLog.appendInfo("createBLEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			BillOfLading bl = BillOfLading.hydrate(blGkey);
			if (bl != null && bl.getBlGkey() != null) {
				EventType eventType = EventType.findEventType(event);
				if (eventType == null) {
					return "Evento " + event + " nao cadastrado no Navis";
				}
				serviceManager.recordEvent(eventType, notes, null, null, null, bl, null);
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	public String formataCNPJ(String cnpj) {
		Pattern pattern = Pattern.compile("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})");
		Matcher matcher = pattern.matcher(cnpj);
		if(matcher.find()){
			cnpj = matcher.replaceAll("\$1.\$2.\$3/\$4-\$5");
		}
		return cnpj;
	}
	
	
	
	public void logBlMap(Map<String, String> blMap) {
		sbLog.appendError("########## toBLMap - blNbr : " + blMap.get("blNbr"));
		sbLog.appendError("########## toBLMap - blNotes : " + blMap.get("blNotes"));
		sbLog.appendError("########## toBLMap - blShipper : " + blMap.get("blShipper"));
		sbLog.appendError("########## toBLMap - blConsignee : " + blMap.get("blConsignee"));
		sbLog.appendError("########## toBLMap - blCustomDFFBkNF : " + blMap.get("blCustomDFFBkNF"));
		sbLog.appendError("########## toBLMap - blitemNbr : " + blMap.get("blitemNbr"));
		sbLog.appendError("########## toBLMap - blitemCommodity : " + blMap.get("blitemCommodity"));
		sbLog.appendError("########## toBLMap - blitemQuantity : " + blMap.get("blitemQuantity"));
		sbLog.appendError("########## toBLMap - blitemPackageType : " + blMap.get("blitemPackageType"));
		sbLog.appendError("########## toBLMap - blitemWeightTotalKg : " + blMap.get("blitemWeightTotalKg"));
		sbLog.appendError("########## toBLMap - blitemMarksAndNumbers : " + blMap.get("blitemMarksAndNumbers"));
		sbLog.appendError("########## toBLMap - blitemNotes : " + blMap.get("blitemNotes"));
		sbLog.appendError("########## toBLMap - novoEmitente : " + blMap.get("novoEmitente"));
		sbLog.appendError("########## toBLMap - emitente.razaosocial : " + blMap.get("emitente.razaosocial"));
		sbLog.appendError("########## toBLMap - emitente.nomecontato : " + blMap.get("emitente.nomecontato"));
		sbLog.appendError("########## toBLMap - emitente.telefone : " + blMap.get("emitente.telefone"));
		sbLog.appendError("########## toBLMap - emitente.email : " + blMap.get("emitente.email"));
		sbLog.appendError("########## toBLMap - novoConsolidador : " + blMap.get("novoConsolidador"));
		sbLog.appendError("########## toBLMap - consolidador.razaosocial : " + blMap.get("consolidador.razaosocial"));
		sbLog.appendError("########## toBLMap - consolidador.nomecontato : " + blMap.get("consolidador.nomecontato"));
		sbLog.appendError("########## toBLMap - consolidador.telefone : " + blMap.get("consolidador.telefone"));
		sbLog.appendError("########## toBLMap - consolidador.email : " + blMap.get("consolidador.email"));
	}
	
	
	
	public String createNewDriver(String nome, String  cpf, String cnh, String passaporte) {
		sbLog.appendError("createTruckDriver - PARAMS - NOME: " + nome + " - CPF: " + cpf + " - CNH: " + cnh + " - PASSAPORTE: " + passaporte);
		try {
			TruckDriver truckDriver = new TruckDriver();
			truckDriver.setDriverName(nome);
			truckDriver.setDriverCardId((cnh == null || "".equals(cnh)) ? passaporte : cnh);
			truckDriver.setDriverFlexString01((cpf == null || "".equals(cpf)) ? passaporte : cpf);
			truckDriver.setDriverId( "card="+truckDriver.getDriverCardId() );
			truckDriver.setDriverStatus(DriverStatusEnum.OK);
			Roastery.getHibernateApi().saveOrUpdate(truckDriver);
			Roastery.getHibernateApi().flush();
	
			return "OK";
			
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	public String deleteBL(String blGkey) {
		sbLog.appendError("deleteBL");
		try {
			
			BillOfLading bl = BillOfLading.hydrate(blGkey);
			if(bl != null) {
				sbLog.appendError("deleteBL - BL FINDED");
				BillOfLading.deleteBl(bl.getGkey());
			} else {
				sbLog.appendError("deleteBL - BL NOT FINDED - TRYING DOMAINQUERY");
				DomainQuery dq = QueryUtils.createDomainQuery("BillOfLading");
				dq.addDqPredicate(PredicateFactory.eq(MetafieldIdFactory.valueOf("gkey"), blGkey));
				List<BillOfLading> bls = (List<BillOfLading>) Roastery.getHibernateApi().findEntitiesByDomainQuery(dq);
				if(bls != null && !bls.isEmpty()) {
					sbLog.appendError("deleteBL - BL FINDED BY DOMAINQUERY");
					BillOfLading.deleteBl(bls.get(0).getGkey());
				} else {
					sbLog.appendError("deleteBL - BL NOT FINDED BY DOMAINQUERY");
					return "ERRO";
				}
			}
	
			return "OK";
			
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	
	public String createUnitDUEEvents(String unitGkey, String notes, String geraLiberacaoEmbarque) {
		sbLog.appendInfo("createUnitDUEEvents");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null) {
				EventType eventType = EventType.findEventType("UNIT_VINC_DUE");
				if (eventType == null) {
					return "Evento " + eventType + " nao cadastrado no Navis";
				}
				if(naoExisteEvento(unitGkey, "UNIT_VINC_DUE")) {
					serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
				}
				if(geraLiberacaoEmbarque.equals("1")) {
					eventType = EventType.findEventType("LIBERACAO_PARA_EMBARQUE");
					if (eventType == null) {
						return "Evento " + eventType + " nao cadastrado no Navis";
					}
					if(naoExisteEvento(unitGkey, "LIBERACAO_PARA_EMBARQUE")) {
						serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
					}
				}
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	public boolean naoExisteEvento(String gkey, String evento) {
		sbLog.appendError("#### naoExisteEvento");
		sbLog.appendError("#### EVENTO : " + evento + "  | GKEY : " + gkey);
		StringBuilder hql = new StringBuilder(" select e.evntGkey from Event e where e.evntEventType.evnttypeId = '" + evento + "' and e.evntAppliedToPrimaryKey = " + gkey);
		sbLog.appendError("#### HQL : " + hql.toString());
		List<String> lstAux = com.navis.framework.business.Roastery.getHibernateApi().find(hql.toString());
		sbLog.appendError("#### IF RETORNO (lstAux == null || lstAux.isEmpty() || lstAux.size() == 0) -> RESULTADO : " + (lstAux == null || lstAux.isEmpty() || lstAux.size() == 0));
		return (lstAux == null || lstAux.isEmpty() || lstAux.size() == 0);
	}
	
	
	public String applyHoldUnit(String unitGkey, String hold, String notes) {
		sbLog.appendInfo("applyHoldUnit");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			serviceManager.applyHold(hold, unit, null, null, notes);
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}
	
	
	
	public String applyPermissionUnit(String unitGkey, String permission, String notes) {
		sbLog.appendInfo("applyHoldUnit");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			serviceManager.applyPermission(permission, unit, null, null, notes);
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	
	}
	
	
	public String createDesbloqueioEvents(String unitGkey, String notes, String eventsToCreate) {
		sbLog.appendError("############ createDesbloqueioEvents");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			if (unit != null && unit.getUnitGkey() != null && eventsToCreate != null && !"".equals(eventsToCreate)) {
				List<String> listaEventos = Arrays.asList(eventsToCreate.split(","));
				for (String evento : listaEventos) {
					EventType eventType = EventType.findEventType(evento.trim());
					if (eventType == null) {
						return "EVENTO " + evento + " NAO CADASTRADO";
					} 
					
					if ("LIBERACAO_DE_EXPORTACAO".equals(evento)) {
						if(naoExisteEvento(unitGkey, "LIBERACAO_DE_EXPORTACAO")) {
							serviceManager.recordEvent(eventType, "Liberado para Despacho", null, null, null, unit, null);
						}
					} 
					
					if ("INCLUSAO_DOC_DESPACHO".equals(evento)) {
						//if(naoExisteEvento(unitGkey, "INCLUSAO_DOC_DESPACHO")) {
							serviceManager.recordEvent(eventType, notes, null, null, null, unit, null);
						//}
					} 
				}
			} 
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String atualizaBookingShipper(String bookingGkey, String shipperGkey) {
		sbLog.appendInfo("createCFPEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Booking booking = Booking.hydrate(bookingGkey);
			if (booking != null && booking.getEqboGkey() != null) {
				ScopedBizUnit shipper = ScopedBizUnit.hydrate(shipperGkey);
				if (shipper != null && shipper.getBzuGkey() != null) {
					booking.setEqoShipper(shipper);
					com.navis.framework.business.Roastery.getHibernateApi().save(booking);
					com.navis.framework.business.Roastery.getHibernateApi().flush();
				} else {
					return "ERRO SHIPPER";
				}
			} else {
				return "ERRO BOOKING";
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}

	public String createCFPEvent(String unitGkey, String notes, String responsiblePartyGkey) {
		sbLog.appendInfo("createCFPEvent");
		try {
			com.navis.framework.business.Roastery.getHibernateApi().createSessionIfNecessary();
			Unit unit = Unit.hydrate(unitGkey);
			ScopedBizUnit scopedBizUnit = ScopedBizUnit.hydrate(responsiblePartyGkey);
			if (unit != null && unit.getUnitGkey() != null) {
				EventType eventType = EventType.findEventType("CLIENTE_FINANCEIRO_PAGADOR");
				if (eventType == null) {
					return "Evento " + event + " nao cadastrado no Navis";
				}
				//if(naoExisteEvento(unitGkey, "CLIENTE_FINANCEIRO_PAGADOR")) {
					serviceManager.recordEvent(eventType, notes, null, null, scopedBizUnit, unit, null);
				//}
			}
			return "OK";
		} catch (Exception e) {
			return "ERRO : " + e.getMessage();
		}
	}


}
