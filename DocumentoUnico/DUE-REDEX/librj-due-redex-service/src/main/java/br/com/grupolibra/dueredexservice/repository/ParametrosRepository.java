package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Parametros;

@Repository
public interface ParametrosRepository extends CrudRepository<Parametros, Long>, QuerydslPredicateExecutor<Parametros> {
	
	public Parametros findByChave(String chave);
	
	public List<Parametros> findAll(Predicate predicate);
	
}