package br.com.grupolibra.dueredexservice.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;


@Service
public class EnvioSiscomexService {
	
//	private static final String DUE_API_HTTPS = "https://10.2.10.8:8005";
//	private static final String DUE_API_HTTP = "http://10.2.10.9:8055";
	//private static final String DUE_API_HTTP = "http://10.2.10.9:8055";
	//private static final String DUE_API_HTTP = "http://172.26.221.25:8054/enviosiscomex";
	
	@Autowired
	private RestTemplate rest;
	
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(List<String> dues) {
		
		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultadadosresumidos");
		for (String due : dues) {
			builder.queryParam("numeroDUE", due);
		}
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ConsultaDadosResumidosResponseListDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity,
				ConsultaDadosResumidosResponseListDTO.class);
		ConsultaDadosResumidosResponseListDTO response = responseEntity.getBody();
		
		return response;
		
		//TODO PRD
//		ConsultaDadosResumidosResponseListDTO retorno = new ConsultaDadosResumidosResponseListDTO();
//		List<ConsultaDadosResumidosResponseDTO> resultList = new ArrayList<>();
//		
//		for (String due : dues) {
//			consultaDadosResumidosPorDUE(resultList, due);
//		}
//		retorno.setListaRetorno(resultList);
//		return retorno;
	}
		
	private HttpHeaders getHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
}
