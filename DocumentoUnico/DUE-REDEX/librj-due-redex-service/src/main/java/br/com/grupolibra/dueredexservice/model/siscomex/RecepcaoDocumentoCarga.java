package br.com.grupolibra.dueredexservice.model.siscomex;

import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recepcaoDocumentoCarga", propOrder = { "identificacaoRecepcao", "cnpjResp", "local", "referenciaLocalRecepcao",
		"entregador", "documentos", "pesoAferido", "localArmazenamento", "codigoIdentCarga",
		"avariasIdentificadas", "divergenciasIdentificadas", "observacoesGerais", "transitoSimplificado"})
public class RecepcaoDocumentoCarga {

	@XmlElement(required = true)
	protected String identificacaoRecepcao;

	@XmlElement(required = true)
	protected String cnpjResp;

	@XmlElement(required = true)
	protected Local local;

	protected String referenciaLocalRecepcao;

	@XStreamAlias("entregador")
	protected Entregador entregador;

	@XmlElement(required = true)
	@XStreamAlias("documentos")
	protected List<documento> documentos;

	@XmlElement(required = true)
	protected BigDecimal pesoAferido;

	@XmlElement(required = true)
	protected String motivoNaoPesagem;

	protected String localArmazenamento;

	protected String codigoIdentCarga;

	protected String avariasIdentificadas;

	protected String divergenciasIdentificadas;

	protected String observacoesGerais;
	
	@XStreamAlias("transitoSimplificado")
	protected TransitoSimplificadoRecepcao transitoSimplificadoRecepcao;

	public String getIdentificacaoRecepcao() {
		return identificacaoRecepcao;
	}

	public void setIdentificacaoRecepcao(String identificacaoRecepcao) {
		this.identificacaoRecepcao = identificacaoRecepcao;
	}

	public String getCnpjResp() {
		return cnpjResp;
	}

	public void setCnpjResp(String cnpjResp) {
		this.cnpjResp = cnpjResp;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public String getReferenciaLocalRecepcao() {
		return referenciaLocalRecepcao;
	}

	public void setReferenciaLocalRecepcao(String referenciaLocalRecepcao) {
		this.referenciaLocalRecepcao = referenciaLocalRecepcao;
	}

	public Entregador getEntregador() {
		return entregador;
	}

	public void setEntregador(Entregador entregador) {
		this.entregador = entregador;
	}

	public List<documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<documento> documentos) {
		this.documentos = documentos;
	}

	public BigDecimal getPesoAferido() {
		return pesoAferido;
	}

	public void setPesoAferido(BigDecimal pesoAferido) {
		this.pesoAferido = pesoAferido;
	}

	public String getMotivoNaoPesagem() {
		return motivoNaoPesagem;
	}

	public void setMotivoNaoPesagem(String motivoNaoPesagem) {
		this.motivoNaoPesagem = motivoNaoPesagem;
	}

	public String getLocalArmazenamento() {
		return localArmazenamento;
	}

	public void setLocalArmazenamento(String localArmazenamento) {
		this.localArmazenamento = localArmazenamento;
	}

	public String getCodigoIdentCarga() {
		return codigoIdentCarga;
	}

	public void setCodigoIdentCarga(String codigoIdentCarga) {
		this.codigoIdentCarga = codigoIdentCarga;
	}

	public String getAvariasIdentificadas() {
		return avariasIdentificadas;
	}

	public void setAvariasIdentificadas(String avariasIdentificadas) {
		this.avariasIdentificadas = avariasIdentificadas;
	}

	public String getDivergenciasIdentificadas() {
		return divergenciasIdentificadas;
	}

	public void setDivergenciasIdentificadas(String divergenciasIdentificadas) {
		this.divergenciasIdentificadas = divergenciasIdentificadas;
	}

	public String getObservacoesGerais() {
		return observacoesGerais;
	}

	public void setObservacoesGerais(String observacoesGerais) {
		this.observacoesGerais = observacoesGerais;
	}

	public TransitoSimplificadoRecepcao getTransitoSimplificadoRecepcao() {
		return transitoSimplificadoRecepcao;
	}

	public void setTransitoSimplificadoRecepcao(TransitoSimplificadoRecepcao transitoSimplificadoRecepcao) {
		this.transitoSimplificadoRecepcao = transitoSimplificadoRecepcao;
	}

}
