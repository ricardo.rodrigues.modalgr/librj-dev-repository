package br.com.grupolibra.dueredexservice.model.siscomex;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documento", propOrder = { "numeroDUE", "numeroRUC", "cargaSoltaVeiculo", "granel"})
@XStreamAlias("documento")
public class documento {

	@XmlElement(required = true)
	protected String numeroDUE;
	
	protected String numeroRUC;

	@XmlElement(required = true)
	protected List<Carga> cargaSoltaVeiculo;

	@XmlElement(required = true)
	protected List<Carga> granel;

	public String getNumeroDUE() {
		return numeroDUE;
	}

	public void setNumeroDUE(String numeroDUE) {
		this.numeroDUE = numeroDUE;
	}

	public String getNumeroRUC() {
		return numeroRUC;
	}

	public void setNumeroRUC(String numeroRUC) {
		this.numeroRUC = numeroRUC;
	}

	public List<Carga> getCargaSoltaVeiculo() {
		return cargaSoltaVeiculo;
	}

	public void setCargaSoltaVeiculo(List<Carga> cargaSoltaVeiculo) {
		this.cargaSoltaVeiculo = cargaSoltaVeiculo;
	}

	public List<Carga> getGranel() {
		return granel;
	}

	public void setGranel(List<Carga> granel) {
		this.granel = granel;
	}
}
