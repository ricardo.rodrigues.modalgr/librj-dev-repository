package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QItem is a Querydsl query type for Item
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QItem extends EntityPathBase<Item> {

    private static final long serialVersionUID = 42182553L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QItem item = new QItem("item");

    public final QDocumento documento;

    public final StringPath embalagem = createString("embalagem");

    public final StringPath embalagemId = createString("embalagemId");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath infoAdicional = createString("infoAdicional");

    public final StringPath marcasNumeros = createString("marcasNumeros");

    public final StringPath ncm = createString("ncm");

    public final StringPath ncmId = createString("ncmId");

    public final StringPath numero = createString("numero");

    public final StringPath peso = createString("peso");

    public final StringPath quantidade = createString("quantidade");

    public QItem(String variable) {
        this(Item.class, forVariable(variable), INITS);
    }

    public QItem(Path<? extends Item> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QItem(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QItem(PathMetadata metadata, PathInits inits) {
        this(Item.class, metadata, inits);
    }

    public QItem(Class<? extends Item> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.documento = inits.isInitialized("documento") ? new QDocumento(forProperty("documento"), inits.get("documento")) : null;
    }

}

