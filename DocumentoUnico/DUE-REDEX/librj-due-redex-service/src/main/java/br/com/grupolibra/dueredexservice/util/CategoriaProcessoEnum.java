package br.com.grupolibra.dueredexservice.util;

/**
 * 
 * @author dev
 *
 * Categoria de processamento Recepcao/Entrega
 *
 */
public enum CategoriaProcessoEnum {

	NFE, TRANSBORDO_MARITIMO, DUE;

}
