package br.com.grupolibra.dueredexservice.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.Lists;

import br.com.grupolibra.dueredexservice.dto.CargaSoltaVeiculoDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDueDanfeDTO;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Item;
import br.com.grupolibra.dueredexservice.repository.BDRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.TransporteRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.EnvioSiscomexService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.ParadaProgramadaUtil;

@Component
public class ConsultaDUEPorNFETask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private EnvioSiscomexService envioSiscomexService;
	
	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private BDRepository bdRepository;
	
	@Autowired
	private DocumentoRepository documentoRepository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void consultaDUEPorNFe() {
		logger.info("###### INICIO TASK CONSULTA DUE POR NFE");
		
//		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
//			logger.info("###### PARADA PROGRAMADA");
//			return;
//		}
		
		List<DocumentoDueDanfeDTO> docs = bdRepository.getNFe();		
		if (docs == null && docs.isEmpty()) {
			logger.info("consultaDUEPorNFe - Sem registros com cadastro de NFe");
			return;
		}
		
		logger.info("consultaDUEPorNFe - qtd registros tipo ITEM : {} ", docs.size());							
		int count = 1;
		for (DocumentoDueDanfeDTO _doc : docs) {		
			siscomexService.consultaDuePorNf(_doc);
			count++;
			logger.info("consultaDUEPorNFe - registro {} ", count);		
		}
		
		
	}
}
