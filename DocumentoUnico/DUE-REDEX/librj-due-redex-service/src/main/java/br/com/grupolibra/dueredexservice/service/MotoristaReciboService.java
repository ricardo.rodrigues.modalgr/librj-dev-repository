package br.com.grupolibra.dueredexservice.service;

import java.util.List;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.MotoristaReciboDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Transporte;

public interface MotoristaReciboService {

	MotoristaReciboDTO buscaTransportadora(String cnpj);

	MotoristaReciboDTO buscaMotorista(String searchDoc, boolean isEstrangeiro);

	MotoristaReciboDTO buscaDocumentosDisponiveis(String cnpj, String novo);

	MotoristaReciboDTO salvaRecibo(Transporte dto);

	CadastroDocumentoDTO validaModificacaoDocumentos(List<Documento> documentos);

	MotoristaReciboDTO excluirRecibo(Transporte dto);

	MotoristaReciboDTO validaImpressaoRecibo(Transporte dto);

	byte[] geraRecibo(Long idTransporte);

	ConteinerDTO getTransportadoraInfo(String cnpj);

}


