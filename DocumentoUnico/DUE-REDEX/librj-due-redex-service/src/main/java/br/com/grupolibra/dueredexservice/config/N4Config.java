package br.com.grupolibra.dueredexservice.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.grupolibra.dueredexservice.util.PropertiesUtil;
import br.com.grupolibra.fmwPortService.genericInvoke.ConnectionConfig;
import br.com.grupolibra.fmwPortService.genericInvoke.N4ServiceInvoke;
import br.com.grupolibra.fmwPortService.helper.scope.Scope;

@Configuration
public class N4Config {

	private static final String NAVIS_URL = "navis.url";
	private static final String NAVIS_USER_AUTH = "navis.user.auth";
	private static final String NAVIS_AUTH = "navis.password.auth";

	@Bean(name = "n4ServiceInvoke")
	public N4ServiceInvoke n4ServiceInvoke() {
		return new N4ServiceInvoke(connectionConfig(), scope());
	}

	@Bean(name = "scope")
	public Scope scope() {
		Scope scope = new Scope();
		scope.setComplex(PropertiesUtil.getProp().getProperty("n4.complex"));
		scope.setFacility(PropertiesUtil.getProp().getProperty("n4.facility"));
		scope.setOperator(PropertiesUtil.getProp().getProperty("n4.operator"));
		scope.setYard(PropertiesUtil.getProp().getProperty("n4.yard"));
		return scope;
	}

	@Bean("connectionConfig")
	public ConnectionConfig connectionConfig() {
		return new ConnectionConfig(getN4Properties());
	}

	@Bean
	public Properties getN4Properties() {
		Properties properties = new Properties();
		properties.setProperty(NAVIS_URL, PropertiesUtil.getProp().getProperty("n4.url").trim());
		properties.setProperty(NAVIS_USER_AUTH, PropertiesUtil.getProp().getProperty("n4.username").trim());
		properties.setProperty(NAVIS_AUTH, PropertiesUtil.getProp().getProperty("n4.password").trim());
		return properties;
	}

}
