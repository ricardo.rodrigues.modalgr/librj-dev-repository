package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDocumento is a Querydsl query type for Documento
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDocumento extends EntityPathBase<Documento> {

    private static final long serialVersionUID = -753923602L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDocumento documento = new QDocumento("documento");

    public final StringPath blGkey = createString("blGkey");

    public final StringPath blNbr = createString("blNbr");

    public final StringPath booking = createString("booking");

    public final StringPath cnpjTransportadora = createString("cnpjTransportadora");

    public final QShipper consolidador;

    public final StringPath danfe = createString("danfe");

    public final QShipper emitente;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ListPath<Item, QItem> itens = this.<Item, QItem>createList("itens", Item.class, QItem.class, PathInits.DIRECT2);

    public final StringPath numero = createString("numero");

    public final StringPath ruc = createString("ruc");

    public final StringPath tipo = createString("tipo");

    public final QTransporte transporte;
    
    public final DateTimePath<java.util.Date> dataCadastro = createDateTime("dataCadastro", java.util.Date.class);

    public final StringPath numeroConteiner = createString("numeroConteiner");
    
    public QDocumento(String variable) {
        this(Documento.class, forVariable(variable), INITS);
    }

    public QDocumento(Path<? extends Documento> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QDocumento(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QDocumento(PathMetadata metadata, PathInits inits) {
        this(Documento.class, metadata, inits);
    }

    public QDocumento(Class<? extends Documento> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.consolidador = inits.isInitialized("consolidador") ? new QShipper(forProperty("consolidador")) : null;
        this.emitente = inits.isInitialized("emitente") ? new QShipper(forProperty("emitente")) : null;
        this.transporte = inits.isInitialized("transporte") ? new QTransporte(forProperty("transporte")) : null;
    }

}

