package br.com.grupolibra.dueredexservice.service;

import java.util.List;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoGridDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoRequestDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.response.GridNcm;
import br.com.grupolibra.dueredexservice.response.GridEmbalagem;

public interface CadastroDocumentoService {
	
	boolean existeCadastroShipper(String cnpj);

	CadastroDocumentoDTO salvaDocumento(Documento dto);
	
	DocumentoDTO buscaDocumentosTransportadora(DocumentoRequestDTO dto);

	GridEmbalagem buscaEmbalagens();

	GridNcm buscaCommodities();

	String excluirDocumento(Documento documento);

	CadastroDocumentoDTO validaModificacaoDocumento(Documento dto);


}

