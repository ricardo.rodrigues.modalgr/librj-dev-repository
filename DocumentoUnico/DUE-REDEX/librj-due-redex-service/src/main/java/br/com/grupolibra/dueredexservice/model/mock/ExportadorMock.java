package br.com.grupolibra.dueredexservice.model.mock;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "EXPORTADOR_MOCK")
@NamedQuery(name = "ExportadorMock.findAll", query = "SELECT e FROM ExportadorMock e")
public class ExportadorMock {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXPORTADOR_MOCK_SEQ")
	@SequenceGenerator(name = "EXPORTADOR_MOCK_SEQ", sequenceName = "EXPORTADOR_MOCK_SEQ", allocationSize = 1)
	private long id;

	private String numero;
	
	private String tipo;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "DADOS_RESUMIDOS_MOCK_ID")
	private DadosResumidosMock dadosResumidosMock;

	public long getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public DadosResumidosMock getDadosResumidosMock() {
		return dadosResumidosMock;
	}

	public void setDadosResumidosMock(DadosResumidosMock dadosResumidosMock) {
		this.dadosResumidosMock = dadosResumidosMock;
	}
	
}
