package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDat is a Querydsl query type for Dat
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDat extends EntityPathBase<Dat> {

    private static final long serialVersionUID = -325204132L;

    public static final QDat dat = new QDat("dat");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath numero = createString("numero");
    public final NumberPath<Integer> situacao = createNumber("situacao", Integer.class);
    public final NumberPath<Integer> total = createNumber("total", Integer.class);
    public final StringPath veiculo = createString("veiculo");
    
    public QDat(String variable) {
        super(Dat.class, forVariable(variable));
    }

    public QDat(Path<? extends Dat> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDat(PathMetadata metadata) {
        super(Dat.class, metadata);
    }

}

