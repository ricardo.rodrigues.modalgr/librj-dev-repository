package br.com.grupolibra.dueredexservice.dto;

import java.io.Serializable;
import java.util.List;


public class ConsultaDadosResumidosResponseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String numeroDUE;
	
	private String numeroRUC;
	
	private String situacaoDUE;
	
	private String dataSituacaoDUE;
	
	private String indicadorBloqueio;
	
	private String controleAdministrativo;
	
	private String uaEmbarque;
	
	private String uaDespacho;
	
	private String responsaveluaDespacho;
	
	private String codigoRecintoAduaneiroDespacho;
	
	private String codigoRecintoAduaneiroEmbarque;
	
	private String coordenadasDespacho;
	
	private String recintoDespacho;
	
	private Declarante declarante;
	
	private List<Exportador> exportadores;
	
	private List<Integer> situacaoCarga;

	public String getNumeroDUE() {
		return numeroDUE;
	}

	public void setNumeroDUE(String numeroDUE) {
		this.numeroDUE = numeroDUE;
	}

	public String getNumeroRUC() {
		return numeroRUC;
	}

	public void setNumeroRUC(String numeroRUC) {
		this.numeroRUC = numeroRUC;
	}

	public String getSituacaoDUE() {
		return situacaoDUE;
	}

	public void setSituacaoDUE(String situacaoDUE) {
		this.situacaoDUE = situacaoDUE;
	}

	public String getDataSituacaoDUE() {
		return dataSituacaoDUE;
	}

	public void setDataSituacaoDUE(String dataSituacaoDUE) {
		this.dataSituacaoDUE = dataSituacaoDUE;
	}

	public String getIndicadorBloqueio() {
		return indicadorBloqueio;
	}

	public void setIndicadorBloqueio(String indicadorBloqueio) {
		this.indicadorBloqueio = indicadorBloqueio;
	}

	public String getControleAdministrativo() {
		return controleAdministrativo;
	}

	public void setControleAdministrativo(String controleAdministrativo) {
		this.controleAdministrativo = controleAdministrativo;
	}

	public String getUaEmbarque() {
		return uaEmbarque;
	}

	public void setUaEmbarque(String uaEmbarque) {
		this.uaEmbarque = uaEmbarque;
	}

	public String getUaDespacho() {
		return uaDespacho;
	}

	public void setUaDespacho(String uaDespacho) {
		this.uaDespacho = uaDespacho;
	}

	public String getResponsaveluaDespacho() {
		return responsaveluaDespacho;
	}

	public void setResponsaveluaDespacho(String responsaveluaDespacho) {
		this.responsaveluaDespacho = responsaveluaDespacho;
	}

	public String getCodigoRecintoAduaneiroDespacho() {
		return codigoRecintoAduaneiroDespacho;
	}

	public void setCodigoRecintoAduaneiroDespacho(String codigoRecintoAduaneiroDespacho) {
		this.codigoRecintoAduaneiroDespacho = codigoRecintoAduaneiroDespacho;
	}

	public String getCodigoRecintoAduaneiroEmbarque() {
		return codigoRecintoAduaneiroEmbarque;
	}

	public void setCodigoRecintoAduaneiroEmbarque(String codigoRecintoAduaneiroEmbarque) {
		this.codigoRecintoAduaneiroEmbarque = codigoRecintoAduaneiroEmbarque;
	}

	public String getCoordenadasDespacho() {
		return coordenadasDespacho;
	}

	public void setCoordenadasDespacho(String coordenadasDespacho) {
		this.coordenadasDespacho = coordenadasDespacho;
	}

	public String getRecintoDespacho() {
		return recintoDespacho;
	}

	public void setRecintoDespacho(String recintoDespacho) {
		this.recintoDespacho = recintoDespacho;
	}

	public Declarante getDeclarante() {
		return declarante;
	}

	public void setDeclarante(Declarante declarante) {
		this.declarante = declarante;
	}

	public List<Exportador> getExportadores() {
		return exportadores;
	}

	public void setExportadores(List<Exportador> exportadores) {
		this.exportadores = exportadores;
	}

	public List<Integer> getSituacaoCarga() {
		return situacaoCarga;
	}

	public void setSituacaoCarga(List<Integer> situacaoCarga) {
		this.situacaoCarga = situacaoCarga;
	}
	
}
