package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Duedat;

@Repository
public interface DuedatRepository extends CrudRepository<Duedat, Long>, QuerydslPredicateExecutor<Duedat> {	
	
	public List<Duedat> findAll(Predicate predicate);
}
