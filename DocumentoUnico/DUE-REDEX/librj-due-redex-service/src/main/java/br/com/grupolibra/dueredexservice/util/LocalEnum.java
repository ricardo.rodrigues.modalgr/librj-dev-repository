package br.com.grupolibra.dueredexservice.util;

public enum LocalEnum {

	LIBRA_TERMINAL_RJ("7922709"), PORTO_DE_RJ("0717600"), LATITUDE("-22.873438"), LONGITUDE("-43.205887");

	private String value;

	LocalEnum(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
