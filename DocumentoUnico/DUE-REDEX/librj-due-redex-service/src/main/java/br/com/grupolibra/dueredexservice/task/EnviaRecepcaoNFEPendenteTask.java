package br.com.grupolibra.dueredexservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.RecepcaoService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.ParadaProgramadaUtil;

@Component
public class EnviaRecepcaoNFEPendenteTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void enviaRecepcaoNFEDUEPendente() {
		logger.info("###### INICIO TASK ENVIO RECEPCAO POR NFE / DUE PENDENTE");
		
	/*	if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMADA");
			return;
		}*/
		
		logger.info("enviaRecepcaoPendente - NFE");
		List<Cct> cctListRecepcaoPendenteAux = cctService.getEnvioRecepcaoNFEPendente();
		if (cctListRecepcaoPendenteAux != null && !cctListRecepcaoPendenteAux.isEmpty()) {
			logger.info("enviaRecepcaoPendente - NFE - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd: {} ", cctListRecepcaoPendenteAux.size());
			List<Cct> cctListRecepcaoPendente = n4Repository.validaUnitFacilityVisitStateActive(cctListRecepcaoPendenteAux);				
			logger.info("enviaRecepcaoPendente NFE - qtd registros recepcao pendente: {} ", cctListRecepcaoPendente.size());
			List<Cct> cctListRecepcaoPendenteCorrigidos = n4Repository.validaCorrecaoPendenciasRecepcaoNFE(cctListRecepcaoPendente);
			if (!cctListRecepcaoPendenteCorrigidos.isEmpty()) {
				logger.info("enviaRecepcaoPendente NFE - qtd registros recepcao pendente corrigidos: {} ", cctListRecepcaoPendenteCorrigidos.size());
				for (Cct cct : cctListRecepcaoPendenteCorrigidos) {
					RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFE(cct);
					siscomexService.sendRecepcaoNFE(xml, cct);
				}
			} else {
				logger.info("enviaRecepcaoPendente NFE - sem correcao recepcao pendente");
			}
		} else {
			logger.info("enviaRecepcaoPendente NFE - sem registro recepcao pendente");
		}
		
		logger.info("enviaRecepcaoPendente - DUE");
		cctListRecepcaoPendenteAux = cctService.getEnvioRecepcaoDUEPendente();
		if (cctListRecepcaoPendenteAux != null && !cctListRecepcaoPendenteAux.isEmpty()) {
			logger.info("enviaRecepcaoPendente - DUE - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd: {} ", cctListRecepcaoPendenteAux.size());
			List<Cct> cctListRecepcaoPendente = n4Repository.validaUnitFacilityVisitStateActive(cctListRecepcaoPendenteAux);
			logger.info("enviaRecepcaoPendente DUE - qtd registros recepcao pendente: {} ", cctListRecepcaoPendente.size());
			List<Cct> cctListRecepcaoPendenteCorrigidos = n4Repository.validaCorrecaoPendenciasRecepcaoDUE(cctListRecepcaoPendente);
			if (!cctListRecepcaoPendenteCorrigidos.isEmpty()) {
				logger.info("enviaRecepcaoPendente DUE - qtd registros recepcao pendente corrigidos: {} ", cctListRecepcaoPendenteCorrigidos.size());
				for (Cct cct : cctListRecepcaoPendenteCorrigidos) {
					RecepcoesDocumentoCarga xml = recepcaoService.getXMLRecepcaoDUE(cct);
					siscomexService.sendRecepcaoCargaSoltaDUE(xml, cct);
				}
			} else {
				logger.info("enviaRecepcaoPendente DUE - sem correcao recepcao pendente");
			}
		} else {
			logger.info("enviaRecepcaoPendente DUE - sem registro recepcao pendente");
		}
		
		
		
		logger.info("enviaRecepcaoPendente - NFE Carga Solta");
		List<Cct> cctListRecepcaoPendenteCargaSoltaAux = cctService.getEnvioRecepcaoNFEPendenteCargaSolta();
		if (cctListRecepcaoPendenteCargaSoltaAux != null && !cctListRecepcaoPendenteCargaSoltaAux.isEmpty()) {
			logger.info("enviaRecepcaoPendente - NFE Carga Solta - VALIDANDO UNIT_FACILITY_VISIT_STATE qtd: {} ", cctListRecepcaoPendenteCargaSoltaAux.size());
			List<Cct> cctListRecepcaoPendenteCargaSolta = n4Repository.validaUnitFacilityVisitStateActive(cctListRecepcaoPendenteCargaSoltaAux);				
			logger.info("enviaRecepcaoPendente - NFE Carga Solta - qtd registros recepcao pendente: {} ", cctListRecepcaoPendenteCargaSolta.size());
			List<Cct> cctListRecepcaoPendenteCorrigidosCargaSolta = n4Repository.validaCorrecaoPendenciasRecepcaoNFECargaSolta(cctListRecepcaoPendenteCargaSolta);
			if (!cctListRecepcaoPendenteCorrigidosCargaSolta.isEmpty()) {
				logger.info("enviaRecepcaoPendente - NFE Carga Solta - qtd registros recepcao pendente corrigidos: {} ", cctListRecepcaoPendenteCorrigidosCargaSolta.size());
				for (Cct cct : cctListRecepcaoPendenteCorrigidosCargaSolta) {
					RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFECargaSolta(cct);
					siscomexService.sendRecepcaoNFECargaSolta(xml, cct);
				}
			} else {
				logger.info("enviaRecepcaoPendente - NFE Carga Solta - sem correcao recepcao pendente");
			}
		} else {
			logger.info("enviaRecepcaoPendente - NFE Carga Solta - sem registro recepcao pendente");
		}
	}
}
