package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DAT")
@NamedQuery(name = "Dat.findAll", query = "SELECT a FROM Dat a")
public class Dat implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final Integer PENDENTE = 0;
	public static final Integer COLETADAS = 1;
	public static final Integer RECEPCIONADO = 2;
	public static final Integer ENTREGUE = 3;
	public static final Integer INVALIDO = 9;	
	
	@Id
	@Column(name = "ID_DAT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dat_seq")
	@SequenceGenerator(name = "dat_seq", sequenceName = "dat_seq", allocationSize = 1)
	private long id;

	@Column(name = "NUMERO")
	private String numero;

	@Column(name = "SITUACAO")
	private Integer situacao=PENDENTE;
	
	@Column(name = "TOTAL")
	private Integer total;	

	@Column(name = "VEICULO")
	private String veiculo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}	

}
