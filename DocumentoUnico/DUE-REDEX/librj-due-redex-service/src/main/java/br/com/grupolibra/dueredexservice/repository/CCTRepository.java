package br.com.grupolibra.dueredexservice.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Cct;

@Repository
public interface CCTRepository extends CrudRepository<Cct, Long>, QuerydslPredicateExecutor<Cct> {
	
	public Optional<Cct> findById(Long id);

	public List<Cct> findByIdExterno(BigDecimal idExterno);

	public List<Cct> findByStatusProc(String statusProc);

	public List<Cct> findByNrodue(String nroDue);

//	public List<Cct> findAll();
	public List<Cct> findAllByOrderByIdDesc();

	public List<Cct> findByStatusProcOrStatusProc(String statusProc1, String statusProc2);

	public List<Cct> findByStatusProcOrStatusProcOrStatusProc(String statusProc1, String statusProc2, String statusProc3);

	public List<Cct> findAll(Predicate predicate);

	public List<Cct> findByStatusProcOrStatusProcOrStatusProcOrStatusProc(String statusProc1, String statusProc2, String statusProc3, String statusProc4);
	
	public List<Cct> findByStatus(String status);
	
	public List<Cct> findAllByBooking(String booking);
	
	public List<Cct> findByNumeroCont(String numeroCont);
}