package br.com.grupolibra.dueredexservice.dto;

public class CadastroConteinerDTO {
	
	public CadastroConteinerDTO() {
		super();
	}
	
	private BookingDTO bookingDTO;
	
	private ConteinerDTO conteinerCadastro;
	
	private boolean erro;
	
	private String mensagem;
	
	private String informaISO;
	
	public BookingDTO getBookingDTO() {
		return bookingDTO;
	}

	public ConteinerDTO getConteinerCadastro() {
		return conteinerCadastro;
	}

	public void setBookingDTO(BookingDTO bookingDTO) {
		this.bookingDTO = bookingDTO;
	}

	public void setConteinerCadastro(ConteinerDTO conteinerCadastro) {
		this.conteinerCadastro = conteinerCadastro;
	}

	public boolean isErro() {
		return erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getInformaISO() {
		return informaISO;
	}

	public void setInformaISO(String informaISO) {
		this.informaISO = informaISO;
	}

	@Override
	public String toString() {
		return "CadastroConteinerDTO [bookingDTO=" + bookingDTO + ", conteinerCadastro=" + conteinerCadastro + ", erro=" + erro + ", mensagem=" + mensagem + ", informaISO=" + informaISO + "]";
	}	
}
