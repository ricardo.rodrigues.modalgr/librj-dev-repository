package br.com.grupolibra.dueredexservice.service.impl;

import java.math.BigDecimal;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.thoughtworks.xstream.XStream;

import br.com.grupolibra.dueredexservice.dto.CargaSoltaVeiculoDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaConteineresResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaConteineresResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDueDanfeDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoTransporteDTO;
import br.com.grupolibra.dueredexservice.dto.EntregaConteinerResponseDTO;
import br.com.grupolibra.dueredexservice.dto.RecepcaoResponseDTO;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Dat;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Duedanfe;
import br.com.grupolibra.dueredexservice.model.Duedat;
import br.com.grupolibra.dueredexservice.model.Item;
import br.com.grupolibra.dueredexservice.model.Transporte;
import br.com.grupolibra.dueredexservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.DatRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.DuedanfeRepository;
import br.com.grupolibra.dueredexservice.repository.DuedatRepository;
import br.com.grupolibra.dueredexservice.repository.ItemRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Service
public class SiscomexServiceImpl implements SiscomexService {

	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestTemplate rest;

	@Autowired
	private CCTRepository cctRepository;

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private DatRepository datRepository;
	
	@Autowired
	private DuedatRepository duedatRepository;
	
	@Autowired
	private DuedanfeRepository duedanfeRepository;
		
	@Autowired
	private CctService cctService;

	@Override
	public void sendRecepcaoNFE(RecepcoesNFE recepcoesNFE, Cct cct) {
		logger.info("sendRecepcaoNFE - Enviando recepcão NFE ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(recepcoesNFE);
		logger.info("Body: {} ", body);
		
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("RECEPCAO NFE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);
		try {
			String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");
//			logger.info("URL: {} ", url);
			ResponseEntity<RecepcaoResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, RecepcaoResponseDTO.class);
			processResponseRecepcaoNFE(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
			logger.error("Fim da recepcão ao siscomex. HttpStatusCode Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. ResourceAccess Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. Generic Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		}
	}
	
	
	
	@Override
	public void sendRecepcaoNFECargaSolta(RecepcoesNFE recepcoesNFE, Cct cct) {
		logger.info("sendRecepcaoNFE - Enviando recepcão NFE BBK ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(recepcoesNFE);
		logger.info("sendRecepcaoNFE - Body: {} ", body);
		
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("RECEPCAO NFE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);
		try {
			String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");
			logger.info("sendRecepcaoNFE - URL: {} ", url);
			ResponseEntity<RecepcaoResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, RecepcaoResponseDTO.class);
			processResponseRecepcaoNFECargaSolta(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
			logger.error("sendRecepcaoNFE - Fim da recepcão ao siscomex. HttpStatusCode Exception");
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("sendRecepcaoNFE - Fim da recepcão ao siscomex. ResourceAccess Exception");
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("sendRecepcaoNFE - Fim da recepcão ao siscomex. Generic Exception");
			cctService.save(cct);
		}
	}
	
	
	private void processResponseRecepcaoNFE(Cct cct, ResponseEntity<RecepcaoResponseDTO> responseEntity) {
		logger.info("processResponseEntregaNFE");
		RecepcaoResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ", response.getErro());
			
			if(response.getErro() != null && !response.getErro().isEmpty() && response.getErro().contains("parada programada")) {
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setErro(Cct.STATUS_ERRO_PARADA_PROGRAMADA);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			} else {
				if (response.getMensagem().isEmpty()) {
					cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
					countEnvio(cct);
				} else {
					processResponseErrorSiscomexMS(cct, response.getMensagem());
				}
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_RECEPCIONADO_OK);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				List<String> danfeList = new ArrayList<>();
				cct.getDanfes().forEach(danfe -> danfeList.add(danfe.getChave()));
				String notes = "DANFE : " + String.join(", DANFE : ", danfeList);
				String retorno = n4Repository.createRecepcaoNFEEvent(cct.getIdExterno(), notes);
				logger.info("Retorno createRecepcaoEvent : {} ", retorno);
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO RECEPCAO: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro("ERRO CRIANDO EVENTO RECEPCAO - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}
	
	
	private void processResponseRecepcaoNFECargaSolta(Cct cct, ResponseEntity<RecepcaoResponseDTO> responseEntity) {
		logger.info("processResponseRecepcaoNFECargaSolta");
		RecepcaoResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ", response.getErro());
			
			if(response.getErro() != null && !response.getErro().isEmpty() && response.getErro().contains("parada programada")) {
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setErro(Cct.STATUS_ERRO_PARADA_PROGRAMADA);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			} else {
				if (response.getMensagem().isEmpty()) {
					cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
					countEnvio(cct);
				} else {
					processResponseErrorSiscomexMS(cct, response.getMensagem());
				}
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_RECEPCIONADO_OK);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				Documento documento = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());				
				String notes = "DANFE: " + documento.getDanfe();
				String retorno = n4Repository.createRecepcaoNFEEvent(cct.getIdExterno(), notes);
				logger.info("Retorno createRecepcaoEvent : {} ", retorno);
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO RECEPCAO: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro("ERRO CRIANDO EVENTO RECEPCAO - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}


	private String limitaQuantidadeCaracteresBD(String msg) {
		if(msg != null) {
			if(msg.length() >= 4000) {
				return msg.substring(0,  3000) + " ....";
			} else {
				return msg;
			}
		} else {
			return null;
		}
	}

	@Override
	public List<Cct> consultaDUEPorConteiner(List<Cct> cctListConsultaCadastroDUE) {
		logger.info("consultaDUEPorConteiner");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultaconteiner");
		for (Cct cct : cctListConsultaCadastroDUE) {
			builder.queryParam("nrConteiner", cct.getNumeroCont());
		}
		URI url = builder.build().encode().toUri();
		try {
			List<Cct> listaRetorno = new ArrayList<>();
			ResponseEntity<ConsultaConteineresResponseListDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaConteineresResponseListDTO.class);
			ConsultaConteineresResponseListDTO responseDTO = responseEntity.getBody();
			if (!Boolean.valueOf(responseDTO.isFlag()) && responseDTO.getListaRetorno() != null && !responseDTO.getListaRetorno().isEmpty()) {
				listaRetorno = processResponseConsultaDUEporConteiner(cctListConsultaCadastroDUE, responseDTO);
			}
			logger.info("consultaDUEPorConteiner - qtd registro com cadastro de DUE : {} ", listaRetorno.size());
			return listaRetorno;
		} catch(Exception e) {
			logger.error("SISCOMEX ERROR - consultaDUEPorConteiner : {} ", e.getMessage());
			return Collections.emptyList();
		}
	}	

	private List<Cct> processResponseConsultaDUEporConteiner(List<Cct> cctListConsultaCadastroDUE, ConsultaConteineresResponseListDTO responseDTO) {
		logger.info("processResponseConsultaDUEporConteiner");
		for (Cct cct : cctListConsultaCadastroDUE) {			
			for (ConsultaConteineresResponseDTO response : responseDTO.getListaRetorno()) {
				if (response.getNrConteiner().equals(cct.getNumeroCont())) {
					if (response.getNrDUE() != null && !"".equals(response.getNrDUE())) {
						logger.info("processResponseConsultaDUEporConteiner - RESPONSE DUE {} ", response.getNrDUE());
						if(cct.getNrodue() != null && !"".equals(cct.getNrodue())) {
							cct.setNrodue(cct.getNrodue() + ", " + response.getNrDUE());
						} else {
							cct.setNrodue(response.getNrDUE());
						}
						cctService.save(cct);
					}
				}
			}
		}
		List<Cct> listaRetorno = new ArrayList<>();
		for (Cct cct : cctListConsultaCadastroDUE) {
			if(cct.getNrodue() != null && !"".equals(cct.getNrodue())) {
				listaRetorno.add(cct);
			}
		}
		logger.info("processResponseConsultaDUEporConteiner - QTD Atualizar DUE : {} ", listaRetorno.size());
		return listaRetorno;
	}
		
	@Override
	public void sendEntregaConteiner(EntregasConteineres entregasConteineres, Cct cct) {
		logger.info("sendEntregaConteiner - Enviando entrega conteiner ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(entregasConteineres);

//		logger.info("Body: {} ", body);
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("ENTREGA NFE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);

		String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		try {
			ResponseEntity<EntregaConteinerResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, EntregaConteinerResponseDTO.class);
			processResponseEntregaConteier(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. ResourceAccess Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("Fim da recepcão ao siscomex. Generic Exception");
//			cctRepository.save(cct);
			cctService.save(cct);
		}
	}

	private void processResponseEntregaConteier(Cct cct, ResponseEntity<EntregaConteinerResponseDTO> responseEntity) {
//		logger.info("processResponseEntregaConteier");
		EntregaConteinerResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ",response.getErro());

			if (response.getMensagem().isEmpty()) {
				cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
				countEnvio(cct);
			} else {
				processResponseErrorSiscomexMS(cct, response.getMensagem());
			}
			cct.setStatus(Cct.STATUS_ERRO);
			cct.setStatusProc(Cct.STATUS_ERRO_ENTREGA);
			cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//			cctRepository.save(cct);
			cctService.save(cct);
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_ENTREGA_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_ENTREGUE);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				String notes = "DUE: " + cct.getNrodue();
				String retorno = n4Repository.createEntregaEvent(cct, notes);
				logger.info("Retorno createEntregaEvent : {} ", retorno);				
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO ENTREGA: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_ENTREGA);
				cct.setErro("ERRO CRIANDO EVENTO ENTREGA - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}

	private void processResponseErrorSiscomexMS(Cct cct, String responseMsg) {
		if(Cct.ERRO_INTERNO_SERVIDOR.equals(responseMsg) || Cct.ERRO_TIMEOUT.equals(responseMsg) || 
				Cct.SERVICO_INDISPONIVEL.equals(responseMsg) || Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.equals(responseMsg) || 
				Cct.ERRO_ENVIO_RECEITA.equals(responseMsg)) {
			cct.setMensagem(responseMsg);
		} else {
			cct.setMensagem(responseMsg);
			countEnvio(cct);
		} 
	}

	private void processHttpStatusCodeException(Cct cct, HttpStatusCodeException e) {
		logger.info("processHttpStatusCodeException - STATUS CODE : {} ", e.getStatusCode());
		HttpStatus statusCode = e.getStatusCode();
		if (statusCode == HttpStatus.BAD_REQUEST) {
			cct.setMensagem(Cct.REQUISICAO_MAL_FORMATADA.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
			countEnvio(cct);
		} else if (statusCode == HttpStatus.UNAUTHORIZED) {
			cct.setMensagem(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString());
			countEnvio(cct);
		} else if (statusCode == HttpStatus.FORBIDDEN) {
			cct.setMensagem(Cct.REQUISICAO_NAO_AUTORIZADA.toString());
			countEnvio(cct);
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
		} else if (statusCode == HttpStatus.NOT_FOUND) {
			cct.setMensagem(Cct.REGISTRO_NAO_ENCONTRADO.toString());
			countEnvio(cct);
		} else if (statusCode == HttpStatus.UNPROCESSABLE_ENTITY) {
			cct.setMensagem(Cct.ERRO_NEGOCIO.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getResponseBodyAsString()));
			countEnvio(cct);
		} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
		} else if (statusCode == HttpStatus.SERVICE_UNAVAILABLE) {
			cct.setMensagem(Cct.SERVICO_INDISPONIVEL.toString());
		}
		cct.setStatus("Erro");
	}
	
	@Override
	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos) {
//		logger.info("consultaDadosResumidos");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultadadosresumidos");
		for (Cct cct : cctListConsultaDadosResumidos) {
			if(cct.getNrodue().contains(",")) {
				String[] dues = cct.getNrodue().split(",");
				for (String due : dues) {
					due = due.replaceAll(" ", "");
					due = due.trim();
					builder.queryParam("numeroDUE", due);
				}
			} else {
				builder.queryParam("numeroDUE", cct.getNrodue());
			}
		}
		URI url = builder.build().encode().toUri();
		try {
			ResponseEntity<ConsultaDadosResumidosResponseListDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaDadosResumidosResponseListDTO.class);
			ConsultaDadosResumidosResponseListDTO responseDTO = responseEntity.getBody();
			if (responseDTO.getListaRetorno() != null && !responseDTO.getListaRetorno().isEmpty()) {
				if (responseDTO.getListaRetorno().get(0)== null) return null;
				return responseDTO;
			}
		} catch(Exception e) {
			logger.error("SISCOMEX ERROR - consultaDadosResumidos : {} ", e.getMessage());
		}
		return null;
	}
	
	private void countEnvio(Cct cct) {
		logger.info("countEnvio");
		if(cct.getContadorEnvio() == null) {
			cct.setContadorEnvio(1L);
			return;
		} else {
			cct.setContadorEnvio(cct.getContadorEnvio() + 1L);
		}
	}

	@Override
	public void sendRecepcaoCargaSoltaDUE(RecepcoesDocumentoCarga recepcoesDocumentoCarga, Cct cct) {
		logger.info("sendRecepcaoCargaSoltaDUE - Enviando recepcão BBK Due ao siscomex");

		cct.setDtProcessamento(new SimpleDateFormat(FORMATO_DATA).format(new Date(System.currentTimeMillis())));
		cct.setDtproc(new Date(System.currentTimeMillis()));

		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		xstream.setMode(XStream.NO_REFERENCES);

		String body = xstream.toXML(recepcoesDocumentoCarga);
		logger.info("sendRecepcaoCargaSoltaDUE - Body: {} ", body);
		
		cct.setUltimoXmlEnviado(limitaQuantidadeCaracteresBD("RECEPCAO DUE " + body));

		HttpEntity<String> entity = new HttpEntity<>(body);
		try {
			String url = PropertiesUtil.getProp().getProperty("siscomex-service.url");
			logger.info("sendRecepcaoCargaSoltaDUE - url: {} ", url);
			ResponseEntity<RecepcaoResponseDTO> responseEntity = rest.exchange(url, HttpMethod.POST, entity, RecepcaoResponseDTO.class);
			processResponseRecepcaoConteiner(cct, responseEntity);
		} catch (HttpStatusCodeException e) {
			processHttpStatusCodeException(cct, e);
			cctService.save(cct);
		} catch (ResourceAccessException e) {
			cct.setMensagem(Cct.ERRO_TIMEOUT.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("sendRecepcaoCargaSoltaDUE - Fim da recepcão ao siscomex. ResourceAccess Exception");
			cctService.save(cct);
		} catch (Exception e) {
			cct.setMensagem(Cct.ERRO_INTERNO_SERVIDOR.toString());
			cct.setErro(limitaQuantidadeCaracteresBD(e.getMessage()));
			cct.setStatus("Erro");
			logger.error("sendRecepcaoCargaSoltaDUE - Fim da recepcão ao siscomex. Generic Exception");
			cctService.save(cct);
		}		
	}
	
	private void processResponseRecepcaoConteiner(Cct cct, ResponseEntity<RecepcaoResponseDTO> responseEntity) {
//		logger.info("processResponseRecepcaoConteiner");
		RecepcaoResponseDTO response = responseEntity.getBody();
		if (Boolean.valueOf(response.isFlag())) {
			logger.info("#### ERRO ### - {} ", response.getErro());
			
			if(response.getErro() != null && !response.getErro().isEmpty() && response.getErro().contains("parada programada")) {
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setErro(Cct.STATUS_ERRO_PARADA_PROGRAMADA);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			} else {
			
				if (response.getMensagem().isEmpty()) {
					cct.setMensagem(Cct.ERRO_ENVIO_RECEITA);
					countEnvio(cct);
				} else {
					processResponseErrorSiscomexMS(cct, response.getMensagem());
				}
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro(limitaQuantidadeCaracteresBD(response.getErro()));
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		} else {
			logger.info("#### SUCESSO ###");
			cct.setMensagem(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO);
			cct.setStatus(Cct.STATUS_OK);
			cct.setStatusProc(Cct.STATUS_RECEPCIONADO_OK);
			cct.setErro(null);
			cct.setContadorEnvio(0L);
//			cctRepository.save(cct);
			cctService.save(cct);
			try {
				String notes = "DUE: " + cct.getNrodue();
				String retorno = n4Repository.createRecepcaoConteinerEvent(cct, notes);
				logger.info("Retorno createRecepcaoEvent : {} ", retorno);
			} catch(Exception e) {
				logger.info("ERRO CRIANDO EVENTO RECEPCAO: {}", e.getMessage());
				cct.setStatus(Cct.STATUS_ERRO);
				cct.setStatusProc(Cct.STATUS_ERRO_RECEPCAO);
				cct.setErro("ERRO CRIANDO EVENTO RECEPCAO - N4 INDISPONIVEL (AJUSTE MANUAL)");
//				cctRepository.save(cct);
				cctService.save(cct);
			}
		}
	}
	
	@Override
	public ConsultaDadosDueResponseListDTO consultaDadosDue(String dues) {
		logger.info("consultaDadosDue");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultadue");
		builder.queryParam("due", dues);
		URI url = builder.build().encode().toUri();
		
		ConsultaDadosDueResponseListDTO responseDTO = null;
		
		try {			
			ResponseEntity<ConsultaDadosDueResponseListDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaDadosDueResponseListDTO.class);
			responseDTO = responseEntity.getBody();
			if (responseDTO.getListaRetorno() != null && !responseDTO.getListaRetorno().isEmpty()) 				
				processResponseConsultaDUE(responseDTO);
								
		} catch(Exception e) {
			logger.error("consultaDadosDue - SISCOMEX ERROR : {} ", e.getMessage());
		}
		
		logger.info("consultaDadosDue - OK ");		
		return responseDTO;
	}		
	
	private void processResponseConsultaDUE(ConsultaDadosDueResponseListDTO responseDTO) {
		logger.info("processResponseConsultaDUE");
		
		for (ConsultaDadosDueResponseDTO response : responseDTO.getListaRetorno()) {						
			if (response.getDocumentosDeTransporte() != null && !response.getDocumentosDeTransporte().isEmpty()) { 	
				for (DocumentoTransporteDTO docs : response.getDocumentosDeTransporte()) {					
					if (docs.getTipoDocumento().equals("DAT")) {
						
						if (response.getListaCargasSoltasVeiculos() == null && response.getDocumentosDeTransporte().isEmpty()) 
							break;
						
						//get total embalagem
						Integer total = 0;
						Documento documento = documentoRepository.findByNumero(response.getNumeroDUE());
						ItemRepository itemRepository = null; 
						for (CargaSoltaVeiculoDTO cargas : response.getListaCargasSoltasVeiculos()) {
							for (Item itens : documento.getItens()) {
								if (cargas.getTipoEmbalagem().equals(itens.getEmbalagemId())) {
									total=cargas.getQuantidade();
									break;
								}						
							}
						}
						
						Dat dat = new Dat();
						dat.setNumero(docs.getNumeroDocumento());
						dat.setSituacao(1); //coletado
						dat.setVeiculo(docs.getIdentificacaoVeiculo());
						dat.setTotal(total);
						datRepository.save(dat);					
						
						Duedat duedat = new Duedat();
						duedat.setDueId(documento.getId());
						duedat.setDatId(dat.getId());
						duedatRepository.save(duedat);
						
						//Criação do evento UNIT_VINC_DAT
						String unitGkey = n4Repository.getGkeyByConteiner(documento.getNumeroConteiner());	
						String notes = docs.getNumeroDocumento();
						BigDecimal _unitGkey = new BigDecimal(unitGkey);
						String responseCreateUnitVincDatEvent = n4Repository.createUnitVincDatEvent(_unitGkey, notes);
					}
				}
			}
				
		}
		
		logger.info("processResponseConsultaDUE - OK");
	}	
	
	@Override
	public void consultaDuePorNf(DocumentoDueDanfeDTO nf) {
		logger.info("consultaDuePorNf");

		String prefix = PropertiesUtil.getProp().getProperty("siscomex-service.url");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prefix + "/consultarduepornf");
		builder.queryParam("nf", nf.getDanfe());
		URI url = builder.build().encode().toUri();
		
		try {			
			ResponseEntity<ConsultaDadosDueResponseDTO> responseEntity = rest.exchange(url, HttpMethod.GET, new HttpEntity<String>(""), ConsultaDadosDueResponseDTO.class);
			if (responseEntity != null) 
				processResponseConsultaDuePorNf(nf, responseEntity.getBody().getNumeroDUE());
								
		} catch(Exception e) {
			logger.error("consultaDuePorNf - SISCOMEX ERROR : {} ", e.getMessage());
		}
		
		logger.info("consultaDuePorNf - OK ");		
		return;
	}		
	
	public void processResponseConsultaDuePorNf(DocumentoDueDanfeDTO nf, String due) {
		
		Documento documentoDue = documentoRepository.findByNumero(due);
		if(documentoDue == null) {
			logger.error("processResponseConsultaDuePorNf - Due não localizada : {} ", due);
			return;
		}
		
		Duedanfe duedanfe = new Duedanfe();
		duedanfe.setDanfeId(nf.getId_documento());
		duedanfe.setDueId(documentoDue.getId());
		duedanfeRepository.save(duedanfe);

		Documento documentoDafe = null;
		Optional<Documento> documentoDanfeAtualizar = documentoRepository.findById(nf.getId_documento());
		if(documentoDanfeAtualizar.isPresent()) {
			documentoDafe = documentoDanfeAtualizar.get();						
			documentoDafe.setStatus(1); //informações coletadas
			documentoRepository.save(documentoDafe);
		}
		
		//Criação do evento BL_VINC_DUE
		String blGkey = n4Repository.getBLGkeyByBlNbr(due);
		String responseCreateBlVincDueEvent = n4Repository.createBLEvent(blGkey, "BL_VINC_DUE", due);
		
		//Criação do evento LIBERACAO_PARA_EMBARQUE
		String unitGkey = n4Repository.getGkeyByConteiner(documentoDafe.getNumeroConteiner());	
		BigDecimal _unitGkey = new BigDecimal(unitGkey);
		String responseCreateUnitLiberacaoEmbarqueEvent = n4Repository.createUnitLiberacaoEmbarqueEvent(_unitGkey, "");		
	}
	
}
