package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaDadosResumidosResponseDTO_TMP {
	
	private String numeroDUE;
	
	private String numeroRUC;
	
	private String situacaoDUE;
	
	private String dataSituacaoDUE;
	
	private String indicadorBloqueio;
	
	private String controleAdministrativo;
	
	private String uaEmbarque;
	
	private String uaDespacho;
	
	private String responsaveluaDespacho;
	
	private String codigoRecintoAduaneiroDespacho;
	
	private String codigoRecintoAduaneiroEmbarque;
	
	private String coordenadasDespacho;
	
	private String recintoDespacho;
	
	private Declarante declarante;
	
	private List<Exportador> exportadores;
	
	private List<Integer> situacaoCarga;
	
}
