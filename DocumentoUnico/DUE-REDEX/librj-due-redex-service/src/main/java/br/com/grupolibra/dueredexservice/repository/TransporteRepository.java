package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueredexservice.model.Transporte;

@Repository
public interface TransporteRepository extends CrudRepository<Transporte, Long>, QuerydslPredicateExecutor<Transporte> {
	
	public List<Transporte> findByCnpjTransportadora(String cnpjTransportadora);
	
}