package br.com.grupolibra.dueredexservice.model.siscomex;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("entregaConteiner")
public class EntregaConteiner {
	
	private String identificacaoEntrega;
	
	private String identificacaoPessoaJuridica;
	
	private Local local;
	
	private List<Conteiner> conteineres;
	
	private Recebedor recebedor;
	
	private String avariasIdentificadas;
	
	private String divergenciasIdentificadas;
	
	private String observacoesGerais;

	public String getIdentificacaoEntrega() {
		return identificacaoEntrega;
	}

	public void setIdentificacaoEntrega(String identificacaoEntrega) {
		this.identificacaoEntrega = identificacaoEntrega;
	}

	public String getIdentificacaoPessoaJuridica() {
		return identificacaoPessoaJuridica;
	}

	public void setIdentificacaoPessoaJuridica(String identificacaoPessoaJuridica) {
		this.identificacaoPessoaJuridica = identificacaoPessoaJuridica;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public List<Conteiner> getConteineres() {
		return conteineres;
	}

	public void setConteineres(List<Conteiner> conteineres) {
		this.conteineres = conteineres;
	}

	public Recebedor getRecebedor() {
		return recebedor;
	}

	public void setRecebedor(Recebedor recebedor) {
		this.recebedor = recebedor;
	}

	public String getAvariasIdentificadas() {
		return avariasIdentificadas;
	}

	public void setAvariasIdentificadas(String avariasIdentificadas) {
		this.avariasIdentificadas = avariasIdentificadas;
	}

	public String getDivergenciasIdentificadas() {
		return divergenciasIdentificadas;
	}

	public void setDivergenciasIdentificadas(String divergenciasIdentificadas) {
		this.divergenciasIdentificadas = divergenciasIdentificadas;
	}

	public String getObservacoesGerais() {
		return observacoesGerais;
	}

	public void setObservacoesGerais(String observacoesGerais) {
		this.observacoesGerais = observacoesGerais;
	}
}
