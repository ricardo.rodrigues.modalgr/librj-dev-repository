package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Dat;

@Repository
public interface DatRepository extends CrudRepository<Dat, Long>, QuerydslPredicateExecutor<Dat> {	
	
	public Dat findByNumero(String numero);
	
	public List<Dat> findAll(Predicate predicate);
}
