package br.com.grupolibra.dueredexservice.repository.n4;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.text.MaskFormatter;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.common.base.CharMatcher;

import br.com.grupolibra.dueredexservice.dto.CadastroConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.ClienteFinanceiroPagadorDTO.Tabela;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.MotoristaReciboDTO;
import br.com.grupolibra.dueredexservice.model.Armador;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Danfe;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Entregador;
import br.com.grupolibra.dueredexservice.model.Exportador;
import br.com.grupolibra.dueredexservice.model.Parametros;
import br.com.grupolibra.dueredexservice.model.Recebedor;
import br.com.grupolibra.dueredexservice.model.Shipper;
import br.com.grupolibra.dueredexservice.model.Transporte;
import br.com.grupolibra.dueredexservice.repository.ArmadorRepository;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.ExportadorRepository;
import br.com.grupolibra.dueredexservice.repository.ParametrosRepository;
import br.com.grupolibra.dueredexservice.repository.ShipperRepository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;
import br.com.grupolibra.dueredexservice.util.QueryUtil;
import br.com.grupolibra.dueredexservice.util.SituacaoDUEEnum;
import br.com.grupolibra.fmwPortService.exception.N4ServiceInvokeException;
import br.com.grupolibra.fmwPortService.genericInvoke.N4ServiceInvoke;
import br.com.grupolibra.fmwPortService.genericInvoke.ServiceParam;
import br.com.grupolibra.fmwPortService.helper.logger.LoggerN4;

@Component
public class N4Repository {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String LOG_N4 = LoggerN4.ERROR;

	private static final String DUE_REDEX_GROOVY_PROXY_NAME = "DueRedexServiceProxy";

	private static final String EXPRT = "EXPRT";

	private static final String TRSHP = "TRSHP";

	private static final String VESSEL = "VESSEL";

	private static final String ERRO = "ERRO : ";

	private static final String NOTES = "notes";

	private static final String EVENT = "event";

	private static final String HOLD = "hold";

	private static final String PERMISSION = "permission";

	private static final String UNIT_GKEY = "unitGkey";

	private static final String BR = "BR";

	private static final String ACTION = "action";

	private static final String DUE_GROOVY_PROXY_NAME = "DueRedexServiceProxy";

	private static final String CREATE_UNIT_EVENT_SERVICE = "createUnitEvent";

	private static final String APPLY_HOLD_SERVICE = "applyHoldUnit";

	private static final String APPLY_PERMISSION_SERVICE = "applyPermissionUnit";

	private static final String CREATE_UNIT_DUE_EVENTS_SERVICE = "createUnitDUEEvents";

	private static final String CREATE_DESBLOQUEIO_EVENTS_SERVICE = "createDesbloqueioEvents";

	private static final String CREATE_CFP_EVENT_SERVICE = "createCFPEvent";

	private static final String CREATE_CFP_LIBERACAO_DE_EXPORTACAO_EVENT_SERVICE = "createCFPLiberacaoExportacaoEvent";

	private static final String RECEPCAO_NFE_UNIT_EVENT = "UNIT_RECEP_DANFE";
	
	private static final String CREATE_UNIT_VINC_DAT_EVENT = "UNIT_VINC_DAT";
	
	private static final String CREATE_UNIT_LIBERACAO_PARA_EMBARQUE_EVENT = "LIBERACAO_PARA_EMBARQUE";

	private static final String RECEPCAO_CONTEINER_TRANSBORDO_MARITIMO_UNIT_EVENT = "UNIT_RECEP_TRANSBORDO";

	private static final String RECEPCAO_CONTEINER_DUE_UNIT_EVENT = "UNIT_RECEP_DUE";

	private static final String ENTREGA_UNIT_EVENT = "UNIT_ENTREGA";

	private static final String ENTREGA_TRANSBORDO_MARITIMO_UNIT_EVENT = "UNIT_ENTREGA_DUE";

	private static final String HOLD_PERMISSION_EVENT = "PRT_REQUIRES_EXP_CUSTOMS_CLEARANCE";

	private static final String UNITGKEY = "UNITGKEY";

	private static final String ROUTING_POINT_GKEY = "ROUTING_POINT_GKEY";

	private static final String DIVERGENCIA_PESO_ACIMA = "DIVERGENCIA_PESO_ACIMA";

	private static final String VALIDACAO_LIBERACAO = "VALIDACAO_LIBERACAO";

	private String divergenciaPesoAcima;

	private static final String DIVERGENCIA_PESO_ABAIXO = "DIVERGENCIA_PESO_ABAIXO";

	private String divergenciaPesoAbaixo;

	private static final String USERDATABASE = PropertiesUtil.getProp().getProperty("n4.datasource.schema");

	private static final String CNPJ = "CNPJ";

	private static final String UNIT_RECEIVE = "UNIT_RECEIVE";

	private static final String UNIT_IN_VESSEL = "UNIT_IN_VESSEL";

	private static final String RESPONSIBLE_PARTY = "responsiblePartyGkey";

	private static final String CNPJ_PATTERN = "##.###.###/####-##";

	private static final String NBR = "NBR";

	private static final String ATUALIZA_BOOKING_SHIPPER = "atualizaBookingShipper";
	
	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";
	
	private static final String FCL = "FCL";
	
	private static final String BBK = "BBK";
	
	private static final String BALANCA_LCL = "BALANCA_LCL";
	
	private static final String BALANCA_FCL = "BALANCA_FCL";
	
	private static final String BALANCA_RODOVIARIA = "BALANCA_RODOVIARIA";
	

	@Autowired
	@Qualifier("n4EntityManagerFactory")
	private EntityManager entityManager;

	@Autowired
	@Qualifier("n4ServiceInvoke")
	private N4ServiceInvoke n4ServiceInvoke;

	@Autowired
	private CCTRepository cctRepository;

	@Autowired
	private ArmadorRepository armadorRepository;

	@Autowired
	private ExportadorRepository exportadorRepository;

	@Autowired
	private ParametrosRepository parametrosRepository;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private DanfeRepository danfeRepository;
	
	@Autowired
	private CctService cctService;
	
	@Autowired
	private ShipperRepository shipperRepository;	
	
	@Autowired
	private DocumentoRepository documentoRepository;

	@SuppressWarnings("unchecked")
	public Object[] getBooking(String bookingNbr) {
		logger.info("getBooking");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_BY_NBR);
		q.setParameter("NBR", bookingNbr);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
		} catch (Exception e) {
			logger.info("getBooking - ERROR : {} ", e.getMessage());
		}
		return null;
	}

	public List<Object[]> getBookingItens(String bookingGkey) {
		logger.info("getBookingItens");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_ITENS_BY_BOOKING_GKEY);
		q.setParameter("GKEY", bookingGkey);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList;
			}
		} catch (Exception e) {
			logger.error("getBookingItens - {} ", e.getMessage());
		}
		return null;
	}

	public String getQtdDisponivelItem(String bookingGkey, String iso, String quantidade) {
		logger.info("getQtdDisponivelItem");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_QTD_CONTEINER_DISPONIVEL_BY_ISO_AND_BOOKING_GKEY);
		q.setParameter("ISO", iso);
		q.setParameter("GKEY", bookingGkey);
		Object result = null;
		try {
			result = q.getSingleResult();
			if (result != null) {
				Integer qtdDisponivel = Integer.parseInt(quantidade) - Integer.parseInt(result.toString());
				return qtdDisponivel.toString();
			}
		} catch (Exception e) {
			logger.error("getQtdDisponivelItem - {} ", e.getMessage());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getConteinerListByBookingGkey(String bookingGkey) {
		logger.info("getConteinerListByBookingGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CONTEINER_LIST_BY_BOOKING_GKEY);
		q.setParameter("GKEY", bookingGkey);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList;
			}
		} catch (Exception e) {
			logger.error("getConteinerListByBookingGkey - {} ", e.getMessage());
		}
		return null;
	}

	public Object[] validaUnitId(String numeroConteiner) {
		logger.info("validaUnitId - N4");
		logger.info("validaUnitId - N4 - numeroConteiner -> {} ", numeroConteiner);
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "validaUnitId");
			param.addParameter("numeroConteiner", numeroConteiner);

			Object[] retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, Object[].class, LOG_N4);
			logger.info("validaUnitId - RETORNO = {} - {} ", retorno[0], retorno[1]);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("validaUnitId - {} ", e.getMessage());
			return new Object[] { true, e.getMessage() };
		}
	}

	public Object[] saveConteiner(String bookingGkey, ConteinerDTO conteinerDTO) {
		logger.info("saveConteiner - N4");
		logger.info("saveConteiner - N4 - equipmentOrderItemGkey -> {} ", conteinerDTO.getBookingItem().getItemGkey());
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "generateUnit");
			param.addParameter("bookingGkey", bookingGkey);
			param.addParameter("equipmentOrderItemGkey", conteinerDTO.getBookingItem().getItemGkey());
			param.addParameter("numeroConteiner", conteinerDTO.getNumeroConteiner());
			param.addParameter("pesoBruto", conteinerDTO.getPesoBruto());
			param.addParameter("lacre1", conteinerDTO.getLacre1());
			param.addParameter("lacre2", conteinerDTO.getLacre2());
			param.addParameter("lacre3", conteinerDTO.getLacre3());
			param.addParameter("lacre4", conteinerDTO.getLacre4());
			param.addParameter("excessoFrontal", conteinerDTO.getExcessoFrontal());
			param.addParameter("excessoTraseiro", conteinerDTO.getExcessoTraseiro());
			param.addParameter("excessoAltura", conteinerDTO.getExcessoAltura());
			param.addParameter("excessoDireita", conteinerDTO.getExcessoDireita());
			param.addParameter("excessoEsquerda", conteinerDTO.getExcessoEsquerda());

			Object[] retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, Object[].class, LOG_N4);
			logger.info("saveConteiner - RETORNO = {} - {} - {} ", retorno[0], retorno[1], retorno[2]);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("saveConteiner - {} ", e.getMessage());
			return new Object[] { true, e.getMessage() };
		}
	}

	public boolean hasBookingItem(String bookingGkey) {
		List<Object> resultList = entityManager.createNativeQuery(QueryUtil.SELECT_INV_EQ_BASE_ORDER_ITEM_GKEY_BY_BOOKING_GKEY).setParameter("GKEY", bookingGkey).getResultList();
		return (resultList != null && !resultList.isEmpty());
	}

	public Object[] updateConteiner(ConteinerDTO conteinerDTO) {
		logger.info("updateConteiner");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "updateUnit");
			param.addParameter("unitGkey", conteinerDTO.getGkey());
			param.addParameter("pesoBruto", conteinerDTO.getPesoBruto());
			param.addParameter("lacre1", conteinerDTO.getLacre1());
			param.addParameter("lacre2", conteinerDTO.getLacre2());
			param.addParameter("lacre3", conteinerDTO.getLacre3());
			param.addParameter("lacre4", conteinerDTO.getLacre4());
			param.addParameter("excessoFrontal", conteinerDTO.getExcessoFrontal());
			param.addParameter("excessoTraseiro", conteinerDTO.getExcessoTraseiro());
			param.addParameter("excessoAltura", conteinerDTO.getExcessoAltura());
			param.addParameter("excessoDireita", conteinerDTO.getExcessoDireita());
			param.addParameter("excessoEsquerda", conteinerDTO.getExcessoEsquerda());

			Object[] retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, Object[].class, LOG_N4);
			logger.info("updateConteiner - RETORNO = {} - {}", retorno[0], retorno[1]);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("updateConteiner - ERROR : {} ", e.getMessage());
			return new Object[] { true, e.getMessage() };
		}
	}
		
	public String deleteDocument(String blGkey){
		String retorno = "";
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, "deleteBL");
			param.addParameter("blGkey", blGkey);
			retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
			logger.info("deleteDocument - RETORNO N4 : {} ", retorno);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("deleteDocument  - " + e.getMessage());
			retorno = "ERRO";
		}
		return retorno;
	}
		
	public CadastroConteinerDTO deleteUnit(String gkey) {

		logger.info("deleteUnit");
		
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		retorno.setErro(false);

		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, "deleteUnit");
			param.addParameter("unitGkey", gkey);
			String retornoN4 = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
			logger.info("deleteUnit - RETORNO N4 : {} ", retorno);
			
			if("OK".equals(retornoN4)) {
				retorno.setMensagem("Unidade excluida com sucesso.");
				retorno.setErro(false);
			} else {
				retorno.setMensagem("Erro ao excluir unidade, por favor entre em contato com o administrador do sistema.");
				retorno.setErro(true);
			}
			
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("deleteUnit - " + e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem(ERRO + e.getMessage());
		}
		return retorno;
	}

	@SuppressWarnings("unchecked")
	public boolean hasUnitInGateEvent(String gkey) {
		logger.info("hasUnitInGateEvent");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_IN_GATE_EVENT);
		q.setParameter("UNITGKEY", gkey);
		List<Object> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return true;
			}
		} catch (Exception e) {
			logger.error("hasUnitInGateEvent - " + e.getMessage());
		}
		return false;
	}

	public String createUnitEvent(String unitGkey, String event, String notes) {
		logger.info("createUnitEvent");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "createUnitEvent");
			param.addParameter("unitGkey", unitGkey);
			param.addParameter("event", event);
			param.addParameter("notes", notes);
			String retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
			logger.info("createUnitEvent - RETORNO = {} ", retorno);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("createUnitEvent - {} ", e.getMessage());
			return "ERRO - " + e.getMessage();
		}
	}

	@SuppressWarnings("unchecked")
	public String getUnitGkeyByUnitId(String id) {
		logger.info("getUnitGkeyByUnitId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_GKEY_BY_UNIT_ID);
		q.setParameter("ID", id);
		List<Object> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("getUnitGkeyByUnitId - {} ", e.getMessage());
		}
		return null;
	}

	public Object[] getTransportadoraInfoByCnpj(String cnpj) {
		logger.info("getTransportadoraInfoByCnpj");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_TRANSPORTADORA_INFO_BY_CNPJ);
		q.setParameter("CNPJ", cnpj);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
		} catch (Exception e) {
			logger.error("getTransportadoraInfoByCnpj - {} ", e.getMessage());
		}
		return null;
	}

	public Object[] getConteinerByGkey(Long unitGkey) {
		logger.info("getConteinerByGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CONTEINER_BY_GKEY);
		q.setParameter("GKEY", unitGkey);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
		} catch (Exception e) {
			logger.error("getConteinerByGkey - {} ", e.getMessage());
		}
		return null;
	}

	public String getReciboSequenceNextValue() {
		logger.info("getReciboSequenceNextValue");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_RECIBO_SEQ_NEXTVAL);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch (Exception e) {
			o = "99999";
		}
		return o.toString();
	}

	public Cct getCctRecepcaoNFEByUnitGkey(String unitGkey) throws ParseException {
		logger.info("getCctRecepcaoNFEByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.NFE.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);
		
		setDadosTransporteRedex(unitGkey, cct);

//		cct.setTipoTransportador(0);
//		cct.setCnpjTransportador("25284506000109");

		setDadosConteiner(unitGkey, cct);
		setDanfesRedex(cct);
		setBooking(unitGkey, cct);
		setDataEntrada(unitGkey, cct, UNIT_RECEIVE);
		setUnitSeal(unitGkey, cct);
		setRegraPesagemRecepcao(unitGkey, cct);
		setDadosArmador(unitGkey, cct);
		
		setDadosViagem(unitGkey, cct);
		
		
		return cct;
	}		
	
	public Cct getCctRecepcaoDUECargaSoltaByUnitGkey(String unitGkey) throws ParseException {
		logger.info("getCctRecepcaoDUECargaSoltaByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.DUE.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);
		
		setDataEntrada(unitGkey, cct, UNIT_RECEIVE);
		setDadosConteinerDue(unitGkey, cct);
		
		//Balança Carga Solta LCL inoperante ou inexistente
		setRegraPesagemRecepcao(unitGkey, cct);
		setUnitSeal(unitGkey, cct);

		setDueCargaSoltaRedex(cct);
		
		setBillOfLanding(cct);

		setDadosTransporte(unitGkey, cct);

//		cct.setTipoTransportador(0);
//		cct.setCnpjTransportador("25284506000109");
//		cct.setCpfCondutor("42411080832");
//		cct.setPlacaveiculo("BUA1234");
//		cct.setPlacareboque("CEU9876");
		
		setDadosEntregadorDUE(unitGkey, cct);
		setDadosRecebedorByOutBoundCarrier(unitGkey, cct);
		
		setDadosViagem(unitGkey, cct);
		
		return cct;
	}
	
	public Cct getCctRecepcaoNFECargaSoltaByUnitGkey(String unitGkey) throws ParseException{
		logger.info("getCctRecepcaoNFECargaSoltaByUnitGkey");
		Cct cct = validaConteinerExistenteBaseCCT(unitGkey);
		cct.setIdExterno(new BigDecimal(unitGkey));
		cct.setArmador(null);
		cct.setCategoria(CategoriaProcessoEnum.NFE.toString());
		cct.setContadorEnvio(0L);
		cct.setFinalizado(0);
		
		setDadosTransporteRedex(unitGkey, cct);

//		cct.setTipoTransportador(0);
//		cct.setCnpjTransportador("25284506000109");
		
		setDadosConteiner(unitGkey, cct);
		setBillOfLanding(cct);
//		setDanfesCargaSoltaRedex(cct);
		setBooking(unitGkey, cct);
		setDataEntrada(unitGkey, cct, UNIT_RECEIVE);
//		setUnitSeal(unitGkey, cct);
		setRegraPesagemRecepcao(unitGkey, cct);
		setDadosArmador(unitGkey, cct);		
//		setDadosViagem(unitGkey, cct);
				
		return cct;
	}	

	public void setDadosViagem(String unitGkey, Cct cct) {
		logger.info("setDadosViagem");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOME_NAVIO_OUTBOUND_CARRIER_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] obj = null;
		try {
			obj = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		if(obj != null) {
			cct.setNavio(obj[0].toString());
			cct.setViagem(obj[1].toString());
		}
	}

	@SuppressWarnings("unchecked")
	private void setDadosEntregadorDUE(String unitGkey, Cct cct) {
		logger.info("setDadosEntregadorDUE");
		Entregador entregador = null;
		if(cct.getEntregador() != null) {
			entregador = cct.getEntregador();
		} else {
			entregador = new Entregador();
		}
		if(cct.getTipoTransportador() == 1) {
			entregador.setNome(cct.getNomeTransportador()); 
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CUSTOM_RECINTO_BY_CNPJ_TRANSPORTADORA);
			q.setParameter(CNPJ, formataCNPJ(cct.getCnpjTransportador()));
			List<Object[]> rows = q.getResultList();
			if (rows != null && !rows.isEmpty()) {
				entregador.setCpf(cct.getCpfCondutor());
			} else {
				entregador.setCnpj(cct.getCnpjTransportador());
			}
		}
		entregador.setCct(cct);
		cct.setEntregador(entregador);
	}

	@SuppressWarnings("unchecked")
	public void setDadosRecebedorByOutBoundCarrier(String unitGkey, Cct cct) {
		logger.info("setDadosRecebedorByOutBoundCarrier");

		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Recebedor recebedor = null;
			if(cct.getRecebedor() != null) {
				recebedor = cct.getRecebedor();
			} else {
				recebedor = new Recebedor();
			}
			String tipo = "" + rows.get(0)[0];
			if ("ESTRANGEIRO".equals(tipo)) {
				recebedor.setNome("" + rows.get(0)[2]);
			} else if ("CNPJ".equals(tipo)) {
				recebedor.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
			recebedor.setCct(cct);
			cct.setRecebedor(recebedor);
			
			Optional<Armador> armador = armadorRepository.findByIdExterno(new BigDecimal(rows.get(0)[3].toString()));
			if (armador.isPresent()) {
				cct.setArmador(armador.get());
			} else {
				Armador novoArmador = new Armador();
				novoArmador.setIdExterno(new BigDecimal(rows.get(0)[3].toString()));
				novoArmador.setNome("" + rows.get(0)[2]);
				if ("ESTRANGEIRO".equals(tipo))
					novoArmador.setEstrangeiro(1);
				novoArmador.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
				armadorRepository.save(novoArmador);
				cct.setArmador(novoArmador);
			}
		} else {
			logger.info("setDadosRecebedorByOutBoundCarrier - ERRO SEM DADOS SELECT_ACTUAL_ARMADOR_BY_UNIT_GKEY ");
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getRecebedorIdByConteinerGkey(String unitGkey) {
		logger.info("getRecebedorIdByConteinerGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Object[] o = rows.get(0);
			if(o != null) {
				return (o[2] != null ? o[2].toString() : null);
			}
		}
		return null;
	}

	private Cct validaConteinerExistenteBaseCCT(String unitGkey) {
		logger.info("validaConteinerExistenteBaseCCT");
		Cct cct = null;
		List<Cct> cctlist = cctRepository.findByIdExterno(new BigDecimal(unitGkey));
		if (cctlist.isEmpty()) {
			cct = new Cct();
		} else {
			cct = cctlist.get(0);
		}
		return cct;
	}

	@SuppressWarnings("unchecked")
	private void setRegraPesagemRecepcao(String unitGkey, Cct cct) {		
		logger.info("setRegraPesagemRecepcao");
		
		Parametros statusBalanca;
		
		Documento documento = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());
		if(documento != null){
			logger.info("setRegraPesagemRecepcao - Carga solta");
			if(documento.getConsolidador() != null){
				logger.info("setRegraPesagemRecepcao - Carga solta | Tem info de consolidador - BALANCA LCL");
				statusBalanca = parametrosRepository.findByChave(BALANCA_LCL);
				if(statusBalanca != null && "1".equals(statusBalanca.getValor())){
					cct.setPendenciaPeso(false);
					return;
				}
			}else{
				logger.info("setRegraPesagemRecepcao - Carga solta | N tem info de consolidador - BALANCA FCL");
				statusBalanca = parametrosRepository.findByChave(BALANCA_FCL);
				if(statusBalanca != null && "1".equals(statusBalanca.getValor())) {
					cct.setPendenciaPeso(false);
					return;
				}
			}
		}else{
			logger.info("setRegraPesagemRecepcao - Pre stacking");
			if(cct.getDanfes()!= null){
				logger.info("setRegraPesagemRecepcao - BALANCA_RODOVIARIA");
				statusBalanca = parametrosRepository.findByChave(BALANCA_RODOVIARIA);
				if(statusBalanca != null && "1".equals(statusBalanca.getValor())) {
					cct.setPendenciaPeso(false);
					return;
				}
			}
		}
	
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_REGRA_PESAGEM_RECEPCAO);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			cct.setOog((rows.get(0)[0] != null && "1".equals(rows.get(0)[0].toString())) ? true : false);
			cct.setFirstWeighing(("YES".equals(rows.get(0)[1])) ? true : false);
			if (cct.isFirstWeighing()) {
				cct.setPendenciaPeso(false);
			} else if (cct.isOog()) {
				cct.setPendenciaPeso(false);
				cct.setMotivoNaoPesagem(Cct.MOTIVO_NAO_PESAGEM);
				cct.setPesoBruto(null);
			} else {
				cct.setPendenciaPeso(true);
			}
		} else {
			cct.setPendenciaPeso(true);
		}
	}

	@SuppressWarnings("unchecked")
	private void setUnitSeal(String unitGkey, Cct cct) {
		logger.info("setUnitSeal");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_SEAL_EVENT);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		cct.setUnitSeal(!rows.isEmpty() && rows.get(0) != null ? true : false);
	}

	@SuppressWarnings("unchecked")
	private void setDataEntrada(String unitGkey, Cct cct, String event) throws ParseException {
		logger.info("setDataEntrada");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENT_PLACED_TIME);
		q.setParameter(UNITGKEY, unitGkey);
		q.setParameter("EVENT", event);
		q.setParameter("EVENTTEST", event+"_TESTE_SISCOMEX");
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty() && rows.get(0) != null) {
			try {
				Object dtTmp=rows.get(0);
				cct.setDtentr(new SimpleDateFormat(FORMATO_DATA).parse((String) dtTmp));
			} catch(Exception e) {
				cct.setDtentr(null);
				logger.error("SEM DATA DE ENTRADA");
			}			
		} else {
			logger.error("SEM DATA DE ENTRADA");
		}
	}

	@SuppressWarnings("unchecked")
	public void setBooking(String unitGkey, Cct cct) {
		logger.info("setBooking");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_NUMBER_POR_NUMERO_CONTEINER);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		cct.setBooking(rows != null && !rows.isEmpty() ? "" + rows.get(0) : "");
	}
	
	@SuppressWarnings("unchecked")
	private void setDadosTransporte(String unitGkey, Cct cct) {
		logger.info("setDadosTransporte");
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_TRANSPORTADORA_CONDUTOR);
		q.setParameter(UNITGKEY, unitGkey);
		
		List<Object[]> rows = q.getResultList();
		
		if (rows != null && !rows.isEmpty()) {
			String tipo = "" + rows.get(0)[4];
			if ("ESTRANGEIRO".equals(tipo)) {
				cct.setTipoTransportador(1);
				cct.setNomeTransportador("" + rows.get(0)[5]);
				cct.setNomeCondutor("" + rows.get(0)[6]);
				cct.setCpfTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			} else if ("CPF".equals("" + rows.get(0)[4])) {
				cct.setTipoTransportador(0);
				cct.setCpfTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			} else if ("CNPJ".equals("" + rows.get(0)[4])) {
				cct.setTipoTransportador(0);
				cct.setCnpjTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
			cct.setCpfCondutor(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[0]));
			cct.setPlacaveiculo("" + rows.get(0)[2]);
			cct.setPlacareboque("" + rows.get(0)[3]);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setDadosTransporteRedex(String unitGkey, Cct cct) {
		logger.info("setDadosTransporteRedex");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_INBOUND_LINE_OPERATOR_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			String tipo = "" + rows.get(0)[0];
			if ("ESTRANGEIRO".equals(tipo)) {
				cct.setTipoTransportador(1);
				cct.setNomeTransportador("" + rows.get(0)[2]);
			} else if ("CNPJ".equals(tipo)) {
				cct.setTipoTransportador(0);
				cct.setCnpjTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			} else if ("CPF".equals(tipo)) {
				cct.setTipoTransportador(0);
				cct.setCpfTransportador(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
			}
		} else {
			logger.info("setDadosTransporteRedex - ERROR : Transportadora não localizada.");			
		}
	}

	@SuppressWarnings("unchecked")
	private void setDadosArmador(String unitGkey, Cct cct) {
		logger.info("setDadosArmador");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ACTUAL_RECEBEDOR_ARMADOR_BY_UNIT_GKEY);
		
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			Optional<Armador> armador = armadorRepository.findByIdExterno(new BigDecimal(rows.get(0)[3].toString()));
			if (armador.isPresent()) {
				cct.setArmador(armador.get());
			} else {
				Armador novoArmador = new Armador();
				novoArmador.setIdExterno(new BigDecimal(rows.get(0)[3].toString()));
				novoArmador.setNome("" + rows.get(0)[2].toString());
				if ("ESTRANGEIRO".equals("" + rows.get(0)[0]))
					novoArmador.setEstrangeiro(1);
				novoArmador.setCnpj(CharMatcher.inRange('0', '9').retainFrom("" + rows.get(0)[1]));
				armadorRepository.save(novoArmador);
				cct.setArmador(novoArmador);
			}
			//cct.setNavio("" + rows.get(0)[4]);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setDanfes(String unitGkey, Cct cct) {
		logger.info("setDanfes");
		List<Danfe> danfeList = new ArrayList<>();
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DANFE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		if (cct.getDanfes() != null && !cct.getDanfes().isEmpty()) {
			Set<String> cctDanfes = new HashSet<>();
			cct.getDanfes().forEach(danfe -> cctDanfes.add(danfe.getChave()));
			Set<String> n4Danfes = new HashSet<>();
			rows.forEach(chave -> n4Danfes.add(chave));
			n4Danfes.removeAll(cctDanfes);
			if (n4Danfes.isEmpty()) {
				return;
			} else {
				for (String chaveDanfe : n4Danfes) {
					Danfe danfe = new Danfe();
					danfe.setChave(chaveDanfe);
					if(danfe.getChave() != null && !"".equals(danfe.getChave()) && !danfe.getChave().isEmpty()) {
						danfeList.add(danfe);
					}
				}
			}
		} else {
			for (String chaveDanfe : rows) {
				Danfe danfe = new Danfe();
				danfe.setChave(chaveDanfe);
				if(danfe.getChave() != null && !"".equals(danfe.getChave()) && !danfe.getChave().isEmpty()) {
					danfeList.add(danfe);
				}
			}
		}
		cct.setDanfes(danfeList);
	}
	
	private void setDanfesRedex(Cct cct) {
		logger.info("setDanfesRedex");
		cct.setDanfes(danfeRepository.findByNumeroConteiner(cct.getNumeroCont()));
		for (Danfe danfe : cct.getDanfes()) {
			danfe.setCct(cct);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setDues(String unitGkey, Cct cct) {
		logger.info("setDues");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DUE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		String dues = "";
		for (String due : rows) {
			dues = dues + due + ",";
		}
		cct.setNrodue(dues.substring(0, dues.length()-1));
	}
	
	private void setDueCargaSoltaRedex(Cct cct){
		logger.info("setDueCargaSoltaRedex");
		Documento documento = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());
		cct.setNrodue(documento.getNumero());
	}
	
	private void setBillOfLanding(Cct cct){
		logger.info("setBillOfLanding");
		Documento doc = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());
		cct.setBilloflanding(doc.getBlNbr());
	}

	@SuppressWarnings("unchecked")
	public boolean hasDanfe(String unitGkey) {
		logger.info("hasDanfe");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DANFE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		return rows != null && !rows.isEmpty();
	}
	
	@SuppressWarnings("unchecked")
	public boolean hasDUE(String unitGkey) {
		logger.info("hasDUE");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CHAVE_DUE);
		q.setParameter(UNITGKEY, unitGkey);
		List<String> rows = q.getResultList();
		return rows != null && !rows.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void setDadosConteiner(String unitGkey, Cct cct) {
		logger.info("setDadosConteiner");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_CONTEINER);
		q.setParameter(DIVERGENCIA_PESO_ACIMA, Double.parseDouble(this.getDivergenciaPesoAcima()));
		logger.info("setDadosConteiner - DIVERGENCIA_PESO_ACIMA : {} ", this.getDivergenciaPesoAcima());
		q.setParameter(DIVERGENCIA_PESO_ABAIXO, Double.parseDouble(this.getDivergenciaPesoAbaixo()));
		logger.info("setDadosConteiner - DIVERGENCIA_PESO_ABAIXO : {} ", this.getDivergenciaPesoAbaixo());
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			cct.setNumeroCont("" + rows.get(0)[0]);
			cct.setTara(new BigDecimal(rows.get(0)[1] != null ? rows.get(0)[1].toString() : "0").setScale(3));
			cct.setPesoBruto(new BigDecimal(rows.get(0)[2] != null ? rows.get(0)[2].toString() : "0").setScale(3));

			if (rows.get(0)[7] != null) {
				q = entityManager.createNativeQuery(QueryUtil.SELECT_AVARIAS);
				q.setParameter("AVARIA_FK", rows.get(0)[7].toString());
				List<String> results = q.getResultList();
				cct.setAvaria(String.join("\n", results));
			} else {
				cct.setAvaria("");
			}
			preencheLacres(cct, rows);
			cct.setDivergenciasIdentificadas(rows.get(0)[8] != null ? rows.get(0)[8].toString() : "");
		}
	}
	
	@SuppressWarnings("unchecked")
	private void setDadosConteinerDue(String unitGkey, Cct cct) {
		logger.info("setDadosConteinerDue");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_CONTEINER);
		q.setParameter(DIVERGENCIA_PESO_ACIMA, Double.parseDouble(this.getDivergenciaPesoAcima()));
		q.setParameter(DIVERGENCIA_PESO_ABAIXO, Double.parseDouble(this.getDivergenciaPesoAbaixo()));
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			cct.setNumeroCont("" + rows.get(0)[0]);
			cct.setTara(new BigDecimal(rows.get(0)[1] != null ? rows.get(0)[1].toString() : "0").setScale(3));
			cct.setPesoBruto(new BigDecimal(rows.get(0)[2] != null ? rows.get(0)[2].toString() : "0").setScale(3));

			if (rows.get(0)[7] != null) {
				q = entityManager.createNativeQuery(QueryUtil.SELECT_AVARIAS);
				q.setParameter("AVARIA_FK", rows.get(0)[7].toString());
				List<String> results = q.getResultList();
				cct.setAvaria(String.join("\n", results));
			} else {
				cct.setAvaria("");
			}
			preencheLacres(cct, rows);
			cct.setDivergenciasIdentificadas(rows.get(0)[8] != null ? rows.get(0)[8].toString() : "");
		}
	}	

	private void preencheLacres(Cct cct, List<Object[]> rows) {
		logger.info("preencheLacres");
		List<String> lacres = new ArrayList<>();
		if (rows.get(0)[1] != null)
			lacres.add(rows.get(0)[1].toString());
		if (rows.get(0)[2] != null)
			lacres.add(rows.get(0)[2].toString());
		if (rows.get(0)[3] != null)
			lacres.add(rows.get(0)[3].toString());
		if (rows.get(0)[4] != null)
			lacres.add(rows.get(0)[4].toString());

		cct.setLacres(String.join(",", lacres));
	}

	@SuppressWarnings("unchecked")
	public void setDadosLacres(String unitGkey, Cct cct) {
		logger.info("setDadosLacres");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DADOS_LACRES);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object[]> rows = q.getResultList();
		if (rows != null && !rows.isEmpty()) {
			List<String> lacres = new ArrayList<>();
			if (rows.get(0)[0] != null)
				lacres.add(rows.get(0)[0].toString());
			if (rows.get(0)[1] != null)
				lacres.add(rows.get(0)[1].toString());
			if (rows.get(0)[2] != null)
				lacres.add(rows.get(0)[2].toString());
			if (rows.get(0)[3] != null)
				lacres.add(rows.get(0)[3].toString());

			cct.setLacres(String.join(",", lacres));
		}
	}

	public String createRecepcaoNFEEvent(BigDecimal unitGkey, String notes) {
		logger.info("createRecepcaoNFEEvent"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(EVENT, RECEPCAO_NFE_UNIT_EVENT);
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}
	
	public String createRecepcaoConteinerEvent(Cct cct, String notes) {
		logger.info("createRecepcaoConteinerEvent - CRIACAO DE EVENTO : UNIT_RECEP_TRANSBORDO, UNIT_RECEP_DUE"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
			if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())) {
				param.addParameter(EVENT, RECEPCAO_CONTEINER_TRANSBORDO_MARITIMO_UNIT_EVENT);
			} else if(CategoriaProcessoEnum.DUE.toString().equals(cct.getCategoria())) {
				param.addParameter(EVENT, RECEPCAO_CONTEINER_DUE_UNIT_EVENT);
			}
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}

	public String createDueEvents(BigDecimal unitGkey, String notes, boolean geraEventoLiberacaoEmbarque) {
		logger.info("createDueEvents - unitGkey : {} | notes : {} - CRIACAO DE EVENTO : UNIT_VINC_DUE, LIBERACAO_PARA_EMBARQUE", unitGkey, notes); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_DUE_EVENTS_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(NOTES, notes);
			param.addParameter("geraLiberacaoEmbarque", geraEventoLiberacaoEmbarque ? "1" : "0");
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.info("createDueEvent - ERROR : {} ", e.getMessage());
			return ERRO + e.getMessage();
		}
	}

	public List<Cct> validaCorrecaoPendenciasRecepcaoNFE(List<Cct> cctListRecepcaoPendente) {
		logger.info("validaCorrecaoPendenciasRecepcaoNFE");
		List<Cct> cctListRecepcaoPendenteAux = new ArrayList<>();
		ListIterator<Cct> cctIt = cctListRecepcaoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (!existePendenciaRecepcaoNFE(cct)) {
				cctListRecepcaoPendenteAux.add(cct);
			}
		}
		return cctListRecepcaoPendenteAux;
	}

	private boolean existePendenciaRecepcaoNFE(Cct cct) {
		logger.info("existePendenciaRecepcaoNFE");
		switch (cct.getStatus()) {
		case Cct.STATUS_ERRO_SEM_DANFE:
			setDanfes(cct.getIdExterno().toString(), cct);
			if (cct.getDanfes() == null || cct.getDanfes().isEmpty()) {
				logger.info("existePendenciaRecepcaoNFE - TRUE");
				return true;
			}
			break;
		case Cct.STATUS_ERRO_SEM_LACRE:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				logger.info("existePendenciaRecepcaoNFE - TRUE");
				return true;
			}
			break;
		case Cct.STATUS_ERRO_SEM_UNIT_SEAL:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				logger.info("existePendenciaRecepcaoNFE - TRUE");
				return true;
			}
			break;
		case Cct.STATUS_ERRO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				logger.info("existePendenciaRecepcaoNFE - TRUE");
				return true;
			}
			break;
		}
		logger.info("existePendenciaRecepcaoNFE - FALSE");
		return false;
	}
	
	public List<Cct> validaCorrecaoPendenciasRecepcaoDUE(List<Cct> cctListRecepcaoPendente) {
		logger.info("validaCorrecaoPendenciasRecepcaoDUE");
		List<Cct> cctListRecepcaoPendenteAux = new ArrayList<>();
		ListIterator<Cct> cctIt = cctListRecepcaoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (!existePendenciaRecepcaoDUE(cct)) {
				cctListRecepcaoPendenteAux.add(cct);
			}
		}
		return cctListRecepcaoPendenteAux;
	}

	private boolean existePendenciaRecepcaoDUE(Cct cct) {
		logger.info("existePendenciaRecepcaoDUE");
		switch (cct.getStatus()) {
		case Cct.STATUS_ERRO_SEM_LACRE:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				return true;
			}
			break;
		case Cct.STATUS_ERRO_SEM_UNIT_SEAL:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				return true;
			}
			break;
		case Cct.STATUS_ERRO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				return true;
			}
		}
		return false;
	}
	
	public void validaCorrecaoPendenciasCctTrasbordoMaritimo(List<Cct> cctListTransbordoMaritimoPendente) {
		logger.info("validaCorrecaoPendenciasCctTrasbordoMaritimo");
		ListIterator<Cct> cctIt = cctListTransbordoMaritimoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (existePendenciaCctTransbordoMaritimo(cct))
				cctIt.remove();
		}
	}

	private boolean existePendenciaCctTransbordoMaritimo(Cct cct) {
		logger.info("existePendenciaCctTransbordoMaritimo");
		switch (cct.getStatus()) {
		case Cct.STATUS_ERRO_SEM_LACRE:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				return true;
			}
			break;
		case Cct.STATUS_ERRO_SEM_UNIT_SEAL:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				return true;
			}
			break;
		case Cct.STATUS_ERRO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				return true;
			}
		}
		return false;
	}
	
	public List<Cct> validaCorrecaoPendenciasRecepcaoNFECargaSolta(List<Cct> cctListRecepcaoPendente) {
		logger.info("validaCorrecaoPendenciasRecepcaoNFECargaSolta");
		List<Cct> cctListRecepcaoPendenteAux = new ArrayList<>();
		ListIterator<Cct> cctIt = cctListRecepcaoPendente.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			if (!existePendenciaRecepcaoNFECargaSolta(cct)) {
				cctListRecepcaoPendenteAux.add(cct);
			}
		}
		return cctListRecepcaoPendenteAux;
	}
	
	
	private boolean existePendenciaRecepcaoNFECargaSolta(Cct cct) {
		logger.info("existePendenciaRecepcaoNFECargaSolta");
		switch (cct.getStatus()) {
		case Cct.STATUS_ERRO_SEM_LACRE:
			setDadosLacres(cct.getIdExterno().toString(), cct);
			if (StringUtils.isEmpty(cct.getLacres())) {
				logger.info("existePendenciaRecepcaoNFECargaSolta - TRUE");
				return true;
			}
			break;
		case Cct.STATUS_ERRO_SEM_UNIT_SEAL:
			setUnitSeal(cct.getIdExterno().toString(), cct);
			if (!cct.isUnitSeal()) {
				logger.info("existePendenciaRecepcaoNFECargaSolta - TRUE");
				return true;
			}
			break;
		case Cct.STATUS_ERRO_REGRA_PESO:
			setRegraPesagemRecepcao(cct.getIdExterno().toString(), cct);
			if (cct.isPendenciaPeso()) {
				logger.info("existePendenciaRecepcaoNFECargaSolta - TRUE");
				return true;
			}
		}
		logger.info("existePendenciaRecepcaoNFECargaSolta - FALSE");
		return false;
	}	

	public void validaVesselATAInserido(List<Cct> cctListEntregaConteiner) {
		logger.info("validaVesselATAInserido");
		ListIterator<Cct> cctIt = cctListEntregaConteiner.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			validaMudancaNavioEntrega(cct);
			if (!isVesselATAInserido(cct)) {
				cctIt.remove();
			}
		}
	}

	private void validaMudancaNavioEntrega(Cct cct) {
		logger.info("validaMudancaNavioEntrega");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOME_NAVIO_OUTBOUND_CARRIER_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		Object[] obj = null;
		try {
			obj = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		if(obj != null) {
			
			if(cct.getNavio() == null || cct.getViagem() == null) {
				cct.setNavio(obj[0].toString());
				cct.setViagem(obj[1].toString());
				cctService.save(cct);
			} else if(!cct.getNavio().equals(obj[0].toString()) || !cct.getViagem().equals(obj[1].toString())) {
				cct.setNavio(obj[0].toString());
				cct.setViagem(obj[1].toString());
				cctService.save(cct);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean isVesselATAInserido(Cct cct) {
		logger.info("isVesselATAInserido");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_VESSEL_VISIT_ACTUAL_TIME_ARRIVAL_BY_OUTBOUND_CARRIER);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		List<Object[]> rows = q.getResultList();
		return (rows != null && !rows.isEmpty() && rows.get(0) != null);
	}

	public String createEntregaEvent(Cct cct, String notes) {
		logger.info("createEntregaEvent");
		logger.info("### createEntregaEvent - CRIACAO DE EVENTO : UNIT_ENTREGA, UNIT_ENTREGA_DUE");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
			if(CategoriaProcessoEnum.NFE.toString().equals(cct.getCategoria())){
				param.addParameter(EVENT, ENTREGA_UNIT_EVENT);
			} else if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())){
				param.addParameter(EVENT, ENTREGA_TRANSBORDO_MARITIMO_UNIT_EVENT);
			}
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}

	public String getDivergenciaPesoAcima() {
		logger.info("getDivergenciaPesoAcima");
		if (divergenciaPesoAcima == null) {
			Parametros param = getParameter(DIVERGENCIA_PESO_ACIMA);
			if (param != null && param.getValor() != null && !param.getValor().isEmpty()) {
				divergenciaPesoAcima = param.getValor();
			} else {
				divergenciaPesoAcima = "0.05";
			}
		}
		return divergenciaPesoAcima;
	}

	public String getDivergenciaPesoAbaixo() {
		logger.info("getDivergenciaPesoAbaixo");
		if (divergenciaPesoAbaixo == null) {
			Parametros param = getParameter(DIVERGENCIA_PESO_ABAIXO);
			if (param != null && param.getValor() != null && !param.getValor().isEmpty()) {
				divergenciaPesoAbaixo = param.getValor();
			} else {
				divergenciaPesoAbaixo = "0.05";
			}
		}
		return divergenciaPesoAbaixo;
	}

	private Parametros getParameter(String chave) {
		logger.info("getParameter");
		return parametrosRepository.findByChave(chave);
	}

	public void processaRetornoConsultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos, List<ConsultaDadosResumidosResponseDTO> listaRetorno) {
		logger.info("processaRetornoConsultaDadosResumidos");
		boolean isValidacaoLiberacao = isValidacaoLiberacao();
		
		ListIterator<Cct> cctIt = cctListConsultaDadosResumidos.listIterator();
		while (cctIt.hasNext()) {
			Cct cct = cctIt.next();
			for (ConsultaDadosResumidosResponseDTO response : listaRetorno) {
				if (response != null && response.getNumeroDUE() != null && cct.getNrodue().contains(response.getNumeroDUE())) {
					
					boolean isAlterado = processaEventosConsultaDadosResumidos(cct, response, isValidacaoLiberacao);
										
					//cct.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
					
					logger.info("##### ATUALIZANDO STATUS DUE TABELA EXPORTADOR");
					if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
						for (Exportador exportador : cct.getExportadores()) {
							if(exportador.getNrodue() != null && response.getNumeroDUE().equals(exportador.getNrodue())) {
								if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio()) && exportador.getBloqueadoSiscomex() == 0) {
									exportador.setBloqueadoSiscomex(1);
									isAlterado = true;
								} else if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio()) && exportador.getBloqueadoSiscomex() == 1) {
									exportador.setBloqueadoSiscomex(0);
									isAlterado = true;
								}
								if(exportador.getSituacaoDUE() != Integer.parseInt(response.getSituacaoDUE())) {
									exportador.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
									isAlterado = true;
								}
								if(isAlterado) {
									exportadorRepository.save(exportador);
								}
							}
						}
					}

					if(isAlterado) {
						cctService.save(cct);
					} else {
						logger.info("processaRetornoConsultaDadosResumidos - SEM ALTERACAO - NAO SALVAR");
					}
				}
			}
		}
	}

	private boolean processaEventosConsultaDadosResumidos(Cct cct, ConsultaDadosResumidosResponseDTO response, boolean isValidacaoLiberacao) {
		logger.info("processaEventosConsultaDadosResumidos");
		boolean isAlterado = true;
		try {
			if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
				if(cct.getBloqueadoSiscomex() == null || cct.getBloqueadoSiscomex() == 0) {
					
					String unitGkey = cct.getIdExterno().toString();
					
					logger.info("processaEventosConsultaDadosResumidos - CONTAINER BLOQUEADO SISCOMEX : {} ", cct.getNumeroCont());
					cct.setBloqueadoSiscomex(1);
					ServiceParam param = new ServiceParam();
					param.addParameter(ACTION, APPLY_HOLD_SERVICE);
					param.addParameter(UNIT_GKEY, unitGkey);
					param.addParameter(HOLD, HOLD_PERMISSION_EVENT);
					param.addParameter(NOTES, "BLOQUEIO CONSULTA DADOS RESUMIDOS SISCOMEX");
					String retornoApplyHold = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
					logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO APPLY HOLD : {} ", retornoApplyHold);
					
					Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY);
					q.setParameter(UNITGKEY, unitGkey);
					Object event = null;
					try {
						event = q.getSingleResult();
					} catch(Exception e) {
						event = null;
					}
					if(event != null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(EVENT, "LIBERACAO_DE_EXPORTACAO_CANCELAR");
						param.addParameter(NOTES, "Liberado para Despacho");
						String retornoUnitEvent = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO EVENT LIBERACAO_DE_EXPORTACAO_CANCELAR : {} ", retornoUnitEvent);
					}
					
				} else {
					logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - SEM ALTERACAO");
					isAlterado = false;
				}
			}
			if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
				if(cct.getBloqueadoSiscomex() == null || cct.getBloqueadoSiscomex() == 1) {
					logger.info("### processaEventosConsultaDadosResumidos- CONTAINER DESBLOQUEADO SISCOMEX : {} - CRIACAO DE EVENTO : LIBERACAO_DE_EXPORTACAO, INCLUSAO_DOC_DESPACHO", cct.getNumeroCont());
					
					cct.setBloqueadoSiscomex(0);
					
					List<String> idsUnitEventToCreate = new ArrayList<>();
					if(naoExisteEventoInclusaoDocDespachoDestaDUE(cct)) {
						idsUnitEventToCreate.add("INCLUSAO_DOC_DESPACHO");
					}
					
					if(!isValidacaoLiberacao) {
						
						if(isTodasDUEsDoConteinerDesembaracadas(cct)) {
							
							idsUnitEventToCreate.add("LIBERACAO_DE_EXPORTACAO");
							
							logger.info("processaConsultaDadosResumidos - LIBERANDO HOLD"); 
							ServiceParam param = new ServiceParam();
							param.addParameter(ACTION, APPLY_PERMISSION_SERVICE);
							param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
							param.addParameter(PERMISSION, HOLD_PERMISSION_EVENT);
							param.addParameter(NOTES, "DESBLOQUEIO CONSULTA DADOS RESUMIDOS SISCOMEX");
							String retornoApplyPermission = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
							logger.info("processaConsultaDadosResumidos - CONTAINER DESBLOQUEADO - RETORNO APPLY PERMISSION : {} ", retornoApplyPermission);
						}
					}
					if (!idsUnitEventToCreate.isEmpty()) {
						logger.info("processaConsultaDadosResumidos - CRIAR Eventos : {} ", idsUnitEventToCreate.toString()); 
						ServiceParam param = new ServiceParam();
						param.addParameter(ACTION, CREATE_DESBLOQUEIO_EVENTS_SERVICE);
						param.addParameter(UNIT_GKEY, cct.getIdExterno().toString());
						param.addParameter(NOTES, "Inclusão documento DUE número " + cct.getNrodue());
						
						String eventos = idsUnitEventToCreate.toString().replaceAll(" ", "");
						eventos = eventos.replaceAll("[\\[\\]]", "");
						eventos = eventos.trim();
						
						logger.info("processaConsultaDadosResumidos - EVENTOS -> {} ", eventos);
						
						param.addParameter("events_to_create", eventos);
						String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("processaConsultaDadosResumidos - CREATE_DESBLOQUEIO_EVENTS = {} - RETORNO N4 {} ", idsUnitEventToCreate.toString(), retorno);
					} else {
						logger.info("processaConsultaDadosResumidos - SEM Eventos a serem criados"); 
					}
				} else {
					logger.info("processaConsultaDadosResumidos - CONTAINER DESBLOQUEADO - SEM ALTERACAO");
					isAlterado = false;
				}
			}
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("processaConsultaDadosResumidos - {} ", e.getMessage());
		}
		return isAlterado;
	}
	
	@SuppressWarnings({ "unchecked" })
	private boolean naoExisteEventoInclusaoDocDespachoDestaDUE(Cct cct) {
		logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_NOTES_EVENTO_INCLUSAO_DOC_DESPACHO_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, cct.getIdExterno());
		List<Object> lista = q.getResultList();
		for (Object note : lista) {
			if(note != null && !"".equals(note.toString()) && note.toString().contains(cct.getNrodue())) {
				logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE - ACHOU EVENTO DA DUE | NOTE : {} ", note);
				return false;
			}
		}
		logger.info("naoExisteEventoInclusaoDocDespachoDestaDUE - NAO ACHOU EVENTO DA DUE");
		return true;
	}

	private boolean isTodasDUEsDoConteinerDesembaracadas(Cct cct) {
		logger.info("isTodasDUEsDoConteinerDesembaracadas");
		if(cct.getNrodue().contains(",")) {
			logger.info("isTodasDUEsDoConteinerDesembaracadas - MAIS DE UMA DUE NO CONTEINER");
			List<Cct> cctListConsultaDadosResumidos = new ArrayList<>();
			cctListConsultaDadosResumidos.add(cct);
			ConsultaDadosResumidosResponseListDTO result = null;
			result = siscomexService.consultaDadosResumidos(cctListConsultaDadosResumidos);
			for (ConsultaDadosResumidosResponseDTO dto : result.getListaRetorno()) {
				if (Cct.CONTAINER_BLOQUEADO.toString().equals(dto.getIndicadorBloqueio()) || !isLiberadoDUE(dto.getSituacaoDUE())) {
					logger.info("isTodasDUEsDoConteinerDesembaracadas - CONTEINER TEM UMA DUE BLOQUEADA");
					return false;
				}
			}
			
		} 
		logger.info("isTodasDUEsDoConteinerDesembaracadas - TRUE");
		return true;
	}	

	private boolean isLiberadoDUE(String situacaoDUE) {
		logger.info("isLiberadoDUE");
		return ( String.valueOf(SituacaoDUEEnum.LIBERADA_SEM_CONFERENCIA_ADUANEIRA_CANAL_VERDE.value()).equals(situacaoDUE) ||
				 String.valueOf(SituacaoDUEEnum.EMBARQUE_ANTECIPADO_AUTORIZADO.value()).equals(situacaoDUE) ||
				 String.valueOf(SituacaoDUEEnum.DESEMBARACADA.value()).equals(situacaoDUE) ); 
	}
	
	@SuppressWarnings("unchecked")
	public List<String> validaEventosLiberacao(String unitGkey) {
		logger.info("validaEventosLiberacao");
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTOSLIBERACAO_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		List<Object> lista = q.getResultList();
		
		if (lista != null && !lista.isEmpty()) {
			if(lista.size() == 2) {
				return Collections.emptyList();
			}
			if(lista.size() == 1) {
				q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTOSLIBERACAO_BY_GKEY);
				q.setParameter("GKEY", lista.get(0));
				return q.getResultList();
			}
			return Arrays.asList("LIBERACAO_DE_EXPORTACAO", "INCLUSAO_DOC_DESPACHO");
		} else {
			return Arrays.asList("LIBERACAO_DE_EXPORTACAO", "INCLUSAO_DOC_DESPACHO");
		}
	}
	
	public boolean isConteinerExportacao(String unitGkey) {
		logger.info("isConteinerExportacao");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_CATEGORY_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && EXPRT.equals(o.toString()));
	}
	
	public boolean isConteinerTransbordo(String unitGkey) {
		logger.info("isConteinerTransbordo");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_CATEGORY_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && TRSHP.equals(o.toString()));
	}
	
	public boolean isPortoOrigemNacional(String unitGkey) {
		logger.info("isPortoOrigemNacional");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_POL_OPL_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] o = null;
		try {
			o = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return false;
		}
		String routingPointGkey = "";
		for (Object object : o) {
			if(object != null) {
				routingPointGkey = object.toString();
			}
		}
		q = entityManager.createNativeQuery(QueryUtil.SELECT_COUNTRY_BY_ROUTING_POINT_GKEY);
		q.setParameter(ROUTING_POINT_GKEY, routingPointGkey);
		Object obj = null;
		try {
			obj = q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		return (obj != null && BR.equals(obj.toString()));		
	}
	
	public boolean isPortoDestinoInternacional(String unitGkey) {
		logger.info("isPortoDestinoInternacional");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_POD_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object[] o = null;
		try {
			o = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return false;
		}
		String routingPointGkey = "";
		for (Object object : o) {
			if(object != null) {
				routingPointGkey = object.toString();
			}
		}
		q = entityManager.createNativeQuery(QueryUtil.SELECT_COUNTRY_BY_ROUTING_POINT_GKEY);
		q.setParameter(ROUTING_POINT_GKEY, routingPointGkey);
		Object obj = null;
		try {
			obj = q.getSingleResult();
		} catch(Exception e) {
			obj = null;
		}
		return (obj != null && !BR.equals(obj.toString()));
	}

	public boolean isVesselInBoundCarrier(String unitGkey) {
		logger.info("isVesselInBoundCarrier");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_INBOUND_CARRIER_MODE_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && VESSEL.equals(o.toString()));
	}
	
	public boolean isVesselOutBoundCarrier(String unitGkey) {
		logger.info("isVesselOutBoundCarrier");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_OUTBOUND_CARRIER_MODE_BY_UNIT_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && VESSEL.equals(o.toString()));
	}
	
	private String formataCNPJ(String cnpj) {
		logger.info("formataCNPJ");
		cnpj = CharMatcher.inRange('0', '9').retainFrom(cnpj);
		try {
			MaskFormatter mask = null;
			mask = new MaskFormatter(CNPJ_PATTERN);
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(cnpj);
		} catch (ParseException e) {
			return null;
		}
	}
	
	
	/** CLIENTE FINANCEIRO PAGADOR */
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getBooking(String nbr, String lineOperatorGkey) {
		logger.info("getBooking");
		if(lineOperatorGkey == null) {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_BY_NBR);
			q.setParameter("NBR", nbr);
			List<Object[]> rows = q.getResultList();
			return rows;
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_BY_NBR_LINE_OPERATOR);
			q.setParameter("NBR", nbr);
			q.setParameter("LINEGKEY", lineOperatorGkey);
			List<Object[]> rows = q.getResultList();
			return rows;
		}
	}
	
	public Object[] getLineOperatorByGkey(Long gkey) {
		logger.info("getLineOperatorByGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_LINE_OPERATOR_BY_GKEY);
		q.setParameter("GKEY", gkey);
		Object[] row = null;
		try {
			row = (Object[]) q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return row;
	}
	
	public Object getLineOpGkeyByUnitGkey(String unitGkey) {
		logger.info("getLineOpGkeyByUnitGkey");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_ARMADOR_GKEY_POR_NUMERO_CONTEINER);
		q.setParameter("UNITGKEY", unitGkey);
		Object row = null;
		try {
			row = (Object) q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return row;
	}
	
	public String getDeadLineByBookingNbr(String bookingNbr, String lineOperatorGkey) {
		logger.info("getDeadLineByBookingNbr");
		if(lineOperatorGkey == null) {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DEADLINE_BY_BOOKING_NBR);
			q.setParameter("NBR", bookingNbr);
			Object deadLine = null;
			try {
				deadLine = q.getSingleResult();
			} catch(Exception e) {
				return null;
			}
			if (deadLine==null)
				return null;
			
			return deadLine.toString();
		} else {
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DEADLINE_BY_BOOKING_NBR_AND_LINE_OPERATOR);
			q.setParameter("NBR", bookingNbr);
			q.setParameter("LINEGKEY", lineOperatorGkey);
			Object deadLine = null;
			try {
				deadLine = q.getSingleResult();
			} catch(Exception e) {
				return null;
			}
			if (deadLine==null)
				return null;
			
			return deadLine.toString();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAgentRepresentations(String despachante) {
		logger.info("getAgentRepresentations");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_GKEY_BY_AGENT_DOC);
		q.setParameter("DESPACHANTE", despachante);
		List<Object> agentGkeys = q.getResultList();
		if(agentGkeys == null || agentGkeys.isEmpty() || agentGkeys.size() == 0) {
			return null;
		} else {
			q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_REPRESENTATIONS_BY_AGENT_GKEY);
			q.setParameter("AGENT_GKEYS", agentGkeys);
			List<Object[]> rows = q.getResultList();
			return rows;
		}
	}

	public String createClienteFinanceiroPagadorEvents(Cct cct, Tabela cfp) {
		logger.info("createClienteFinanceiroPagadorEvents");
		try {
			ServiceParam param = null;
			String unitGkey = cct.getIdExterno().toString();
			for (Exportador exportador : cct.getExportadores()) {
				param = new ServiceParam();
				param.addParameter(ACTION, CREATE_CFP_EVENT_SERVICE);
				param.addParameter(UNIT_GKEY, unitGkey);
				param.addParameter(NOTES, "DUE/" + exportador.getNrodue());
				param.addParameter(RESPONSIBLE_PARTY, getGkeyScopedBizUnitByShipperId(String.valueOf(exportador.getNumero())));
				String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
				logger.info("createClienteFinanceiroPagadorEvents - RETORNO EVENTO CLIENTE_FINANCEIRO_PAGADOR : {} ", retorno);
			}
			
			if(isValidacaoLiberacao()) {
				boolean geraEventoLiberacaoExportacao = true;
				logger.info("#### VALIDANDO CONTEINER LIBERADO E DESBLOQUEADO");
				for (Exportador exportador : cct.getExportadores()) {
					if(exportador.getBloqueadoSiscomex() == 1 || !isLiberadoDUE(exportador.getSituacaoDUE().toString())) {
						logger.info("#### VALIDANDO CONTEINER NÃO LIBERADO OU BLOQUEADO - NAO SERA GERADO EVENTO LIBERACAO_DE_EXPORTACAO");
						geraEventoLiberacaoExportacao = false;
						break;
					}
				}
				
				Query q = entityManager.createNativeQuery(QueryUtil.SELECT_EVENTO_LIBERACAO_DE_EXPORTACAO_BY_UNIT_GKEY);
				q.setParameter(UNITGKEY, unitGkey);
				Object event = null;
				try {
					event = q.getSingleResult();
				} catch(Exception e) {
					event = null;
				}
				
				if(geraEventoLiberacaoExportacao) {
					if(event == null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_CFP_LIBERACAO_DE_EXPORTACAO_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(NOTES, "Liberado para Despacho");
						String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("createClienteFinanceiroPagadorEvents - RETORNO EVENTO LIBERACAO_DE_EXPORTACAO : {} ", retorno);
					}
				} else {
					if(event != null) { 
						param = new ServiceParam();
						param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
						param.addParameter(UNIT_GKEY, unitGkey);
						param.addParameter(EVENT, "LIBERACAO_DE_EXPORTACAO_CANCELAR");
						param.addParameter(NOTES, "Liberado para Despacho");
						String retornoUnitEvent = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
						logger.info("processaConsultaDadosResumidos - CONTAINER BLOQUEADO - RETORNO EVENT LIBERACAO_DE_EXPORTACAO_CANCELAR : {} ", retornoUnitEvent);
					}
				}
			}
						
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error(ERRO + e.getMessage());
			return Cct.STATUS_ERRO;
		}
		return Cct.STATUS_OK;
	}

	private boolean isValidacaoLiberacao() {
		logger.info("isValidacaoLiberacao");
		Parametros parametroCFP = parametrosRepository.findByChave(VALIDACAO_LIBERACAO);
		boolean isValidacaoLiberacao = false;
		
		if(parametroCFP != null && parametroCFP.getValor() != null){
			if(parametroCFP.getValor().equals("0"))
				isValidacaoLiberacao = false;
			else
				isValidacaoLiberacao = true;
		} 
		return isValidacaoLiberacao;
		
	}

	private String getGkeyScopedBizUnitByShipperId(String shipperId) {
		logger.info("getGkeyScopedBizUnitByShipperId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", shipperId);
		Object gkey = null;
		try {
			gkey = q.getSingleResult();
		} catch(Exception e) {
			return null;
		}
		return gkey.toString();
	}

	@SuppressWarnings("unchecked")
	public boolean hasCustomRecintoNTE(Cct cct) {
		logger.info("hasCustomRecintoNTE");
		String cnpj = cct.getCnpjTransportador();
		if(cnpj != null || !"".equals(cnpj)) {
			cnpj = formataCNPJ(cnpj);
			Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CUSTOM_RECINTO_NTE_BY_CNPJ_EXPORTADOR);
			q.setParameter("CNPJ", cnpj);
			List<Object[]> result = null;
			result = q.getResultList();
			return result != null;
		}
		return false;
	}
	
	public boolean processaExportador(Cct cct) {
		logger.info("processaExportador");
		logger.info("#### PRIMEIRO CADASTRO EXPORTADORES - DADOS RESUMIDOS");
		boolean isAlterado = true;
		List<Cct> cctList = new ArrayList<>();
		cctList.add(cct);
		ConsultaDadosResumidosResponseListDTO result = null;
		result = siscomexService.consultaDadosResumidos(cctList);
		
		if(result.getListaRetorno() != null && !result.getListaRetorno().isEmpty()) {
			
			cct.setExportadores(processaExportadoresRetornoSiscomex(result.getListaRetorno(), cct));
			
			if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
				if(!isValidacaoLiberacao()) {
					logger.info("#### VERIFICANDO ATUALIZACAO SHIPPER NO BOOKING");
					atualizaBookingShipper(cct, null);
				} else {
					logger.info("#### PARAM CFP LIGADA - NAO ATUALIZAR SHIPPER NO BOOKING");
				}
			} else {
				logger.info("#### NENHUM EXPORTADOR VINCULADO NO CCT");
				isAlterado = false;
			}
		}
		return isAlterado;
	}

	private List<br.com.grupolibra.dueredexservice.model.Exportador> processaExportadoresRetornoSiscomex(List<ConsultaDadosResumidosResponseDTO> listaRetorno,Cct cct) {
		logger.info("processaExportadoresRetornoSiscomex");
		List<br.com.grupolibra.dueredexservice.model.Exportador> exportadorList = new ArrayList<>();
		for (ConsultaDadosResumidosResponseDTO response : listaRetorno) {
			logger.info("processaExportadoresRetornoSiscomex - QTD EXPORTADORES RETORNADOS SISCOMEX : {} ", response.getExportadores().size());
			for (br.com.grupolibra.dueredexservice.dto.Exportador exportadorResp : response.getExportadores()) {
				
				boolean hasExportador = false;
				if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
					for (Exportador exportadorAux : cct.getExportadores()) {
						if(exportadorResp.getNumero().equals(exportadorAux.getNumero()) ) {
							hasExportador = true;
						}
					}
				}
				
				if(!hasExportador) {
					br.com.grupolibra.dueredexservice.model.Exportador exportador = new br.com.grupolibra.dueredexservice.model.Exportador();
					exportador.setNumero(exportadorResp.getNumero());
					exportador.setTipo(exportadorResp.getTipo());
					exportador.setCct(cct);
					exportador.setNrodue(response.getNumeroDUE());
					exportador.setValidado(0);
					if (Cct.CONTAINER_BLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
						exportador.setBloqueadoSiscomex(1);
					} else if (Cct.CONTAINER_DESBLOQUEADO.toString().equals(response.getIndicadorBloqueio())) {
						exportador.setBloqueadoSiscomex(0);
					}
					exportador.setSituacaoDUE(Integer.parseInt(response.getSituacaoDUE()));
					exportadorList.add(exportador);
				}
			}
		}
		return exportadorList;
	}

	public void atualizaBookingShipper(Cct cct, String exportadorId) {
		logger.info("atualizaBookingShipper");

		if(cct.getExportadores() != null && !cct.getExportadores().isEmpty() && cct.getBooking() != null) {
			
			String shipperGkeyAtualizarBooking = "";
			
			if(exportadorId == null) {
				logger.info("atualizaBookingShipper - ATUALIZANDO PELO DADOS RESUMIDOS");
				for (br.com.grupolibra.dueredexservice.model.Exportador exportador : cct.getExportadores()) {
					
					String shipperGkey = getShipperGkeyByExportador(exportador);
		    		if(shipperGkey == null) {
		    			cct.setMensagem(Cct.EXPORTADOR_NAO_CADASTRADO + " - CNPJ: " + exportador.getNumero());
		    			cct.setStatus(Cct.STATUS_ERRO);
		    			cct.setStatusProc(Cct.STATUS_ERRO_EXPORTADOR);
		    			return;
		    		} else {
		    			shipperGkeyAtualizarBooking = shipperGkey;
		    		}
				}
			} else {
				logger.info("atualizaBookingShipper - ATUALIZANDO PELO CFP");
				String shipperGkey = getShipperGkeyByExportadorId(exportadorId);
				shipperGkeyAtualizarBooking = shipperGkey;
			}
			
        	Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BOOKING_GKEY_BY_BOOKING_NBR);
    		q.setParameter(NBR, cct.getBooking());
    		String bookingGkey = q.getResultList().get(0).toString();
    		
	    	try {	
	    		ServiceParam param = new ServiceParam();
				param.addParameter(ACTION, ATUALIZA_BOOKING_SHIPPER);
				param.addParameter("bookingGkey", bookingGkey);
				param.addParameter("shipperGkey", shipperGkeyAtualizarBooking);
				String retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
				logger.info("atualizaBookingShipper - RETORNO N4 : {} ", retorno);
			} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
				logger.error(ERRO + e.getMessage());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public String getShipperGkeyByExportador(br.com.grupolibra.dueredexservice.model.Exportador exportador) {
		logger.info("getShipperGkeyByExportador");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", exportador.getNumero());
		List<Object> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0).toString();
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getShipperGkeyByExportadorId(String exportadoId) {
		logger.info("getShipperGkeyByExportadorId");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_GKEY_BY_SHIPPER_ID);
		q.setParameter("ID", exportadoId);
		List<Object> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0).toString();
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Object[] getDespachanteDocAndNameById(String despachante) {
		logger.info("getDespachanteDocAndNameById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_AGENT_DOC_AND_NAME_BY_AGENT_ID);
		q.setParameter("DESPACHANTE", despachante);
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Object[] getShipperDocAndNameById(String shipper) {
		logger.info("getShipperDocAndNameById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SCOPED_BIZ_UNIT_NAME_AND_DOC_BY_SHIPPER_ID);
		q.setParameter("ID", shipper);
		List<Object[]> resultList = q.getResultList();
		if(resultList != null && !resultList.isEmpty()) {
			return resultList.get(0);
		} else {
			return null;
		}
	}
	
	public List<Cct> validaUnitFacilityVisitStateActive(List<Cct> cctList) {
		logger.info("validaUnitFacilityVisitStateActive");
		List<Cct> listaRetorno = new ArrayList<>();
		for (Cct cct : cctList) {
			try {
				Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_FACILITY_VISIT_STATE_BY_UNIT_GKEY);
				q.setParameter("GKEY", cct.getIdExterno());
				List<Object[]> resultList = q.getResultList();
				if(resultList != null && !resultList.isEmpty()) {
					Object[] o = resultList.get(0);
					if("1ACTIVE".equals(o[1].toString())) {
						listaRetorno.add(cct);
					} else {
						cct.setFinalizado(1);
						cct.setStatusProc(Cct.STATUS_DEVOLVIDO);
						cct.setErro("UNIT FACILITY VISIT STATE " + o[1]);
						cctService.save(cct);
					}
				} else {
					logger.info("validaUnitFacilityVisitStateActive - SEM UFV");
					cct.setFinalizado(1);
					cct.setStatusProc(Cct.STATUS_DEVOLVIDO);
					cct.setErro("SEM UNIT FACILITY VISIT NO N4");
					cctService.save(cct);
				}
			} catch(Exception e) {
				logger.info("validaUnitFacilityVisitStateActive ERRO : {}", e.getMessage());
			}
		}
		return listaRetorno;
	}

	public boolean existeCadastroShipper(String cnpj) {
		logger.info("existeCadastroShipper");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_SHIPPER_BY_CNPJ);
		q.setParameter("CNPJ", cnpj);
		Object result = null;
		try {
			result = q.getSingleResult();
			if (result != null) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public Object[] geraBillOfLadingDoDocumento(Documento dto) {
		logger.info("geraBillOfLadingDoDocumento");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, "geraBillOfLadingDoDocumento");
			addBLParamsToServiceParam(dto, param);
			Object[] retorno = n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, Object[].class, LOG_N4);
			logger.info("geraBillOfLadingDoDocumento - RETORNO N4 : {} ", retorno);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.error("geraBillOfLadingDoDocumento - " + e.getMessage());
			return new Object[] { true, e.getMessage() };
		}		
	}

	private ServiceParam addBLParamsToServiceParam(Documento dto, ServiceParam param) {
		param.addParameter("blNbr", dto.getBlNbr());
		param.addParameter("booking", dto.getBooking());
		param.addParameter("blNotes", dto.getBlNotes());
		param.addParameter("blShipper", dto.getEmitente().getCnpj());
		if (dto.getEmitente().isNovo()) {
			param.addParameter("novoEmitente", "sim");
			param.addParameter("emitente.razaosocial", dto.getEmitente().getRazaoSocial());
			param.addParameter("emitente.nomecontato", dto.getEmitente().getNomeContato());
			param.addParameter("emitente.telefone", dto.getEmitente().getTelefone());
			param.addParameter("emitente.email", dto.getEmitente().getEmail());
		}
		if (dto.getConsolidador() != null && dto.getConsolidador().getCnpj() != null && !dto.getConsolidador().getCnpj().isEmpty()) {
			param.addParameter("blConsignee", dto.getConsolidador().getCnpj());
			param.addParameter("blCustomDFFBkNF", dto.getBooking());
			if (dto.getConsolidador().isNovo()) {
				param.addParameter("novoConsolidador", "sim");
				param.addParameter("consolidador.razaosocial", dto.getConsolidador().getRazaoSocial());
				param.addParameter("consolidador.nomecontato", dto.getConsolidador().getNomeContato());
				param.addParameter("consolidador.telefone", dto.getConsolidador().getTelefone());
				param.addParameter("consolidador.email", dto.getConsolidador().getEmail());
			}
		}
		param.addParameter("blitemNbr", "1");
		param.addParameter("blitemCommodity", dto.getItens().get(0).getNcmId());
		param.addParameter("blitemQuantity", dto.getItens().get(0).getQuantidade());
		param.addParameter("blitemPackageType", dto.getItens().get(0).getEmbalagemId());
		param.addParameter("blitemWeightTotalKg", dto.getItens().get(0).getPeso());
		param.addParameter("blitemMarksAndNumbers", dto.getItens().get(0).getMarcasNumeros());
		param.addParameter("blitemNotes", (dto.getItens().get(0).getInfoAdicional() != null && dto.getItens().get(0).getInfoAdicional().length() >= 245) ? dto.getItens().get(0).getInfoAdicional().substring(0,  245) + " ..." : dto.getItens().get(0).getInfoAdicional());

		return param;
	}

	public String getBLGkeyByBlNbr(String blNbr) {
		logger.info("getBLGkeyByBlNbr");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_BL_GKEY_BY_BL_NBR);
		q.setParameter("NBR", blNbr);
		List<Object> result = q.getResultList();
		if (result != null && !result.isEmpty()) {
			return result.get(0)!=null ? result.get(0).toString() : null;
		} else {
			return null;
		}
	}

	public String createBLEvent(String blGkey, String event, String notes) {
		logger.info("createBLEvent");
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "createBLEvent");
			param.addParameter("blGkey", blGkey);
			param.addParameter("event", event);
			param.addParameter("notes", notes);
			String retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
			logger.info("createBLEvent - RETORNO = {} ", retorno);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.info("createBLEvent - ERROR : {} ", e.getMessage());
			return "ERRO - " + e.getMessage();
		}
	}

	public List<Object[]> buscaEmbalagens() {
		logger.info("buscaEmbalagens");
		
		StringBuilder sql = new StringBuilder(QueryUtil.SELECT_PACKAGE_TYPES);
		sql.append("WHERE UCC_CODE IS NOT NULL ORDER BY UCC_CODE");

		Query q = entityManager.createNativeQuery(sql.toString());
		
		List<Object[]> resultList = q.getResultList();
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		} else {
			return null;
		}
	}


	public List<Object[]> buscaCommodities() {
		logger.info("buscaCommodities");
		
		StringBuilder sql = new StringBuilder(QueryUtil.SELECT_COMMODITIES);
		sql.append("ORDER BY SHORT_NAME");
		Query q = entityManager.createNativeQuery(sql.toString());
		
		List<Object[]> resultList = q.getResultList();
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		} else {
			return null;
		}
	}
	
	
	public Object[] getPackageTypesById(String id) {
		logger.info("getPackageTypesById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_PACKAGE_TYPES_BY_ID);
		q.setParameter("ID", id);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}
	
	public Object[] getCommoditiesById(String id) {
		logger.info("getCommoditiesById");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_COMMODITIES_BY_ID);
		q.setParameter("ID", id);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}
	
	public MotoristaReciboDTO getMotoristaByDocumento(String searchDoc, boolean isEstrangeiro) {
		logger.info("getMotoristaByDocumento");
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_MOTORISTA);
		q.setParameter("SEARCHDOC", searchDoc);
		
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				Object[] o = resultList.get(0);
				if(o[3] != null && "BANNED".equals(o[3].toString())) {
					retorno.setErro(true);
					retorno.setMensagem("Motorista não autorizado");
				} else {
					Transporte t = new Transporte();
					t.setNome(o[0] != null ? o[0].toString() : "");
					if(isEstrangeiro) {
						t.setPassaporte(o[2] != null ? o[2].toString() : "");
					} else {
						t.setCpf(o[2] != null ? o[2].toString() : "");
						t.setCnh(o[1] != null ? o[1].toString() : "");
					}
					retorno.setErro(false);
					retorno.setMensagem(null);
					retorno.setTransporte(t);
				}
			} else {
				retorno.setErro(false);
				retorno.setMensagem("Motorista não cadastrado, por favor, informe os dados para cadastro.");
			}
		} catch (Exception e) {
			retorno.setErro(true);
			retorno.setMensagem("Ocorreu um erro ao buscar motorista, por favor, tente novamente se o problema persistir entre em contato com o administrador do sistema.");
		}
		return retorno;
	}
	
	public boolean existeMotoristaByDocumento(String searchDoc) {
		
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();
		
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_MOTORISTA);
		q.setParameter("SEARCHDOC", searchDoc);
		
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				logger.info("existeMotoristaByDocumento - TRUE");
				return true;
			} else {
				logger.info("existeMotoristaByDocumento - FALSE");
				return false;
			}
		} catch (Exception e) {
			logger.info("existeMotoristaByDocumento - ERRO FALSE");
			return false;
		}
	}

	public String salvaNovoMotorista(Transporte transporte) {
		logger.info("salvaNovoMotorista - PARAMS - Nome: {} - CPF: {} - CNH: {} - PASSAPORTE: {}", transporte.getNome(), transporte.getCpf(), transporte.getCnh(), transporte.getPassaporte());
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter("action", "createNewDriver");
			param.addParameter("nome", transporte.getNome());
			param.addParameter("cpf", transporte.getCpf());
			param.addParameter("cnh", transporte.getCnh());
			param.addParameter("passaporte", transporte.getPassaporte());
			
			String retorno = n4ServiceInvoke.executeToObject(DUE_REDEX_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
			logger.info("salvaNovoMotorista - RETORNO = {} ", retorno);
			return retorno;
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			logger.info("salvaNovoMotorista - ERROR : {} ", e.getMessage());
			return "ERRO - " + e.getMessage();
		}
	}

	public boolean docsHasUnitInGate(List<Long> idsDocumento) {
		logger.info("docHasUnitInGate");
		if (idsDocumento.size()==0) return false;
		
		String query = QueryUtil.SELECT_DOCUMENTOS_SEM_UNIT_IN_GATE_EVENT;
		query += " AND D.ID_DOCUMENTO in ( ";
		for (Long id : idsDocumento) {
			query += " " + id + ",";
		}
		query = query.substring(0, query.length()-1) + " )";
		
		Query q = entityManager.createNativeQuery(query);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public List<Documento> getDocumentosSemUnitInGateEvent() {
		logger.info("getDocumentosSemUnitInGateEvent");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DOCUMENTOS_SEM_UNIT_IN_GATE_EVENT);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				List<Documento> retorno = new ArrayList<>();
				for (Object[] o : resultList) {
					
					Optional<Shipper> emitente = shipperRepository.findById(Long.valueOf(o[8].toString()));
					if(emitente.isPresent()) {
						o[8] = emitente.get().getRazaoSocial();
					}
					
					Documento d = new Documento(o);
					retorno.add(d);
				}
				return retorno;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}
	
	public boolean docHasUnitInGate(String blGkey) {
		logger.info("docHasUnitInGate");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_CARGO_LOT_UNIT_IN_GATE_EVENT);
		q.setParameter("BLGKEY", blGkey);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public String getNumeroLoteByBLGkey(String blGkey) {
		logger.info("docHasUnitInGate");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_LOT_ID_BY_BL_GKEY);
		q.setParameter("BLGKEY", blGkey);
		List<Object> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0) != null ? resultList.get(0).toString() : "";
			}
		} catch (Exception e) {
			return "";
		}
		return "";
	}
	
	
	public boolean isConteinerFCL(String unitGkey) {
		logger.info("isConteinerFCL");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_FREIGHT_KIND_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && FCL.equals(o.toString()));
	}
	
	public boolean isConteinerBBK(String unitGkey) {
		logger.info("isConteinerBBK");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_UNIT_FREIGHT_KIND_BY_GKEY);
		q.setParameter(UNITGKEY, unitGkey);
		Object o = null;
		try {
			o = q.getSingleResult();
		} catch(Exception e) {
			o = null;
		}
		return (o != null && BBK.equals(o.toString()));
	}
	
	public String getGkeyByConteiner(String unitId) {
		logger.info("getGkeyByConteiner");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_GKEY_BY_CONTEINER);
		q.setParameter("UNITID", unitId);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0) != null ? resultList.get(0).toString() : "";
			}
		} catch (Exception e) {
			return "";
		}
		return "";
	}
	
	public String createUnitVincDatEvent(BigDecimal unitGkey, String notes) {
		logger.info("### createUnitVincDatEvent - CRIACAO DE EVENTO : UNIT_VINC_DAT"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(EVENT, CREATE_UNIT_VINC_DAT_EVENT);
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}
	
	public String createUnitLiberacaoEmbarqueEvent(BigDecimal unitGkey, String notes) {
		logger.info("### createUnitLiberacaoEmbarqueEvent - CRIACAO DE EVENTO : LIBERACAO_PARA_EMBARQUE"); 
		try {
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, unitGkey.toString());
			param.addParameter(EVENT, CREATE_UNIT_LIBERACAO_PARA_EMBARQUE_EVENT);
			param.addParameter(NOTES, notes);
			return n4ServiceInvoke.executeToObject(DUE_GROOVY_PROXY_NAME, param, String.class, LOG_N4);
		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
			return ERRO + e.getMessage();
		}
	}
	
	public String getDestinationSite(String unitGkey){
		logger.info("getDestinationSite");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_DESTINATION_SITE_BY_GKEY);
		q.setParameter("UNITGKEY", unitGkey);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0) != null ? resultList.get(0).toString() : "";
			}
		} catch (Exception e) {	
			return "";
		}
		return "";
	}

	// 1 - Com Flag | 0 - Sem Flag
	public String getRecintoNTE(String destinationSite){
		logger.info("getDestinationSite");
		Query q = entityManager.createNativeQuery(QueryUtil.SELECT_RECINTO_NTE_BY_DESTINATION_SITE);
		q.setParameter("GKEY", destinationSite);
		List<Object[]> resultList = null;
		try {
			resultList = q.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0) != null ? resultList.get(0).toString() : "";
			}
		} catch (Exception e) {
			return "";
		}
		return "";
	}
	
	
}
