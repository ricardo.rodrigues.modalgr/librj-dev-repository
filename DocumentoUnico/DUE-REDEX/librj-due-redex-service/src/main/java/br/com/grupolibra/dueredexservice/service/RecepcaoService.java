package br.com.grupolibra.dueredexservice.service;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesConteineres;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;

public interface RecepcaoService {
	
	public RecepcoesNFE getXMLRecepcaoNFE(Cct cct);

	public RecepcoesDocumentoCarga getXMLRecepcaoDUE(Cct cct);

	public RecepcoesDocumentoCarga getXMLRecepcaoDANFECargaSolta(Cct cct);

	RecepcoesNFE getXMLRecepcaoNFECargaSolta(Cct cct);

	
}
