package br.com.grupolibra.dueredexservice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.swing.text.MaskFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.CharMatcher;
import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueredexservice.dto.ClienteFinanceiroPagadorDTO;
import br.com.grupolibra.dueredexservice.dto.ClienteFinanceiroPagadorDTO.Tabela;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Exportador;
import br.com.grupolibra.dueredexservice.model.Parametros;
import br.com.grupolibra.dueredexservice.model.QCct;
import br.com.grupolibra.dueredexservice.model.QExportador;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.ExportadorRepository;
import br.com.grupolibra.dueredexservice.repository.ParametrosRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.ClienteFinanceiroPagadorService;
import br.com.grupolibra.dueredexservice.service.EmailService;
import br.com.grupolibra.dueredexservice.util.CpfCnpjRg;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Service
public class ClienteFinanceiroPagadorServiceImpl implements ClienteFinanceiroPagadorService {

	private static final String CPF_PATTERN = "###.###.###-##";
	private static final String CNPJ_PATTERN = "##.###.###/####-##";
	private static final String CPF = "CPF";
	private static final String CNPJ = "CNPJ";
	private static final String VALIDACAO_LIBERACAO = "VALIDACAO_LIBERACAO";
	private static final String BD_N4_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private N4Repository n4Repository;

	@Autowired
	private CCTRepository cctRepository;

	@Autowired
	private ParametrosRepository parametrosRepository;
	
	@Autowired
	private ExportadorRepository exportadorRepository;
	
	@Autowired
	private EmailService emailService; 

	@Override
	public ClienteFinanceiroPagadorDTO salvaListaCFP(ClienteFinanceiroPagadorDTO dto) {
		logger.info("salvaListaCFP");
		if(dto.getListaTabela() == null) {
			ClienteFinanceiroPagadorDTO retorno = new ClienteFinanceiroPagadorDTO();
			retorno.setErro(true);
			retorno.setMensagem("Sem dados para salvar.");
			return retorno;
		}
		for (ClienteFinanceiroPagadorDTO.Tabela cfp : dto.getListaTabela()) {
//			if (cfp.isAlterado()) {
			if(cfp.getIdExportadorSel() != null) {
				logger.info("salvaListaCFP - AVALIANDO Alterado - DUE {} - CONTEINER {} ", cfp.getDue(), cfp.getConteiner());
				cfp.setConfirmado(1);
				List<Cct> cctList = cctRepository.findByNumeroCont(cfp.getConteiner());
				if(cctList != null && !cctList.isEmpty()) {
					logger.info("salvaListaCFP - ACHOU CCT");
					Cct cct = cctList.get(0);
					if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
						logger.info("salvaListaCFP - AVALIANDO EXPORTADORES DO CCT");
						int countNaoValidado = 0;
						if(cct.getNrodue().contains(",")) {
							logger.info("salvaListaCFP - MAIS DE UMA DUE NO CONTEINER");
							
							Optional<Exportador> exportadorOpt = getExportador(cfp.getIdExportadorSel(), cct, cfp.getDue());
							if(exportadorOpt.isPresent()) {
								logger.info("salvaListaCFP - IS PRESENT");
								Exportador exportador = exportadorOpt.get();
								exportador.setValidado(1);
								exportador.setNumero(String.valueOf(cfp.getExportadorSel().getId()));
								logger.info("salvaListaCFP - SALVANDO EXPORTADOR VALIDADO");
								exportadorRepository.save(exportador);
							}
							
							for (Exportador exp : cct.getExportadores()) {
								if(exp.getValidado() == 0) {
									countNaoValidado++;
								}
							}
						} else {
							logger.info("salvaListaCFP - APENAS UMA DUE NO CONTEINER");
							Exportador exportador = cct.getExportadores().get(0);
							exportador.setValidado(1);
							exportador.setNumero(String.valueOf(cfp.getExportadorSel().getId()));
							logger.info("salvaListaCFP - SALVANDO EXPORTADOR VALIDADO");
							exportadorRepository.save(exportador);
						}
						if(countNaoValidado == 0) {
							logger.info("salvaListaCFP - TODOS EXPORTADORES VALIDADOS");
							logger.info("salvaListaCFP - CRIANDO EVENTOS");
							String retorno = n4Repository.createClienteFinanceiroPagadorEvents(cct, cfp);
							logger.info("salvaListaCFP - RETORNO CRIANDO EVENTOS : {} ", retorno);
							
							logger.info("salvaListaCFP - ATUALIZANDO EXPORTADOR NO BOOKING");
							n4Repository.atualizaBookingShipper(cct, cfp.getExportadorSel().getId());
							logger.info("salvaListaCFP - RETORNO ATUALIZANDO EXPORTADOR NO BOOKING : {} ", retorno);
							
							logger.info("salvaListaCFP - ENVIANDO EMAIL");
							String retornoEmail = sendEmailCFP(cct, dto);
							logger.info("salvaListaCFP - RETORNO sendEmailCFP : {} ", retornoEmail);
							
							if(Cct.STATUS_OK.equals(retorno)) {
								logger.info("salvaListaCFP - EVENTOS CRIADOS COM SUCESSO");
								dto.setMensagem(Cct.MSG_VALIDACAO_EXPORTADORES_CFP_SUCESSO);
								dto.setErro(false);
							} else {
								logger.info("salvaListaCFP - ERRO NA CRIACAO DE EVENTOS");
								dto.setMensagem(Cct.ERRO_VALIDACAO_EXPORTADORES_CFP);
								dto.setErro(true);
							}
						} else {
							logger.info("salvaListaCFP - POSSUI EXPORTADORES PENDENTES DE VALIDACAO");
							dto.setMensagem(Cct.MSG_VALIDACAO_EXPORTADORES_CFP_INCOMPLETA);
							dto.setErro(false);
						}
						
						//List<Object[]> rows = n4Repository.getBooking(dto.getBooking(), dto.getLineOperator().getGkey());
						//getDadosDUEByBooking(rows, dto, dto.getDespachante());
						
					} else {
						logger.info("salvaListaCFP - ERRO - CCT SEM EXPORTADORES");
						dto.setMensagem("Erro ao buscar exportadores da carga na base CCT, por favor, contate o administrador do sistema.");
						dto.setErro(true);
					}
				} else {
					logger.info("salvaListaCFP - ERRO - CCT NAO ENCONTRADA");
					dto.setMensagem("Erro ao buscar registro na base CCT, por favor, contate o administrador do sistema.");
					dto.setErro(true);
				}
			} else {
				logger.info("salvaListaCFP - REGISTRO NAO REPRESENTA - DUE {} ", cfp.getDue());
			}
		}
		List<Object[]> rows = n4Repository.getBooking(dto.getBooking(), dto.getLineOperator().getGkey());
		
		getDadosDUEByBooking(rows, dto, dto.getDespachante());
		
		return dto;
	}

	private Optional<Exportador> getExportador(String idExportadorSel, Cct cct, String due) {
//		logger.info("getExportador");
		QExportador exp = QExportador.exportador;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(exp.numero.eq(idExportadorSel));
		builder.and(exp.cct.id.eq(cct.getId()));
		builder.and(exp.nrodue.eq(due));
//		logger.info("getExportador - QUERY : {} ", builder.toString());
		return exportadorRepository.findOne(builder.getValue());
	}

	private String sendEmailCFP(Cct cct, ClienteFinanceiroPagadorDTO dto) {
//		logger.info("sendEmailCFP");
		
		String retorno = "";
		String to = PropertiesUtil.getProp().getProperty("mail.to");
		String subject = PropertiesUtil.getProp().getProperty("mail.subject");
		
		StringBuilder sb = new StringBuilder();
		Object[] despachante = n4Repository.getDespachanteDocAndNameById(dto.getDespachante());
		sb.append("Despachante : " + despachante[0] + " - " + despachante[1]);
		sb.append("<br>");
		sb.append("Booking : " + dto.getBooking()); 
		sb.append("<br>");
		sb.append("Contêiner : " + cct.getNumeroCont());
		sb.append("<br>");
		for (Exportador exportador : cct.getExportadores()) {
			sb.append("DU-E : " + exportador.getNrodue());
			sb.append("<br>");
			Object[] exp = n4Repository.getShipperDocAndNameById(exportador.getNumero());
			sb.append("Exportador : " + exp[0] + " - " + exp[1]);
			sb.append("<br>");
		}
		sb.append("DtHr alteração : " + new Date());
		sb.append("<br>");
		sb.append("Usuário : " + despachante[1]);
		sb.append("<br>");
		
		String mensagem = sb.toString();

		logger.info("sendEmailCFP - PARA {} ", to);
		logger.info("sendEmailCFP - ASSUNTO {} ", subject);
		logger.info("sendEmailCFP - MENSAGEM {} ", mensagem);
		
		retorno = emailService.sendSimpleMessage(to, subject, mensagem);
		
		logger.info("sendEmailCFP - RETORNO {} ", retorno);
		
		return retorno;
	}

	@Override
	public int validaCheckObrigatorio() {
//		logger.info("validaCheckObrigatorio");
		Parametros param = parametrosRepository.findByChave(VALIDACAO_LIBERACAO);
		return Integer.parseInt(param.getValor());
	}

	@Override
	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(String bookingNbr, String lineOperatorGkey, String despachante) {
//		logger.info("getDadosClienteFinanceiroPagador");
//		logger.info("### PARAMS ###");
//		logger.info("### bookingNbr : {} ", bookingNbr);
//		logger.info("### lineOperatorGkey : {} ", lineOperatorGkey);
//		logger.info("### despachante : {} ", despachante);

		
		ClienteFinanceiroPagadorDTO dto = new ClienteFinanceiroPagadorDTO();
		dto.setBooking(bookingNbr);
		dto.setDespachante(despachante);
		
		

		if (isEmptyDespachante(despachante)) {
			dto.setMensagem("Despachante não informado, por favor, entre em contato com o administrador do sistema.");
			return dto;
		} else {
			despachante = validaDocDespachante(despachante);
			
			if (despachante == null) {
				dto.setMensagem("Documento despachante inválido, por favor, entre em contato com o administrador do sistema.");
				return dto;
			}
		}

		if (lineOperatorGkey == null || "".equals(lineOperatorGkey) || "null".equals(lineOperatorGkey)) {
			List<Object[]> rows = n4Repository.getBooking(bookingNbr, null);
			if (rows != null) {
				
				if (rows.size() == 1) {
					preencheLineOperator(dto, rows);
					if (!isValidDeadLineByBookingNbr(bookingNbr, null)) {
						dto.setMensagem("Os dados não poderão ser apresentados. A viagem já ultrapassou a data limite ou esta em branco (Deadline).");
						return dto;
					}
					getDadosDUEByBooking(rows, dto, despachante);
					if(dto.getListaTabela() == null) {
						dto.setMensagem(Cct.MSG_SEM_CONTEINER_BOOKING_LINEOP);
					} else if(isTodosExportadoresNaoRepresentadoPeloDespachante(dto)) {
						dto.setListaTabela(null);
						dto.setMensagem(Cct.MSG_EXPORTADOR_NAO_REPRESENTA);
					}
				} else if (rows.size() > 1) {
					List<ClienteFinanceiroPagadorDTO.LineOperator> lineOperatorList = new ArrayList<>();
					for (Object[] bookingObj : rows) {
						ClienteFinanceiroPagadorDTO.LineOperator lineOperator = new ClienteFinanceiroPagadorDTO.LineOperator();
						//lineOperator.setGkey(bookingObj[8].toString());
						lineOperator.setGkey(bookingObj[12].toString());
						getLabelLineOperator(lineOperator);
						lineOperatorList.add(lineOperator);
					}
					dto.setLineOperatorList(lineOperatorList);
				}
			} else {
				dto.setMensagem("Booking não encontrado.");
			}
		} else {
			if (!isValidDeadLineByBookingNbr(bookingNbr, lineOperatorGkey)) {
				dto.setMensagem("Os dados não poderão ser apresentados. A viagem já ultrapassou a data limite (Deadline).");
				return dto;
			}
			List<Object[]> rows = n4Repository.getBooking(bookingNbr, lineOperatorGkey);
			preencheLineOperator(dto, rows);
			getDadosDUEByBooking(rows, dto, despachante);
			if(dto.getListaTabela() == null) {
				dto.setMensagem(Cct.MSG_SEM_CONTEINER_BOOKING_LINEOP);
			} else if(isTodosExportadoresNaoRepresentadoPeloDespachante(dto)) {
				dto.setListaTabela(null);
				dto.setMensagem(Cct.MSG_EXPORTADOR_NAO_REPRESENTA);
			}
		}
		return dto;
	}

	private void preencheLineOperator(ClienteFinanceiroPagadorDTO dto, List<Object[]> rows) {
//		logger.info("preencheLineOperator");
		Object[] bookingObj = rows.get(0);
		List<ClienteFinanceiroPagadorDTO.LineOperator> lineOperatorList = new ArrayList<>();
		ClienteFinanceiroPagadorDTO.LineOperator lineOperator = new ClienteFinanceiroPagadorDTO.LineOperator();
		//lineOperator.setGkey(bookingObj[8].toString());
		if (bookingObj[12]!=null)
		{
			lineOperator.setGkey(bookingObj[12].toString());
			getLabelLineOperator(lineOperator);
			lineOperatorList.add(lineOperator);
			dto.setLineOperatorList(lineOperatorList);
		}
	}

	private boolean isTodosExportadoresNaoRepresentadoPeloDespachante(ClienteFinanceiroPagadorDTO dto) {
//		logger.info("isTodosExportadoresNaoRepresentadoPeloDespachante");
		boolean todosNaoRepresenta = true;
		for (ClienteFinanceiroPagadorDTO.Tabela row : dto.getListaTabela()) {
			if(row.getStatus() == null || Cct.STATUS_EXPORTADOR_INEXISTENTE.toString().equals(row.getStatus()) || Cct.STATUS_EXPORTADOR_VENCIDO.equals(row.getStatus())) {
				todosNaoRepresenta = false;
			}
		}
		return todosNaoRepresenta;
	}

	private void getDadosDUEByBooking(List<Object[]> rows, ClienteFinanceiroPagadorDTO dto, String despachante) {
//		logger.info("getDadosDUEByBooking");
		Object[] bookingObj = rows.get(0);
		ClienteFinanceiroPagadorDTO.LineOperator lineOperator = new ClienteFinanceiroPagadorDTO.LineOperator();
		//lineOperator.setGkey(bookingObj[8].toString());
		if (bookingObj[12]==null)
			return;
		
		lineOperator.setGkey(bookingObj[12].toString());
		getLabelLineOperator(lineOperator);
		dto.setLineOperator(lineOperator);
		
		//List<Cct> cctList = findAllByBooking(bookingObj[2].toString(), lineOperator.getGkey());
		List<Cct> cctList = findAllByBooking(bookingObj[0].toString(), lineOperator.getGkey());
		if(cctList == null) {
			dto.setListaTabela(null);
			return;
		}
		
		List<ClienteFinanceiroPagadorDTO.Tabela> tabelaList = new ArrayList<>();
		List<Object[]> agentRepresentations = n4Repository.getAgentRepresentations(despachante);
		for (Cct cct : cctList) {
			if (cct.getNrodue() != null) {
				for (Exportador exportador : cct.getExportadores()) {
					ClienteFinanceiroPagadorDTO.Tabela row = new ClienteFinanceiroPagadorDTO.Tabela();
					row.setIdExportadorCct(exportador.getId());
					row.setConfirmado(0);
					row.setConteiner(cct.getNumeroCont());
					row.setIdExterno(cct.getIdExterno().toString());
					row.setDue(exportador.getNrodue());
					row.setAlterado(false);
					if (exportador.getValidado() == 1) {
						row.setMensagem("Alterado");
						row.setConfirmado(1);
					}
					if(exportadorNaoCadastradoN4(exportador)) {
						row.setStatus(Cct.STATUS_EXPORTADOR_INEXISTENTE);
						row.setExportadores(new ArrayList<>());
					} else {
						boolean representa = getAgentRepresentations(despachante, exportador, row, agentRepresentations);
						if (!representa) {
							row.setStatus(Cct.STATUS_EXPORTADOR_NAO_REPRESENTA);
						}
					}
					tabelaList.add(row);
				}
			}
		}
		dto.setListaTabela(tabelaList);
	}

	private List<Cct> findAllByBooking(String booking, String lineOpGkey) {
//		logger.info("findAllByBooking");
		QCct qcct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(qcct.booking.eq(booking));
		builder.andAnyOf(qcct.finalizado.isNull(), qcct.finalizado.ne(1));
//		logger.info("findAllByBooking - QUERY : {} ", builder.toString());
		List<Cct> resultListAux = cctRepository.findAll(builder.getValue());
		List<Cct> resultList = new ArrayList<Cct>();
//		logger.info("findAllByBooking - VALIDANDO MESMO LINE OPERATOR");
		for (Cct cct : resultListAux) {
			
//			logger.info("findAllByBooking - LINE OPERATOR = " + lineOpGkey);
			
			Object unitLineOpGkey = n4Repository.getLineOpGkeyByUnitGkey(cct.getIdExterno().toString());
			if(unitLineOpGkey != null) {
				
//				logger.info("findAllByBooking - UNIT LINE OPERATOR = " + unitLineOpGkey);
				
				if(lineOpGkey.equals(unitLineOpGkey.toString())) {
					resultList.add(cct);
				}
			}
		}
		if(resultList.isEmpty()) {
			return null;
		} else {
			return resultList;
		}
	}

	private boolean exportadorNaoCadastradoN4(Exportador exportador) {
//		logger.info("exportadorNaoCadastradoN4");
		if(n4Repository.getShipperGkeyByExportador(exportador) == null) {
			return true;
		} else {
			return false;
		}
	}

	private boolean getAgentRepresentations(String despachante, Exportador exportador, Tabela row, List<Object[]> agentRepresentations) {
//		logger.info("getAgentRepresentations");
		if (agentRepresentations != null) {
			List<ClienteFinanceiroPagadorDTO.Exportador> exportadorList = new ArrayList<>();
			Date hoje = new Date();
			boolean representa = false;
			for (Object[] representation : agentRepresentations) {
				Date startDate = (Date) representation[3];
				Date endDate = (Date) representation[4];
				
				if (exportador.getNumero().equals(representation[1].toString())) {
					representa = true;
					if (endDate == null || hoje.after(startDate) && hoje.before(endDate)) {
						ClienteFinanceiroPagadorDTO.Exportador exp = new ClienteFinanceiroPagadorDTO.Exportador();
						exp.setId(representation[1].toString());
						exp.setDoc(representation[2].toString());
						exp.setLabel(representation[2].toString() + " - " + representation[0].toString());
						exp.setSiscomex(true);
						exportadorList.add(exp);
					} else {
						row.setStatus(Cct.STATUS_EXPORTADOR_VENCIDO);
					}
				} else {
					if (endDate == null || hoje.after(startDate) && hoje.before(endDate)) {
						ClienteFinanceiroPagadorDTO.Exportador exp = new ClienteFinanceiroPagadorDTO.Exportador();
						exp.setId(representation[1].toString());
						exp.setDoc(representation[2].toString());
						exp.setLabel(representation[2].toString() + " - " + representation[0].toString());
						exp.setSiscomex(false);
						exportadorList.add(exp);
					}
				}
			}
			row.setExportadores(exportadorList);
			return representa;
		} else {
			return false;
		}
	}

	private boolean isValidDeadLineByBookingNbr(String bookingNbr, String lineOperatorGkey) {
//		logger.info("isValidDeadLineByBookingNbr");
		try {
			String data = "";
			if (lineOperatorGkey == null || "".equals(lineOperatorGkey)) {
				data = n4Repository.getDeadLineByBookingNbr(bookingNbr, null);
			} else {
				data = n4Repository.getDeadLineByBookingNbr(bookingNbr, lineOperatorGkey);
			}
			if (data==null)
				return false;
			
			Date deadLine = new SimpleDateFormat(BD_N4_DATE_FORMAT).parse(data);

//			logger.info("### DEADLINE N4 : {} ", deadLine);
//			logger.info("### IF : {} ", deadLine.after(new Date()));

			return deadLine.after(new Date());
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	private String validaDocDespachante(String despachante) {
//		logger.info("validaDocDespachante");
		if (CpfCnpjRg.isCNPJ(despachante)) {
			return formataCpfDespachante(despachante, CNPJ);
		}
		if (CpfCnpjRg.isCPF(despachante)) {
			return formataCpfDespachante(despachante, CPF);
		}
		return null;
	}

	private void getLabelLineOperator(ClienteFinanceiroPagadorDTO.LineOperator lineOperator) {
//		logger.info("getLabelLineOperator");
		Object[] lineOperatorN4 = n4Repository.getLineOperatorByGkey(Long.valueOf(lineOperator.getGkey()));
		String label = lineOperatorN4[1].toString() + " - " + lineOperatorN4[3].toString();
		lineOperator.setLabel(label);
	}

	private String formataCpfDespachante(String despachante, String tipo) {
//		logger.info("formataCpfDespachante");
		despachante = CharMatcher.inRange('0', '9').retainFrom(despachante);
		try {
			MaskFormatter mask = null;
			if (CPF.equals(tipo)) {
				mask = new MaskFormatter(CPF_PATTERN);
			}
			if (CNPJ.equals(tipo)) {
				mask = new MaskFormatter(CNPJ_PATTERN);
			}
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(despachante);
		} catch (ParseException e) {
			return null;
		}
	}

	private boolean isEmptyDespachante(String despachante) {
//		logger.info("isEmptyDespachante");
		return (despachante == null || "".equals(despachante) || "null".equals(despachante) || "undefined".equals(despachante));
	}
}
