package br.com.grupolibra.dueredexservice.util;

import java.util.HashMap;
import java.util.Map;

public enum SituacaoDocTransEnum {

	MANIFESTADO(1, "Manifestado"), 
	TRANSITO_AUTORIZADO(2, "Trânsito Autorizado"), 
	TRANSITO_INICIADO(3, "Trânsito Iniciado"),
	RECEBIDO_EM_TRANSITO(4, "Recebido em Trânsito"),
	TRANSITO_CONCLUIDO(5, "Trânsito Concluído"),
	CANCELADO(6, "Cancelado"),
	EM_ANALISE_AUTORIZACAO(7, "Em Análise Autorização"),
	EM_ANALISE_CONCLUSAO(8, "Em Análise Conclusão");

	private int value;

	private String descricao;
	
	private static Map<Integer, SituacaoDocTransEnum> map  = new HashMap<Integer, SituacaoDocTransEnum>();
	
	static {
		for(SituacaoDocTransEnum situacao : SituacaoDocTransEnum.values()) {
			map.put(situacao.value, situacao);
		}
	}

	SituacaoDocTransEnum(int value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	public int value() {
		return value;
	}

	public String descricao() {
		return descricao;
	}
	
	public static SituacaoDocTransEnum valueOf(int value) {
		return map.get(value);
	}

}
