package br.com.grupolibra.dueredexservice.dto;

import java.io.Serializable;

public class MonitoramentoRequestDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String tipo;
	
	private String pageIndex;
	
	private String pageSize;
	
	private String navio;
	
	private String visit;
	
	private String lineOperator;
	
	private String booking;
	
	private String conteiner;
	
	private String danfe;
	
	private String due;
	
	private String dataEntradaDe;
	
	private String dataEntradaAte;
	
	private String dataProcDe;
	
	private String dataProcAte;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getNavio() {
		return navio;
	}

	public void setNavio(String navio) {
		this.navio = navio;
	}

	public String getVisit() {
		return visit;
	}

	public void setVisit(String visit) {
		this.visit = visit;
	}

	public String getLineOperator() {
		return lineOperator;
	}

	public void setLineOperator(String lineOperator) {
		this.lineOperator = lineOperator;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getConteiner() {
		return conteiner;
	}

	public void setConteiner(String conteiner) {
		this.conteiner = conteiner;
	}

	public String getDanfe() {
		return danfe;
	}

	public void setDanfe(String danfe) {
		this.danfe = danfe;
	}

	public String getDue() {
		return due;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public String getDataEntradaDe() {
		return dataEntradaDe;
	}

	public void setDataEntradaDe(String dataEntradaDe) {
		this.dataEntradaDe = dataEntradaDe;
	}

	public String getDataEntradaAte() {
		return dataEntradaAte;
	}

	public void setDataEntradaAte(String dataEntradaAte) {
		this.dataEntradaAte = dataEntradaAte;
	}

	public String getDataProcDe() {
		return dataProcDe;
	}

	public void setDataProcDe(String dataProcDe) {
		this.dataProcDe = dataProcDe;
	}

	public String getDataProcAte() {
		return dataProcAte;
	}

	public void setDataProcAte(String dataProcAte) {
		this.dataProcAte = dataProcAte;
	}
	
	

}
