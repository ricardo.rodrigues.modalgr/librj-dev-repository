package br.com.grupolibra.dueredexservice.model.siscomex;

public class Recebedor {
	
	public static final String MARITIMA = "01";
	public static final String FLUVIAL = "02";
	public static final String LACUSTRE = "03";
	public static final String AEREA = "04";
	public static final String POSTAL = "05";
	public static final String FERROVIARIA = "06";
	public static final String RODOVIARIA = "07";
	public static final String DUTO_REDE_TRANSMISSAO = "08";
	public static final String MEIOS_PROPRIOS = "09";
	public static final String VIA_FICTA = "10";
	public static final String COURRIER = "11";
	
	private String cnpj;
	
	private String cpf;
	
	private String nomeEstrangeiro;
	
	private String viaTransporte;
	
	private String baldeacaoOuTransbordo;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeEstrangeiro() {
		return nomeEstrangeiro;
	}

	public void setNomeEstrangeiro(String nomeEstrangeiro) {
		this.nomeEstrangeiro = nomeEstrangeiro;
	}

	public String getViaTransporte() {
		return viaTransporte;
	}

	public void setViaTransporte(String viaTransporte) {
		this.viaTransporte = viaTransporte;
	}

	public String getBaldeacaoOuTransbordo() {
		return baldeacaoOuTransbordo;
	}

	public void setBaldeacaoOuTransbordo(String baldeacaoOuTransbordo) {
		this.baldeacaoOuTransbordo = baldeacaoOuTransbordo;
	}
}
