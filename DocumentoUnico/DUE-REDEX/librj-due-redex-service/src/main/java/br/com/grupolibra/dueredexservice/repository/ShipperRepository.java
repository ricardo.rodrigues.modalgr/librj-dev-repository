package br.com.grupolibra.dueredexservice.repository;

import java.util.Optional;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueredexservice.model.Shipper;

@Repository
public interface ShipperRepository extends CrudRepository<Shipper, Long>, QuerydslPredicateExecutor<Shipper> {
	
	public Optional<Shipper> findByCnpj(String cnpj);
	
}