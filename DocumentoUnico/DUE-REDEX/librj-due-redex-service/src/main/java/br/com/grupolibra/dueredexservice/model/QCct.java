package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCct is a Querydsl query type for Cct
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCct extends EntityPathBase<Cct> {

    private static final long serialVersionUID = 1354446L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCct cct = new QCct("cct");

    public final QArmador armador;

    public final StringPath avaria = createString("avaria");

    public final NumberPath<Integer> bloqueadoSiscomex = createNumber("bloqueadoSiscomex", Integer.class);

    public final StringPath booking = createString("booking");

    public final StringPath categoria = createString("categoria");

    public final StringPath cnpjTransportador = createString("cnpjTransportador");

    public final NumberPath<Integer> contadorEnvio = createNumber("contadorEnvio", Integer.class);

    public final StringPath cpfCondutor = createString("cpfCondutor");

    public final StringPath cpfTransportador = createString("cpfTransportador");

    public final ListPath<Danfe, QDanfe> danfes = this.<Danfe, QDanfe>createList("danfes", Danfe.class, QDanfe.class, PathInits.DIRECT2);

    public final StringPath dataentrada = createString("dataentrada");

    public final StringPath datasaida = createString("datasaida");

    public final StringPath divergenciasIdentificadas = createString("divergenciasIdentificadas");

    public final StringPath dtProcessamento = createString("dtProcessamento");
    
    public final DateTimePath<java.util.Date> dtEntr = createDateTime("dtEntr", java.util.Date.class);
    
    public final DateTimePath<java.util.Date> dtProc = createDateTime("dtProc", java.util.Date.class);    

    public final QEntregador entregador;

    public final StringPath erro = createString("erro");

    public final ListPath<Exportador, QExportador> exportadores = this.<Exportador, QExportador>createList("exportadores", Exportador.class, QExportador.class, PathInits.DIRECT2);

    public final NumberPath<Integer> finalizado = createNumber("finalizado", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> idExterno = createNumber("idExterno", java.math.BigDecimal.class);

    public final StringPath lacres = createString("lacres");

    public final StringPath mensagem = createString("mensagem");

    public final StringPath motivoNaoPesagem = createString("motivoNaoPesagem");

    public final StringPath navio = createString("navio");

    public final StringPath nomeCondutor = createString("nomeCondutor");

    public final StringPath nomeTransportador = createString("nomeTransportador");

    public final StringPath nrodue = createString("nrodue");

    public final StringPath numeroCont = createString("numeroCont");

    public final NumberPath<java.math.BigDecimal> pesoBruto = createNumber("pesoBruto", java.math.BigDecimal.class);

    public final StringPath placareboque = createString("placareboque");

    public final StringPath placaveiculo = createString("placaveiculo");

    public final QRecebedor recebedor;

    public final StringPath status = createString("status");

    public final StringPath statusProc = createString("statusProc");

    public final NumberPath<java.math.BigDecimal> tara = createNumber("tara", java.math.BigDecimal.class);

    public final NumberPath<Integer> tipoTransportador = createNumber("tipoTransportador", Integer.class);

    public final StringPath ultimoXmlEnviado = createString("ultimoXmlEnviado");

    public final StringPath viagem = createString("viagem");
    
    public final StringPath billoflanding = createString("billoflanding");

    public QCct(String variable) {
        this(Cct.class, forVariable(variable), INITS);
    }

    public QCct(Path<? extends Cct> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCct(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCct(PathMetadata metadata, PathInits inits) {
        this(Cct.class, metadata, inits);
    }

    public QCct(Class<? extends Cct> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.armador = inits.isInitialized("armador") ? new QArmador(forProperty("armador")) : null;
        this.entregador = inits.isInitialized("entregador") ? new QEntregador(forProperty("entregador"), inits.get("entregador")) : null;
        this.recebedor = inits.isInitialized("recebedor") ? new QRecebedor(forProperty("recebedor"), inits.get("recebedor")) : null;
    }

}
