package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;

@Entity
@Table(name="CCT")
@NamedQuery(name = "Cct.findAll", query = "SELECT c FROM Cct c")
public class Cct implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String STATUS_RECEPCIONADO_OK = "Recepcionado Ok";
	public static final String STATUS_ERRO = "Erro";
	public static final String STATUS_ERRO_PARADA_PROGRAMADA = "Aguardando retorno Parada Programada";
	public static final String STATUS_ERRO_RECEPCAO = "Erro Recepção";
	public static final String STATUS_ERRO_ENTREGA = "Erro Entrega";
	public static final String STATUS_ERRO_SEM_DANFE = "Contêiner sem DANFE atrelado.";
	public static final String STATUS_ERRO_SEM_LACRE = "Contêiner sem LACRE.";
	public static final String STATUS_ERRO_SEM_UNIT_SEAL = "Contêiner sem evento de UNIT_SEAL.";
	public static final String STATUS_ERRO_EVENTO_DUE = "Erro ao gerar eventos de cadastro de DUE.";
	public static final String CONTAINER_INVALIDO_MSG = "Contêiner inválido (ISO 6346)";
	public static final String CONTAINER_INVALIDO_MSG_NOTIFICACAO = "Notificação:\nContêiner inválido (ISO 6346). O envio será realizado sem o dígito verificador do contêiner.\n\n";
	public static final String STATUS_AGUARDANDO_RECEPCAO = "Aguardando Recepção";
	public static final String FALHA_AO_LEVANTAR_PESO_TARA_MSG = "Falha ao levantar peso ou tara.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_DANFE_MSG = "Falha ao levantar dados da DANFE.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_LACRE_MSG = "Falha ao levantar dados de LACRE.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG = "Falha ao levantar dados de evento UNIT_SEAL.\n";
	public static final String FALHA_DANFES_DUPLICADAS = "DANFEs duplicadas.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_DADOS_TRANSPORTADORA_CONDUTOR_MSG = "Falha ao levantar dados da transportadora/condutor.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_NUMERO_CONTAINER_MSG = "Falha ao levantar dados do número do conteiner.\n";
	public static final String FALHA_AO_LEVANTAR_DADOS_LACRE_CONTAINER_MSG = "Falha ao levantar dados dos lacres do conteiner.\n";
	public static final String STATUS_OK = "Ok";
	public static final String STATUS_RECEPCAO_EFETUADA_SUCESSO = "Recepção efetuada com sucesso";
	public static final String ERRO_ENVIO_RECEITA = "Erro no envio para a receita.";
	public static final String CONTAINER_RECEBIDO_AGUARDANDO_ENVIO_RFB = "Contêiner recebido. Aguardando envio para RFB.";
	public static final Object CERTIFICADO_NAO_AUTORIZADO_OPERACAO = "Certificado não autorizado para esta operação.";
	public static final Object REQUISICAO_NAO_AUTORIZADA = "Requisição não autorizada.";
	public static final Object REGISTRO_NAO_ENCONTRADO = "Registro não encontrado.";
	public static final Object ERRO_NEGOCIO = "Erro de negócio.";
	public static final Object ERRO_INTERNO_SERVIDOR = "Erro interno do servidor.";
	public static final Object ERRO_TIMEOUT = "Timeout, falha ao se conectar ao siscomex.";
	public static final Object SERVICO_INDISPONIVEL = "Serviço SISCOMEX indisponível";
	public static final Object REQUISICAO_MAL_FORMATADA = "Requisição mal formatada.";
	public static final String STATUS_INDICADOR_BLOQUEIO_BLOQUEADO_MSG = "Bloqueado";
	public static final String STATUS_INDICADOR_BLOQUEIO_LIBERADO_MSG = "Liberado";
	public static final BigDecimal CONTAINER_DESBLOQUEADO = BigDecimal.valueOf(2);
	public static final BigDecimal CONTAINER_BLOQUEADO = BigDecimal.valueOf(1);
	public static final String STATUS_ENTREGUE = "Entregue Ok";
	public static final String STATUS_DEVOLVIDO = "Saida GATE";
	public static final String STATUS_FINALIZADO = "Finalizado";
	public static final String STATUS_ENTREGA_EFETUADA_SUCESSO = "Entrega efetuada com sucesso";
	public static final String FALHA_AO_LEVANTAR_DADOS_ARMADOR_MSG = "Falha da levantar dados do armador";
	public static final String MOTIVO_NAO_PESAGEM = "IOP – Impossibilidade Operacional de Pesagem";
	public static final String FALHA_VALIDACAO_REGRA_PESO = "Falha ao aplicar regra de peso (First Weighing = YES ou IsOOG = YES)";
	public static final String STATUS_ERRO_REGRA_PESO = "Conteiner com falha na regra de peso (First Weighing = YES ou IsOOG = YES)";
	
	public static final String STATUS_AGUARDANDO_DUE_TRANSBORDO_MARITIMO = "Aguardando DUE Transbordo Maritimo";
	public static final String STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO = "Aguardando envio recepcao Transbordo Maritimo";
	
	public static final String EXPORTADOR_NAO_CADASTRADO = "Exportador não cadastrado";
	public static final String STATUS_ERRO_EXPORTADOR = "Erro Exportador";
	
	public static final String STATUS_EXPORTADOR_NAO_REPRESENTA = "NAO_REPRESENTA";
	public static final String STATUS_EXPORTADOR_INEXISTENTE = "INEXISTENTE";
	public static final String STATUS_EXPORTADOR_VENCIDO = "VENCIDO";
	public static final String MSG_EXPORTADOR_NAO_REPRESENTA = "Não há nenhum Exportador representado neste booking.";
	public static final String MSG_VALIDACAO_EXPORTADORES_CFP_INCOMPLETA = "Esta unidade só será liberada quando todas as DUEs forem confirmadas.";
	public static final String MSG_VALIDACAO_EXPORTADORES_CFP_SUCESSO = "Unidade(s) liberada(s) com sucesso.";
	public static final String ERRO_VALIDACAO_EXPORTADORES_CFP = "Ocorreu um erro na liberação, por favor, tente novamente, caso o problema persista, contate o administrador do sistema.";
	public static final String MSG_SEM_CONTEINER_BOOKING_LINEOP = "Não há nenhum contêiner para este Booking / Line Operator.";
	public static final String STATUS_AGUARDANDO_DUE_ENTREGA = "Aguardando DUE para Entrega";
	public static final String STATUS_AGUARDANDO_RECEPCAO_ENTREGA = "Aguardando Recepção para Entrega";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CCT_SEQ")
	@SequenceGenerator(name = "CCT_SEQ", sequenceName = "CCT_SEQ", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ARMADOR")
	private Armador armador;

	private String avaria;

	private String booking;

	private String categoria;

	@Column(name = "STATUS_PROC")
	private String statusProc;

	private String status;

	private String mensagem;

	private String erro;

	private String dataentrada;

	private String datasaida;

	@Column(name = "DT_PROCESSAMENTO")
	private String dtProcessamento;
	
	@Column(name = "DTENTR")
	private Date dtEntr;	
	
	@Column(name = "DTSAID")
	private Date dtSaid;
	
	@Column(name = "DTPROC")
	private Date dtProc;

	@Column(name = "ID_EXTERNO")
	private BigDecimal idExterno;

	private String lacres;

	@Column(name = "MOTIVO_NAO_PESAGEM")
	private String motivoNaoPesagem;

	private String navio;

	@Column(name = "NUMERO_CONT")
	private String numeroCont;

	@Column(name = "PESO_BRUTO")
	private BigDecimal pesoBruto;

	private String placareboque;

	private String placaveiculo;

	private BigDecimal tara;

	private String viagem;

	@Column(name = "NRODUE")
	private String nrodue;

	@Column(name = "CPF_CONDUTOR")
	private String cpfCondutor;

	@Column(name = "CNPJ_TRANSPORTADOR")
	private String cnpjTransportador;

	@Column(name = "DIVERGENCIAS_IDENTIFICADAS")
	private String divergenciasIdentificadas;

	@Column(name = "CPF_TRANSPORTADOR")
	private String cpfTransportador;
	
	@Column(name = "NOME_TRANSPORTADOR")
	private String nomeTransportador;
	
	@Column(name = "NOME_CONDUTOR")
	private String nomeCondutor;
	
	/**
	 * 1 - ESTRANGEIRO 0 - NACIONAL
	 */
	@Column(name = "TRANSPORTADOR_ID")
	private Integer tipoTransportador;

	@OneToMany(mappedBy = "cct", cascade = CascadeType.MERGE, fetch = FetchType.LAZY, targetEntity = Danfe.class)
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	private List<Danfe> danfes;

	@Transient
	private boolean unitSeal;

	@Transient
	private boolean firstWeighing;

	@Transient
	private boolean oog;

	@Transient
	private boolean pendenciaPeso;
	
	@Column(name = "CONTADOR_ENVIO")
	private Long contadorEnvio;
	
	@Column(name = "ULTIMO_XML_ENVIADO")
	private String ultimoXmlEnviado;
	
	@OneToOne(mappedBy = "cct", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Entregador.class)
	private Entregador entregador;
	
	@OneToOne(mappedBy = "cct", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Recebedor.class)
	private Recebedor recebedor;
	
	@OneToMany(mappedBy = "cct", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Exportador.class)
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	private List<Exportador> exportadores;
	
	/**
	 * 0 - DESBLOQUEADO
	 * 1 - BLOQUEADO
	 * */
	@Column(name = "BLOQUEADO_SISCOMEX")
	private Integer bloqueadoSiscomex;
	
	private Integer finalizado;
	
	@Column(name = "BILL_OF_LANDING")
	private String billoflanding;
	
//	@OneToOne(mappedBy = "cct", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Documento.class)
	@Transient
	private Documento documento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Armador getArmador() {
		return armador;
	}

	public void setArmador(Armador armador) {
		this.armador = armador;
	}

	public String getAvaria() {
		return avaria;
	}

	public void setAvaria(String avaria) {
		this.avaria = avaria;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getStatusProc() {
		return statusProc;
	}

	public void setStatusProc(String statusProc) {
		this.statusProc = statusProc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getDataentrada() {
		return dataentrada;
	}

	public void setDataentrada(String dataentrada) {
		this.dataentrada = dataentrada;
	}

	public String getDatasaida() {
		return datasaida;
	}

	public void setDatasaida(String datasaida) {
		this.datasaida = datasaida;
	}

	public String getDtProcessamento() {
		return dtProcessamento;
	}

	public void setDtProcessamento(String dtProcessamento) {
		this.dtProcessamento = dtProcessamento;
	}

	public BigDecimal getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(BigDecimal idExterno) {
		this.idExterno = idExterno;
	}

	public String getLacres() {
		return lacres;
	}

	public void setLacres(String lacres) {
		this.lacres = lacres;
	}

	public String getMotivoNaoPesagem() {
		return motivoNaoPesagem;
	}

	public void setMotivoNaoPesagem(String motivoNaoPesagem) {
		this.motivoNaoPesagem = motivoNaoPesagem;
	}

	public String getNavio() {
		return navio;
	}

	public void setNavio(String navio) {
		this.navio = navio;
	}

	public String getNumeroCont() {
		return numeroCont;
	}

	public void setNumeroCont(String numeroCont) {
		this.numeroCont = numeroCont;
	}

	public BigDecimal getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(BigDecimal pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public String getPlacareboque() {
		return placareboque;
	}

	public void setPlacareboque(String placareboque) {
		this.placareboque = placareboque;
	}

	public String getPlacaveiculo() {
		return placaveiculo;
	}

	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}

	public BigDecimal getTara() {
		return tara;
	}

	public void setTara(BigDecimal tara) {
		this.tara = tara;
	}

	public String getViagem() {
		return viagem;
	}

	public void setViagem(String viagem) {
		this.viagem = viagem;
	}

	public String getNrodue() {
		return nrodue;
	}

	public void setNrodue(String nrodue) {
		this.nrodue = nrodue;
	}

	public String getCpfCondutor() {
		return cpfCondutor;
	}

	public void setCpfCondutor(String cpfCondutor) {
		this.cpfCondutor = cpfCondutor;
	}

	public String getCnpjTransportador() {
		return cnpjTransportador;
	}

	public void setCnpjTransportador(String cnpjTransportador) {
		this.cnpjTransportador = cnpjTransportador;
	}

	public String getDivergenciasIdentificadas() {
		return divergenciasIdentificadas;
	}

	public void setDivergenciasIdentificadas(String divergenciasIdentificadas) {
		this.divergenciasIdentificadas = divergenciasIdentificadas;
	}

	public Integer getTipoTransportador() {
		return tipoTransportador;
	}

	public void setTipoTransportador(Integer tipoTransportador) {
		this.tipoTransportador = tipoTransportador;
	}

	public List<Danfe> getDanfes() {
		return danfes;
	}

	public void setDanfes(List<Danfe> danfes) {
		this.danfes = danfes;
	}

	public boolean isUnitSeal() {
		return unitSeal;
	}

	public void setUnitSeal(boolean unitSeal) {
		this.unitSeal = unitSeal;
	}

	public boolean isFirstWeighing() {
		return firstWeighing;
	}

	public void setFirstWeighing(boolean firstWeighing) {
		this.firstWeighing = firstWeighing;
	}

	public boolean isOog() {
		return oog;
	}

	public void setOog(boolean oog) {
		this.oog = oog;
	}

	public boolean isPendenciaPeso() {
		return pendenciaPeso;
	}

	public void setPendenciaPeso(boolean pendenciaPeso) {
		this.pendenciaPeso = pendenciaPeso;
	}

	public String getCpfTransportador() {
		return cpfTransportador;
	}

	public void setCpfTransportador(String cpfTransportador) {
		this.cpfTransportador = cpfTransportador;
	}

	public String getNomeTransportador() {
		return nomeTransportador;
	}

	public void setNomeTransportador(String nomeTransportador) {
		this.nomeTransportador = nomeTransportador;
	}

	public String getNomeCondutor() {
		return nomeCondutor;
	}

	public void setNomeCondutor(String nomeCondutor) {
		this.nomeCondutor = nomeCondutor;
	}

	public Long getContadorEnvio() {
		return contadorEnvio;
	}

	public void setContadorEnvio(Long contadorEnvio) {
		this.contadorEnvio = contadorEnvio;
	}

	public String getUltimoXmlEnviado() {
		return ultimoXmlEnviado;
	}

	public void setUltimoXmlEnviado(String ultimoXmlEnviado) {
		this.ultimoXmlEnviado = ultimoXmlEnviado;
	}

	public Entregador getEntregador() {
		return entregador;
	}

	public void setEntregador(Entregador entregador) {
		this.entregador = entregador;
	}

	public Recebedor getRecebedor() {
		return recebedor;
	}

	public void setRecebedor(Recebedor recebedor) {
		this.recebedor = recebedor;
	}

	public Integer getBloqueadoSiscomex() {
		return bloqueadoSiscomex;
	}

	public void setBloqueadoSiscomex(Integer bloqueadoSiscomex) {
		this.bloqueadoSiscomex = bloqueadoSiscomex;
	}

	public Integer getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Integer finalizado) {
		this.finalizado = finalizado;
	}

	public List<Exportador> getExportadores() {
		return exportadores;
	}

	public void setExportadores(List<Exportador> exportadores) {
		this.exportadores = exportadores;
	}
	
	public Date getDtproc() {
		return dtProc;
	}

	public void setDtproc(Date dtproc) {
		this.dtProc = dtproc;
	}

	public Date getDtentr() {
		return dtEntr;
	}

	public void setDtentr(Date dtentr) {
		this.dtEntr = dtentr;
	}

	public Date getDtsaid() {
		return dtSaid;
	}

	public void setDtsaid(Date dtsaid) {
		this.dtSaid = dtsaid;
	}

	public String getBilloflanding() {
		return billoflanding;
	}

	public void setBilloflanding(String billoflanding) {
		this.billoflanding = billoflanding;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

 



	
}
