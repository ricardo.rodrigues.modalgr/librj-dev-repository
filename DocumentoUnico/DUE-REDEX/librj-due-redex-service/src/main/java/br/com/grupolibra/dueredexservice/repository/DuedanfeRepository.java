package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Duedanfe;

@Repository
public interface DuedanfeRepository extends CrudRepository<Duedanfe, Long>, QuerydslPredicateExecutor<Duedanfe> {	
	
	public List<Duedanfe> findAll(Predicate predicate);
}
