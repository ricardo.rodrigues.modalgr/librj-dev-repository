package br.com.grupolibra.dueredexservice.service;

import br.com.grupolibra.dueredexservice.dto.CadastroConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.DanfeDTO;

public interface CadastroConteinerService {
	
	CadastroConteinerDTO saveConteiner(CadastroConteinerDTO dto);

	CadastroConteinerDTO getBooking(String bookingNbr);

	CadastroConteinerDTO validaInclusaoDanfe(String chave, String gkey, String numero);

	CadastroConteinerDTO deleteDanfe(DanfeDTO dto);

	ConteinerDTO getTransportadoraInfo(String cnpjs);

	byte[] geraRecibo(Long unitGkey, String cnpjs);

	CadastroConteinerDTO validaImpressaoRecibo(String unitgkey);

	CadastroConteinerDTO deleteUnit(ConteinerDTO dto);
	
	public CadastroConteinerDTO validaUnitId(String numeroConteiner);
}
