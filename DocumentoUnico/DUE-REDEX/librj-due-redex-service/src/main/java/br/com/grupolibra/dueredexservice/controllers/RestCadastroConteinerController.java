package br.com.grupolibra.dueredexservice.controllers;

import java.io.ByteArrayInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueredexservice.dto.CadastroConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.DanfeDTO;
import br.com.grupolibra.dueredexservice.service.CadastroConteinerService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestCadastroConteinerController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CadastroConteinerService cadastroConteinerService;
	
	@CrossOrigin
	@RequestMapping(value = "api/validaunitid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaUnitId(@RequestParam("numeroConteiner") String numeroConteiner) {
		logger.info("getBooking");
		return cadastroConteinerService.validaUnitId(numeroConteiner);
	}

	@CrossOrigin
	@RequestMapping(value = "api/saveconteiner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO saveConteiner(@RequestBody(required = true) CadastroConteinerDTO dto) {
		logger.info("saveConteiner");
		return cadastroConteinerService.saveConteiner(dto);
	}

	@CrossOrigin
	@RequestMapping(value = "api/getbooking", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO getBooking(@RequestParam("bookingNbr") String bookingNbr) {
		logger.info("getBooking");
		return cadastroConteinerService.getBooking(bookingNbr);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/validainclusaodanfe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaInclusaoDanfe(@RequestParam("chave") String chave, @RequestParam("unitgkey") String unitgkey, @RequestParam("numero") String numero) {
		logger.info("validaInclusaoDanfe");
		return cadastroConteinerService.validaInclusaoDanfe(chave, unitgkey, numero);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/validaimpressaorecibo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaImpressaoRecibo(@RequestParam("gkey") String gkey) {
		logger.info("validaImpressaoRecibo");
		return cadastroConteinerService.validaImpressaoRecibo(gkey);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/deletedanfe", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO deleteDanfe(@RequestBody(required = true) DanfeDTO dto) {
		logger.info("deleteDanfe");
		return cadastroConteinerService.deleteDanfe(dto);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/gettransportadorainfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConteinerDTO getTransportadoraInfo(@RequestParam("cnpjs") String cnpjs) {
		logger.info("getTransportadoraInfo");
		return cadastroConteinerService.getTransportadoraInfo(cnpjs);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/deleteconteiner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO deleteUnit(@RequestBody(required = true) ConteinerDTO dto) {
		logger.info("deleteUnit");
		return cadastroConteinerService.deleteUnit(dto);
	}	
	
	@SuppressWarnings("rawtypes")
	@CrossOrigin
	@RequestMapping(value = "/api/reciboconteiner", method = RequestMethod.GET)
	@Transactional
	public @ResponseBody ResponseEntity imprimeRecibo(@RequestParam("unitGkey") Long unitGkey, @RequestParam("numeroConteiner") String numeroConteiner, @RequestParam("cnpjs") String  cnpjs) {
		logger.info("imprimeRecibo");
		try {
			logger.info("imprimeRecibo - Gerando bytes pdf");
			byte[] pdf = cadastroConteinerService.geraRecibo(unitGkey, cnpjs);
			
			HttpHeaders headers = createHeaders(numeroConteiner);

		    return ResponseEntity
		            .ok()
		            .headers(headers)
		            .contentLength(pdf.length)
		            .contentType(MediaType.parseMediaType("application/octet-stream"))
		            .body(new InputStreamResource(new ByteArrayInputStream(pdf) ));

		} catch (Exception e) {			
			logger.info("ERRO - " + e.getMessage());
			return new ResponseEntity<>("ERRO AO GERAR PDF", null, HttpStatus.BAD_REQUEST);
		}
	}
	
	private HttpHeaders createHeaders(String numeroConteiner) {
		logger.info("createHeaders");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("x-filename", "ticket-Agendamento-cs.pdf");
		headers.add("Content-Disposition", "attachment; filename=\"recibo_exportacao_pre-stacking_"+numeroConteiner+".pdf\""); 
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		return headers;
	}
	
}