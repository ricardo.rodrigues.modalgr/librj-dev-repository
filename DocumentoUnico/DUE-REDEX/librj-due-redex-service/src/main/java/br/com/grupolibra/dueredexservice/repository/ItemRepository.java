package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueredexservice.model.Item;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long>, QuerydslPredicateExecutor<Item> {
		 
	public List<Item> findByDocumentoId(Long documentoId);	
}