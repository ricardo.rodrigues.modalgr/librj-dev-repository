package br.com.grupolibra.dueredexservice.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.EntregaConteinerService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueredexservice.util.ParadaProgramadaUtil;

@Component
public class EnviaEntregaConteinerNFEPendenteTask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// private static final long POLLING_RATE_MS = ((1000 * 60) * 60);
	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private EntregaConteinerService entregaConteinerService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private N4Repository n4Repository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void enviaEntregaConteinerNFE() {
		logger.info("###### INICIO TASK ENVIO ENTREGA PENDENTE POR NFE");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
		logger.info("enviaEntregaConteinerNFE");
		List<Cct> cctListEntregaConteiner = cctService.getCctsEnviaEntregaConteinerPendente(CategoriaProcessoEnum.NFE.toString());
		if (cctListEntregaConteiner != null && !cctListEntregaConteiner.isEmpty()) {
			if (!cctListEntregaConteiner.isEmpty()) {
				logger.info("enviaEntregaConteiner - qtd registros aguardando entrega conteiner : {} ", cctListEntregaConteiner.size());
				for (Cct cct : cctListEntregaConteiner) {
					EntregasConteineres xml = entregaConteinerService.getXMLEntregaConteinerNFE(cct);
					siscomexService.sendEntregaConteiner(xml, cct);
				}
			} else {
				logger.info("enviaEntregaConteinerNFE - sem registro ATA");
			}
		} else {
			logger.info("enviaEntregaConteinerNFE - sem registro para entrega conteiner");
		}
	}

}
