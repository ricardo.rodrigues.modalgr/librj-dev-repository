package br.com.grupolibra.dueredexservice.model.siscomex;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransitoSimplificadoRecepcao", propOrder = { "veiculoFerroviario", "veiculoRodoviario" })
@XStreamAlias("transitoSimplificado")
public class TransitoSimplificadoRecepcao {

	protected TransitoSimplificadoRecepcao.VeiculoFerroviario veiculoFerroviario;
	protected TransitoSimplificadoRecepcao.VeiculoRodoviario veiculoRodoviario;

	public TransitoSimplificadoRecepcao.VeiculoFerroviario getVeiculoFerroviario() {
		return veiculoFerroviario;
	}

	public void setVeiculoFerroviario(TransitoSimplificadoRecepcao.VeiculoFerroviario value) {
		this.veiculoFerroviario = value;
	}

	public TransitoSimplificadoRecepcao.VeiculoRodoviario getVeiculoRodoviario() {
		return veiculoRodoviario;
	}

	public void setVeiculoRodoviario(TransitoSimplificadoRecepcao.VeiculoRodoviario value) {
		this.veiculoRodoviario = value;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "prefixoTrem", "quantidadeVagoes" })
	public static class VeiculoFerroviario {

		@XmlElement(required = true)
		protected String prefixoTrem;
		@XmlElement(required = true)
		protected BigInteger quantidadeVagoes;

		public String getPrefixoTrem() {
			return prefixoTrem;
		}

		public void setPrefixoTrem(String value) {
			this.prefixoTrem = value;
		}

		public BigInteger getQuantidadeVagoes() {
			return quantidadeVagoes;
		}

		public void setQuantidadeVagoes(BigInteger value) {
			this.quantidadeVagoes = value;
		}

	}

	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "cpfCondutor", "documentoCondutorEstrangeiro", "nomeCondutorEstrangeiro", "veiculos" })
	public static class VeiculoRodoviario {

		protected String cpfCondutor;
		protected String documentoCondutorEstrangeiro;
		protected String nomeCondutorEstrangeiro;
		@XmlElement(required = true)
		protected TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos veiculos;

		/**
		 * Gets the value of the cpfCondutor property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getCpfCondutor() {
			return cpfCondutor;
		}

		/**
		 * Sets the value of the cpfCondutor property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setCpfCondutor(String value) {
			this.cpfCondutor = value;
		}

		/**
		 * Gets the value of the documentoCondutorEstrangeiro property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getDocumentoCondutorEstrangeiro() {
			return documentoCondutorEstrangeiro;
		}

		/**
		 * Sets the value of the documentoCondutorEstrangeiro property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setDocumentoCondutorEstrangeiro(String value) {
			this.documentoCondutorEstrangeiro = value;
		}

		/**
		 * Gets the value of the nomeCondutorEstrangeiro property.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getNomeCondutorEstrangeiro() {
			return nomeCondutorEstrangeiro;
		}

		/**
		 * Sets the value of the nomeCondutorEstrangeiro property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 * 
		 */
		public void setNomeCondutorEstrangeiro(String value) {
			this.nomeCondutorEstrangeiro = value;
		}

		/**
		 * Gets the value of the veiculos property.
		 * 
		 * @return possible object is {@link TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos }
		 * 
		 */
		public TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos getVeiculos() {
			return veiculos;
		}

		/**
		 * Sets the value of the veiculos property.
		 * 
		 * @param value
		 *            allowed object is {@link TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos }
		 * 
		 */
		public void setVeiculos(TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos value) {
			this.veiculos = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="veiculo" type="{http://www.pucomex.serpro.gov.br/cct}TVeiculoTransitoSimplificado" maxOccurs="4"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "veiculo" })
		public static class Veiculos {

			@XmlElement(required = true)
			@XStreamImplicit(itemFieldName = "veiculo")
			protected List<TVeiculoTransitoSimplificado> veiculo;

			/**
			 * Gets the value of the veiculo property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the veiculo property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getVeiculo().add(newItem);
			 * </pre>
			 * 
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list {@link TVeiculoTransitoSimplificado }
			 * 
			 * 
			 */
			public List<TVeiculoTransitoSimplificado> getVeiculo() {
				if (veiculo == null) {
					veiculo = new ArrayList<TVeiculoTransitoSimplificado>();
				}
				return this.veiculo;
			}

		}

	}

}
