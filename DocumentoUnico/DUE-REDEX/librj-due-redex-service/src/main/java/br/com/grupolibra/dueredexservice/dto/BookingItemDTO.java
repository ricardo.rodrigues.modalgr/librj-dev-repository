package br.com.grupolibra.dueredexservice.dto;

public class BookingItemDTO {
	
	public BookingItemDTO() {
		super();
	}
	
	public BookingItemDTO(Object[] o) {
		this.itemGkey 	= (o[0] != null && !"".equals(o[0])) ? o[0].toString() : "";
		this.descricao  = (o[1] != null && !"".equals(o[1])) ? o[1].toString() : "";
		this.iso		= (o[2] != null && !"".equals(o[2])) ? o[2].toString() : "";
		this.quantidade	= (o[3] != null && !"".equals(o[3])) ? o[3].toString() : "";
		this.archType 	=  (o[4] != null && !"".equals(o[4])) ? o[4].toString() : "";
	}
	
	public BookingItemDTO(String itemGkey, String descricao, String iso, String archType) {
		this.itemGkey 	= itemGkey;
		this.descricao  = descricao;
		this.iso		= iso;
		this.archType   = archType;
	}
	
	private String itemGkey;
	
	private String descricao;
	
	private String iso;
	
	private String archType;
	
	private String quantidade;
	
	private String disponivel;

	public String getItemGkey() {
		return itemGkey;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getIso() {
		return iso;
	}

	public void setItemGkey(String itemGkey) {
		this.itemGkey = itemGkey;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(String disponivel) {
		this.disponivel = disponivel;
	}
	
	public String getArchType() {
		return archType;
	}

	public void setArchType(String archType) {
		this.archType = archType;
	}

	@Override
	public String toString() {
		return "BookingItemDTO [itemGkey=" + itemGkey + ", descricao=" + descricao + ", iso=" + iso + ", quantidade=" + quantidade + ", disponivel=" + disponivel + ", archType=" + archType + "]";
	}
}
