package br.com.grupolibra.dueredexservice.model.mock;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONSULTA_CONTEINER_MOCK")
@NamedQuery(name = "ConsultaConteinerMock.findAll", query = "SELECT c FROM ConsultaConteinerMock c")
public class ConsultaConteinerMock implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONSULTA_CONTEINER_MOCK_SEQ")
	@SequenceGenerator(name = "CONSULTA_CONTEINER_MOCK_SEQ", sequenceName = "CONSULTA_CONTEINER_MOCK_SEQ", allocationSize = 1)
	private long id;

	@Column(name="NR_CONTEINER")
	private String nrConteiner;
	
	@Column(name="NR_DUE")
	private String nrDUE;

	public long getId() {
		return id;
	}

	public String getNrConteiner() {
		return nrConteiner;
	}

	public String getNrDUE() {
		return nrDUE;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNrConteiner(String nrConteiner) {
		this.nrConteiner = nrConteiner;
	}

	public void setNrDUE(String nrDUE) {
		this.nrDUE = nrDUE;
	}

	
}