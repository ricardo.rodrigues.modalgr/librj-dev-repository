package br.com.grupolibra.dueredexservice.model.siscomex;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carga", propOrder = { "tipoEmbalagem", "tipoGranel", "unidademedida", "total", "quantidade"})
@XStreamAlias("carga")
public class Carga {

	protected Integer tipoEmbalagem;	

	@XmlElement(required = true)
	protected Integer tipoGranel;

	@XmlElement(required = true)
	protected String unidademedida;

	@XmlElement(required = true)
	protected Integer total;

	@XmlElement(required = true)
	protected Integer quantidade;

	public Integer getTipoEmbalagem() {
		return tipoEmbalagem;
	}

	public void setTipoEmbalagem(Integer tipoEmbalagem) {
		this.tipoEmbalagem = tipoEmbalagem;
	}

	public Integer getTipoGranel() {
		return tipoGranel;
	}

	public void setTipoGranel(Integer tipoGranel) {
		this.tipoGranel = tipoGranel;
	}

	public String getUnidademedida() {
		return unidademedida;
	}

	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}
