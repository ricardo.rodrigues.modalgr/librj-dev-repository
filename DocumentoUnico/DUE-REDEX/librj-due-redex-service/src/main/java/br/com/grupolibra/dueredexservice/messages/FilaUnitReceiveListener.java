package br.com.grupolibra.dueredexservice.messages;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Danfe;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.RecepcaoService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;

@Component
public class FilaUnitReceiveListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String FILA_RECEPCAO_NFE = "due-redex-service.recepcao-nfe-due.queue";

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private DanfeRepository danfeRepository; 
	
	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private CctService cctService;
	
	@Autowired
	private CCTRepository cctRepository;

	@Autowired
	private DocumentoRepository documentoRepository;

	
	
	/**
	 * Listener da fila de Danfe Ao receber um evento UNIT_RECEIVE será postado na fila a gkey da unit para envio de recepção para siscomex
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_RECEPCAO_NFE)
	public void processaRecepcaoNFERedex(final String unitGkey) {
		logger.info("###### INICIO RECEPCAO POR NFE REDEX");
		logger.info("processaRecepcaoNFERedex - gKey UNIT_RECEIVE : {} ", unitGkey);
		try {
			if(!n4Repository.isConteinerExportacao(unitGkey)) {
				logger.info("processaRecepcaoNFERedex - Nao e conteiner exportacao");
				return;
			}else{
				if(n4Repository.isConteinerFCL(unitGkey)){
					String numeroConteiner = n4Repository.getConteinerByGkey(Long.valueOf(unitGkey))[0].toString();
					List<Danfe> danfeList = danfeRepository.findByNumeroConteiner(numeroConteiner);
					if(danfeList != null && !danfeList.isEmpty()) {
						logger.info("processaRecepcaoNFERedex - TEM DANFE");
						processaRecepcaoNFE(unitGkey);
					}else {
						logger.info("processaRecepcaoNFERedex - NÃO TEM DANFE");
						return;
					}
				}else if(n4Repository.isConteinerBBK(unitGkey)){
					String numeroConteiner =  n4Repository.getConteinerByGkey(Long.valueOf(unitGkey))[0].toString();
					Documento doc = documentoRepository.findByNumeroConteiner(numeroConteiner);
					if (doc==null) {
						logger.info("processaRecepcaoNFECargaSolta - NÃO TEM DANFE|DUE");
						return;
					}
					
					if("ITEM".equals(doc.getTipo())){
						logger.info("processaRecepcaoCargaSoltaDUERedex - TEM DUE");
						processaRecepcaoDUE(unitGkey);		
					}else if("NF".equals(doc.getTipo())){ 
						logger.info("processaRecepcaoCargaSoltaNFRedex - TEM DANFE");
						processaRecepcaoNFECargaSolta(unitGkey);					
					}
				}
			}
		} catch (Exception e) {
			logger.info("ERRO: {}", e.getMessage());
		}
	}

	private void processaRecepcaoNFE(final String unitGkey) {
		logger.info("processaRecepcaoNFE");
		Cct cct = cctService.createCctRecepcaoNFE(unitGkey);
		if (cct != null) {
			RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFE(cct);
			siscomexService.sendRecepcaoNFE(xml, cct);
		} else {
			logger.info("processaRecepcaoNFE - UNIT com pendencia para envio de recepcao");
		}
	}
	
	private void processaRecepcaoDUE(final String unitGkey) {
		logger.info("processaRecepcaoDUE");
		Cct cct = cctService.createCctRecepcaoDUE(unitGkey);
		if (cct != null) {
			RecepcoesDocumentoCarga xml = recepcaoService.getXMLRecepcaoDUE(cct);
			if (xml == null || xml.getRecepcaoDocumentoCarga() == null) {
				logger.info("processaRecepcaoDUE - erro ao gerar xml due");
			}else {
				siscomexService.sendRecepcaoCargaSoltaDUE(xml, cct);
			
				if (Cct.STATUS_RECEPCIONADO_OK.equals(cct.getStatusProc())) {
					boolean isAlterado = n4Repository.processaExportador(cct);
					if(isAlterado) 
						cctService.save(cct);
				}
			}
		} else {
			logger.info("processaRecepcaoDUE - UNIT com pendencia para envio de recepcao");
		}
	}
	
	
	private void processaRecepcaoNFECargaSolta(final String unitGkey) {
		logger.info("processaRecepcaoNFECargaSolta");
		Cct cct = cctService.createCctRecepcaoNFECargaSolta(unitGkey);
		if (cct != null) {
			RecepcoesNFE xml = recepcaoService.getXMLRecepcaoNFECargaSolta(cct);
			siscomexService.sendRecepcaoNFECargaSolta(xml, cct);
		} else {
			logger.info("processaRecepcaoNFECargaSolta - UNIT com pendencia para envio de recepcao");
		}
	}

}
