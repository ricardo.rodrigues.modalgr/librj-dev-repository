package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

public class BookingDTO {
	
	public BookingDTO() {
		super();
	}
	
	public BookingDTO(Object[] o) {
		this.bookingNbr 	= (o[0] != null && !"".equals(o[0])) ? o[0].toString() : "";
		this.viagem 		= (o[1] != null && !"".equals(o[1])) ? o[1].toString() : "";
		this.pod1 			= (o[2] != null && !"".equals(o[2])) ? o[2].toString() : "";
		this.pod2 			= (o[3] != null && !"".equals(o[3])) ? o[3].toString() : "";
		this.qtd 			= (o[4] != null && !"".equals(o[4])) ? o[4].toString() : "";
		this.gate 			= (o[5] != null && !"".equals(o[5])) ? o[5].toString() : "";
		this.imo 			= (o[6] != null && !"".equals(o[6])) ? o[6].toString() : "";
		this.refeer 		= (o[7] != null && !"".equals(o[7])) ? o[7].toString() : "";
		this.dry 			= (o[8] != null && !"".equals(o[8])) ? o[8].toString() : "";
		this.bookingGkey 	= (o[9] != null && !"".equals(o[9])) ? o[9].toString() : "";
		this.pol 			= (o[10] != null && !"".equals(o[10])) ? o[10].toString() : "";
		this.vesselName 	= (o[11] != null && !"".equals(o[11])) ? o[11].toString() : "";		
	}

	private String bookingGkey;
	
	private String bookingNbr;
	
	private String viagem;
	
	private String pol;
	
	private String vesselName;
	
	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}	
	
	
	private String pod1;
	
	private String qtd;
	
	private String pod2;
	
	private String gate;
	
	private String imo;
	
	private String refeer;
	
	private String dry;
	
	private List<ConteinerDTO> conteinerList;
	
	private List<BookingItemDTO> itemList;

	public String getBookingGkey() {
		return bookingGkey;
	}

	public String getBookingNbr() {
		return bookingNbr;
	}

	public String getViagem() {
		return viagem;
	}

	public String getPod1() {
		return pod1;
	}

	public String getQtd() {
		return qtd;
	}

	public String getPod2() {
		return pod2;
	}

	public String getGate() {
		return gate;
	}

	public String getImo() {
		return imo;
	}

	public String getRefeer() {
		return refeer;
	}

	public String getDry() {
		return dry;
	}

	public List<ConteinerDTO> getConteinerList() {
		return conteinerList;
	}

	public void setBookingGkey(String bookingGkey) {
		this.bookingGkey = bookingGkey;
	}

	public void setBookingNbr(String bookingNbr) {
		this.bookingNbr = bookingNbr;
	}

	public void setViagem(String viagem) {
		this.viagem = viagem;
	}

	public void setPod1(String pod1) {
		this.pod1 = pod1;
	}

	public void setQtd(String qtd) {
		this.qtd = qtd;
	}

	public void setPod2(String pod2) {
		this.pod2 = pod2;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public void setImo(String imo) {
		this.imo = imo;
	}

	public void setRefeer(String refeer) {
		this.refeer = refeer;
	}

	public void setDry(String dry) {
		this.dry = dry;
	}

	public void setConteinerList(List<ConteinerDTO> conteinerList) {
		this.conteinerList = conteinerList;
	}

	public List<BookingItemDTO> getItemList() {
		return itemList;
	}

	public void setItemList(List<BookingItemDTO> itemList) {
		this.itemList = itemList;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("BookingDTO [bookingGkey=" + bookingGkey + ", bookingNbr=" + bookingNbr + ", viagem=" + viagem + ", pod1=" + pod1 + ", qtd=" + qtd + ", pod2=" + pod2 + ", gate=" + gate + ", imo=" + imo + ", refeer=" + refeer + ", dry=" + dry + ", ");
		sb.append("itemList=");
		if(itemList != null && !itemList.isEmpty()){
			itemList.forEach(itemDTO -> sb.append(itemDTO.toString()));
		} else {
			sb.append("NULL");
		}
		sb.append("conteinerList=");
		if(conteinerList != null && !conteinerList.isEmpty()){
			conteinerList.forEach(conteinerDTO -> sb.append(conteinerDTO.toString()));
		} else {
			sb.append("NULL");
		}
		sb.append(" ]");
		return sb.toString();
	}

}
