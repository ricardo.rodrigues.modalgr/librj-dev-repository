package br.com.grupolibra.dueredexservice.service;

import java.util.List;

import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDueDanfeDTO;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;

public interface SiscomexService {
	
	public void sendRecepcaoNFE(RecepcoesNFE recepcoesNFE, Cct cct);
	
	public void sendRecepcaoCargaSoltaDUE(RecepcoesDocumentoCarga RecepcoesDocumentoCarga, Cct cct);

	public List<Cct> consultaDUEPorConteiner(List<Cct> cctListConsultaCadastroDUE);
	
	public void sendEntregaConteiner(EntregasConteineres entregasConteineres, Cct cct);

	public ConsultaDadosResumidosResponseListDTO consultaDadosResumidos(List<Cct> cctListConsultaDadosResumidos);

	public ConsultaDadosDueResponseListDTO consultaDadosDue(String dues);

	void sendRecepcaoNFECargaSolta(RecepcoesNFE recepcoesNFE, Cct cct);

	void consultaDuePorNf(DocumentoDueDanfeDTO nf);
	
}
