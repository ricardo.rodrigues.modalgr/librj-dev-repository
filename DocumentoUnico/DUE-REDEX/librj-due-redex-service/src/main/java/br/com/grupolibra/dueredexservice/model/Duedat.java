package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DUEDAT")
@NamedQuery(name = "Duedat.findAll", query = "SELECT a FROM Duedat a")
public class Duedat implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_DUEDAT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "duedat_seq")
	@SequenceGenerator(name = "duedat_seq", sequenceName = "duedat_seq", allocationSize = 1)
	private long id;

	@Column(name = "DUE_ID")
	private long due_Id;

	@Column(name = "DAT_ID")
	private long dat_Id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDueId() {
		return due_Id;
	}

	public void setDueId(long dueId) {
		this.due_Id = dueId;
	}

	public long getDatId() {
		return dat_Id;
	}

	public void setDatId(long datId) {
		this.dat_Id = datId;
	}

}
