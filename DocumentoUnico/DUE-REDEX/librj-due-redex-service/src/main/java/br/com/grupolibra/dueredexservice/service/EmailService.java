package br.com.grupolibra.dueredexservice.service;

public interface EmailService {
	
	public String sendSimpleMessage(String to, String subject, String mensagem);
	
}