package br.com.grupolibra.dueredexservice.model.siscomex;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamImplicitCollection;

@XStreamAlias("entregasConteineres")
public class EntregasConteineres {
	
	@XStreamAsAttribute
	private final String xmlns = "http://www.pucomex.serpro.gov.br/cct";

	@XStreamImplicit
	private List<EntregaConteiner> entregaConteiner;

	public EntregasConteineres() {
		this.entregaConteiner = new ArrayList<EntregaConteiner>();
	}

	public List<EntregaConteiner> getEntregaConteiner() {
		return entregaConteiner;
	}

	public void setEntregaConteiner(List<EntregaConteiner> entregaConteiner) {
		this.entregaConteiner = entregaConteiner;
	}

	public String getXmlns() {
		return xmlns;
	}

}
