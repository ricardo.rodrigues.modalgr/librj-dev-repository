package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Transporte;

public class MotoristaReciboDTO {

	public MotoristaReciboDTO() {
		super();
	}
	
	private Transporte transporte;
	
	private List<Transporte> transporteList;
	
	private List<Documento> documentosDisponiveis;
	
	private boolean erro;
	
	private String mensagem;
	
	public Transporte getTransporte() {
		return transporte;
	}

	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}

	public List<Transporte> getTransporteList() {
		return transporteList;
	}

	public void setTransporteList(List<Transporte> transporteList) {
		this.transporteList = transporteList;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<Documento> getDocumentosDisponiveis() {
		return documentosDisponiveis;
	}

	public void setDocumentosDisponiveis(List<Documento> documentosDisponiveis) {
		this.documentosDisponiveis = documentosDisponiveis;
	}

	@Override
	public String toString() {
		return "MotoristaReciboDTO [transporte=" + transporte + ", transporteList=" + transporteList + ", documentosDisponiveis=" + documentosDisponiveis + ", erro=" + erro + ", mensagem=" + mensagem + "]";
	}
		
}
