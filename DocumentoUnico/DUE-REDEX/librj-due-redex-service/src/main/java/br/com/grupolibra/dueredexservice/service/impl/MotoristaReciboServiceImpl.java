package br.com.grupolibra.dueredexservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.MotoristaReciboDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.DocumentoRecibo;
import br.com.grupolibra.dueredexservice.model.QDocumento;
import br.com.grupolibra.dueredexservice.model.Transporte;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.ShipperRepository;
import br.com.grupolibra.dueredexservice.repository.TransporteRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.MotoristaReciboService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class MotoristaReciboServiceImpl implements MotoristaReciboService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private N4Repository n4Repository;

	@Autowired
	private DocumentoRepository documentoRepository;

	@Autowired
	private ShipperRepository shipperRepository;

	@Autowired
	private TransporteRepository transporteRepository;

	@Override
	public MotoristaReciboDTO buscaTransportadora(String cnpj) {
		logger.info("buscaTransportadora - CNPJ - " + cnpj);
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();
		List<Transporte> lstAux = transporteRepository.findByCnpjTransportadora(cnpj);
		List<Transporte> lst = new ArrayList<>();
		if (lstAux != null && !lstAux.isEmpty()) {

			for (Transporte transporte : lstAux) {

				List<Long> ids = new ArrayList<>();
				for (Documento doc : transporte.getDocumentos()) {
					ids.add(doc.getId());
				}
				if (n4Repository.docsHasUnitInGate(ids)) {
					logger.info("buscaTransportadora - HAS UNIT_IN_GATE Event");
				} else {
					lst.add(transporte);
				}
			}

			retorno.setTransporteList(lst);
		}
		// if (lst != null && !lst.isEmpty()) {
		// retorno.setTransporteList(lst);
		// }
		return retorno;
	}

	@Override
	public MotoristaReciboDTO buscaMotorista(String searchDoc, boolean isEstrangeiro) {
		return n4Repository.getMotoristaByDocumento(searchDoc, isEstrangeiro);
	}

	@Override
	public MotoristaReciboDTO buscaDocumentosDisponiveis(String cnpj, String novo) {
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();

		QDocumento qdoc = QDocumento.documento;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(qdoc.cnpjTransportadora.eq(cnpj));
		builder.and(qdoc.transporte.isNull());
		List<Documento> lstAux = documentoRepository.findAll(builder.getValue());
		List<Documento> lst = new ArrayList<>();
		if (lstAux != null && !lstAux.isEmpty()) {

			for (Documento doc : lstAux) {
				if (!n4Repository.docHasUnitInGate(doc.getBlGkey())) {
					lst.add(doc);
				}
			}

			retorno.setDocumentosDisponiveis(lst);

		} else {
			if (novo.equals("1")) {
				retorno.setErro(true);
				retorno.setMensagem("Não existem NFs ou DU-Es disponíveis para a composição de Novo Recibo.");
			}
		}
		return retorno;
	}

	@Override
	public MotoristaReciboDTO salvaRecibo(Transporte transporte) {
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();
		
		try {
			logger.info("salvaRecibo - SALVANDO NOVO MOTOISTA");
			
			logger.info("salvaRecibo - transporte.isNovoCadastroMotorista() " + transporte.getNovoCadastroMotorista());
			logger.info("salvaRecibo - transporte.getPassaporte() " + transporte.getPassaporte());
			logger.info("salvaRecibo - transporte.getCpf() " + transporte.getCpf());
			logger.info("salvaRecibo - transporte.getCnh() " + transporte.getCnh());
			
			if(transporte.getPassaporte() != null && !"".equals(transporte.getPassaporte())) {
				if(n4Repository.existeMotoristaByDocumento(transporte.getPassaporte())) { 
					if(transporte.getId() == null && transporte.getNovoCadastroMotorista()) {
						retorno.setErro(true);
						retorno.setMensagem("Erro ao salvar novo motorista - Já existe motorista cadastrado para este passaporte. ");
						return retorno;
					}
				} else {
					String r = n4Repository.salvaNovoMotorista(transporte);
					if (r.contains("ERRO")) {
						logger.info("salvaRecibo - ERRO ao salvar motorista N4 -> " + r);
						retorno.setErro(true);
						retorno.setMensagem("Erro ao salvar novo motorista");
						return retorno;
					}
				}
			} else {
				if(n4Repository.existeMotoristaByDocumento(transporte.getCpf())) { 
					if(transporte.getId() == null && transporte.getNovoCadastroMotorista()) {
						retorno.setErro(true);
						retorno.setMensagem("Erro ao salvar novo motorista - Já existe motorista cadastrado para este CPF. ");
						return retorno;
					}
				} else if(n4Repository.existeMotoristaByDocumento(transporte.getCnh())) {
					if(transporte.getId() == null && transporte.getNovoCadastroMotorista()) {
						retorno.setErro(true);
						retorno.setMensagem("Erro ao salvar novo motorista - Já existe motorista cadastrado para esta CNH. ");
						return retorno;
					}
				} else {
					String r = n4Repository.salvaNovoMotorista(transporte);
					if (r.contains("ERRO")) {
						logger.info("salvaRecibo - ERRO ao salvar motorista N4 -> " + r);
						retorno.setErro(true);
						retorno.setMensagem("Erro ao salvar novo motorista");
						return retorno;
					}
				}
			}
		} catch (Exception e) {
			logger.info("salvaRecibo - ERRO ao salvar motorista N4 -> " + e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem("Erro ao salvar novo motorista");
			return retorno;
		}
		try {
			Transporte transporteAux = transporteRepository.save(transporte);
			
			if(transporte.getId() != null) {
				
				List<Documento> docList = documentoRepository.findByTransporteId(transporte.getId());
				for (Documento documento : docList) {
					logger.info("salvaRecibo - RETIRANDO DOCUMENTO -> " + documento.getNumero());
					documento.setTransporte(null);
					documentoRepository.save(documento);
				}
				
				for (Documento doc : transporte.getDocumentos()) {
					logger.info("salvaRecibo - DOCUMENTO -> " + doc.getNumero());
					doc.setTransporte(transporte);
					documentoRepository.save(doc);
				}
				
			} else {
			
				for (Documento doc : transporte.getDocumentos()) {
					logger.info("salvaRecibo - DOCUMENTO -> " + doc.getNumero());
					doc.setTransporte(transporteAux);
					documentoRepository.save(doc);
				}
			}

			retorno.setErro(false);
			retorno.setMensagem("Recibo salvo com sucesso.");
			return retorno;
		} catch (Exception e) {
			logger.info("salvaRecibo - ERRO ao salvar recibo -> " + e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem("Erro ao salvar recibo.");
			return retorno;
		}
	}

	@Override
	public CadastroDocumentoDTO validaModificacaoDocumentos(List<Documento> documentos) {
		CadastroDocumentoDTO retorno = new CadastroDocumentoDTO();
		retorno.setErro(false);
		for (Documento documento : documentos) {
			if (n4Repository.docHasUnitInGate(documento.getBlGkey())) {
				retorno.setErro(true);
				retorno.setMensagem("Recibo possui documento com entrada no GATE.");
				return retorno;
			}
		}
		return retorno;
	}

	@Override
	public MotoristaReciboDTO excluirRecibo(Transporte dto) {

		MotoristaReciboDTO retorno = new MotoristaReciboDTO();

		try {

			for (Documento doc : dto.getDocumentos()) {
				logger.info("salvaRecibo - DOCUMENTO -> " + doc.getNumero());
				doc.setTransporte(null);
				documentoRepository.save(doc);
			}
			transporteRepository.delete(dto);

			retorno.setErro(false);
			retorno.setMensagem("Recibo salvo com sucesso.");
			return retorno;
		} catch (Exception e) {
			logger.info("salvaRecibo - ERRO ao salvar recibo -> " + e.getMessage());
			retorno.setErro(true);
			retorno.setMensagem("Erro ao salvar recibo.");
			return retorno;
		}
	}

	@Override
	public MotoristaReciboDTO validaImpressaoRecibo(Transporte dto) {
		MotoristaReciboDTO retorno = new MotoristaReciboDTO();
		retorno.setErro(false);
		for (Documento documento : dto.getDocumentos()) {
			if (n4Repository.docHasUnitInGate(documento.getBlGkey())) {
				retorno.setErro(true);
				retorno.setMensagem("Recibo possui documento com entrada no GATE.");
				return retorno;
			}
		}
		return retorno;
	}

	@Override
	public byte[] geraRecibo(Long idTransporte) {
		logger.info("geraRecibo");

		Optional<Transporte> transporteOpt = transporteRepository.findById(idTransporte);
		
		if(transporteOpt.isPresent()) {
			Transporte transporte = transporteOpt.get();
			
			transporte.setDocumentosRecibo(geraDocumentosRecibo(transporte.getDocumentos()));
			
			ConteinerDTO dtoTransporte = getTransportadoraInfo(transporte.getCnpjTransportadora());
			transporte.setNomeTransportadora(dtoTransporte.getNomeTransportadora());
			
			List<Transporte> dtoList = new ArrayList<>();
			dtoList.add(transporte);
			
			Map<String, Object> parameters = new HashMap<>();
			Locale locale = new Locale("pt", "BR");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("pathImagemLogo", System.getProperty("user.dir") + getBarra());
			String pathJasper = System.getProperty("user.dir") + getBarra()+"reports"+getBarra()+"report-carga-solta"+getBarra();
			parameters.put("pathSubRelatorio", pathJasper + "reciboEntregaCargaSoltaRedex-subReport.jasper");
	
			JasperReport report = null;
			try {
				report = JasperCompileManager.compileReport(pathJasper + "reciboEntregaCargaSoltaRedex.jrxml");
				report.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
				return JasperRunManager.runReportToPdf(report, parameters, new JRBeanCollectionDataSource(dtoList));
			} catch (JRException e) {
				logger.error("ERRO : JASPER - {} ", e.getMessage());
				return null;
			}
		}
		return null;
	}
	
	private List<DocumentoRecibo> geraDocumentosRecibo(List<Documento> documentos) {
		List<DocumentoRecibo> docsRecibo = new ArrayList<>();
		for (Documento documento : documentos) {
			DocumentoRecibo docRecibo = new DocumentoRecibo();
			docRecibo.setNumero(documento.getNumero());
			docRecibo.setQuantidade(documento.getItens().get(0).getQuantidade());
			docRecibo.setPeso(documento.getItens().get(0).getPeso());
			docRecibo.setExportador(documento.getEmitente().getRazaoSocial());
			docRecibo.setConsolidador(documento.getConsolidador() != null ? documento.getConsolidador().getRazaoSocial() : "");
			docRecibo.setBookingnf(documento.getBooking() != null ? documento.getBooking() : "");
			
			docRecibo.setLote(n4Repository.getNumeroLoteByBLGkey(documento.getBlGkey()));
			docsRecibo.add(docRecibo);
		}
		return docsRecibo;
	}

	private static String getBarra() {		
		if ("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
			return "\\";
		}
		return "//";
	}
	
	@Override
	public ConteinerDTO getTransportadoraInfo(String cnpj) {
		logger.info("getTransportadoraInfo");
		ConteinerDTO retorno = new ConteinerDTO();
		if(cnpj != null && !cnpj.isEmpty()) {
			Object[] transportadoraInfo = n4Repository.getTransportadoraInfoByCnpj(cnpj);
			if(transportadoraInfo != null) {
				retorno.setNomeTransportadora(transportadoraInfo[0].toString());
				retorno.setCnpjTransportadora(transportadoraInfo[1].toString());
			}
		} else {
			logger.info("getTransportadoraInfo - CNPJ EMPTY");
		}
		return retorno;
	}
}
