package br.com.grupolibra.dueredexservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.grupolibra.dueredexservice.dto.BookingDTO;
import br.com.grupolibra.dueredexservice.dto.BookingItemDTO;
import br.com.grupolibra.dueredexservice.dto.CadastroConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.ConteinerDTO;
import br.com.grupolibra.dueredexservice.dto.DanfeDTO;
import br.com.grupolibra.dueredexservice.model.Danfe;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CadastroConteinerService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class CadastroConteinerServiceImpl implements CadastroConteinerService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private DanfeRepository danfeRepository;
	
	@Override
	public CadastroConteinerDTO validaUnitId(String numeroConteiner) {
		logger.info("validaUnitId");
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		Object[] retornoN4 = n4Repository.validaUnitId(numeroConteiner);
		if((boolean) retornoN4[0]) {
			logger.info("validaUnitId - ERRO VALIDANDO UNIT ID");
			logger.info("validaUnitId - ERRO - {} ", retornoN4[1]);
			retorno.setErro(true);
			if ("NUMERACAO".equals(retornoN4[1].toString())) 
					retorno.setMensagem("Já existe um Contêiner ativo com esta numeração.");			
			
			if ("CARGA SOLTA".equals(retornoN4[1].toString()))
				retorno.setMensagem("Este número já existe como Carga Solta.");
			
		} else {
			retorno.setErro(false);
			retorno.setMensagem(retornoN4[1].toString());
			if (retornoN4[2]!=null)
				retorno.setInformaISO(retornoN4[2].toString());
		}
		return retorno;
	}

	@Override
	public CadastroConteinerDTO saveConteiner(CadastroConteinerDTO dto) {
		logger.info("saveConteiner");
		
		if(dto.getConteinerCadastro() != null) {
			
			logger.info("saveConteiner - ConteinerCadastroDTO -> {} ", dto.getConteinerCadastro().toString());
			
			if(dto.getConteinerCadastro().getGkey() != null && !"".equals(dto.getConteinerCadastro().getGkey())) {
				logger.info("saveConteiner - UPDATE CONTEINER");
				
				if(n4Repository.hasUnitInGateEvent(dto.getConteinerCadastro().getGkey())) {
					dto.setErro(true);
					dto.setMensagem("Conteiner possui entrada no GATE.");
				} else {
					Object[] retorno = n4Repository.updateConteiner(dto.getConteinerCadastro());
					if((boolean) retorno[0]) {
						logger.info("saveConteiner - ERRO UPDATE CONTEINER");
						logger.info("saveConteiner - ERRO - {} ", retorno[1]);
						dto.setErro(true);
						dto.setMensagem(retorno[1] != null ? retorno[1].toString() : "NULL");
					}
				}
			} else {
				logger.info("saveConteiner - NEW CONTEINER");
				Object[] retorno = n4Repository.saveConteiner(dto.getBookingDTO().getBookingGkey(), dto.getConteinerCadastro());
				logger.info("saveConteiner - NEW CONTEINER - RETORNO 1 {} - 2 {} - 3 {}", retorno[0], retorno[1], retorno[2]);
				if((boolean) retorno[0]) {
					logger.info("saveConteiner - ERRO NEW CONTEINER");
					logger.info("saveConteiner - ERRO - {} ", retorno[1]);
					dto.setErro(true);
					dto.setMensagem(retorno[1] != null ? retorno[1].toString() : "NULL");
				} else {
					dto.getConteinerCadastro().setGkey(retorno[2] != null ? retorno[2].toString() : "NULL");
				}
			}
			
			if(!dto.isErro()) {
				if(dto.getConteinerCadastro().getDanfeList() != null && !dto.getConteinerCadastro().getDanfeList().isEmpty()) {
					logger.info("saveConteiner - SAVE DANFE LIST");
					saveDanfeList(dto);
					if(dto.isErro()) {
						return dto;
					} else {
						logger.info("saveConteiner - DANFE LIST SALVA COM SUCESSO.");
					}
				} else {
					logger.info("saveConteiner - EMPTY DANFE LIST");
				}
				
				dto.setErro(false);
				dto.setMensagem("Conteiner salvo com sucesso!");
			} 
		} else {
			logger.info("saveConteiner - CadastroConteinerDTO NULL");
			dto.setErro(true);
			dto.setMensagem("Ocorreu um problema ao salavar o conteiner, recearregue a tela e tente novamente.");
		}
		
		return dto;
	}

	private void saveDanfeList(CadastroConteinerDTO dto) {
		logger.info("saveDanfeList");
		String numeroConteiner = dto.getConteinerCadastro().getNumeroConteiner();
		for (DanfeDTO danfeDTO : dto.getConteinerCadastro().getDanfeList()) {
			if (danfeDTO.getId() == null) {
				logger.info("saveDanfeList - VALIDANDO NEW DANFE");
				List<Danfe> listDanfe = danfeRepository.findByChave(danfeDTO.getChave());
				if (listDanfe != null && !listDanfe.isEmpty()) {
					logger.info("saveDanfeList - CHAVE DANFE JA CADASTRADA");
					dto.setErro(true);
					dto.setMensagem("DANFE " + danfeDTO.getChave() + " já cadastrada para outra unidade.");
					return;
				}
				listDanfe = danfeRepository.findByNumero(danfeDTO.getNumero());
				if (listDanfe != null && !listDanfe.isEmpty()) {
					boolean match = false;
					for (int i = 0; i < listDanfe.size(); i++) {						
						String chaveAtual = listDanfe.get(i).getChave();						
						if(chaveAtual.contains(danfeDTO.getChave().substring(6, 20))){
							match = true;
						}						
					}					
					if(match){
						logger.info("saveDanfeList - NUMERO DANFE JA CADASTRADO");
						dto.setErro(true);
						dto.setMensagem("Número DANFE já cadastrado para este CNPJ!");
						return;
					}
				}
			}
		}

		for (DanfeDTO danfeDTO : dto.getConteinerCadastro().getDanfeList()) {
			if (danfeDTO.getId() == null) {
				
				logger.info("saveDanfeList - SALVANDO NEW DANFE ");
				
				Danfe Danfe = new Danfe();
				Danfe.setNumeroConteiner(numeroConteiner);
				Danfe.setNumero(danfeDTO.getNumero());
				Danfe.setChave(danfeDTO.getChave());
				danfeRepository.save(Danfe);
				
				logger.info("saveDanfeList - GERANDO EVENDO CADASTRO DANFE ");
				String event = "UNIT_VINC_DANFE";
				String unitGkey = dto.getConteinerCadastro().getGkey();
				String notes = "DANFE : " + danfeDTO.getChave();
				try { 
					String retorno = n4Repository.createUnitEvent(unitGkey, event, notes);
					if(!"OK".equals(retorno)) {
						dto.setErro(true);
						dto.setMensagem("ERRO ao criar evento de vinculo de DANFE para a chave " + danfeDTO.getChave() + ".");
						return;
					}
				} catch(Exception e) {
					dto.setErro(true);
					dto.setMensagem("ERRO ao criar evento de vinculo de DANFE para a chave " + danfeDTO.getChave() + ".");
					return;
				}
			}
		}
	}

	@Override
	public CadastroConteinerDTO getBooking(String bookingNbr) {
		logger.info("getBooking");
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		Object[] o =  n4Repository.getBooking(bookingNbr);
		if(o != null) {
			
			String bookingGkey = o[9].toString();
			if(!n4Repository.hasBookingItem(bookingGkey)) {
				retorno.setErro(true);
				retorno.setMensagem("O booking não possui ITEM.");
				return retorno;
			}
			
			BookingDTO bookingDTO = new BookingDTO(o);
			List<Object[]> oItens = n4Repository.getBookingItens(bookingGkey);
			if(oItens != null && !oItens.isEmpty()) {
				List<BookingItemDTO> bokingItemList = new ArrayList<>();
				addEmptyItem(bokingItemList);				
				for (Object[] oItem : oItens) {
					BookingItemDTO itemDTO = new BookingItemDTO(oItem);
					itemDTO.setDisponivel(n4Repository.getQtdDisponivelItem(bookingGkey, itemDTO.getIso(), itemDTO.getQuantidade()));
					bokingItemList.add(itemDTO);
				}
				bookingDTO.setItemList(bokingItemList);
			} else {
				retorno.setErro(true);
				retorno.setMensagem("Erro ao buscar itens do Booking.");
				return retorno;
			}
			logger.info("getBooking - BOOKING {} ", bookingDTO.toString());
			List<Object[]> objConteinerList = n4Repository.getConteinerListByBookingGkey(bookingDTO.getBookingGkey());
			if(objConteinerList != null && !objConteinerList.isEmpty()) {
				List<ConteinerDTO> conteinerDTOList = new ArrayList<>();
				for (Object[] object : objConteinerList) {
					ConteinerDTO conteinerDTO = new ConteinerDTO(object);
					for(BookingItemDTO item : bookingDTO.getItemList()) {
						if(item.getItemGkey().equals(conteinerDTO.getBookingItem().getItemGkey())) {
							conteinerDTO.getBookingItem().setQuantidade(item.getQuantidade());
							conteinerDTO.getBookingItem().setDisponivel(item.getDisponivel());
						}
					}
					logger.info("getBooking - CONTEINER {} ", conteinerDTO.toString());
					setDanfesConteiner(conteinerDTO);					
					conteinerDTOList.add(conteinerDTO);
				}
				conteinerDTOList.sort((c1, c2) -> c1.getGkey().compareTo(c2.getGkey()));
				bookingDTO.setConteinerList(conteinerDTOList);
			}
			retorno.setBookingDTO(bookingDTO);
		}
		return retorno;
	}

	private void addEmptyItem(List<BookingItemDTO> bokingItemList) {
		BookingItemDTO itemDTO = new BookingItemDTO();
		itemDTO.setDescricao("");
		itemDTO.setDisponivel("");
		itemDTO.setIso("");
		itemDTO.setItemGkey("0");
		itemDTO.setQuantidade("");
		bokingItemList.add(itemDTO);
	}

	private void setDanfesConteiner(ConteinerDTO conteinerDTO) {
		logger.info("setDanfesConteiner");
		
		List<Danfe> listDanfe = danfeRepository.findByNumeroConteiner(conteinerDTO.getNumeroConteiner());
		if(listDanfe != null && !listDanfe.isEmpty()) {
			List<DanfeDTO> danfeDTOlist = new ArrayList<>();
			String doc = "";
			for (Danfe Danfe : listDanfe) {
				DanfeDTO danfeDTO = new DanfeDTO();
				danfeDTO.setId(String.valueOf(Danfe.getId()));
				danfeDTO.setChave(Danfe.getChave());
				danfeDTO.setNumero(Danfe.getNumero());
				logger.info("setDanfesConteiner - DANFE {} ", danfeDTO.toString());
				doc += Danfe.getChave() + "<br>";
				danfeDTOlist.add(danfeDTO);
			}
			conteinerDTO.setDanfeList(danfeDTOlist);
			conteinerDTO.setDoc(doc.substring(0, doc.length()-4));
		}
		
	}

	@Override
	public CadastroConteinerDTO validaInclusaoDanfe(String chave, String gkey, String numero) {
		logger.info("validaInclusaoDanfe");
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		retorno.setErro(false);
		if(n4Repository.hasUnitInGateEvent(gkey)) {
			retorno.setErro(true);
			retorno.setMensagem("Conteiner possui entrada no GATE.");
		} else {
			List<Danfe> listDanfe = danfeRepository.findByChave(chave);
			if(listDanfe != null && !listDanfe.isEmpty()) {
				retorno.setErro(true);
				retorno.setMensagem("Documento DANFE ja cadastrado em outra unidade.");
			}
			listDanfe = danfeRepository.findByNumero(numero);
			if (listDanfe != null && !listDanfe.isEmpty()) {
				boolean match = false;
				for (int i = 0; i < listDanfe.size(); i++) {
					String chaveAtual = listDanfe.get(i).getChave();
					if (chaveAtual.contains(chave.substring(6, 20))) {
						match = true;
					}
				}
				if (match) {
					logger.info("saveDanfeList - NUMERO DANFE JA CADASTRADO");
					retorno.setErro(true);
					retorno.setMensagem("Número DANFE já cadastrado para este CNPJ!");
				}
			}
		}
		return retorno;
	}

	@Override
	public CadastroConteinerDTO deleteDanfe(DanfeDTO dto) {
		logger.info("deleteDanfe");
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		try {
			Optional<Danfe> danfeOpt = danfeRepository.findById(Long.valueOf(dto.getId()));
			if(danfeOpt.isPresent()) {
				Danfe danfe = danfeOpt.get();
				
				String event = "UNIT_VINC_DANFE_CANCEL";
				String unitGkey = n4Repository.getUnitGkeyByUnitId(danfe.getNumeroConteiner());
				String notes = "DANFE : " + danfe.getChave();
				
				if(n4Repository.hasUnitInGateEvent(unitGkey)) {
					retorno.setErro(true);
					retorno.setMensagem("Conteiner possui entrada no GATE.");
					return retorno;
				} 
				
				danfeRepository.delete(danfe);
				
				// GERANDO EVENDO CADASTRO DANFE
				try { 
					String retornoN4 = n4Repository.createUnitEvent(unitGkey, event, notes);
					if(!"OK".equals(retornoN4)) {
						retorno.setErro(true);
						retorno.setMensagem("ERRO ao criar evento de exclusão de documento DANFE.");
					} else {
						retorno.setErro(false);
						retorno.setMensagem("DANFE excluida com sucesso.");
					}
				} catch(Exception e) {
					retorno.setErro(true);
					retorno.setMensagem("ERRO - " + e.getMessage());
				}
			} else {
				retorno.setErro(true);
				retorno.setMensagem("ERRO ao buscar documento DANFE na base de dados.");
			}			
		} catch(Exception e) {
			retorno.setErro(true);
			retorno.setMensagem("ERRO - " + e.getMessage());
		}
		return retorno;
	}

	@Override
	public ConteinerDTO getTransportadoraInfo(String cnpj) {
		logger.info("getTransportadoraInfo");
		ConteinerDTO retorno = new ConteinerDTO();
		if(cnpj != null && !cnpj.isEmpty()) {
			Object[] transportadoraInfo = n4Repository.getTransportadoraInfoByCnpj(cnpj);
			if(transportadoraInfo != null) {
				retorno.setNomeTransportadora(transportadoraInfo[0].toString());
				retorno.setCnpjTransportadora(transportadoraInfo[1].toString());
			}
		} else {
			logger.info("getTransportadoraInfo - CNPJ EMPTY");
		}
		return retorno;
	}

	@Override
	public byte[] geraRecibo(Long unitGkey, String cnpjs) {
		logger.info("geraRecibo");
		
		List<ConteinerDTO> dtoList = new ArrayList<>();
		ConteinerDTO dto = getTransportadoraInfo(cnpjs);
		Object[] o = n4Repository.getConteinerByGkey(unitGkey);
		if(o != null) {
			dto.setNumeroConteiner(o[0].toString());
			dto.setPesoBruto(o[1].toString());
			dto.setLacre1(o[2] != null ? o[2].toString() : "");
			dto.setLacre2(o[3] != null ? o[3].toString() : "");
			dto.setLacre3(o[4] != null ? o[4].toString() : "");
			dto.setLacre4(o[5] != null ? o[5].toString() : "");
			setDanfesConteiner(dto);
			dtoList.add(dto);
			
			Map<String, Object> parameters = new HashMap<>();
			Locale locale = new Locale("pt", "BR");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			parameters.put("pathImagemLogo",  System.getProperty("user.dir")+getBarra());
			parameters.put("numeroRecibo", n4Repository.getReciboSequenceNextValue());
			String pathJasper = System.getProperty("user.dir")+getBarra()+"reports"+getBarra()+"report-pre-stacking"+getBarra();
			
			try {
				JasperCompileManager.compileReportToFile(pathJasper + "reciboRedex-subReport.jrxml", pathJasper + "reciboRedex-subReport.jasper");
			} catch (JRException e1) {
				logger.error("ERRO : reciboRedex-subReport - JASPER - {} ", e1.getMessage());
			} 			
			
			parameters.put("pathSubRelatorio",  pathJasper + "reciboRedex-subReport.jasper");
			
			JasperReport report = null;
			try {
				report = JasperCompileManager.compileReport(pathJasper+"reciboRedex.jrxml");
				report.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
				return JasperRunManager.runReportToPdf(report, parameters , new JRBeanCollectionDataSource(dtoList));  
			} catch (JRException e) {
				logger.error("ERRO : reciboRedex - JASPER - {} ", e.getMessage());
				return null;
			}
		}
		return null;
	}
	
	private static String getBarra() {		
		if ("WIN".equals(System.getProperty("os.name").toUpperCase().substring(0, 3))) {
			return "\\";
		}
		return "//";
	}

	@Override
	public CadastroConteinerDTO validaImpressaoRecibo(String unitgkey) {
		CadastroConteinerDTO retorno = new CadastroConteinerDTO();
		retorno.setErro(false);
		if(n4Repository.hasUnitInGateEvent(unitgkey)) {
			retorno.setErro(true);
			retorno.setMensagem("Não é possivel imprimir o recibo, Conteiner possui entrada no GATE.");
		}
		return retorno;
	}

	@Override
	public CadastroConteinerDTO deleteUnit(ConteinerDTO dto) {
		String gkey = dto.getGkey();
		if(n4Repository.hasUnitInGateEvent(gkey)) {
			CadastroConteinerDTO retorno = new CadastroConteinerDTO();
			retorno.setErro(true);
			retorno.setMensagem("Não é possivel excluir, Contêiner possui entrada no GATE.");
			return retorno;
		}
		return n4Repository.deleteUnit(gkey);
	}
}
