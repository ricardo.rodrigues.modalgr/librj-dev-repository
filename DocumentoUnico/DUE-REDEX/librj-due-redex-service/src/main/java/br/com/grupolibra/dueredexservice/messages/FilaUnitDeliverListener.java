package br.com.grupolibra.dueredexservice.messages;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.siscomex.EntregasConteineres;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.EntregaConteinerService;
import br.com.grupolibra.dueredexservice.service.RecepcaoService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;

@Component
public class FilaUnitDeliverListener {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String FILA_DELIVER_NFE = "due-redex-service.deliver-conteiner.queue";

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private DanfeRepository danfeRepository; 
	
	@Autowired
	private RecepcaoService recepcaoService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private EntregaConteinerService entregaConteinerService;
	
	@Autowired
	private CctService cctService;
	
	@Autowired
	private CCTRepository cctRepository;

	/**
	 * Listener da fila de Danfe Ao receber um evento UNIT_DELIVER será postado na fila a gkey da unit para envio de entrega para siscomex
	 * 
	 * @param unitGkey
	 */
	@JmsListener(destination = FILA_DELIVER_NFE)
	public void processaDeliverNFERedex(final String unitGkey) {
		logger.info("###### INICIO DELIVER POR NFE REDEX");
		logger.info("processaDeliverNFERedex - gKey UNIT_DELIVER : {} ", unitGkey);
		try {
			
			List<Cct> cctList = cctRepository.findByIdExterno(new BigDecimal(unitGkey));
			if(cctList != null && !cctList.isEmpty()) {
				Cct cct = cctList.get(0);
				if( !cct.getStatusProc().equals(Cct.STATUS_RECEPCIONADO_OK)) {
					logger.info("processaDeliverNFERedex - ERRO - " + Cct.STATUS_AGUARDANDO_RECEPCAO_ENTREGA);
					cct.setStatusProc(Cct.STATUS_AGUARDANDO_RECEPCAO_ENTREGA);
					cctService.save(cct);
					return;
				}
				if(cct.getNrodue() == null) {
					logger.info("processaDeliverNFERedex - ERRO - " + Cct.STATUS_AGUARDANDO_DUE_ENTREGA);
					cct.setStatusProc(Cct.STATUS_AGUARDANDO_DUE_ENTREGA);
					cctService.save(cct);
					return;
				}
				
				EntregasConteineres xml = entregaConteinerService.getXMLEntregaConteinerNFE(cct);
				siscomexService.sendEntregaConteiner(xml, cct);
				
			} else {
				logger.info("processaDeliverNFERedex - ERRO - UNIT não encontrada na base CCT");
			}
			
		} catch (Exception e) {
			logger.info("ERRO : {}", e.getMessage());
		}
	}

}
