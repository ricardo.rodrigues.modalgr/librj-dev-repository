package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import br.com.grupolibra.dueredexservice.model.Documento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoDTO {
	
	
	private long qtdRegistros;
	
	private int currentPage;
	
	public List<Documento> listaDocumento;
	
	
}
