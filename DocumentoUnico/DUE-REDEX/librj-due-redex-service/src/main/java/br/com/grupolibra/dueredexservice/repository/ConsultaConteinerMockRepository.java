package br.com.grupolibra.dueredexservice.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueredexservice.model.mock.ConsultaConteinerMock;

@Repository
public interface ConsultaConteinerMockRepository extends CrudRepository<ConsultaConteinerMock, Long>, QuerydslPredicateExecutor<ConsultaConteinerMock> {
	
	public ConsultaConteinerMock findByNrConteiner(String nrConteiner);
	
}