package br.com.grupolibra.dueredexservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Exportador {

	private String numero;
	
	private String tipo;
	
}
