package br.com.grupolibra.dueredexservice.dto;

import java.io.Serializable;

public class DocumentoRequestDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String cnpj;
	
	private String tipo;
	
	private String pageIndex;
	
	private String pageSize;
	
	private String booking;
	
	private String danfe;
	
	private String due;
	
	private String dataCadastroDe;
	
	private String dataCadastroAte;
	
	private String emitente;
	
	private String consolidador;
	
	private String numero;
	
	private String ncmNfCodigo;
	
	private String embalagemNfCodigo;
	
	private String ncmNfNome;
	
	private String embalagemNfNome;

	public String getNcmNfCodigo() {
		return ncmNfCodigo;
	}

	public void setNcmNfCodigo(String ncmNfCodigo) {
		this.ncmNfCodigo = ncmNfCodigo;
	}

	public String getEmbalagemNfCodigo() {
		return embalagemNfCodigo;
	}

	public void setEmbalagemNfCodigo(String embalagemNfCodigo) {
		this.embalagemNfCodigo = embalagemNfCodigo;
	}

	public String getNcmNfNome() {
		return ncmNfNome;
	}

	public void setNcmNfNome(String ncmNfNome) {
		this.ncmNfNome = ncmNfNome;
	}

	public String getEmbalagemNfNome() {
		return embalagemNfNome;
	}

	public void setEmbalagemNfNome(String embalagemNfNome) {
		this.embalagemNfNome = embalagemNfNome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getDanfe() {
		return danfe;
	}

	public void setDanfe(String danfe) {
		this.danfe = danfe;
	}

	public String getDue() {
		return due;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public String getDataCadastroDe() {
		return dataCadastroDe;
	}

	public void setDataCadastroDe(String dataCadastroDe) {
		this.dataCadastroDe = dataCadastroDe;
	}

	public String getDataCadastroAte() {
		return dataCadastroAte;
	}

	public void setDataCadastroAte(String dataCadastroAte) {
		this.dataCadastroAte = dataCadastroAte;
	}

	public String getEmitente() {
		return emitente;
	}

	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}

	public String getConsolidador() {
		return consolidador;
	}

	public void setConsolidador(String consolidador) {
		this.consolidador = consolidador;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@Override
	public String toString() {
		return "DocumentoRequestDTO [cnpj=" + cnpj + ", tipo=" + tipo + ", pageIndex=" + pageIndex + ", pageSize="
				+ pageSize + ", booking=" + booking + ", danfe=" + danfe + ", due=" + due + ", emitente=" + emitente
				+ ", consolidador=" + consolidador + ", numero=" + numero + "]";
	}

}
