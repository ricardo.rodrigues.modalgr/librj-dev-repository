package br.com.grupolibra.dueredexservice.dto;



import java.util.List;

import br.com.grupolibra.dueredexservice.model.Shipper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListaDocumentoDTO {
	
	private long id;
	
	private String cnpjTransportadora;
	
	private String numero;
	
	private Shipper emitente;
	
	private String danfe;
	
	private String ruc;
	
	private Shipper consolidador;
	
	private String booking;
	
	private String tipo;
	
	private List<String> itens;
	

	
	
	
	
	
}
