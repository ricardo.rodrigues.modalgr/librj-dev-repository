package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "DOCUMENTO")
@NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d")
public class Documento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_DOCUMENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOCUMENTO_SEQ")
	@SequenceGenerator(name = "DOCUMENTO_SEQ", sequenceName = "DOCUMENTO_SEQ", allocationSize = 1)
	private Long id;

	@Column(name = "TIPO")
	private String tipo;

	private String numero;

	@ManyToOne
	@JoinColumn(name = "EMITENTE_ID")
	private Shipper emitente;
	
	@ManyToOne
	@JoinColumn(name = "CONSOLIDADOR_ID")
	private Shipper consolidador;

	private String booking;

	private String danfe;

	private String ruc;

	@Column(name = "CNPJ_TRANSPORTADORA")
	private String cnpjTransportadora;

	@JsonIgnore
	@ManyToOne
    @JoinColumn(name="TRANSPORTE_ID")
    private Transporte transporte;

	@OneToMany(mappedBy = "documento", fetch = FetchType.EAGER, targetEntity = Item.class, cascade = CascadeType.REMOVE)
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	private List<Item> itens;
	
	private String blGkey;
	
	private String blNbr;
	
	@Transient
	private String blNotes;

	private Date dataCadastro;
	
	private String numeroConteiner;	

	@Column(name = "TOTAL")
	private Long Total;
	
	@Column(name = "STATUS")
	private Integer Status;		

	public Documento() {
		super();
	}
	
	public Documento(Object[] o) {
		this.setId(Long.valueOf(o[0].toString()));
		this.setTipo(String.valueOf(o[1]));
		this.setNumero(String.valueOf(o[2]));
		this.setBooking(String.valueOf(o[3]));
		this.setDanfe(String.valueOf(o[4]));
		this.setRuc(String.valueOf(o[5]));
		this.setCnpjTransportadora(String.valueOf(o[6]));
		this.setTransporte(null);
		this.setEmitente(null);
		this.setConsolidador(null);
		this.setBlNbr(String.valueOf(o[10]));
		this.setBlGkey(String.valueOf(o[11]));
		this.setDataCadastro(null);
		this.setNumeroConteiner(String.valueOf(o[13]));
	}

	public Long getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public String getNumero() {
		return numero;
	}

	public String getBooking() {
		return booking;
	}

	public String getDanfe() {
		return danfe;
	}

	public String getRuc() {
		return ruc;
	}

	public String getCnpjTransportadora() {
		return cnpjTransportadora;
	}

	public Transporte getTransporte() {
		return transporte;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public void setDanfe(String danfe) {
		this.danfe = danfe;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public void setCnpjTransportadora(String cnpjTransportadora) {
		this.cnpjTransportadora = cnpjTransportadora;
	}

	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Shipper getEmitente() {
		return emitente;
	}

	public void setEmitente(Shipper emitente) {
		this.emitente = emitente;
	}

	public Shipper getConsolidador() {
		return consolidador;
	}

	public void setConsolidador(Shipper consolidador) {
		this.consolidador = consolidador;
	}

	public String getBlGkey() {
		return blGkey;
	}

	public void setBlGkey(String blGkey) {
		this.blGkey = blGkey;
	}

	public String getBlNbr() {
		return blNbr;
	}

	public void setBlNbr(String blNbr) {
		this.blNbr = blNbr;
	}

	public String getBlNotes() {
		return blNotes;
	}

	public void setBlNotes(String blNotes) {
		this.blNotes = blNotes;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}
	
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNumeroConteiner() {
		return numeroConteiner;
	}

	public void setNumeroConteiner(String numeroConteiner) {
		this.numeroConteiner = numeroConteiner;
	}

	public Long getTotal() {
		return Total;
	}

	public void setTotal(Long total) {
		Total = total;
	}

	public Integer getStatus() {
		return Status;
	}

	public void setStatus(Integer status) {
		Status = status;
	}
	
	@Override
	public String toString() {
		return "Documento [id=" + id + ", tipo=" + tipo + ", numero=" + numero + ", emitente=" + emitente + ", consolidador=" + consolidador + ", booking=" + booking + ", danfe=" + danfe + ", ruc=" + ruc + ", cnpjTransportadora=" + cnpjTransportadora + ", transporte=" + transporte + ", itens=" 
				+ (itens != null ? itens.size() : "null") + ", blGkey=" + blGkey + ", blNbr=" + blNbr + ", dataCadastro=" + dataCadastro + ", numeroConteiner=" + numeroConteiner + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blGkey == null) ? 0 : blGkey.hashCode());
		result = prime * result + ((blNbr == null) ? 0 : blNbr.hashCode());
		result = prime * result + ((blNotes == null) ? 0 : blNotes.hashCode());
		result = prime * result + ((booking == null) ? 0 : booking.hashCode());
		result = prime * result + ((cnpjTransportadora == null) ? 0 : cnpjTransportadora.hashCode());
		result = prime * result + ((consolidador == null) ? 0 : consolidador.hashCode());
		result = prime * result + ((danfe == null) ? 0 : danfe.hashCode());
		result = prime * result + ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result + ((emitente == null) ? 0 : emitente.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((itens == null) ? 0 : itens.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((numeroConteiner == null) ? 0 : numeroConteiner.hashCode());
		result = prime * result + ((ruc == null) ? 0 : ruc.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((transporte == null) ? 0 : transporte.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		if (blGkey == null) {
			if (other.blGkey != null)
				return false;
		} else if (!blGkey.equals(other.blGkey))
			return false;
		if (blNbr == null) {
			if (other.blNbr != null)
				return false;
		} else if (!blNbr.equals(other.blNbr))
			return false;
		if (blNotes == null) {
			if (other.blNotes != null)
				return false;
		} else if (!blNotes.equals(other.blNotes))
			return false;
		if (booking == null) {
			if (other.booking != null)
				return false;
		} else if (!booking.equals(other.booking))
			return false;
		if (cnpjTransportadora == null) {
			if (other.cnpjTransportadora != null)
				return false;
		} else if (!cnpjTransportadora.equals(other.cnpjTransportadora))
			return false;
		if (consolidador == null) {
			if (other.consolidador != null)
				return false;
		} else if (!consolidador.equals(other.consolidador))
			return false;
		if (danfe == null) {
			if (other.danfe != null)
				return false;
		} else if (!danfe.equals(other.danfe))
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (emitente == null) {
			if (other.emitente != null)
				return false;
		} else if (!emitente.equals(other.emitente))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itens == null) {
			if (other.itens != null)
				return false;
		} else if (!itens.equals(other.itens))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (numeroConteiner == null) {
			if (other.numeroConteiner != null)
				return false;
		} else if (!numeroConteiner.equals(other.numeroConteiner))
			return false;
		if (ruc == null) {
			if (other.ruc != null)
				return false;
		} else if (!ruc.equals(other.ruc))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		if (transporte == null) {
			if (other.transporte != null)
				return false;
		} else if (!transporte.equals(other.transporte))
			return false;
		return true;
	}

	 
}
