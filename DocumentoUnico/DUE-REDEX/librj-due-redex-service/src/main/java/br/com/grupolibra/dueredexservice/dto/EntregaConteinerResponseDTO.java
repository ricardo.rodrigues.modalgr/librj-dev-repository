package br.com.grupolibra.dueredexservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EntregaConteinerResponseDTO {

	private boolean flag;

	private String erro;

	private String mensagem;

}
