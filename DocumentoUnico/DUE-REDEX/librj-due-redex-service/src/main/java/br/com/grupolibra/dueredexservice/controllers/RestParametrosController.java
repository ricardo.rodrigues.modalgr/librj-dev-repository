package br.com.grupolibra.dueredexservice.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueredexservice.model.Parametros;
import br.com.grupolibra.dueredexservice.repository.ParametrosRepository;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestParametrosController {

//	private static final String BD_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	private static final String DIVERGENCIA_PESO_ACIMA = "DIVERGENCIA_PESO_ACIMA";
	private static final String DIVERGENCIA_PESO_ABAIXO = "DIVERGENCIA_PESO_ABAIXO";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ParametrosRepository parametrosRepository;

	@CrossOrigin
	@RequestMapping(value = "/api/parametros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> getParametrosDivergenciaPeso() {
		logger.info("getParametrosDivergenciaPeso");
		// QParametros qParametros = QParametros.parametros;
		// BooleanBuilder builder = new BooleanBuilder();
		// builder.or(qParametros.chave.eq(DIVERGENCIA_PESO_ACIMA));
		// builder.or(qParametros.chave.eq(DIVERGENCIA_PESO_ABAIXO));
		// List<Parametros> paramDivPesoList = parametrosRepository.findAll(builder.getValue());
		List<Parametros> paramList = (List<Parametros>) parametrosRepository.findAll();
		convertToScreen(paramList);
		return paramList;
	}

	@CrossOrigin
	@RequestMapping(value = "/api/parametros/divergenciapeso", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> salvarParametrosDivergenciaPeso(@RequestBody(required = true) List<Parametros> parametros) {
		logger.info("salvarParametrosDivergenciaPeso");
		convertToBD(parametros);
		List<Parametros> paramDivPesoList = (List<Parametros>) parametrosRepository.saveAll(parametros);
		convertToScreen(paramDivPesoList);
		return paramDivPesoList;
	}

	@CrossOrigin
	@RequestMapping(value = "/api/parametros/clientefinanceiropagador", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Parametros salvarParametrosClienteFinanceiroPagador(@RequestBody(required = true) Parametros parametro) {
		logger.info("salvarParametrosClienteFinanceiroPagador");

		logger.info("################ PARAMS");

		logger.info("################ ID : " + parametro.getId());
		logger.info("################ CHAVE : " + parametro.getChave());
		logger.info("################ VALOR : " + parametro.getValor());
		logger.info("################ DATA : " + parametro.getData());
		parametro.setData(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

		Parametros param = (Parametros) parametrosRepository.save(parametro);
		return param;
	}

	@CrossOrigin
	@RequestMapping(value = "/api/parametros/balanca", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> salvarParametrosBalanca(@RequestBody(required = true) List<Parametros> parametroList) {
		logger.info("salvarParametrosClienteFinanceiroPagador");

		List<Parametros> retorno = new ArrayList<>();
		logger.info("################ PARAMS");

		for (Parametros parametro : parametroList) {

			logger.info("################ ID : " + parametro.getId());
			logger.info("################ CHAVE : " + parametro.getChave());
			logger.info("################ VALOR : " + parametro.getValor());
			logger.info("################ DATA : " + parametro.getData());
			parametro.setData(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

			Parametros param = (Parametros) parametrosRepository.save(parametro);

			logger.info("SALVO ID : " + parametro.getId());

			retorno.add(param);
		}

		return retorno;
	}

	private void convertToBD(List<Parametros> parametros) {
		for (Parametros parametro : parametros) {
			if (DIVERGENCIA_PESO_ACIMA.equals(parametro.getChave()) || DIVERGENCIA_PESO_ABAIXO.equals(parametro.getChave())) {
				String valor = parametro.getValor();
				Double valorDouble = (Double.parseDouble(valor)) / 100;
				parametro.setValor(valorDouble.toString());
				parametro.setData(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			}
		}
	}

	private void convertToScreen(List<Parametros> paramDivPesoList) {
		for (Parametros parametro : paramDivPesoList) {
			if (DIVERGENCIA_PESO_ACIMA.equals(parametro.getChave()) || DIVERGENCIA_PESO_ABAIXO.equals(parametro.getChave())) {
				Double valor = (Double.parseDouble(parametro.getValor()) * 100);
				Integer valorInt = valor.intValue();
				parametro.setValor(valorInt.toString());
			}
		}
	}

}