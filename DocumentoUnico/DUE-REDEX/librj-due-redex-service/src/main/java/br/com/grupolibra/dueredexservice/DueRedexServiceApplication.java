package br.com.grupolibra.dueredexservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DueRedexServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DueRedexServiceApplication.class, args);
	}
}
