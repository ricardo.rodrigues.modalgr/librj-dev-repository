package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import br.com.grupolibra.dueredexservice.model.Documento;

public class CadastroDocumentoDTO {

	public CadastroDocumentoDTO() {
		super();
	}
	
	private Documento documento;
	
	private List<Documento> documentoList;
	
	private boolean erro;
	
	private String mensagem;

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<Documento> getDocumentoList() {
		return documentoList;
	}

	public void setDocumentoList(List<Documento> documentoList) {
		this.documentoList = documentoList;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return "CadastroDocumentoDTO [documento=" + documento + ", documentoList=" + documentoList + ", erro=" + erro + ", mensagem=" + mensagem + "]";
	}
	
}
