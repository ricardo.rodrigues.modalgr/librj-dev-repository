package br.com.grupolibra.dueredexservice.util;

import java.util.HashMap;
import java.util.Map;

public enum SituacaoDUEEnum {

	EM_ELABORACAO(1, "Em elaboração"), 
	REGISTRADA(10, "Registrada"), 
	DECLARACAO_APRESENTADA_DESPACHO(11, "Declaração Apresentada para Despacho"), 
	LIBERADA_SEM_CONFERENCIA_ADUANEIRA_CANAL_VERDE(20, "Liberada sem conferência Aduaneira canal Verde"), 
	SELECIONADA_CONFERENCIA_CANAL_lARANJA_VERMELHO(21, "Selecionada para conferência canal laranja ou vermelho"), 
	EMBARQUE_ANTECIPADO_AUTORIZADO(25, "Embarque antecipado autorizado"), 
	EMBARQUE_ANTECIPADO_PENDENTE(26, "Embarque antecipado pendente de autorização"), 
	EM_ANALISE_FISCAL(30, "Em análise fiscal"), 
	CONCLUIDA_ANALISE_FISCAL(35, "Concluída análise fiscal"), 
	DESEMBARACADA(40, "Desenbaraçada"), 
	AVERBADA(70, "Averbada"), 
	CANCELADA_EXPORTADOR(80, "Cancelada pelo Exportador"), 
	CANCELADA_EXPIRACAO_PRAZO(81, "Cancelada por Expiração de Prazo"), 
	CANCELADA_ADUANA(82, "Cancelada pela Aduana"), 
	CANCELADA_ADUANA_EXPORTADOR(83, "Cancelada pela Aduana a pedido do exportador"), 
	INTERROMPIDA(86, "Interrompida");

	private int value;

	private String descricao;
	
	private static Map<Integer, SituacaoDUEEnum> map  = new HashMap<Integer, SituacaoDUEEnum>();
	
	static {
		for(SituacaoDUEEnum situacao : SituacaoDUEEnum.values()) {
			map.put(situacao.value, situacao);
		}
	}

	SituacaoDUEEnum(int value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	public int value() {
		return value;
	}

	public String descricao() {
		return descricao;
	}
	
	public static SituacaoDUEEnum valueOf(int value) {
		return map.get(value);
	}

}
