package br.com.grupolibra.dueredexservice.util;

import java.util.Calendar;
import java.util.Date;

public class ParadaProgramadaUtil {
	
	public static boolean isInPeriodoParadaProgramada() {
		
		Integer inicio = Integer.parseInt(PropertiesUtil.getProp().getProperty("parada.programada.hora.inicio"));
		Integer fim = Integer.parseInt(PropertiesUtil.getProp().getProperty("parada.programada.hora.fim"));
		
		Calendar calInicio = Calendar.getInstance();
		calInicio.set(Calendar.HOUR_OF_DAY,inicio);
		calInicio.set(Calendar.MINUTE,0);
		calInicio.set(Calendar.SECOND,0);
		calInicio.set(Calendar.MILLISECOND,0);
		Date dateInicio = calInicio.getTime();
		
		Calendar calFim = Calendar.getInstance();
		calFim.set(Calendar.HOUR_OF_DAY,fim);
		calFim.set(Calendar.MINUTE,0);
		calFim.set(Calendar.SECOND,0);
		calFim.set(Calendar.MILLISECOND,0);
		Date dateFim = calFim.getTime();
		
		Date agora = new Date();
		
		System.out.println("INICIO -> " + dateInicio);
		System.out.println("FIM -> " + dateFim);
		System.out.println("AGORA -> " + agora);
		
		if(agora.after(dateInicio) && agora.before(dateFim)) {
			return true;
		} else {
			return false;
		}
		
	}

}
