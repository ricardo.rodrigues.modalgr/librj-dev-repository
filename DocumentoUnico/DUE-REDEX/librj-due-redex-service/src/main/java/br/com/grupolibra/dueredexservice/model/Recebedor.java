package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="RECEBEDOR")
@NamedQuery(name = "Recebedor.findAll", query = "SELECT r FROM Recebedor r")
public class Recebedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECEBEDOR_SEQ")
	@SequenceGenerator(name = "RECEBEDOR_SEQ", sequenceName = "RECEBEDOR_SEQ", allocationSize = 1)
	private long id;

	private String cnpj;
	
	private String nome;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "CCT_ID")
	private Cct cct;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cct getCct() {
		return cct;
	}

	public void setCct(Cct cct) {
		this.cct = cct;
	}

}