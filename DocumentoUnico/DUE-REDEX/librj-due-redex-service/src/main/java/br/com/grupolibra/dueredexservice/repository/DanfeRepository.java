package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.dueredexservice.model.Danfe;

@Repository
public interface DanfeRepository extends CrudRepository<Danfe, Long>, QuerydslPredicateExecutor<Danfe> {
	
	public List<Danfe> findByCctId(long id);
	
	public List<Danfe> findByNumeroConteiner(String numeroConteiner);
	
	public List<Danfe> findByNumeroConteinerAndChave(String numeroConteiner, String chave);
	
	public List<Danfe> findByChave(String chave);
	
	public List<Danfe> findByNumero(String numero);	
}
