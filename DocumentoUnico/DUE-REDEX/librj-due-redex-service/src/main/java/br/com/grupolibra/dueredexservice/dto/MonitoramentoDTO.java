package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import br.com.grupolibra.dueredexservice.model.Monitoramento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoramentoDTO {
	
	private long qtdPaginas;
	
	private long qtdRegistros;
	
	private int currentPage;
	
	private List<ListaMonitoramentoDTO> listaFinalizado;
	
	private List<Monitoramento> listaMonitoramento;
	
}
