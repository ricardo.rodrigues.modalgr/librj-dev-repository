package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ITEM")
@NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_ITEM")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_SEQ")
	@SequenceGenerator(name = "ITEM_SEQ", sequenceName = "ITEM_SEQ", allocationSize = 1)
	private long id;

	private String numero;

	@Column(name = "NCM_ID")
	private String ncmId;
	
	private String ncm;

	private String quantidade;

	@Column(name = "EMBALAGEM_ID")
	private String embalagemId;
	
	private String embalagem;

	private String peso;

	@Column(name = "MARCAS_NUMEROS")
	private String marcasNumeros;

	@Column(name = "INFO_ADICIONAL")
	private String infoAdicional;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "DOCUMENTO_ID")
	private Documento documento;	

	@Column(name = "TOTAL")
	private Integer Total;	

	public long getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public String getNcm() {
		return ncm;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public String getEmbalagem() {
		return embalagem;
	}

	public String getPeso() {
		return peso;
	}

	public String getMarcasNumeros() {
		return marcasNumeros;
	}

	public String getInfoAdicional() {
		return infoAdicional;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public void setEmbalagem(String embalagem) {
		this.embalagem = embalagem;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public void setMarcasNumeros(String marcasNumeros) {
		this.marcasNumeros = marcasNumeros;
	}

	public void setInfoAdicional(String infoAdicional) {
		this.infoAdicional = infoAdicional;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getNcmId() {
		return ncmId;
	}

	public void setNcmId(String ncmId) {
		this.ncmId = ncmId;
	}

	public String getEmbalagemId() {
		return embalagemId;
	}

	public void setEmbalagemId(String embalagemId) {
		this.embalagemId = embalagemId;
	}
	
	public Integer getTotal() {
		return Total;
	}

	public void setTotal(Integer total) {
		Total = total;
	}	

}