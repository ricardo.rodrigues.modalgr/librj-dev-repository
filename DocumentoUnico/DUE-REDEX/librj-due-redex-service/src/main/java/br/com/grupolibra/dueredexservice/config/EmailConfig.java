package br.com.grupolibra.dueredexservice.config;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.stereotype.Component;

import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Component("EmailConfig")
public class EmailConfig {

	public Session getJavaMailSession() {

		Properties props = new Properties();
			
		props.put("mail.transport.protocol","smtp");		
		props.put("mail.smtp.host", PropertiesUtil.getProp().getProperty("mail.host")); //ArquivoConfiguracao.getMailSenderHost());
		props.put("mail.smtp.port", PropertiesUtil.getProp().getProperty("mail.port"));//ArquivoConfiguracao.getMailSenderPort());
		props.put("mail.debug", "true");
		
		//if (!ArquivoConfiguracao.getMailSenderSmtpAuth().equals("")) {
			props.put("mail.smtp.auth", PropertiesUtil.getProp().getProperty("mail.smtp.auth"));//ArquivoConfiguracao.getMailSenderSmtpAuth());			
		//}		
		props.put("mail.smtp.starttls.enable", PropertiesUtil.getProp().getProperty("mail.smtp.starttls.enable")); //ArquivoConfiguracao.getMailSenderSmtpStarttlsEnable());
	
		// Get the Session object.
		return Session.getInstance(props, new javax.mail.Authenticator() {			
			protected PasswordAuthentication getPasswordAuthentication() {				
				return new PasswordAuthentication(PropertiesUtil.getProp().getProperty("mail.username"), PropertiesUtil.getProp().getProperty("mail.password")); //ArquivoConfiguracao.getMailUserName(), ArquivoConfiguracao.getMailSenderPassword());
			}
		});
	}
}
