package br.com.grupolibra.dueredexservice.model.siscomex;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <p>
 * Java class for recepcaoNFE complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="recepcaoNFE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificacaoRecepcao" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica50"/>
 *         &lt;element name="cnpjResp" type="{http://www.pucomex.serpro.gov.br/cct}CNPJ"/>
 *         &lt;element name="local" type="{http://www.pucomex.serpro.gov.br/cct}Local"/>
 *         &lt;element name="referenciaLocalRecepcao" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica150" minOccurs="0"/>
 *         &lt;element name="notasFiscais">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="notaFiscalEletronica" type="{http://www.pucomex.serpro.gov.br/cct}NotaFiscalEletronica" maxOccurs="50"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transportador" type="{http://www.pucomex.serpro.gov.br/cct}Transportador"/>
 *         &lt;choice>
 *           &lt;element name="pesoAferido" type="{http://www.pucomex.serpro.gov.br/cct}Dec_9v3"/>
 *           &lt;element name="motivoNaoPesagem" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica250"/>
 *         &lt;/choice>
 *         &lt;element name="localArmazenamento" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica150" minOccurs="0"/>
 *         &lt;element name="codigoIdentCarga" type="{http://www.pucomex.serpro.gov.br/cct}NumeroRUC" minOccurs="0"/>
 *         &lt;element name="avariasIdentificadas" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica250" minOccurs="0"/>
 *         &lt;element name="divergenciasIdentificadas" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica250" minOccurs="0"/>
 *         &lt;element name="observacoesGerais" type="{http://www.pucomex.serpro.gov.br/cct}StringBasica250" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecepcaoConteiner", propOrder = { "identificacaoRecepcao", "cnpjResp", "local", "referenciaLocalRecepcao", "entregador", "conteineres", "localArmazenamento", "codigoIdentCarga", "avariasIdentificadas", "divergenciasIdentificadas", "observacoesGerais", "transitoSimplificado" })
public class RecepcaoConteiner {

	@XmlElement(required = true)
	protected String identificacaoRecepcao;

	@XmlElement(required = true)
	protected String cnpjResp;

	@XmlElement(required = true)
	protected Local local;

	protected String referenciaLocalRecepcao;

	@XmlElement(required = true)
	protected Entregador entregador;

	@XmlElement(required = true)
	@XStreamAlias("conteineres")
	List<Conteiner> conteineres = new ArrayList<>();

	protected String localArmazenamento;
	protected String codigoIdentCarga;
	protected String avariasIdentificadas;
	protected String divergenciasIdentificadas;
	protected String observacoesGerais;
	protected TransitoSimplificadoRecepcao transitoSimplificado;

	public String getIdentificacaoRecepcao() {
		return identificacaoRecepcao;
	}

	public void setIdentificacaoRecepcao(String identificacaoRecepcao) {
		this.identificacaoRecepcao = identificacaoRecepcao;
	}

	public String getCnpjResp() {
		return cnpjResp;
	}

	public void setCnpjResp(String cnpjResp) {
		this.cnpjResp = cnpjResp;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public String getReferenciaLocalRecepcao() {
		return referenciaLocalRecepcao;
	}

	public void setReferenciaLocalRecepcao(String referenciaLocalRecepcao) {
		this.referenciaLocalRecepcao = referenciaLocalRecepcao;
	}

	public Entregador getEntregador() {
		return entregador;
	}

	public void setEntregador(Entregador entregador) {
		this.entregador = entregador;
	}

	public List<Conteiner> getConteineres() {
		return conteineres;
	}

	public void setConteineres(List<Conteiner> conteineres) {
		this.conteineres = conteineres;
	}

	public String getLocalArmazenamento() {
		return localArmazenamento;
	}

	public void setLocalArmazenamento(String localArmazenamento) {
		this.localArmazenamento = localArmazenamento;
	}

	public String getCodigoIdentCarga() {
		return codigoIdentCarga;
	}

	public void setCodigoIdentCarga(String codigoIdentCarga) {
		this.codigoIdentCarga = codigoIdentCarga;
	}

	public String getAvariasIdentificadas() {
		return avariasIdentificadas;
	}

	public void setAvariasIdentificadas(String avariasIdentificadas) {
		this.avariasIdentificadas = avariasIdentificadas;
	}

	public String getDivergenciasIdentificadas() {
		return divergenciasIdentificadas;
	}

	public void setDivergenciasIdentificadas(String divergenciasIdentificadas) {
		this.divergenciasIdentificadas = divergenciasIdentificadas;
	}

	public String getObservacoesGerais() {
		return observacoesGerais;
	}

	public void setObservacoesGerais(String observacoesGerais) {
		this.observacoesGerais = observacoesGerais;
	}

	public TransitoSimplificadoRecepcao getTransitoSimplificado() {
		return transitoSimplificado;
	}

	public void setTransitoSimplificado(TransitoSimplificadoRecepcao transitoSimplificado) {
		this.transitoSimplificado = transitoSimplificado;
	}

}
