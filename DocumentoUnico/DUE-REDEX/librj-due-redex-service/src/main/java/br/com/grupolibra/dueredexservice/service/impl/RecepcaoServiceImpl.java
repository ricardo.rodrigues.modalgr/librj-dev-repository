package br.com.grupolibra.dueredexservice.service.impl;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Danfe;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Item;
import br.com.grupolibra.dueredexservice.model.siscomex.Carga;
import br.com.grupolibra.dueredexservice.model.siscomex.Conteiner;
import br.com.grupolibra.dueredexservice.model.siscomex.documento;
import br.com.grupolibra.dueredexservice.model.siscomex.Entregador;
import br.com.grupolibra.dueredexservice.model.siscomex.Local;
import br.com.grupolibra.dueredexservice.model.siscomex.NotaFiscalEletronica;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcaoDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcaoNFE;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesDocumentoCarga;
import br.com.grupolibra.dueredexservice.model.siscomex.RecepcoesNFE;
import br.com.grupolibra.dueredexservice.model.siscomex.TVeiculoTransitoSimplificado;
import br.com.grupolibra.dueredexservice.model.siscomex.TransitoSimplificadoRecepcao;
import br.com.grupolibra.dueredexservice.model.siscomex.TransitoSimplificadoRecepcao.VeiculoRodoviario;
import br.com.grupolibra.dueredexservice.model.siscomex.TransitoSimplificadoRecepcao.VeiculoRodoviario.Veiculos;
import br.com.grupolibra.dueredexservice.model.siscomex.Transportador;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.RecepcaoService;
import br.com.grupolibra.dueredexservice.util.LocalEnum;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Service
public class RecepcaoServiceImpl implements RecepcaoService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DanfeRepository danfeRepository;
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private N4Repository n4Repository;
	
	Documento doc;
	List<Item> itens;
	

	@Override
	public RecepcoesNFE getXMLRecepcaoNFE(Cct cct) {
		logger.info("getXMLRecepcaoNFE");
		cct.setDanfes(danfeRepository.findByCctId(cct.getId()));
		RecepcoesNFE recepcoesNFE = new RecepcoesNFE();
		RecepcaoNFE recepcaoNFE = new RecepcaoNFE();
		recepcaoNFE.setIdentificacaoRecepcao("RECNFE|" + cct.getNumeroCont() + new Date().getTime());
		recepcaoNFE.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		if (cct.getDivergenciasIdentificadas() != null && !cct.getDivergenciasIdentificadas().isEmpty()) {
			recepcaoNFE.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		}
		recepcaoNFE.setLocal(getLocal());
		List<Conteiner> conteineres = getListConteineres(cct, recepcaoNFE);
		setNFes(cct, conteineres, recepcaoNFE);
		setTransportador(cct, recepcaoNFE);
		recepcoesNFE.setRecepcaoNFE(recepcaoNFE);
		return recepcoesNFE;
	}	
	
	@Override
	public RecepcoesNFE getXMLRecepcaoNFECargaSolta(Cct cct) {
		logger.info("getXMLRecepcaoNFE");
		RecepcoesNFE recepcoesNFE = new RecepcoesNFE();
		RecepcaoNFE recepcaoNFE = new RecepcaoNFE();
		recepcaoNFE.setIdentificacaoRecepcao("RECNFE|" + cct.getNumeroCont() + new Date().getTime());
		recepcaoNFE.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
		if (cct.getDivergenciasIdentificadas() != null && !cct.getDivergenciasIdentificadas().isEmpty()) {
			recepcaoNFE.setDivergenciasIdentificadas(cct.getDivergenciasIdentificadas());
		}
		recepcaoNFE.setLocal(getLocal());
		List<Conteiner> conteineres = getListConteineres(cct, recepcaoNFE);
		setNFesCargaSolta(cct, recepcaoNFE);
		setTransportador(cct, recepcaoNFE);
		
		recepcoesNFE.setRecepcaoNFE(recepcaoNFE);
		return recepcoesNFE;
	}

	private void setTransportador(Cct cct, RecepcaoNFE recepcaoNFE) {
		logger.info("setTransportador");
		Transportador transportador = new Transportador();
		if (cct.getTipoTransportador()!=null) {
			if (cct.getTipoTransportador() == 1) {
				transportador.setNomeEstrangeiro(cct.getNomeTransportador().substring(0, 59));
				transportador.setNomeCondutorEstrangeiro(cct.getNomeCondutor());
			} else {
				if (cct.getCnpjTransportador() != null && !cct.getCnpjTransportador().isEmpty()) {
					transportador.setCnpj(cct.getCnpjTransportador());
				}
				if (cct.getCpfTransportador() != null && !cct.getCpfTransportador().isEmpty()) {
					transportador.setCpf(cct.getCpfTransportador());
				}
				if (cct.getCpfCondutor() != null && !cct.getCpfCondutor().isEmpty()) {
					transportador.setCpfCondutor(cct.getCpfCondutor());
				}
			}
			recepcaoNFE.setTransportador(transportador);
		}
	}

	private void setNFes(Cct cct, List<Conteiner> conteineres, RecepcaoNFE recepcaoNFE) {
		logger.info("setNFes");
		List<NotaFiscalEletronica> notasFiscais = new ArrayList<>();
		for (Danfe danfe : cct.getDanfes()) {
			NotaFiscalEletronica nfe = new NotaFiscalEletronica();
			nfe.setChaveAcesso(danfe.getChave());
			nfe.setConteineres(conteineres);
			notasFiscais.add(nfe);
		}
		recepcaoNFE.setNotasFiscais(notasFiscais);
	}
	
	
	private void setNFesCargaSolta(Cct cct, RecepcaoNFE recepcaoNFE) {
		logger.info("setNFesCargaSolta");
		List<NotaFiscalEletronica> notasFiscais = new ArrayList<>();
			NotaFiscalEletronica nfe = new NotaFiscalEletronica();
			Documento documento = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());
			nfe.setChaveAcesso(documento.getDanfe());
			nfe.setConteineres(null);
			notasFiscais.add(nfe);
			recepcaoNFE.setNotasFiscais(notasFiscais);
	}
	

	private Local getLocal() {
		logger.info("setLocal");
		Local local = new Local();
		local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
		local.setCodigoURF(LocalEnum.PORTO_DE_RJ.value());
		return local;
	}

	private List<Conteiner> getListConteineres(Cct cct, RecepcaoNFE recepcaoNFE) {
		logger.info("getListConteineres");
		List<Conteiner> conteineres = new ArrayList<>();
		Conteiner conteiner = new Conteiner();
		if (cct.getPesoBruto() == null && cct.getMotivoNaoPesagem() != null && !cct.getMotivoNaoPesagem().isEmpty()) {
			recepcaoNFE.setMotivoNaoPesagem(cct.getMotivoNaoPesagem());
		} else {
			if(cct.getPesoBruto() != null && cct.getPesoBruto().longValue() > 0L ) {
				recepcaoNFE.setPesoAferido(new BigDecimal(cct.getPesoBruto().toString()).setScale(3));
			} else {
				recepcaoNFE.setPesoAferido(new BigDecimal("0.001"));
			}
		}
		conteiner.setTara(new BigDecimal(cct.getTara() != null ? cct.getTara().toString() : "0").setScale(3));
		conteiner.setNumeroConteiner(cct.getNumeroCont());
		List<String> lacres = Arrays.asList(cct.getLacres().split(","));
		conteiner.setLacres(lacres);
		conteineres.add(conteiner);
		if (!StringUtils.isEmpty(cct.getAvaria())) {
			recepcaoNFE.setAvariasIdentificadas(cct.getAvaria());
		}
		return conteineres;
	}
	
// Carga Solta	

	@SuppressWarnings("null")
	@Override
	public RecepcoesDocumentoCarga getXMLRecepcaoDUE(Cct cct) {
		String[] dues = null;
		String due = null;
		if(cct.getNrodue().contains(",")) {			
			dues = cct.getNrodue().split(",");
		}else {
			due = cct.getNrodue();
		}

		RecepcoesDocumentoCarga recepcoesDocumentoCarga = new RecepcoesDocumentoCarga();

//		for (int i=0; i < dues.length; i++) {
					
			doc = documentoRepository.findByNumero(due);
			if (doc != null) {
				itens = doc.getItens();
				if (itens != null) {			
					RecepcaoDocumentoCarga recepcaoDocumentoCarga = new RecepcaoDocumentoCarga();
					recepcaoDocumentoCarga.setIdentificacaoRecepcao("RECDUE|" + cct.getNumeroCont() + new Date().getTime());
					recepcaoDocumentoCarga.setCnpjResp(PropertiesUtil.getProp().getProperty("siscomex.cnpj-cert"));
					
					Local local = new Local();
					local.setCodigoURF(LocalEnum.PORTO_DE_RJ.value());
					local.setCodigoRA(LocalEnum.LIBRA_TERMINAL_RJ.value());
					recepcaoDocumentoCarga.setLocal(local);		
					
					setEntregadorDue(cct, recepcaoDocumentoCarga);
					
					setDocumentoDue(cct, recepcaoDocumentoCarga);		
					
					BigDecimal peso;
					try {
						peso = new BigDecimal(itens.get(0).getPeso()).setScale(3);
					}
					catch (NumberFormatException e)
					{
						peso = new BigDecimal("0.001");
					}
					
					recepcaoDocumentoCarga.setPesoAferido(peso);
			
					setVeiculoRodoviarioDue(cct, recepcaoDocumentoCarga);
			
					recepcoesDocumentoCarga.setRecepcaoDocumentoCarga(recepcaoDocumentoCarga);
	//				break;
				}
			}
		//}
		return recepcoesDocumentoCarga;
	}
	
	
	private void setEntregadorDue(Cct cct, RecepcaoDocumentoCarga recepcaoDocumentoCarga) {
		Entregador entregador = new Entregador();
		if (cct.getEntregador() != null) {
			if (cct.getEntregador().getNome() != null) {
				entregador.setNomeEstrangeiro(cct.getEntregador().getNome().substring(0, 59));
			} else {				
				if(cct.getEntregador().getCnpj() != null){
					entregador.setCnpj(cct.getEntregador().getCnpj());
				} else {
					entregador.setCpf(cct.getCpfTransportador() != null && !"".equals(cct.getCpfTransportador()) ? cct.getCpfTransportador() : cct.getCpfCondutor() != null && !"".equals(cct.getCpfCondutor()) ? cct.getCpfCondutor() : "");
				}
			}
		}
		recepcaoDocumentoCarga.setEntregador(entregador);
	}
	
	private void setDocumentoDue(Cct cct, RecepcaoDocumentoCarga recepcaoDocumentoCarga) {
		List<documento> documentos = new ArrayList<>();
		String[] dues = null;
		String due = null;
		if(cct.getNrodue().contains(",")) {
			dues = cct.getNrodue().split(",");
		}else {
			due = cct.getNrodue();
		}

	//	for (int i=0; i < dues.length; i++) {
		
			documento documento_ = new documento();
			documento_.setNumeroDUE(due);		
		
			int qtd;
			try {
				qtd = Integer.parseInt(itens.get(0).getQuantidade());
			}
			catch (NumberFormatException e)
			{
				qtd = 0;
			}
			
			Carga cargaVeiculo = new Carga();	
			cargaVeiculo.setTotal(qtd);
			cargaVeiculo.setQuantidade(qtd);
			List<Carga> cargasVeiculo = new ArrayList<>();
			cargasVeiculo.add(cargaVeiculo);
			documento_.setCargaSoltaVeiculo(cargasVeiculo); 
			
			/*carga cargaGranel = new carga();
			cargaGranel.setTipoGranel(10);
			cargaGranel.setTotal(itens.get(0).getPeso());
			cargaGranel.setQuantidade(itens.get(0).getPeso());
			List<carga> cargasGranel = new ArrayList<>();
			cargasGranel.add(cargaGranel);
			documento_.setGranel(cargasGranel);*/
			
			documentos.add(documento_);
		//}
		
		recepcaoDocumentoCarga.setDocumentos(documentos);

	}
	
	
	public static String currencyFormat(BigDecimal n) {
	    return NumberFormat.getCurrencyInstance().format(n);
	}

	private void setVeiculoRodoviarioDue(Cct cct, RecepcaoDocumentoCarga recepcaoDocumentoCarga) {
		TransitoSimplificadoRecepcao transitoSimplificado = new TransitoSimplificadoRecepcao();
		VeiculoRodoviario veiculoRodoviario = new VeiculoRodoviario();
		if(cct.getTipoTransportador() == 1) {
			veiculoRodoviario.setNomeCondutorEstrangeiro(cct.getNomeCondutor());
			veiculoRodoviario.setDocumentoCondutorEstrangeiro(cct.getCpfCondutor());
		} else {
			veiculoRodoviario.setCpfCondutor(cct.getCpfCondutor());
		}
		Veiculos veiculos = new Veiculos();
		List<TVeiculoTransitoSimplificado> veiculosList = new ArrayList<>();
		TVeiculoTransitoSimplificado veiculo = new TVeiculoTransitoSimplificado();
		veiculo.setPlaca(cct.getPlacaveiculo());		
		
		veiculosList.add(veiculo);
		veiculos.getVeiculo().addAll(veiculosList);
		veiculoRodoviario.setVeiculos(veiculos);
		transitoSimplificado.setVeiculoRodoviario(veiculoRodoviario);
		recepcaoDocumentoCarga.setTransitoSimplificadoRecepcao(transitoSimplificado);
	}

	@Override
	public RecepcoesDocumentoCarga getXMLRecepcaoDANFECargaSolta(Cct cct) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
