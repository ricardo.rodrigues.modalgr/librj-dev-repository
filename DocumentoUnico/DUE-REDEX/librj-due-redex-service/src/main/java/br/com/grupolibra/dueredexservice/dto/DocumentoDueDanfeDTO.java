package br.com.grupolibra.dueredexservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoDueDanfeDTO {
		
	private long id_documento;
	
	public String danfe;
	
	
}
