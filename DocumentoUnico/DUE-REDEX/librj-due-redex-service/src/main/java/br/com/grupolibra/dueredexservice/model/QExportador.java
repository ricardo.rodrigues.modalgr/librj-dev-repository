package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QExportador is a Querydsl query type for Exportador
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QExportador extends EntityPathBase<Exportador> {

    private static final long serialVersionUID = -872290560L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QExportador exportador = new QExportador("exportador");

    public final NumberPath<Integer> bloqueadoSiscomex = createNumber("bloqueadoSiscomex", Integer.class);

    public final QCct cct;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nrodue = createString("nrodue");

    public final StringPath numero = createString("numero");

    public final NumberPath<Integer> situacaoDUE = createNumber("situacaoDUE", Integer.class);

    public final StringPath tipo = createString("tipo");

    public final NumberPath<Integer> validado = createNumber("validado", Integer.class);

    public QExportador(String variable) {
        this(Exportador.class, forVariable(variable), INITS);
    }

    public QExportador(Path<? extends Exportador> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QExportador(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QExportador(PathMetadata metadata, PathInits inits) {
        this(Exportador.class, metadata, inits);
    }

    public QExportador(Class<? extends Exportador> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cct = inits.isInitialized("cct") ? new QCct(forProperty("cct"), inits.get("cct")) : null;
    }

}

