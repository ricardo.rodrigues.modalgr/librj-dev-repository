package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

import br.com.grupolibra.dueredexservice.model.Documento;

@Repository
public interface DocumentoRepository extends CrudRepository<Documento, Long>, QuerydslPredicateExecutor<Documento> {
		
	public List<Documento> findByCnpjTransportadoraAndTipo(String cnpjTransportadora, String tipo);
	
	public List<Documento> findByCnpjTransportadora(String cnpjTransportadora);
	
	public Documento findByNumero(String numero);
	
	public Documento findByDanfe (String danfe);
	
	public List<Documento> findAll(Predicate predicate);
	
	public Documento findByIdAndDanfe(Long id, String danfe);
	
	public List<Documento> findByTransporteId(Long transporteId);
	
	public Documento findByNumeroConteiner (String numeroConteiner);
	
	public List<Documento> findByTipo(String tipo);	
}
