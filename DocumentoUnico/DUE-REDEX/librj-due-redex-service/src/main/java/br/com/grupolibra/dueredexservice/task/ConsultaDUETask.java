package br.com.grupolibra.dueredexservice.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.Lists;

import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosDueResponseListDTO;
import br.com.grupolibra.dueredexservice.dto.ConsultaDadosResumidosResponseListDTO;
import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Item;
import br.com.grupolibra.dueredexservice.repository.BDRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.TransporteRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.service.EnvioSiscomexService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.util.ParadaProgramadaUtil;

@Component
public class ConsultaDUETask {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final long POLLING_RATE_MS = 300000;

	@Autowired
	private CctService cctService;

	@Autowired
	private SiscomexService siscomexService;

	@Autowired
	private EnvioSiscomexService envioSiscomexService;
	
	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private BDRepository bdRepository;
	
	@Autowired
	private DocumentoRepository documentoRepository;

	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void consultaDUE() {
		logger.info("###### INICIO TASK CONSULTA DUE");
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMADA");
			return;
		}
		
		List<Documento> doc = bdRepository.getDueWithoutTotal();		
		if (doc == null && doc.isEmpty()) {
			logger.info("consultaDUE - Sem registros com cadastro de DUE");
			return;
		}
		
		logger.info("consultaDUE - qtd registros tipo ITEM : {} ", doc.size());							
		ConsultaDadosDueResponseListDTO consultaDadosDueResponseListDTO = consultaDUEPorLote(doc);
	}

	private ConsultaDadosDueResponseListDTO consultaDUEPorLote(List<Documento> ListDocs) {
		logger.info("consultaDUEPorLote");
		ConsultaDadosDueResponseListDTO resultList = new ConsultaDadosDueResponseListDTO();		  
		
		List<List<Documento>> lotes = Lists.partition(ListDocs, 40);
		logger.info("consultaDUEPorLote - QTD LISTAS PARTICIONADAS -> {} ", lotes.size());
		int count = 1;
		String dues = "";
		Iterator<List<Documento>> loteIterator = lotes.iterator();
		while(loteIterator.hasNext()) {
			List<Documento> lote = loteIterator.next();

			logger.info("consultaDUEPorLote - DENTRO DO FOR - LACO {} ", count);
			logger.info("consultaDUEPorLote - QTD DESTE LOTE - {} ", lote.size());

			if (lote.size()>1) {
				for (Documento doc: lote) 
					dues+= doc.getNumero() + ",";
			}else {
				dues=lote.get(0).getNumero();
			}
			
			resultList = siscomexService.consultaDadosDue(dues);
			count++;
			logger.info("consultaDUEPorLote - FIM DO LACO {} ", count);
		}
		
		return resultList;
	}
}
