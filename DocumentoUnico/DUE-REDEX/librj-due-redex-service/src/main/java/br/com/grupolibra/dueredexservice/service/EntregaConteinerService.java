package br.com.grupolibra.dueredexservice.service;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.siscomex.EntregasConteineres;

public interface EntregaConteinerService {
	
	public EntregasConteineres getXMLEntregaConteinerNFE(Cct cct);
	
	public EntregasConteineres getXMLEntregaConteinerTransbordoMaritimo(Cct cct);
	
	EntregasConteineres getXMLEntregaConteinerDUE(Cct cct);
	
}
