package br.com.grupolibra.dueredexservice.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoGridDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoRequestDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.QDocumento;
import br.com.grupolibra.dueredexservice.model.QTransporte;
import br.com.grupolibra.dueredexservice.model.Shipper;
import br.com.grupolibra.dueredexservice.model.Transporte;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.ItemRepository;
import br.com.grupolibra.dueredexservice.repository.ShipperRepository;
import br.com.grupolibra.dueredexservice.repository.TransporteRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.response.CETNcm;
import br.com.grupolibra.dueredexservice.response.GridNcm;
import br.com.grupolibra.dueredexservice.service.CadastroDocumentoService;
import br.com.grupolibra.dueredexservice.service.EmailService;
import br.com.grupolibra.dueredexservice.service.SiscomexService;
import br.com.grupolibra.dueredexservice.response.Embalagem;
import br.com.grupolibra.dueredexservice.response.GridEmbalagem;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Service
public class CadastroDocumentoServiceImpl implements CadastroDocumentoService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private N4Repository n4Repository;

	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private TransporteRepository transporteRepository;
	
	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private ShipperRepository shipperRepository;
	
	@Autowired
	private EmailService emailService; 
	
	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";
	
	private static final String DUE = "DUE";

	@Autowired
	private SiscomexService siscomexService;

	@Override
	public boolean existeCadastroShipper(String cnpj) {
		return n4Repository.existeCadastroShipper(cnpj);
	}

	@Override
	public CadastroDocumentoDTO salvaDocumento(Documento dto) {

		logger.info("salvaDocumento - Documento {} ", dto);
		logger.info("salvaDocumento - ITEM {} ", dto.getItens().size());
		logger.info("salvaDocumento - EMITENTE {} ", dto.getEmitente());
		logger.info("salvaDocumento - CONSOLIDADOR {} ", dto.getConsolidador());

		CadastroDocumentoDTO retorno = new CadastroDocumentoDTO();
		
		if (dto.getId() == null) {
			if (documentoRepository.findByNumero(dto.getNumero()) != null) {
				retorno.setErro(true);
				if ("NF".equals(dto.getTipo())) {
					retorno.setMensagem("Nota Fiscal j� cadastrada no sistema.");
				}
				if ("ITEM".equals(dto.getTipo())) {
					retorno.setMensagem("N�mero DUE j� cadastrada no sistema.");
				}
				return retorno;
			}

			if("NF".equals(dto.getTipo()) && n4Repository.getBLGkeyByBlNbr(dto.getEmitente().getCnpj().substring(0, 8) + dto.getNumero())!= null){
				logger.info("Verifica se existe no N4 {}", dto.getEmitente().getCnpj().substring(0, 8) + dto.getNumero());
				retorno.setErro(true);
				retorno.setMensagem("Nota Fiscal j� cadastrada no sistema.");
				return retorno;
			}
			
			if("ITEM".equals(dto.getTipo()) && n4Repository.getBLGkeyByBlNbr(dto.getNumero())!= null){
				logger.info("Verifica se existe no N4 {}", dto.getNumero());
				retorno.setErro(true);
				retorno.setMensagem("N�mero DUE j� cadastrada no sistema.");
				return retorno;
			}
			
//			if("ITEM".equals(dto.getTipo()) && siscomexService.consultaDadosDue(dto.getNumero())!= null){
//				logger.info("Verifica se existe no Portal Siscomex  {}", dto.getNumero());
//				retorno.setErro(true);
//				retorno.setMensagem("DUE j� cadastrada na RFB.");
//				return retorno;
//			}
			
		}
		
		if(dto.getId() == null) {
			if (dto.getDanfe() != null && documentoRepository.findByDanfe(dto.getDanfe()) != null) {
				retorno.setErro(true);
				retorno.setMensagem("Chave DANFE j� cadastrada no sistema.");
				return retorno;
			}
		} else {
			if (dto.getDanfe() != null) {
				Documento docAux = documentoRepository.findByIdAndDanfe(dto.getId(), dto.getDanfe());
				if(docAux == null) {
					if(documentoRepository.findByDanfe(dto.getDanfe()) != null) {
						retorno.setErro(true);
						retorno.setMensagem("Chave DANFE j� cadastrada no sistema.");
						return retorno;
					}
				} else {
					if(!dto.getDanfe().equals(docAux.getDanfe())) {
						if(documentoRepository.findByDanfe(dto.getDanfe()) != null) {
							retorno.setErro(true);
							retorno.setMensagem("Chave DANFE j� cadastrada no sistema.");
							return retorno;
						}
					}
				}
			}
		}
		
		String event = "";
		String notes = "";
		
		if ("NF".equals(dto.getTipo())) {
			dto.setBlNbr(dto.getEmitente().getCnpj().substring(0, 8) + dto.getNumero());
			event = "BL_VINC_DANFE";
			notes = dto.getDanfe();
		}
		if ("ITEM".equals(dto.getTipo())) {
			dto.setBlNbr(dto.getNumero());
			dto.setBlNotes(DUE); 
			event = "BL_VINC_DUE";
			notes = dto.getNumero();
		}
		
		String diversos = "DIVERSOS";
		String outros = "0000";			
		String codEmbalagem = dto.getItens().get(0).getEmbalagemId();
		if(dto.getItens().get(0).getEmbalagemId() != null){
			Object[] result = n4Repository.getPackageTypesById(dto.getItens().get(0).getEmbalagemId());
			if(result != null) {		
				dto.getItens().get(0).setEmbalagemId(result[0] != null ? result[0].toString() : "");
				dto.getItens().get(0).setEmbalagem(result[1] != null ? result[1].toString() : "");
			}else {
				retorno.setErro(true);
				retorno.setMensagem("C�digo da embalagem inv�lido.");
				return retorno;
			}
		}else {
			dto.getItens().get(0).setEmbalagem("");
		}
		
//		if (dto.getItens().get(0).getNcmId() != null ) {
//			Object [] result = n4Repository.getCommoditiesById(dto.getItens().get(0).getNcmId());		
//			if (result == null) 
//				result = n4Repository.getCommoditiesById(outros);
//				
//				dto.getItens().get(0).setNcm(result[1] != null ? result[1].toString() : "");
//		}else {		
//			dto.getItens().get(0).setNcm("");			
//		}
		
		Object[] retornoN4 = null;
		
		if(dto.getId() == null) {
			retornoN4 = n4Repository.geraBillOfLadingDoDocumento(dto);
			logger.info("salvaDocumento - geraBillOfLadingDoDocumento OK {}", retornoN4);
		}
		
		if (dto.getId() != null || "OK".equals(retornoN4[1].toString())) {
			
			logger.info("salvaDocumento - geraBillOfLadingDoDocumento OK");

			if (!dto.getEmitente().isNovo()) {
				Optional<Shipper> emitente = shipperRepository.findByCnpj(dto.getEmitente().getCnpj());
				if (emitente.isPresent()) {
					dto.setEmitente(emitente.get());
				} else {	
					Object[] o = n4Repository.getShipperDocAndNameById(dto.getEmitente().getCnpj());
					if(o != null && o[1] != null){	
						Shipper a = new Shipper();
						a.setCnpj(dto.getEmitente().getCnpj());
						a.setRazaoSocial(o[1].toString());
						dto.setEmitente(shipperRepository.save(a));
					}
					else {
						logger.info("Erro Emitente");
					}
				}
			} else {
				dto.setEmitente(shipperRepository.save(dto.getEmitente()));
				sendEmailNewShipper(dto.getEmitente(), dto.getCnpjTransportadora());
			}

			if(dto.getConsolidador() != null && dto.getConsolidador().getCnpj() != null) {
				if (!dto.getConsolidador().isNovo()) {
					Optional<Shipper> consolidador = shipperRepository.findByCnpj(dto.getConsolidador().getCnpj());
					if (consolidador.isPresent()) {
						dto.setConsolidador(consolidador.get());
					} else {
						Object[] o = n4Repository.getShipperDocAndNameById(dto.getConsolidador().getCnpj());
						if(o != null && o[1] != null){	
							Shipper a = new Shipper();
							a.setCnpj(dto.getConsolidador().getCnpj());
							a.setRazaoSocial(o[1].toString());
							dto.setConsolidador(shipperRepository.save(a));
						}
						else {
							logger.info("Erro Consolidador");
						}
					}
				} else {
					dto.setConsolidador(shipperRepository.save(dto.getConsolidador()));
					sendEmailNewShipper(dto.getConsolidador(), dto.getCnpjTransportadora());
				}
			} else {
				dto.setConsolidador(null);
			}
	
			if (dto.getBlGkey()==null ) {
				dto.setBlGkey(n4Repository.getBLGkeyByBlNbr(dto.getBlNbr()));
				String retornoEvento = "";
				if(dto.getId() == null) {
					retornoEvento = n4Repository.createBLEvent(dto.getBlGkey(), event, notes);
				}	
				if ("OK".equals(retornoEvento)) 
					logger.info("salvaDocumento - createBLEvent OK");				
			}

			dto.setDataCadastro(new Date(System.currentTimeMillis()));			
			if (dto.getNumeroConteiner()==null) 
				if (retornoN4!=null)
					dto.setNumeroConteiner(retornoN4[0].toString());

			dto.getItens().get(0).setEmbalagemId(codEmbalagem);

			dto.getItens().get(0).setInfoAdicional(limitaQuantidadeCaracteres(dto.getItens().get(0).getInfoAdicional()));
			Documento aux = documentoRepository.save(dto);
			dto.getItens().get(0).setDocumento(aux);
			itemRepository.save(dto.getItens().get(0));
			
			if (dto.getId() != null) {
				logger.info("salvaDocumento - OK");
				retorno.setDocumento(aux);
				retorno.setErro(false);
				retorno.setMensagem("Documento salvo com sucesso.");
			} else {
				logger.info("salvaDocumento - ERRO N4 createBLEvent");
				retorno.setDocumento(dto);
				retorno.setErro(true);
				retorno.setMensagem("ERRO ao criar eventos.");
			}

		} else {
			logger.info("salvaDocumento - ERRO N4 geraBillOfLadingDoDocumento");
			retorno.setDocumento(dto);
			retorno.setErro(true);
			if (retornoN4!=null) retorno.setMensagem("ERRO ao criar BillOfLading - " + retornoN4[1]);
		}

		return retorno;
	}	

	@Override
	public GridEmbalagem buscaEmbalagens() {
		
		List<Object[]> retorno = n4Repository.buscaEmbalagens();
		
		if (retorno==null) return null;
		
		List<Embalagem> retList = new ArrayList<Embalagem>();
		for (int i=0, n=retorno.size(); i < n; i++) {
			Embalagem itemCet = new Embalagem();
			
			if (retorno.get(i)[0]!=null) 
				itemCet.setCodigo(retorno.get(i)[0].toString());

			if (retorno.get(i)[1]!=null) 
				itemCet.setDescricao(retorno.get(i)[1].toString());			

			retList.add(itemCet);	
		}				
		
		GridEmbalagem retornoTip = new GridEmbalagem();
		retornoTip.setListaEmbalagemResponses(retList);
		
		return retornoTip;	
	}

	@Override
	public GridNcm buscaCommodities() {
				
		List<Object[]> retorno=n4Repository.buscaCommodities();
		
		if (retorno==null) return null;
		
		List<CETNcm> retList = new ArrayList<CETNcm>();
		for (int i=0, n=retorno.size(); i < n; i++) {
			CETNcm itemCet = new CETNcm();
			
			if (retorno.get(i)[0]!=null) 
				itemCet.setCodigo(retorno.get(i)[0].toString());

			if (retorno.get(i)[1]!=null) 
				itemCet.setDescricao(retorno.get(i)[1].toString());			

			retList.add(itemCet);	
		}				
		
		GridNcm retornoTip = new GridNcm();
		retornoTip.setListaNcmResponses(retList);
		
		return retornoTip;		
		
	}

	@Override
	public String excluirDocumento(Documento documento) {
		logger.info("deleteDocument - {}", documento);
		if(documento.getTransporte() != null && documento.getTransporte().getId() != null){
			return "Erro - Não possivel exluir um documento vinculado a um recibo.";
		}
		if(n4Repository.docHasUnitInGate(documento.getBlGkey())) {
			return "Erro - Não possivel exluir um documento que possui entrada no GATE.";
		}
		String retornoN4 = n4Repository.deleteDocument(documento.getBlGkey());
		if("OK".equals(retornoN4)){
			logger.info("deleteDocument - RETORNO N4 Ok");
			documentoRepository.delete(documento);
			return "Documento exclu�do com sucesso!";
		}else {
			logger.info("deleteDocument - RETORNO N4 ERRO - {}", retornoN4);
			return "Ocorreu um erro ao excluir o documento, por favor tente novamente, se o problema persistir contacte o administrador do sistema.";
		}
	}
	
	
	private String sendEmailNewShipper(Shipper shipper, String cnpjTransportadora) {
		logger.info("sendEmailNewShipper");
		
		String retorno = "";
		String to = PropertiesUtil.getProp().getProperty("mail.shipper.to");
		String subject = PropertiesUtil.getProp().getProperty("mail.shipper.subject");
		
		StringBuilder sb = new StringBuilder();
		sb.append("Inclus�o de registro (Shippers&Consignees - N4) para solicita��o de documentos, procura��o e cadastro SAP:");
		sb.append("<br>");
		sb.append("CNPJ: " + shipper.getCnpj());
		sb.append("<br>");
		sb.append("Razão Social: " + shipper.getRazaoSocial());
		sb.append("<br>");
		sb.append("Nome Contato: " + shipper.getNomeContato());
		sb.append("<br>");
		sb.append("Telefone: " + shipper.getTelefone());
		sb.append("<br>");
		
		sb.append("Transportador: " + cnpjTransportadora);
		sb.append("<br>");
		sb.append("DtHr cadastro: " + new Date());
		sb.append("<br>");
		
		String mensagem = sb.toString();

		logger.info("sendEmailNewShipper - PARA {} ", to);
		logger.info("sendEmailNewShipper - ASSUNTO {} ", subject);
		logger.info("sendEmailNewShipper - MENSAGEM {} ", mensagem);
		
		retorno = emailService.sendSimpleMessage(to, subject, mensagem);
		
		return retorno;
	}
		
	public String limitaQuantidadeCaracteres(String notes) {
		if(notes != null) {
			if(notes.length() >= 3990) {
				return notes.substring(0,  3990) + " ...";
			} else {
				return notes;
			}
		} else {
			return null;
		}
	}

	@Override
	public CadastroDocumentoDTO validaModificacaoDocumento(Documento dto) {
		
		CadastroDocumentoDTO retorno = new CadastroDocumentoDTO();
		retorno.setErro(false);
		
		if(n4Repository.docHasUnitInGate(dto.getBlGkey())) {
			retorno.setErro(true);
			retorno.setMensagem("Documento possui entrada no GATE.");
		}
		
		QTransporte qTransp = QTransporte.transporte;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(qTransp.documentos.contains(dto));
		List<Transporte> lstAux = (List<Transporte>) transporteRepository.findAll(builder.getValue());
		if(lstAux != null && !lstAux.isEmpty()) {
			retorno.setErro(true);
			retorno.setMensagem("Documento esta vinculado a um recibo.");
		}
		
		return retorno;
	}
	
	@Override
	public DocumentoDTO buscaDocumentosTransportadora(DocumentoRequestDTO dto) {		
		DocumentoDTO retorno = new DocumentoDTO();
		retorno.setCurrentPage(Integer.parseInt(dto.getPageIndex()));
		
		QDocumento qdocumento = QDocumento.documento;		
		BooleanBuilder builder = new BooleanBuilder();
		long numRegistros = 0L;
		builder.and(qdocumento.cnpjTransportadora.eq(dto.getCnpj()));
		
		switch (dto.getTipo()) {
		case "1":
			 builder.and(qdocumento.tipo.eq("NF"));
			 break;

		case "2":
			builder.and(qdocumento.tipo.eq("ITEM"));
			 break;
		
		default:
			break;
		}
		
		getBuilderFilterText(builder, qdocumento, dto);
		
		numRegistros = documentoRepository.count(builder.getValue());
		
		Page<Documento> docList = documentoRepository.findAll(builder.getValue(), PageRequest.of(Integer.parseInt(dto.getPageIndex()) -1, Integer.parseInt(dto.getPageSize())));

		retorno.setQtdRegistros(numRegistros);
		
		retorno.setListaDocumento(docList.getContent());
		return retorno;

	}

	private void getBuilderFilterText(BooleanBuilder builder, QDocumento qdocumento, DocumentoRequestDTO dto) {
		if(dto.getBooking() != null && !"".equals(dto.getBooking())) {
			builder.and(qdocumento.booking.equalsIgnoreCase(dto.getBooking()));
		}		 
		if(dto.getDanfe() != null && !"".equals(dto.getDanfe())) {
			builder.and(qdocumento.danfe.equalsIgnoreCase(dto.getDanfe()));
		}
		if(dto.getDue() != null && !"".equals(dto.getDue())) { // DUE 
			builder.and(qdocumento.numero.equalsIgnoreCase(dto.getDue()));
		}
		
		if(dto.getNumero() != null && !"".equals(dto.getNumero())){ // Numero NF
			builder.and(qdocumento.numero.equalsIgnoreCase(dto.getNumero()));
		}
		
		if(dto.getConsolidador() != null && !"".equals(dto.getConsolidador())){
			builder.and(qdocumento.consolidador.cnpj.equalsIgnoreCase(dto.getConsolidador()));
		}
		
		if(dto.getEmitente() != null && !"".equals(dto.getEmitente())){
			builder.and(qdocumento.emitente.cnpj.equalsIgnoreCase(dto.getEmitente()));
		}
		
		if(dto.getDataCadastroDe() != null && !"".equals(dto.getDataCadastroDe()) && dto.getDataCadastroAte() != null && !"".equals(dto.getDataCadastroAte())){			
			try {
				builder.and(qdocumento.dataCadastro.between(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataCadastroDe()), new SimpleDateFormat("dd/MM/yyyy").parse(dto.getDataCadastroAte())));
			} catch (Exception e) {
				logger.info("getBuilderFilterText - DATA CADASTRO PARSE ERROR");
			}
		}
		
		if(dto.getEmbalagemNfCodigo() != null && !"".equals(dto.getEmbalagemNfCodigo())){
			builder.and(qdocumento.itens.any().embalagemId.equalsIgnoreCase(dto.getEmbalagemNfCodigo()));
		}
		
		if(dto.getNcmNfCodigo() != null && !"".equals(dto.getNcmNfCodigo())){
			builder.and(qdocumento.itens.any().ncmId.equalsIgnoreCase(dto.getNcmNfCodigo()));
		}
		
		
	}


}
