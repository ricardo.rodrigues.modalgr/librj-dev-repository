package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDuedat is a Querydsl query type for Duedat
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDuedat extends EntityPathBase<Duedat> {

    private static final long serialVersionUID = -325204132L;

    public static final QDuedat duedat = new QDuedat("duedat");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> due_id = createNumber("due_id", Long.class);
    public final NumberPath<Long> dat_id = createNumber("dat_id", Long.class);

    public QDuedat(String variable) {
        super(Duedat.class, forVariable(variable));
    }

    public QDuedat(Path<? extends Duedat> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDuedat(PathMetadata metadata) {
        super(Duedat.class, metadata);
    }

}

