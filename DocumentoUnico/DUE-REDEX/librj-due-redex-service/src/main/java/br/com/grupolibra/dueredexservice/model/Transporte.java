package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;

@Entity
@Table(name = "TRANSPORTE")
@NamedQuery(name = "Transporte.findAll", query = "SELECT t FROM Transporte t")
public class Transporte implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_TRANSPORTE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRANSPORTE_SEQ")
	@SequenceGenerator(name = "TRANSPORTE_SEQ", sequenceName = "TRANSPORTE_SEQ", allocationSize = 1)
	private Long id;

	private String cpf;

	private String cnh;

	private String passaporte;

	private String nome;

	private String cavalo;

	private String reboque1;

	private String reboque2;

	@Column(name = "CNPJ_TRANSPORTADORA")
	private String cnpjTransportadora;
	
	@Transient
	private String nomeTransportadora;

	@OneToMany(mappedBy = "transporte", fetch = FetchType.EAGER, targetEntity = Documento.class)
	private List<Documento> documentos;
	
	@Transient
	private boolean novoCadastroMotorista;
	
	@Transient
	private List<DocumentoRecibo> documentosRecibo;

	public Long getId() {
		return id;
	}

	public String getCpf() {
		return cpf;
	}

	public String getCnh() {
		return cnh;
	}

	public String getPassaporte() {
		return passaporte;
	}

	public String getNome() {
		return nome;
	}

	public String getCavalo() {
		return cavalo;
	}

	public String getReboque1() {
		return reboque1;
	}

	public String getReboque2() {
		return reboque2;
	}

	public String getCnpjTransportadora() {
		return cnpjTransportadora;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCavalo(String cavalo) {
		this.cavalo = cavalo;
	}

	public void setReboque1(String reboque1) {
		this.reboque1 = reboque1;
	}

	public void setReboque2(String reboque2) {
		this.reboque2 = reboque2;
	}

	public void setCnpjTransportadora(String cnpjTransportadora) {
		this.cnpjTransportadora = cnpjTransportadora;
	}

	public boolean getNovoCadastroMotorista() {
		return novoCadastroMotorista;
	}

	public void setNovoCadastroMotorista(boolean novoCadastroMotorista) {
		this.novoCadastroMotorista = novoCadastroMotorista;
	}
	
	

	@Override
	public String toString() {
		return "Transporte [id=" + id + ", cpf=" + cpf + ", cnh=" + cnh + ", passaporte=" + passaporte + ", nome=" + nome + ", cavalo=" + cavalo + ", reboque1=" + reboque1 + ", reboque2=" + reboque2 + ", cnpjTransportadora=" + cnpjTransportadora + ", documentos=" 
				+ (documentos != null ? documentos.size() : "null") + "]";
	}

	public List<Documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}

	public String getNomeTransportadora() {
		return nomeTransportadora;
	}

	public void setNomeTransportadora(String nomeTransportadora) {
		this.nomeTransportadora = nomeTransportadora;
	}

	public List<DocumentoRecibo> getDocumentosRecibo() {
		return documentosRecibo;
	}

	public void setDocumentosRecibo(List<DocumentoRecibo> documentosRecibo) {
		this.documentosRecibo = documentosRecibo;
	}
}