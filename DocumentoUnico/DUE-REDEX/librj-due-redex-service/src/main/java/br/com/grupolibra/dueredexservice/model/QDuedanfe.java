package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDuedat is a Querydsl query type for Duedanfe
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDuedanfe extends EntityPathBase<Duedanfe> {

    private static final long serialVersionUID = -325204132L;

    public static final QDuedanfe duedanfe = new QDuedanfe("duedanfe");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> due_id = createNumber("due_id", Long.class);
    public final NumberPath<Long> danfe_id = createNumber("danfe_id", Long.class);

    public QDuedanfe(String variable) {
        super(Duedanfe.class, forVariable(variable));
    }

    public QDuedanfe(Path<? extends Duedanfe> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDuedanfe(PathMetadata metadata) {
        super(Duedanfe.class, metadata);
    }

}

