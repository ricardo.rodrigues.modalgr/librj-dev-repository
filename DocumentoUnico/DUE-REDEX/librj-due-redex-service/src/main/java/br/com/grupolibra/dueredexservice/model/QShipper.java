package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QShipper is a Querydsl query type for Shipper
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QShipper extends EntityPathBase<Shipper> {

    private static final long serialVersionUID = -1819534565L;

    public static final QShipper shipper = new QShipper("shipper");

    public final StringPath cnpj = createString("cnpj");

    public final StringPath email = createString("email");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nomeContato = createString("nomeContato");

    public final StringPath razaoSocial = createString("razaoSocial");

    public final StringPath telefone = createString("telefone");

    public QShipper(String variable) {
        super(Shipper.class, forVariable(variable));
    }

    public QShipper(Path<? extends Shipper> path) {
        super(path.getType(), path.getMetadata());
    }

    public QShipper(PathMetadata metadata) {
        super(Shipper.class, metadata);
    }

}

