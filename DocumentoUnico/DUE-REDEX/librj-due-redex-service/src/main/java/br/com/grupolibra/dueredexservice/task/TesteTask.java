package br.com.grupolibra.dueredexservice.task;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.util.ParadaProgramadaUtil;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;
import br.com.grupolibra.fmwPortService.genericInvoke.N4ServiceInvoke;
import br.com.grupolibra.fmwPortService.genericInvoke.ServiceParam;
import br.com.grupolibra.fmwPortService.helper.logger.LoggerN4;

//@Component
public class TesteTask {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
//	private static final long POLLING_RATE_MS = 300000;
	
	private static final String ACTION = "action";
	
	private static final String CREATE_UNIT_EVENT_SERVICE_INTERFACE = "UnitEventServiceProxy";

	private static final String CREATE_UNIT_EVENT_SERVICE = "createUnitEvent";

	private static final String UNIT_GKEY = "unitGkey";
	
	private static final String EVENT = "event";
	
	private static final String RECEPCAO_NFE_UNIT_EVENT = "UNIT_RECEP_DANFE";
	
	private static final String NOTES = "notes";
	
	public static final String LOG_N4 = LoggerN4.ERROR;
	
	@Autowired
	@Qualifier("n4ServiceInvoke")
	private N4ServiceInvoke n4ServiceInvoke;
	
//	@Autowired
//	private N4Repository n4Repository;
	
//	@Scheduled(fixedRate = POLLING_RATE_MS)
	public void testeTask() {
		
		if(ParadaProgramadaUtil.isInPeriodoParadaProgramada()) {
			logger.info("###### PARADA PROGRAMDA");
			return;
		}
		
		try {
			
			logger.info("############# testeTask ");
			
			logger.info("############# testeTask - n4.url {} ", PropertiesUtil.getProp().getProperty("n4.url").trim());
			logger.info("############# testeTask - n4.username {} ", PropertiesUtil.getProp().getProperty("n4.username").trim());
			logger.info("############# testeTask - n4.password {} ", PropertiesUtil.getProp().getProperty("n4.password").trim());
			
			String notes = "TESTE DE ACESSO N4";
			
			Cct cct = new Cct();
			cct.setIdExterno(new BigDecimal("320834192"));
			cct.setNrodue("TESTE");
			
			ServiceParam param = new ServiceParam();
			param.addParameter(ACTION, CREATE_UNIT_EVENT_SERVICE);
			param.addParameter(UNIT_GKEY, "316140320"); // KANE0000060
			param.addParameter(EVENT, RECEPCAO_NFE_UNIT_EVENT);
			param.addParameter(NOTES, notes);
			String retorno = n4ServiceInvoke.executeToObject(CREATE_UNIT_EVENT_SERVICE_INTERFACE, param, String.class, LOG_N4);
			
//			ServiceParam param = new ServiceParam();
//			param.addParameter("action", "createDesbloqueioEvents");
//			param.addParameter("unitGkey", "320834192");
//			param.addParameter("notes", notes);
//			param.addParameter("events_to_create", "LIBERACAO_DE_EXPORTACAO, INCLUSAO_DOC_DESPACHO");
//			String retorno = n4ServiceInvoke.executeToObject("UnitEventServiceProxy", param, String.class, LoggerN4.ERROR.toString());
			
			logger.error("############# RETORNO -> {} ", retorno);
			
//		} catch (MalformedURLException | RemoteException | N4ServiceInvokeException | ServiceException e) {
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("############# ERRO -> {} ", e.getMessage());
		}
	}

	
}
