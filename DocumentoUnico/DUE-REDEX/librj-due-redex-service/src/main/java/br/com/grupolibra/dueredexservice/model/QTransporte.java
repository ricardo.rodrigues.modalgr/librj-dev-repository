package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTransporte is a Querydsl query type for Transporte
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTransporte extends EntityPathBase<Transporte> {

    private static final long serialVersionUID = 1495277154L;

    public static final QTransporte transporte = new QTransporte("transporte");

    public final StringPath cavalo = createString("cavalo");

    public final StringPath cnh = createString("cnh");

    public final StringPath cnpjTransportadora = createString("cnpjTransportadora");

    public final StringPath cpf = createString("cpf");

    public final ListPath<Documento, QDocumento> documentos = this.<Documento, QDocumento>createList("documentos", Documento.class, QDocumento.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nome = createString("nome");

    public final StringPath passaporte = createString("passaporte");

    public final StringPath reboque1 = createString("reboque1");

    public final StringPath reboque2 = createString("reboque2");

    public QTransporte(String variable) {
        super(Transporte.class, forVariable(variable));
    }

    public QTransporte(Path<? extends Transporte> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTransporte(PathMetadata metadata) {
        super(Transporte.class, metadata);
    }

}

