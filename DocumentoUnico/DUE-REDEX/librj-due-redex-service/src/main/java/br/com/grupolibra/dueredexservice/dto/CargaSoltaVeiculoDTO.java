package br.com.grupolibra.dueredexservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CargaSoltaVeiculoDTO {
	
	private Integer tipoEmbalagem;
	
	private Integer quantidade;
		
}
