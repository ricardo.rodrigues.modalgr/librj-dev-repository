package br.com.grupolibra.dueredexservice.model.siscomex;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("recepcoesDocumentoCarga")
public class RecepcoesDocumentoCarga {

	@XStreamAlias("xsi:schemaLocation")
	@XStreamAsAttribute
	final String schemaLocation = "http://www.pucomex.serpro.gov.br/cct RecepcaoDocumentoCarga.xsd";

	@XStreamAsAttribute
	final String xmlns = "http://www.pucomex.serpro.gov.br/cct";

	@XStreamAlias("xmlns:xsi")
	@XStreamAsAttribute
	final String xsi = "http://www.w3.org/2001/XMLSchema-instance";

	protected RecepcaoDocumentoCarga recepcaoDocumentoCarga;

	public RecepcaoDocumentoCarga getRecepcaoDocumentoCarga() {
		return recepcaoDocumentoCarga;
	}

	public void setRecepcaoDocumentoCarga(RecepcaoDocumentoCarga value) {
		this.recepcaoDocumentoCarga = value;
	}

}
