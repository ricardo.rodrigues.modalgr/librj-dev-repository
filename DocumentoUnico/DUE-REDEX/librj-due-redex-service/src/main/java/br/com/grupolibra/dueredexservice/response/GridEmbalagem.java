package br.com.grupolibra.dueredexservice.response;

import java.util.List;

import br.com.grupolibra.dueredexservice.response.Embalagem;
import br.com.grupolibra.dueredexservice.response.EmbalagemView;

public class GridEmbalagem {
	private List<Embalagem> listaEmbalagemResponses;
	
	private List<EmbalagemView> listaEmbalagemViewResponses;    

	public List<EmbalagemView> getListaEmbalagemViewResponses() {
		return listaEmbalagemViewResponses;
	}

	public void setListaEmbalagemViewResponses(List<EmbalagemView> listaEmbalagemViewResponses) {
		this.listaEmbalagemViewResponses = listaEmbalagemViewResponses;
	}

	public List<Embalagem> getListaEmbalagemResponses() {
		return listaEmbalagemResponses;
	}

	public void setListaEmbalagemResponses(List<Embalagem> listaEmbalagemResponses) {
		this.listaEmbalagemResponses = listaEmbalagemResponses;
	}
	
}
