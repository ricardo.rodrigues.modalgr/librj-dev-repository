package br.com.grupolibra.dueredexservice.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QEntregador is a Querydsl query type for Entregador
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEntregador extends EntityPathBase<Entregador> {

    private static final long serialVersionUID = 682017941L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEntregador entregador = new QEntregador("entregador");

    public final QCct cct;

    public final StringPath cnpj = createString("cnpj");

    public final StringPath cpf = createString("cpf");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nome = createString("nome");

    public QEntregador(String variable) {
        this(Entregador.class, forVariable(variable), INITS);
    }

    public QEntregador(Path<? extends Entregador> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QEntregador(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QEntregador(PathMetadata metadata, PathInits inits) {
        this(Entregador.class, metadata, inits);
    }

    public QEntregador(Class<? extends Entregador> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.cct = inits.isInitialized("cct") ? new QCct(forProperty("cct"), inits.get("cct")) : null;
    }

}

