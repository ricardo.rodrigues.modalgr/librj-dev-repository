package br.com.grupolibra.dueredexservice.dto;

import java.io.Serializable;
import java.util.List;


public class ConsultaDadosResumidosResponseListDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ConsultaDadosResumidosResponseDTO> listaRetorno;

	public List<ConsultaDadosResumidosResponseDTO> getListaRetorno() {
		return listaRetorno;
	}

	public void setListaRetorno(List<ConsultaDadosResumidosResponseDTO> listaRetorno) {
		this.listaRetorno = listaRetorno;
	}

}
