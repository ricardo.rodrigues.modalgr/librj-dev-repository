package br.com.grupolibra.dueredexservice.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.grupolibra.dueredexservice.dto.DocumentoDueDanfeDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.fmwPortService.helper.logger.LoggerN4;


@Component
public class BDRepository {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static final String LOG_N4 = LoggerN4.ERROR;

	@Autowired
	@Qualifier("n4EntityManagerFactory")
	private EntityManager entityManager;
	
	@Transactional(readOnly=true)
	public List<Documento> getDueWithoutTotal() {
		logger.info("getDueWithoutTotal");
					
		String sql = "SELECT NUMERO FROM DOCUMENTO WHERE TIPO='ITEM' AND TOTAL IS NULL AND STATUS = 0";
	    org.hibernate.Session session = entityManager.unwrap(org.hibernate.Session.class);
	    org.hibernate.SQLQuery q = (org.hibernate.SQLQuery) session.createSQLQuery(sql)
	        .addScalar("numero", StringType.INSTANCE)
	        .setResultTransformer(Transformers.aliasToBean(Documento.class));

	    List<Documento> dueList = q.list();		
				
		return dueList;
	}
	
	@Transactional(readOnly=true)
	public List<DocumentoDueDanfeDTO> getNFe() {
		logger.info("getNFe");
					
		String sql = "SELECT ID_DOCUMENTO, DANFE FROM DOCUMENTO WHERE TIPO='NF' AND STATUS = 0";
	    org.hibernate.Session session = entityManager.unwrap(org.hibernate.Session.class);
	    org.hibernate.SQLQuery q = (org.hibernate.SQLQuery) session.createSQLQuery(sql)
	    		.addScalar("id_documento", LongType.INSTANCE)
	    		.addScalar("danfe", StringType.INSTANCE)
	        .setResultTransformer(Transformers.aliasToBean(DocumentoDueDanfeDTO.class));

	    List<DocumentoDueDanfeDTO> nfeList = q.list();		
				
		return nfeList;
	}		
	
}
