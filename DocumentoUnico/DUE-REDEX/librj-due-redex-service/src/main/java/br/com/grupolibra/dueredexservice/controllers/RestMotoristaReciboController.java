package br.com.grupolibra.dueredexservice.controllers;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.MotoristaReciboDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Transporte;
import br.com.grupolibra.dueredexservice.service.MotoristaReciboService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestMotoristaReciboController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MotoristaReciboService motoristaReciboService;

	@CrossOrigin
	@RequestMapping(value = "api/buscatransportadora", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaTransportadora(@RequestParam("cnpj") String cnpj) {
		logger.info("buscaTransportadora - PARAMS - cnpj: {}", cnpj);
		return motoristaReciboService.buscaTransportadora(cnpj);
	}

	@CrossOrigin
	@RequestMapping(value = "api/buscamotorista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaMotorista(@RequestParam("searchDoc") String searchDoc, @RequestParam("isEstrangeiro") boolean isEstrangeiro) {
		logger.info("buscaMotorista - PARAM - searchDoc: {} - isEstrangeiro: {}", searchDoc, isEstrangeiro);
		return motoristaReciboService.buscaMotorista(searchDoc, isEstrangeiro);
	}

	@CrossOrigin
	@RequestMapping(value = "api/buscadocumentosdisponiveis", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaDocumentosDisponiveis(@RequestParam("cnpj") String cnpj, @RequestParam("novo") String novo) {
		logger.info("buscaDocumentosDisponiveis - PARAMS - cnpj: {} novo: {}", cnpj, novo);
		return motoristaReciboService.buscaDocumentosDisponiveis(cnpj, novo);
	}

	@CrossOrigin
	@RequestMapping(value = "api/salvarecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO salvaRecibo(@RequestBody(required = true) Transporte dto) {
		logger.info("salvaRecibo");
		return motoristaReciboService.salvaRecibo(dto);
	}

	@CrossOrigin
	@RequestMapping(value = "api/validamodificacaodocumentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO validaModificacaoDocumentos(@RequestBody(required = true) List<Documento> documentos) {
		logger.info("validaModificacaoDocumento");
		return motoristaReciboService.validaModificacaoDocumentos(documentos);
	}

	@CrossOrigin
	@RequestMapping(value = "api/excluirrecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO excluirRecibo(@RequestBody(required = true) Transporte dto) {
		logger.info("validaModificacaoDocumento");
		return motoristaReciboService.excluirRecibo(dto);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/validaimpressaorecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO validaImpressaoRecibo(@RequestBody(required = true) Transporte dto) {
		logger.info("validaModificacaoDocumento");
		return motoristaReciboService.validaImpressaoRecibo(dto);
	}
	
	@SuppressWarnings("rawtypes")
	@CrossOrigin
	@RequestMapping(value = "/api/recibo", method = RequestMethod.GET)
	@Transactional
	public @ResponseBody ResponseEntity imprimeRecibo(@RequestParam("idTransporte") Long idTransporte) {
		logger.info("imprimeRecibo");
		try {
			logger.info("imprimeRecibo - Gerando bytes pdf");
			byte[] pdf = motoristaReciboService.geraRecibo(idTransporte);
			
			HttpHeaders headers = createHeaders(idTransporte);

		    return ResponseEntity
		            .ok()
		            .headers(headers)
		            .contentLength(pdf.length)
		            .contentType(MediaType.parseMediaType("application/octet-stream"))
		            .body(new InputStreamResource(new ByteArrayInputStream(pdf) ));

		} catch (Exception e) {			
			logger.info("ERRO - " + e.getMessage());
			return new ResponseEntity<>("ERRO AO GERAR PDF", null, HttpStatus.BAD_REQUEST);
		}
	}
	
	private HttpHeaders createHeaders(Long idTransporte) {
		logger.info("createHeaders");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("x-filename", "recibo_entrega_carga_solta_"+idTransporte+".pdf");
		headers.add("Content-Disposition", "attachment; filename=\"recibo_entrega_carga_solta_"+idTransporte+".pdf\""); 
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		return headers;
	}
}