package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DUEDANFE")
@NamedQuery(name = "Duedanfe.findAll", query = "SELECT a FROM Duedanfe a")
public class  Duedanfe implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_DUEDAT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Duedanfe_seq")
	@SequenceGenerator(name = "Duedanfe_seq", sequenceName = "Duedanfe_seq", allocationSize = 1)
	private long id;

	@Column(name = "DUE_ID")
	private long due_Id;

	@Column(name = "DANFE_ID")
	private long danfe_Id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDueId() {
		return due_Id;
	}

	public void setDueId(long dueId) {
		this.due_Id = dueId;
	}

	public long getDanfeId() {
		return danfe_Id;
	}

	public void setDanfeId(long danfe_Id) {
		this.danfe_Id = danfe_Id;
	}



}
