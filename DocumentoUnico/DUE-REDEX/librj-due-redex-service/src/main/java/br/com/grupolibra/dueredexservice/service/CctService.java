package br.com.grupolibra.dueredexservice.service;

import java.util.List;

import br.com.grupolibra.dueredexservice.model.Cct;

public interface CctService {
	
	Cct saveCctNFE(Cct cct);

	Cct createCctRecepcaoNFE(String unitGkey);
	
	List<Cct> getCctsNFEConsultaCadastroDUE();

	void updateNumeroDUE(List<Cct> resultList);
	
	List<Cct> getEnvioRecepcaoNFEPendente();
	
	List<Cct> getEnvioRecepcaoDUEPendente();
	
	List<Cct> getCctsEnviaEntregaConteinerPendente(String categoria);
	
	List<Cct> getCctsConsultaDadosResumidos();
	
	Cct createCctRecepcaoDUE(String unitGkey);
	
	List<Cct> getCctsExportadorPendente();
	
	Cct save(Cct cct);
	
	void saveMonitoramento(Cct cct);

	Cct createCctRecepcaoNFECargaSolta(String unitGkey);

	Cct saveCctNFECargaSolta(Cct cct);

	List<Cct> getCctsConsultaDUE();
	
	List<Cct> getEnvioRecepcaoNFEPendenteCargaSolta();

}
