package br.com.grupolibra.dueredexservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "SHIPPER")
@NamedQuery(name = "Shipper.findAll", query = "SELECT s FROM Shipper s")
public class Shipper implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_SHIPPER")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHIPPER_SEQ")
	@SequenceGenerator(name = "SHIPPER_SEQ", sequenceName = "SHIPPER_SEQ", allocationSize = 1)
	private long id;

	private String cnpj;

	@Column(name = "RAZAO_SOCIAL")
	private String razaoSocial;

	@Column(name = "NOME_CONTATO")
	private String nomeContato;

	private String telefone;

	private String email;

//	@OneToMany(mappedBy = "shipper", fetch = FetchType.LAZY, targetEntity = Documento.class)
//	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
//	private List<Documento> documentos;

	@Transient
	private boolean novo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeContato() {
		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public List<Documento> getDocumentos() {
//		return documentos;
//	}
//
//	public void setDocumentos(List<Documento> documentos) {
//		this.documentos = documentos;
//	}

	public boolean isNovo() {
		return novo;
	}

	public void setNovo(boolean novo) {
		this.novo = novo;
	}

	@Override
	public String toString() {
		return "Shipper [id=" + id + ", cnpj=" + cnpj + ", razaoSocial=" + razaoSocial + ", nomeContato=" + nomeContato + ", telefone=" + telefone + ", email=" + email + ", novo=" + novo + "]";
	}

}