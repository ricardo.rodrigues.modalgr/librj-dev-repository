package br.com.grupolibra.dueredexservice.service;

import br.com.grupolibra.dueredexservice.dto.ClienteFinanceiroPagadorDTO;

public interface ClienteFinanceiroPagadorService{

	public ClienteFinanceiroPagadorDTO salvaListaCFP(ClienteFinanceiroPagadorDTO dto);

	public int validaCheckObrigatorio();

	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(String bookingNbr, String lineOperatorGkey, String despachante);

}
