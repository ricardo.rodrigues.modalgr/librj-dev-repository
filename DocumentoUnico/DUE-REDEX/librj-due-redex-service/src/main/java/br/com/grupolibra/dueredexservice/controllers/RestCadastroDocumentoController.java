package br.com.grupolibra.dueredexservice.controllers;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.dueredexservice.dto.CadastroDocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoGridDTO;
import br.com.grupolibra.dueredexservice.dto.DocumentoRequestDTO;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.response.GridNcm;
import br.com.grupolibra.dueredexservice.response.GridEmbalagem;
import br.com.grupolibra.dueredexservice.service.CadastroDocumentoService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RestCadastroDocumentoController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CadastroDocumentoService cadastroDocumentoService;

	@CrossOrigin
	@RequestMapping(value = "api/existecadastroshipper", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean existeCadastroShipper(@RequestParam("cnpj") String cnpj) {
		logger.info("existeCadastroShipper");
		return cadastroDocumentoService.existeCadastroShipper(cnpj);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/salvadocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO salvaDocumento(@RequestBody(required = true) Documento dto) {
		logger.info("salvaDocumento");
		return cadastroDocumentoService.salvaDocumento(dto);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/validamodificacaodocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO validaModificacaoDocumento(@RequestBody(required = true) Documento dto) {
		logger.info("validaModificacaoDocumento");
		return cadastroDocumentoService.validaModificacaoDocumento(dto);
	}
		
	@CrossOrigin
	@RequestMapping(value = "api/buscadocumentostransportadora", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public DocumentoDTO buscaDocumentosTransportadora(@RequestBody(required = true) DocumentoRequestDTO dto) {
		logger.info("buscaDocumentosTransportadora dto - {} ", dto.toString() );
		return cadastroDocumentoService.buscaDocumentosTransportadora(dto);
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/buscaembalagens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GridEmbalagem buscaEmbalagens() {
		logger.info("buscaEmbalagens INICIO - {} ", new Date());
		GridEmbalagem retorno = cadastroDocumentoService.buscaEmbalagens();
		logger.info("buscaEmbalagens FIM - {} ", new Date());
		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/excluirdocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String excluirDocumento(@RequestBody(required = true) Documento documento) {
		logger.info("excluirDocumento {}", documento);
		String retorno = cadastroDocumentoService.excluirDocumento(documento);
		if(retorno.toUpperCase().contains("ERRO")) {
			throw new IllegalArgumentException(retorno);
		} else {
			return retorno;
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "api/buscacommodities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GridNcm buscaCommodities() {
		logger.info("buscaCommodities INICIO - {} ", new Date());
		GridNcm retorno = cadastroDocumentoService.buscaCommodities();
		logger.info("buscaCommodities FIM - {} ", new Date());
		return retorno;
	}

}
