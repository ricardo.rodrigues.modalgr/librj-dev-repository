package br.com.grupolibra.dueredexservice.service.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.querydsl.core.BooleanBuilder;

import br.com.grupolibra.dueredexservice.model.Cct;
import br.com.grupolibra.dueredexservice.model.Danfe;
import br.com.grupolibra.dueredexservice.model.Documento;
import br.com.grupolibra.dueredexservice.model.Exportador;
import br.com.grupolibra.dueredexservice.model.Monitoramento;
import br.com.grupolibra.dueredexservice.model.QCct;
import br.com.grupolibra.dueredexservice.model.QDocumento;
import br.com.grupolibra.dueredexservice.repository.CCTRepository;
import br.com.grupolibra.dueredexservice.repository.DanfeRepository;
import br.com.grupolibra.dueredexservice.repository.DocumentoRepository;
import br.com.grupolibra.dueredexservice.repository.MonitoramentoRepository;
import br.com.grupolibra.dueredexservice.repository.n4.N4Repository;
import br.com.grupolibra.dueredexservice.service.CctService;
import br.com.grupolibra.dueredexservice.util.CategoriaProcessoEnum;
import br.com.grupolibra.dueredexservice.util.PropertiesUtil;

@Service
public class CctServiceImpl implements CctService {

	private static final String BD_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	private static final String VALIDA_ENVIO_RECEPCAO_NFE = "validaEnvioRecepcao - {} ";

	private static final String FORMATO_DATA = "dd/MM/yyyy HH:mm:ss";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CCTRepository cctRepository;
	
	@Autowired
	private DanfeRepository danfeRepository;
	
	@Autowired
	private DocumentoRepository documentoRepository;

	@Autowired
	private N4Repository n4Repository;
	
	@Autowired
	private MonitoramentoRepository monitoramentoRepository;

	@Override
	public Cct saveCctNFE(Cct cct) {
		logger.info("saveCctNFE");
		cct = this.save(cct);
		for (Danfe danfe : cct.getDanfes()) {
			logger.info("saveCctNFE --> danfe.setCct(cct)");
			danfe.setCct(cct);
			logger.info("saveCctNFE --> danfeRepository.save(danfe)");
			danfeRepository.save(danfe);
		}
		return cct;
	}
	
	@Override
	public Cct saveCctNFECargaSolta(Cct cct) {
		logger.info("saveCctNFE");
		cct = this.save(cct);
		return cct;
	}	

	@Override
	public Cct createCctRecepcaoNFE(String unitGkey) {
		logger.info("createCctRecepcaoNFE - unitGkey: " + unitGkey);
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoNFEByUnitGkey(unitGkey);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return validaEnvioRecepcaoNFE(cct);
	}

	private Cct validaEnvioRecepcaoNFE(Cct cct) {
		logger.info("validaEnvioRecepcaoNFE");

		if (cct.getDanfes()== null) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_DANFE);			
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
		} else if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_LACRE);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_UNIT_SEAL);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
		} else if(cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO_REGRA_PESO);
			logger.info(VALIDA_ENVIO_RECEPCAO_NFE, Cct.FALHA_VALIDACAO_REGRA_PESO);
		}	else { 
			logger.info("validaEnvioRecepcaoNFE - OK");
			return this.saveCctNFE(cct);
		}
	
		this.saveCctNFE(cct);
		return null;
	}

	public Cct validaEnvioRecepcaoNFECargaSolta(Cct cct) {
		logger.info("validaEnvioRecepcaoNFECargaSolta - NroConteiner: " + cct.getNumeroCont());
		
		Documento documento = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());		
		if (documento == null) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_DANFE);			
			logger.info("validaEnvioRecepcaoNFECargaSolta - " + Cct.FALHA_AO_LEVANTAR_DADOS_DANFE_MSG);
			
		} else if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_LACRE);
			logger.info("validaEnvioRecepcaoNFECargaSolta - " + Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_UNIT_SEAL);			
			logger.info("validaEnvioRecepcaoNFECargaSolta - " + Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			
		} else if(cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO_REGRA_PESO);
			logger.info("validaEnvioRecepcaoNFECargaSolta - " + Cct.FALHA_VALIDACAO_REGRA_PESO);
			
		} else {
			logger.info("validaEnvioRecepcaoNFECargaSolta - OK");
			return this.save(cct);
		}
		
		this.save(cct);
		return null;
	}
	
	private Cct validaEnvioRecepcaoDUE(Cct cct) {
		logger.info("validaEnvioRecepcaoDUE");
		if(StringUtils.isEmpty(cct.getLacres())) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_LACRE);
			logger.info("validaEnvioRecepcaoDUE - " + Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG);
		} else if(!cct.isUnitSeal()) {
			cct.setErro(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
			cct.setStatus(Cct.STATUS_ERRO_SEM_UNIT_SEAL);			
			logger.info("validaEnvioRecepcaoDUE - " + Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG);
		} else if (cct.isPendenciaPeso()) {
			cct.setErro(Cct.FALHA_VALIDACAO_REGRA_PESO);
			cct.setStatus(Cct.STATUS_ERRO_REGRA_PESO);				
			logger.info("validaEnvioRecepcaoDUE - " + Cct.FALHA_VALIDACAO_REGRA_PESO);
		} else {
			logger.info("validaEnvioRecepcaoDUE - OK");
			return this.save(cct);
		}
		
		this.save(cct);
		return null;
	}	
	
	@Override
	public void updateNumeroDUE(List<Cct> resultList) {
		logger.info("updateNumeroDUE");
		logger.info("updateNumeroDUE - Qtd registros : {} ", resultList.size());
		for (Cct cct : resultList) {
			logger.info("updateNumeroDUE - CONTEINER : {} | DUE : {} ", cct.getNumeroCont(), cct.getNrodue()); 
			if(cct.getNrodue() != null && !cct.getNrodue().isEmpty() && !"".equals(cct.getNrodue())) {
				
				cct.setDtproc(new Date(System.currentTimeMillis()));
				
				String notes = "DUE: " + cct.getNrodue();
				boolean isNFE = CategoriaProcessoEnum.NFE.toString().equals(cct.getCategoria());			
				String retorno = n4Repository.createDueEvents(cct.getIdExterno(), notes, isNFE);
				logger.info("updateNumeroDUE - RETORNO N4 : {} ", retorno);
				if(!"OK".equals(retorno)) {
					cct.setStatus(Cct.STATUS_ERRO_EVENTO_DUE);
					cct.setMensagem(Cct.STATUS_ERRO_EVENTO_DUE);
				} else if(CategoriaProcessoEnum.TRANSBORDO_MARITIMO.toString().equals(cct.getCategoria())) {
					cct.setStatus(Cct.STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO);
					cct.setStatusProc(Cct.STATUS_AGUARDANDO_ENVIO_RECEPCAO_TRANSBORDO_MARITIMO);
				}
				
				if(isNFE) 
					n4Repository.processaExportador(cct);
				
				this.save(cct);
			}
		}
		logger.info("updateNumeroDUE - OK");	
	}

	@Override
	public Cct createCctRecepcaoDUE(String unitGkey) {
		logger.info("createCctRecepcaoDUE");
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoDUECargaSoltaByUnitGkey(unitGkey);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return validaEnvioRecepcaoDUE(cct);
	}
	
	@Override
	public List<Cct> getEnvioRecepcaoNFEPendente() {
		logger.info("getEnvioRecepcaoNFEPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.danfes.isNotEmpty());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	
	
	@Override
	public List<Cct> getEnvioRecepcaoNFEPendenteCargaSolta() {
		logger.info("getEnvioRecepcaoNFEPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.billoflanding.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getEnvioRecepcaoNFEPendenteCargaSolta() {
		logger.info("getEnvioRecepcaoNFEPendenteCargaSolta");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.billoflanding.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}	
	
	@Override
	public List<Cct> getEnvioRecepcaoDUEPendente() {
		logger.info("getEnvioRecepcaoDUEPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.andAnyOf(cct.erro.eq(Cct.STATUS_ERRO_PARADA_PROGRAMADA), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_LACRE_MSG), cct.erro.eq(Cct.FALHA_AO_LEVANTAR_DADOS_UNIT_SEAL_MSG), cct.erro.eq(Cct.FALHA_VALIDACAO_REGRA_PESO), 
				cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), 
				cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.DUE.toString()));
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	
	@Override
	public List<Cct> getCctsEnviaEntregaConteinerPendente(String categoria) {
		logger.info("getCctsEnviaEntregaConteiner - CATEGORIA: {} ", categoria);
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		
		builder.andAnyOf(cct.statusProc.eq(Cct.STATUS_AGUARDANDO_RECEPCAO_ENTREGA), cct.statusProc.eq(Cct.STATUS_AGUARDANDO_DUE_ENTREGA), cct.statusProc.eq(Cct.STATUS_ERRO_ENTREGA));
		builder.andAnyOf(cct.mensagem.eq(Cct.STATUS_RECEPCAO_EFETUADA_SUCESSO), cct.mensagem.eq(Cct.ERRO_TIMEOUT.toString()), cct.mensagem.eq(Cct.REQUISICAO_MAL_FORMATADA.toString()), cct.mensagem.eq(Cct.ERRO_INTERNO_SERVIDOR.toString()), cct.mensagem.eq(Cct.SERVICO_INDISPONIVEL.toString()), cct.mensagem.eq(Cct.CERTIFICADO_NAO_AUTORIZADO_OPERACAO.toString()), cct.mensagem.eq(Cct.REGISTRO_NAO_ENCONTRADO.toString()));
		
		builder.and(cct.categoria.eq(categoria));
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.contadorEnvio.lt(Integer.valueOf(PropertiesUtil.getProp().getProperty("siscomex.limite.envio"))));		
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
		
	@Override
	public List<Cct> getCctsConsultaDadosResumidos() {
		logger.info("getCctsConsultaDadosResumidos");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.nrodue.isNotNull());
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}

	@Override
	public List<Cct> getCctsConsultaDUE() {
		logger.info("getCctsConsultaDUE");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.statusProc.isNull());
		builder.and(cct.nrodue.isNull());
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctsNFEConsultaCadastroDUE() {
		logger.info("getCctsNFEConsultaCadastroDUE");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.statusProc.eq(Cct.STATUS_RECEPCIONADO_OK));
		builder.and(cct.nrodue.isNull());
		builder.and(cct.categoria.eq(CategoriaProcessoEnum.NFE.toString()));
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	@Override
	public List<Cct> getCctsExportadorPendente() {
		logger.info("getCctsExportadorPendente");
		QCct cct = QCct.cct;
		BooleanBuilder builder = new BooleanBuilder();
		builder.and(cct.nrodue.isNotNull());
		builder.and(cct.exportadores.isEmpty());
		builder.andAnyOf(cct.finalizado.isNull(), cct.finalizado.ne(1));

		return cctRepository.findAll(builder.getValue());
	}
	
	public Cct save(Cct cct) {
		cct = cctRepository.save(cct);
		if(cct.getFinalizado() != null && cct.getFinalizado() == 1) {
			logger.info("DELETE MONITORAMENTO");
			monitoramentoRepository.deleteById(cct.getId());
		} else {
			saveMonitoramento(cct);
		}
		return cct;
	}
	
	public void saveMonitoramento(Cct cct) {
		logger.info("SAVE MONITORAMENTO");
		Monitoramento monitoramento = new Monitoramento();
		monitoramento.setId(cct.getId());
		monitoramento.setNavio(cct.getNavio());
		monitoramento.setVisit(cct.getViagem());
		monitoramento.setLineOp((cct.getArmador() != null) ?  (cct.getArmador().getNomeEstrangeiro() != null ? cct.getArmador().getNomeEstrangeiro() : cct.getArmador().getNome()) : "");
		monitoramento.setBooking((cct.getBooking() != null ? cct.getBooking() : cct.getBilloflanding()));
		monitoramento.setConteiner(cct.getNumeroCont());
		StringBuilder danfes = new StringBuilder();
		
		if(cct.getDanfes() != null){
			for(Danfe danfe : cct.getDanfes()) {
				danfes.append(danfe.getChave() + " - ");
			}
			monitoramento.setDanfesString((danfes != null && danfes.toString() != null && !danfes.toString().isEmpty()) ? danfes.toString().substring(0, danfes.toString().length() - 3) : null);
		}
		
		Documento danfe = documentoRepository.findByNumeroConteiner(cct.getNumeroCont());
		if (danfe!=null)
			monitoramento.setDanfesString(danfe.getDanfe());
		
		monitoramento.setDuesString(cct.getNrodue());
		monitoramento.setStatus(cct.getStatusProc());
		
		if(cct.getExportadores() != null && !cct.getExportadores().isEmpty()) {
			String situacao = "";
			for (Exportador exportador : cct.getExportadores()) {
				if(exportador.getSituacaoDUE() != null && exportador.getSituacaoDUE() > 0) {
					situacao += exportador.getSituacaoDUE() + "-";
				}
			}
			monitoramento.setSituacao(!"".equals(situacao) ? situacao.substring(0, situacao.length()-1) : "Não processado");
		} else {
			monitoramento.setSituacao("Não processado");
		}
		
		monitoramento.setBloqueado(cct.getBloqueadoSiscomex() != null && cct.getBloqueadoSiscomex() == 1 ? true : false);
		monitoramento.setMensagem(cct.getMensagem());
		monitoramento.setErro(cct.getErro());
		try {
			monitoramento.setDataEntrada(cct.getDtentr() != null ? cct.getDtentr() : null);
		} catch(Exception e) {
			monitoramento.setDataEntrada(null);
		}
		try {	
			monitoramento.setDataProcessamento(cct.getDtproc() != null ? cct.getDtproc() : null);
		} catch(Exception e) {
			monitoramento.setDataProcessamento(null);
		}
		
		if(Cct.STATUS_ERRO.equals(cct.getStatus())) {
			monitoramento.setOk(false);
		} else {
			monitoramento.setOk(true);
		}
		
		monitoramentoRepository.save(monitoramento);
	}

	@Override
	public Cct createCctRecepcaoNFECargaSolta(String unitGkey) {
		Cct cct = null;
		try {
			cct = n4Repository.getCctRecepcaoNFECargaSoltaByUnitGkey(unitGkey);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		cct=validaEnvioRecepcaoNFECargaSolta(cct);

		return cct;
	}

}
