app.controller('MasterCtrl', ['$scope', '$sce', '$location', '$rootScope', '$modal', 'cadastroConteinerService', 'modalService', 'FormataCampo',
    function($scope, $sce, $location, $rootScope, $modal, cadastroConteinerService, modalService, FormataCampo) {

        $scope.modalContainer = "";
        
        $scope.showContent = false;
        $scope.showDialogCadastroConteiner = false;
        
        $scope.apiURL = $(document.getElementById("divUrlApi").childNodes[1]).find('span').html();

        $scope.hasPermissionDUE = $('#hasPermissionDUE').val();
        $scope.hasPermissionDespachante = $('#hasPermissionDespachante').val();
        $scope.hasPermissionSupervisor = $('#hasPermissionSupervisor').val();
        
        $scope.cnpjs = $('#cnpjs').val();
        $scope.cnpjs = FormataCampo($scope.cnpjs);
        console.log("cnpjs -> " + $scope.cnpjs);
        $scope.nomeTransportadora = $('#nomeTransportadora').val();
        $scope.nomeTransportadora = FormataCampo($scope.nomeTransportadora);
        
        $scope.getTransportadoraInfo = function() {
        	openGifModal();
        	cadastroConteinerService.getTransportadoraInfo($scope.apiURL, $scope.cnpjs).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                closeGifModal();
                $scope.nomeTransportadora = response.data.nomeTransportadora;
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
            })
        };
        $scope.getTransportadoraInfo();        
        
        $scope.showDialogSuccess = false;
        $scope.showDialogError = false;
        $scope.showDialogErrorCadastro = false;
        $scope.showDialogErrorCadastroCustom = false;
        $scope.showDialogSuccessCadastro = false;
        $scope.showDialogExcluirDanfe = false;
        $scope.showDialogDeleteConteiner = false;
        $scope.Mensagem = "";
        $scope.carregaBooking = false;

        $scope.cadastroConteinerDTO = {};
        $scope.bookingNbr = "";
        $scope.booking = {};
        
        $scope.conteinerCadastro = {};
        $scope.conteinerCadastro.danfeList = [];
        
        $scope.cadastroDanfe = {};
        $scope.danfeRemove = {};
        $scope.conteinerDelete = {};
        
        $scope.liberaCadastroUnit = false;
        
        $scope.itemList = [];
        $scope.itemSel = {};
        $scope.itemSelecionado = false;
        	
    	$scope.getBooking = function() {
    		
    		$scope.showContent = false;
    		$scope.cadastroConteinerDTO = {};
    		$scope.booking = {};
    		
    		if($scope.isEmptyString($scope.bookingNbr)) {
    			msg = "Digite um numero de booking para a pesquisa.";
                modalService.closeModalErrors(msg);
    			return;
    		}
    		
    		openGifModal();
        	cadastroConteinerService.getBooking($scope.apiURL, $scope.bookingNbr).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                closeGifModal();
                $scope.cadastroConteinerDTO = response.data;
                if($scope.cadastroConteinerDTO.erro) {
                	msg = $scope.cadastroConteinerDTO.mensagem;
                    modalService.closeModalErrors(msg);
                	return;
                } else if($scope.cadastroConteinerDTO.bookingDTO != null && $scope.cadastroConteinerDTO.bookingDTO.bookingGkey != null) {
                	$scope.booking  = $scope.cadastroConteinerDTO.bookingDTO;
                	$scope.itemList = $scope.booking.itemList;
                	if($scope.itemList.length == 1) {
                		$scope.itemSel = $scope.itemList[0];
                	}
                	$scope.showContent = true;
                	return;
                } else {
                	msg = "Booking não encontrado.";
                    modalService.closeModalErrors(msg);
        			return;
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
            })
        };
        
        $scope.filtradoc = function(doc) {
        	return $sce.trustAsHtml(doc);
        }
        
        
        
        $scope.salvaConteiner = function() {
        	if($scope.isValidCadastroConteiner()) {
        		
        		$scope.cadastroConteinerDTO = {};
        		
        		$scope.cadastroConteinerDTO.bookingDTO = $scope.booking;
        		$scope.conteinerCadastro.bookingItem = $scope.itemSel;        		
        		$scope.cadastroConteinerDTO.conteinerCadastro = $scope.conteinerCadastro;
        		
        		openGifModal();
        		
        		cadastroConteinerService.salvaConteiner($scope.apiURL, $scope.cadastroConteinerDTO).then(function(response) {
                    // SUCCESS
                    // 100,101,200,201
                    console.log(response);
                    $scope.cadastroConteinerDTO = response.data;
                    closeGifModal();
                	
                    msg = $scope.cadastroConteinerDTO.mensagem;
                    modalService.closeModalErrors(msg);
   
                    if(!$scope.cadastroConteinerDTO.erro) 
                    {
                    	$scope.itemSelecionado = false;                    
                    	$scope.carregaBooking = true;
                    }
                    
            		return;
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    msg = response.data;
                    modalService.closeModalErrors(msg);
                })
        		
        	} 
        };
        
        $scope.isValidCadastroConteiner = function() {
        	if(Object.keys($scope.itemSel).length == 0) {
        		msg = "Tipo do conteiner é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	if($scope.itemSel.itemGkey == 0) {
        		msg = "Tipo do conteiner é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	if($scope.isEmptyString($scope.conteinerCadastro.numeroConteiner)) {
        		msg = "Numero do conteiner é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	if($scope.isEmptyString($scope.conteinerCadastro.pesoBruto)) {
        		msg = "Peso bruto é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	if($scope.isEmptyString($scope.conteinerCadastro.lacre1)) {
        		msg = "Lacre 1 é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}        	
        	return true;
        };
        
        $scope.validaUnitId = function() {
        	
        	if($scope.isEmptyString($scope.conteinerCadastro.numeroConteiner)) {    		        		
        		msg = "Insira um Numero de conteiner para validação.";
        		modalService.closeModalErrors(msg);
    			return;
        	}
        	
        	openGifModal();
        	
        	cadastroConteinerService.validaUnitId($scope.apiURL, $scope.conteinerCadastro.numeroConteiner).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                $scope.cadastroConteinerDTO = response.data;
                closeGifModal();
                if($scope.cadastroConteinerDTO.erro) {
                	msg = $scope.cadastroConteinerDTO.mensagem;
                	modalService.closeModalErrors(msg);
                	$scope.liberaCadastroUnit = false;
                } else{
                	if($scope.cadastroConteinerDTO.mensagem != "OK") {
                		// retornou o ISO do conteiner
                		//var iso = $scope.cadastroConteinerDTO.mensagem;        
                		var archType = $scope.cadastroConteinerDTO.mensagem;
                		var informaISO = $scope.cadastroConteinerDTO.informaISO;
                		if($scope.itemList.length > 1) {
                			var match = false;
                			$($scope.itemList).each(function(index, value){                		
                				if(value.archType == archType) {
	                        		if(value.disponivel != 0){
										$scope.itemSel = value;
										$scope.itemSelecionado = true;
										$("#tipo").val(value.gkey);
										$("#tipo select").val(value.gkey);
										match = true;
	                        		}
	                        	}
                				
                        	});
                			if(!match) {
                				var msg = "<B>Os itens do booking não são compativeis com o ISO do Contêiner. </B><BR><BR>";
                				msg += " CONTÊINER: " + $scope.conteinerCadastro.numeroConteiner + " - ISO: " + informaISO + "<BR><BR>";
                				msg += " Os itens do booking são : <BR>";
                				$($scope.itemList).each(function(index, value){
                					if (value.descricao) 
                						msg += " " + value.iso + " - " + value.descricao  + "<BR>";
                				});
                				modalService.closeModalErrors(msg);
                				
                            	$scope.liberaCadastroUnit = false;
                			} else {
//                				$("#tipo").val("0");
                        		$("#tipo").prop("disabled", true);
                        		$('#tipo').attr("disabled","disabled");
                				$scope.liberaCadastroUnit = true;
                			}
                		} else {
                			if($scope.itemList[0].archType != archType) {
                				var msg = "<B>Os itens do booking não são compativeis com o ISO do Contêiner. </B><BR>";
                				msg += " CONTÊINER: " + $scope.conteinerCadastro.numeroConteiner + " - ISO: " + informaISO + "<BR>";
                				msg += " ITEM BOOKING: " + $scope.itemList[0].descricao + " - " + $scope.itemList[0].iso;
                				modalService.closeModalErrors(msg);
                            	$scope.liberaCadastroUnit = false;
                			} else {
                				$scope.liberaCadastroUnit = true;
                			} 
                		}
                	} else {
                		$scope.resetComboTipo();
                		
                		$("#tipo").prop("disabled", false);
        				$('#tipo').removeAttr('disabled');
        				$scope.liberaCadastroUnit = true;
                	}                	
                }
        		return;
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
                $scope.showContent = false;
            })
        };
        
        $scope.imprimeRecibo = function(conteiner) {
        	openGifModal();
        	cadastroConteinerService.validaImpressaoRecibo($scope.apiURL, conteiner.gkey).then(function(response) {
                $scope.cadastroConteinerDTO = response.data;
                
                if($scope.cadastroConteinerDTO.erro) {
                	msg = $scope.cadastroConteinerDTO.mensagem;
                	modalService.closeModalErrors(msg);
                } else{
                	                	                
                	cadastroConteinerService.imprimeRecibo(conteiner.gkey, conteiner.numeroConteiner, $scope.cnpjs).then(function(response) {
                      		
                        var objectUrl = window.URL.createObjectURL(base64toBlob(response.data, "application/pdf"));
                        var downloadLink = document.createElement("a");
                        downloadLink.href = objectUrl;
                        downloadLink.download = "recibo.pdf";
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);   
                        closeGifModal();
                	 },
                     function errorCallBack(response) {
                         // ERROR 400,401,500
                         console.log(response);
                         closeGifModal();
                         msg = response.data;
                         modalService.closeModalErrors(msg);
                     })     
                }
                	                	                
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
            })
        };
        
        $scope.editConteiner = function(conteiner) {
        	$scope.liberaCadastroUnit = true;
        	$scope.conteinerCadastro = conteiner;
        	$scope.itemSel = conteiner.bookingItem;
        	$scope.itemSelecionado = true;
        	if($scope.conteinerCadastro.danfeList == null || $scope.conteinerCadastro.danfeList == undefined) {
        		$scope.conteinerCadastro.danfeList = [];
        	}
        	$scope.showDialogCadastroConteiner = true;
        };
        
        $scope.deleteConteiner = function(conteiner) {
        	$scope.conteinerDelete = conteiner;
        	if($scope.conteinerCadastro.danfeList != undefined && $scope.conteinerCadastro.danfeList.length) {
        		msg = "O Contêiner " + conteiner.numeroConteiner + " possui documentos cadastrados. Deseja excluir ?";
        		modalService.dialogModal(msg, $scope.deleteConteinerConfirm);
        		return;
        	}
        	
        	msg = "O Contêiner " + conteiner.numeroConteiner + " será removido do sistema. Deseja excluir ?";
        	modalService.dialogModal(msg, $scope.deleteConteinerConfirm);
    		return;
        }
        
        $scope.deleteConteinerConfirm = function() {
        	
        	$scope.Mensagem = "";
        	$scope.showDialogDeleteConteiner = false;
        	
        	openGifModal();
        	
        	cadastroConteinerService.deleteConteiner($scope.apiURL, $scope.conteinerDelete).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                $scope.cadastroConteinerDTO = response.data;
                closeGifModal();
                
            	msg = $scope.cadastroConteinerDTO.mensagem;
            	modalService.closeModalErrors(msg);
            	$scope.conteinerDelete = {};
           
                if(!$scope.cadastroConteinerDTO.erro)
                	$scope.getBooking();
        		return;
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
                $scope.showContent = false;
            })
        }
        
        $scope.novoConteiner = function() {
        	
        	if(Object.keys($scope.booking).length == 0) {
        		msg = "Por favor, pesquise um booking novamente.";
        		modalService.closeModalErrors(msg);
    			return;
        	}
        	
        	if($scope.booking.conteinerList == null || $scope.booking.conteinerList.length < $scope.booking.qtd) {
        		$scope.liberaCadastroUnit = false;
        		$scope.conteinerCadastro = {};
        		$scope.conteinerCadastro.danfeList = [];
        		$scope.cadastroDanfe = {};
        		$scope.resetComboTipo();
        		$("#tipo").prop("disabled", true);
        		$('#tipo').attr("disabled","disabled");
        		$scope.itemSel = {};
        		$scope.showDialogCadastroConteiner = true;
       		
        	} else {
        		msg = "Já foram cadastrados todas as unidades para este booking.";
        		modalService.closeModalErrors(msg);
    			return;
        	}
        };
        
        $scope.desabilitaFormCadastroConteiner = function() {
        	$scope.itemSelecionado = false;
        	$scope.liberaCadastroUnit = false;
        	$scope.resetComboTipo();
    		$("#tipo").prop("disabled", true);
    		$('#tipo').attr("disabled","disabled");
        };
        
        $scope.addDanfe = function() {
        	if($scope.isValidCadastroDanfe()) {
        		
        		for(let i = 0; i < $scope.conteinerCadastro.danfeList.length; i++) {
					if($scope.conteinerCadastro.danfeList[i].chave == $scope.cadastroDanfe.chave){
						msg = "Chave DANFE " + $scope.cadastroDanfe.chave + " já incluida nesta unidade.";
						modalService.closeModalErrors(msg);
                        return;
					}
				}
        		
        		var danfeCnpj = $scope.cadastroDanfe.chave;
    			var subDanfeCnpj = danfeCnpj.substring(6,20);
    			var str = $scope.cadastroDanfe.chave;
    			var sub = str.substring(25, 34);
    			$scope.cadastroDanfe.numero = sub;
    			
    			for(let i = 0 ; i < $scope.conteinerCadastro.danfeList.length; i++){
    				if($scope.conteinerCadastro.danfeList[i].chave.includes(subDanfeCnpj)){
    					if($scope.conteinerCadastro.danfeList[i].chave.includes(subDanfeCnpj) && $scope.conteinerCadastro.danfeList[i].chave.includes(sub)){
    						msg = "Número DANFE já cadastrado para cnpj informado para esta unidade!";
    						modalService.closeModalErrors(msg);
    						return;
    					}
    				}
    			}    
    			
            	$scope.conteinerCadastro.danfeList.push($scope.cadastroDanfe);
        		$scope.cadastroDanfe = {};    			
        		
//        		openGifModal();
//        		
//        		cadastroConteinerService.validaInclusaoDanfe($scope.apiURL, $scope.cadastroDanfe.chave, $scope.conteinerCadastro.gkey, $scope.cadastroDanfe.numero).then(function(response) {        			
//
//                    console.log(response);
//                    closeGifModal();                    
//                    $scope.cadastroConteinerDTO = response.data;
//                    closeGifModal();
//                    if($scope.cadastroConteinerDTO.erro) {
//                    	msg = $scope.cadastroConteinerDTO.mensagem;
//                    	modalService.closeModalErrors(msg);
//                    	return;
//                    } else{
//                    	$scope.conteinerCadastro.danfeList.push($scope.cadastroDanfe);
//                		$scope.cadastroDanfe = {};
//                    }
//                },
//                function errorCallBack(response) {
//                    // ERROR 400,401,500
//                    console.log(response);
//                    closeGifModal();
//                    msg = response.data;
//                    modalService.closeModalErrors(msg);
//                })
        	}
        };
        
        $scope.isValidCadastroDanfe = function() {        	
        	if($scope.isEmptyString($scope.cadastroDanfe.chave)) {
        		msg = "Chave DANFE é obrigatório.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	if(!$scope.isValidDanfe($scope.cadastroDanfe.chave)) {
        		msg = "Chave DANFE inválida.";
        		modalService.closeModalErrors(msg);
    			return false;
        	}
        	return true;
        };
        
        $scope.excluirDanfe = function(danfe) {
        	$scope.danfeRemove = danfe;
        	msg = "Deseja excluir a DANFE ?"
        	modalService.dialogModal(msg, $scope.excluirDanfeConfirm);
        };
        
        $scope.excluirDanfeConfirm = function() {
        	$scope.showDialogExcluirDanfe = false;
	        if($scope.danfeRemove.id == null) {
	    		for(let i = 0; i < $scope.conteinerCadastro.danfeList.length; i++) {
					if($scope.conteinerCadastro.danfeList[i].chave == $scope.danfeRemove.chave){
						$scope.conteinerCadastro.danfeList.splice(i, 1);
					}
	        	}
	    		msg = "DANFE excluida com sucesso.";
	    		modalService.closeModalErrors(msg);
            	return;
	    	} else {
	    		openGifModal();
	    		cadastroConteinerService.excluirDanfe($scope.apiURL, $scope.danfeRemove).then(function(response) {
                    // SUCCESS
                    // 100,101,200,201
                    console.log(response);
                    $scope.cadastroConteinerDTO = response.data;
                    closeGifModal();
                    if($scope.cadastroConteinerDTO.erro) {
                    	msg = $scope.cadastroConteinerDTO.mensagem;
                    	modalService.closeModalErrors(msg);
                    } else {
                    	for(let i = 0; i < $scope.conteinerCadastro.danfeList.length; i++) {
        					if($scope.conteinerCadastro.danfeList[i].chave == $scope.danfeRemove.chave){
        						$scope.conteinerCadastro.danfeList.splice(i, 1);
        					}
        	        	}
                    	msg = "DANFE excluida com sucesso.";
                    	modalService.closeModalErrors(msg);
                    	$scope.carregaBooking = false;
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    msg = response.data;
                    modalService.closeModalErrors(msg);
                })
	    	}
        };
        
        $scope.selItem = function() {
        	var itemGkey = $('#tipo').val();
        	$($scope.itemList).each(function(index, value){
        		if(value.itemGkey == itemGkey) {
       				$scope.itemSel = value;
       				$scope.itemSelecionado = true;
        		}
        	});
        	
        	if($scope.itemSel.itemGkey == 0) {
        		$scope.itemSelecionado = false;
        	} else if($scope.itemSel.disponivel == 0) {
        		$scope.resetComboTipo();
				$scope.itemSel = {};
   				$scope.itemSelecionado = false;
				msg = "Não exitem unidades disponiveis para este tipo.";
				modalService.closeModalErrors(msg);
			}
        };
        
        $scope.resetComboTipo = function() {
        	$scope.itemSel = $scope.itemList[0];
        	$("#tipo").val("0");
    		$("#tipo select").val("0");
    		$("#tipo option:first").attr('selected','selected');
    		$("#tipo").val($("#tipo option:first").val());
        };
        
        $scope.closeDialogSuccess = function() {
        	$scope.showDialogSuccess = false;
            $scope.Mensagem = "";
            if($scope.carregaBooking) {
        		$scope.getBooking();
        	}
        };
        
        $scope.closeDialogError = function() {
            $scope.showDialogError = false;
            $scope.Mensagem = "";
        };
        
        $scope.closeDialogErrorCadastro = function() {
            $scope.showDialogErrorCadastro = false;
            $scope.Mensagem = "";
            if(!$scope.itemSelecionado) {
            	$scope.resetComboTipo();
            }
        };
        
        $scope.closeDialogErrorCadastroCustom = function() {
        	$scope.showDialogErrorCadastroCustom = false;
        };
        
        $scope.closeDialogSuccessCadastro = function() {
        	$scope.showDialogSuccessCadastro = false;
        	$scope.Mensagem = "";
        	if($scope.carregaBooking) {
        		$scope.closeDialogCadastroConteiner();
        		$scope.getBooking();
        	}
        };
        
        $scope.closeDialogCadastroConteiner = function() {
        	$scope.itemSelecionado = false;
        	$scope.conteinerCadastro = {};
        	$scope.cadastroDanfe = {};
        	$scope.showDialogCadastroConteiner = false;
        };
        
        $scope.closeDialogCadastroConteinerAtualiza = function() {
        	$scope.itemSelecionado = false;
        	$scope.conteinerCadastro = {};
        	$scope.cadastroDanfe = {};
        	$scope.showDialogCadastroConteiner = false;
        	if($scope.carregaBooking) {
        		$scope.getBooking();
        	}        	
        };        
        
        $scope.closeDialogExcluirDanfe = function() {
        	$scope.danfeRemove = {};
        	$scope.showDialogExcluirDanfe = false;
        };
        
        $scope.closeDialogDeleteConteiner = function() {
        	$scope.conteinerDelete = {};
        	$scope.Mensagem = "";
        	$scope.showDialogDeleteConteiner = false;
        };
        
        function openGifModal() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DueRedexCadastroConteinerPortlet/paginas/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }

        function closeGifModal() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }
        
        $scope.isEmptyString = function(string) {
        	return (string == null || string == "" || string == undefined);
        };
        
        function base64toBlob(base64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }
//        
//        $('#danfe').keyup(function() {
//            var val = this.value;
//            var lastLength;
//            do {
//                // Loop over the input to apply rules repeately to pasted inputs
//                lastLength = val.length;
//                val = replaceBadInputs(val);
//            } while (val.length > 0 && lastLength !== val.length);
//            this.value = val;
//        });
        
        $scope.isValidDanfe = function(danfe) {
    		var msgErro = '';
    		var multiplicadores = [ 2, 3, 4, 5, 6, 7, 8, 9 ];
    		var i = 42;
    		var soma_ponderada = 0;
    		var chave43 = danfe.split('');
    		while (i >= 0) {
    			for (var m = 0; m < multiplicadores.length && i >= 0; m++) {
    				soma_ponderada += chave43[i] * multiplicadores[m];
    				i--;
    			}
    		}
    		var resto = soma_ponderada % 11;
    		var digito = 0;
    		if (resto != 0 && resto != 1) {
    			digito = (11 - resto);
    		}
    		var digitoValido = (Number(chave43[43]) == digito);
//    		if (danfe == "" || isNaN(danfe as any) || danfe.toString().length != 44 ) {
    		if (danfe == '' || danfe.toString().length != 44) {
//    			return 'DANFE deve ser preenchido corretamente.';
    			return false;
    		}
    		if (danfe == '00000000000000000000000000000000000000000000') {
//    			return 'DANFE deve ser preenchido corretamente.';
    			return false;
    		}
    		if (digitoValido == false) {
//    			return 'D�gito da DANFE inv�lido.';
    			return false;
    		}
    		return true;
    	}

        var mobileView = 992;
        $scope.getWidth = function() {
            return window.innerWidth;
        };
        $scope.$watch($scope.getWidth, function(newValue, oldValue) {
            if (newValue >= mobileView) {

            } else {
                $scope.toggle = false;
            }

        });
        $scope.toggleSidebar = function() {
            $scope.toggle = !$scope.toggle;
        };
        window.onresize = function() {
            $scope.$apply();
        };
    }
]);