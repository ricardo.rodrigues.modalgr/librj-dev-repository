app.factory('modalService', ['$q', '$modal', '$sce',
	function($q, $modal, $sce) {

	
    var _closeModalErrors = function(msg) {
    	this.Errors = $modal.open({
			templateUrl: '/DueRedexCadastroConteinerPortlet/paginas/modal-errors.html',
			controller: function($modalInstance,$scope){					
				$scope.Mensagem = $sce.trustAsHtml(msg);
				$scope.Title = "Aviso";
				
				$scope.closeDialogError = function() {
					$modalInstance.close('cancel');
				};				
			}
    	});
    }
    
    var _dialogModal = function(msg, funcao) {
    	this.Errors = $modal.open({
			templateUrl: '/DueRedexCadastroConteinerPortlet/paginas/modal-dialog.html',
			controller: function($modalInstance,$scope){					
				$scope.Mensagem = msg;
				$scope.Title = "Aviso";
				
				$scope.closeDialogError = function() {
					$modalInstance.close('cancel');
				};
				
				$scope.chamarFuncao = funcao;
				
			}
    	});
    }      

    return {
    	closeModalErrors: _closeModalErrors,
    	dialogModal: _dialogModal
    }

}]);