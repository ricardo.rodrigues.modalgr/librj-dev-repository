app.factory('cadastroConteinerService',['$http','$q',
	function($http, $q) {
	
		function AllowAccesServiceAPI() {
			$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
			// Send this header only in post requests.
			// Specifies you are sending a JSON object
			$http.defaults.headers.post['dataType'] = 'json';
			$http.defaults.headers.post['Content-Type'] = 'application/json';
		}
	
		var _getBooking = function(apiUrl, bookingNbr) {
			AllowAccesServiceAPI();
			return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/getbooking?bookingNbr=" + bookingNbr);
		}
	
		var _validaUnitId = function(apiUrl,
				numeroConteiner) {
			AllowAccesServiceAPI();
			return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/validaunitid?numeroConteiner=" + numeroConteiner);
		}
	
		var _salvaConteiner = function(apiUrl, dto) {
			AllowAccesServiceAPI();
			var config = {
				headers : {
					"Content-Type" : "application/json;charset=UTF-8"
				}
			}
	
			return $http.post("/DueRedexCadastroConteinerPortlet/dueredex/api/saveconteiner",dto, config);
		}
	
		var _validaInclusaoDanfe = function(apiUrl, chave,
				unitGkey, numero) {
			AllowAccesServiceAPI();
	
			return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/validainclusaodanfe?chave="
							+ chave
							+ "&unitgkey="
							+ unitGkey
							+ "&numero="
							+ numero);
		}
	
		var _validaImpressaoRecibo = function(apiUrl,
				unitGkey) {
			AllowAccesServiceAPI();
			return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/validaimpressaorecibo?gkey=" + unitGkey);
		}
		
	    var _imprimeRecibo = function(unitGkey, numeroConteiner, cnpjs) {
	    	AllowAccesServiceAPI();
	    	return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/imprimeRecibo?unitGkey=" + unitGkey + "&numeroConteiner=" + numeroConteiner + "&cnpjs=" + cnpjs);
	    }		
	
		var _excluirDanfe = function(apiUrl, dto) {
			AllowAccesServiceAPI();
			var config = {
				headers : {
					"Content-Type" : "application/json;charset=UTF-8"
				}
			}
	
			return $http.post("/DueRedexCadastroConteinerPortlet/dueredex/api/deletedanfe", dto, config);
		}
	
		var _getTransportadoraInfo = function(apiUrl, cnpjs) {
			AllowAccesServiceAPI();
			return $http.get("/DueRedexCadastroConteinerPortlet/dueredex/api/gettransportadorainfo?cnpjs=" + cnpjs);
		}
	
		var _deleteConteiner = function(apiUrl, dto) {
			AllowAccesServiceAPI();
			var config = {
				headers : {
					"Content-Type" : "application/json;charset=UTF-8"
				}
			}
	
			return $http.post("/DueRedexCadastroConteinerPortlet/dueredex/api/deleteconteiner", dto, config);
		}
	
		return {
			getBooking : _getBooking,
			salvaConteiner : _salvaConteiner,
			validaInclusaoDanfe : _validaInclusaoDanfe,
			excluirDanfe : _excluirDanfe,
			getTransportadoraInfo : _getTransportadoraInfo,
			validaImpressaoRecibo : _validaImpressaoRecibo,
			deleteConteiner : _deleteConteiner,
			validaUnitId : _validaUnitId,
			imprimeRecibo : _imprimeRecibo
		}
	
	} 
]);