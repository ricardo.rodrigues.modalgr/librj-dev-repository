app.factory("FormataCampo", [function(){
	return function(cnpj){
		
		if (cnpj==undefined) return;
		
		let exp = /\-|\.|\/|\(|\)|\[|\]| /g;
		
		let cnpjFormat = cnpj.replace( exp, "" ); 
		
		cnpjFormat = cnpjFormat.replace('"','');
		cnpjFormat = cnpjFormat.replace('"','');
		
		return cnpjFormat;
	}
}]);