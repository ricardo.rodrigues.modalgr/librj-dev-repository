<div class="modal-backdrop" ng-class="{'mostrarformulario': showDialogCadastroConteiner == true, 'ocultarformulario': showDialogCadastroConteiner == false}" style="opacity: 1 !important;">
	<div class="alert alert-block alert-warning  fade in" style="opacity: 1; position: absolute; left: 12%; top: 0%; width: 75%; height: 100%; color: black; overflow-y: auto; background-color: white; border: 0;">
			
		<div class="row-fluid" style="margin-bottom: 2%;">
			<label class="span4"><b>CADASTRO DE CONTEINER</b></label> 
			<i class="fa fa-times" style="float: right; font-size: large; cursor: pointer;" ng-click="closeDialogCadastroConteinerAtualiza()"></i>
		</div>
		
			
		<div class="row-fluid">
			<label class="span3" style="float: left; margin: 0px;">NUMERO DO CONTEINER</label> 
			<input type="text" style="float: left; border-radius: 12px; margin: 0px;" class="input-block-level span4" 
				value="{{conteinerCadastro.numeroConteiner}}" ng-model="conteinerCadastro.numeroConteiner" 
				ng-disabled=" conteinerCadastro.gkey != null && conteinerCadastro.gkey != '' " ng-change="desabilitaFormCadastroConteiner()" capitalize />
			
			<button style="float: left; width: 90px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-left: 10px;" title="Validar N�mero de Conteiner" class="btn-primary span2" ng-click="validaUnitId()">
	        	<i class="fa fa-check" aria-hidden="true"></i> Validar 
	        </button>
		</div>
		<div class="row-fluid" style="margin: 0px;" ng-show="!liberaCadastroUnit">
			<label class="span12" style="color: red;">
				<b style="color: red;font-size: smaller;">Insira o n�mero do conteiner e clique em validar para prosseguir com o cadastro.</b>
			</label> 
		</div>
		<div class="row-fluid" style="margin: 0px;" ng-show="liberaCadastroUnit">
			<label class="span12" style="color: red;">
				<b style="color: green; font-size: smaller;">N�mero de conteiner v�lido para cadastro.</b>
			</label> 
		</div>
		
	
		<div class="row-fluid" ng-show="itemList.length == 1">
			<label class="span3">TIPO DO CONTEINER</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span4" value="{{itemList[0].descricao}}" ng-disabled="true" />
			<label class="span3" ng-show="conteinerCadastro.gkey == null">Quantidade : {{itemList[0].quantidade - itemList[0].disponivel}} de {{itemList[0].quantidade}}</label>
		</div>
		
		<div class="row-fluid" ng-show="itemList.length > 1">
			<label class="span3" style="float: left;">TIPO DO CONTEINER</label> 
			<select style="height: 25px; margin-top: 6px; float: left;"
				class="input-xlarge search-query span4" id="tipo" name="tipo" ng-disabled="conteinerCadastro.gkey != null"
				ng-model="itemSel" ng-options="item as (item.iso + ' - ' + item.descricao) for item in itemList track by item.itemGkey" onchange="selItemOnChange()">
			</select>
			<i class="fa fa-question-circle-o" aria-hidden="true" style="float: left;" title="Tipo do Conteiner n�o pode ser alterado.
	Caso esteja cadastrado com o tipo errado, exclua e crie novamente o Conteiner." ng-show="conteinerCadastro.gkey != null"></i>
			<label class="span3" style="float: left;" ng-show="itemSelecionado && conteinerCadastro.gkey == null">Quantidade : {{itemSel.quantidade - itemSel.disponivel}} de {{itemSel.quantidade}}</label>
		</div>
	
		<div class="row-fluid">
			<label class="span3">PESO BRUTO(KG)</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.pesoBruto}}" ng-model="conteinerCadastro.pesoBruto" ng-disabled="!liberaCadastroUnit" somentenumeros />
		</div>
	
		<h5>LACRES</h5>
	
		<div class="row-fluid">
			<label class="span2" style="float: left;">LACRE 1</label> 
			<input type="text" id="lacre1" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.lacre1}}" ng-model="conteinerCadastro.lacre1" ng-disabled="!liberaCadastroUnit" capitalize />
			<label class="span2" style="float: left;">LACRE 2</label> 
			<input type="text" id="lacre2" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.lacre2}}" ng-model="conteinerCadastro.lacre2" ng-disabled="!liberaCadastroUnit" capitalize />
		</div>
	
		<div class="row-fluid">
			<label class="span2" style="float: left;">LACRE 3</label> 
			<input type="text" id="lacre3" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.lacre3}}" ng-model="conteinerCadastro.lacre3" ng-disabled="!liberaCadastroUnit" capitalize />
			<label class="span2" style="float: left;">LACRE 4</label> 
			<input type="text" id="lacre4" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.lacre4}}" ng-model="conteinerCadastro.lacre4" ng-disabled="!liberaCadastroUnit" capitalize />
		</div>
	
		<h5>EXCESSO (CM)</h5>
	
		<div class="row-fluid">
			<label class="span2" style="float: left;">FRONTAL</label> 
			<input type="text" id="frontal" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.excessoFrontal}}" ng-model="conteinerCadastro.excessoFrontal" ng-disabled="!liberaCadastroUnit" />
			<label class="span2" style="float: left;">TRASEIRO</label> 
			<input type="text" id="traseiro" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.excessoTraseiro}}" ng-model="conteinerCadastro.excessoTraseiro" ng-disabled="!liberaCadastroUnit" />
		</div>
	
		<div class="row-fluid">
			<label class="span2" style="float: left;">DIREITA</label> 
			<input type="text" id="dreita" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.excessoDireita}}" ng-model="conteinerCadastro.excessoDireita" ng-disabled="!liberaCadastroUnit" />
			<label class="span2" style="float: left;">ESQUERDA</label> 
			<input type="text" id="esquerda" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.excessoEsquerda}}" ng-model="conteinerCadastro.excessoEsquerda" ng-disabled="!liberaCadastroUnit" />
		</div>
	
		<div class="row-fluid">
			<label class="span2">ALTURA</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span2" 
				value="{{conteinerCadastro.excessoAltura}}" ng-model="conteinerCadastro.excessoAltura" ng-disabled="!liberaCadastroUnit" />
		</div>
	
		<h5>CADASTRO DANFE</h5>
	
		<div class="row-fluid"> 
			<label class="span2">TIPO DE DOCUMENTO</label>
			<input type="text" value="NF" style="border-radius: 12px;" class="input-block-level span2" ng-disabled="true" /> 
		</div>
		
		<div class="row-fluid"> 
			<label class="span2">STATUS</label> 
			<input type="text" value="CARGA A DESEMBARA�AR" style="border-radius: 12px;" class="input-block-level span4" ng-disabled="true" /> 
		</div>
	
		<div class="control-group">
			<label class="span2" style="float: left;">NUMERO</label> 
			<input type="text" style="float: left; border-radius: 12px;" class="input-block-level span2" 
				value="{{cadastroDanfe.numero}}" ng-model="cadastroDanfe.numero" ng-disabled="!liberaCadastroUnit" somentenumeros />
			<label class="span2" style="float: left;">CHAVE</label>
			<input type="text" style="float: left; border-radius: 12px;" class="input-block-level span5" 
				value="{{cadastroDanfe.chave}}" ng-model="cadastroDanfe.chave" id="danfe" ng-disabled="!liberaCadastroUnit" somentenumeros />
			 
			<button style="float: left; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px;"
				title="Incluir DANFE" class="btn-primary span2" ng-click="addDanfe()" ng-disabled="!liberaCadastroUnit">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</button>
			
		</div>
	
		<div class="row-fluid">
	
			<div class="span12">
	
				<table class="tale table-bordered" style="width: 100%;">
					<thead>
						<tr>
							<th style="text-align: center;">NUMERO</th>
							<th style="text-align: center;">CHAVE</th>
							<th style="text-align: center;">EXCLUIR</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="danfe in conteinerCadastro.danfeList">
							<td style="text-align: center;">{{danfe.numero}}</td>
							<td style="text-align: center;">{{danfe.chave}}</td>
							<td style="text-align: center;">
								<a style='cursor: pointer;'><i class='fa fa-times' aria-hidden='true' ng-click="excluirDanfe(danfe)" ng-disabled="!liberaCadastroUnit"></i></a>
							</td>
						</tr>
					</tbody>
				</table>
	
			</div>
	
		</div>
		
		<hr style="border-width: 2px;" />
	
		<div class="row-fluid">
	
			<div class="span1"></div>
	
			<div class="span12" style="margin-left: -10%;">
	
				<button style="float: right; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-right: -4%;"
					title="Salvar Conteiner" class="btn-primary span2" ng-click="salvaConteiner()" ng-disabled="!liberaCadastroUnit">
					<i class="fa fa-check" aria-hidden="true"></i>
				</button>
				
				<button style="float: right; width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px;"
					title="Cancelar" class="btn-primary span2" ng-click="closeDialogCadastroConteinerAtualiza()">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>
	
			</div>
	
		</div>
	
	</div>
</div>