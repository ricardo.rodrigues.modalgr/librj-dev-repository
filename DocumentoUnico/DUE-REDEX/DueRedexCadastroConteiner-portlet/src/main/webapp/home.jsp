<%@ include file="init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
if(hasPermissionDUE) {
%>
<div ng-app="App" ng-controller="MasterCtrl">
	
	<link rel="stylesheet" href="/DueRedexCadastroConteinerPortlet/css/main.css" />
	<link rel="stylesheet" href="/DueRedexCadastroConteinerPortlet/css/main.min.css" />

<% 
Group liveGroup = (Group)request.getAttribute("site.liveGroup"); 
%>

<script>
function selItemOnChange() {
	angular.element(document.getElementById('tipo')).scope().selItem();
}
</script>

	<div id="divUrlApi" style="visibility: hidden">
		<liferay-ui:custom-attribute
			className="com.liferay.portal.model.Group"
			classPK="<%= (liveGroup != null) ? liveGroup.getGroupId() : 0 %>"
			editable="<%= false %>" label="<%= false %>" name="dueRedexApiUrl" />
	</div>
	
	<input type="hidden" id="usuario" name="usuario" value='${nmUserLogado}' /> 
	<input type="hidden" id="cpf" name="cpf" value='${cpf}' /> 
	<input type="hidden" id="cnpjs" name="cnpjs" value='${cnpjs}' />
	<input type="hidden" id="nomeTransportadora" name="nomeTransportadora" value='${nomeTransportadora}' /> 
	<input type="hidden" id="validado" name="validado" value='${validado}' /> 
	<input type="hidden" id="hasPermissionDUE" name="hasPermissionDUE" value='${hasPermissionDUE}' /> 
	<input type="hidden" id="hasPermissionDespachante" name="hasPermissionDespachante" value='${hasPermissionDespachante}' /> 
	<input type="hidden" id="hasPermissionSupervisor" name="hasPermissionSupervisor" value='${hasPermissionSupervisor}' />

	<div id="page-wrapper" class="container-fluid">
		<div class="row-fluid">
			<label class="span1">Transportadora</label>
			<div class="span5">
				<input type="text" style="border-radius: 12px;" value="{{nomeTransportadora}}" ng-disabled="true">
			</div>
		</div>
		<div class="row-fluid">
			<label class="span1">Booking</label>
			<div class="span5">
				<input type="text" style="border-radius: 12px;" ng-model="bookingNbr" ng-enter="getBooking()" capitalize>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span2">
                <button style="width: 30px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-bottom: 3%;" title="Pesquisar" class="btn-primary span2" ng-click="getBooking()">
                    <i class="icon-search"></i>
                </button>
            </div>
		</div>
		
		<div id="content" ng-show="showContent" ng-cloak>
		
			<div class="well well-small">
				<h5 style="color: #363E41;">Informações do Booking</h5>
				<div class="row-fluid">
					<label class="span2">VIAGEM: </label>
					<label class="span2">{{booking.viagem}}</label>
					<label class="span2">NAVIO: </label>
					<label class="span2">{{booking.vesselName}}</label>
					<label class="span2">PORTO DE DESCARGA: </label>
					<label class="span2">{{booking.pol}}</label>				
				</div>				
				<div class="row-fluid">
					<label class="span2">PORTO DE DESTINO: </label>
					<label class="span2">{{booking.pod1}}</label>
					<label class="span2">PORTO DE DESTINO 2: </label>
					<label class="span2">{{booking.pod2}}</label>
					<label class="span2">ABERTURA DO GATE: </label>
					<label class="span2">{{booking.gate}}</label>
				</div>
				<div class="row-fluid">
					<label class="span2">DEADLINE IMO: </label>
					<label class="span2">{{booking.imo}}</label>
					<label class="span2">DEADLINE REFEER: </label>
					<label class="span2">{{booking.refeer}}</label>
					<label class="span2">DEADLINE DRY: </label>
					<label class="span2">{{booking.dry}}</label>
				</div>
	
			</div>
	
			<h5 style="color: #363E41;">CONTEINERS CADASTRADOS DO BOOKING</h5>
			<div class="well well-small">
				
				<div class="row-fluid">
					<label class="span2">QTD DO BOOKING: </label>
					<label class="span3">{{booking.conteinerList == null ? '0' : booking.conteinerList.length}} de {{booking.qtd}}</label>					
				</div>
				
				<h5 style="color: #363E41;" ng-show="booking.itemList.length == 1">Item do Booking</h5>
				<h5 style="color: #363E41;" ng-show="booking.itemList.length > 1">Itens do Booking</h5>
				
				<div class="row-fluid" ng-repeat="item in booking.itemList">
					<label class="span1" ng-show="item.itemGkey > 0">DESCRIÇÃO: </label>
					<label class="span2" ng-show="item.itemGkey > 0">{{item.descricao}}</label>
					<label class="span1" ng-show="item.itemGkey > 0">ISO: </label>
					<label class="span2" ng-show="item.itemGkey > 0">{{item.iso}}</label>
					<label class="span2" ng-show="item.itemGkey > 0">QTD DO ITEM: </label>
					<label class="span2" ng-show="item.itemGkey > 0">{{item.quantidade - item.disponivel}} de {{item.quantidade}}</label>
				</div>
				
				<hr style="background-color: black; color: black; border-top: 1px solid;">
		
				<div class="row-fluid">
		
					<div class="span12">
		
						<table class="tale table-bordered" style="width: 100%;">
							<thead>
								<tr>
									<th style="text-align: center;">NUMERO CONTEINER</th>
									<th style="text-align: center;">ISO</th>
									<th style="text-align: center;">REEFER</th>
									<th style="text-align: center;">IMO</th>
									<th style="text-align: center;">DOC</th>
									<th style="text-align: center;">IMPRIMIR RECIBO</th>
									<th style="text-align: center;">EDITAR</th>
									<th style="text-align: center;">EXCLUIR</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="conteiner in booking.conteinerList">
								
									<td style="text-align: center;">{{conteiner.numeroConteiner}}</td>
									<td style="text-align: center;">{{conteiner.iso}}</td>
									<td style="text-align: center;">{{conteiner.reefer}}</td>
									<td style="text-align: center;">{{conteiner.imo}}</td>
									<td style="text-align: center;"><div ng-bind-html="filtradoc(conteiner.doc)"></div></td>
									
									<td style="text-align: center;">
										<a title="Imprimir Recibo" style='cursor: pointer;' ng-click="imprimeRecibo(conteiner)" ng-show="conteiner.doc != null && conteiner.doc != ''"><i class='fa fa-print' aria-hidden='true'></i></a>
									</td>
									<td style="text-align: center;">
										<a title="Editar Contêiner" style='cursor: pointer;' ng-click="editConteiner(conteiner)"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
									</td>
									<td style="text-align: center;">
										<a title="Excluir Contêiner" style='cursor: pointer;' ng-click="deleteConteiner(conteiner)"><i class='fa fa fa-times' aria-hidden='true'></i></a>
									</td>
								</tr>
							</tbody>
						</table>
		
					</div>
		
				</div>
				
				<div class="row-fluid">
					<button style="width: 190px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-top: 3%;" title="Incluir Contêiner" class="btn-primary span2" ng-click="novoConteiner()">
	                    <i class="icon-plus"></i>
	                    INCLUIR CONTÊINER
	                </button>
				</div>
				
			</div>
			
		</div>
		
		<div class="modal-backdrop" ng-show="showDialogCadastroConteiner" style="opacity: 1 !important;">
			<div class="alert alert-block alert-warning fade in" style="opacity: 1; position: absolute; left: 12%; top: 0%; width: 75%; height: 100%; color: black; overflow-y: none; background-color: white; border: 0;" ng-cloak>
				<%@ include file="paginas/cadastroConteinerModal.jsp"%>						
			</div>
		</div>		

	</div>

</div>

<% } else {%>
<div class="alert alert-error">Você não tem permissão de acessar
	essa funcionalidade</div>
<% } %>