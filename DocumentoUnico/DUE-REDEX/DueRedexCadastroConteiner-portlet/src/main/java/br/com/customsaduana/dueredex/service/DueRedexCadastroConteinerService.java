package br.com.customsaduana.dueredex.service;

import java.io.FileOutputStream;
import java.net.URI;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.customsaduana.dueredex.dto.CadastroConteinerDTO;
import br.com.customsaduana.dueredex.dto.ConteinerDTO;
import br.com.customsaduana.dueredex.dto.DanfeDTO;
import br.com.customsaduana.dueredex.util.PropertiesUtil;


@Service
public class DueRedexCadastroConteinerService {

	@Autowired
	private RestTemplate rest;
	
	private HttpHeaders getHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	public CadastroConteinerDTO validaUnitId(String numeroConteiner) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validaunitid");
		builder.queryParam("numeroConteiner", numeroConteiner);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO saveConteiner(CadastroConteinerDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/saveconteiner");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<CadastroConteinerDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO getBooking(String bookingNbr) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/getbooking");
		builder.queryParam("bookingNbr", bookingNbr);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO validaInclusaoDanfe(String chave, String unitgkey, String numero) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validainclusaodanfe");
		builder.queryParam("chave", chave);
		builder.queryParam("unitgkey", unitgkey);
		builder.queryParam("numero", numero);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO validaImpressaoRecibo(String gkey) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validaimpressaorecibo");
		builder.queryParam("gkey", gkey);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO deleteDanfe(DanfeDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/deletedanfe");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<DanfeDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}

	public ConteinerDTO getTransportadoraInfo(String cnpjs) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/gettransportadorainfo");
		builder.queryParam("cnpjs", cnpjs);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, ConteinerDTO.class);
		return responseEntity.getBody();
	}

	public CadastroConteinerDTO deleteUnit(ConteinerDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/deleteconteiner");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<ConteinerDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<CadastroConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroConteinerDTO.class);
		return responseEntity.getBody();
	}
	
	public String imprimeRecibo(Long unitGkey, String numeroConteiner, String cnpjs) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/reciboconteiner");
		builder.queryParam("unitGkey", unitGkey);
		builder.queryParam("numeroConteiner", numeroConteiner);
		builder.queryParam("cnpjs", cnpjs);
		URI urlFormatterd = builder.build().encode().toUri();
		
		HttpHeaders headers = new HttpHeaders();

	    headers.add("Content-Description", "File Transfer");
	    headers.add("Content-Disposition", "attachment; filename=recibo.pdf");
	    headers.add("Content-Transfer-Encoding", "binary");
	    headers.add("Connection", "Keep-Alive");
	    headers.setContentType(
	    		MediaType.parseMediaType("application/pdf"));
		
		headers.setAccept(Arrays.asList(MediaType.ALL));
		
		HttpEntity entity = new HttpEntity(headers);

		rest.getMessageConverters().add(new ByteArrayHttpMessageConverter());    

		FileOutputStream out = null;
		ResponseEntity<byte[]> result = rest.exchange(urlFormatterd, HttpMethod.GET, entity, byte[].class);
		
		String encoded = Base64.encodeBase64String(result.getBody());
		return encoded;
		
		
	}
	
}
