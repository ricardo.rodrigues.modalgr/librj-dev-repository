package br.com.customsaduana.dueredex.dto;

public class DanfeDTO {
	
	public DanfeDTO() {
		super();
	}
	
	public DanfeDTO(Object[] o) {
		this.numero = (o[0] != null && !"".equals(o[0])) ? o[0].toString() : "";
		this.chave  = (o[1] != null && !"".equals(o[1])) ? o[1].toString() : "";
		this.id		= (o[2] != null && !"".equals(o[2])) ? o[2].toString() : "";
	}
	
	private String id;
	
	private String numero;
	
	private String chave;

	public String getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public String getChave() {
		return chave;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	@Override
	public String toString() {
		return "DanfeDTO [id=" + id + ", numero=" + numero + ", chave=" + chave + "]";
	}
}
