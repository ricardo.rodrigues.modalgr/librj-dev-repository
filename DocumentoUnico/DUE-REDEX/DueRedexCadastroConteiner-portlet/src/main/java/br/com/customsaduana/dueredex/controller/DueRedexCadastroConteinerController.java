package br.com.customsaduana.dueredex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.customsaduana.dueredex.dto.CadastroConteinerDTO;
import br.com.customsaduana.dueredex.dto.ConteinerDTO;
import br.com.customsaduana.dueredex.dto.DanfeDTO;
import br.com.customsaduana.dueredex.service.DueRedexCadastroConteinerService;

@RestController
@RequestMapping("/dueredex")
public class DueRedexCadastroConteinerController {
	
	@Autowired
	private DueRedexCadastroConteinerService dueRedexCadastroConteinerService;
	
	@RequestMapping(value = "/api/validaunitid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaUnitId(@RequestParam("numeroConteiner") String numeroConteiner) {
		return dueRedexCadastroConteinerService.validaUnitId(numeroConteiner);
	}

	@RequestMapping(value = "/api/saveconteiner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO saveConteiner(@RequestBody(required = true) CadastroConteinerDTO dto) {
		return dueRedexCadastroConteinerService.saveConteiner(dto);
	}

	@RequestMapping(value = "/api/getbooking", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO getBooking(@RequestParam("bookingNbr") String bookingNbr) {
		return dueRedexCadastroConteinerService.getBooking(bookingNbr);
	}
	
	@RequestMapping(value = "/api/validainclusaodanfe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaInclusaoDanfe(@RequestParam("chave") String chave, @RequestParam("unitgkey") String unitgkey, @RequestParam("numero") String numero) {
		return dueRedexCadastroConteinerService.validaInclusaoDanfe(chave, unitgkey, numero);
	}
	
	@RequestMapping(value = "/api/validaimpressaorecibo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO validaImpressaoRecibo(@RequestParam("gkey") String gkey) {
		return dueRedexCadastroConteinerService.validaImpressaoRecibo(gkey);
	}
	
	@RequestMapping(value = "/api/deletedanfe", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO deleteDanfe(@RequestBody(required = true) DanfeDTO dto) {
		return dueRedexCadastroConteinerService.deleteDanfe(dto);
	}
	
	@RequestMapping(value = "/api/gettransportadorainfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConteinerDTO getTransportadoraInfo(@RequestParam("cnpjs") String cnpjs) {
		return dueRedexCadastroConteinerService.getTransportadoraInfo(cnpjs);
	}
	
	@RequestMapping(value = "/api/deleteconteiner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroConteinerDTO deleteUnit(@RequestBody(required = true) ConteinerDTO dto) {
		return dueRedexCadastroConteinerService.deleteUnit(dto);
	}
	
	@RequestMapping(value = "/api/imprimeRecibo", method = RequestMethod.GET)
	@ResponseBody	
	public String imprimeRecibo(@RequestParam("unitGkey") Long unitGkey, @RequestParam("numeroConteiner") String numeroConteiner, @RequestParam("cnpjs") String cnpjs) {
		return dueRedexCadastroConteinerService.imprimeRecibo(unitGkey, numeroConteiner, cnpjs);
	}
	
	
	
}