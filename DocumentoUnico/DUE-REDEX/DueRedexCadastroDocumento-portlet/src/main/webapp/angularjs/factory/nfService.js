app.factory('nfService', ['$http', '$q',
	function($http, $q) {

	function AllowAccesServiceAPI() {
		$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		$http.defaults.headers.post['dataType'] = 'json';
		$http.defaults.headers.post['Content-Type'] = 'application/json';
	}
	
    var _existeCadastroShipper = function(apiUrl, cnpj) {
    	AllowAccesServiceAPI();
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/existecadastroshipper?cnpj=" + cnpj);
    }
    
    var _buscaEmbalagens = function() {
        AllowAccesServiceAPI();
        return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscaembalagens");
    }
    
    var _buscaCommodities = function() {
    	AllowAccesServiceAPI();
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscacommodities");
    }
    
    var _salvaDocumento = function(apiUrl, dto) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post( "/DueRedexCadastroDocumentoPortlet/dueredex/api/salvadocumento", dto, config );
    }
    
    var _validaModificacaoDocumento = function(apiUrl, dto) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post("/DueRedexCadastroDocumentoPortlet/dueredex/api/validamodificacaodocumento", dto, config );
    }
    
    
    var _buscaDocumentosTransportadora = function(apiUrl, cnpj, tipo, pageindex, pagesize) {
    	AllowAccesServiceAPI();
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscadocumentostransportadora?cnpj=" + cnpj + "&tipo=" + tipo + "&pageindex=" + pageindex + "&pagesize=" + pagesize);
    }
      
    
    var _excluirDocumento = function(apiUrl, documento) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post("/DueRedexCadastroDocumentoPortlet/dueredex/api/excluirdocumento", documento, config );
    }
  
	var _buscaDocumentosTransportadoraFiltro = function(dto) {
		AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}

    	return $http.post("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscadocumentostransportadorafiltro", dto, config );
    }

    

    return {
    	existeCadastroShipper : _existeCadastroShipper,
    	salvaDocumento : _salvaDocumento,
    	buscaDocumentosTransportadora : _buscaDocumentosTransportadora,
    	buscaEmbalagens : _buscaEmbalagens,
    	buscaCommodities : _buscaCommodities,
    	excluirDocumento : _excluirDocumento,
    	validaModificacaoDocumento : _validaModificacaoDocumento,
    	buscaDocumentosTransportadoraFiltro: _buscaDocumentosTransportadoraFiltro
    	
    }

}]);