app.factory('motoristaReciboService', ['$http', '$q',
	function($http, $q) {

	function AllowAccesServiceAPI() {
		$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		// Send this header only in post requests.
		// Specifies you are sending a JSON object
		$http.defaults.headers.post['dataType'] = 'json';
		$http.defaults.headers.post['Content-Type'] = 'application/json';
	}
	
    var _buscaDocumentosDisponiveis = function(apiUrl, cnpj, novo) {
    	AllowAccesServiceAPI();
        return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscadocumentosdisponiveis?cnpj=" + cnpj + "&novo=" + novo);
        
    }
    
    var _buscaTransportadora = function(apiUrl, cnpj) {
    	AllowAccesServiceAPI();
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscatransportadora?cnpj=" + cnpj);
    }
    
    var _buscaMotorista = function(apiUrl, searchDoc, isEstrangeiro) {
    	AllowAccesServiceAPI();
        return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/buscamotorista?searchDoc=" + searchDoc + "&isEstrangeiro=" + isEstrangeiro);
    }
    
    var _salvaRecibo = function(apiUrl, dto) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post( "/DueRedexCadastroDocumentoPortlet/dueredex/api/salvarecibo", dto, config );
    }
    
    var _validaModificacaoDocumentos = function(apiUrl, documentos) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post( "/DueRedexCadastroDocumentoPortlet/dueredex/api/validamodificacaodocumentos", documentos, config );
    }	
    
    var _excluirRecibo = function(apiUrl, transporte) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post( "/DueRedexCadastroDocumentoPortlet/dueredex/api/excluirrecibo", transporte, config );
    }
    
    var _validaImpressaoRecibo = function(apiUrl, transporte) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
    	return $http.post( "/DueRedexCadastroDocumentoPortlet/dueredex/api/validaimpressaorecibo", transporte, config );
    }
    
    var _imprimeRecibo = function(id) {
    	AllowAccesServiceAPI();
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/recibo?idTransporte=" + id);
    }
    
    return {
    	buscaTransportadora : _buscaTransportadora,
    	buscaDocumentosDisponiveis : _buscaDocumentosDisponiveis,
    	buscaMotorista : _buscaMotorista,
    	salvaRecibo : _salvaRecibo,
    	validaModificacaoDocumentos : _validaModificacaoDocumentos,
    	excluirRecibo : _excluirRecibo,
    	validaImpressaoRecibo : _validaImpressaoRecibo,
    	imprimeRecibo : _imprimeRecibo
    }

}]);