app.factory('transportadoraService', ['$http', '$q',
	function($http, $q) {

	function AllowAccesServiceAPI() {
		$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		// Send this header only in post requests.
		// Specifies you are sending a JSON object
		$http.defaults.headers.post['dataType'] = 'json';
		$http.defaults.headers.post['Content-Type'] = 'application/json';
	}
	
    var _getTransportadoraInfo = function(apiUrl, cnpjs) {
    	AllowAccesServiceAPI();
//        return $http.get(apiUrl + "/api/gettransportadorainfo?cnpjs=" + cnpjs);
    	return $http.get("/DueRedexCadastroDocumentoPortlet/dueredex/api/gettransportadorainfo?cnpjs=" + cnpjs);
    }
    
    return {
    	getTransportadoraInfo 	: _getTransportadoraInfo
    }

}]);