app.directive('onReadFile', function ($parse) {
  return {
	    restrict: 'A',
	    scope: false,
	    link: function(scope, element, attrs) {
	      var fn = $parse(attrs.onReadFile);
	 
	      element.on('change', function(onChangeEvent) {
	        var reader = new FileReader();
	 
	        reader.onload = function(onLoadEvent) {
	          scope.$apply(function() {
	            fn(scope, {$fileContent:onLoadEvent.target.result});
	          });
	        };
	 
	        reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
	      });
	    }
	  };
	});

//app.directive('fileUpload', ['$parse', function($parse) {
//    return {
//      restrict: 'A',
//      link: function(scope, element, attrs) {
//    	  var model = $parse(attrs.fileUpload);
//          var modelSetter = model.assign;
//          
//          element.bind('change', function(){
//             scope.$apply(function(){
//                modelSetter(scope.$parent, element[0].files[0]);
//                scope.$parent.upload();                
//             });
//          });
//      }
//    };
//  }]);
