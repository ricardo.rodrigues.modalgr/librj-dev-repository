app.directive('autocomplete', function($parse) {
                return {
                  restrict: 'E',
                  replace: true,
                  template: '<input type="text"/>',
                  link: function(scope, element, attrs) {
                    scope.$watch(attrs.selection, function(selection) {
                      // event when select item
                      element.on("autocompleteselect", function(e, ui) {
                        e.preventDefault(); // prevent the "value" being written back after we've done our own changes
                        this.value = ui.item.name;
                      });

                      element.autocomplete({
                        source: scope.testeParis,
                        select: function(event, ui) {
                          scope.$apply(function() {
                            $parse(attrs.selection)
                              .assign(scope, {
                                name: ui.item.name,
                                id: ui.item.id,
                              });
                          });
                        }
                      }) // to display name instead of default label attribute
                      .data("ui-autocomplete")._renderItem = function(ul, item) {
                        return $("<li></li>")
                          .data("item.autocomplete", item)
                          .append("<a>" + item.name + "</a>")
                          .appendTo(ul);
                      };


                    });
                  }
                }
});
                