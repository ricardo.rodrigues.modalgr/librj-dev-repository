app.controller('MasterCtrl', ['$scope', '$location', '$rootScope', '$modal', 'transportadoraService','FormataCampo',
	function($scope, $location, $rootScope, $modal, transportadoraService, FormataCampo) {

        $scope.modalContainer = "";
        
        $scope.apiURL = $(document.getElementById("divUrlApi").childNodes[1]).find('span').html();

        $scope.hasPermissionDUE = $('#hasPermissionDUE').val();
        $scope.hasPermissionDespachante = $('#hasPermissionDespachante').val();
        $scope.hasPermissionSupervisor = $('#hasPermissionSupervisor').val();                
        
        $scope.cnpjs = $('#cnpjs').val();
        $scope.cnpjs = FormataCampo($scope.cnpjs);
        console.log("cnpjs -> " + $scope.cnpjs);
        $scope.nomeTransportadora = $('#nomeTransportadora').val();
        $scope.nomeTransportadora = FormataCampo($scope.nomeTransportadora);
                
        $scope.getTransportadoraInfo = function() {
        	openGifModal();
        	transportadoraService.getTransportadoraInfo($scope.apiURL, $scope.cnpjs).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                closeGifModal();
                $scope.nomeTransportadora = response.data.nomeTransportadora;
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                $scope.Mensagem = response.data;
                $scope.showDialogError = true;
                $scope.showContent = false;
            })
        };
        
        function openGifModal() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DueRedexCadastroDocumentoPortlet/paginas/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }
        
        function closeGifModal() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }
        
        $scope.nf = {
				active: false,
				classe: 'active'
		};
		$scope.motoristaRecibo = {
				active: false,
				classe: 'active'
		};
		
        $scope.setActive = function(idPagina) {
        	        	
        	$scope.nf.active = false;
        	$scope.nf.classe = '';
        	$scope.motoristaRecibo.active = false;
        	$scope.motoristaRecibo.classe = '';
        	if(idPagina == 1) {
        		$scope.nf.active = true;
        		$scope.nf.classe = 'active';
        	}
        	if(idPagina == 2) {
        		$scope.motoristaRecibo.active = true;
        		$scope.motoristaRecibo.classe = 'active';
        	}
        	$rootScope.$broadcast(idPagina);
        };
        
        $scope.initPaginas = function() {
    		$scope.nf.active = true;
    		$scope.motoristaRecibo.active = false;    		
        };
                
        $scope.initPaginas();
        $scope.getTransportadoraInfo();
        
        function convertHTMLtoCaracter(htmlString) {
            return $('<textarea />').html(htmlString).text();
        }   
        
        
    }
]);