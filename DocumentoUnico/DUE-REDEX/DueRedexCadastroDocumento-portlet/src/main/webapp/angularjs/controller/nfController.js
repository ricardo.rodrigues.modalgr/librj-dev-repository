app.controller('NFController', ['$scope', '$location', '$rootScope', '$modal', '$timeout', 'nfService', 'modalService','FormataCampo',
	function($scope, $location, $rootScope, $modal, $timeout, nfService, modalService, FormataCampo) {

		$scope.Mensagem = "";
		$scope.MensagemInfo = "";
		$scope.showModalNF = false;
		$scope.isUpload = false;
		$scope.isNew = false;
		$scope.showNFtable = true;
		$scope.exibeNroDUe = false;
		$scope.exibeNroNF = true;
		$scope.itemsPerPage = 10;
        $scope.qtdRegistros = 0;
        $scope.currentPage = 1;
        $scope.qtdporpaginalist = [{ id: 0, qtd: 10 }, { id: 1, qtd: 25 }, { id: 2, qtd: 50 }, { id: 3, qtd: 100 }];
        $scope.qtdporpagina = { id: 0, qtd: 10 };
        $scope.changeqtdporpagina = function() {        	
            $scope.currentPage = 0;
            $scope.itemsPerPage = $scope.qtdporpagina.qtd;
            $scope.buscaDocumentosTransportadora();
        };		
        
        $scope.optradio = 1;
        $scope.docList = [];
        
        $scope.commodities = [];
        $scope.availableTagsCommodity = [];
        $scope.availableEmbalagemUiTags = [];  
        
        $scope.ncm = {};
        $scope.ncmValorPesquisado = 'NOVO';
        $scope.ncmNfNome = '';
        $scope.ncmOutroNome = '';
        $scope.ncms = [];   
        $scope.ncmTipoPesquisa = '';
        
        $scope.exibeCommodities = false;
        
        $scope.embalagem = {};
        $scope.EmbalagemValorPesquisado = 'NOVO';
        $scope.embalagemNfNome = '';
        $scope.embalagemOutroNome = '';
        $scope.embalagens = [];
        $scope.listaEmbalagemUi = [];
        $scope.embalagemTipoPesquisa = '';
        
        $scope.embalagemNfCodigo = '';
        $scope.embalagemNfNome = '';
        
        $scope.selectedEmb = [];
        
        $scope.TipoDocumentoRadio = 'NF';
        $scope.TipoDocumentoSelect = 'NF';
        
        $scope.showFilters = false;
        
        $scope.filter = {};     
                     
        //$scope.showFilter = function() {
            //$scope.showFilters = !$scope.showFilters;
        //};

        $scope.showFilter = function() {			
//			if(getTipoDocumentoRadio() == undefined || getTipoDocumentoRadio() == null){
//				msg = "Selecione o tipo do documento para habilitar exibição de filtros.";
//            	modalService.closeModalErrors(msg);
//			}else{
//				$scope.showFilters = !$scope.showFilters;
//			}		
        	$scope.showFilters = !$scope.showFilters;
        };        
        
        $scope.limparCampos = function(){
			  $scope.filter.booking = "";
			  $scope.filter.danfe = "";
			  $scope.filter.due = "";  
			  $scope.filter.numero = "";
			  $scope.filter.emitente = "";
			  $scope.filter.consolidador = "";
			  $scope.filter.dataCadastroDe = "";
			  $scope.filter.dataCadastroAte = "";
			  $scope.filter.ncmNfCodigo = "";
			  $scope.filter.embalagemNfCodigo = "";
			  $scope.filter.ncmNfNome = "";
			  $scope.filter.embalagemNfNome = "";
				$('#ncm-codigo-nf-filter_value').val("");
				$('#ncm-nome-nf-filter_value').val("");
				$('#embalagem-codigo-nf-filter_value').val("");
				$('#embalagem-nome-nf-filter_value').val("");
		};
		
		$scope.preencheNcmDescricao = function (valor) {			
			$('#ncm-nome-nf_value').val(valor);
			$scope.item.ncmId = $('#ncm-codigo-nf_value').val();
		} 
		$scope.preencheNcmCodigo = function (valor) {			
			$('#ncm-codigo-nf_value').val(valor);
			$scope.item.ncmId = $('#ncm-codigo-nf_value').val();
		} 
		$scope.preencheEmbalagemDescricao = function (valor) {			
			$('#embalagem-nome-nf_value').val(valor);
			$scope.item.embalagemId = $('#embalagem-codigo-nf_value').val();
		} 
		$scope.preencheEmbalagemCodigo = function (valor) {			
			$('#embalagem-codigo-nf_value').val(valor);
			$scope.item.embalagemId = $('#embalagem-codigo-nf_value').val();
		} 
		
		$scope.preencheNcmDescricaoFilter = function (valor) {			
			$('#ncm-nome-nf-filter_value').val(valor);
			$scope.filter.ncmNfCodigo = $('#ncm-codigo-nf-filter_value').val();
		} 
		$scope.preencheNcmCodigoFilter = function (valor) {			
			$('#ncm-codigo-nf-filter_value').val(valor);
			$scope.filter.ncmNfCodigo = $('#ncm-codigo-nf-filter_value').val();
		} 
		$scope.preencheEmbalagemDescricaoFilter = function (valor) {			
			$('#embalagem-nome-nf-filter_value').val(valor);
			$scope.filter.embalagemNfCodigo = $('#embalagem-codigo-nf-filter_value').val();
		} 
		$scope.preencheEmbalagemCodigoFilter = function (valor) {			
			$('#embalagem-codigo-nf-filter_value').val(valor);
			$scope.filter.embalagemNfCodigo = $('#embalagem-codigo-nf-filter_value').val();
		} 		
				
		$scope.filtrarCampos = function(page) {
        	        	     
        	var cnpj = retiraFormatoCnpj($scope.cnpjs);
			console.log(cnpj);

        	if(page != undefined) {
      	       $scope.currentPage = page;
         	} else {
         		$scope.currentPage = 1;
         	}

            $scope.filter.dataCadastroDe = $('#filterDataCadastroDe').val();
            $scope.filter.dataCadastroAte = $('#filterDataCadastroAte').val();                       
        	
        	if(!$scope.isBlank($scope.filter.dataCadastroDe) && !$scope.isBlank($scope.filter.dataCadastroAte)) {            
	            if($scope.periodosInvalidos()) {
	            	return;
	            }
        	}
        	
        	$scope.filter.cnpj = cnpj;
            $scope.filter.pageIndex = $scope.currentPage;
            $scope.filter.pageSize = $scope.itemsPerPage;
            if($scope.filter.tipo == undefined) {$scope.filter.tipo = getTipoDocumentoRadio();} 
            if($scope.filter.numero == undefined) {$scope.filter.numero = "";}   
            if($scope.filter.booking == undefined) {$scope.filter.booking = "";}         
            if($scope.filter.danfe == undefined) {$scope.filter.danfe = "";}
            if($scope.filter.due == undefined) {$scope.filter.due = "";}
            if($scope.filter.emitente == undefined) {$scope.filter.emitente = "";}
            if($scope.filter.consolidador == undefined) {$scope.filter.consolidador = "";}
            if($scope.filter.ncmNfCodigo == undefined) {$scope.filter.ncmNfCodigo = "";}
            if($scope.filter.embalagemNfCodigo == undefined) {$scope.filter.embalagemNfCodigo = "";}
            if($scope.filter.ncmNfNome == undefined) {$scope.filter.ncmNfNome = "";}
            if($scope.filter.embalagemNfNome == undefined) {$scope.filter.embalagemNfNome = "";}
            
            console.log($scope.filter);
            openGifModal();   
            nfService.buscaDocumentosTransportadoraFiltro($scope.filter).then(
                function(response) {
                	closeGifModal();
                    console.log(response.data);
                    $scope.pagedItems = [];
                    $scope.docList = response.data.listaDocumento;
                    $scope.qtdRegistros = response.data.qtdRegistros; 
//                    $scope.limparCampos();
                    if($scope.qtdRegistros == 0){
                    	msg = "Registro não encontrado.";
                    	modalService.closeModalErrors(msg);
                    	$scope.limparCampos();
                    }
                },
                function(error) {
                	closeGifModal();
                    console.log(error);
                    modalService.closeModalErrors(error);
                    $scope.limparCampos();
                })
        };  
        
        $scope.periodosInvalidos = function() {
        	
        	if($scope.isBlank($scope.filter.dataCadastroDe)) {
        		var msg = "Periodo de cadastro inválido.";
                modalService.closeModalErrors(msg);
                return true;
        	}        	
        	
        	if($scope.isBlank($scope.filter.dataCadastroAte)) {
        		var msg = "Periodo de cadastro inválido.";
                modalService.closeModalErrors(msg);
                return true;
        	}        	
        	
        	if(!$scope.isBlank($scope.filter.dataCadastroDe) && !$scope.isBlank($scope.filter.dataCadastroAte)) {

        		var split = $scope.filter.dataCadastroDe.split("/");
        		var dataCadastroDe = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		
        		var split = $scope.filter.dataCadastroAte.split("/");
        		var dataCadastroAte = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		  
        		console.log(dataCadastroDe);
            	console.log(dataCadastroAte);
        		
        		if(dataCadastroDe > dataCadastroAte) {
        			var msg = "Periodo de entrada inválido.";
                    modalService.closeModalErrors(msg);
                    return true;
        		}
        	}
        	                
        	return false;
        };     
        
        $scope.isBlank = function(string) {
    		if(string == undefined) return true;
    		if(string == null) return true;
    		if(string == "null") return true;
    		if(string == "") return true;
    		return false;
    	};
        
		$scope.initModalCadastroVars = function() {
			$scope.doc = {};
			$scope.doc.itens = [];
			$scope.item = {};
			$scope.emitente = {};
			$scope.consolidador = {};
			$scope.bookingDisabeld = true;
			$scope.rucDisabled = false;
			$scope.exibeFormCadastroEmitente = false;
			$scope.exibeFormCadastroConsolidador = false;
			$("#infoRucItem").hide();
			$("#labelDUE").hide();
			
			$scope.availableTagsCommodity = [];
			$scope.availableTagsEmbalagem = [];
			
			$scope.ncmNfCodigo = '';			
	        $scope.ncm = {};
	        $scope.ncmValorPesquisado = 'NOVO';
	        $scope.ncmNfNome = '';
	        $scope.ncmOutroNome = '';
//	        $scope.ncms = [];   
	        $scope.ncmTipoPesquisa = '';
	        $scope.item.ncm = '';
	        
			$scope.embalagemNfCodigo = '';
	        $scope.embalagem = {};
	        $scope.EmbalagemValorPesquisado = 'NOVO';
	        $scope.embalagemNfNome = '';
	        $scope.embalagemOutroNome = '';
//	        $scope.embalagens = [];      
	        $scope.embalagemTipoPesquisa = '';
	        $scope.item.embalagem = '';			
				
			
			$( "input" ).removeClass( "myError" );			        	         

		};
		$scope.initModalCadastroVars();
			
		
		$scope.buscaDocumentosTransportadora = function(tipo, page) {						
			
			$scope.TipoDocumentoRadio = tipo;
			if (tipo=='NF') $scope.optradio = 1;
			
			if(tipo == undefined){
				var tipoDoc = getTipoDocumentoRadio();
				tipo = tipoDoc;
			}
			
			if(page != undefined) {
      	       $scope.currentPage = page;
         	} else {
         		$scope.currentPage = 1;
         	}

			var cnpj = retiraFormatoCnpj($scope.cnpjs);
			console.log(cnpj);
			openGifModal();
			nfService.buscaDocumentosTransportadora($scope.apiURL, cnpj, tipo, $scope.currentPage, $scope.itemsPerPage).then(
				
			function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                
                $scope.pagedItems = [];
                $scope.docList = response.data.listaDocumento;
                $scope.qtdRegistros = response.data.qtdRegistros;
               
                var dto = response.data;
                if(isNF(tipo)){
            		$scope.showNFtable = true;
            	}else {
            		$scope.showNFtable = false;
            	}
                if(dto.erro) {
                	msg = dto.mensagem;
                	modalService.closeModalErrors(msg);
                } else {
                	
                    $scope.docList = dto.listaDocumento;
                	
                }
                closeGifModal();
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                $scope.exibeFormCadastroEmitente = false;
                msg = response.data;
                modalService.closeModalErrors(msg);
            })
		};
		

		$scope.buscaDocumentosTransportadora("NF");
		
		$scope.selTipoDoc = function() {
			
			if(!$scope.isNew){
				var tipo = $('#tipoEdit').val();
			}
			
			if($scope.isNew){
				var tipo = $('#tipo').val();
			}
			
			console.log("TIPO SELECIONADO = " + tipo);
			if(isNF(tipo)) {
				$scope.exibeNroDUe = false;
				$scope.exibeNroNF = true;
			}else{
				$scope.exibeNroDUe = true;
				$scope.exibeNroNF = false;			
			}
		};		
		
		$scope.upload = function($fileContent) {
			
//			openGifModal();
			$scope.isUpload = true;
			$scope.exibeNroDUe = false;
			$scope.exibeNroNF = true;
			
			$('#tipo').val($('#tipo > option:first').val());
			$("#infoRucNF").show();
			$("#infoRucItem").hide();
			$("#labelNF").show();
			$("#labelDUE").hide();
			$scope.rucDisabled = false;
			$("#ruc").prop('disabled', false);
			$("#ruc").removeAttr('disabled');
			
			
			var x2js = new X2JS();
			var json = x2js.xml_str2json( $fileContent );
			console.log("FILE JSON");
			console.log(json);
			
		
			
			$scope.doc.numero 				= json.nfeProc.NFe.infNFe.ide.nNF;
			$scope.emitente.cnpj 			= json.nfeProc.NFe.infNFe.emit.CNPJ;
			$scope.doc.cnpjTransportadora 	= json.nfeProc.NFe.infNFe.transp.transporta.CNPJ;
			$scope.doc.danfe 				= json.nfeProc.protNFe.infProt.chNFe;
			$scope.item.numero 				= json.nfeProc.NFe.infNFe.det._nItem;
			$scope.item.ncmId 				= json.nfeProc.NFe.infNFe.det.prod.NCM;
			$scope.item.quantidade 			= json.nfeProc.NFe.infNFe.transp.vol.qVol;
			$scope.item.embalagem 			= json.nfeProc.NFe.infNFe.transp.vol.esp;
			$scope.item.peso 				= json.nfeProc.NFe.infNFe.transp.vol.pesoB;
			$scope.item.infoAdicional 		= json.nfeProc.NFe.infNFe.infAdic.infCpl;
			
			$('#ncm-codigo-nf_value').val($scope.item.ncmId);
			if ($scope.ncms!=null) {
				for (var m = 0; m < $scope.ncms.length; m++) {
					if ($scope.item.ncmId==$scope.ncms[m].codigo) {
						$('#ncm-nome-nf_value').val($scope.ncms[m].descricao);
						break;
					}
				}
			}
			
//			$('#ncm-nome-nf_value').val($scope.item.ncm);
//			$('#embalagem-codigo-nf_value').val($scope.item.embalagemId);
			$('#embalagem-nome-nf_value').val($scope.item.embalagem);
			if ($scope.embalagens!=null) {
				for (var w = 0; w < $scope.embalagens.length; w++) {
					if ($scope.item.embalagem==$scope.embalagens[w].descricao) {
						$('#embalagem-codigo-nf_value').val($scope.embalagens[w].codigo);
						$scope.item.embalagemId=$scope.embalagens[w].codigo;
						break;
					}
				}
			}
			
			
			$scope.validaEmitente();						
			
			msg = "Documento carregado com sucesso, verifique as informações antes de prosseguir";
			modalService.closeModalErrors(msg);
//			closeGifModal();
		};
		
		$scope.validaDue = function() {
			if (!isDueValid()) {
				msg = "Número da Due inválido";
				modalService.closeModalErrors(msg);
			}							
		}
		
		function isDueValid(){
			var due = $scope.doc.numero;
			
			if (due==undefined) return false;
			if (due=="") return false;
			
    		if (!isNumeric(due.substr(0,2)) || hasNumbers(due.substr(2,2)) || !isNumeric(due.substr(4,20))) 
                return false;    			

			return true;
		}
		
		$scope.saveNF = function() {		
			
			if($scope.isValidFormInclusao()) {
				$scope.doc.cnpjTransportadora = retiraFormatoCnpj($scope.cnpjs);
				
				if(!$scope.isNew){
					var tipo = $('#tipoEdit').val();
				}else {
					var tipo = $('#tipo').val();
				}
				
				if(isNF(tipo) && !isDanfeValid($scope.doc.danfe)) {
					msg = "Chave DANFE inválida.";
					modalService.closeModalErrors(msg);
                    return;
				}	
				
				if(isITEM(tipo) && !isDueValid()) {
					msg = "DUE inválida.";
					modalService.closeModalErrors(msg);
                    return;
				}				
				
				if($scope.emitente.novo){
					if(!isEmail($scope.emitente.email)){
						msg = "Email Emitente inválido.";
						modalService.closeModalErrors(msg);
						return;
					}

					if($scope.emitente.razaoSocial.length <= 4){
						msg = "Razão Social não pode ser menor que 5 caracteres.";
						modalService.closeModalErrors(msg);
						return;
					}
					if($scope.emitente.nomeContato.length <= 4){
						msg = "Nome Contato não pode ser menor que 5 caracteres.";
						modalService.closeModalErrors(msg);
						return;
					}
				}
				
				if($scope.consolidador != null && $scope.consolidador.novo){
					if(!isEmail($scope.consolidador.email)){
						msg = "Email Consolidador inválido.";
						modalService.closeModalErrors(msg);
						return;
					}
						
					if($scope.consolidador.razaoSocial.length <= 4){
						msg = "Razão Social não pode ser menor que 5 caracteres.";
						modalService.closeModalErrors(msg);
						return;
					}
					
					if($scope.consolidador.nomeContato.length <= 4){
						msg = "Nome Contato não pode ser menor que 5 caracteres.";
						modalService.closeModalErrors(msg);
						return;
					}
				}					
				
				
				$scope.doc.tipo = tipo;
				$scope.doc.itens = [];
				$scope.doc.itens.push($scope.item);
				$scope.doc.emitente = $scope.emitente;
				$scope.doc.consolidador = $scope.consolidador;
				console.log($scope.doc);
				
				openGifModal();
				nfService.salvaDocumento($scope.apiURL, $scope.doc).then(function(response) {
                    // SUCCESS 100,101,200,201
                    console.log(response);
                    var dto = response.data;
                    if(dto.erro) {
                    	closeGifModal();
                    	msg = dto.mensagem;
                    	modalService.closeModalErrors(msg);
                    } else {
                    	var dto = response.data;
        				msg = "Documento salvo com sucesso.";
        				modalService.closeModalErrors(msg);
        				closeGifModal();
                        $scope.closeModalNF();
//                        $scope.buscaDocumentosTransportadora(getTipoDocumentoRadio(), $scope.currentPage);
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    var dto = response.data;
                    msg = dto.mensagem;
                    modalService.closeModalErrors(msg);
                })
				
			}
		};
		
		$scope.editDocumento = function(documento){
			
			openGifModal();
			nfService.validaModificacaoDocumento($scope.apiURL, documento).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if(dto.erro) {
                	closeGifModal();
                	msg = dto.mensagem;
                	modalService.closeModalErrors(msg);
                } else {
                	
                	$scope.isNew = false;
        			
        			var comparaTipoDocumento = getTipoDocumentoRadio();
        			
        			if($scope.doc.tipo == comparaTipoDocumento) {
        				$("#tipoEdit").val($scope.doc.tipo);
        			}else {
        				$("#tipoEdit").val(comparaTipoDocumento);
        			}

        			$scope.selTipo();
        			$scope.TipoDocumentoSelect = comparaTipoDocumento;
        			if (comparaTipoDocumento=="ITEM")
        				$scope.exibeNroNF=false;
        			else
        				$scope.exibeNroNF=true;
        			
        			$scope.doc = documento;
        			$scope.emitente = documento.emitente;
        			$scope.consolidador = documento.consolidador;
        		
        			$scope.item.numero = "1";
        			$scope.item = documento.itens[0];
        			$('#ncm-codigo-nf_value').val($scope.item.ncmId);
        			$('#ncm-nome-nf_value').val($scope.item.ncm);
        			$('#embalagem-codigo-nf_value').val($scope.item.embalagemId);
        			$('#embalagem-nome-nf_value').val($scope.item.embalagem);
        			closeGifModal();
        			$scope.showModalNF = true;
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                var dto = response.data;
                msg = dto.mensagem;
                modalService.closeModalErrors(msg);
            })
		};

        $scope.excluirDocumento = function(documento) {
        	$scope.documento = documento;
        	var msg = "Confirma a exclusão do Documento ? ";
        	modalService.dialogModal(msg, $scope.excluirDocumentoConfirm)
        }
        
		$scope.excluirDocumentoConfirm = function() {
        	openGifModal();
        	
        	nfService.validaModificacaoDocumento($scope.apiURL, $scope.documento).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if(dto.erro) {
                	closeGifModal();
                	msg = dto.mensagem;
                	modalService.closeModalErrors(msg);
                } else {
                	
                	 nfService.excluirDocumento($scope.apiURL, $scope.documento).then(
                         	function(response) {
                         		closeGifModal();
                                 console.log(response);
                                 var tipoDoc = getTipoDocumentoRadio();
                                 console.log(tipoDoc);
                                 $scope.buscaDocumentosTransportadora(tipoDoc);
                                 msg = "Documento excluido com sucesso"
                                 modalService.closeModalErrors(msg);
                             },
                             function(error) {
                             	 console.log(error);               
                                  closeGifModal();
                                  msg= error.data.message;
                                  modalService.closeModalErrors(msg);
                             })
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                var dto = response.data;
                msg = dto.mensagem;
                modalService.closeModalErrors(msg);
            })
        };
        

		$scope.isValidFormInclusao = function() {
			
			var isValid = true;
			
			if(!$scope.isNew) {
				var tipo = $('#tipoEdit').val();
			}else {
				var tipo = $('#tipo').val();
			}
			$scope.doc.tipo = tipo;

			if(isNF(tipo)) {
				// NUMERO NF E CHAVE DANFE
				if(isEmpty($scope.doc.numero)) {
					isValid = false;
                    msg= "Número da NF obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.doc.danfe)) {
					isValid = false;
                    msg= "Número da Danfe obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
			}
			// NUMERO DUE
			if(isITEM(tipo)) {
				if(isEmpty($scope.doc.numero)) {
					isValid = false;
                    msg= "Número da Due obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
			}
			// EMITENTE
			if(isEmpty($scope.emitente.cnpj)) {
				isValid = false;
                msg= "Número do CNPJ do Emitente obrigatório";
                modalService.closeModalErrors(msg);
                return;
			}
			// Formulario NOVO EMITENTE (SHIPPER)
			if($scope.exibeFormCadastroEmitente) {
				if(isEmpty($scope.emitente.razaoSocial)) {
					isValid = false;
                    msg= "Razão Social obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.emitente.nomeContato)) {
					isValid = false;
                    msg= "Contato obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.emitente.telefone)) {
					isValid = false;
                    msg= "Telefone obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.emitente.email)) {
					isValid = false;
                    msg= "Email obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
			}
			// Formulario NOVO CONSOLIDADOR (SHIPPER)
			if($scope.exibeFormCadastroConsolidador) {
				if(isEmpty($scope.consolidador.razaoSocial)) {
					isValid = false;
                    msg= "Consolidador obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.consolidador.nomeContato)) {
					isValid = false;
                    msg= "Contato do consolidador obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.consolidador.telefone)) {
					isValid = false;
                    msg= "Telefone do consolidador obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
				if(isEmpty($scope.consolidador.email)) {
					isValid = false;
                    msg= "Email do consolidador obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
			}
			// SE informado consolidador DEVE informar booking
			if($scope.consolidador != null && !isEmpty($scope.consolidador.cnpj)) {
				if(isEmpty($scope.doc.booking)) {
					isValid = false;
                    msg= "Booking obrigatório";
                    modalService.closeModalErrors(msg);
                    return;
				}
			}
			
			// ITEM
			if(isEmpty($scope.item.ncmId)) {
				isValid = false;
                msg= "Ncm obrigatório";
                modalService.closeModalErrors(msg);
                return;
			}
			if(isEmpty($scope.item.quantidade)) {
				isValid = false;
                msg= "Quantidade obrigatório";
                modalService.closeModalErrors(msg);
                return;
			}
		
			if(isEmpty($scope.item.embalagemId)) {
				isValid = false;
                msg= "Embalagem obrigatório";
                modalService.closeModalErrors(msg);
                return;
			}
			
			if(isEmpty($scope.item.peso)) {
				isValid = false;
                msg= "Peso obrigatório";
                modalService.closeModalErrors(msg);
                return;
			}			
			
			return isValid;
		};
		
		$scope.validaDanfe = function(){
			var danfe = $scope.doc.danfe;
			if(danfe == undefined) return;
			if(danfe.length > 0){
				
				if (!isDanfeValid(danfe)){
					msg = "Chave DANFE inválida.";
					modalService.closeModalErrors(msg);
                    $scope.emitente.cnpj = "";
                    $scope.doc.numero = "";
                    $scope.exibeFormCadastroEmitente = false;
					$scope.emitente.novo = false;
				}
				else{					
					console.log("danfe: " + danfe);
					$scope.emitente.cnpj = danfe.slice(6,20);
					console.log("emitente: "+$scope.emitente.cnpj);
					$scope.retornaNotaFiscal(danfe);
					$scope.validaEmitente();
					$("#docNumeroNF").prop('disabled', true);
					$("#cnpjEmitente").prop('disabled', true);
				}	
						
			}			
		}
		
		$scope.retornaNotaFiscal = function(danfe){
			var notaFisc = danfe.slice(25,34);
			notaFisc = notaFisc.replace(/^(0+)(\d)/g,"$2");
			$scope.doc.numero = notaFisc;			
		}		
		
		$scope.validaEmitente = function() {
			if($scope.emitente.cnpj == undefined || $scope.emitente.cnpj.length == 0) {
				$scope.exibeFormCadastroEmitente = false;
			} else if($scope.emitente.cnpj.length > 0){
				
				if($scope.validarCNPJ($scope.emitente.cnpj)) {
					
//					openGifModal();
		        	nfService.existeCadastroShipper($scope.apiURL, $scope.emitente.cnpj).then(function(response) {
		                // SUCCESS 100,101,200,201
		                console.log(response);
//		                closeGifModal();
		                if(response.data == "true") {
		                	$scope.emitente.novo = false;
		                	return;
		                } else {
		                	$scope.exibeFormCadastroEmitente = true;
							msg = "Emitente não cadastrado no sistema, por gentileza, informe os dados abaixo para cadastro.";
							modalService.closeModalErrors(msg);
							$scope.emitente.novo = true;
		                }
		            },
		            function errorCallBack(response) {
		                // ERROR 400,401,500
		                console.log(response);
//		                closeGifModal();
		                $scope.exibeFormCadastroEmitente = false;
		                msg = response.data;
		                modalService.closeModalErrors(msg);
		            })
					
				} else {
					$scope.exibeFormCadastroEmitente = false;
					$scope.emitente.cnpj = "";
					msg = "CNPJ Inválido.";
					modalService.closeModalErrors(msg);
				}
			}
		};
		
		$scope.validaConsolidador = function() {
			if($scope.consolidador.cnpj == undefined || $scope.consolidador.cnpj.length == 0) {
			    $scope.bookingDisabeld = true;
			    $scope.doc.booking = "";
			    $scope.exibeFormCadastroConsolidador = false;
			} else if($scope.consolidador.cnpj.length > 0){
				
				if($scope.validarCNPJ($scope.consolidador.cnpj)) {
					
		        	openGifModal();
		        	nfService.existeCadastroShipper($scope.apiURL, $scope.consolidador.cnpj).then(function(response) {
		                // SUCCESS 100,101,200,201
		                console.log(response);
		                closeGifModal();
		                if(response.data == "true") {
		                	$scope.consolidador.novo = false;
		                	$scope.bookingDisabeld = false;
		                	$scope.exibeFormCadastroConsolidador = false;
		                	return;
		                } else {
		                	$scope.exibeFormCadastroConsolidador = true;
							msg = "Consolidador não cadastrado no sistema, por gentileza, informe os dados abaixo para cadastro.";
							modalService.closeModalErrors(msg);
							$scope.consolidador.novo = true;
		                }
		                $scope.bookingDisabeld = false;
		            },
		            function errorCallBack(response) {
		                // ERROR 400,401,500
		                console.log(response);
		                closeGifModal();
		                $scope.bookingDisabeld = true;
					    $scope.exibeFormCadastroConsolidador = false;
		                msg = response.data;
		                modalService.closeModalErrors(msg);
		            })
		            
				} else {
					$scope.bookingDisabeld = true;
				    $scope.exibeFormCadastroConsolidador = false;
					$scope.consolidador.cnpj = "";
					msg = "CNPJ Inválido.";
					modalService.closeModalErrors(msg);
				}
			}
		};
		
		$scope.selTipo = function() {
			
			if(!$scope.isNew){
				var tipo = $('#tipoEdit').val();
			}
			
			if($scope.isNew){
				var tipo = $('#tipo').val();
			}
			
			console.log("TIPO SELECIONADO = " + tipo);
			if(isNF(tipo)) {
				$scope.rucDisabled = false;			
				
				$("#infoRucNF").show();
				$("#infoRucItem").hide();
					
				$scope.exibeNroNF=true;
			} else {
				$scope.rucDisabled = true;
				
				$("#infoRucNF").hide();
				$("#infoRucItem").show();
				
				$scope.exibeNroNF=false;

			}
		};
		
		$scope.openModalNF = function(isNew) {
			
			$( "input" ).removeClass( "myError" );
			$scope.isNew = isNew;
			$scope.showModalNF = true;
			
			if(isNew) {
				//$('#tipo').val($('#tipo > option:first').val());
				//$("#labelNF").show();
				
				$scope.initModalCadastroVars();
				angular.forEach(
						angular.element("input[type='file']"),function(inputElem) {
							angular.element(inputElem).val(null);
			    });
				$scope.item.numero = "1";
				$scope.selTipo();
			}
			
		};									
		
		$scope.buscaCommodities = function() {				        	        
//			openGifModal();	        	
        	nfService.buscaCommodities().then(function(response) {
        		if (response.data.listaNcmResponses==undefined) return;
        		$scope.ncms = response.data.listaNcmResponses;
        		$scope.ncm = response.data.listaNcmResponses[0];
            }, function (err) {
                msg = "Erro ao buscar lista de NCM/SH.";
                modalService.closeModalErrors(msg);
            }).finally (function () {   
            	if ($scope.ncms) { 
            		console.log("ncms = " + $scope.ncms.length);
            		$scope.exibeCommodities=true;
            	}
//            	closeGifModal();
            });        		
        		        		
		};				
		
		$scope.buscaEmbalagens = function() {			
	        
        	nfService.buscaEmbalagens().then(function(response) {
        		if (response.data.listaEmbalagemResponses==undefined) return;
                $scope.embalagens = response.data.listaEmbalagemResponses;
        		$scope.embalagem = response.data.listaEmbalagemResponses[0];
	        }, function (err) {
	        	msg = "Erro ao buscar lista de embalagens.";
	            modalService.closeModalErrors(msg);
	        }).finally (function () {   
        	
	        });                         
		};

		
		$scope.closeModalNF = function() {
			$scope.isUpload = false;
			$scope.showModalNF = false;
			$scope.limparCadastro();
			$scope.buscaDocumentosTransportadora();
		};				
        
		$scope.limparCadastro = function() {
			if ($scope.doc!=null) {
				$scope.doc.danfe="";
				$scope.doc.numero="";
				$scope.doc.booking="";
			}
			if ($scope.emitente!=null) {
				$scope.emitente.cnpj="";
				$scope.emitente.razaoSocial="";			
				$scope.emitente.nomeContato="";	
				$scope.emitente.telefone="";	
				$scope.emitente.email="";
			}
			if ($scope.consolidador != null) {
				$scope.consolidador.razaoSocial="";	
				$scope.consolidador.cnpj="";
				$scope.consolidador.nomeContato="";			
				$scope.consolidador.telefone="";			
				$scope.consolidador.email="";				
			}
			if ($scope.item!=null) {
				$scope.item.ncmId="";
				$scope.item.embalagemId="";
				$scope.item.quantidade="";			
				$scope.item.peso="";			
			}
			$('#ncm-codigo-nf_value').val("");
			$('#ncm-nome-nf_value').val("");
			$('#embalagem-codigo-nf_value').val("");
			$('#embalagem-nome-nf_value').val("");
		};
		
        
        function isEmail(email) {
        	er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
        	if (er.exec(email)) {
        		return true;
        	} else {
        		return false;
        	}
        }
		
		function openGifModal() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DueRedexCadastroDocumentoPortlet/paginas/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }
		
		function closeGifModal() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }
		
		$scope.validarCNPJ = function(ObjCnpj){
			var valor = true;
	        var cnpj = ObjCnpj;
	        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
	        var dig1= new Number;
	        var dig2= new Number;

	        exp = /\.|\-|\//g;
	        cnpj = cnpj.toString().replace( exp, "" ); 
	        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

	        for(i = 0; i<valida.length; i++){
	                dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
	                dig2 += cnpj.charAt(i)*valida[i];       
	        }
	        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
	        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

	        if(((dig1*10)+dig2) != digito){
	        	valor = false;
	        }
	        return valor;
		};
		
		function isEmpty(str) {
			return str == null || str == undefined || str == "";
		}
		
		function isNF(tipo) {
			return 'NF' == tipo || "NF" == tipo;
		}
		
		function isITEM(tipo) {
			return 'ITEM' == tipo || "ITEM" == tipo;
		}
		
		function isDanfeValid(danfe) {
    		var multiplicadores = [ 2, 3, 4, 5, 6, 7, 8, 9 ];
    		var i = 42;
    		var soma_ponderada = 0;
    		var chave43 = danfe.split('');
    		while (i >= 0) {
    			for (var m = 0; m < multiplicadores.length && i >= 0; m++) {
    				soma_ponderada += chave43[i] * multiplicadores[m];
    				i--;
    			}
    		}
    		var resto = soma_ponderada % 11;
    		var digito = 0;
    		if (resto != 0 && resto != 1) {
    			digito = (11 - resto);
    		}
    		var digitoValido = (Number(chave43[43]) == digito);
    		if (danfe == '' || danfe.toString().length != 44) {
    			return false;
    		}
    		if (danfe == '00000000000000000000000000000000000000000000') {
    			return false;
    		}
    		if (digitoValido == false) {
    			return false;
    		}
    		
    		return true;
		}
		
		function retiraFormatoCnpj(cnpj){
			cnpj = cnpj.replace('.', '');
			cnpj = cnpj.replace('.', '');
			cnpj = cnpj.replace('/', '');
			cnpj = cnpj.replace('-', '');
			
			return cnpj;
		}
		
		function getTipoDocumentoRadio() {
			return $scope.TipoDocumentoRadio;
		}		   
        
        function hasNumbers(t)
        {
    	    var regex = /\d/g;
    	    return regex.test(t);
        }  
        
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }            
        
		$scope.$on('1', function(event, args) {
			$scope.Mensagem = "";
			$scope.MensagemInfo = "";			
			$scope.showModalNF = false;
			$scope.isUpload = false;
			$scope.isNew = false;
			$scope.showNFtable = true;
			$scope.itemsPerPage = 10;
	        $scope.qtdRegistros = 0;
	        $scope.currentPage = 1;
	        $scope.qtdporpaginalist = [{ id: 0, qtd: 10 }, { id: 1, qtd: 25 }, { id: 2, qtd: 50 }, { id: 3, qtd: 100 }];
	        $scope.qtdporpagina = { id: 0, qtd: 10 };
	        console.log("cnpjs -> " + $scope.cnpjs);
	        $scope.docList = [];
//	        $scope.embalagens = [];
	        $scope.commodities = [];
	        $scope.availableTagsCommodity = [];
	        $scope.availableTagsEmbalagem = [];
	        $scope.initModalCadastroVars();
			$scope.buscaDocumentosTransportadora("NF");
		});
	
    }
]);

