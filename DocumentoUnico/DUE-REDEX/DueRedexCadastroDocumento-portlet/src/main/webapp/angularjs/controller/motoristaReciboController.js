app.controller('MotoristaReciboController', ['$scope', '$location', '$rootScope', '$modal', 'motoristaReciboService', 'modalService',
    function($scope, $location, $rootScope, $modal, motoristaReciboService, modalService) {

        $scope.Mensagem = "";
        $scope.showDivCadastro = false;

        $scope.transportes = [];
        $scope.isEstrangeiro = false;
        $scope.hasCadastroMotorista = true;
        $scope.docDisponivelList = [];
        $scope.docVinculadoList = [];
        $scope.transporte = {};
        $scope.transporte.documentos = [];

        $scope.tableRowExpanded = false;
        $scope.tableRowIndexExpandedCurr = "";
        $scope.tableRowIndexExpandedPrev = "";
        $scope.storeIdExpanded = "";
        $scope.dayDataCollapse = [];

        $scope.motorista = {};
        $scope.searchDoc = "";

        $scope.buscaTransportadora = function() {

            openGifModal();
            var cnpj = retiraFormatoCnpj($scope.cnpjs);
            console.log(cnpj);
            motoristaReciboService.buscaTransportadora($scope.apiURL, cnpj).then(function(response) {
                    // SUCCESS 100,101,200,201
                    console.log(response);
                    var dto = response.data;
                    if (dto.erro) {
                        closeGifModal();
                        $scope.showDivCadastro = false;
                        msg = dto.mensagem;
                        modalService.closeModalErrors(msg);
                    } else {
                        closeGifModal();
                        $scope.transportes = dto.transporteList;
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    msg = response.data;
                    modalService.closeModalErrors(msg);
                })
        };

        $scope.dayDataCollapseFn = function() {

            for (var i = 0; i < $scope.ListUnits.length; i += 1) {
                $scope.dayDataCollapse.push(false);
            }
        };

        $scope.selectTableRow = function(index, storeId) {
        	      	        	
            if (typeof $scope.dayDataCollapse === 'undefined') {
                $scope.dayDataCollapseFn();
            }

            if ($scope.tableRowExpanded === false && $scope.tableRowIndexExpandedCurr === "" && $scope.storeIdExpanded === "") {
                $scope.tableRowIndexExpandedPrev = "";
                $scope.tableRowExpanded = true;
                $scope.tableRowIndexExpandedCurr = index;
                $scope.storeIdExpanded = storeId;
                $scope.dayDataCollapse[index] = true;
            } else if ($scope.tableRowExpanded === true) {
                if ($scope.tableRowIndexExpandedCurr === index && $scope.storeIdExpanded === storeId) {
                    $scope.tableRowExpanded = false;
                    $scope.tableRowIndexExpandedCurr = "";
                    $scope.storeIdExpanded = "";
                    $scope.dayDataCollapse[index] = false;
                } else {
                    $scope.tableRowIndexExpandedPrev = $scope.tableRowIndexExpandedCurr;
                    $scope.tableRowIndexExpandedCurr = index;
                    $scope.storeIdExpanded = storeId;
                    $scope.dayDataCollapse[$scope.tableRowIndexExpandedPrev] = false;
                    $scope.dayDataCollapse[$scope.tableRowIndexExpandedCurr] = true;
                }
            }
        };

        $scope.buscaMotorista = function() {

        	/*$scope.motorista.cpf = "";
        	$scope.motorista.cnh = "";
    		$scope.motorista.passaporte = "";
			$scope.motorista.nome = "";*/
        			
            openGifModal();
            motoristaReciboService.buscaMotorista($scope.apiURL, $scope.searchDoc, $scope.isEstrangeiro).then(function(response) {
                    // SUCCESS 100,101,200,201
                    console.log(response);
                    closeGifModal();
                    var dto = response.data;
                    if (dto.erro) {
                        $scope.limpaMotorista();
                        msg = dto.mensagem;
                        modalService.closeModalErrors(msg);
                    } else {
                        if (dto.mensagem) {
                            $scope.hasCadastroMotorista = true;
                            msg = dto.mensagem;
                            modalService.closeModalErrors(msg);
                        } else {
                            $scope.hasCadastroMotorista = true;
                            $scope.motorista.cpf = dto.transporte.cpf;
                            $scope.motorista.cnh = dto.transporte.cnh;
                            $scope.motorista.passaporte = dto.transporte.passaporte;
                            $scope.motorista.nome = dto.transporte.nome;
                        }
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    msg = response.data;
                    modalService.closeModalErrors(msg);
                })
        };

        $scope.limpaMotorista = function() {
            $scope.hasCadastroMotorista = true;
            $scope.motorista.cpf = "";
            $scope.motorista.cnh = "";
            $scope.motorista.passaporte = "";
            $scope.motorista.nome = "";
        };

        $scope.buscaDocumentosDisponiveis = function(novo) {

            openGifModal();

            var cnpj = retiraFormatoCnpj($scope.cnpjs);
            console.log(cnpj);
            motoristaReciboService.buscaDocumentosDisponiveis($scope.apiURL, cnpj, novo).then(function(response) {
                    // SUCCESS 100,101,200,201
                    console.log(response);
                    closeGifModal();
                    var dto = response.data;
                    if (dto.erro) {
                        $scope.showDivCadastro = false;
                        msg = dto.mensagem;
                        modalService.closeModalErrors(msg);
                    } else {
                        $scope.docDisponivelList = dto.documentosDisponiveis;
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    msg = response.data;
                    modalService.closeModalErrors(msg);
                })
        };

        $scope.saveRecibo = function() {

            $("input").removeClass("myError");

            if ($scope.docVinculadoList == undefined || $scope.docVinculadoList.length == 0) {
                msg = "Selecione ao menos um documento para o Recibo.";
                modalService.closeModalErrors(msg);
                return;
            }

            if (!$scope.isEstrangeiro) {
                if ($scope.isBlank($scope.motorista.cpf)) {
                    $("#cpf").addClass("myError");
                    msg = "CPF do motorista deve ser informado.";
                    modalService.closeModalErrors(msg);
                    return;
                }

                if (!$scope.validaCpf($scope.motorista.cpf)) {
                    $("#cpf").addClass("myError");
                    msg = "CPF do motorista inválido.";
                    modalService.closeModalErrors(msg);
                    return;
                }

                if ($scope.isBlank($scope.motorista.cnh)) {
                    $("#cnh").addClass("myError");
                    msg = "CNH do motorista deve ser informado.";
                    modalService.closeModalErrors(msg);
                    return;
                }
            } else {
                if ($scope.isBlank($scope.motorista.passaporte)) {
                    $("#passaporte").addClass("myError");
                    msg = "Passaporte do motorista deve ser informado.";
                    modalService.closeModalErrors(msg);
                    return;
                }
            }

            if ($scope.isBlank($scope.motorista.nome)) {
                $("#nome").addClass("myError");
                msg = "Nome do motorista deve ser informado.";
                modalService.closeModalErrors(msg);
                return;
            }

            if ($scope.isBlank($scope.motorista.cavalo)) {
                $("#placa").addClass("myError");
                msg = "Placa do cavalo deve ser informada.";
                modalService.closeModalErrors(msg);
                return;
            } else if(!$scope.isValidPlaca($scope.motorista.cavalo)) {
            	 $("#placa").addClass("myError");
                 msg = "Placa do cavalo inválida.";
                 modalService.closeModalErrors(msg);
                 return;
            }
            
            if ($scope.isBlank($scope.motorista.reboque1)) {
                $("#reboque1").addClass("myError");
                msg = "Placa do reboque 1 deve ser informada.";
                modalService.closeModalErrors(msg);
                return;
            } else if(!$scope.isValidPlaca($scope.motorista.reboque1)) {
            	 $("#reboque1").addClass("myError");
                 msg = "Placa do reboque 1 inválida.";
                 modalService.closeModalErrors(msg);
                 return;
            }
            
            if(!$scope.isBlank($scope.motorista.reboque2) && !$scope.isValidPlaca($scope.motorista.reboque2)) {
            	$("#reboque2").addClass("myError");
                msg = "Placa do reboque 2 inválida.";
                modalService.closeModalErrors(msg);
                return;
            }

            $scope.transporte = $scope.motorista;
            $scope.transporte.documentos = $scope.docVinculadoList;
            /*$scope.transporte.novoCadastroMotorista = !$scope.hasCadastroMotorista;*/
            $scope.transporte.cnpjTransportadora = retiraFormatoCnpj($scope.cnpjs);

            openGifModal();
            motoristaReciboService.salvaRecibo($scope.apiURL, $scope.transporte).then(function(response) {
                    // SUCCESS 100,101,200,201
                    console.log(response);
                    var dto = response.data;
                    if (dto.erro) {
                        closeGifModal();
                        msg = dto.mensagem;
                        modalService.closeModalErrors(msg);
                    } else {
                        closeGifModal();
                        var dto = response.data;
                        $scope.showDivCadastro = false;
                        msg = "Recibo salvo com sucesso.";
                        modalService.closeModalErrors(msg);
                        $scope.buscaTransportadora();
                    }
                },
                function errorCallBack(response) {
                    // ERROR 400,401,500
                    console.log(response);
                    closeGifModal();
                    var dto = response.data;
                    msg = dto.mensagem;
                    modalService.closeModalErrors(msg);
                })
        };

        $scope.addDoc = function() {
            console.log("add");

            if ($('#disponiveis input:checked').length == 0) {
                msg = "Selecione ao menos um documento para vincular ao recibo.";
                modalService.closeModalErrors(msg);
            }

            $('#disponiveis input:checked').each(function(index, value) {

                $($scope.docDisponivelList).each(function(i, v) {
                    if (v.id == value.value) {
                    	if ($scope.docDisponivelList==null)
                    		$scope.docDisponivelList = [];
                    	$scope.docVinculadoList.push(v);
                        $scope.docDisponivelList.splice($scope.docDisponivelList.indexOf(v), 1)
                    }
                });

                $(this).prop('checked', false)
                $(this).attr('checked', false);
                $(this).removeAttr('checked');
            });
        };

        $scope.removeDoc = function() {
            console.log("remove");

            if ($('#vinculados input:checked').length == 0) {
                msg = "Selecione ao menos um documento para desvincular do recibo.";
                modalService.closeModalErrors(msg);
            }

            $('#vinculados input:checked').each(function(index, value) {

                $($scope.docVinculadoList).each(function(i, v) {
                    if (v.id == value.value) {
                    	if ($scope.docDisponivelList==null)
                    		$scope.docDisponivelList = [];
                    	$scope.docDisponivelList.push(v);
                        $scope.docVinculadoList.splice($scope.docVinculadoList.indexOf(v), 1)
                    }
                });

                $(this).prop('checked', false)
                $(this).attr('checked', false);
                $(this).removeAttr('checked');
            });
        };

        $scope.checkTodosAdd = function() {

            if ($('#checkTodosAdd').is(':checked')) {
                $('#disponiveis input').each(function(index, value) {
                    $(this).prop('checked', true)
                    $(this).attr('checked', true);
                });
            } else {
                $('#disponiveis input').each(function(index, value) {
                    $(this).prop('checked', false)
                    $(this).attr('checked', false);
                });
            }
        };

        $scope.checkTodosRemove = function() {

            if ($('#checkTodosRemove').is(':checked')) {
                $('#vinculados input').each(function(index, value) {
                    $(this).prop('checked', true)
                    $(this).attr('checked', true);
                });
            } else {
                $('#vinculados input').each(function(index, value) {
                    $(this).prop('checked', false)
                    $(this).attr('checked', false);
                });
            }
        };

        $scope.novoRecibo = function() {
        	$scope.hasCadastroMotorista = true;
        	$scope.searchDoc = "";
            $scope.motorista = {};
            $scope.transporte = {};
            $scope.docVinculadoList = [];
            $scope.buscaDocumentosDisponiveis("1");
            $scope.showDivCadastro = true;
        };
        
        $scope.editarRecibo = function(transporte) {
        	openGifModal();
        	motoristaReciboService.validaModificacaoDocumentos($scope.apiURL, transporte.documentos).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if (dto.erro) {
                    closeGifModal();
                    msg = dto.mensagem;
                    modalService.closeModalErrors(msg);
                } else {
                	closeGifModal();
                	if(transporte.passaporte != null){
                	    $scope.estrangeiro = true;
                	    $('#checkEstrangeiro')["0"].checked = true;
                	    $scope.isEstrangeiro = true;                 	    
                	}
                	$scope.searchDoc = "";
                	$scope.motorista = transporte;
                	$scope.docVinculadoList = transporte.documentos;
                	$scope.buscaDocumentosDisponiveis("0");
                    $scope.showDivCadastro = true;
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                var dto = response.data;
                msg = dto.mensagem;
                modalService.closeModalErrors(msg);
            })
        };
        
        $scope.excluirRecibo = function(transporte) {
        	$scope.transporte = transporte;
        	var msg = "Confirma a exclusão do recibo ?";
        	modalService.dialogModal(msg, $scope.excluirReciboConfirm)
        }
        
        $scope.excluirReciboConfirm = function() {
        	openGifModal();
        	motoristaReciboService.validaModificacaoDocumentos($scope.apiURL, $scope.transporte.documentos).then(function(response) {
                // SUCCESS 100,101,200,201
                console.log(response);
                var dto = response.data;
                if (dto.erro) {
                    closeGifModal();
                    msg = dto.mensagem;
                    modalService.closeModalErrors(msg);
                } else {
                	motoristaReciboService.excluirRecibo($scope.apiURL, $scope.transporte).then(function(response) {
                        // SUCCESS 100,101,200,201
                        console.log(response);
                        var dto = response.data;
                        if (dto.erro) {
                            closeGifModal();
                            msg = dto.mensagem;
                            modalService.closeModalErrors(msg);
                        } else {
                        	closeGifModal();
                            var dto = response.data;
                            $scope.showDivCadastro = false;
                            msg = "Recibo excluido com sucesso";
                            modalService.closeModalErrors(msg);
                            $scope.buscaTransportadora();
                        }
                    },
                    function errorCallBack(response) {
                        // ERROR 400,401,500
                        console.log(response);
                        closeGifModal();
                        var dto = response.data;
                        msg = dto.mensagem;
                        modalService.closeModalErrors(msg);
                    })
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                var dto = response.data;
                msg = dto.mensagem;
                modalService.closeModalErrors(msg);
            })
        };

        $scope.cancelRecibo = function() {
            $scope.showDivCadastro = false;
        };

        $scope.checkEstrangeiro = function() {
            console.log("CHECK ESTRANGEIRO");
            //$scope.limpaMotorista();
            if ($('#checkEstrangeiro')["0"].checked) {
                $scope.isEstrangeiro = true;
            } else {
                $scope.isEstrangeiro = false;
            }
        };
        
        $scope.imprimirRecibo = function(transporte) {
        	openGifModal();
        	motoristaReciboService.validaImpressaoRecibo($scope.apiURL, transporte).then(function(response) {
                // SUCCESS
                // 100,101,200,201
                console.log(response);
                
                if(response.data.erro) {
                	closeGifModal();
                	msg = response.data.mensagem;
                	modalService.closeModalErrors(msg);
                } else{                	

                	motoristaReciboService.imprimeRecibo(transporte.id).then(function(response) {
                		console.log(response);      
                		
                        var objectUrl = window.URL.createObjectURL(base64toBlob(response.data, "application/pdf"));
                        var downloadLink = document.createElement("a");
                        downloadLink.href = objectUrl;
                        downloadLink.download = "recibo.pdf";
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);   
                        closeGifModal();
                	 },
                     function errorCallBack(response) {
                         // ERROR 400,401,500
                         console.log(response);
                         closeGifModal();
                         msg = response.data;
                         modalService.closeModalErrors(msg);
                     })
                }
            },
            function errorCallBack(response) {
                // ERROR 400,401,500
                console.log(response);
                closeGifModal();
                msg = response.data;
                modalService.closeModalErrors(msg);
            })
        };     
        
        function base64toBlob(base64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }

        $scope.$on('2', function(event, args) {
        	$scope.Mensagem = "";
            $scope.showDivCadastro = false;

            $scope.transportes = [];
            $scope.isEstrangeiro = false;
            $scope.hasCadastroMotorista = true;
            $scope.docDisponivelList = [];
            $scope.docVinculadoList = [];
            $scope.transporte = {};
            $scope.transporte.documentos = [];

            $scope.tableRowExpanded = false;
            $scope.tableRowIndexExpandedCurr = "";
            $scope.tableRowIndexExpandedPrev = "";
            $scope.storeIdExpanded = "";
            $scope.dayDataCollapse = [];

            $scope.motorista = {};
            $scope.searchDoc = "";
            $scope.buscaTransportadora();
        });

        function openGifModal() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DueRedexCadastroDocumentoPortlet/paginas/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }

        function closeGifModal() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }

        function retiraFormatoCnpj(cnpj) {
            cnpj = cnpj.replace('.', '');
            cnpj = cnpj.replace('.', '');
            cnpj = cnpj.replace('/', '');
            cnpj = cnpj.replace('-', '');

            return cnpj;
        }

        $scope.isBlank = function(string) {
            if (string == undefined) return true;
            if (string == null) return true;
            if (string == "null") return true;
            if (string == "") return true;
            return false;
        };

        $scope.validaCpf = function(cpfParam) {
            value = jQuery.trim(cpfParam);

            value = value.replace('.', '');
            value = value.replace('.', '');
            cpf = value.replace('-', '');
            while (cpf.length < 11) cpf = "0" + cpf;
            var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
            var a = [];
            var b = new Number;
            var c = 11;
            for (i = 0; i < 11; i++) {
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
            b = 0;
            c = 11;
            for (y = 0; y < 10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;
            return true;
        };
        
        $scope.isValidPlaca = function(placa) {
        	var er = /[a-z]{3}-?\d{4}/gim;  
        	er.lastIndex = 0;  
        	return er.test( placa );
        };
    }
]);