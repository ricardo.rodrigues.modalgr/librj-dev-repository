  <div ng-app="App" ng-controller="MotoristaReciboController">

      <div class="row-fluid">

          <div class="span12">

              <table class="table table-bordered">

                  <thead>
                      <tr style="height: 10px;">

                          <th>MOTORISTA</th>
                          <th>PLACA</th>
                          <th>CNH</th>
                          <th>RECIBO</th>
                          <th>IMPRIMIR</th>
                          <th>EDITAR</th>
                          <th>EXCLUIR</th>
                      </tr>
                  </thead>

                  <tbody ng-repeat="transporte in transportes" ng-switch="dayDataCollapse[$index]">
                      <tr style="height: 10px;">

                          <td data-ng-click="selectTableRow($index, transporte.id)">{{transporte.nome}}</td>
                          <td data-ng-click="selectTableRow($index, transporte.id)">{{transporte.cavalo}}</td>
                          <td data-ng-click="selectTableRow($index, transporte.id)">{{transporte.cnh}}</td>
                          <td data-ng-click="selectTableRow($index, transporte.id)">{{transporte.id}}</td>

                          <td>
                              <a title="Imprimir Recibo" style='cursor: pointer;' ng-click="imprimirRecibo(transporte)"><i class="fa fa-print" aria-hidden="true"></i></a>
                          </td>
                          <td>
                              <a title="Editar Recibo" style='cursor: pointer;' ng-click="editarRecibo(transporte)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          </td>
                          <td>
                              <a title="Excluir Recibo" style='cursor: pointer;' ng-click="excluirRecibo(transporte)"><i class="fa fa-times" aria-hidden="true"></i></a>
                          </td>

                      </tr>

                      <tr ng-switch-when="true">
                          <td colspan="7">
                              <div class="span12">
                                  <table class="table table-bordered">
                                      <thead>
                                          <tr style="height: 10px;">
                                              <th>NUMERO</th>
                                              <th>EMITENTE</th>
                                          </tr>
                                      </thead>
                                      <tbody ng-repeat="documento in transportes[$index].documentos" style="height: 10px;">
                                          <td>{{documento.numero}}</td>
                                          <td>{{documento.emitente.cnpj + ' - ' + documento.emitente.razaoSocial}}</td>
                                      </tbody>
                                  </table>
                              </div>
                          </td>
                      </tr>
                  </tbody>

              </table>

          </div>

      </div>

      <div class="row-fluid" class="margin-bottom:16px;">
          <div class="span12" style="text-align: center;">
              <button style="height: 29px; width: 150px; text-align: center; border-radius: 15px;" title="Incluir novo recibo" class="btn-primary" ng-click="novoRecibo()" ng-disabled="showDivCadastro">
	<i class="icon-plus"></i> Novo Recibo
</button>
           </div>
       </div>

       <div id="divCadastro" ng-show="showDivCadastro" style="background-color: rgb(218,218,218);">
          <div style="margin: 10px;">
          
              <div class="row-fluid">
                  <input type="checkbox" id="checkEstrangeiro" ng-model="estrangeiro" class="ng-valid ng-dirty" style="float: left;" ng-change="checkEstrangeiro()">
                  <label class="span1" style="margin: 0px; margin-left: 6px; margin-top: 2px; margin-bottom: 0px;">Estrangeiro</label>
              </div>
          	<div class="row-fluid">
                  <label class="span2" style=" margin-bottom: 0px;">Documento:</label>
                  <div id="infoBuscaMotorista" style="float: left; margin-right: 5px; margin-left: -80px;">
				<i class="fa fa-question-circle" title="Informe o CPF, CNH ou Passaporte e clique em Buscar Motorista para validar se motorista j� encontra-se cadastrado em nossa base de dados."></i>
			</div>
                  <input type="text" style="border-radius: 12px; margin-bottom: 0px;" class="input-block-level span4 ng-pristine ng-valid" ng-model="searchDoc" tabindex="1" id="searchDoc">
              </div>
          
          	<div class="span12" style="text-align: left; margin-bottom: 0px; margin-left: 0px;">
                  <button ng-click="buscaMotorista()" style="height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 120px;" class="btn btn-primary" tabindex="2">Buscar Motorista</button>
              </div>


              <div class="row-fluid" ng-show="!estrangeiro">
                  <label class="span2" style="float: left;">CPF:</label>
                  <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3 ng-pristine ng-valid" ng-model="motorista.cpf" tabindex="2" id="cpf" ng-disabled="hasCadastroMotorista">

                  <label class="span2" style="float: left;">CNH:</label>
                  <input type="text" style="border-radius: 12px; float: left; width: 427px;" class="input-block-level span3 ng-pristine ng-valid" ng-model="motorista.cnh" tabindex="3" id="cnh" ng-disabled="hasCadastroMotorista">
              </div>

              <div class="row-fluid" ng-show="isEstrangeiro">
                  <label class="span2">Passaporte:</label>
                  <input type="text" style="border-radius: 12px;" class="input-block-level span5 ng-pristine ng-valid" ng-model="motorista.passaporte" tabindex="2" id="passaporte" ng-disabled="hasCadastroMotorista">
              </div>

              <div class="row-fluid">
                  <label class="span2">Nome do Motorista:</label>
                  <input type="text" style="border-radius: 12px; width: 880px;" class="input-block-level span8 ng-pristine ng-valid" ng-model="motorista.nome" tabindex="4" id="nome" ng-disabled="hasCadastroMotorista">
              </div>

              <div class="row-fluid" style="margin-top: 3px;">
                  <label class="span2" style="float: left;">Placa do Cavalo:</label>
                  <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span2" tabindex="5" ng-model="motorista.cavalo" id="placa">

                  <label class="span2" style="float: left;">Placa do Reboque 1:</label>
                  <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span2" tabindex="6" ng-model="motorista.reboque1" id="reboque1">

                  <label class="span2" style="float: left;">Placa do Reboque 2:</label>
                  <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span2" tabindex="7" ng-model="motorista.reboque2">
              </div>

              <div class="row-fluid" style="margin-left: 45px; margin-top: 20px;">

                  <div class="span12">

                      <div class="span5" id="disponiveis" style="height: 300px; overflow-y: scroll;">
                          <table class="table table-bordered">

                              <thead>
                                  <tr>
                                      <th><input type="checkbox" style="margin-right: 10%;" ng-click="checkTodosAdd()" title="Selecionar todos" id="checkTodosAdd" ng-disabled="docDisponivelList.length == 0"></th>
                                      <th>NF/DUE</th>
                                      <th>Emitente</th>
                                  </tr>
                              </thead>

                              <tbody>
                                  <tr ng-repeat="doc in docDisponivelList">
                                      <td><input type="checkbox" style="margin-right: 10%;" value="{{doc.id}}"></td>
                                      <td>{{ doc.numero }}</td>
                                      <td>{{ doc.emitente.razaoSocial }}</td>
                                  </tr>
                              </tbody>

                          </table>
                      </div>

                      <div class="span1" style="margin-top: 20px; margin-left: 35px;">
                          <div class="row-fluid">
                              <a href="#" ng-click="addDoc()" style="margin-bottom: 12px; margin-top: 12px;" title="Vincular documento ao Recibo">
                                  <i class="fa fa-arrow-circle-right" style="font-size: xx-large;" aria-hidden="true"></i>
                              </a>
                          </div>
                          <div class="row-fluid">
                              <a href="#" ng-click="removeDoc()" title="Desvincular documento ao Recibo">
                                  <i class="fa fa-arrow-circle-left" style="font-size: xx-large;" aria-hidden="true"></i>
                              </a>
                          </div>
                      </div>

                      <div class="span5" id="vinculados" style="margin-left: 0px; height: 300px; overflow-y: scroll;">
                          <table class="table table-bordered">

                              <thead>
                                  <tr>
                                      <th><input type="checkbox" style="margin-right: 10%;" ng-click="checkTodosRemove()" title="Selecionar todos" id="checkTodosRemove" ng-disabled="docVinculadoList.length == 0"></th>
                                      <th>NF/DUE</th>
                                      <th>Emitente</th>
                                  </tr>
                              </thead>

                              <tbody>
                                  <tr ng-repeat="documento in docVinculadoList">
                                      <td><input type="checkbox" style="margin-right: 10%;" value="{{documento.id}}"></td>
                                      <td>{{ documento.numero }}</td>
                                      <td>{{ documento.emitente.razaoSocial }}</td>
                                  </tr>
                              </tbody>

                          </table>
                      </div>

                  </div>

              </div>

              <div class="row">
                  <div class="span1"></div>
                  <div class="span12" style="text-align: center;margin-bottom: 10px;">

                      <button ng-click="saveRecibo()" style="margin-left: -55px; height: 30px; margin-right: 10%; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px;" class="btn btn-primary">Salvar</button>

                      <button ng-click="cancelRecibo()" style="height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px;" class="btn btn-primary">Cancelar</button>

                  </div>
              </div>
          </div>

      </div>


      <!-- MODALS -->

      <div class="modal-backdrop" ng-show="showDialogError">
          <div class="alert alert-block alert-error fade in" id="modalDialogError" style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center;">
              <h4 class="alert-heading">Aten��o</h4>
              <p>{{Mensagem}}</p>
              <p>
                  <button class="btn-primary btn-danger span2" ng-click="closeDialogError()" title="Fechar" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-top: 5%; margin-left: 47%;">
	<i class="fa fa-check" aria-hidden="true"></i>
</button>
               </p>
           </div>
       </div>

       <div class="modal-backdrop" ng-show="showDialogSuccess">
           <div class="alert alert-block alert-success fade in" id="modalDialogSuccess" style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center;">
              <h4 class="alert-heading">Info</h4>
              <p>{{Mensagem}}</p>
              <p>
                  <button class="btn-primary btn-success span2" ng-click="closeDialogSuccess()" title="Fechar" style="width: 30px; height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; margin-top: 5%; margin-left: 47%;">
	<i class="fa fa-check" aria-hidden="true"></i>
</button>
                </p>
            </div>
        </div>

    </div>