<div>

	<i class="fa fa-times" style="float: right; font-size: large; cursor: pointer;" ng-click="closeModalNF()"></i>

	<div class="row-fluid" style="background-color: rgb(218,218,218); width: 85%; border-radius: 11px;" ng-show="isNew">
	
		<div class="row-fluid">
			<h5 style="float: left; margin-left: 5px;">Upload</h5>
	
			<div id="infoUpload" style="float: left; margin-right: 5px; margin-left: 5px;">
				<i class="fa fa-question-circle" title="Use esta op��o para preencher os dados automaticamente a partir de arquivo XML da NF."></i>
			</div>
		</div>
		
		<div class="row-fluid">
			<input type="file" style="width: 435px; border-radius: 12px; float: left; margin-left: 5px;"
				class="input-block-level span5" on-read-file="upload($fileContent)">
		</div>

	</div>


	<div class="row-fluid" ng-show="isNew">
		<h5>Inclus�o de documento</h5>
	</div>
	
	<div class="row-fluid" ng-show="!isNew">
		<h5>Altera��o de documento</h5>
	</div>	

	<div class="row-fluid">
		<label class="span3">Tipo:</label> 
		<select style="border-radius: 15px;" ng-change="selTipoDoc()" ng-model="TipoDocumentoSelect" id="tipo" name="tipo" ng-disabled="isUpload" ng-show="isNew">
			<option value="NF">NF</option>
			<option value="ITEM">Item DUE(Bagagem,Evento,Doa��o)</option>
		</select>
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" id="tipoEdit" ng-disabled="true" ng-show="!isNew">
	</div>

	<div class="row-fluid" ng-show="exibeNroNF">
		<label class="span3">Chave DANFE*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" id="danfe" ng-model="doc.danfe" ng-show="exibeNroNF" ng-blur="validaDanfe()" ng-disabled="isUpload">
	</div>

	<div class="row-fluid" ng-show="exibeNroNF">
		<label class="span3">N�mero NF*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="doc.numero" id="docNumeroNF" ng-show="exibeNroNF" ng-disabled="isUpload || !isNew">
	</div>
	
	<div class="row-fluid" ng-show="!exibeNroNF">
		<label class="span3">N�mero DUE*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="doc.numero" id="docNumeroDUE" ng-show="!exibeNroNF" ng-blur="validaDue()" ng-disabled="isUpload || !isNew" capitalize>
	</div>

	<div class="row-fluid">
		<label class="span3">CNPJ Emitente*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" id="cnpjEmitente" ng-model="emitente.cnpj" ng-disabled="!isNew" ng-blur="validaEmitente()" somentenumeros>
	</div>
	
	<div class="row-fluid span10" style="background-color: rgb(218,218,218); margin-bottom: 10px; border-radius: 15px;" ng-show="exibeFormCadastroEmitente">
		<div class="row-fluid" style="margin-top: 5px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Raz�o Social*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="emitenteRazaoSocial" ng-model="emitente.razaoSocial">
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Nome Contato*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="emitenteNomeContato" ng-model="emitente.nomeContato">
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Telefone*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="emitenteTelefone" ng-model="emitente.telefone" maxlength="11" somentenumeros>
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">E-mail*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="emitenteEmail" ng-model="emitente.email">
		</div>
	</div>


	<div class="row-fluid">
		<label class="span3">Consolidador:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" id="cnpjConsolidador" ng-model="consolidador.cnpj" ng-blur="validaConsolidador()" ng-disabled="!isNew" somentenumeros>
	</div>
	
	<div class="row-fluid span10" style="background-color: rgb(218,218,218); margin-bottom: 10px; border-radius: 15px;" ng-show="exibeFormCadastroConsolidador">
		<div class="row-fluid" style="margin-top: 5px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Raz�o Social*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="consolidadorRazaoSocial" ng-model="consolidador.razaoSocial">
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Nome Contato*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="consolidadorNomeContato" ng-model="consolidador.nomeContato">
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">Telefone*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="consolidadorTelefone" ng-model="consolidador.telefone" maxlength="11" somentenumeros>
		</div>
		<div class="row-fluid" style="margin: 0px">
			<label class="span3" style="color: black !important; margin-left: 5px;">E-mail*:</label> 
			<input type="text" style="border-radius: 12px;" class="input-block-level span8" id="consolidadorEmail" ng-model="consolidador.email">
		</div>
	</div>
	
	<div class="row-fluid">
		<label class="span3">Booking:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="doc.booking" id="docBooking" ng-disabled="bookingDisabeld" capitalize>
	</div>

	<div class="row-fluid">
		<h5>Item da Nota Fiscal</h5>
	</div>

	<div class="row-fluid">
		<label class="span3">Item:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="item.numero" ng-disabled="true">
	</div>
	

 	<div class="row-fluid" ng-init="buscaCommodities()">
		<label class="span3" style="float: left;">NCM/SH*:</label>
		
		<div data-ng-show="exibeCommodities" style="margim: 0px;">
	 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="ncm-codigo-nf" placeholder="C�digo"  ng-disabled="!exibeCommodities"
	              pause="100" selectedobject="selectedNcm" localdata="ncms" searchfields="codigo"
	              titlefield="codigo" minlength="1" class="input-block-level span4" preenche-Valor-Codigo="preencheNcmCodigo" preenche-Valor-Descricao="preencheNcmDescricao"/>
		</div>
			              
		<div data-ng-show="!exibeCommodities" style="margin-left: 265px;">
			<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/modal-gif-small.html'"></div>
		</div>
	</div>               	
	<div class="row-fluid" ng-disabled="!exibeCommodities">
		<label class="span3" style="float: left;"></label>			
		
		<div data-ng-show="exibeCommodities" style="margim: 0px;">
	 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="ncm-nome-nf" placeholder="Digite a descri��o do NCM"  ng-disabled="!exibeCommodities"
	              pause="100" selectedobject="selectedNcm" localdata="ncms" searchfields="descricao"
	              titlefield="descricao" minlength="1" class="input-block-level span5" preenche-Valor-Codigo="preencheNcmCodigo" preenche-Valor-Descricao="preencheNcmDescricao"/>	
        </div>
        
		<div data-ng-show="!exibeCommodities" style="margin-left: 265px;">
			<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/modal-gif-small.html'"></div>
		</div>              
	</div> 

	<div class="row-fluid">
		<label class="span3">Quantidade*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="item.quantidade" id="itemQuantidade" ng-disabled="isUpload" somentenumeros>
	</div>

	
	<div class="row-fluid" ng-init="buscaEmbalagens()">
		<label class="span3">Embalagem*:</label> 				

 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="embalagem-codigo-nf" placeholder="C�digo"
              pause="100" selectedobject="selectedEmb" localdata="embalagens" searchfields="codigo"
              titlefield="codigo" minlength="1" class="input-block-level span4" preenche-Valor-Codigo="preencheEmbalagemCodigo" preenche-Valor-Descricao="preencheEmbalagemDescricao"/> 
	</div>
	<div class="row-fluid">   
		<label class="span3"></label>
		             		
 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="embalagem-nome-nf" placeholder="Digite a descri��o da embalagem"
              pause="100" selectedobject="selectedEmb" localdata="embalagens" searchfields="descricao"
              titlefield="descricao" minlength="1" class="input-block-level span5" preenche-Valor-Codigo="preencheEmbalagemCodigo" preenche-Valor-Descricao="preencheEmbalagemDescricao"/>

	</div>

	<div class="row-fluid">
		<label class="span3">Peso Total(kg)*:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="item.peso" id="itemPeso" ng-disabled="isUpload" somentenumeros>
	</div>

	<div class="row-fluid">
		<label class="span3">Marcas e N�meros:</label> 
		<input type="text" style="border-radius: 12px;" class="input-block-level span5" ng-model="item.marcasNumeros" ng-disabled="isUpload">
	</div>

	<div class="row-fluid">
		<label class="span3">Inf. Adicionais:</label> 
		<textarea rows="5" cols="50" style="border-radius: 12px; background-color: white;" class="input-block-level span5" ng-model="item.infoAdicional" ng-show="!isUpload"> </textarea>
		<textarea rows="5" cols="50" style="border-radius: 12px;" class="input-block-level span5" ng-model="item.infoAdicional" ng-disabled="true" ng-show="isUpload"> </textarea>
	</div>

	
	<div class="row">
		<div class="span1"></div>
		<div class="span12" style="margin-left: -10%; margin-top: 3%;">

			<button ng-click="saveNF()"
				style="margin-left: 29%; height: 30px; margin-right: 10%; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px;"
				class="btn btn-primary">Salvar</button>
	
			<button ng-click="closeModalNF()"
				style="height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px;"
				class="btn btn-primary">Cancelar</button>
				
		</div>
	</div>

</div>
