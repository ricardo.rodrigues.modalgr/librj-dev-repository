<script>
    $(document).ready(function() {

        $('#datepicker input').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });
</script>

<div ng-app="App" ng-controller="NFController">

	<div class="row-fluid" class="margin-bottom:16px;">
		<div class="span1">
			<button
				style="height: 29px; width: 100px; text-align: center; border-radius: 15px;"
				title="Incluir novo documento" class="btn-primary" ng-click="openModalNF(true)">
				<i class="icon-plus"></i>   Incluir</button>
		</div>
	</div>
	
	<div class="span4" style="float: right; margin-top: 15px;">
	        <select class="span2" style="float: right;" ng-options="item as item.qtd for item in qtdporpaginalist track by item.id" ng-model="qtdporpagina" ng-change="changeqtdporpagina()"></select>
	        <label style="float: right;">Itens por p�gina</label>
	</div>

	<div class="row-fluid">

		<h5>Consulta Documento</h5>

	</div>

	<div class="row-fluid">

		<div class="span5">

			<div class="radio" style="float: left;margin: 0px;" id="radioPesquisa">
				<label><input type="radio" name="optradio" ng-model="optradio" value="1" 
				ng-click="buscaDocumentosTransportadora('NF')" value ="true">  NF    </label>
				<label><input style="margin-left: 20px;" type="radio" name="optradio" ng-model="optradio" value="2"
				ng-click="buscaDocumentosTransportadora('ITEM')"> Item DUE (Bagagem, Evento, Doa��o)</label>
			</div>
			
 			<div class="row-fluid span12" ng-show="!showFilters">
       	 		<a href="#" title="Exibir filtros" class="pull-left" data-ng-click="showFilter()"><i class="fa fa-search-plus"></i> Exibir filtros</a>
    		</div>
		    <div class="row-fluid span12" ng-show="showFilters">
		        <a href="#" title="Esconder filtros" class="pull-left" data-ng-click="showFilter()"><i class="fa fa-search-minus"></i> Esconder filtros</a>
		    </div> 

		
		</div>

	</div>
	
	<div class="row-fluid span12" ng-show="showFilters" ng-cloak style="background-color: rgb(218,218,218); border-radius: 12px; margin-left: 1px; margin-bottom: 10px;">

   		<div class="row-fluid" style="margin-left: 20px;">     
   			<br/>     
        </div>

        <div class="row-fluid" style="margin-left: 10px;" ng-show="showNFtable">
            <label class="span2" style="float: left; width: 110px;">N�mero NF:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.numero" id="filterNumero" capitalize>
				
			<label class="span2" style="float: left; width: 110px;">DANFE:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span4" ng-model="filter.danfe" id="filterDANFE" maxlength="44" capitalize somentenumeros>
          
        </div>
        
        <div class="row-fluid" style="margin-left: 10px;" ng-show="!showNFtable">
            <label class="span2" style="float: left; width: 110px;">DUE:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.due" id="filterDUE" capitalize>
        </div>
        
         <div class="row-fluid" style="margin-left: 10px;">           
             <label class="span2" style="float: left; width: 110px;">Emitente:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.emitente" id="filterEmitente"  maxlength="14" capitalize somentenumeros>
        </div>
        
         <div class="row-fluid" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">Consolidador:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.consolidador" id="filterConsolidador" maxlength="14" capitalize somentenumeros>

            <label class="span2" style="float: left; width: 110px;">Booking:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.booking" id="filterBooking" capitalize>
        </div>    

		<div class="row-fluid" style="margin-left: 10px;">
			<label class="span2" style="float: left; width: 110px;">NCM/SH*:</label>
		
			<div data-ng-show="exibeCommodities" style="margim: 0px;">
		 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="ncm-codigo-nf-filter" placeholder="C�digo"  ng-disabled="!exibeCommodities"
		              pause="100" selectedobject="selectedNcm" localdata="ncms" searchfields="codigo"
		              titlefield="codigo" minlength="1" class="input-block-level span4" preenche-Valor-Codigo="preencheNcmCodigoFilter" preenche-Valor-Descricao="preencheNcmDescricaoFilter"/>
			</div>
			              
			<div data-ng-show="!exibeCommodities" style="margin-left: 200px;">
				<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/modal-gif-small.html'"></div>
			</div>
		</div>               	
		<div class="row-fluid" ng-disabled="!exibeCommodities">	
			<div data-ng-show="exibeCommodities" style="margim: 0px;">
		 		<angucomplete style="border-radius: 12px;margin-left: 120px;" id="ncm-nome-nf-filter" placeholder="Digite a descri��o do NCM"  ng-disabled="!exibeCommodities"
		              pause="100" selectedobject="selectedNcm" localdata="ncms" searchfields="descricao"
		              titlefield="descricao" minlength="1" class="input-block-level span5" preenche-Valor-Codigo="preencheNcmCodigoFilter" preenche-Valor-Descricao="preencheNcmDescricaoFilter"/>	
	        </div>
	        
			<div data-ng-show="!exibeCommodities" style="margin-left: 200px;">
				<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/modal-gif-small.html'"></div>
			</div>              
		</div> 
		
		<div class="row-fluid" style="margin-left: 10px;">
			<label class="span2" style="float: left; width: 110px;">Embalagem*:</label>	
	
	 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="embalagem-codigo-nf-filter" placeholder="C�digo"
	              pause="100" selectedobject="selectedEmb" localdata="embalagens" searchfields="codigo"
	              titlefield="codigo" minlength="1" class="input-block-level span4" preenche-Valor-Codigo="preencheEmbalagemCodigoFilter" preenche-Valor-Descricao="preencheEmbalagemDescricaoFilter"/> 
		</div>
		<div class="row-fluid" style="margin-left: 10px;">
			<label class="span2" style="float: left; width: 110px;"></label>		             		
	 		<angucomplete style="border-radius: 12px;margin-left: 0px;" id="embalagem-nome-nf-filter" placeholder="Digite a descri��o da embalagem"
	              pause="100" selectedobject="selectedEmb" localdata="embalagens" searchfields="descricao" style="margin-left:120px;"
	              titlefield="descricao" minlength="1" class="input-block-level span5" preenche-Valor-Codigo="preencheEmbalagemCodigoFilter" preenche-Valor-Descricao="preencheEmbalagemDescricaoFilter"/>
	
		</div>	
        
		 <div class="row-fluid" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">Data:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataCadastroDe" id="filterDataCadastroDe">
			</div>

            <label class="span1'" style="float: left;margin-left: 5px; width: 30px;">at�:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataCadastroAte" id="filterDataCadastroAte">
			</div>
        </div>  


		<div class="row-fluid" style="margin-left: 10px;">
            <button ng-click="filtrarCampos()" style="height: 30px; margin-right: 4%; text-align: center; padding: 1px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px; margin-bottom: 10px; margin-top: 10px;" class="btn btn-primary" title="Filtrar">Filtrar</button>
       		<button ng-click="limparCampos()" style="height: 30px; margin-right: 4%; text-align: center; padding: 1px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px; margin-bottom: 10px; margin-top: 10px;" class="btn btn-primary" title="Limpar">Limpar</button>
        </div>

    </div>
		

	<div class="row-fluid">

		<div class="span12">

			<table class="table table-bordered" ng-show="showNFtable" style="overflow:scroll;display:block;">

				<thead>
					<tr>

						<th>N�mero NF</th>
						<th>Emitente</th>
						<th>DANFE</th>
						<th>Consolidador</th>
						<th>Booking</th>
						<th>NCM</th>
						<th>Quantidade</th>
						<th>Embalagem</th>
						<th>Peso</th>
						<th>Editar</th>
						<th>Excluir</th>

					</tr>
				</thead>

				<tbody>
                    <tr ng-repeat="doc in docList">
                        <td style="text-align:center;font-size: 11px;">{{ doc.numero }}</td>
                        <td style="text-align:center;font-size: 11px;"><a tooltip="{{ doc.emitente.razaoSocial }}" style="text-decoration: none; cursor: pointer;">{{ doc.emitente.cnpj }}</a></td>
                        <td style="text-align:center;font-size: 11px;">{{ doc.danfe }}</td>
                        <td style="text-align:center;font-size: 11px;"><a tooltip="{{ doc.consolidador.razaoSocial }}" style="text-decoration: none; cursor: pointer;">{{ doc.consolidador.cnpj }}</a></td>
                        <td style="text-align:center;font-size: 11px;">{{ doc.booking }}</td>
                        <td style="text-align:center;font-size: 11px;"><a tooltip="{{doc.itens[0].ncm}}" style="text-decoration: none; cursor: pointer;">{{ doc.itens[0].ncmId}}</a></td>
                        <td style="text-align:center;font-size: 11px;">{{ doc.itens[0].quantidade}}</td>
                        <td style="text-align:center;font-size: 11px;">{{ doc.itens[0].embalagem }}</td>
                        <td style="text-align:center;font-size: 11px; ">{{ doc.itens[0].peso}}</td>
                        <td style="text-align:center;font-size: 11px;">
                        	<a title="Editar Documento" style='cursor: pointer;' ng-click="editDocumento(doc)"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                        </td>
                        <td style="text-align:center;font-size: 11px;">
                        	<a title="Excluir Documento" style='cursor: pointer;' ng-click="excluirDocumento(doc)"><i class='fa fa fa-times' aria-hidden='true'></i></a>
                        </td>
                    </tr>

                </tbody>
                  <tfoot>
                    <td colspan="14">
                        <div class="pagination pull-left">
                        	<paging
							  	page="currentPage" 
							  	page-size="itemsPerPage" 
							  	total="qtdRegistros"
							  	show-prev-next="true"
							  	show-first-last="true"
							  	paging-action="buscaDocumentosTransportadora(tipo, page)">
							</paging>                        
                        </div>
                    </td>
                </tfoot> 

			</table>
			
			<table class="table table-bordered"  ng-show="!showNFtable" style="overflow:scroll;display:block;">

				<thead>
					<tr>

						<th>N�mero DUE</th>
						<th>Emitente</th>
						<th>Consolidador</th>
						<th>Booking</th>
						<th>NCM</th>
						<th>Quantidade</th>
						<th>Embalagem</th>
						<th>Peso</th>
						<th>Editar</th>
						<th>Excluir</th>

					</tr>
				</thead>

				<tbody>
                    <tr ng-repeat="doc in docList">
                        <td style="text-align:center;font-size: 11px;">{{ doc.numero }}</td>
                        <td style="text-align:center;font-size: 11px; width:150px;"><a tooltip="{{ doc.emitente.razaoSocial }}" style="text-decoration: none; cursor: pointer;">{{ doc.emitente.cnpj }}</a></td>
                        <td style="text-align:center;font-size: 11px;width:150px;"><a tooltip="{{ doc.consolidador.razaoSocial }}" style="text-decoration: none; cursor: pointer;">{{ doc.consolidador.cnpj }}</a></td>
                        <td style="text-align:center;font-size: 11px;width:100px">{{ doc.booking }}</td>
                        <td style="text-align:center;font-size: 11px; width:100px;" ><a tooltip="{{doc.itens[0].ncm}}" style="text-decoration: none; cursor: pointer;">{{ doc.itens[0].ncmId}}</a></td>
                        <td style="text-align:center;font-size: 11px;">{{ doc.itens[0].quantidade}}</td>
                        <td style="text-align:center;font-size: 11px;">{{doc.itens[0].embalagem}}</td>
                        <td style="text-align:center;font-size: 11px; width: 120px">{{ doc.itens[0].peso}}</td>
                        <td style="text-align:center;font-size: 11px;width: 70px">
                        	<a title="Editar Documento" style='cursor: pointer;' ng-click="editDocumento(doc)"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
                        </td>
                        <td style="text-align:center;font-size: 11px;width: 70px">
                        	<a title="Excluir Documento" style='cursor: pointer;' ng-click="excluirDocumento(doc)"><i class='fa fa fa-times' aria-hidden='true'></i></a>
                        </td>
                    </tr>

                </tbody>
                
                  <tfoot>
                    <td colspan="14">
                        <div class="pagination pull-left">
                        	<paging
							  	page="currentPage" 
							  	page-size="itemsPerPage" 
							  	total="qtdRegistros"
							  	show-prev-next="true"
							  	show-first-last="true"
							  	paging-action="buscaDocumentosTransportadora(tipo, page)">
							</paging>                        
                        </div>
                    </td>
                </tfoot> 

			</table>
			

		</div>

	</div>
	
	<!-- MODALS -->
	
	<div class="modal-backdrop" ng-show="showDialogError">
        <div class="alert alert-block alert-error fade in" 
        	id="modalDialogError" 
		    style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center;">
            <h4 class="alert-heading">Aten��o</h4>
            <p>{{Mensagem}}</p>
            <p>
            	<button class="btn-primary btn-danger span2" ng-click="closeDialogError()" title="Fechar"
            		style="width: 30px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-top: 5%; margin-left: 47%;">
                    <i class="fa fa-check" aria-hidden="true"></i>
                </button>
            </p>
        </div>
    </div>

    <div class="modal-backdrop" ng-show="showDialogSuccess">
        <div class="alert alert-block alert-success fade in" 
        	id="modalDialogSuccess" 
		    style="opacity: 1; position: absolute; top: 50%; left: 50%; width: 500px; height: 200px; margin-left: -250px; margin-top: -100px; text-align: center;">
            <h4 class="alert-heading">Info</h4>
            <p>{{Mensagem}}</p>
            <p>
            	<button class="btn-primary btn-success span2" ng-click="closeDialogSuccess()"  title="Fechar"
            		style="width: 30px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-top: 5%; margin-left: 47%;">
                    <i class="fa fa-check" aria-hidden="true"></i>
                </button>
            </p>
        </div>
    </div>
    
    <!-- MODAL INCLUSAO / EDICAO -->
    <div class="modal-backdrop" ng-show="showModalNF" style="opacity: 1 !important;">
		<div class="alert alert-block alert-warning  fade in" style="opacity: 1; position: absolute; left: 12%; top: 0%; width: 75%; height: 100%; color: black; overflow-y: scroll; background-color: white; border: 0;">

			<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/modalNF.jsp'"></div>
								    
		</div>
	</div>

</div>