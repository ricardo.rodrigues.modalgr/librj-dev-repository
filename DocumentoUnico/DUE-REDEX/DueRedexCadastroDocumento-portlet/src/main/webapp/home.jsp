<%@ include file="init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	
<%
	if (hasPermissionDUE) {
%>
<div ng-app="App" ng-controller="MasterCtrl">

	<link rel="stylesheet" href="/DueRedexCadastroDocumentoPortlet/css/main.css" />
	<link rel="stylesheet" href="/DueRedexCadastroDocumentoPortlet/css/main.min.css" />

	<%
		Group liveGroup = (Group) request.getAttribute("site.liveGroup");
	%>
	
	<script>
		function selTipoOnChange() {
			angular.element(document.getElementById('tipo')).scope().selTipo();
		}	
	</script>

	<div id="divUrlApi" style="visibility: hidden">
		<liferay-ui:custom-attribute
			className="com.liferay.portal.model.Group"
			classPK="<%=(liveGroup != null) ? liveGroup.getGroupId() : 0%>"
			editable="<%=false%>" label="<%=false%>" name="dueRedexApiUrl" />
	</div>

	<input type="hidden" id="usuario" name="usuario"
		value='${nmUserLogado}' /> <input type="hidden" id="cpf" name="cpf"
		value='${cpf}' /> <input type="hidden" id="cnpjs" name="cnpjs"
		value='${cnpjs}' /> <input type="hidden" id="nomeTransportadora"
		name="nomeTransportadora" value='${nomeTransportadora}' /> <input
		type="hidden" id="validado" name="validado" value='${validado}' /> <input
		type="hidden" id="hasPermissionDUE" name="hasPermissionDUE"
		value='${hasPermissionDUE}' /> <input type="hidden"
		id="hasPermissionDespachante" name="hasPermissionDespachante"
		value='${hasPermissionDespachante}' /> <input type="hidden"
		id="hasPermissionSupervisor" name="hasPermissionSupervisor"
		value='${hasPermissionSupervisor}' />
		
	<input type="hidden" id="portletGrVersion" value="1.100">

	<div id="page-wrapper" class="container-fluid">
		
		<div class="span12" style="margim: 0px;">
			<div class="control-group" style="margim: 0px;">
				<ul class="nav nav-tabs" style="margim: 0px;">
				
					<li class="{{pagina.active}}">
						<a data-ng-click="setActive(1)" style="cursor: pointer;"> Cadastro NF </a>
					</li>
					<li class="{{pagina.active}}">
						<a data-ng-click="setActive(2)" style="cursor: pointer;"> Dados Motorista/Recibo </a>
					</li>
					
				</ul>
			</div>
			
			<div class="row-fluid" style="margim: 0px;">
				
				<div data-ng-show="nf.active" style="margim: 0px;">
					<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/nf.jsp'"></div>
				</div>
				
				<div data-ng-show="motoristaRecibo.active" style="margim: 0px;">
					<div data-ng-include="'/DueRedexCadastroDocumentoPortlet/paginas/motoristaRecibo.jsp'"></div>
				</div>
				
			</div>
			
		</div>

	</div>
	
</div>

<%
	} else {
%>
<div class="alert alert-error">Você não tem permissão de acessar
	essa funcionalidade</div>
<%
	}
%>