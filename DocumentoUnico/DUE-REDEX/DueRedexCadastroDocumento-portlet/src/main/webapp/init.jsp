<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.model.Group"%>
<%@ page import="com.liferay.portal.model.UserGroup"%>
<%@ page import="com.liferay.portal.theme.PortletDisplay"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.io.Serializable"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Map.Entry"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<!-- SCRIPTS -->
 
<script src="/DueRedexCadastroDocumentoPortlet/js/main.min.js"></script>
 
<script src="/DueRedexCadastroDocumentoPortlet/webjars/angular-ui-bootstrap/0.12.0/ui-bootstrap-tpls.js"></script>
<script src="/DueRedexCadastroDocumentoPortlet/webjars/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexCadastroDocumentoPortlet/webjars/font-awesome/4.7.0/css/font-awesome.min.css" />
 	
<!-- Plugins -->
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexCadastroDocumentoPortlet/webjars/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
<script src="/DueRedexCadastroDocumentoPortlet/webjars/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="/DueRedexCadastroDocumentoPortlet/webjars/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexCadastroDocumentoPortlet/css/main.css" />

<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/js/xml2json.js"></script>

<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/module/app.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/factory/nfService.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/factory/motoristaReciboService.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/factory/transportadoraService.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/factory/modalService.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/controller/masterCtrl.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/controller/motoristaReciboController.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/factory/FormataCampo.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/controller/nfController.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/capitalizeDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/numersOnlyDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/enterKeyDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/fileUploadDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/pagingDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/autoCompleteDirective.js"></script>
<script type="text/javascript" src="/DueRedexCadastroDocumentoPortlet/angularjs/directives/angucomplete.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexCadastroDocumentoPortlet/js/angucomplete/angucomplete.css" />

<%
List<UserGroup> groups = user.getUserGroups();
Boolean hasPermissionDUE = false;
Boolean hasPermissionDespachante = false;
Boolean hasPermissionSupervisor = false;
for (int x=0;x<groups.size();x++){
	if (groups.get(x).getName().equals("DUE")) { 
		hasPermissionDUE=true;
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Despachante")) {
		hasPermissionDespachante=true;		
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Supervisor DUE")) {
		hasPermissionSupervisor=true;		
		break;
	}
}

Map<String, Serializable> expandoBridgeAttributes = user.getExpandoBridge().getAttributes();
String cpf = (String) expandoBridgeAttributes.get("NumeroDocumentoUsuario");
String nomeTransportadora = (String) expandoBridgeAttributes.get("NomeRazaoSocial");
String cnpj = (String) expandoBridgeAttributes.get("NumeroDocumentoEmpresa");
String validado = (String) expandoBridgeAttributes.get("Validado");
List<String> cnpjs = Arrays.asList(cnpj);

session.setAttribute("cpf", cpf);
session.setAttribute("cnpjs", cnpjs);
session.setAttribute("nomeTransportadora", nomeTransportadora);
session.setAttribute("validado", validado);
session.setAttribute("nmUserLogado", user.getFullName());

session.setAttribute("hasPermissionDUE", hasPermissionDUE);
session.setAttribute("hasPermissionDespachante", hasPermissionDespachante);
session.setAttribute("hasPermissionSupervisor", hasPermissionSupervisor);
%>