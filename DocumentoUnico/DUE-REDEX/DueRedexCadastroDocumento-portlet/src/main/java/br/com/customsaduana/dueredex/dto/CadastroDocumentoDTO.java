package br.com.customsaduana.dueredex.dto;

import java.io.Serializable;
import java.util.List;

public class CadastroDocumentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public CadastroDocumentoDTO() {
		super();
	}
	
	private Documento documento;
	
	private List<Documento> documentoList;
	
	private boolean erro;
	
	private String mensagem;

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public List<Documento> getDocumentoList() {
		return documentoList;
	}

	public void setDocumentoList(List<Documento> documentoList) {
		this.documentoList = documentoList;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return "CadastroDocumentoDTO [documento=" + documento + ", documentoList=" + documentoList + ", erro=" + erro + ", mensagem=" + mensagem + "]";
	}
	
}
