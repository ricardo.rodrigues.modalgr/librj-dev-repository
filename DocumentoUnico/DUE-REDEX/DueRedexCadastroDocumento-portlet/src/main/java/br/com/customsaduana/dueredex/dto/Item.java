package br.com.customsaduana.dueredex.dto;

import java.io.Serializable;

public class Item implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private String numero;

	private String ncmId;
	
	private String ncm;

	private String quantidade;

	private String embalagemId;
	
	private String embalagem;

	private String peso;

	private String marcasNumeros;

	private String infoAdicional;

	private Documento documento;

	public long getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public String getNcm() {
		return ncm;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public String getEmbalagem() {
		return embalagem;
	}

	public String getPeso() {
		return peso;
	}

	public String getMarcasNumeros() {
		return marcasNumeros;
	}

	public String getInfoAdicional() {
		return infoAdicional;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public void setEmbalagem(String embalagem) {
		this.embalagem = embalagem;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public void setMarcasNumeros(String marcasNumeros) {
		this.marcasNumeros = marcasNumeros;
	}

	public void setInfoAdicional(String infoAdicional) {
		this.infoAdicional = infoAdicional;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getNcmId() {
		return ncmId;
	}

	public void setNcmId(String ncmId) {
		this.ncmId = ncmId;
	}

	public String getEmbalagemId() {
		return embalagemId;
	}

	public void setEmbalagemId(String embalagemId) {
		this.embalagemId = embalagemId;
	}

}