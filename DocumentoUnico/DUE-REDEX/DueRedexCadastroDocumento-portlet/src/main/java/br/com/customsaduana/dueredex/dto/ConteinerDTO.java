package br.com.customsaduana.dueredex.dto;

import java.util.List;

public class ConteinerDTO {
	
	public ConteinerDTO() {
		super();
	}
	
	public ConteinerDTO(Object[] o) {
		this.numeroConteiner 	= (o[0]  != null && !"".equals(o[0]))  ? o[0].toString()  : "";
		this.pesoBruto 			= (o[1]  != null && !"".equals(o[1]))  ? o[1].toString()  : "";
		this.lacre1 			= (o[2]  != null && !"".equals(o[2]))  ? o[2].toString()  : "";
		this.lacre2 			= (o[3]  != null && !"".equals(o[3]))  ? o[3].toString()  : "";
		this.lacre3 			= (o[4]  != null && !"".equals(o[4]))  ? o[4].toString()  : "";
		this.lacre4 			= (o[5]  != null && !"".equals(o[5]))  ? o[5].toString()  : "";
		this.excessoFrontal 	= (o[6]  != null && !"".equals(o[6]))  ? o[6].toString()  : "";
		this.excessoTraseiro 	= (o[7]  != null && !"".equals(o[7]))  ? o[7].toString()  : "";
		this.excessoAltura 		= (o[8]  != null && !"".equals(o[8]))  ? o[8].toString()  : "";
		this.excessoDireita 	= (o[9]  != null && !"".equals(o[9]))  ? o[9].toString()  : "";
		this.excessoEsquerda 	= (o[10] != null && !"".equals(o[10])) ? o[10].toString() : "";
		this.gkey		 		= (o[11] != null && !"".equals(o[11])) ? o[11].toString() : "";
		this.itemGkey		 	= (o[12] != null && !"".equals(o[12])) ? o[12].toString() : "";
		this.iso		 		= (o[13] != null && !"".equals(o[13])) ? o[13].toString() : "-";
		this.reefer		 		= (o[14] != null && !"".equals(o[14])) ? o[14].toString() : "-";
		this.imo		 		= (o[15] != null && !"".equals(o[15])) ? o[15].toString() : "-";
		this.descricaoISO 		= (o[16] != null && !"".equals(o[16])) ? o[16].toString() : "-";
		this.archType			= (o[17] != null && !"".equals(o[17])) ? o[17].toString() : "-";
		this.bookingItem = new BookingItemDTO(this.itemGkey, this.descricaoISO, this.iso, this.archType);
	}
	
	private String gkey;
	
	private String numeroConteiner;
	
	private String pesoBruto;
	
	private String lacre1;
	
	private String lacre2;
	
	private String lacre3;
	
	private String lacre4;
	
	private String excessoFrontal;
	
	private String excessoTraseiro;
	
	private String excessoAltura;
	
	private String excessoDireita;
	
	private String excessoEsquerda;
	
	private String itemGkey;
	
	private String iso;
	
	private String reefer;
	
	private String imo;
	
	private String doc;

	private List<DanfeDTO> danfeList;
	
	private String nomeTransportadora;
	
	private String cnpjTransportadora;
	
	private String descricaoISO;
	
	private BookingItemDTO bookingItem; 
	
	private String archType;

	public String getGkey() {
		return gkey;
	}

	public String getNumeroConteiner() {
		return numeroConteiner;
	}

	public String getPesoBruto() {
		return pesoBruto;
	}

	public String getLacre1() {
		return (lacre1 == null ? "" : lacre1.equals("null") ? "" : lacre1);
	}

	public String getLacre2() {
		return (lacre2 == null ? "" : lacre2.equals("null") ? "" : lacre2);
	}

	public String getLacre3() {
		return (lacre3 == null ? "" : lacre3.equals("null") ? "" : lacre3);
	}

	public String getLacre4() {
		return (lacre4 == null ? "" : lacre4.equals("null") ? "" : lacre4);
	}

	public String getExcessoFrontal() {
		return excessoFrontal;
	}

	public String getExcessoTraseiro() {
		return excessoTraseiro;
	}

	public String getExcessoAltura() {
		return excessoAltura;
	}

	public String getExcessoDireita() {
		return excessoDireita;
	}

	public String getExcessoEsquerda() {
		return excessoEsquerda;
	}

	public String getItemGkey() {
		return itemGkey;
	}

	public String getIso() {
		return iso;
	}

	public String getReefer() {
		return reefer;
	}

	public String getImo() {
		return imo;
	}

	public String getDoc() {
		return doc;
	}

	public List<DanfeDTO> getDanfeList() {
		return danfeList;
	}

	public void setGkey(String gkey) {
		this.gkey = gkey;
	}

	public void setNumeroConteiner(String numeroConteiner) {
		this.numeroConteiner = numeroConteiner;
	}

	public void setPesoBruto(String pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public void setLacre1(String lacre1) {
		this.lacre1 = lacre1;
	}

	public void setLacre2(String lacre2) {
		this.lacre2 = lacre2;
	}

	public void setLacre3(String lacre3) {
		this.lacre3 = lacre3;
	}

	public void setLacre4(String lacre4) {
		this.lacre4 = lacre4;
	}

	public void setExcessoFrontal(String excessoFrontal) {
		this.excessoFrontal = excessoFrontal;
	}

	public void setExcessoTraseiro(String excessoTraseiro) {
		this.excessoTraseiro = excessoTraseiro;
	}

	public void setExcessoAltura(String excessoAltura) {
		this.excessoAltura = excessoAltura;
	}

	public void setExcessoDireita(String excessoDireita) {
		this.excessoDireita = excessoDireita;
	}

	public void setExcessoEsquerda(String excessoEsquerda) {
		this.excessoEsquerda = excessoEsquerda;
	}

	public void setItemGkey(String itemGkey) {
		this.itemGkey = itemGkey;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public void setReefer(String reefer) {
		this.reefer = reefer;
	}

	public void setImo(String imo) {
		this.imo = imo;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public void setDanfeList(List<DanfeDTO> danfeList) {
		this.danfeList = danfeList;
	}

	public String getNomeTransportadora() {
		return nomeTransportadora;
	}

	public String getCnpjTransportadora() {
		return cnpjTransportadora;
	}

	public void setNomeTransportadora(String nomeTransportadora) {
		this.nomeTransportadora = nomeTransportadora;
	}

	public void setCnpjTransportadora(String cnpjTransportadora) {
		this.cnpjTransportadora = cnpjTransportadora;
	}
	
	public BookingItemDTO getBookingItem() {
		return bookingItem;
	}

	public void setBookingItem(BookingItemDTO bookingItem) {
		this.bookingItem = bookingItem;
	}

	public String getDescricaoISO() {
		return descricaoISO;
	}

	public void setDescricaoISO(String descricaoISO) {
		this.descricaoISO = descricaoISO;
	}
	
	public String getArchType() {
		return archType;
	}

	public void setArchType(String archType) {
		this.archType = archType;
	}	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ConteinerDTO [gkey=" + gkey + ", numeroConteiner=" + numeroConteiner + ", pesoBruto=" + pesoBruto + ", lacre1=" + lacre1 + ", lacre2=" + lacre2 + ", lacre3=" + lacre3 + ", lacre4=" + lacre4 + ", excessoFrontal=" + excessoFrontal + ", excessoTraseiro=" + excessoTraseiro + ", excessoAltura=" + excessoAltura + ", excessoDireita=" + excessoDireita + ", excessoEsquerda=" + excessoEsquerda + ", itemGkey=" + itemGkey + ", iso=" + iso + ", reefer=" + reefer + ", imo=" + imo + ", doc=" + doc + ", descricaoISO=" + descricaoISO + ", ");
		sb.append("danfeList=");
		sb.append("bookingItem=");
		if(bookingItem != null){
			bookingItem.toString();
		} else {
			sb.append("NULL");
		}
		sb.append(" ]");
		return sb.toString();
	}

}
