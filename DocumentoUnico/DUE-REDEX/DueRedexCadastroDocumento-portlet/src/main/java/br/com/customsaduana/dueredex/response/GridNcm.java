package br.com.customsaduana.dueredex.response;

import java.util.List;

public class GridNcm {

    private List<CETNcm> listaNcmResponses;
    
    private List<CETNcmView> listaNcmViewResponses;

	public List<CETNcm> getListaNcmResponses() {
		return listaNcmResponses;
	}

	public void setListaNcmResponses(List<CETNcm> listaNcmResponses) {
		this.listaNcmResponses = listaNcmResponses;
	}

	public List<CETNcmView> getListaNcmViewResponses() {
		return listaNcmViewResponses;
	}

	public void setListaNcmViewResponses(List<CETNcmView> listaNcmViewResponses) {
		this.listaNcmViewResponses = listaNcmViewResponses;
	}
}
