package br.com.customsaduana.dueredex.dto;

public class DocumentoRecibo {
	
	private String numero;
	
	private String lote;
	
	private String quantidade;
	
	private String peso;
	
	private String exportador;
	
	private String consolidador;
	
	private String bookingnf;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getExportador() {
		return exportador;
	}

	public void setExportador(String exportador) {
		this.exportador = exportador;
	}

	public String getConsolidador() {
		return consolidador;
	}

	public void setConsolidador(String consolidador) {
		this.consolidador = consolidador;
	}

	public String getBookingnf() {
		return bookingnf;
	}

	public void setBookingnf(String bookingnf) {
		this.bookingnf = bookingnf;
	}
}
