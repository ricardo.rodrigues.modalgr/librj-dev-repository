package br.com.customsaduana.dueredex.response;


public class CETNcm {
	
	private Long objRef;

	private Integer objRefNCMSintetico;

	private String codigo;

	private String descricao;

	private String representatividade;

	private String liquido;

	private String anvisa;

	private String policiaFederal;

	private String exercito;
	
	private String codigoDescricao;

	public Long getObjRef() {
		return objRef;
	}

	public void setObjRef(Long objRef) {
		this.objRef = objRef;
	}

	public Integer getObjRefNCMSintetico() {
		return objRefNCMSintetico;
	}

	public void setObjRefNCMSintetico(Integer objRefNCMSintetico) {
		this.objRefNCMSintetico = objRefNCMSintetico;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getRepresentatividade() {
		return representatividade;
	}

	public void setRepresentatividade(String representatividade) {
		this.representatividade = representatividade;
	}

	public String getLiquido() {
		return liquido;
	}

	public void setLiquido(String liquido) {
		this.liquido = liquido;
	}

	public String getAnvisa() {
		return anvisa;
	}

	public void setAnvisa(String anvisa) {
		this.anvisa = anvisa;
	}

	public String getPoliciaFederal() {
		return policiaFederal;
	}

	public void setPoliciaFederal(String policiaFederal) {
		this.policiaFederal = policiaFederal;
	}

	public String getExercito() {
		return exercito;
	}

	public void setExercito(String exercito) {
		this.exercito = exercito;
	}
	
	public String getCodigoDescricao() {
		return codigoDescricao;
	}

	public void setCodigoDescricao(String codigoDescricao) {
		this.codigoDescricao = codigoDescricao;
	}
}
