package br.com.customsaduana.dueredex.service;

import java.io.FileOutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.customsaduana.dueredex.dto.CadastroDocumentoDTO;
import br.com.customsaduana.dueredex.dto.ConteinerDTO;
import br.com.customsaduana.dueredex.dto.Documento;
import br.com.customsaduana.dueredex.dto.DocumentoDTO;
import br.com.customsaduana.dueredex.dto.DocumentoRequestDTO;
import br.com.customsaduana.dueredex.dto.MotoristaReciboDTO;
import br.com.customsaduana.dueredex.dto.Transporte;
import br.com.customsaduana.dueredex.response.GridEmbalagem;
import br.com.customsaduana.dueredex.response.GridNcm;
import br.com.customsaduana.dueredex.util.PropertiesUtil;

@Service
public class DueRedexCadastroDocumentoService {

	@Autowired
	private RestTemplate rest;
	
	private HttpHeaders getHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	public boolean existeCadastroShipper(String cnpj) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/existecadastroshipper");
		builder.queryParam("cnpj", cnpj);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Boolean> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, Boolean.class);
		return responseEntity.getBody();
	}

	public CadastroDocumentoDTO salvaDocumento(Documento dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/salvadocumento");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Documento> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<CadastroDocumentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroDocumentoDTO.class);
		return responseEntity.getBody();
	}

	public CadastroDocumentoDTO validaModificacaoDocumento(Documento dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validamodificacaodocumento");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Documento> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<CadastroDocumentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroDocumentoDTO.class);
		return responseEntity.getBody();
	}

	public DocumentoDTO buscaDocumentostransportadora(String cnpj, String tipo, String pageIndex, String pageSize) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscadocumentostransportadora");
		builder.queryParam("cnpj", cnpj);
		builder.queryParam("tipo", tipo);
		builder.queryParam("pageindex", pageIndex);
		builder.queryParam("pagesize", pageSize);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<DocumentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, DocumentoDTO.class);
		return responseEntity.getBody();
	}

	public DocumentoDTO buscaDocumentostransportadoraFiltro(DocumentoRequestDTO dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscadocumentostransportadorafiltro");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<DocumentoRequestDTO> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<DocumentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, DocumentoDTO.class);
		return responseEntity.getBody();
	}
	
	public GridEmbalagem buscaEmbalagens() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscaembalagens");

		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<GridEmbalagem> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, GridEmbalagem.class);
		return responseEntity.getBody();
	}

	public String excluirDocumento(Documento documento) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/excluirdocumento");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Documento> entity = new HttpEntity<>(documento, headers);
		ResponseEntity<String> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, String.class);
		return responseEntity.getBody();
	}

	public GridNcm buscaCommodities() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscacommodities");

		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<GridNcm> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, GridNcm.class);
		return responseEntity.getBody();
	}
	
	/***/

	public MotoristaReciboDTO buscaTransportadora(String cnpj) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscatransportadora");
		builder.queryParam("cnpj", cnpj);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public MotoristaReciboDTO buscaMotorista(String searchDoc, boolean isEstrangeiro) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscamotorista");
		builder.queryParam("searchDoc", searchDoc);
		builder.queryParam("isEstrangeiro", isEstrangeiro);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public MotoristaReciboDTO buscaDocumentosDisponiveis(String cnpj, String novo) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/buscadocumentosdisponiveis");
		builder.queryParam("cnpj", cnpj);
		builder.queryParam("novo", novo);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public MotoristaReciboDTO salvaRecibo(Transporte dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/salvarecibo");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Transporte> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public CadastroDocumentoDTO validaModificacaoDocumentos(List<Documento> documentos) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validamodificacaodocumentos");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<List<Documento>> entity = new HttpEntity<>(documentos, headers);
		ResponseEntity<CadastroDocumentoDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, CadastroDocumentoDTO.class);
		return responseEntity.getBody();
	}

	public MotoristaReciboDTO excluirRecibo(Transporte dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/excluirrecibo");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Transporte> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public MotoristaReciboDTO validaImpressaoRecibo(Transporte dto) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/validaimpressaorecibo");
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<Transporte> entity = new HttpEntity<>(dto, headers);
		ResponseEntity<MotoristaReciboDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.POST, entity, MotoristaReciboDTO.class);
		return responseEntity.getBody();
	}

	public String imprimeRecibo(Long idTransporte) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/recibo");
		builder.queryParam("idTransporte", idTransporte);
		URI urlFormatterd = builder.build().encode().toUri();
		
		HttpHeaders headers = new HttpHeaders();

	    headers.add("Content-Description", "File Transfer");
	    headers.add("Content-Disposition", "attachment; filename=recibo.pdf");
	    headers.add("Content-Transfer-Encoding", "binary");
	    headers.add("Connection", "Keep-Alive");
	    headers.setContentType(
	    		MediaType.parseMediaType("application/pdf"));
		
		headers.setAccept(Arrays.asList(MediaType.ALL));
		
		HttpEntity entity = new HttpEntity(headers);

		rest.getMessageConverters().add(new ByteArrayHttpMessageConverter());    

		FileOutputStream out = null;
		ResponseEntity<byte[]> result = rest.exchange(urlFormatterd, HttpMethod.GET, entity, byte[].class);
		
//		String encoded = Base64.getEncoder().encodeToString(result.getBody());
		String encoded = Base64.encodeBase64String(result.getBody());
		return encoded;
		
		
	}

	public ConteinerDTO getTransportadoraInfo(String cnpjs) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(PropertiesUtil.getProp() + "/api/gettransportadorainfo");
		builder.queryParam("cnpjs", cnpjs);
		URI urlFormatterd = builder.build().encode().toUri();
		HttpHeaders headers = this.getHttpHeaders();

		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ConteinerDTO> responseEntity = rest.exchange(urlFormatterd, HttpMethod.GET, entity, ConteinerDTO.class);
		return responseEntity.getBody();
	}

}
