package br.com.customsaduana.dueredex.response;

import java.util.List;

public class GridEmbalagem {
	private List<Embalagem> listaEmbalagemResponses;
	
	private List<EmbalagemView> listaEmbalagemViewResponses;    

	public List<EmbalagemView> getListaEmbalagemViewResponses() {
		return listaEmbalagemViewResponses;
	}

	public void setListaEmbalagemViewResponses(List<EmbalagemView> listaEmbalagemViewResponses) {
		this.listaEmbalagemViewResponses = listaEmbalagemViewResponses;
	}

	public List<Embalagem> getListaEmbalagemResponses() {
		return listaEmbalagemResponses;
	}

	public void setListaEmbalagemResponses(List<Embalagem> listaEmbalagemResponses) {
		this.listaEmbalagemResponses = listaEmbalagemResponses;
	}
	
}
