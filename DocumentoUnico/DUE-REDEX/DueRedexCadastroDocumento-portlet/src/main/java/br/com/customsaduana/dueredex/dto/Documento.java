package br.com.customsaduana.dueredex.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Documento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;

	private String tipo;

	private String numero;

	private Shipper emitente;
	
	private Shipper consolidador;

	private String booking;

	private String danfe;

	private String ruc;

	private String cnpjTransportadora;

    private Transporte transporte;

	private List<Item> itens;
	
	private String blGkey;
	
	private String blNbr;
	
	private String blNotes;
	
	private Date DataCadastro;
	
	private String NumeroConteiner;	
	
	public Date getDataCadastro() {
		return DataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		DataCadastro = dataCadastro;
	}

	public String getNumeroConteiner() {
		return NumeroConteiner;
	}

	public void setNumeroConteiner(String numeroConteiner) {
		NumeroConteiner = numeroConteiner;
	}

	public Documento() {
		super();
	}
	
	public Long getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public String getNumero() {
		return numero;
	}

	public String getBooking() {
		return booking;
	}

	public String getDanfe() {
		return danfe;
	}

	public String getRuc() {
		return ruc;
	}

	public String getCnpjTransportadora() {
		return cnpjTransportadora;
	}

	public Transporte getTransporte() {
		return transporte;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public void setDanfe(String danfe) {
		this.danfe = danfe;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public void setCnpjTransportadora(String cnpjTransportadora) {
		this.cnpjTransportadora = cnpjTransportadora;
	}

	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Shipper getEmitente() {
		return emitente;
	}

	public void setEmitente(Shipper emitente) {
		this.emitente = emitente;
	}

	public Shipper getConsolidador() {
		return consolidador;
	}

	public void setConsolidador(Shipper consolidador) {
		this.consolidador = consolidador;
	}

	public String getBlGkey() {
		return blGkey;
	}

	public void setBlGkey(String blGkey) {
		this.blGkey = blGkey;
	}

	public String getBlNbr() {
		return blNbr;
	}

	public void setBlNbr(String blNbr) {
		this.blNbr = blNbr;
	}

	public String getBlNotes() {
		return blNotes;
	}

	public void setBlNotes(String blNotes) {
		this.blNotes = blNotes;
	}

	
}