package br.com.customsaduana.dueredex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.customsaduana.dueredex.dto.CadastroDocumentoDTO;
import br.com.customsaduana.dueredex.dto.ConteinerDTO;
import br.com.customsaduana.dueredex.dto.Documento;
import br.com.customsaduana.dueredex.dto.DocumentoDTO;
import br.com.customsaduana.dueredex.dto.DocumentoRequestDTO;
import br.com.customsaduana.dueredex.dto.MotoristaReciboDTO;
import br.com.customsaduana.dueredex.dto.Transporte;
import br.com.customsaduana.dueredex.response.GridEmbalagem;
import br.com.customsaduana.dueredex.response.GridNcm;
import br.com.customsaduana.dueredex.service.DueRedexCadastroDocumentoService;

@RestController
@RequestMapping("/dueredex")
public class DueRedexCadastroDocumentoController {
	
	@Autowired
	private DueRedexCadastroDocumentoService dueRedexCadastroDocumentoService;
	
	@RequestMapping(value = "/api/existecadastroshipper", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean existeCadastroShipper(@RequestParam("cnpj") String cnpj) {
		return dueRedexCadastroDocumentoService.existeCadastroShipper(cnpj);
	}
	
	@RequestMapping(value = "/api/salvadocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO salvaDocumento(@RequestBody(required = true) Documento dto) {
		return dueRedexCadastroDocumentoService.salvaDocumento(dto);
	}
	
	@RequestMapping(value = "/api/validamodificacaodocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO validaModificacaoDocumento(@RequestBody(required = true) Documento dto) {
		return dueRedexCadastroDocumentoService.validaModificacaoDocumento(dto);
	}
	
	@RequestMapping(value = "/api/buscadocumentostransportadora", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public DocumentoDTO buscaDocumentosTransportadora(@RequestParam("cnpj") String cnpj, @RequestParam("tipo") String tipo, 
			 @RequestParam("pageindex") String pageIndex, @RequestParam("pagesize") String pageSize) {
		return dueRedexCadastroDocumentoService.buscaDocumentostransportadora(cnpj, tipo, pageIndex, pageSize);
	}
	
	@RequestMapping(value = "/api/buscadocumentostransportadorafiltro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public DocumentoDTO buscaDocumentosTransportadoraPost(@RequestBody(required = true) DocumentoRequestDTO dto) {
		return dueRedexCadastroDocumentoService.buscaDocumentostransportadoraFiltro(dto);
	}	
	
	@RequestMapping(value = "/api/buscaembalagens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GridEmbalagem buscaEmbalagens() {
		return dueRedexCadastroDocumentoService.buscaEmbalagens();
	}
	
	@RequestMapping(value = "/api/excluirdocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String excluirDocumento(@RequestBody(required = true) Documento documento) {
		return dueRedexCadastroDocumentoService.excluirDocumento(documento);
	}
	
	@RequestMapping(value = "/api/buscacommodities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GridNcm buscaCommodities() {
		return dueRedexCadastroDocumentoService.buscaCommodities();
	}
	
	/***/
	
	@RequestMapping(value = "/api/buscatransportadora", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaTransportadora(@RequestParam("cnpj") String cnpj) {
		return dueRedexCadastroDocumentoService.buscaTransportadora(cnpj);
	}

	@RequestMapping(value = "/api/buscamotorista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaMotorista(@RequestParam("searchDoc") String searchDoc, @RequestParam("isEstrangeiro") boolean isEstrangeiro) {
		return dueRedexCadastroDocumentoService.buscaMotorista(searchDoc, isEstrangeiro);
	}

	@RequestMapping(value = "/api/buscadocumentosdisponiveis", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO buscaDocumentosDisponiveis(@RequestParam("cnpj") String cnpj, @RequestParam("novo") String novo) {
		return dueRedexCadastroDocumentoService.buscaDocumentosDisponiveis(cnpj, novo);
	}

	@RequestMapping(value = "/api/salvarecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO salvaRecibo(@RequestBody(required = true) Transporte dto) {
		return dueRedexCadastroDocumentoService.salvaRecibo(dto);
	}

	@RequestMapping(value = "/api/validamodificacaodocumentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CadastroDocumentoDTO validaModificacaoDocumentos(@RequestBody(required = true) List<Documento> documentos) {
		return dueRedexCadastroDocumentoService.validaModificacaoDocumentos(documentos);
	}

	@RequestMapping(value = "/api/excluirrecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO excluirRecibo(@RequestBody(required = true) Transporte dto) {
		return dueRedexCadastroDocumentoService.excluirRecibo(dto);
	}
	
	@RequestMapping(value = "/api/validaimpressaorecibo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MotoristaReciboDTO validaImpressaoRecibo(@RequestBody(required = true) Transporte dto) {
		return dueRedexCadastroDocumentoService.validaImpressaoRecibo(dto);
	}
	
	@RequestMapping(value = "/api/recibo", method = RequestMethod.GET)
	@ResponseBody	
	public String imprimeRecibo(@RequestParam("idTransporte") Long idTransporte) {
		return dueRedexCadastroDocumentoService.imprimeRecibo(idTransporte);
	}
	
	@RequestMapping(value = "/api/gettransportadorainfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConteinerDTO getTransportadoraInfo(@RequestParam("cnpjs") String cnpjs) {
		return dueRedexCadastroDocumentoService.getTransportadoraInfo(cnpjs);
	}
}
