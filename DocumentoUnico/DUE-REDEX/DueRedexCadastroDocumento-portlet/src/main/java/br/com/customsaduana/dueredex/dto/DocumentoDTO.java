package br.com.customsaduana.dueredex.dto;

import java.util.List;

public class DocumentoDTO {
	
	private long qtdRegistros;
	
	private int currentPage;
	
	public List<Documento> listaDocumento;

	public long getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(long qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}

	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	
	
}
