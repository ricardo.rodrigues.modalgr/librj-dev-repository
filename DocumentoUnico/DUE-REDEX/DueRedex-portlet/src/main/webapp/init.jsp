<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.model.Group"%>
<%@ page import="com.liferay.portal.model.UserGroup"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.io.Serializable"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Map.Entry"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<!-- SCRIPTS -->
<script src="/DueRedexPortlet/js/main.min.js"></script>

<!-- Bootstrap -->
<script src="/DueRedexPortlet/webjars/angular-ui-bootstrap/0.12.0/ui-bootstrap-tpls.js"></script>
<script src="/DueRedexPortlet/webjars/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexPortlet/webjars/font-awesome/4.7.0/css/font-awesome.min.css" />

<!-- Plugins -->
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexPortlet/webjars/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
<script src="/DueRedexPortlet/webjars/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="/DueRedexPortlet/webjars/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/DueRedexPortlet/css/main.css" />


<script type="text/javascript" src="/DueRedexPortlet/angularjs/module/app.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/factory/monitoramentoService.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/factory/parametrosService.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/factory/cfpService.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/factory/modalService.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/controller/masterCtrl.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/controller/monitoramentoController.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/controller/parametrosController.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/controller/cfpController.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/directives/capitalizeDirective.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/directives/numersOnlyDirective.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/directives/enterKeyDirective.js"></script>
<script type="text/javascript" src="/DueRedexPortlet/angularjs/directives/pagingDirective.js"></script>


<%
List<UserGroup> groups = user.getUserGroups();
Boolean hasPermissionDUE = false;
Boolean hasPermissionDespachante = false;
Boolean hasPermissionSupervisor = false;
for (int x=0;x<groups.size();x++){
	if (groups.get(x).getName().equals("DUE")) { 
		hasPermissionDUE=true;
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Despachante")) {
		hasPermissionDespachante=true;		
		break;
	}
}

for (int x=0;x<groups.size();x++){	
	if (groups.get(x).getName().equals("Supervisor DUE")) {
		hasPermissionSupervisor=true;		
		break;
	}
}

Map<String, Serializable> expandoBridgeAttributes = user.getExpandoBridge().getAttributes();
String cpf = (String) expandoBridgeAttributes.get("NumeroDocumentoUsuario");
String nomeTransportadora = (String) expandoBridgeAttributes.get("NomeRazaoSocial");
String cnpj = (String) expandoBridgeAttributes.get("NumeroDocumentoEmpresa");
String validado = (String) expandoBridgeAttributes.get("Validado");
List<String> cnpjs = Arrays.asList(cnpj);

session.setAttribute("cpf", cpf);
session.setAttribute("cnpjs", cnpjs);
session.setAttribute("nomeTransportadora", nomeTransportadora);
session.setAttribute("validado", validado);
session.setAttribute("nmUserLogado", user.getFullName());

session.setAttribute("hasPermissionDUE", hasPermissionDUE);
session.setAttribute("hasPermissionDespachante", hasPermissionDespachante);
session.setAttribute("hasPermissionSupervisor", hasPermissionSupervisor);
%>