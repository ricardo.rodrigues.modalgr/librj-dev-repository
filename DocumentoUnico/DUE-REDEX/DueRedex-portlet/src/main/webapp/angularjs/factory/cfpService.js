app.factory('cfpService', ['$http', '$q',
	function($http, $q) {

	function AllowAccesServiceAPI() {
		$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		// Send this header only in post requests.
		// Specifies you are sending a JSON object
		$http.defaults.headers.post['dataType'] = 'json';
		$http.defaults.headers.post['Content-Type'] = 'application/json';
	}
	
    var _pesquisaBooking = function(apiUrl, booking, lineOperatorGkey, despachante) {
    	AllowAccesServiceAPI();
//        return $http.get(apiUrl + "/api/cfp?bookingNbr=" + booking + "&lineOperatorGkey=" + lineOperatorGkey + "&despachante=" + despachante);
    	return $http.get("/DueRedexPortlet/dueredex/api/cfp?bookingNbr=" + booking + "&lineOperatorGkey=" + lineOperatorGkey + "&despachante=" + despachante);
    }
    
    var _validaCheckObrigatorio = function(apiUrl) {
    	AllowAccesServiceAPI();
//        return $http.get(apiUrl + "/api/validaparam");
    	return $http.get("/DueRedexPortlet/dueredex/api/validaparam");
    }
    
    var _salvaListaCFP = function(apiUrl, dto) {
    	AllowAccesServiceAPI();
    	var config = {
    		headers : {
    			"Content-Type": "application/json;charset=UTF-8"
    		}
    	}
    	
//        return $http.post( apiUrl + '/api/savecfp', dto, config );
    	return $http.post( "/DueRedexPortlet/dueredex/api/savecfp", dto, config );
    }

    return {
    	pesquisaBooking: _pesquisaBooking,
    	validaCheckObrigatorio : _validaCheckObrigatorio,
    	salvaListaCFP : _salvaListaCFP
    }

}]);