app.factory('monitoramentoService', ['$http', '$q',
	function($http, $q) {
	
		function AllowAccesServiceAPI() {
			$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
			// Send this header only in post requests.
			// Specifies you are sending a JSON object
			$http.defaults.headers.post['dataType'] = 'json';
			$http.defaults.headers.post['Content-Type'] = 'application/json';
		}
		
	    var _findMonitoramentoOLD = function(apiUrl, searchText, tipo, pageindex, pagesize) {
        	AllowAccesServiceAPI();
//        	return  $http.get(apiUrl + "/api/monitoramento?tipo=" + tipo + "&searchtext=" + searchText + "&pageindex=" + pageindex + "&pagesize=" + pagesize);
        	return  $http.get("/DueRedexPortlet/dueredex/api/monitoramento?tipo=" + tipo + "&searchtext=" + searchText + "&pageindex=" + pageindex + "&pagesize=" + pagesize);
           
        }
        
        var _findMonitoramento = function(dto) {
        	
        	var config = {
        		headers : {
        			"Content-Type": "application/json;charset=UTF-8"
        		}
        	}
//            return $http.post( apiUrl + '/api/savecfp', dto, config );
        	return $http.post("/DueRedexPortlet/dueredex/api/monitoramento", dto, config );
        }
	            
	    var _finalizar = function(apiUrl, cctId) {
	    	AllowAccesServiceAPI();
//	    	return $http.get(apiUrl + "/api/monitoramento/finalizar?cctId=" + cctId);
	    	return $http.get("/DueRedexPortlet/dueredex/api/monitoramento/finalizar?cctId=" + cctId);
	    }
	
	    return {
	    	findMonitoramento: _findMonitoramento,
	    	finalizar : _finalizar
	    }
	}
]);