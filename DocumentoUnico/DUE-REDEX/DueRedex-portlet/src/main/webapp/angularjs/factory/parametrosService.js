app.factory('parametrosService', ['$http', '$q',
    function($http, $q) {
	
		function AllowAccesServiceAPI() {
			$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
			// Send this header only in post requests.
			// Specifies you are sending a JSON object
			$http.defaults.headers.post['dataType'] = 'json';
			$http.defaults.headers.post['Content-Type'] = 'application/json';
		}

        var _findAllParametros = function(apiUrl) {
        	AllowAccesServiceAPI();
            var def = $q.defer();
//            $http.get(apiUrl + "/api/parametros").then(
            $http.get("/DueRedexPortlet/dueredex/api/parametros").then(
                function(response) {
                    console.log(response);
                    def.resolve(response);
                },
                function(error) {
                    console.log('error', error);
                });
            return def.promise;
        }

        var _saveParametrosDivergenciaPeso = function(apiUrl, parametros) {
        	AllowAccesServiceAPI();
            var def = $q.defer();
            $http({
//                url: apiUrl + '/api/parametros/divergenciapeso',
            	url: "/DueRedexPortlet/dueredex/api/parametros/divergenciapeso",
                method: "POST",
                data: parametros,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then(function(response) {
                console.log(response);
                def.resolve(response);
            }, function(error) {
                console.log('error', error);
                def.rejected(error);
            });
            return def.promise;
        }
        
        var _salvarParametrosClienteFinanceiroPagador = function(apiUrl, obj) {
        	AllowAccesServiceAPI();
            var def = $q.defer();
            $http({
//                url: apiUrl + '/api/parametros/clientefinanceiropagador',
                url: "/DueRedexPortlet/dueredex/api/parametros/clientefinanceiropagador",
                method: "POST",
                data: obj,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then(function(response) {
                console.log(response);
                def.resolve(response);
            }, function(error) {
                console.log('error', error);
                def.rejected(error);
            });
            return def.promise;
        }
        
        var _salvarParametrosBalanca = function(apiUrl, listaParametro) {
        	AllowAccesServiceAPI();
        	
        	var config = {
        		headers : {
        			"Content-Type": "application/json;charset=UTF-8"
        		}
        	}
        	
//            return $http.post( apiUrl + '/api/parametros/balanca', listaParametro, config );
        	return $http.post( "/DueRedexPortlet/dueredex/api/parametros/balanca", listaParametro, config );
        }

        return {
            findAllParametros: _findAllParametros,
            saveParametrosDivergenciaPeso: _saveParametrosDivergenciaPeso,
            salvarParametrosClienteFinanceiroPagador : _salvarParametrosClienteFinanceiroPagador,
            salvarParametrosBalanca : _salvarParametrosBalanca
        }
    }
]);