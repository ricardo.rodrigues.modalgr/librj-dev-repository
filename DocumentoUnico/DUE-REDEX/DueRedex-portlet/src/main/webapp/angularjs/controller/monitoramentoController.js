app.controller('monitoramentoController', ['$scope', '$modal', '$location', '$rootScope', 'monitoramentoService', '$filter', 'modalService', 
    function($scope, $modal, $location, $rootScope, monitoramentoService, $filter, modalService) {

        $scope.apiURL = $(document.getElementById("divUrlApi").childNodes[1]).find('span').html();
        console.log("API URL -> " + $scope.apiURL);       
        
        $scope.showDialogError = false;
        $scope.Mensagem = "";
        
        $scope.filter = {};

        $scope.sortType = 'dataEntrada';
        $scope.sortReverse = true;
        $scope.searchText = '';
        $scope.exibeErros = false;
        $scope.listaMonitoramento = [];
        $scope.showFilters = false;
        
        $scope.itemsPerPage = 10;
        $scope.qtdRegistros = 0;
        $scope.currentPage = 1;
        $scope.cctId = 0;

        $scope.qtdporpaginalist = [{ id: 0, qtd: 10 }, { id: 1, qtd: 25 }, { id: 2, qtd: 50 }, { id: 3, qtd: 100 }];
        $scope.qtdporpagina = { id: 0, qtd: 10 };
        $scope.changeqtdporpagina = function() {
            $scope.currentPage = 0;
            $scope.itemsPerPage = $scope.qtdporpagina.qtd;
            $scope.findMonitoramento();
        };


        $scope.dataHoje = moment().format("DD/MM/YY");

        $scope.cctDetail = {};
        $scope.showModalDetailCCT = false;

        $scope.showDetailCCT = function(cct) {
        	this.Modal = $modal.open({
        		templateUrl: '/DueRedexPortlet/paginas/modal-dados-conteiner.html',
        		controller: function($modalInstance, $scope){
        		    $scope.cctDetail = cct;
                    $scope.showModalDetailCCT = true;
                    $scope.infoContainer = cct.container;
                    
                    $scope.closeDetailCCT = function() {
                        $modalInstance.close('cancel');
                    };
                    
        		}
        	});
        
        };

        $scope.tipo = {
            value: '1'
        };
        
		$scope.limparCampos = function(){
		  	  $scope.filter.navio = "";
			  $scope.filter.visit = "";
			  $scope.filter.lineOperator = "";
			  $scope.filter.booking = "";
			  $scope.filter.conteiner = "";
			  $scope.filter.danfe = "";
			  $scope.filter.due = "";  
			  $scope.filter.dataEntradaDe = "";
			  $scope.filter.dataEntradaAte = "";
			  $scope.filter.dataProcDe = "";
			  $scope.filter.dataProcAte = "";
		}

        /**
         * TIPO
         * 1 - ATIVOS
         * 2 - ERRORS
         * 3 - ENTREGUES / DEVOLVIDOS
         * */
        $scope.findMonitoramento = function(page) {

            if(page != undefined) {
      	       $scope.currentPage = page;
         	} else {
         		$scope.currentPage = 1;
         	}
            
            $scope.filter.dataEntradaDe = $('#filterDataEntradaDe').val();
            $scope.filter.dataEntradaAte = $('#filterDataEntradaAte').val();
            $scope.filter.dataProcDe = $('#filterDataProcDe').val();
            $scope.filter.dataProcAte = $('#filterDataProcAte').val();
            
            if($scope.periodosInvalidos()) {
            	return;
            }
            
            $scope.filter.tipo = $scope.tipo.value;
            $scope.filter.pageIndex = $scope.currentPage;
            $scope.filter.pageSize = $scope.itemsPerPage;
            
            if($scope.filter.navio == undefined) {$scope.filter.navio = "";}
            if($scope.filter.visit == undefined) {$scope.filter.visit = "";}
            if($scope.filter.lineOperator == undefined) {$scope.filter.lineOperator = "";}
            if($scope.filter.booking == undefined) {$scope.filter.booking = "";}
            if($scope.filter.conteiner == undefined) {$scope.filter.conteiner = "";}
            if($scope.filter.danfe == undefined) {$scope.filter.danfe = "";}
            if($scope.filter.due == undefined) {$scope.filter.due = "";}
            
            console.log($scope.filter);

            $scope.openGifModal();
            monitoramentoService.findMonitoramento($scope.filter).then(
                function(response) {
                    console.log(response.data);
                    $scope.pagedItems = [];
                    if($scope.tipo.value == '3') {
                    	$scope.listaMonitoramento = response.data.listaFinalizado;
                    } else {
                    	$scope.listaMonitoramento = response.data.listaMonitoramento;
                    }
                    $scope.qtdRegistros = response.data.qtdRegistros;

                    normalizaDatas();
                    $scope.closeGifModal();
                },
                function(error) {
                    console.log(error);
                    modalService.closeModalErrors(error);
                    $scope.closeGifModal();
                })
        };
        
        $scope.periodosInvalidos = function() {
        	
        	if($scope.isBlank($scope.filter.dataEntradaDe) && !$scope.isBlank($scope.filter.dataEntradaAte)) {
        		var msg = "Periodo de entrada inválido.";
                $scope.showDialogError = true;
                modalService.closeModalErrors(msg);
                return true;
        	}
        	if($scope.isBlank($scope.filter.dataEntradaAte) && !$scope.isBlank($scope.filter.dataEntradaDe)) {
        		var msg = "Periodo de entrada inválido.";
                $scope.showDialogError = true;
                modalService.closeModalErrors(msg);
                return true;
        	}
        	if($scope.isBlank($scope.filter.dataProcDe) && !$scope.isBlank($scope.filter.dataProcAte)) {
        		var msg = "Periodo de processamento inválido.";
                $scope.showDialogError = true;
                modalService.closeModalErrors(msg);
                return true;
        	}
        	if($scope.isBlank($scope.filter.dataProcAte) && !$scope.isBlank($scope.filter.dataProcDe)) {
        		var msg = "Periodo de processamento inválido.";
                $scope.showDialogError = true;
                modalService.closeModalErrors(msg);
                return true;
        	}
        	
        	if(!$scope.isBlank($scope.filter.dataEntradaDe) && !$scope.isBlank($scope.filter.dataEntradaAte)) {

        		var split = $scope.filter.dataEntradaDe.split("/");
        		var dataEntradaDe = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		
        		var split = $scope.filter.dataEntradaAte.split("/");
        		var dataEntradaAte = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		
        		console.log($scope.filter.dataEntradaDe);
        		console.log($scope.filter.dataEntradaAte);
        		console.log(dataEntradaDe);
            	console.log(dataEntradaAte);
        		
        		if(dataEntradaAte < dataEntradaDe) {
        			var msg = "Periodo de entrada inválido.";
                    $scope.showDialogError = true;
                    modalService.closeModalErrors(msg);
                    return true;
        		}
        	}
        	
        	if(!$scope.isBlank($scope.filter.dataProcDe) && !$scope.isBlank($scope.filter.dataProcAte)) {

        		var split = $scope.filter.dataProcDe.split("/");
        		var dataProcDe = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		
        		var split = $scope.filter.dataProcAte.split("/");
        		var dataProcAte = new Date(split[2]+"-"+split[1]+"-"+(parseInt(split[0], 10)));
        		
        		console.log($scope.filter.dataProcDe);
        		console.log($scope.filter.dataProcAte);
        		console.log(dataProcDe);
            	console.log(dataProcAte);
        		
        		if(dataProcAte < dataProcDe) {
        			var msg = "Periodo de processamento inválido.";
        			$scope.showDialogError = true;
        			modalService.closeModalErrors(msg);
        			return true;
        		}
        	}
        	return false;
        };
        
        $scope.finalizar = function(cctId) {
        	$scope.cctId = cctId;
        	var msg = "Deseja finalizar ?";
        	modalService.dialogModal(msg, $scope.finalizar_);
        };
        

        $scope.finalizar_ = function() {
            monitoramentoService.finalizar($scope.apiURL, $scope.cctId).then(
                function(response) {
                    console.log(response.data);
                    $scope.findMonitoramento();
                },
                function(error) {
                    console.log(error);
                    modalService.closeModalErrors(error);
                })
        };

        $scope.changeTipo = function() {
            console.log($scope.tipo);
            if ($scope.tipo.value == "2") {
                $scope.exibeErros = true;
            } else {
                $scope.exibeErros = false;
            }
            $scope.findMonitoramento();
        };

        $scope.showFilter = function() {
            $scope.showFilters = !$scope.showFilters;
        };

        $scope.resetPage = function() {
            $scope.sortType = 'dataEntrada';
            $scope.sortReverse = true;
            $scope.searchText = '';
            $scope.exibeErros = false;
            $scope.listaMonitoramento = [];
        };

        $scope.$on('1', function(event, args) {
            $scope.resetPage();
            $scope.findMonitoramento();
        });
        
        $scope.isBlank = function(string) {
    		if(string == undefined) return true;
    		if(string == null) return true;
    		if(string == "null") return true;
    		if(string == "") return true;
    		return false;
    	};
    	
    	$scope.closeDialogError = function() {
        	$scope.showDialogError = false;
        };

        $scope.findMonitoramento();

        function dateTimeStringToDate(strDate) {
            var dateTime = strDate.split(" ");
            var dateParts = dateTime[0].split("/");
            var timeParts = dateTime[1].split(":");
            return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
        }

        function normalizaDatas() {
            for (let index = 0; index < $scope.listaMonitoramento.length; index++) {

                if ($scope.listaMonitoramento[index].dataEntrada != null) {
                	var data = new Date($scope.listaMonitoramento[index].dataEntrada);
                	


                	$scope.listaMonitoramento[index].dataEntradaString =(data.getDate() < 10 ? "0" + data.getDate() : data.getDate())+                	  
                			"/" +(data.getMonth()+1 < 10 ? "0" + (data.getMonth()+1) : (data.getMonth()+1))
                			+ "/" + data.getFullYear() + 
                			" "+(data.getHours()< 10 ? "0" + data.getHours() : data.getHours())+":"+
                			(data.getMinutes()< 10 ? "0" + data.getMinutes() : data.getMinutes())+":"+
                			(data.getSeconds()< 10 ? "0" + data.getSeconds() : data.getSeconds());

                }

                if ($scope.listaMonitoramento[index].dataProcessamento) {
                	var data = new Date($scope.listaMonitoramento[index].dataProcessamento);
                	$scope.listaMonitoramento[index].dataProcessamentoString = (data.getDate() < 10 ? "0" + data.getDate() : data.getDate())+ 
        			"/" +(data.getMonth()+1 < 10 ? "0" + (data.getMonth()+1) : (data.getMonth()+1))+ "/" +data.getFullYear() + 
        			" "+(data.getHours()< 10 ? "0" + data.getHours() : data.getHours())+":"+
                			(data.getMinutes()< 10 ? "0" + data.getMinutes() : data.getMinutes())+":"+
                			(data.getSeconds()< 10 ? "0" + data.getSeconds() : data.getSeconds());

                }

                if($scope.tipo.value == '3') {
	                for (let index2 = 0; index2 < $scope.listaMonitoramento[index].danfes.length; index2++) {
	
	                    if (index2 == 0)
	                        $scope.listaMonitoramento[index].danfesString = $scope.listaMonitoramento[index].danfes[index2];
	                    else
	                        $scope.listaMonitoramento[index].danfesString = $scope.listaMonitoramento[index].danfesString + " " + $scope.listaMonitoramento[index].danfes[index2];
	
	                }
	
	                for (let index3 = 0; index3 < $scope.listaMonitoramento[index].dues.length; index3++) {
	
	                    if (index3 == 0)
	                        $scope.listaMonitoramento[index].duesString = $scope.listaMonitoramento[index].dues[index3];
	                    else
	                        $scope.listaMonitoramento[index].duesString = $scope.listaMonitoramento[index].duesString + " " + $scope.listaMonitoramento[index].dues[index3];
	
	                }
                }
            }
        }
    }
]);