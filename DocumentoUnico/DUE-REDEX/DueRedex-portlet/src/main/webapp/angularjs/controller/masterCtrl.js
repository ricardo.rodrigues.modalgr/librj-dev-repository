app.controller('MasterCtrl', ['$scope', '$location', '$rootScope', '$modal', 'modalService',
    function($scope, $location, $rootScope, $modal, modalService) {
	
		$scope.hasPermissionDUE = $('#hasPermissionDUE').val();
		$scope.hasPermissionDespachante = $('#hasPermissionDespachante').val();
		$scope.hasPermissionSupervisor = $('#hasPermissionSupervisor').val();
		
		console.log("hasPermissionDUE -> " + hasPermissionDUE);
		console.log("hasPermissionDespachante -> " + hasPermissionDespachante);
		console.log("hasPermissionSupervisor -> " + hasPermissionSupervisor);
	
		$scope.monitoramento = {
				active: true
		};
		$scope.parametros = {
				active: false
		};
		$scope.clienteFinanceiroPagador = {
				active: false
		};
        
        $scope.setActive = function(idPagina) {
        	$scope.monitoramento.active = false;
        	$scope.parametros.active = false;
        	$scope.clienteFinanceiroPagador.active = false;
        	if(idPagina == 1 && $scope.hasPermissionSupervisor) {
        		$scope.monitoramento.active = true;
        	}
        	if(idPagina == 2 && $scope.hasPermissionSupervisor) {
        		$scope.parametros.active = true;
        	}
        	if(idPagina == 3 && hasPermissionDespachante) {
        		$scope.clienteFinanceiroPagador.active = true;
        	}
        	$rootScope.$broadcast(idPagina);
            
        };
        
        $scope.initPaginas = function() {
        	
        	if($scope.hasPermissionSupervisor == "true") {
        		$scope.monitoramento.active = true;
        		$scope.parametros.active = false;
        		$scope.clienteFinanceiroPagador.active = false;
        	} else if($scope.hasPermissionSupervisor == "false") {
        		$scope.monitoramento.active = false;
        		$scope.parametros.active = false;
        		$scope.clienteFinanceiroPagador.active = true;
        	}
        	
        };
        
        $scope.initPaginas();
        
        var mobileView = 992;
        $scope.getWidth = function() {
            return window.innerWidth;
        };
        $scope.$watch($scope.getWidth, function(newValue, oldValue) {
            if (newValue >= mobileView) {

            } else {
                $scope.toggle = false;
            }

        });
        $scope.toggleSidebar = function() {
            $scope.toggle = !$scope.toggle;
        };
        window.onresize = function() {
            $scope.$apply();
        };
        
        $scope.openGifModal = function() {
            $scope.ModalGif = $modal.open({
                templateUrl: '/DueRedexPortlet/paginas/modal-gif.html',
                controller: function($modalInstance) {}
            });
        }

        $scope.closeGifModal = function() {
            if ($scope.ModalGif !== undefined)
                $scope.ModalGif.close();
        }  
    }
]);