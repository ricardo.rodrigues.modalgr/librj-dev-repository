<%@ include file="init.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
    $(document).ready(function() {

        $('#datepicker input').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });
</script>

<%
if(hasPermissionDUE) {
%>
<div ng-app="App" ng-controller="MasterCtrl">

	<link rel="stylesheet" href="/DueRedexPortlet/css/main.css" />
	<link rel="stylesheet" href="/DueRedexPortlet/css/main.min.css" />
	
	<% 
	Group liveGroup = (Group)request.getAttribute("site.liveGroup"); 
	%>
	<div id="divUrlApi" style="visibility:hidden">
	  <liferay-ui:custom-attribute className="com.liferay.portal.model.Group"
	         classPK="<%= (liveGroup != null) ? liveGroup.getGroupId() : 0 %>"
	         editable="<%= false %>" 
	         label="<%= false %>" 
	         name="dueRedexApiUrl"   />
	</div>
	<input type="hidden" id="usuario" name="usuario" value='${nmUserLogado}' />
	<input type="hidden" id="cpf" name="cpf" value='${cpf}' />
	<input type="hidden" id="validado" name="validado" value='${validado}' />
	
	<input type="hidden" id="hasPermissionDUE" name="hasPermissionDUE" value='${hasPermissionDUE}' />
	<input type="hidden" id="hasPermissionDespachante" name="hasPermissionDespachante" value='${hasPermissionDespachante}' />
	<input type="hidden" id="hasPermissionSupervisor" name="hasPermissionSupervisor" value='${hasPermissionSupervisor}' />
	
	<input type="hidden" id="portletGrVersion" value="1.088">

	<div id="page-wrapper" class="container-fluid" ng-cloak>
		<div class="row">

			<div class="span12" style="margim: 0px;">
				<div class="control-group" style="margim: 0px;">
					<ul class="nav nav-tabs" style="margim: 0px;">

						<%if(hasPermissionSupervisor) {%>
							<li class="{{pagina.active}}">
								<a data-ng-click="setActive(1)" style="cursor: pointer;"> Monitoramento </a>
							</li>
							<li class="{{pagina.active}}">
								<a data-ng-click="setActive(2)" style="cursor: pointer;"> Parametrização </a>
							</li>
							<li class="{{pagina.active}}">
								<a data-ng-click="setActive(3)" style="cursor: pointer;"> Cliente Financeiro Pagador </a>
							</li>							
						<%}%>
						
					</ul>
				</div>
				<div class="row-fluid" style="margim: 0px;">
				
						<div data-ng-show="monitoramento.active" style="margim: 0px;" ng-cloak>
							<%if(hasPermissionSupervisor) {%>
								<%@ include file="paginas/monitoramento.jsp"%>
							<%}else{%>
								<div class="alert alert-error">Você não tem permissão de acessar essa funcionalidade</div>
							<%}%>
						</div>
						
						<div data-ng-show="parametros.active" style="margim: 0px;" ng-cloak>
							<%if(hasPermissionSupervisor) {%>
								<%@ include file="paginas/parametros.jsp"%>
							<%}else{%>
								<div class="alert alert-error">Você não tem permissão de acessar essa funcionalidade</div>
							<%}%>
						</div>
						
						<div data-ng-show="clienteFinanceiroPagador.active" style="margim: 0px;" ng-cloak>
							<%if(hasPermissionSupervisor) {%>
								<%@ include file="paginas/clienteFinanceiroPagador.jsp"%>
							<%}else{%>
								<div class="alert alert-error">Você não tem permissão de acessar essa funcionalidade</div>
							<%}%>
						</div>
						
						
				</div>
			</div>
		</div>
	</div>

</div>
 
 <% } else {%>
 		<div class="alert alert-error">Você não tem permissão de acessar essa funcionalidade</div>
 <% } %>