<div ng-controller="monitoramentoController" ng-cloak>

    <div class="page-header" style="margin-left: 1%;margin-top: 0%;margin-bottom: 0%;">


        <div class="row-fluid">

            <label style="float: left;"> 
				<input type="radio" ng-model="tipo.value" value="1" ng-change="changeTipo()" name="tipo">
				<b>ATIVOS</b>
			</label>
            <label style="float: left;"> 
				<input type="radio" ng-model="tipo.value" value="2" ng-change="changeTipo()" name="tipo"> 
				<b>ERROS</b>
			</label>
            <label style="float: left;"> 
				<input type="radio" ng-model="tipo.value" value="3" ng-change="changeTipo()" name="tipo"> 
				<b>ENTREGUES</b>
			</label>

            <div class="span4" style="float: right; margin-top: 15px;">
                <select class="span2" style="float: right; width: 60px;" ng-options="item as item.qtd for item in qtdporpaginalist track by item.id" ng-model="qtdporpagina" ng-change="changeqtdporpagina()"></select>
                <label style="float: right;">Itens por p�gina</label>
            </div>

        </div> 

    </div>

    <div class="row-fluid span12" ng-show="!showFilters">
        <a href="#" title="Exibir filtros" class="pull-left" data-ng-click="showFilter()"><i class="fa fa-search-plus"></i> Exibir filtros</a>
    </div>
    <div class="row-fluid span12" ng-show="showFilters">
        <a href="#" title="Esconder filtros" class="pull-left" data-ng-click="showFilter()"><i class="fa fa-search-minus"></i> Esconder filtros</a>
    </div>

    <div class="row-fluid span12" ng-show="showFilters" ng-cloak style="background-color: rgb(218,218,218); border-radius: 12px; margin-left: 15px; margin-bottom: 10px;">

        <div class="row-fluid" id="labelDUE" style="margin-top: 10px; margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">Navio:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.navio" id="filterNavio" capitalize>

            <label class="span2" style="float: left; width: 110px;">Visit:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.visit" id="filterVisit" capitalize>
        </div>

        <div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">Line Operator:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.lineOperator" id="filterLineOperator" capitalize>

            <label class="span2" style="float: left; width: 110px;">Booking:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.booking" id="filterBooking" capitalize>
        </div>

        <div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">Conteiner:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.conteiner" id="filterConteiner" capitalize>

            <label class="span2" style="float: left; width: 110px;">Danfe:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.danfe" id="filterDanfe">
        </div>

        <div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 110px;">DUE:</label>
            <input type="text" style="border-radius: 12px; float: left;" class="input-block-level span3" ng-model="filter.due" id="filterDue" capitalize>
        </div>
        
        <div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 205px;">Periodo de entrada - de:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataEntradaDe" id="filterDataEntradaDe">
			</div>

            <label class="span1'" style="float: left;margin-left: 5px; width: 30px;">at�:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataEntradaAte" id="filterDataEntradaAte">
			</div>
        </div>
        
        <div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <label class="span2" style="float: left; width: 205px;">Periodo de processamento - de:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataProcDe" id="filterDataProcDe">
			</div>

            <label class="span1'" style="float: left;margin-left: 5px; width: 30px;">at�:</label>
            <div id="datepicker" class="span2" style="margin: 0px;">
            	<input style="border-radius: 12px;" type="text" class="input-block-level" data-inputmask-regex="[0-9.-]{4}/[0-9.-]{2}/[0-9.-]{4}" placeholder="dd/mm/yyyy" ng-model="filter.dataProcAte" id="filterDataProcAte">
			</div>
        </div>

		<div class="row-fluid" id="labelDUE" style="margin-left: 10px;">
            <button ng-click="findMonitoramento()" style="height: 30px; margin-right: 4%; text-align: center; padding: 1px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px; margin-bottom: 10px; margin-top: 10px;" class="btn btn-primary">Filtrar</button>
       		<button ng-click="limparCampos()" style="height: 30px; margin-right: 4%; text-align: center; padding: 1px 0; font-size: 12px; line-height: 1; border-radius: 15px; width: 80px; margin-bottom: 10px; margin-top: 10px;" class="btn btn-primary">Limpar</button>
        </div>

    </div>

    <div class="row-fluid" ng-cloak>

        <div class="span12">

            <table class="table table-bordered" style="overflow:scroll;display:block;">

                <thead>
                    <tr>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'navio'; sortReverse = !sortReverse">
							Navio 
							<span ng-show="sortType == 'navio' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'navio' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'visit'; sortReverse = !sortReverse">
							Visit 
							<span ng-show="sortType == 'visit' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'visit' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'lineOp'; sortReverse = !sortReverse">
							Line Operator 
							<span ng-show="sortType == 'lineOp' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'lineOp' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'booking'; sortReverse = !sortReverse">
							Booking 
							<span ng-show="sortType == 'booking' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'booking' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'conteiner'; sortReverse = !sortReverse">
							Conteiner 
							<span ng-show="sortType == 'conteiner' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'conteiner' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
							DANFE
                        </th>
                        <th style="text-align:center;">
							DU-E
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'status'; sortReverse = !sortReverse">
							Status 
							<span ng-show="sortType == 'status' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'status' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'situacao'; sortReverse = !sortReverse">
							Situa��o
							<span ng-show="sortType == 'situacao' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'situacao' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'bloqueado'; sortReverse = !sortReverse">
							Bloqueado 
							<span ng-show="sortType == 'bloqueado' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'bloqueado' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'mensagem'; sortReverse = !sortReverse">
							Mensagem 
							<span ng-show="sortType == 'mensagem' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'mensagem' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'dataEntrada'; sortReverse = !sortReverse">
							Data Entrada 
							<span ng-show="sortType == 'dataEntrada' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'dataEntrada' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th style="text-align:center;">
                            <a href="#" ng-click="sortType = 'dataProcessamento'; sortReverse = !sortReverse">
							Data Processamento 
							<span ng-show="sortType == 'dataProcessamento' && !sortReverse" class="fa fa-caret-down"></span> 
							<span ng-show="sortType == 'dataProcessamento' && sortReverse" class="fa fa-caret-up"></span>
						</a>
                        </th>
                        <th ng-show="exibeErros"></th>
                    </tr>
                </thead>

                <tbody>
					<tr ng-repeat="cct in listaMonitoramento | orderBy:sortType:sortReverse" style="cursor: pointer;">
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.navio }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.visit }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.lineOp }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.booking }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.conteiner }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.danfesString }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.duesString }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.status }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.situacao }}</td>
                        <td ng-click="showDetailCCT(cct)">
                            <i class="fa fa-stop" style="color: green;" ng-show="!cct.bloqueado"></i>
                            <i class="fa fa-stop" style="color: red;" ng-show="cct.bloqueado"></i>
                        </td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.mensagem }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.dataEntradaString }}</td>
                        <td ng-click="showDetailCCT(cct)" style="text-align:center;font-size: 11px;">{{ cct.dataProcessamentoString }}</td>
                        <td ng-show="exibeErros">
                            <a href="#" title="Finalizar" class="pull-right" data-ng-click="finalizar(cct.id)"><i class="icon-remove icon-white" style="color:#013B53 !important;margin-right:10px;"></i></a>
                        </td>

                    </tr>

                </tbody>
            </table>
			<div class="row-fluid">
				<div style="float: right;">
					<p>Total registros {{ qtdRegistros }}</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="pagination pull-left" style="margin: 0px;">

					<paging page="currentPage" page-size="itemsPerPage"
						total="qtdRegistros" show-prev-next="true" show-first-last="true"
						paging-action="findMonitoramento(page)"> </paging>
				</div>
			</div>

		</div>

    </div>
</div>    
