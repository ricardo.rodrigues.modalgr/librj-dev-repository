<section ng-controller="cfpController">
    <div style="margin-left: 10px">
        <div class="row-fluid">
            <h5 class="span2">Despachante:</h5>

            <div class="span2">
                <input class="input-xlarge search-query" type="text" value="{{usuario}}" style="margin-left: -106px; height: 30px; margin-top: 7px" disabled>
            </div>
        </div>

        <form>
            <h2>Libera��o para Embarque - Exporta��o</h2>

            <br>

            <div class="row-fluid">
                <h5 class="span2">Booking:</h5>

                <div class="span2">
                
                    <input class="input-medium search-query" type="text" style="height: 30px; margin-left: -106px; margin-top: 8px" ng-model="booking" ng-change="changeBooking()" title="Pesquisar" capitalize>
                </div>

                <div class="span2">
                    <button style="width: 30px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px;margin-left: -140px; margin-top: 5px" title="Pesquisar" class="btn-primary span2" ng-click="pesquisaBooking()">
                        <i class="icon-search"></i>
                    </button>
                </div>
            </div>

            <div class="row-fluid">
                <h5 class="span2">Armador:</h5>

                <div class="span2">
                    <select style="margin-left: -106px; height: 25px; margin-top: 6px" class="input-xlarge search-query" id="lineOp" name="lineOp" ng-model="lineOpSel" ng-options="lineOp as lineOp.label for lineOp in lineOpList track by lineOp.gkey">
					</select>
                </div>
                <div style="margin-left: 1030px;">
                    <input type="checkbox" id="checarTodos" ng-change="checarTodos()" ng-model="checkTodos">
                    <span>Selecionar todos</span>
                </div>
            </div>

            <table class="table table-bordered" id="tabela">
                <thead>
                    <tr>
                        <th>Conteiner</th>
                        <th>DUE</th>
                        <th>Exportador</th>
                        <th>Status</th>
                        <th>Confirmar</th>
                    </tr>
                </thead>


                <tbody>
                	<tr ng-repeat="registro in listaTabela | orderBy:sortType:sortReverse | filter:searchText">
                		<td style="text-align:center;font-size: 11px;">{{ registro.conteiner }}</td>
                		<td style="text-align:center;font-size: 11px;">{{ registro.due }}</td>
                		
                		<td style="text-align:center;font-size: 11px;">
							<select style="height: 25px; margin-top: 6px"
								class="input-xlarge search-query" id="exportadores" name="exportadores"
								ng-model="registro.exportadorSel" ng-options="exp as exp.label for exp in registro.exportadores track by exp.id"
								ng-disabled="(registro.status == 'VENCIDO' || registro.status == 'INEXISTENTE' || registro.status == 'NAO_REPRESENTA')" ng-change="changeExportador(registro)">
							</select>
						</td>
						
						<td style="text-align:center;font-size: 11px;" ng-show="registro.mensagem == null && registro.status == null"></td>
						<td style="text-align:center;font-size: 11px;" ng-show="registro.confirmado == 1 && registro.status == null">ALTERADO</td>
						<td style="text-align:center;font-size: 11px;" ng-show="registro.status == 'VENCIDO'">Procura��o Vencida</td>
						<td style="text-align:center;font-size: 11px;" ng-show="registro.status == 'INEXISTENTE'">Cadastro Inexistente Contate o RC</td>
						<td style="text-align:center;font-size: 11px;" ng-show="registro.status == 'NAO_REPRESENTA'">N�o representa</td>
						
						<td style="text-align:center;font-size: 11px;">
                            <label class="checkbox inline">
                                <input type="checkbox" id="inlineCheckbox1" ng-model="registro.checado" ng-checked="registro.checado" ng-disabled="(registro.status == 'VENCIDO' || registro.status == 'INEXISTENTE' || registro.status == 'NAO_REPRESENTA')"> 
                            </label>
                        </td>
                    </tr>

                </tbody>
            </table>

            <div class="row-fluid">
                <div class="span10"></div>

                <div class="span1">
                    <button ng-click="salvar()" style="width: 30px;height: 30px;text-align: center;padding: 6px 0;font-size: 12px;line-height: 1;border-radius: 15px; margin-left: 118px" title="Salvar" class="btn-primary span2">
                        <i class="fa fa-check"></i>
                    </button>
                </div>
            </div>

        </form>
    </div>

</section>
