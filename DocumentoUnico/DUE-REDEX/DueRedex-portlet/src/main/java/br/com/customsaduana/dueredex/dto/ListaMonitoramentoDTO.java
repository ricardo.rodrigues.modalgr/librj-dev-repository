package br.com.customsaduana.dueredex.dto;

import java.util.Date;
import java.util.List;

public class ListaMonitoramentoDTO {
	
	private long id;

	private String navio;
	
	private String visit;
	
	private String lineOp;
	
	private String booking;
	
	private String conteiner;
	
	private List<String> danfes;
	
	private List<String> dues;
	
	private String status;
	
	private String situacao;
	
	private boolean bloqueado;
	
	private String mensagem;
	
	private Date dataEntrada;
	
	private Date dataProcessamento;
	
	private String erro;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNavio() {
		return navio;
	}

	public void setNavio(String navio) {
		this.navio = navio;
	}

	public String getVisit() {
		return visit;
	}

	public void setVisit(String visit) {
		this.visit = visit;
	}

	public String getLineOp() {
		return lineOp;
	}

	public void setLineOp(String lineOp) {
		this.lineOp = lineOp;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getConteiner() {
		return conteiner;
	}

	public void setConteiner(String conteiner) {
		this.conteiner = conteiner;
	}

	public List<String> getDanfes() {
		return danfes;
	}

	public void setDanfes(List<String> danfes) {
		this.danfes = danfes;
	}

	public List<String> getDues() {
		return dues;
	}

	public void setDues(List<String> dues) {
		this.dues = dues;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
	
}
