package br.com.customsaduana.dueredex.dto;

import java.util.List;

public class Tabela {

	private long idExportadorCct;

	private String mensagem;

	private String conteiner;

	private String idExterno;

	private String due;

	private List<Exportador> exportadores;

	private Exportador exportadorSel;
	
	private String idExportadorSel;

	private String status;

	private int confirmado;

	private boolean alterado;
	
	private boolean checado;

	public long getIdExportadorCct() {
		return idExportadorCct;
	}

	public void setIdExportadorCct(long idExportadorCct) {
		this.idExportadorCct = idExportadorCct;
	}

	public String getMensagem() {
		return mensagem;
	}

	public String getConteiner() {
		return conteiner;
	}

	public String getDue() {
		return due;
	}

	public List<Exportador> getExportadores() {
		return exportadores;
	}

	public String getStatus() {
		return status;
	}

	public int getConfirmado() {
		return confirmado;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setConteiner(String conteiner) {
		this.conteiner = conteiner;
	}

	public void setDue(String due) {
		this.due = due;
	}

	public void setExportadores(List<Exportador> exportadores) {
		this.exportadores = exportadores;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setConfirmado(int confirmado) {
		this.confirmado = confirmado;
	}

	public boolean getAlterado() {
		return alterado;
	}
	
	public boolean isAlterado() {
		return alterado;
	}

	public void setAlterado(boolean alterado) {
		this.alterado = alterado;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}

	public Exportador getExportadorSel() {
		return exportadorSel;
	}

	public void setExportadorSel(Exportador exportadorSel) {
		this.exportadorSel = exportadorSel;
	}

	public String getIdExportadorSel() {
		return idExportadorSel;
	}

	public void setIdExportadorSel(String idExportadorSel) {
		this.idExportadorSel = idExportadorSel;
	}
	
	public boolean getChecado() {
		return checado;
	}

	public boolean isChecado() {
		return checado;
	}

	public void setChecado(boolean checado) {
		this.checado = checado;
	}
}
