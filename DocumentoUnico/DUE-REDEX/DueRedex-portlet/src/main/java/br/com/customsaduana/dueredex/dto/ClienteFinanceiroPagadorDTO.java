package br.com.customsaduana.dueredex.dto;

import java.util.List;

public class ClienteFinanceiroPagadorDTO {

	private String booking;

	private String despachante;

	private List<Tabela> listaTabela;

	private LineOperator lineOperator;

	private List<LineOperator> lineOperatorList;

	private String mensagem;

	private boolean erro;

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getDespachante() {
		return despachante;
	}

	public void setDespachante(String despachante) {
		this.despachante = despachante;
	}

	public LineOperator getLineOperator() {
		return lineOperator;
	}

	public void setLineOperator(LineOperator lineOperator) {
		this.lineOperator = lineOperator;
	}

	public List<LineOperator> getLineOperatorList() {
		return lineOperatorList;
	}

	public void setLineOperatorList(List<LineOperator> lineOperatorList) {
		this.lineOperatorList = lineOperatorList;
	}

	public List<Tabela> getListaTabela() {
		return listaTabela;
	}

	public void setListaTabela(List<Tabela> listaTabela) {
		this.listaTabela = listaTabela;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public boolean isErro() {
		return erro;
	}

	public void setErro(boolean erro) {
		this.erro = erro;
	}
}
