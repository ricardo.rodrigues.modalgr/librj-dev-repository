package br.com.customsaduana.dueredex.portlet;


import java.io.IOException;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class DueRedexPortlet extends GenericPortlet {
	
	private static final Log logger = LogFactoryUtil.getLog(DueRedexPortlet.class);

    private static final String NORMAL_VIEW = "/home.jsp";
    private static final String MAXIMIZED_VIEW = "/home.jsp";
    private static final String HELP_VIEW = "/help.html";

    private PortletRequestDispatcher normalView;
    private PortletRequestDispatcher maximizedView;
    private PortletRequestDispatcher helpView;

    public void doView( RenderRequest request, RenderResponse response )
        throws PortletException, IOException {
    	
    	logger.info("DueRedexPotlet OK");
    	
    	normalView = request.getPortletSession().getPortletContext().getRequestDispatcher( NORMAL_VIEW );
    	maximizedView = request.getPortletSession().getPortletContext().getRequestDispatcher( MAXIMIZED_VIEW );
    	helpView = request.getPortletSession().getPortletContext().getRequestDispatcher( HELP_VIEW );
        if( WindowState.MINIMIZED.equals( request.getWindowState() ) ) {
            return;
        }

        if ( WindowState.NORMAL.equals( request.getWindowState() ) ) {
            normalView.include( request, response );
        } else {
            maximizedView.include( request, response );
        }
    }

    protected void doHelp( RenderRequest request, RenderResponse response )
        throws PortletException, IOException {

        helpView.include( request, response );

    }

    public void init( PortletConfig config ) throws PortletException {
    	
    	logger.info("DueRedexPotlet init OK");
    	
    	super.init( config );

    }

    public void destroy() {
        normalView = null;
        maximizedView = null;
        helpView = null;
        super.destroy();
    }

}
