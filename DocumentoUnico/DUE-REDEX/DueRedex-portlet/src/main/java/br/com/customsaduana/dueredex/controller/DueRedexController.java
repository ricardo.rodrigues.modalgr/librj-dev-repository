package br.com.customsaduana.dueredex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.customsaduana.dueredex.dto.ClienteFinanceiroPagadorDTO;
import br.com.customsaduana.dueredex.dto.MonitoramentoDTO;
import br.com.customsaduana.dueredex.dto.MonitoramentoRequestDTO;
import br.com.customsaduana.dueredex.dto.Parametros;
import br.com.customsaduana.dueredex.service.DueRedexService;



@RestController
@RequestMapping("/dueredex")
public class DueRedexController {
	
	@Autowired
	private DueRedexService dueRedexService;	
	
	@RequestMapping(value = "api/monitoramento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MonitoramentoDTO findMonitoramento(@RequestBody(required = true) MonitoramentoRequestDTO dto) {
		return dueRedexService.findMonitoramento(dto);
	}
	
	@RequestMapping(value = "api/monitoramento/finalizar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String finalizar(@RequestParam("cctId") long cctId) {
		return dueRedexService.finalizar(cctId);
	}
	
	@RequestMapping(value = "/api/parametros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> getParametrosDivergenciaPeso() {
		return dueRedexService.getParametrosDivergenciaPeso();
	}
	
	@RequestMapping(value = "/api/parametros/divergenciapeso", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> salvarParametrosDivergenciaPeso(@RequestBody(required = true) List<Parametros> parametros) {
		return dueRedexService.salvarParametrosDivergenciaPeso(parametros);
	}
	
	@RequestMapping(value = "/api/parametros/clientefinanceiropagador", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Parametros salvarParametrosClienteFinanceiroPagador(@RequestBody(required = true) Parametros parametro) {
		return dueRedexService.salvarParametrosClienteFinanceiroPagador(parametro);
	}
	
	@RequestMapping(value = "/api/parametros/balanca", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Parametros> salvarParametrosBalanca(@RequestBody(required = true) List<Parametros> parametroList) {
		return dueRedexService.salvarParametrosBalanca(parametroList);
	}
	
	@RequestMapping(value = "api/cfp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO getDadosClienteFinanceiroPagador(@RequestParam("bookingNbr") String bookingNbr, @RequestParam("lineOperatorGkey") String lineOperatorGkey, @RequestParam("despachante") String despachante) {
		return dueRedexService.getDadosClienteFinanceiroPagador(bookingNbr, lineOperatorGkey, despachante);
	}
	
	@RequestMapping(value = "/api/validaparam", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public int validaCheckObrigatorio() {
		return dueRedexService.validaCheckObrigatorio();
	}
	
	@RequestMapping(value = "/api/savecfp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ClienteFinanceiroPagadorDTO salvaListaCFP(@RequestBody(required = true) ClienteFinanceiroPagadorDTO dto) {
		return dueRedexService.salvaListaCFP(dto);
	}	
	
}
